/* Generated code for Python module 'ipykernel.eventloops'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_ipykernel$eventloops" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_ipykernel$eventloops;
PyDictObject *moduledict_ipykernel$eventloops;

/* The declarations of module constants used, if any. */
static PyObject *const_str_plain_Start;
static PyObject *const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple;
static PyObject *const_tuple_f84687c6b4b11266e386ae3c1c2f70af_tuple;
static PyObject *const_str_plain__gtk;
static PyObject *const_dict_dfc6781eab05c37013adf5c506a72c2c;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain_loop_asyncio_exit;
static PyObject *const_tuple_ad7d101f73dd0a06e06f2d1d73ce98d9_tuple;
static PyObject *const_str_plain_shutdown_asyncgens;
extern PyObject *const_str_digest_7103caa43923d1b024b5f303388aff54;
extern PyObject *const_str_plain_wx;
extern PyObject *const_str_plain___name__;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_FD;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_plain_IPWxApp;
static PyObject *const_str_plain_EVT_TIMER;
static PyObject *const_str_digest_5fc1751031eed6aa9529864cd935023c;
static PyObject *const_str_plain_QTimer;
static PyObject *const_str_plain_deletefilehandler;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_str_plain_osx_tuple;
static PyObject *const_str_digest_039ffb3d0d7bf635ae665cf0b1b4e3f4;
extern PyObject *const_str_plain_run_forever;
static PyObject *const_str_digest_14f4623b35f4ef5cde8d7a15e5f93278;
static PyObject *const_str_plain_get_app_qt4;
static PyObject *const_str_plain_loop_cocoa_exit;
extern PyObject *const_str_plain_notebook;
extern PyObject *const_str_digest_fe66bcb374d8283cea65f90daae59bd4;
static PyObject *const_str_digest_67e3e3acb75f1a3055eb1098afd63fe5;
extern PyObject *const_str_plain_gtk;
static PyObject *const_str_digest_84a6e22bb74dc37ec7c374a46500eff1;
extern PyObject *const_str_plain_withdraw;
extern PyObject *const_str_plain_exit_now;
static PyObject *const_str_digest_0f638692e21be1921314f231ba0d5918;
extern PyObject *const_str_plain_call_soon;
static PyObject *const_str_plain_loop_tk_exit;
static PyObject *const_str_plain_loop_gtk3_exit;
static PyObject *const_str_digest_0b2f2d231243d6392b8b730031ee445f;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_widget;
extern PyObject *const_str_plain_a;
static PyObject *const_str_plain__loop_qt;
static PyObject *const_str_plain_exit_func;
static PyObject *const_str_digest_2c4c8d69412c28219166b7ab6b355ef6;
static PyObject *const_str_digest_965aac207295f6aebed813fb543a2cb5;
static PyObject *const_str_plain_TimerFrame;
extern PyObject *const_str_plain_signal;
extern PyObject *const_str_plain_instance;
extern PyObject *const_tuple_str_plain_partial_tuple;
static PyObject *const_str_plain_ExitMainLoop;
static PyObject *const_tuple_str_plain_tk_tuple;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
static PyObject *const_str_plain_OnInit;
extern PyObject *const_str_plain_Frame;
extern PyObject *const_str_plain_gtk3;
extern PyObject *const_str_plain_Application;
extern PyObject *const_str_plain_callable;
static PyObject *const_tuple_str_plain_stream_str_plain_notifier_str_plain_kernel_tuple;
static PyObject *const_str_digest_e2c7751dfcc49ea3b9e68c3deaf94833;
static PyObject *const_tuple_str_plain_nope_tuple;
extern PyObject *const_str_plain_func;
extern PyObject *const_str_plain___stdout__;
static PyObject *const_str_digest_62efe02fdb3e422cbc100b1b6331c18f;
extern PyObject *const_str_plain_flush;
extern PyObject *const_tuple_str_plain_wx_tuple;
extern PyObject *const_str_plain_new_event_loop;
static PyObject *const_tuple_str_plain_stream_str_plain_kernel_tuple;
extern PyObject *const_str_plain_start;
extern PyObject *const_str_plain_frame;
extern PyObject *const_str_plain_platform;
extern PyObject *const_tuple_true_tuple;
static PyObject *const_str_plain__should_close;
extern PyObject *const_str_plain_connect;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_digest_a71365800d2c40a6537ed3cf18b749d3;
static PyObject *const_str_plain_on_timer;
extern PyObject *const_str_plain_qt;
extern PyObject *const_str_plain_timeout;
static PyObject *const_str_plain_loop_wx;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_quit;
static PyObject *const_tuple_str_plain_qt_str_plain_qt5_tuple;
extern PyObject *const_str_digest_f0b65ebf32c38a05199c60f79a951b9a;
extern PyObject *const_str_plain_environ;
extern PyObject *const_str_plain_close;
static PyObject *const_str_digest_36c984a8ee7390941177662bc489b714;
static PyObject *const_str_plain_ipympl;
extern PyObject *const_str_plain_loop;
extern PyObject *const_str_plain_s;
static PyObject *const_str_plain_register_integration;
extern PyObject *const_str_plain_nope;
extern PyObject *const_str_plain_redirect;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_b9c9692660661ee5f06ab97d344bf8fc;
static PyObject *const_str_digest_cce2d97ed5b6d1fec07023491fdede4e;
static PyObject *const_str_digest_e9c4e2ebaa0b1f02607e723383994479;
static PyObject *const_str_plain_loop_qt_exit;
extern PyObject *const_str_plain_add_reader;
extern PyObject *const_str_plain_value;
static PyObject *const_str_digest_4994da5742005379d5c09c0ac8d5f35c;
static PyObject *const_str_plain_close_loop;
static PyObject *const_str_plain__notify_stream_qt;
static PyObject *const_str_digest_21ebb8eee3fecbf4d4ad63ba91522984;
extern PyObject *const_str_plain_is_running;
extern PyObject *const_str_plain_after;
extern PyObject *const_str_plain_kernel;
extern PyObject *const_str_plain_limit;
static PyObject *const_tuple_str_plain_toolkitnames_str_plain_decorator_tuple;
static PyObject *const_tuple_str_plain_asyncio_tuple;
static PyObject *const_tuple_str_plain_mainloop_str_plain_stop_tuple;
extern PyObject *const_str_plain_e;
static PyObject *const_str_digest_4a347beb0791a92fa68aec31d137b09d;
static PyObject *const_str_plain_pyqt5;
static PyObject *const_str_plain_handle_int;
extern PyObject *const_str_plain_keys;
static PyObject *const_str_plain_loop_map;
static PyObject *const_str_digest_b2d60b51f38a7258ac0dfb3c34db5b9c;
extern PyObject *const_str_plain_fd;
static PyObject *const_str_digest_f5f0542cf37066ad9d393b3514b1c31a;
extern PyObject *const_str_plain_default_int_handler;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_loop_gtk_exit;
static PyObject *const_dict_f22dd43ccca7e02b2f29370abbf82a84;
extern PyObject *const_str_plain_V;
extern PyObject *const_str_plain_mac_ver;
extern PyObject *const_str_plain__darwin_app_nap;
static PyObject *const_tuple_str_plain_self_str_plain_TimerFrame_str_plain_wake_tuple;
extern PyObject *const_str_plain_GTKEmbed;
static PyObject *const_tuple_9e7831bee705c2088df553c5ae6dd9ec_tuple;
static PyObject *const_str_digest_7d7c4a4759da263eab1d31f52e3667fa;
static PyObject *const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple;
extern PyObject *const_tuple_str_plain_loop_tuple;
static PyObject *const_tuple_str_plain_stream_str_plain_a_str_plain_kw_str_plain_app_tuple;
static PyObject *const_str_plain_gtk_kernel;
extern PyObject *const_str_plain_appnope;
static PyObject *const_str_plain__in_event_loop;
static PyObject *const_str_digest_00c3f8c06b14a3722161887dd5baae41;
extern PyObject *const_tuple_str_digest_08a8820022fa063f6c6662cf23ef51be_tuple;
extern PyObject *const_str_plain_name;
static PyObject *const_str_plain_toolkitnames;
static PyObject *const_tuple_str_plain_stop_tuple;
static PyObject *const_str_plain_activated;
static PyObject *const_list_str_space_list;
extern PyObject *const_tuple_str_plain_kernel_tuple;
extern PyObject *const_str_plain_wake;
static PyObject *const_str_plain_exit_decorator;
extern PyObject *const_str_plain_is_closed;
extern PyObject *const_tuple_str_plain_Application_tuple;
static PyObject *const_tuple_str_digest_62efe02fdb3e422cbc100b1b6331c18f_tuple;
static PyObject *const_str_plain_createfilehandler;
extern PyObject *const_str_plain_error;
static PyObject *const_str_plain_setEnabled;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_SIGINT;
static PyObject *const_str_digest_b8c1173120a40eec7a007296f19f6fe0;
extern PyObject *const_str_plain_exit_hook;
extern PyObject *const_str_plain_etype;
static PyObject *const_str_plain_QT_API;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_inline;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_stream;
static PyObject *const_str_plain__use_appnope;
extern PyObject *const_str_plain_exec_;
static PyObject *const_tuple_str_plain_gui_str_plain_kernel_str_plain_e_str_plain_loop_tuple;
extern PyObject *const_str_plain_destroy;
static PyObject *const_str_digest_ed580d2cf2fea27977dfab5a03530f45;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_zmq;
static PyObject *const_str_plain_loop_asyncio;
extern PyObject *const_str_plain_tb;
static PyObject *const_str_digest_445cf3bf72607f9eb9f9bebe979fd350;
static PyObject *const_str_digest_b14ca5a183b199cb4df97fbc116e19b1;
static PyObject *const_str_digest_41c30847a44603e18da0fe3098acb1cb;
extern PyObject *const_str_digest_08a8820022fa063f6c6662cf23ef51be;
extern PyObject *const_int_pos_1000;
extern PyObject *const_str_plain_asyncio;
static PyObject *const_tuple_str_plain_kernel_str_plain_GTKEmbed_str_plain_gtk_kernel_tuple;
extern PyObject *const_str_plain_shell_streams;
extern PyObject *const_str_plain_timer;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_qt5;
static PyObject *const_tuple_str_plain_gtk3_tuple;
extern PyObject *const_str_plain_tk;
extern PyObject *const_str_plain_event;
static PyObject *const_str_plain_setSingleShot;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_95d07a8a573117c8a04622ad0ece989a;
extern PyObject *const_str_plain_qt4;
extern PyObject *const_str_plain_eventloop;
static PyObject *const_tuple_str_plain_stream_str_plain_loop_tuple;
extern PyObject *const_str_plain__poll_interval;
static PyObject *const_str_plain_Exit;
static PyObject *const_str_plain_real_excepthook;
static PyObject *const_str_plain_loop_wx_exit;
static PyObject *const_tuple_str_plain_kernel_str_plain_wx_tuple;
extern PyObject *const_tuple_str_digest_fe66bcb374d8283cea65f90daae59bd4_tuple;
static PyObject *const_tuple_str_plain_GTKEmbed_tuple;
static PyObject *const_str_plain_loop_tk;
extern PyObject *const_str_angle_lambda;
static PyObject *const_str_digest_2d4ee65694e57b933f5856d0c282944c;
extern PyObject *const_str_plain_type;
static PyObject *const_tuple_str_plain_qt4_tuple;
static PyObject *const_tuple_str_plain_kernel_str_plain_stop_tuple;
static PyObject *const_str_digest_c14f651ff533bc3e31bd18bcef523a7c;
extern PyObject *const_str_space;
extern PyObject *const_str_plain_app;
static PyObject *const_tuple_21580f539e3be0854d6691bc0ed6dad4_tuple;
extern PyObject *const_str_plain_Show;
static PyObject *const_tuple_str_plain_kernel_str_plain_get_app_qt4_str_plain_s_tuple;
extern PyObject *const_str_plain_get_event_loop;
extern PyObject *const_str_plain_exit;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_ab961728ca2578f1a92223bd29998ce8;
extern PyObject *const_str_plain_getsignal;
extern PyObject *const_str_plain___class__;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_str_digest_68e19db4deb3cfd9bcff9bbba59a16fc;
static PyObject *const_str_plain_notifier;
extern PyObject *const_str_plain_set_event_loop;
static PyObject *const_str_digest_dc4f4e7db006731ca43e14f0afdcb9c0;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_functools;
extern PyObject *const_str_plain_excepthook;
extern PyObject *const_str_plain_sys;
static PyObject *const_tuple_str_plain_exit_func_str_plain_func_tuple;
static PyObject *const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple;
static PyObject *const_str_plain_loop_qt5;
extern PyObject *const_tuple_str_plain_app_tuple;
extern PyObject *const_str_plain_kw;
static PyObject *const_str_plain_READABLE;
static PyObject *const_str_plain_loop_cocoa;
extern PyObject *const_str_plain_gui;
extern PyObject *const_str_plain_partial;
static PyObject *const_str_plain_process_stream_events;
static PyObject *const_str_plain_Read;
static PyObject *const_str_digest_ad8cb7ff5767f8253cefcb2821b0e89d;
extern PyObject *const_str_plain_Timer;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_4fc3b1699fcd0924231a3ffcab71de3c;
static PyObject *const_str_digest_4b7ea5a266a558058bbe98690987425b;
extern PyObject *const_str_plain_shell;
extern PyObject *const_str_plain_initialized;
static PyObject *const_str_plain_poll_interval;
static PyObject *const_str_plain_MainLoop;
static PyObject *const_str_plain_nbagg;
static PyObject *const_str_digest_858e5195e53addafe6d0eb7f2e812e18;
static PyObject *const_dict_3cfeb0dac4449d6bb047ee2d6ba92616;
static PyObject *const_str_digest_73c970d07f1cc7f805ecc5b6fc3991d6;
extern PyObject *const_str_plain_stop;
extern PyObject *const_str_plain_print;
static PyObject *const_str_plain_setQuitOnLastWindowClosed;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
static PyObject *const_str_digest_8af48764ad8e59ae5784fe3e7c8bbca2;
extern PyObject *const_str_plain_osx;
extern PyObject *const_str_plain_self;
static PyObject *const_tuple_str_plain_gtk_tuple;
static PyObject *const_str_plain_Bind;
static PyObject *const_str_plain_QtCore;
static PyObject *const_str_plain_loop_gtk3;
static PyObject *const_str_plain_loop_gtk;
static PyObject *const_str_plain_QSocketNotifier;
extern PyObject *const_str_plain_tkinter;
extern PyObject *const_str_plain_run_until_complete;
static PyObject *const_str_plain__eventloop_macos;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain__loop_wx;
extern PyObject *const_str_plain_LooseVersion;
static PyObject *const_str_plain_App;
extern PyObject *const_str_plain_darwin;
extern PyObject *const_tuple_str_plain_LooseVersion_tuple;
extern PyObject *const_str_plain_getsockopt;
static PyObject *const_str_plain_loop_qt4;
static PyObject *const_tuple_c23b9af55e410ee545c4b789cb459991_tuple;
extern PyObject *const_str_plain_coroutine;
extern PyObject *const_str_plain_mainloop;
static PyObject *const_tuple_str_plain_QtCore_tuple;
static PyObject *const_str_digest_5b8c27a5a48ef7b8219cedc05addca03;
extern PyObject *const_str_plain_enable_gui;
extern PyObject *const_str_plain_Tk;
extern PyObject *const_str_digest_4af59da437d2f21ccb08423e5fb98074;
static PyObject *const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple;
extern PyObject *const_tuple_str_plain_self_str_plain_event_tuple;
extern PyObject *const_str_plain_file;
static PyObject *const_str_digest_ac333f5dcf9852c64fda641f168e8125;
extern PyObject *const_str_plain_decorator;
static PyObject *const_tuple_str_plain_Tk_str_plain_READABLE_tuple;
static PyObject *const_tuple_str_plain_get_app_qt4_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_Start = UNSTREAM_STRING_ASCII( &constant_bin[ 77865 ], 5, 1 );
    const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple, 1, const_str_plain_wx ); Py_INCREF( const_str_plain_wx );
    PyTuple_SET_ITEM( const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple, 2, const_str_plain_nope ); Py_INCREF( const_str_plain_nope );
    const_str_plain_poll_interval = UNSTREAM_STRING_ASCII( &constant_bin[ 858027 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple, 3, const_str_plain_poll_interval ); Py_INCREF( const_str_plain_poll_interval );
    PyTuple_SET_ITEM( const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple, 4, const_str_plain_wake ); Py_INCREF( const_str_plain_wake );
    const_str_plain_TimerFrame = UNSTREAM_STRING_ASCII( &constant_bin[ 858040 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple, 5, const_str_plain_TimerFrame ); Py_INCREF( const_str_plain_TimerFrame );
    const_str_plain_IPWxApp = UNSTREAM_STRING_ASCII( &constant_bin[ 858050 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple, 6, const_str_plain_IPWxApp ); Py_INCREF( const_str_plain_IPWxApp );
    PyTuple_SET_ITEM( const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple, 7, const_str_plain_signal ); Py_INCREF( const_str_plain_signal );
    const_tuple_f84687c6b4b11266e386ae3c1c2f70af_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_f84687c6b4b11266e386ae3c1c2f70af_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_f84687c6b4b11266e386ae3c1c2f70af_tuple, 1, const_str_plain_asyncio ); Py_INCREF( const_str_plain_asyncio );
    PyTuple_SET_ITEM( const_tuple_f84687c6b4b11266e386ae3c1c2f70af_tuple, 2, const_str_plain_loop ); Py_INCREF( const_str_plain_loop );
    const_str_plain_close_loop = UNSTREAM_STRING_ASCII( &constant_bin[ 858057 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_f84687c6b4b11266e386ae3c1c2f70af_tuple, 3, const_str_plain_close_loop ); Py_INCREF( const_str_plain_close_loop );
    const_str_plain__gtk = UNSTREAM_STRING_ASCII( &constant_bin[ 858067 ], 4, 1 );
    const_dict_dfc6781eab05c37013adf5c506a72c2c = _PyDict_NewPresized( 6 );
    PyDict_SetItem( const_dict_dfc6781eab05c37013adf5c506a72c2c, const_str_plain_inline, Py_None );
    const_str_plain_nbagg = UNSTREAM_STRING_ASCII( &constant_bin[ 858071 ], 5, 1 );
    PyDict_SetItem( const_dict_dfc6781eab05c37013adf5c506a72c2c, const_str_plain_nbagg, Py_None );
    PyDict_SetItem( const_dict_dfc6781eab05c37013adf5c506a72c2c, const_str_plain_notebook, Py_None );
    const_str_plain_ipympl = UNSTREAM_STRING_ASCII( &constant_bin[ 858076 ], 6, 1 );
    PyDict_SetItem( const_dict_dfc6781eab05c37013adf5c506a72c2c, const_str_plain_ipympl, Py_None );
    PyDict_SetItem( const_dict_dfc6781eab05c37013adf5c506a72c2c, const_str_plain_widget, Py_None );
    PyDict_SetItem( const_dict_dfc6781eab05c37013adf5c506a72c2c, Py_None, Py_None );
    assert( PyDict_Size( const_dict_dfc6781eab05c37013adf5c506a72c2c ) == 6 );
    const_str_plain_loop_asyncio_exit = UNSTREAM_STRING_ASCII( &constant_bin[ 858082 ], 17, 1 );
    const_tuple_ad7d101f73dd0a06e06f2d1d73ce98d9_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_ad7d101f73dd0a06e06f2d1d73ce98d9_tuple, 0, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_ad7d101f73dd0a06e06f2d1d73ce98d9_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_plain_exit_decorator = UNSTREAM_STRING_ASCII( &constant_bin[ 858099 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_ad7d101f73dd0a06e06f2d1d73ce98d9_tuple, 2, const_str_plain_exit_decorator ); Py_INCREF( const_str_plain_exit_decorator );
    const_str_plain_toolkitnames = UNSTREAM_STRING_ASCII( &constant_bin[ 858113 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_ad7d101f73dd0a06e06f2d1d73ce98d9_tuple, 3, const_str_plain_toolkitnames ); Py_INCREF( const_str_plain_toolkitnames );
    const_str_plain_shutdown_asyncgens = UNSTREAM_STRING_ASCII( &constant_bin[ 858125 ], 18, 1 );
    const_str_plain_EVT_TIMER = UNSTREAM_STRING_ASCII( &constant_bin[ 858143 ], 9, 1 );
    const_str_digest_5fc1751031eed6aa9529864cd935023c = UNSTREAM_STRING_ASCII( &constant_bin[ 858152 ], 54, 0 );
    const_str_plain_QTimer = UNSTREAM_STRING_ASCII( &constant_bin[ 858206 ], 6, 1 );
    const_str_plain_deletefilehandler = UNSTREAM_STRING_ASCII( &constant_bin[ 858212 ], 17, 1 );
    const_tuple_str_plain_osx_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_osx_tuple, 0, const_str_plain_osx ); Py_INCREF( const_str_plain_osx );
    const_str_digest_039ffb3d0d7bf635ae665cf0b1b4e3f4 = UNSTREAM_STRING_ASCII( &constant_bin[ 858229 ], 99, 0 );
    const_str_digest_14f4623b35f4ef5cde8d7a15e5f93278 = UNSTREAM_STRING_ASCII( &constant_bin[ 858328 ], 41, 0 );
    const_str_plain_get_app_qt4 = UNSTREAM_STRING_ASCII( &constant_bin[ 858369 ], 11, 1 );
    const_str_plain_loop_cocoa_exit = UNSTREAM_STRING_ASCII( &constant_bin[ 858380 ], 15, 1 );
    const_str_digest_67e3e3acb75f1a3055eb1098afd63fe5 = UNSTREAM_STRING_ASCII( &constant_bin[ 858395 ], 47, 0 );
    const_str_digest_84a6e22bb74dc37ec7c374a46500eff1 = UNSTREAM_STRING_ASCII( &constant_bin[ 858442 ], 30, 0 );
    const_str_digest_0f638692e21be1921314f231ba0d5918 = UNSTREAM_STRING_ASCII( &constant_bin[ 858472 ], 57, 0 );
    const_str_plain_loop_tk_exit = UNSTREAM_STRING_ASCII( &constant_bin[ 858529 ], 12, 1 );
    const_str_plain_loop_gtk3_exit = UNSTREAM_STRING_ASCII( &constant_bin[ 858541 ], 14, 1 );
    const_str_digest_0b2f2d231243d6392b8b730031ee445f = UNSTREAM_STRING_ASCII( &constant_bin[ 858555 ], 24, 0 );
    const_str_plain__loop_qt = UNSTREAM_STRING_ASCII( &constant_bin[ 858579 ], 8, 1 );
    const_str_plain_exit_func = UNSTREAM_STRING_ASCII( &constant_bin[ 858587 ], 9, 1 );
    const_str_digest_2c4c8d69412c28219166b7ab6b355ef6 = UNSTREAM_STRING_ASCII( &constant_bin[ 858596 ], 38, 0 );
    const_str_digest_965aac207295f6aebed813fb543a2cb5 = UNSTREAM_STRING_ASCII( &constant_bin[ 858472 ], 39, 0 );
    const_str_plain_ExitMainLoop = UNSTREAM_STRING_ASCII( &constant_bin[ 858634 ], 12, 1 );
    const_tuple_str_plain_tk_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_tk_tuple, 0, const_str_plain_tk ); Py_INCREF( const_str_plain_tk );
    const_str_plain_OnInit = UNSTREAM_STRING_ASCII( &constant_bin[ 858646 ], 6, 1 );
    const_tuple_str_plain_stream_str_plain_notifier_str_plain_kernel_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_notifier_str_plain_kernel_tuple, 0, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    const_str_plain_notifier = UNSTREAM_STRING_ASCII( &constant_bin[ 858652 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_notifier_str_plain_kernel_tuple, 1, const_str_plain_notifier ); Py_INCREF( const_str_plain_notifier );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_notifier_str_plain_kernel_tuple, 2, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    const_str_digest_e2c7751dfcc49ea3b9e68c3deaf94833 = UNSTREAM_STRING_ASCII( &constant_bin[ 858660 ], 12, 0 );
    const_tuple_str_plain_nope_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_nope_tuple, 0, const_str_plain_nope ); Py_INCREF( const_str_plain_nope );
    const_str_digest_62efe02fdb3e422cbc100b1b6331c18f = UNSTREAM_STRING_ASCII( &constant_bin[ 858672 ], 37, 0 );
    const_tuple_str_plain_stream_str_plain_kernel_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_kernel_tuple, 0, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_kernel_tuple, 1, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    const_str_plain__should_close = UNSTREAM_STRING_ASCII( &constant_bin[ 858709 ], 13, 1 );
    const_str_digest_a71365800d2c40a6537ed3cf18b749d3 = UNSTREAM_STRING_ASCII( &constant_bin[ 858722 ], 104, 0 );
    const_str_plain_on_timer = UNSTREAM_STRING_ASCII( &constant_bin[ 858826 ], 8, 1 );
    const_str_plain_loop_wx = UNSTREAM_STRING_ASCII( &constant_bin[ 858555 ], 7, 1 );
    const_tuple_str_plain_qt_str_plain_qt5_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_qt_str_plain_qt5_tuple, 0, const_str_plain_qt ); Py_INCREF( const_str_plain_qt );
    PyTuple_SET_ITEM( const_tuple_str_plain_qt_str_plain_qt5_tuple, 1, const_str_plain_qt5 ); Py_INCREF( const_str_plain_qt5 );
    const_str_digest_36c984a8ee7390941177662bc489b714 = UNSTREAM_STRING_ASCII( &constant_bin[ 858834 ], 110, 0 );
    const_str_plain_register_integration = UNSTREAM_STRING_ASCII( &constant_bin[ 858472 ], 20, 1 );
    const_str_digest_b9c9692660661ee5f06ab97d344bf8fc = UNSTREAM_STRING_ASCII( &constant_bin[ 858944 ], 92, 0 );
    const_str_digest_cce2d97ed5b6d1fec07023491fdede4e = UNSTREAM_STRING_ASCII( &constant_bin[ 859036 ], 36, 0 );
    const_str_digest_e9c4e2ebaa0b1f02607e723383994479 = UNSTREAM_STRING_ASCII( &constant_bin[ 859072 ], 12, 0 );
    const_str_plain_loop_qt_exit = UNSTREAM_STRING_ASCII( &constant_bin[ 859084 ], 12, 1 );
    const_str_digest_4994da5742005379d5c09c0ac8d5f35c = UNSTREAM_STRING_ASCII( &constant_bin[ 859096 ], 30, 0 );
    const_str_plain__notify_stream_qt = UNSTREAM_STRING_ASCII( &constant_bin[ 859126 ], 17, 1 );
    const_str_digest_21ebb8eee3fecbf4d4ad63ba91522984 = UNSTREAM_STRING_ASCII( &constant_bin[ 859036 ], 27, 0 );
    const_tuple_str_plain_toolkitnames_str_plain_decorator_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_toolkitnames_str_plain_decorator_tuple, 0, const_str_plain_toolkitnames ); Py_INCREF( const_str_plain_toolkitnames );
    PyTuple_SET_ITEM( const_tuple_str_plain_toolkitnames_str_plain_decorator_tuple, 1, const_str_plain_decorator ); Py_INCREF( const_str_plain_decorator );
    const_tuple_str_plain_asyncio_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_asyncio_tuple, 0, const_str_plain_asyncio ); Py_INCREF( const_str_plain_asyncio );
    const_tuple_str_plain_mainloop_str_plain_stop_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_mainloop_str_plain_stop_tuple, 0, const_str_plain_mainloop ); Py_INCREF( const_str_plain_mainloop );
    PyTuple_SET_ITEM( const_tuple_str_plain_mainloop_str_plain_stop_tuple, 1, const_str_plain_stop ); Py_INCREF( const_str_plain_stop );
    const_str_digest_4a347beb0791a92fa68aec31d137b09d = UNSTREAM_STRING_ASCII( &constant_bin[ 859143 ], 52, 0 );
    const_str_plain_pyqt5 = UNSTREAM_STRING_ASCII( &constant_bin[ 859195 ], 5, 1 );
    const_str_plain_handle_int = UNSTREAM_STRING_ASCII( &constant_bin[ 610501 ], 10, 1 );
    const_str_plain_loop_map = UNSTREAM_STRING_ASCII( &constant_bin[ 859200 ], 8, 1 );
    const_str_digest_b2d60b51f38a7258ac0dfb3c34db5b9c = UNSTREAM_STRING_ASCII( &constant_bin[ 859208 ], 39, 0 );
    const_str_digest_f5f0542cf37066ad9d393b3514b1c31a = UNSTREAM_STRING_ASCII( &constant_bin[ 859247 ], 560, 0 );
    const_str_plain_loop_gtk_exit = UNSTREAM_STRING_ASCII( &constant_bin[ 859807 ], 13, 1 );
    const_dict_f22dd43ccca7e02b2f29370abbf82a84 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_f22dd43ccca7e02b2f29370abbf82a84, const_str_plain_limit, const_int_pos_1 );
    assert( PyDict_Size( const_dict_f22dd43ccca7e02b2f29370abbf82a84 ) == 1 );
    const_tuple_str_plain_self_str_plain_TimerFrame_str_plain_wake_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_TimerFrame_str_plain_wake_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_TimerFrame_str_plain_wake_tuple, 1, const_str_plain_TimerFrame ); Py_INCREF( const_str_plain_TimerFrame );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_TimerFrame_str_plain_wake_tuple, 2, const_str_plain_wake ); Py_INCREF( const_str_plain_wake );
    const_tuple_9e7831bee705c2088df553c5ae6dd9ec_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_9e7831bee705c2088df553c5ae6dd9ec_tuple, 0, const_str_plain_etype ); Py_INCREF( const_str_plain_etype );
    PyTuple_SET_ITEM( const_tuple_9e7831bee705c2088df553c5ae6dd9ec_tuple, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_9e7831bee705c2088df553c5ae6dd9ec_tuple, 2, const_str_plain_tb ); Py_INCREF( const_str_plain_tb );
    PyTuple_SET_ITEM( const_tuple_9e7831bee705c2088df553c5ae6dd9ec_tuple, 3, const_str_plain_stop ); Py_INCREF( const_str_plain_stop );
    const_str_plain_real_excepthook = UNSTREAM_STRING_ASCII( &constant_bin[ 859820 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_9e7831bee705c2088df553c5ae6dd9ec_tuple, 4, const_str_plain_real_excepthook ); Py_INCREF( const_str_plain_real_excepthook );
    const_str_digest_7d7c4a4759da263eab1d31f52e3667fa = UNSTREAM_STRING_ASCII( &constant_bin[ 859835 ], 38, 0 );
    const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 1, const_str_plain_asyncio ); Py_INCREF( const_str_plain_asyncio );
    PyTuple_SET_ITEM( const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 2, const_str_plain_loop ); Py_INCREF( const_str_plain_loop );
    const_str_plain_process_stream_events = UNSTREAM_STRING_ASCII( &constant_bin[ 858613 ], 21, 1 );
    PyTuple_SET_ITEM( const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 3, const_str_plain_process_stream_events ); Py_INCREF( const_str_plain_process_stream_events );
    PyTuple_SET_ITEM( const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 4, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    PyTuple_SET_ITEM( const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 5, const_str_plain_fd ); Py_INCREF( const_str_plain_fd );
    PyTuple_SET_ITEM( const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 6, const_str_plain_notifier ); Py_INCREF( const_str_plain_notifier );
    PyTuple_SET_ITEM( const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 7, const_str_plain_error ); Py_INCREF( const_str_plain_error );
    PyTuple_SET_ITEM( const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 8, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_tuple_str_plain_stream_str_plain_a_str_plain_kw_str_plain_app_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_a_str_plain_kw_str_plain_app_tuple, 0, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_a_str_plain_kw_str_plain_app_tuple, 1, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_a_str_plain_kw_str_plain_app_tuple, 2, const_str_plain_kw ); Py_INCREF( const_str_plain_kw );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_a_str_plain_kw_str_plain_app_tuple, 3, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    const_str_plain_gtk_kernel = UNSTREAM_STRING_ASCII( &constant_bin[ 859873 ], 10, 1 );
    const_str_plain__in_event_loop = UNSTREAM_STRING_ASCII( &constant_bin[ 859883 ], 14, 1 );
    const_str_digest_00c3f8c06b14a3722161887dd5baae41 = UNSTREAM_STRING_ASCII( &constant_bin[ 859897 ], 13, 0 );
    const_tuple_str_plain_stop_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_stop_tuple, 0, const_str_plain_stop ); Py_INCREF( const_str_plain_stop );
    const_str_plain_activated = UNSTREAM_STRING_ASCII( &constant_bin[ 859910 ], 9, 1 );
    const_list_str_space_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_space_list, 0, const_str_space ); Py_INCREF( const_str_space );
    const_tuple_str_digest_62efe02fdb3e422cbc100b1b6331c18f_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_62efe02fdb3e422cbc100b1b6331c18f_tuple, 0, const_str_digest_62efe02fdb3e422cbc100b1b6331c18f ); Py_INCREF( const_str_digest_62efe02fdb3e422cbc100b1b6331c18f );
    const_str_plain_createfilehandler = UNSTREAM_STRING_ASCII( &constant_bin[ 859919 ], 17, 1 );
    const_str_plain_setEnabled = UNSTREAM_STRING_ASCII( &constant_bin[ 859936 ], 10, 1 );
    const_str_digest_b8c1173120a40eec7a007296f19f6fe0 = UNSTREAM_STRING_ASCII( &constant_bin[ 859946 ], 42, 0 );
    const_str_plain_QT_API = UNSTREAM_STRING_ASCII( &constant_bin[ 859988 ], 6, 1 );
    const_str_plain__use_appnope = UNSTREAM_STRING_ASCII( &constant_bin[ 859994 ], 12, 1 );
    const_tuple_str_plain_gui_str_plain_kernel_str_plain_e_str_plain_loop_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_gui_str_plain_kernel_str_plain_e_str_plain_loop_tuple, 0, const_str_plain_gui ); Py_INCREF( const_str_plain_gui );
    PyTuple_SET_ITEM( const_tuple_str_plain_gui_str_plain_kernel_str_plain_e_str_plain_loop_tuple, 1, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_str_plain_gui_str_plain_kernel_str_plain_e_str_plain_loop_tuple, 2, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    PyTuple_SET_ITEM( const_tuple_str_plain_gui_str_plain_kernel_str_plain_e_str_plain_loop_tuple, 3, const_str_plain_loop ); Py_INCREF( const_str_plain_loop );
    const_str_digest_ed580d2cf2fea27977dfab5a03530f45 = UNSTREAM_STRING_ASCII( &constant_bin[ 860006 ], 21, 0 );
    const_str_plain_loop_asyncio = UNSTREAM_STRING_ASCII( &constant_bin[ 858082 ], 12, 1 );
    const_str_digest_445cf3bf72607f9eb9f9bebe979fd350 = UNSTREAM_STRING_ASCII( &constant_bin[ 860027 ], 63, 0 );
    const_str_digest_b14ca5a183b199cb4df97fbc116e19b1 = UNSTREAM_STRING_ASCII( &constant_bin[ 860090 ], 48, 0 );
    const_str_digest_41c30847a44603e18da0fe3098acb1cb = UNSTREAM_STRING_ASCII( &constant_bin[ 860138 ], 240, 0 );
    const_tuple_str_plain_kernel_str_plain_GTKEmbed_str_plain_gtk_kernel_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_GTKEmbed_str_plain_gtk_kernel_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_GTKEmbed_str_plain_gtk_kernel_tuple, 1, const_str_plain_GTKEmbed ); Py_INCREF( const_str_plain_GTKEmbed );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_GTKEmbed_str_plain_gtk_kernel_tuple, 2, const_str_plain_gtk_kernel ); Py_INCREF( const_str_plain_gtk_kernel );
    const_tuple_str_plain_gtk3_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_gtk3_tuple, 0, const_str_plain_gtk3 ); Py_INCREF( const_str_plain_gtk3 );
    const_str_plain_setSingleShot = UNSTREAM_STRING_ASCII( &constant_bin[ 860378 ], 13, 1 );
    const_str_digest_95d07a8a573117c8a04622ad0ece989a = UNSTREAM_STRING_ASCII( &constant_bin[ 860391 ], 36, 0 );
    const_tuple_str_plain_stream_str_plain_loop_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_loop_tuple, 0, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_str_plain_loop_tuple, 1, const_str_plain_loop ); Py_INCREF( const_str_plain_loop );
    const_str_plain_Exit = UNSTREAM_STRING_ASCII( &constant_bin[ 858634 ], 4, 1 );
    const_str_plain_loop_wx_exit = UNSTREAM_STRING_ASCII( &constant_bin[ 860427 ], 12, 1 );
    const_tuple_str_plain_kernel_str_plain_wx_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_wx_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_wx_tuple, 1, const_str_plain_wx ); Py_INCREF( const_str_plain_wx );
    const_tuple_str_plain_GTKEmbed_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_GTKEmbed_tuple, 0, const_str_plain_GTKEmbed ); Py_INCREF( const_str_plain_GTKEmbed );
    const_str_plain_loop_tk = UNSTREAM_STRING_ASCII( &constant_bin[ 858529 ], 7, 1 );
    const_str_digest_2d4ee65694e57b933f5856d0c282944c = UNSTREAM_STRING_ASCII( &constant_bin[ 860439 ], 46, 0 );
    const_tuple_str_plain_qt4_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_qt4_tuple, 0, const_str_plain_qt4 ); Py_INCREF( const_str_plain_qt4 );
    const_tuple_str_plain_kernel_str_plain_stop_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_stop_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_stop_tuple, 1, const_str_plain_stop ); Py_INCREF( const_str_plain_stop );
    const_str_digest_c14f651ff533bc3e31bd18bcef523a7c = UNSTREAM_STRING_ASCII( &constant_bin[ 860485 ], 21, 0 );
    const_tuple_21580f539e3be0854d6691bc0ed6dad4_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_21580f539e3be0854d6691bc0ed6dad4_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_21580f539e3be0854d6691bc0ed6dad4_tuple, 1, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_21580f539e3be0854d6691bc0ed6dad4_tuple, 2, const_str_plain_wx ); Py_INCREF( const_str_plain_wx );
    PyTuple_SET_ITEM( const_tuple_21580f539e3be0854d6691bc0ed6dad4_tuple, 3, const_str_plain_poll_interval ); Py_INCREF( const_str_plain_poll_interval );
    const_tuple_str_plain_kernel_str_plain_get_app_qt4_str_plain_s_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_get_app_qt4_str_plain_s_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_get_app_qt4_str_plain_s_tuple, 1, const_str_plain_get_app_qt4 ); Py_INCREF( const_str_plain_get_app_qt4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_kernel_str_plain_get_app_qt4_str_plain_s_tuple, 2, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    const_str_digest_ab961728ca2578f1a92223bd29998ce8 = UNSTREAM_STRING_ASCII( &constant_bin[ 860506 ], 29, 0 );
    const_str_digest_68e19db4deb3cfd9bcff9bbba59a16fc = UNSTREAM_STRING_ASCII( &constant_bin[ 860535 ], 37, 0 );
    const_str_digest_dc4f4e7db006731ca43e14f0afdcb9c0 = UNSTREAM_STRING_ASCII( &constant_bin[ 860572 ], 49, 0 );
    const_tuple_str_plain_exit_func_str_plain_func_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_exit_func_str_plain_func_tuple, 0, const_str_plain_exit_func ); Py_INCREF( const_str_plain_exit_func );
    PyTuple_SET_ITEM( const_tuple_str_plain_exit_func_str_plain_func_tuple, 1, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple, 1, const_str_plain_Tk ); Py_INCREF( const_str_plain_Tk );
    const_str_plain_READABLE = UNSTREAM_STRING_ASCII( &constant_bin[ 860621 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple, 2, const_str_plain_READABLE ); Py_INCREF( const_str_plain_READABLE );
    PyTuple_SET_ITEM( const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple, 3, const_str_plain_process_stream_events ); Py_INCREF( const_str_plain_process_stream_events );
    PyTuple_SET_ITEM( const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple, 4, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    PyTuple_SET_ITEM( const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple, 5, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    PyTuple_SET_ITEM( const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple, 6, const_str_plain_notifier ); Py_INCREF( const_str_plain_notifier );
    const_str_plain_loop_qt5 = UNSTREAM_STRING_ASCII( &constant_bin[ 860629 ], 8, 1 );
    const_str_plain_loop_cocoa = UNSTREAM_STRING_ASCII( &constant_bin[ 858380 ], 10, 1 );
    const_str_plain_Read = UNSTREAM_STRING_ASCII( &constant_bin[ 1290 ], 4, 1 );
    const_str_digest_ad8cb7ff5767f8253cefcb2821b0e89d = UNSTREAM_STRING_ASCII( &constant_bin[ 860637 ], 50, 0 );
    const_str_digest_4fc3b1699fcd0924231a3ffcab71de3c = UNSTREAM_STRING_ASCII( &constant_bin[ 860687 ], 22, 0 );
    const_str_digest_4b7ea5a266a558058bbe98690987425b = UNSTREAM_STRING_ASCII( &constant_bin[ 860709 ], 49, 0 );
    const_str_plain_MainLoop = UNSTREAM_STRING_ASCII( &constant_bin[ 858638 ], 8, 1 );
    const_str_digest_858e5195e53addafe6d0eb7f2e812e18 = UNSTREAM_STRING_ASCII( &constant_bin[ 860758 ], 23, 0 );
    const_dict_3cfeb0dac4449d6bb047ee2d6ba92616 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_3cfeb0dac4449d6bb047ee2d6ba92616, const_str_plain_redirect, Py_False );
    assert( PyDict_Size( const_dict_3cfeb0dac4449d6bb047ee2d6ba92616 ) == 1 );
    const_str_digest_73c970d07f1cc7f805ecc5b6fc3991d6 = UNSTREAM_STRING_ASCII( &constant_bin[ 860781 ], 31, 0 );
    const_str_plain_setQuitOnLastWindowClosed = UNSTREAM_STRING_ASCII( &constant_bin[ 860812 ], 25, 1 );
    const_str_digest_8af48764ad8e59ae5784fe3e7c8bbca2 = UNSTREAM_STRING_ASCII( &constant_bin[ 860837 ], 43, 0 );
    const_tuple_str_plain_gtk_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_gtk_tuple, 0, const_str_plain_gtk ); Py_INCREF( const_str_plain_gtk );
    const_str_plain_Bind = UNSTREAM_STRING_ASCII( &constant_bin[ 860880 ], 4, 1 );
    const_str_plain_QtCore = UNSTREAM_STRING_ASCII( &constant_bin[ 74868 ], 6, 1 );
    const_str_plain_loop_gtk3 = UNSTREAM_STRING_ASCII( &constant_bin[ 858541 ], 9, 1 );
    const_str_plain_loop_gtk = UNSTREAM_STRING_ASCII( &constant_bin[ 858063 ], 8, 1 );
    const_str_plain_QSocketNotifier = UNSTREAM_STRING_ASCII( &constant_bin[ 860884 ], 15, 1 );
    const_str_plain__eventloop_macos = UNSTREAM_STRING_ASCII( &constant_bin[ 849821 ], 16, 1 );
    const_str_plain__loop_wx = UNSTREAM_STRING_ASCII( &constant_bin[ 860899 ], 8, 1 );
    const_str_plain_App = UNSTREAM_STRING_ASCII( &constant_bin[ 22687 ], 3, 1 );
    const_str_plain_loop_qt4 = UNSTREAM_STRING_ASCII( &constant_bin[ 860907 ], 8, 1 );
    const_tuple_c23b9af55e410ee545c4b789cb459991_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_c23b9af55e410ee545c4b789cb459991_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_c23b9af55e410ee545c4b789cb459991_tuple, 1, const_str_plain_mainloop ); Py_INCREF( const_str_plain_mainloop );
    PyTuple_SET_ITEM( const_tuple_c23b9af55e410ee545c4b789cb459991_tuple, 2, const_str_plain_stop ); Py_INCREF( const_str_plain_stop );
    PyTuple_SET_ITEM( const_tuple_c23b9af55e410ee545c4b789cb459991_tuple, 3, const_str_plain_real_excepthook ); Py_INCREF( const_str_plain_real_excepthook );
    PyTuple_SET_ITEM( const_tuple_c23b9af55e410ee545c4b789cb459991_tuple, 4, const_str_plain_handle_int ); Py_INCREF( const_str_plain_handle_int );
    PyTuple_SET_ITEM( const_tuple_c23b9af55e410ee545c4b789cb459991_tuple, 5, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    const_tuple_str_plain_QtCore_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_QtCore_tuple, 0, const_str_plain_QtCore ); Py_INCREF( const_str_plain_QtCore );
    const_str_digest_5b8c27a5a48ef7b8219cedc05addca03 = UNSTREAM_STRING_ASCII( &constant_bin[ 860915 ], 35, 0 );
    const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple, 0, const_str_plain_kernel ); Py_INCREF( const_str_plain_kernel );
    PyTuple_SET_ITEM( const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple, 1, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    PyTuple_SET_ITEM( const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple, 2, const_str_plain_QtCore ); Py_INCREF( const_str_plain_QtCore );
    PyTuple_SET_ITEM( const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple, 3, const_str_plain_process_stream_events ); Py_INCREF( const_str_plain_process_stream_events );
    PyTuple_SET_ITEM( const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple, 4, const_str_plain_fd ); Py_INCREF( const_str_plain_fd );
    PyTuple_SET_ITEM( const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple, 5, const_str_plain_notifier ); Py_INCREF( const_str_plain_notifier );
    PyTuple_SET_ITEM( const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple, 6, const_str_plain_timer ); Py_INCREF( const_str_plain_timer );
    const_str_digest_ac333f5dcf9852c64fda641f168e8125 = UNSTREAM_STRING_ASCII( &constant_bin[ 860950 ], 240, 0 );
    const_tuple_str_plain_Tk_str_plain_READABLE_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Tk_str_plain_READABLE_tuple, 0, const_str_plain_Tk ); Py_INCREF( const_str_plain_Tk );
    PyTuple_SET_ITEM( const_tuple_str_plain_Tk_str_plain_READABLE_tuple, 1, const_str_plain_READABLE ); Py_INCREF( const_str_plain_READABLE );
    const_tuple_str_plain_get_app_qt4_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_app_qt4_tuple, 0, const_str_plain_get_app_qt4 ); Py_INCREF( const_str_plain_get_app_qt4 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_ipykernel$eventloops( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_44d9758b33450b795557bcb678629425;
static PyCodeObject *codeobj_fecfb080a26871ce28075a2556bd2706;
static PyCodeObject *codeobj_97f3f828fe76263c7c5cde3ed71c27e7;
static PyCodeObject *codeobj_9de51be6fe5a74046c8ebc9781101da5;
static PyCodeObject *codeobj_16a6b6ad2c6767fd94f2acb7613c92db;
static PyCodeObject *codeobj_c64f104a35c606358749dca5cc999162;
static PyCodeObject *codeobj_c570518f8534bcd077635f7d0a8d980e;
static PyCodeObject *codeobj_0eaf4d8ea64e3113f2bc3765e8af0dca;
static PyCodeObject *codeobj_0d616b9dc042b49f480d025cc2d7c3e3;
static PyCodeObject *codeobj_eb9114483babb5cba293d09910cc0e3d;
static PyCodeObject *codeobj_e06efd7b945f078458bf864669e9f639;
static PyCodeObject *codeobj_7e19846afe5e9d2916c5dbc889506ede;
static PyCodeObject *codeobj_d538afe2a4994bbddb928a1283d3e3fb;
static PyCodeObject *codeobj_6c3782dfac5fe6f28012060808633334;
static PyCodeObject *codeobj_4d51db9e71dac7e5d3872e993acf1eff;
static PyCodeObject *codeobj_2eda2a1510ed33ba9a8d5b7587967ab6;
static PyCodeObject *codeobj_232eec1d13930f531fd7f1fd0bc255a3;
static PyCodeObject *codeobj_1d13d22c75e3ccb389eca1f15497fc26;
static PyCodeObject *codeobj_18411978282a4eead2ffa6a85c3c3ec8;
static PyCodeObject *codeobj_a6966745d50d40f1362e86d5fcb0ccd9;
static PyCodeObject *codeobj_ba8f45f98f9cdde044b5032bf0075e62;
static PyCodeObject *codeobj_bedfe9919dfee0533970a7bfb93739af;
static PyCodeObject *codeobj_e58cd038bd2e1fa086681096028429f2;
static PyCodeObject *codeobj_32c99576c1a7129c4aba701ed0853e09;
static PyCodeObject *codeobj_000e41d9ce9829957831b5b836d0dc9f;
static PyCodeObject *codeobj_194db13e896a67e6d6de4d8d750d049a;
static PyCodeObject *codeobj_089c561f81035276a66d83f080abe4a3;
static PyCodeObject *codeobj_bcf0b356f68cb06ad5cf5300eb83ba82;
static PyCodeObject *codeobj_e0045759633cb60da5cf039dffd820f4;
static PyCodeObject *codeobj_9ef5d2ec27d9f4286940ad398de57237;
static PyCodeObject *codeobj_ec60cab8fde565006ea3662ae47508de;
static PyCodeObject *codeobj_eb819a4b239d753061a6f7a6c8cd87b6;
static PyCodeObject *codeobj_c819a73e87849bc6bee0f826b7d6670c;
static PyCodeObject *codeobj_203dcfe25c3932f8754558dd2fb4869a;
static PyCodeObject *codeobj_1b61d167e0cdc8739dabddcbaf924ee0;
static PyCodeObject *codeobj_985b5f9f962a775ddfc017b24c9d5d9d;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_858e5195e53addafe6d0eb7f2e812e18 );
    codeobj_44d9758b33450b795557bcb678629425 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 82, const_tuple_str_plain_kernel_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fecfb080a26871ce28075a2556bd2706 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_ab961728ca2578f1a92223bd29998ce8, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_97f3f828fe76263c7c5cde3ed71c27e7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_IPWxApp, 189, const_tuple_str_plain___class___tuple, 0, 0, CO_OPTIMIZED | CO_NOFREE );
    codeobj_9de51be6fe5a74046c8ebc9781101da5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_OnInit, 190, const_tuple_str_plain_self_str_plain_TimerFrame_str_plain_wake_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_16a6b6ad2c6767fd94f2acb7613c92db = MAKE_CODEOBJ( module_filename_obj, const_str_plain_TimerFrame, 175, const_tuple_str_plain___class___tuple, 0, 0, CO_OPTIMIZED | CO_NOFREE );
    codeobj_c64f104a35c606358749dca5cc999162 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 176, const_tuple_21580f539e3be0854d6691bc0ed6dad4_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_c570518f8534bcd077635f7d0a8d980e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__loop_qt, 98, const_tuple_str_plain_app_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0eaf4d8ea64e3113f2bc3765e8af0dca = MAKE_CODEOBJ( module_filename_obj, const_str_plain__loop_wx, 139, const_tuple_str_plain_app_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0d616b9dc042b49f480d025cc2d7c3e3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__notify_stream_qt, 26, const_tuple_e5fa0e01f64f9ae9afc8086547c1740e_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_eb9114483babb5cba293d09910cc0e3d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__use_appnope, 18, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e06efd7b945f078458bf864669e9f639 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_close_loop, 373, const_tuple_str_plain_loop_tuple, 0, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_7e19846afe5e9d2916c5dbc889506ede = MAKE_CODEOBJ( module_filename_obj, const_str_plain_decorator, 78, const_tuple_ad7d101f73dd0a06e06f2d1d73ce98d9_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_d538afe2a4994bbddb928a1283d3e3fb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_enable_gui, 388, const_tuple_str_plain_gui_str_plain_kernel_str_plain_e_str_plain_loop_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6c3782dfac5fe6f28012060808633334 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_exit_decorator, 84, const_tuple_str_plain_exit_func_str_plain_func_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_4d51db9e71dac7e5d3872e993acf1eff = MAKE_CODEOBJ( module_filename_obj, const_str_plain_handle_int, 288, const_tuple_9e7831bee705c2088df553c5ae6dd9ec_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_2eda2a1510ed33ba9a8d5b7587967ab6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_asyncio, 325, const_tuple_4f5d5c880eda297047b91b342fe55fb7_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_232eec1d13930f531fd7f1fd0bc255a3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_asyncio_exit, 367, const_tuple_f84687c6b4b11266e386ae3c1c2f70af_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1d13d22c75e3ccb389eca1f15497fc26 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_cocoa, 280, const_tuple_c23b9af55e410ee545c4b789cb459991_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_18411978282a4eead2ffa6a85c3c3ec8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_cocoa_exit, 319, const_tuple_str_plain_kernel_str_plain_stop_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a6966745d50d40f1362e86d5fcb0ccd9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_gtk, 250, const_tuple_str_plain_kernel_str_plain_GTKEmbed_str_plain_gtk_kernel_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ba8f45f98f9cdde044b5032bf0075e62 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_gtk3, 265, const_tuple_str_plain_kernel_str_plain_GTKEmbed_str_plain_gtk_kernel_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bedfe9919dfee0533970a7bfb93739af = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_gtk3_exit, 275, const_tuple_str_plain_kernel_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e58cd038bd2e1fa086681096028429f2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_gtk_exit, 260, const_tuple_str_plain_kernel_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_32c99576c1a7129c4aba701ed0853e09 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_qt4, 110, const_tuple_str_plain_kernel_str_plain_get_app_qt4_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_000e41d9ce9829957831b5b836d0dc9f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_qt5, 125, const_tuple_str_plain_kernel_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_194db13e896a67e6d6de4d8d750d049a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_qt_exit, 133, const_tuple_str_plain_kernel_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_089c561f81035276a66d83f080abe4a3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_tk, 219, const_tuple_c451ae55125aad47d5db765cb75ca7c0_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bcf0b356f68cb06ad5cf5300eb83ba82 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_tk_exit, 245, const_tuple_str_plain_kernel_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e0045759633cb60da5cf039dffd820f4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_wx, 151, const_tuple_f559f0b9a052803e5bf68ef4a3b47e29_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9ef5d2ec27d9f4286940ad398de57237 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_loop_wx_exit, 213, const_tuple_str_plain_kernel_str_plain_wx_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ec60cab8fde565006ea3662ae47508de = MAKE_CODEOBJ( module_filename_obj, const_str_plain_on_timer, 184, const_tuple_str_plain_self_str_plain_event_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_eb819a4b239d753061a6f7a6c8cd87b6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_process_stream_events, 225, const_tuple_str_plain_stream_str_plain_a_str_plain_kw_str_plain_app_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
    codeobj_c819a73e87849bc6bee0f826b7d6670c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_process_stream_events, 341, const_tuple_str_plain_stream_str_plain_loop_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_203dcfe25c3932f8754558dd2fb4869a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_process_stream_events, 30, const_tuple_str_plain_stream_str_plain_notifier_str_plain_kernel_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_1b61d167e0cdc8739dabddcbaf924ee0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_register_integration, 65, const_tuple_str_plain_toolkitnames_str_plain_decorator_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_NOFREE );
    codeobj_985b5f9f962a775ddfc017b24c9d5d9d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wake, 166, const_tuple_str_plain_stream_str_plain_kernel_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS );
}

// The module function declarations.
static PyObject *ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop$$$genobj_1_close_loop_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_10_loop_wx_exit(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_11_loop_tk(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_11_loop_tk$$$function_1_process_stream_events(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_12_loop_tk_exit(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_13_loop_gtk(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_14_loop_gtk_exit(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_15_loop_gtk3(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_16_loop_gtk3_exit(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_17_loop_cocoa(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_17_loop_cocoa$$$function_1_handle_int(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_18_loop_cocoa_exit(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_19_loop_asyncio(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_19_loop_asyncio$$$function_1_process_stream_events(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_1__use_appnope(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_20_loop_asyncio_exit(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_21_enable_gui( PyObject *defaults );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_2__notify_stream_qt(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_2__notify_stream_qt$$$function_1_process_stream_events(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_2_exit_decorator(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_4__loop_qt(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_5_loop_qt4(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_6_loop_qt5(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_7_loop_qt_exit(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_8__loop_wx(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_1_wake(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_2___init__(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_3_on_timer(  );


static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_4_OnInit(  );


// The module function definitions.
static PyObject *impl_ipykernel$eventloops$$$function_1__use_appnope( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_eb9114483babb5cba293d09910cc0e3d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_eb9114483babb5cba293d09910cc0e3d = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_eb9114483babb5cba293d09910cc0e3d, codeobj_eb9114483babb5cba293d09910cc0e3d, module_ipykernel$eventloops, 0 );
    frame_eb9114483babb5cba293d09910cc0e3d = cache_frame_eb9114483babb5cba293d09910cc0e3d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eb9114483babb5cba293d09910cc0e3d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eb9114483babb5cba293d09910cc0e3d ) == 2 ); // Frame stack

    // Framed code:
    {
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 23;

            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_platform );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_darwin;
        tmp_and_left_value_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_and_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_and_left_value_1 );

            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        Py_DECREF( tmp_and_left_value_1 );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_V );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_V );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "V" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 23;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_platform );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_platform );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "platform" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 23;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        frame_eb9114483babb5cba293d09910cc0e3d->m_frame.f_lineno = 23;
        tmp_subscribed_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_mac_ver );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        frame_eb9114483babb5cba293d09910cc0e3d->m_frame.f_lineno = 23;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_compexpr_left_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_V );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_V );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_compexpr_left_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "V" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 23;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_4;
        frame_eb9114483babb5cba293d09910cc0e3d->m_frame.f_lineno = 23;
        tmp_compexpr_right_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_digest_fe66bcb374d8283cea65f90daae59bd4_tuple, 0 ) );

        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_2 );

            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = RICH_COMPARE_GTE_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_and_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_return_value = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_return_value = tmp_and_left_value_1;
        and_end_1:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb9114483babb5cba293d09910cc0e3d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb9114483babb5cba293d09910cc0e3d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb9114483babb5cba293d09910cc0e3d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eb9114483babb5cba293d09910cc0e3d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eb9114483babb5cba293d09910cc0e3d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eb9114483babb5cba293d09910cc0e3d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eb9114483babb5cba293d09910cc0e3d,
        type_description_1
    );


    // Release cached frame.
    if ( frame_eb9114483babb5cba293d09910cc0e3d == cache_frame_eb9114483babb5cba293d09910cc0e3d )
    {
        Py_DECREF( frame_eb9114483babb5cba293d09910cc0e3d );
    }
    cache_frame_eb9114483babb5cba293d09910cc0e3d = NULL;

    assertFrameObject( frame_eb9114483babb5cba293d09910cc0e3d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_1__use_appnope );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_2__notify_stream_qt( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_kernel = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_stream = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *var_QtCore = NULL;
    PyObject *var_process_stream_events = NULL;
    PyObject *var_fd = NULL;
    struct Nuitka_CellObject *var_notifier = PyCell_EMPTY();
    PyObject *var_timer = NULL;
    struct Nuitka_FrameObject *frame_0d616b9dc042b49f480d025cc2d7c3e3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0d616b9dc042b49f480d025cc2d7c3e3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0d616b9dc042b49f480d025cc2d7c3e3, codeobj_0d616b9dc042b49f480d025cc2d7c3e3, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0d616b9dc042b49f480d025cc2d7c3e3 = cache_frame_0d616b9dc042b49f480d025cc2d7c3e3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0d616b9dc042b49f480d025cc2d7c3e3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0d616b9dc042b49f480d025cc2d7c3e3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_4994da5742005379d5c09c0ac8d5f35c;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_QtCore_tuple;
        tmp_level_name_1 = const_int_0;
        frame_0d616b9dc042b49f480d025cc2d7c3e3->m_frame.f_lineno = 28;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_QtCore );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        assert( var_QtCore == NULL );
        var_QtCore = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_ipykernel$eventloops$$$function_2__notify_stream_qt$$$function_1_process_stream_events(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = par_kernel;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] = var_notifier;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[2] = par_stream;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[2] );


        assert( var_process_stream_events == NULL );
        var_process_stream_events = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( PyCell_GET( par_stream ) );
        tmp_source_name_1 = PyCell_GET( par_stream );
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_getsockopt );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_zmq );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zmq );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zmq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_FD );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 40;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        frame_0d616b9dc042b49f480d025cc2d7c3e3->m_frame.f_lineno = 40;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        assert( var_fd == NULL );
        var_fd = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( var_QtCore );
        tmp_source_name_3 = var_QtCore;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_QSocketNotifier );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_fd );
        tmp_args_element_name_2 = var_fd;
        CHECK_OBJECT( var_QtCore );
        tmp_source_name_5 = var_QtCore;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_QSocketNotifier );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 41;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_Read );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 41;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_kernel ) );
        tmp_source_name_6 = PyCell_GET( par_kernel );
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_app );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 41;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        frame_0d616b9dc042b49f480d025cc2d7c3e3->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_notifier ) == NULL );
        PyCell_SET( var_notifier, tmp_assign_source_4 );

    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_7;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( PyCell_GET( var_notifier ) );
        tmp_source_name_7 = PyCell_GET( var_notifier );
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_activated );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_process_stream_events );
        tmp_args_element_name_5 = var_process_stream_events;
        frame_0d616b9dc042b49f480d025cc2d7c3e3->m_frame.f_lineno = 42;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_connect, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_8;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( var_QtCore );
        tmp_source_name_8 = var_QtCore;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_QTimer );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_kernel ) );
        tmp_source_name_9 = PyCell_GET( par_kernel );
        tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_app );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 50;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        frame_0d616b9dc042b49f480d025cc2d7c3e3->m_frame.f_lineno = 50;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        assert( var_timer == NULL );
        var_timer = tmp_assign_source_5;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( var_timer );
        tmp_called_instance_2 = var_timer;
        frame_0d616b9dc042b49f480d025cc2d7c3e3->m_frame.f_lineno = 51;
        tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_setSingleShot, &PyTuple_GET_ITEM( const_tuple_true_tuple, 0 ) );

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_10;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_7;
        CHECK_OBJECT( var_timer );
        tmp_source_name_10 = var_timer;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_timeout );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_process_stream_events );
        tmp_args_element_name_7 = var_process_stream_events;
        frame_0d616b9dc042b49f480d025cc2d7c3e3->m_frame.f_lineno = 52;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_connect, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_call_result_4;
        CHECK_OBJECT( var_timer );
        tmp_called_instance_4 = var_timer;
        frame_0d616b9dc042b49f480d025cc2d7c3e3->m_frame.f_lineno = 53;
        tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_start, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "ccoooco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0d616b9dc042b49f480d025cc2d7c3e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0d616b9dc042b49f480d025cc2d7c3e3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0d616b9dc042b49f480d025cc2d7c3e3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0d616b9dc042b49f480d025cc2d7c3e3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0d616b9dc042b49f480d025cc2d7c3e3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0d616b9dc042b49f480d025cc2d7c3e3,
        type_description_1,
        par_kernel,
        par_stream,
        var_QtCore,
        var_process_stream_events,
        var_fd,
        var_notifier,
        var_timer
    );


    // Release cached frame.
    if ( frame_0d616b9dc042b49f480d025cc2d7c3e3 == cache_frame_0d616b9dc042b49f480d025cc2d7c3e3 )
    {
        Py_DECREF( frame_0d616b9dc042b49f480d025cc2d7c3e3 );
    }
    cache_frame_0d616b9dc042b49f480d025cc2d7c3e3 = NULL;

    assertFrameObject( frame_0d616b9dc042b49f480d025cc2d7c3e3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_2__notify_stream_qt );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    CHECK_OBJECT( (PyObject *)var_QtCore );
    Py_DECREF( var_QtCore );
    var_QtCore = NULL;

    CHECK_OBJECT( (PyObject *)var_process_stream_events );
    Py_DECREF( var_process_stream_events );
    var_process_stream_events = NULL;

    CHECK_OBJECT( (PyObject *)var_fd );
    Py_DECREF( var_fd );
    var_fd = NULL;

    CHECK_OBJECT( (PyObject *)var_notifier );
    Py_DECREF( var_notifier );
    var_notifier = NULL;

    CHECK_OBJECT( (PyObject *)var_timer );
    Py_DECREF( var_timer );
    var_timer = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    Py_XDECREF( var_QtCore );
    var_QtCore = NULL;

    Py_XDECREF( var_process_stream_events );
    var_process_stream_events = NULL;

    Py_XDECREF( var_fd );
    var_fd = NULL;

    CHECK_OBJECT( (PyObject *)var_notifier );
    Py_DECREF( var_notifier );
    var_notifier = NULL;

    Py_XDECREF( var_timer );
    var_timer = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_2__notify_stream_qt );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_2__notify_stream_qt$$$function_1_process_stream_events( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_203dcfe25c3932f8754558dd2fb4869a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_203dcfe25c3932f8754558dd2fb4869a = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_203dcfe25c3932f8754558dd2fb4869a, codeobj_203dcfe25c3932f8754558dd2fb4869a, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_203dcfe25c3932f8754558dd2fb4869a = cache_frame_203dcfe25c3932f8754558dd2fb4869a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_203dcfe25c3932f8754558dd2fb4869a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_203dcfe25c3932f8754558dd2fb4869a ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        int tmp_truth_name_1;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "stream" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;
            type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[2] );
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_flush );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_f22dd43ccca7e02b2f29370abbf82a84 );
        frame_203dcfe25c3932f8754558dd2fb4869a->m_frame.f_lineno = 36;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 36;
            type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_2;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "notifier" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 37;
                type_description_1 = "ccc";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = PyCell_GET( self->m_closure[1] );
            frame_203dcfe25c3932f8754558dd2fb4869a->m_frame.f_lineno = 37;
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_setEnabled, &PyTuple_GET_ITEM( const_tuple_false_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ccc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_3;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kernel" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 38;
                type_description_1 = "ccc";
                goto frame_exception_exit_1;
            }

            tmp_source_name_2 = PyCell_GET( self->m_closure[0] );
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_app );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "ccc";
                goto frame_exception_exit_1;
            }
            frame_203dcfe25c3932f8754558dd2fb4869a->m_frame.f_lineno = 38;
            tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_quit );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "ccc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_203dcfe25c3932f8754558dd2fb4869a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_203dcfe25c3932f8754558dd2fb4869a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_203dcfe25c3932f8754558dd2fb4869a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_203dcfe25c3932f8754558dd2fb4869a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_203dcfe25c3932f8754558dd2fb4869a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_203dcfe25c3932f8754558dd2fb4869a,
        type_description_1,
        self->m_closure[2],
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_203dcfe25c3932f8754558dd2fb4869a == cache_frame_203dcfe25c3932f8754558dd2fb4869a )
    {
        Py_DECREF( frame_203dcfe25c3932f8754558dd2fb4869a );
    }
    cache_frame_203dcfe25c3932f8754558dd2fb4869a = NULL;

    assertFrameObject( frame_203dcfe25c3932f8754558dd2fb4869a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_2__notify_stream_qt$$$function_1_process_stream_events );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_3_register_integration( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_toolkitnames = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_decorator = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_toolkitnames;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_decorator == NULL );
        var_decorator = tmp_assign_source_1;
    }
    // Tried code:
    CHECK_OBJECT( var_decorator );
    tmp_return_value = var_decorator;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_3_register_integration );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_toolkitnames );
    Py_DECREF( par_toolkitnames );
    par_toolkitnames = NULL;

    CHECK_OBJECT( (PyObject *)var_decorator );
    Py_DECREF( var_decorator );
    var_decorator = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_3_register_integration );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_func = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_name = NULL;
    PyObject *var_exit_decorator = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_7e19846afe5e9d2916c5dbc889506ede;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_7e19846afe5e9d2916c5dbc889506ede = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7e19846afe5e9d2916c5dbc889506ede, codeobj_7e19846afe5e9d2916c5dbc889506ede, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7e19846afe5e9d2916c5dbc889506ede = cache_frame_7e19846afe5e9d2916c5dbc889506ede;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7e19846afe5e9d2916c5dbc889506ede );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7e19846afe5e9d2916c5dbc889506ede ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "toolkitnames" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "cooc";
            goto frame_exception_exit_1;
        }

        tmp_iter_arg_1 = PyCell_GET( self->m_closure[0] );
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "cooc";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "cooc";
                exception_lineno = 79;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_name;
            var_name = tmp_assign_source_3;
            Py_INCREF( var_name );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_ass_subscript_1;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_ass_subvalue_1 = PyCell_GET( par_func );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_map );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_map );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "loop_map" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;
            type_description_1 = "cooc";
            goto try_except_handler_2;
        }

        tmp_ass_subscribed_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_name );
        tmp_ass_subscript_1 = var_name;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "cooc";
            goto try_except_handler_2;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 79;
        type_description_1 = "cooc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_1_lambda(  );



        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_assattr_target_1 = PyCell_GET( par_func );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_exit_hook, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "cooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_2_exit_decorator(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] = par_func;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] );


        assert( var_exit_decorator == NULL );
        var_exit_decorator = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( var_exit_decorator );
        tmp_assattr_name_2 = var_exit_decorator;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_assattr_target_2 = PyCell_GET( par_func );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_exit, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "cooc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7e19846afe5e9d2916c5dbc889506ede );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7e19846afe5e9d2916c5dbc889506ede );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7e19846afe5e9d2916c5dbc889506ede, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7e19846afe5e9d2916c5dbc889506ede->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7e19846afe5e9d2916c5dbc889506ede, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7e19846afe5e9d2916c5dbc889506ede,
        type_description_1,
        par_func,
        var_name,
        var_exit_decorator,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_7e19846afe5e9d2916c5dbc889506ede == cache_frame_7e19846afe5e9d2916c5dbc889506ede )
    {
        Py_DECREF( frame_7e19846afe5e9d2916c5dbc889506ede );
    }
    cache_frame_7e19846afe5e9d2916c5dbc889506ede = NULL;

    assertFrameObject( frame_7e19846afe5e9d2916c5dbc889506ede );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( PyCell_GET( par_func ) );
    tmp_return_value = PyCell_GET( par_func );
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    CHECK_OBJECT( (PyObject *)var_exit_decorator );
    Py_DECREF( var_exit_decorator );
    var_exit_decorator = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_exit_decorator );
    var_exit_decorator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_1_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_2_exit_decorator( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_exit_func = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6c3782dfac5fe6f28012060808633334;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_6c3782dfac5fe6f28012060808633334 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6c3782dfac5fe6f28012060808633334, codeobj_6c3782dfac5fe6f28012060808633334, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *) );
    frame_6c3782dfac5fe6f28012060808633334 = cache_frame_6c3782dfac5fe6f28012060808633334;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6c3782dfac5fe6f28012060808633334 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6c3782dfac5fe6f28012060808633334 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_exit_func );
        tmp_assattr_name_1 = par_exit_func;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_1 = PyCell_GET( self->m_closure[0] );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_exit_hook, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6c3782dfac5fe6f28012060808633334 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6c3782dfac5fe6f28012060808633334 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6c3782dfac5fe6f28012060808633334, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6c3782dfac5fe6f28012060808633334->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6c3782dfac5fe6f28012060808633334, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6c3782dfac5fe6f28012060808633334,
        type_description_1,
        par_exit_func,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_6c3782dfac5fe6f28012060808633334 == cache_frame_6c3782dfac5fe6f28012060808633334 )
    {
        Py_DECREF( frame_6c3782dfac5fe6f28012060808633334 );
    }
    cache_frame_6c3782dfac5fe6f28012060808633334 = NULL;

    assertFrameObject( frame_6c3782dfac5fe6f28012060808633334 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_exit_func );
    tmp_return_value = par_exit_func;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_2_exit_decorator );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_exit_func );
    Py_DECREF( par_exit_func );
    par_exit_func = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_exit_func );
    Py_DECREF( par_exit_func );
    par_exit_func = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_2_exit_decorator );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_4__loop_qt( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_app = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c570518f8534bcd077635f7d0a8d980e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c570518f8534bcd077635f7d0a8d980e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c570518f8534bcd077635f7d0a8d980e, codeobj_c570518f8534bcd077635f7d0a8d980e, module_ipykernel$eventloops, sizeof(void *) );
    frame_c570518f8534bcd077635f7d0a8d980e = cache_frame_c570518f8534bcd077635f7d0a8d980e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c570518f8534bcd077635f7d0a8d980e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c570518f8534bcd077635f7d0a8d980e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_True;
        CHECK_OBJECT( par_app );
        tmp_assattr_target_1 = par_app;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__in_event_loop, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_app );
        tmp_called_instance_1 = par_app;
        frame_c570518f8534bcd077635f7d0a8d980e->m_frame.f_lineno = 106;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_exec_ );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = Py_False;
        CHECK_OBJECT( par_app );
        tmp_assattr_target_2 = par_app;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__in_event_loop, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c570518f8534bcd077635f7d0a8d980e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c570518f8534bcd077635f7d0a8d980e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c570518f8534bcd077635f7d0a8d980e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c570518f8534bcd077635f7d0a8d980e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c570518f8534bcd077635f7d0a8d980e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c570518f8534bcd077635f7d0a8d980e,
        type_description_1,
        par_app
    );


    // Release cached frame.
    if ( frame_c570518f8534bcd077635f7d0a8d980e == cache_frame_c570518f8534bcd077635f7d0a8d980e )
    {
        Py_DECREF( frame_c570518f8534bcd077635f7d0a8d980e );
    }
    cache_frame_c570518f8534bcd077635f7d0a8d980e = NULL;

    assertFrameObject( frame_c570518f8534bcd077635f7d0a8d980e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_4__loop_qt );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_app );
    Py_DECREF( par_app );
    par_app = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_app );
    Py_DECREF( par_app );
    par_app = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_4__loop_qt );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_5_loop_qt4( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *var_get_app_qt4 = NULL;
    PyObject *var_s = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_32c99576c1a7129c4aba701ed0853e09;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_32c99576c1a7129c4aba701ed0853e09 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_32c99576c1a7129c4aba701ed0853e09, codeobj_32c99576c1a7129c4aba701ed0853e09, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_32c99576c1a7129c4aba701ed0853e09 = cache_frame_32c99576c1a7129c4aba701ed0853e09;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_32c99576c1a7129c4aba701ed0853e09 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_32c99576c1a7129c4aba701ed0853e09 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_4fc3b1699fcd0924231a3ffcab71de3c;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_get_app_qt4_tuple;
        tmp_level_name_1 = const_int_0;
        frame_32c99576c1a7129c4aba701ed0853e09->m_frame.f_lineno = 114;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_get_app_qt4 );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_get_app_qt4 == NULL );
        var_get_app_qt4 = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_get_app_qt4 );
        tmp_called_name_1 = var_get_app_qt4;
        tmp_call_arg_element_1 = LIST_COPY( const_list_str_space_list );
        frame_32c99576c1a7129c4aba701ed0853e09->m_frame.f_lineno = 116;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_call_arg_element_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_kernel );
        tmp_assattr_target_1 = par_kernel;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_app, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_1 = par_kernel;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_app );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_32c99576c1a7129c4aba701ed0853e09->m_frame.f_lineno = 117;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_setQuitOnLastWindowClosed, &PyTuple_GET_ITEM( const_tuple_false_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_2 = par_kernel;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_shell_streams );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                exception_lineno = 119;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_s;
            var_s = tmp_assign_source_4;
            Py_INCREF( var_s );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain__notify_stream_qt );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__notify_stream_qt );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_notify_stream_qt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( par_kernel );
        tmp_args_element_name_1 = par_kernel;
        CHECK_OBJECT( var_s );
        tmp_args_element_name_2 = var_s;
        frame_32c99576c1a7129c4aba701ed0853e09->m_frame.f_lineno = 120;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 119;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain__loop_qt );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__loop_qt );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_loop_qt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_2;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_3 = par_kernel;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_32c99576c1a7129c4aba701ed0853e09->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32c99576c1a7129c4aba701ed0853e09 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32c99576c1a7129c4aba701ed0853e09 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_32c99576c1a7129c4aba701ed0853e09, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_32c99576c1a7129c4aba701ed0853e09->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_32c99576c1a7129c4aba701ed0853e09, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_32c99576c1a7129c4aba701ed0853e09,
        type_description_1,
        par_kernel,
        var_get_app_qt4,
        var_s
    );


    // Release cached frame.
    if ( frame_32c99576c1a7129c4aba701ed0853e09 == cache_frame_32c99576c1a7129c4aba701ed0853e09 )
    {
        Py_DECREF( frame_32c99576c1a7129c4aba701ed0853e09 );
    }
    cache_frame_32c99576c1a7129c4aba701ed0853e09 = NULL;

    assertFrameObject( frame_32c99576c1a7129c4aba701ed0853e09 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_5_loop_qt4 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_get_app_qt4 );
    Py_DECREF( var_get_app_qt4 );
    var_get_app_qt4 = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_get_app_qt4 );
    var_get_app_qt4 = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_5_loop_qt4 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_6_loop_qt5( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_000e41d9ce9829957831b5b836d0dc9f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_000e41d9ce9829957831b5b836d0dc9f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_000e41d9ce9829957831b5b836d0dc9f, codeobj_000e41d9ce9829957831b5b836d0dc9f, module_ipykernel$eventloops, sizeof(void *) );
    frame_000e41d9ce9829957831b5b836d0dc9f = cache_frame_000e41d9ce9829957831b5b836d0dc9f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_000e41d9ce9829957831b5b836d0dc9f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_000e41d9ce9829957831b5b836d0dc9f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_ass_subscript_1;
        tmp_ass_subvalue_1 = const_str_plain_pyqt5;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_environ );
        if ( tmp_ass_subscribed_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_ass_subscript_1 = const_str_plain_QT_API;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subscribed_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_qt4 );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_qt4 );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "loop_qt4" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_kernel );
        tmp_args_element_name_1 = par_kernel;
        frame_000e41d9ce9829957831b5b836d0dc9f->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_000e41d9ce9829957831b5b836d0dc9f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_000e41d9ce9829957831b5b836d0dc9f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_000e41d9ce9829957831b5b836d0dc9f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_000e41d9ce9829957831b5b836d0dc9f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_000e41d9ce9829957831b5b836d0dc9f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_000e41d9ce9829957831b5b836d0dc9f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_000e41d9ce9829957831b5b836d0dc9f,
        type_description_1,
        par_kernel
    );


    // Release cached frame.
    if ( frame_000e41d9ce9829957831b5b836d0dc9f == cache_frame_000e41d9ce9829957831b5b836d0dc9f )
    {
        Py_DECREF( frame_000e41d9ce9829957831b5b836d0dc9f );
    }
    cache_frame_000e41d9ce9829957831b5b836d0dc9f = NULL;

    assertFrameObject( frame_000e41d9ce9829957831b5b836d0dc9f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_6_loop_qt5 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_6_loop_qt5 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_7_loop_qt_exit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_194db13e896a67e6d6de4d8d750d049a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_194db13e896a67e6d6de4d8d750d049a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_194db13e896a67e6d6de4d8d750d049a, codeobj_194db13e896a67e6d6de4d8d750d049a, module_ipykernel$eventloops, sizeof(void *) );
    frame_194db13e896a67e6d6de4d8d750d049a = cache_frame_194db13e896a67e6d6de4d8d750d049a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_194db13e896a67e6d6de4d8d750d049a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_194db13e896a67e6d6de4d8d750d049a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_1 = par_kernel;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_app );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_194db13e896a67e6d6de4d8d750d049a->m_frame.f_lineno = 136;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_exit );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_194db13e896a67e6d6de4d8d750d049a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_194db13e896a67e6d6de4d8d750d049a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_194db13e896a67e6d6de4d8d750d049a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_194db13e896a67e6d6de4d8d750d049a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_194db13e896a67e6d6de4d8d750d049a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_194db13e896a67e6d6de4d8d750d049a,
        type_description_1,
        par_kernel
    );


    // Release cached frame.
    if ( frame_194db13e896a67e6d6de4d8d750d049a == cache_frame_194db13e896a67e6d6de4d8d750d049a )
    {
        Py_DECREF( frame_194db13e896a67e6d6de4d8d750d049a );
    }
    cache_frame_194db13e896a67e6d6de4d8d750d049a = NULL;

    assertFrameObject( frame_194db13e896a67e6d6de4d8d750d049a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_7_loop_qt_exit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_7_loop_qt_exit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_8__loop_wx( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_app = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0eaf4d8ea64e3113f2bc3765e8af0dca;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0eaf4d8ea64e3113f2bc3765e8af0dca = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0eaf4d8ea64e3113f2bc3765e8af0dca, codeobj_0eaf4d8ea64e3113f2bc3765e8af0dca, module_ipykernel$eventloops, sizeof(void *) );
    frame_0eaf4d8ea64e3113f2bc3765e8af0dca = cache_frame_0eaf4d8ea64e3113f2bc3765e8af0dca;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0eaf4d8ea64e3113f2bc3765e8af0dca );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0eaf4d8ea64e3113f2bc3765e8af0dca ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_True;
        CHECK_OBJECT( par_app );
        tmp_assattr_target_1 = par_app;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__in_event_loop, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_app );
        tmp_called_instance_1 = par_app;
        frame_0eaf4d8ea64e3113f2bc3765e8af0dca->m_frame.f_lineno = 147;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_MainLoop );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = Py_False;
        CHECK_OBJECT( par_app );
        tmp_assattr_target_2 = par_app;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__in_event_loop, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0eaf4d8ea64e3113f2bc3765e8af0dca );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0eaf4d8ea64e3113f2bc3765e8af0dca );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0eaf4d8ea64e3113f2bc3765e8af0dca, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0eaf4d8ea64e3113f2bc3765e8af0dca->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0eaf4d8ea64e3113f2bc3765e8af0dca, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0eaf4d8ea64e3113f2bc3765e8af0dca,
        type_description_1,
        par_app
    );


    // Release cached frame.
    if ( frame_0eaf4d8ea64e3113f2bc3765e8af0dca == cache_frame_0eaf4d8ea64e3113f2bc3765e8af0dca )
    {
        Py_DECREF( frame_0eaf4d8ea64e3113f2bc3765e8af0dca );
    }
    cache_frame_0eaf4d8ea64e3113f2bc3765e8af0dca = NULL;

    assertFrameObject( frame_0eaf4d8ea64e3113f2bc3765e8af0dca );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_8__loop_wx );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_app );
    Py_DECREF( par_app );
    par_app = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_app );
    Py_DECREF( par_app );
    par_app = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_8__loop_wx );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_9_loop_wx( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_kernel = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *var_wx = PyCell_EMPTY();
    PyObject *var_nope = NULL;
    struct Nuitka_CellObject *var_poll_interval = PyCell_EMPTY();
    struct Nuitka_CellObject *var_wake = PyCell_EMPTY();
    struct Nuitka_CellObject *var_TimerFrame = PyCell_EMPTY();
    PyObject *var_IPWxApp = NULL;
    PyObject *var_signal = NULL;
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    struct Nuitka_FrameObject *frame_e0045759633cb60da5cf039dffd820f4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    bool tmp_result;
    PyObject *locals_ipykernel$eventloops$$$function_9_loop_wx_175 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_16a6b6ad2c6767fd94f2acb7613c92db_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_16a6b6ad2c6767fd94f2acb7613c92db_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *locals_ipykernel$eventloops$$$function_9_loop_wx_189 = NULL;
    struct Nuitka_FrameObject *frame_97f3f828fe76263c7c5cde3ed71c27e7_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_97f3f828fe76263c7c5cde3ed71c27e7_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_e0045759633cb60da5cf039dffd820f4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e0045759633cb60da5cf039dffd820f4, codeobj_e0045759633cb60da5cf039dffd820f4, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e0045759633cb60da5cf039dffd820f4 = cache_frame_e0045759633cb60da5cf039dffd820f4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e0045759633cb60da5cf039dffd820f4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e0045759633cb60da5cf039dffd820f4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_wx;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 155;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_wx ) == NULL );
        PyCell_SET( var_wx, tmp_assign_source_1 );

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain__use_appnope );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__use_appnope );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_use_appnope" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 157;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 157;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 157;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( PyCell_GET( par_kernel ) );
        tmp_source_name_1 = PyCell_GET( par_kernel );
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__darwin_app_nap );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 157;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_import_name_from_1;
            PyObject *tmp_name_name_2;
            PyObject *tmp_globals_name_2;
            PyObject *tmp_locals_name_2;
            PyObject *tmp_fromlist_name_2;
            PyObject *tmp_level_name_2;
            tmp_name_name_2 = const_str_plain_appnope;
            tmp_globals_name_2 = (PyObject *)moduledict_ipykernel$eventloops;
            tmp_locals_name_2 = Py_None;
            tmp_fromlist_name_2 = const_tuple_str_plain_nope_tuple;
            tmp_level_name_2 = const_int_0;
            frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 160;
            tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
            if ( tmp_import_name_from_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_nope );
            Py_DECREF( tmp_import_name_from_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }
            assert( var_nope == NULL );
            var_nope = tmp_assign_source_2;
        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( var_nope );
            tmp_called_name_2 = var_nope;
            frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 161;
            tmp_call_result_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_int_arg_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        tmp_left_name_1 = const_int_pos_1000;
        CHECK_OBJECT( PyCell_GET( par_kernel ) );
        tmp_source_name_2 = PyCell_GET( par_kernel );
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__poll_interval );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_int_arg_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_int_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = PyNumber_Int( tmp_int_arg_1 );
        Py_DECREF( tmp_int_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_poll_interval ) == NULL );
        PyCell_SET( var_poll_interval, tmp_assign_source_3 );

    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_1_wake(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] = par_kernel;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] );


        assert( PyCell_GET( var_wake ) == NULL );
        PyCell_SET( var_wake, tmp_assign_source_4 );

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( PyCell_GET( var_wx ) );
        tmp_source_name_3 = PyCell_GET( var_wx );
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_Frame );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        tmp_assign_source_5 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_5, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_6 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        tmp_condition_result_3 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_8 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_8;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_2;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_4 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_4, const_str_plain___prepare__ );
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_5;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_5 = tmp_class_creation_1__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "ccocccoo";
                goto try_except_handler_2;
            }
            tmp_tuple_element_2 = const_str_plain_TimerFrame;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 175;
            tmp_assign_source_9 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "ccocccoo";
                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_9;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_6 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_6, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "ccocccoo";
                goto try_except_handler_2;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_7;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 175;
                    type_description_1 = "ccocccoo";
                    goto try_except_handler_2;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_7 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_7 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_7 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 175;
                    type_description_1 = "ccocccoo";
                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 175;
                    type_description_1 = "ccocccoo";
                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 175;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ccocccoo";
                goto try_except_handler_2;
            }
            branch_no_4:;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_10;
            tmp_assign_source_10 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_10;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_11;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_ipykernel$eventloops$$$function_9_loop_wx_175 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_f0b65ebf32c38a05199c60f79a951b9a;
        tmp_res = PyObject_SetItem( locals_ipykernel$eventloops$$$function_9_loop_wx_175, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_digest_21ebb8eee3fecbf4d4ad63ba91522984;
        tmp_res = PyObject_SetItem( locals_ipykernel$eventloops$$$function_9_loop_wx_175, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ccocccoo";
            goto try_except_handler_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_16a6b6ad2c6767fd94f2acb7613c92db_2, codeobj_16a6b6ad2c6767fd94f2acb7613c92db, module_ipykernel$eventloops, sizeof(void *) );
        frame_16a6b6ad2c6767fd94f2acb7613c92db_2 = cache_frame_16a6b6ad2c6767fd94f2acb7613c92db_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_16a6b6ad2c6767fd94f2acb7613c92db_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_16a6b6ad2c6767fd94f2acb7613c92db_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_2___init__(  );

        ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = var_poll_interval;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] = var_wx;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] );


        tmp_res = PyObject_SetItem( locals_ipykernel$eventloops$$$function_9_loop_wx_175, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_3_on_timer(  );



        tmp_res = PyObject_SetItem( locals_ipykernel$eventloops$$$function_9_loop_wx_175, const_str_plain_on_timer, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_16a6b6ad2c6767fd94f2acb7613c92db_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_16a6b6ad2c6767fd94f2acb7613c92db_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_16a6b6ad2c6767fd94f2acb7613c92db_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_16a6b6ad2c6767fd94f2acb7613c92db_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_16a6b6ad2c6767fd94f2acb7613c92db_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_16a6b6ad2c6767fd94f2acb7613c92db_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_16a6b6ad2c6767fd94f2acb7613c92db_2 == cache_frame_16a6b6ad2c6767fd94f2acb7613c92db_2 )
        {
            Py_DECREF( frame_16a6b6ad2c6767fd94f2acb7613c92db_2 );
        }
        cache_frame_16a6b6ad2c6767fd94f2acb7613c92db_2 = NULL;

        assertFrameObject( frame_16a6b6ad2c6767fd94f2acb7613c92db_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ccocccoo";
        goto try_except_handler_4;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "ccocccoo";
                goto try_except_handler_4;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_ipykernel$eventloops$$$function_9_loop_wx_175, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "ccocccoo";
                goto try_except_handler_4;
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_4 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_TimerFrame;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_ipykernel$eventloops$$$function_9_loop_wx_175;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 175;
            tmp_assign_source_12 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "ccocccoo";
                goto try_except_handler_4;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_12;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_11 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_11 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_ipykernel$eventloops$$$function_9_loop_wx_175 );
        locals_ipykernel$eventloops$$$function_9_loop_wx_175 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_ipykernel$eventloops$$$function_9_loop_wx_175 );
        locals_ipykernel$eventloops$$$function_9_loop_wx_175 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx );
        return NULL;
        outline_exception_1:;
        exception_lineno = 175;
        goto try_except_handler_2;
        outline_result_1:;
        assert( PyCell_GET( var_TimerFrame ) == NULL );
        PyCell_SET( var_TimerFrame, tmp_assign_source_11 );

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_source_name_8;
        if ( PyCell_GET( var_wx ) == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "wx" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }

        tmp_source_name_8 = PyCell_GET( var_wx );
        tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_App );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        tmp_assign_source_13 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_13, 0, tmp_tuple_element_5 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_14 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_14;
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_4;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        tmp_condition_result_9 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_16 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_16;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_5;
        }
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_9 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___prepare__ );
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_10;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_10 = tmp_class_creation_2__metaclass;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___prepare__ );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "ccocccoo";
                goto try_except_handler_5;
            }
            tmp_tuple_element_6 = const_str_plain_IPWxApp;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 189;
            tmp_assign_source_17 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "ccocccoo";
                goto try_except_handler_5;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_17;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_11 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_11, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "ccocccoo";
                goto try_except_handler_5;
            }
            tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_12;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;
                    type_description_1 = "ccocccoo";
                    goto try_except_handler_5;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_12 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_12 == NULL) );
                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_12 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 189;
                    type_description_1 = "ccocccoo";
                    goto try_except_handler_5;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_7 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;
                    type_description_1 = "ccocccoo";
                    goto try_except_handler_5;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 189;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ccocccoo";
                goto try_except_handler_5;
            }
            branch_no_8:;
        }
        goto branch_end_7;
        branch_no_7:;
        {
            PyObject *tmp_assign_source_18;
            tmp_assign_source_18 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_18;
        }
        branch_end_7:;
    }
    {
        PyObject *tmp_assign_source_19;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_ipykernel$eventloops$$$function_9_loop_wx_189 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_f0b65ebf32c38a05199c60f79a951b9a;
        tmp_res = PyObject_SetItem( locals_ipykernel$eventloops$$$function_9_loop_wx_189, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_digest_0b2f2d231243d6392b8b730031ee445f;
        tmp_res = PyObject_SetItem( locals_ipykernel$eventloops$$$function_9_loop_wx_189, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ccocccoo";
            goto try_except_handler_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_97f3f828fe76263c7c5cde3ed71c27e7_3, codeobj_97f3f828fe76263c7c5cde3ed71c27e7, module_ipykernel$eventloops, sizeof(void *) );
        frame_97f3f828fe76263c7c5cde3ed71c27e7_3 = cache_frame_97f3f828fe76263c7c5cde3ed71c27e7_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_97f3f828fe76263c7c5cde3ed71c27e7_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_97f3f828fe76263c7c5cde3ed71c27e7_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_4_OnInit(  );

        ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = var_TimerFrame;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] = var_wake;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] );


        tmp_res = PyObject_SetItem( locals_ipykernel$eventloops$$$function_9_loop_wx_189, const_str_plain_OnInit, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_97f3f828fe76263c7c5cde3ed71c27e7_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_97f3f828fe76263c7c5cde3ed71c27e7_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_97f3f828fe76263c7c5cde3ed71c27e7_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_97f3f828fe76263c7c5cde3ed71c27e7_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_97f3f828fe76263c7c5cde3ed71c27e7_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_97f3f828fe76263c7c5cde3ed71c27e7_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_97f3f828fe76263c7c5cde3ed71c27e7_3 == cache_frame_97f3f828fe76263c7c5cde3ed71c27e7_3 )
        {
            Py_DECREF( frame_97f3f828fe76263c7c5cde3ed71c27e7_3 );
        }
        cache_frame_97f3f828fe76263c7c5cde3ed71c27e7_3 = NULL;

        assertFrameObject( frame_97f3f828fe76263c7c5cde3ed71c27e7_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;
        type_description_1 = "ccocccoo";
        goto try_except_handler_7;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "ccocccoo";
                goto try_except_handler_7;
            }
            tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_ipykernel$eventloops$$$function_9_loop_wx_189, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "ccocccoo";
                goto try_except_handler_7;
            }
            branch_no_9:;
        }
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_6 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_8 = const_str_plain_IPWxApp;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_8 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
            tmp_tuple_element_8 = locals_ipykernel$eventloops$$$function_9_loop_wx_189;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 189;
            tmp_assign_source_20 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "ccocccoo";
                goto try_except_handler_7;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_20;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_19 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_19 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx );
        return NULL;
        // Return handler code:
        try_return_handler_7:;
        Py_DECREF( locals_ipykernel$eventloops$$$function_9_loop_wx_189 );
        locals_ipykernel$eventloops$$$function_9_loop_wx_189 = NULL;
        goto try_return_handler_6;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_ipykernel$eventloops$$$function_9_loop_wx_189 );
        locals_ipykernel$eventloops$$$function_9_loop_wx_189 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_6;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx );
        return NULL;
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx );
        return NULL;
        outline_exception_2:;
        exception_lineno = 189;
        goto try_except_handler_5;
        outline_result_2:;
        assert( var_IPWxApp == NULL );
        var_IPWxApp = tmp_assign_source_19;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    {
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_operand_name_3;
        int tmp_and_left_truth_2;
        PyObject *tmp_and_left_value_2;
        PyObject *tmp_and_right_value_2;
        PyObject *tmp_getattr_target_3;
        PyObject *tmp_getattr_attr_3;
        PyObject *tmp_getattr_default_3;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_source_name_13;
        PyObject *tmp_source_name_14;
        if ( PyCell_GET( par_kernel ) == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kernel" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 198;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }

        tmp_getattr_target_3 = PyCell_GET( par_kernel );
        tmp_getattr_attr_3 = const_str_plain_app;
        tmp_getattr_default_3 = Py_None;
        tmp_and_left_value_2 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
        if ( tmp_and_left_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_truth_2 = CHECK_IF_TRUE( tmp_and_left_value_2 );
        if ( tmp_and_left_truth_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_and_left_value_2 );

            exception_lineno = 199;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        Py_DECREF( tmp_and_left_value_2 );
        if ( PyCell_GET( par_kernel ) == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kernel" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 199;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_13 = PyCell_GET( par_kernel );
        tmp_isinstance_inst_1 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_app );
        if ( tmp_isinstance_inst_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( var_wx ) == NULL )
        {
            Py_DECREF( tmp_isinstance_inst_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "wx" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 199;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = PyCell_GET( var_wx );
        tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_App );
        if ( tmp_isinstance_cls_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_isinstance_inst_1 );

            exception_lineno = 199;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_inst_1 );
        Py_DECREF( tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_and_right_value_2 );
        tmp_operand_name_3 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_operand_name_3 = tmp_and_left_value_2;
        and_end_2:;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        Py_DECREF( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_14 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_name_7;
            PyObject *tmp_kw_name_5;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( var_IPWxApp );
            tmp_called_name_7 = var_IPWxApp;
            tmp_kw_name_5 = PyDict_Copy( const_dict_3cfeb0dac4449d6bb047ee2d6ba92616 );
            frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 201;
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_7, tmp_kw_name_5 );
            Py_DECREF( tmp_kw_name_5 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 201;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }
            if ( PyCell_GET( par_kernel ) == NULL )
            {
                Py_DECREF( tmp_assattr_name_1 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kernel" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 201;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }

            tmp_assattr_target_1 = PyCell_GET( par_kernel );
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_app, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 201;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_10:;
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_signal;
        tmp_globals_name_3 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 206;
        tmp_assign_source_21 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 206;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        assert( var_signal == NULL );
        var_signal = tmp_assign_source_21;
    }
    {
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_operand_name_4;
        PyObject *tmp_called_name_8;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_15;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_16;
        tmp_called_name_8 = LOOKUP_BUILTIN( const_str_plain_callable );
        assert( tmp_called_name_8 != NULL );
        CHECK_OBJECT( var_signal );
        tmp_source_name_15 = var_signal;
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_getsignal );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_signal );
        tmp_source_name_16 = var_signal;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_SIGINT );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );

            exception_lineno = 207;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 207;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 207;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_operand_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_operand_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
        Py_DECREF( tmp_operand_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_15 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_called_name_10;
            PyObject *tmp_source_name_17;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_source_name_18;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_source_name_19;
            CHECK_OBJECT( var_signal );
            tmp_source_name_17 = var_signal;
            tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_signal );
            if ( tmp_called_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 208;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_signal );
            tmp_source_name_18 = var_signal;
            tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_SIGINT );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_10 );

                exception_lineno = 208;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_signal );
            tmp_source_name_19 = var_signal;
            tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_default_int_handler );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_10 );
                Py_DECREF( tmp_args_element_name_3 );

                exception_lineno = 208;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }
            frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 208;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, call_args );
            }

            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 208;
                type_description_1 = "ccocccoo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_11:;
    }
    {
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_20;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain__loop_wx );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__loop_wx );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_loop_wx" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 210;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_2;
        if ( PyCell_GET( par_kernel ) == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kernel" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 210;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_20 = PyCell_GET( par_kernel );
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_app );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        frame_e0045759633cb60da5cf039dffd820f4->m_frame.f_lineno = 210;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;
            type_description_1 = "ccocccoo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e0045759633cb60da5cf039dffd820f4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_3;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e0045759633cb60da5cf039dffd820f4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e0045759633cb60da5cf039dffd820f4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e0045759633cb60da5cf039dffd820f4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e0045759633cb60da5cf039dffd820f4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e0045759633cb60da5cf039dffd820f4,
        type_description_1,
        par_kernel,
        var_wx,
        var_nope,
        var_poll_interval,
        var_wake,
        var_TimerFrame,
        var_IPWxApp,
        var_signal
    );


    // Release cached frame.
    if ( frame_e0045759633cb60da5cf039dffd820f4 == cache_frame_e0045759633cb60da5cf039dffd820f4 )
    {
        Py_DECREF( frame_e0045759633cb60da5cf039dffd820f4 );
    }
    cache_frame_e0045759633cb60da5cf039dffd820f4 = NULL;

    assertFrameObject( frame_e0045759633cb60da5cf039dffd820f4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_wx );
    Py_DECREF( var_wx );
    var_wx = NULL;

    Py_XDECREF( var_nope );
    var_nope = NULL;

    CHECK_OBJECT( (PyObject *)var_poll_interval );
    Py_DECREF( var_poll_interval );
    var_poll_interval = NULL;

    CHECK_OBJECT( (PyObject *)var_wake );
    Py_DECREF( var_wake );
    var_wake = NULL;

    CHECK_OBJECT( (PyObject *)var_TimerFrame );
    Py_DECREF( var_TimerFrame );
    var_TimerFrame = NULL;

    CHECK_OBJECT( (PyObject *)var_IPWxApp );
    Py_DECREF( var_IPWxApp );
    var_IPWxApp = NULL;

    CHECK_OBJECT( (PyObject *)var_signal );
    Py_DECREF( var_signal );
    var_signal = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_wx );
    Py_DECREF( var_wx );
    var_wx = NULL;

    Py_XDECREF( var_nope );
    var_nope = NULL;

    CHECK_OBJECT( (PyObject *)var_poll_interval );
    Py_DECREF( var_poll_interval );
    var_poll_interval = NULL;

    CHECK_OBJECT( (PyObject *)var_wake );
    Py_DECREF( var_wake );
    var_wake = NULL;

    CHECK_OBJECT( (PyObject *)var_TimerFrame );
    Py_DECREF( var_TimerFrame );
    var_TimerFrame = NULL;

    Py_XDECREF( var_IPWxApp );
    var_IPWxApp = NULL;

    Py_XDECREF( var_signal );
    var_signal = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx );
    return NULL;

function_exception_exit:
    Py_XDECREF( locals_ipykernel$eventloops$$$function_9_loop_wx_189 );

    Py_XDECREF( locals_ipykernel$eventloops$$$function_9_loop_wx_175 );
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.
    Py_XDECREF( locals_ipykernel$eventloops$$$function_9_loop_wx_189 );

    Py_XDECREF( locals_ipykernel$eventloops$$$function_9_loop_wx_175 );


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_9_loop_wx$$$function_1_wake( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_stream = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_985b5f9f962a775ddfc017b24c9d5d9d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_985b5f9f962a775ddfc017b24c9d5d9d = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_985b5f9f962a775ddfc017b24c9d5d9d, codeobj_985b5f9f962a775ddfc017b24c9d5d9d, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *) );
    frame_985b5f9f962a775ddfc017b24c9d5d9d = cache_frame_985b5f9f962a775ddfc017b24c9d5d9d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_985b5f9f962a775ddfc017b24c9d5d9d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_985b5f9f962a775ddfc017b24c9d5d9d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kernel" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 168;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[0] );
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_shell_streams );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oc";
                exception_lineno = 168;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_stream;
            var_stream = tmp_assign_source_3;
            Py_INCREF( var_stream );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_stream );
        tmp_source_name_2 = var_stream;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_flush );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "oc";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_f22dd43ccca7e02b2f29370abbf82a84 );
        frame_985b5f9f962a775ddfc017b24c9d5d9d->m_frame.f_lineno = 169;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "oc";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 169;
            type_description_1 = "oc";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_2;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kernel" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 170;
                type_description_1 = "oc";
                goto try_except_handler_2;
            }

            tmp_source_name_3 = PyCell_GET( self->m_closure[0] );
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 170;
                type_description_1 = "oc";
                goto try_except_handler_2;
            }
            frame_985b5f9f962a775ddfc017b24c9d5d9d->m_frame.f_lineno = 170;
            tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_ExitMainLoop );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 170;
                type_description_1 = "oc";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_2;
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 168;
        type_description_1 = "oc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_985b5f9f962a775ddfc017b24c9d5d9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_985b5f9f962a775ddfc017b24c9d5d9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_985b5f9f962a775ddfc017b24c9d5d9d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_985b5f9f962a775ddfc017b24c9d5d9d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_985b5f9f962a775ddfc017b24c9d5d9d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_985b5f9f962a775ddfc017b24c9d5d9d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_985b5f9f962a775ddfc017b24c9d5d9d,
        type_description_1,
        var_stream,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_985b5f9f962a775ddfc017b24c9d5d9d == cache_frame_985b5f9f962a775ddfc017b24c9d5d9d )
    {
        Py_DECREF( frame_985b5f9f962a775ddfc017b24c9d5d9d );
    }
    cache_frame_985b5f9f962a775ddfc017b24c9d5d9d = NULL;

    assertFrameObject( frame_985b5f9f962a775ddfc017b24c9d5d9d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx$$$function_1_wake );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_stream );
    var_stream = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx$$$function_1_wake );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_9_loop_wx$$$function_2___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_func = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_c64f104a35c606358749dca5cc999162;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_c64f104a35c606358749dca5cc999162 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c64f104a35c606358749dca5cc999162, codeobj_c64f104a35c606358749dca5cc999162, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c64f104a35c606358749dca5cc999162 = cache_frame_c64f104a35c606358749dca5cc999162;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c64f104a35c606358749dca5cc999162 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c64f104a35c606358749dca5cc999162 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "wx" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 177;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[1] );
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Frame );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        tmp_args_element_name_2 = Py_None;
        tmp_args_element_name_3 = const_int_neg_1;
        frame_c64f104a35c606358749dca5cc999162->m_frame.f_lineno = 177;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_1, const_str_plain___init__, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_assattr_target_1;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "wx" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 178;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = PyCell_GET( self->m_closure[1] );
        CHECK_OBJECT( par_self );
        tmp_args_element_name_4 = par_self;
        frame_c64f104a35c606358749dca5cc999162->m_frame.f_lineno = 178;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assattr_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_Timer, call_args );
        }

        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_timer, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_timer );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_Start );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "poll_interval" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 180;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = PyCell_GET( self->m_closure[0] );
        frame_c64f104a35c606358749dca5cc999162->m_frame.f_lineno = 180;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_Bind );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "wx" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = PyCell_GET( self->m_closure[1] );
        tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_EVT_TIMER );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 181;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_args_element_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_on_timer );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_6 );

            exception_lineno = 181;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        frame_c64f104a35c606358749dca5cc999162->m_frame.f_lineno = 181;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_6 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_func );
        tmp_assattr_name_2 = par_func;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_func, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c64f104a35c606358749dca5cc999162 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c64f104a35c606358749dca5cc999162 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c64f104a35c606358749dca5cc999162, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c64f104a35c606358749dca5cc999162->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c64f104a35c606358749dca5cc999162, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c64f104a35c606358749dca5cc999162,
        type_description_1,
        par_self,
        par_func,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_c64f104a35c606358749dca5cc999162 == cache_frame_c64f104a35c606358749dca5cc999162 )
    {
        Py_DECREF( frame_c64f104a35c606358749dca5cc999162 );
    }
    cache_frame_c64f104a35c606358749dca5cc999162 = NULL;

    assertFrameObject( frame_c64f104a35c606358749dca5cc999162 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx$$$function_2___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx$$$function_2___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_9_loop_wx$$$function_3_on_timer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_event = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_ec60cab8fde565006ea3662ae47508de;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ec60cab8fde565006ea3662ae47508de = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ec60cab8fde565006ea3662ae47508de, codeobj_ec60cab8fde565006ea3662ae47508de, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *) );
    frame_ec60cab8fde565006ea3662ae47508de = cache_frame_ec60cab8fde565006ea3662ae47508de;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ec60cab8fde565006ea3662ae47508de );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ec60cab8fde565006ea3662ae47508de ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_ec60cab8fde565006ea3662ae47508de->m_frame.f_lineno = 185;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_func );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec60cab8fde565006ea3662ae47508de );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec60cab8fde565006ea3662ae47508de );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ec60cab8fde565006ea3662ae47508de, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ec60cab8fde565006ea3662ae47508de->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ec60cab8fde565006ea3662ae47508de, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ec60cab8fde565006ea3662ae47508de,
        type_description_1,
        par_self,
        par_event
    );


    // Release cached frame.
    if ( frame_ec60cab8fde565006ea3662ae47508de == cache_frame_ec60cab8fde565006ea3662ae47508de )
    {
        Py_DECREF( frame_ec60cab8fde565006ea3662ae47508de );
    }
    cache_frame_ec60cab8fde565006ea3662ae47508de = NULL;

    assertFrameObject( frame_ec60cab8fde565006ea3662ae47508de );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx$$$function_3_on_timer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx$$$function_3_on_timer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_9_loop_wx$$$function_4_OnInit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9de51be6fe5a74046c8ebc9781101da5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_9de51be6fe5a74046c8ebc9781101da5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9de51be6fe5a74046c8ebc9781101da5, codeobj_9de51be6fe5a74046c8ebc9781101da5, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9de51be6fe5a74046c8ebc9781101da5 = cache_frame_9de51be6fe5a74046c8ebc9781101da5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9de51be6fe5a74046c8ebc9781101da5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9de51be6fe5a74046c8ebc9781101da5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_assattr_target_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "TimerFrame" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 191;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[0] );
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "wake" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 191;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[1] );
        frame_9de51be6fe5a74046c8ebc9781101da5->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_frame, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_frame );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        frame_9de51be6fe5a74046c8ebc9781101da5->m_frame.f_lineno = 192;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_Show, &PyTuple_GET_ITEM( const_tuple_false_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9de51be6fe5a74046c8ebc9781101da5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9de51be6fe5a74046c8ebc9781101da5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9de51be6fe5a74046c8ebc9781101da5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9de51be6fe5a74046c8ebc9781101da5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9de51be6fe5a74046c8ebc9781101da5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9de51be6fe5a74046c8ebc9781101da5,
        type_description_1,
        par_self,
        self->m_closure[0],
        self->m_closure[1]
    );


    // Release cached frame.
    if ( frame_9de51be6fe5a74046c8ebc9781101da5 == cache_frame_9de51be6fe5a74046c8ebc9781101da5 )
    {
        Py_DECREF( frame_9de51be6fe5a74046c8ebc9781101da5 );
    }
    cache_frame_9de51be6fe5a74046c8ebc9781101da5 = NULL;

    assertFrameObject( frame_9de51be6fe5a74046c8ebc9781101da5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_True;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx$$$function_4_OnInit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_9_loop_wx$$$function_4_OnInit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_10_loop_wx_exit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *var_wx = NULL;
    struct Nuitka_FrameObject *frame_9ef5d2ec27d9f4286940ad398de57237;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9ef5d2ec27d9f4286940ad398de57237 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9ef5d2ec27d9f4286940ad398de57237, codeobj_9ef5d2ec27d9f4286940ad398de57237, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *) );
    frame_9ef5d2ec27d9f4286940ad398de57237 = cache_frame_9ef5d2ec27d9f4286940ad398de57237;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9ef5d2ec27d9f4286940ad398de57237 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9ef5d2ec27d9f4286940ad398de57237 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_wx;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_9ef5d2ec27d9f4286940ad398de57237->m_frame.f_lineno = 215;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 215;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_wx == NULL );
        var_wx = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_wx );
        tmp_called_instance_1 = var_wx;
        frame_9ef5d2ec27d9f4286940ad398de57237->m_frame.f_lineno = 216;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_Exit );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9ef5d2ec27d9f4286940ad398de57237 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9ef5d2ec27d9f4286940ad398de57237 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9ef5d2ec27d9f4286940ad398de57237, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9ef5d2ec27d9f4286940ad398de57237->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9ef5d2ec27d9f4286940ad398de57237, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9ef5d2ec27d9f4286940ad398de57237,
        type_description_1,
        par_kernel,
        var_wx
    );


    // Release cached frame.
    if ( frame_9ef5d2ec27d9f4286940ad398de57237 == cache_frame_9ef5d2ec27d9f4286940ad398de57237 )
    {
        Py_DECREF( frame_9ef5d2ec27d9f4286940ad398de57237 );
    }
    cache_frame_9ef5d2ec27d9f4286940ad398de57237 = NULL;

    assertFrameObject( frame_9ef5d2ec27d9f4286940ad398de57237 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_10_loop_wx_exit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_wx );
    Py_DECREF( var_wx );
    var_wx = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_wx );
    var_wx = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_10_loop_wx_exit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_11_loop_tk( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *var_Tk = NULL;
    PyObject *var_READABLE = NULL;
    PyObject *var_process_stream_events = NULL;
    struct Nuitka_CellObject *var_app = PyCell_EMPTY();
    PyObject *var_stream = NULL;
    PyObject *var_notifier = NULL;
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_089c561f81035276a66d83f080abe4a3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_089c561f81035276a66d83f080abe4a3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_089c561f81035276a66d83f080abe4a3, codeobj_089c561f81035276a66d83f080abe4a3, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_089c561f81035276a66d83f080abe4a3 = cache_frame_089c561f81035276a66d83f080abe4a3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_089c561f81035276a66d83f080abe4a3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_089c561f81035276a66d83f080abe4a3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_tkinter;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Tk_str_plain_READABLE_tuple;
        tmp_level_name_1 = const_int_0;
        frame_089c561f81035276a66d83f080abe4a3->m_frame.f_lineno = 223;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "oooocoo";
            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Tk );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "oooocoo";
            goto try_except_handler_2;
        }
        assert( var_Tk == NULL );
        var_Tk = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_3 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_READABLE );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "oooocoo";
            goto try_except_handler_2;
        }
        assert( var_READABLE == NULL );
        var_READABLE = tmp_assign_source_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = MAKE_FUNCTION_ipykernel$eventloops$$$function_11_loop_tk$$$function_1_process_stream_events(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] = var_app;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] );


        assert( var_process_stream_events == NULL );
        var_process_stream_events = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_1;
        CHECK_OBJECT( var_Tk );
        tmp_called_name_1 = var_Tk;
        frame_089c561f81035276a66d83f080abe4a3->m_frame.f_lineno = 232;
        tmp_assign_source_5 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            type_description_1 = "oooocoo";
            goto try_except_handler_3;
        }
        assert( tmp_assign_unpack_1__assign_source == NULL );
        tmp_assign_unpack_1__assign_source = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
        tmp_assattr_name_1 = tmp_assign_unpack_1__assign_source;
        CHECK_OBJECT( par_kernel );
        tmp_assattr_target_1 = par_kernel;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_app, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            type_description_1 = "oooocoo";
            goto try_except_handler_3;
        }
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_assign_unpack_1__assign_source );
    tmp_assign_unpack_1__assign_source = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
        tmp_assign_source_6 = tmp_assign_unpack_1__assign_source;
        assert( PyCell_GET( var_app ) == NULL );
        Py_INCREF( tmp_assign_source_6 );
        PyCell_SET( var_app, tmp_assign_source_6 );

    }
    CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
    Py_DECREF( tmp_assign_unpack_1__assign_source );
    tmp_assign_unpack_1__assign_source = NULL;

    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_1 = par_kernel;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_app );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 233;
            type_description_1 = "oooocoo";
            goto frame_exception_exit_1;
        }
        frame_089c561f81035276a66d83f080abe4a3->m_frame.f_lineno = 233;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_withdraw );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 233;
            type_description_1 = "oooocoo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_2 = par_kernel;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_shell_streams );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 234;
            type_description_1 = "oooocoo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 234;
            type_description_1 = "oooocoo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_7;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooocoo";
                exception_lineno = 234;
                goto try_except_handler_4;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_9 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_stream;
            var_stream = tmp_assign_source_9;
            Py_INCREF( var_stream );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_partial );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_partial );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "partial" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 235;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( var_process_stream_events );
        tmp_args_element_name_1 = var_process_stream_events;
        CHECK_OBJECT( var_stream );
        tmp_args_element_name_2 = var_stream;
        frame_089c561f81035276a66d83f080abe4a3->m_frame.f_lineno = 235;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_10 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 235;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }
        {
            PyObject *old = var_notifier;
            var_notifier = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = const_str_plain_notifier;
        CHECK_OBJECT( var_notifier );
        tmp_assattr_target_2 = var_notifier;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain___name__, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 237;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        CHECK_OBJECT( PyCell_GET( var_app ) );
        tmp_source_name_4 = PyCell_GET( var_app );
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_tk );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_createfilehandler );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( var_stream );
        tmp_source_name_5 = var_stream;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_getsockopt );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 238;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_zmq );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zmq );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zmq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 238;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }

        tmp_source_name_6 = tmp_mvar_value_2;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_FD );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 238;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }
        frame_089c561f81035276a66d83f080abe4a3->m_frame.f_lineno = 238;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 238;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( var_READABLE );
        tmp_args_element_name_5 = var_READABLE;
        CHECK_OBJECT( var_notifier );
        tmp_args_element_name_6 = var_notifier;
        frame_089c561f81035276a66d83f080abe4a3->m_frame.f_lineno = 238;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        CHECK_OBJECT( PyCell_GET( var_app ) );
        tmp_called_instance_2 = PyCell_GET( var_app );
        tmp_args_element_name_7 = const_int_0;
        CHECK_OBJECT( var_notifier );
        tmp_args_element_name_8 = var_notifier;
        frame_089c561f81035276a66d83f080abe4a3->m_frame.f_lineno = 240;
        {
            PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_call_result_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_after, call_args );
        }

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "oooocoo";
            goto try_except_handler_4;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 234;
        type_description_1 = "oooocoo";
        goto try_except_handler_4;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_4;
        CHECK_OBJECT( PyCell_GET( var_app ) );
        tmp_called_instance_3 = PyCell_GET( var_app );
        frame_089c561f81035276a66d83f080abe4a3->m_frame.f_lineno = 242;
        tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_mainloop );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 242;
            type_description_1 = "oooocoo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_089c561f81035276a66d83f080abe4a3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_089c561f81035276a66d83f080abe4a3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_089c561f81035276a66d83f080abe4a3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_089c561f81035276a66d83f080abe4a3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_089c561f81035276a66d83f080abe4a3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_089c561f81035276a66d83f080abe4a3,
        type_description_1,
        par_kernel,
        var_Tk,
        var_READABLE,
        var_process_stream_events,
        var_app,
        var_stream,
        var_notifier
    );


    // Release cached frame.
    if ( frame_089c561f81035276a66d83f080abe4a3 == cache_frame_089c561f81035276a66d83f080abe4a3 )
    {
        Py_DECREF( frame_089c561f81035276a66d83f080abe4a3 );
    }
    cache_frame_089c561f81035276a66d83f080abe4a3 = NULL;

    assertFrameObject( frame_089c561f81035276a66d83f080abe4a3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_11_loop_tk );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_Tk );
    Py_DECREF( var_Tk );
    var_Tk = NULL;

    CHECK_OBJECT( (PyObject *)var_READABLE );
    Py_DECREF( var_READABLE );
    var_READABLE = NULL;

    CHECK_OBJECT( (PyObject *)var_process_stream_events );
    Py_DECREF( var_process_stream_events );
    var_process_stream_events = NULL;

    CHECK_OBJECT( (PyObject *)var_app );
    Py_DECREF( var_app );
    var_app = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    Py_XDECREF( var_notifier );
    var_notifier = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_Tk );
    var_Tk = NULL;

    Py_XDECREF( var_READABLE );
    var_READABLE = NULL;

    Py_XDECREF( var_process_stream_events );
    var_process_stream_events = NULL;

    CHECK_OBJECT( (PyObject *)var_app );
    Py_DECREF( var_app );
    var_app = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    Py_XDECREF( var_notifier );
    var_notifier = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_11_loop_tk );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_11_loop_tk$$$function_1_process_stream_events( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stream = python_pars[ 0 ];
    PyObject *par_a = python_pars[ 1 ];
    PyObject *par_kw = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_eb819a4b239d753061a6f7a6c8cd87b6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_eb819a4b239d753061a6f7a6c8cd87b6 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_eb819a4b239d753061a6f7a6c8cd87b6, codeobj_eb819a4b239d753061a6f7a6c8cd87b6, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_eb819a4b239d753061a6f7a6c8cd87b6 = cache_frame_eb819a4b239d753061a6f7a6c8cd87b6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eb819a4b239d753061a6f7a6c8cd87b6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eb819a4b239d753061a6f7a6c8cd87b6 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_stream );
        tmp_source_name_1 = par_stream;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_flush );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_f22dd43ccca7e02b2f29370abbf82a84 );
        frame_eb819a4b239d753061a6f7a6c8cd87b6->m_frame.f_lineno = 227;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 227;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_mvar_value_1;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 228;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = PyCell_GET( self->m_closure[0] );
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_tk );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_deletefilehandler );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_stream );
            tmp_source_name_4 = par_stream;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_getsockopt );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 228;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_zmq );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zmq );
            }

            if ( tmp_mvar_value_1 == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zmq" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 228;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }

            tmp_source_name_5 = tmp_mvar_value_1;
            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_FD );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 228;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }
            frame_eb819a4b239d753061a6f7a6c8cd87b6->m_frame.f_lineno = 228;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 228;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }
            frame_eb819a4b239d753061a6f7a6c8cd87b6->m_frame.f_lineno = 228;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_3;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 229;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
            frame_eb819a4b239d753061a6f7a6c8cd87b6->m_frame.f_lineno = 229;
            tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_quit );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 229;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb819a4b239d753061a6f7a6c8cd87b6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb819a4b239d753061a6f7a6c8cd87b6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eb819a4b239d753061a6f7a6c8cd87b6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eb819a4b239d753061a6f7a6c8cd87b6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eb819a4b239d753061a6f7a6c8cd87b6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eb819a4b239d753061a6f7a6c8cd87b6,
        type_description_1,
        par_stream,
        par_a,
        par_kw,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_eb819a4b239d753061a6f7a6c8cd87b6 == cache_frame_eb819a4b239d753061a6f7a6c8cd87b6 )
    {
        Py_DECREF( frame_eb819a4b239d753061a6f7a6c8cd87b6 );
    }
    cache_frame_eb819a4b239d753061a6f7a6c8cd87b6 = NULL;

    assertFrameObject( frame_eb819a4b239d753061a6f7a6c8cd87b6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_11_loop_tk$$$function_1_process_stream_events );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    CHECK_OBJECT( (PyObject *)par_a );
    Py_DECREF( par_a );
    par_a = NULL;

    CHECK_OBJECT( (PyObject *)par_kw );
    Py_DECREF( par_kw );
    par_kw = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    CHECK_OBJECT( (PyObject *)par_a );
    Py_DECREF( par_a );
    par_a = NULL;

    CHECK_OBJECT( (PyObject *)par_kw );
    Py_DECREF( par_kw );
    par_kw = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_11_loop_tk$$$function_1_process_stream_events );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_12_loop_tk_exit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_bcf0b356f68cb06ad5cf5300eb83ba82;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_bcf0b356f68cb06ad5cf5300eb83ba82 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bcf0b356f68cb06ad5cf5300eb83ba82, codeobj_bcf0b356f68cb06ad5cf5300eb83ba82, module_ipykernel$eventloops, sizeof(void *) );
    frame_bcf0b356f68cb06ad5cf5300eb83ba82 = cache_frame_bcf0b356f68cb06ad5cf5300eb83ba82;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bcf0b356f68cb06ad5cf5300eb83ba82 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bcf0b356f68cb06ad5cf5300eb83ba82 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_1 = par_kernel;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_app );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_bcf0b356f68cb06ad5cf5300eb83ba82->m_frame.f_lineno = 247;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_destroy );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bcf0b356f68cb06ad5cf5300eb83ba82 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bcf0b356f68cb06ad5cf5300eb83ba82 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bcf0b356f68cb06ad5cf5300eb83ba82, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bcf0b356f68cb06ad5cf5300eb83ba82->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bcf0b356f68cb06ad5cf5300eb83ba82, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bcf0b356f68cb06ad5cf5300eb83ba82,
        type_description_1,
        par_kernel
    );


    // Release cached frame.
    if ( frame_bcf0b356f68cb06ad5cf5300eb83ba82 == cache_frame_bcf0b356f68cb06ad5cf5300eb83ba82 )
    {
        Py_DECREF( frame_bcf0b356f68cb06ad5cf5300eb83ba82 );
    }
    cache_frame_bcf0b356f68cb06ad5cf5300eb83ba82 = NULL;

    assertFrameObject( frame_bcf0b356f68cb06ad5cf5300eb83ba82 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_12_loop_tk_exit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_12_loop_tk_exit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_13_loop_gtk( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *var_GTKEmbed = NULL;
    PyObject *var_gtk_kernel = NULL;
    struct Nuitka_FrameObject *frame_a6966745d50d40f1362e86d5fcb0ccd9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_a6966745d50d40f1362e86d5fcb0ccd9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a6966745d50d40f1362e86d5fcb0ccd9, codeobj_a6966745d50d40f1362e86d5fcb0ccd9, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a6966745d50d40f1362e86d5fcb0ccd9 = cache_frame_a6966745d50d40f1362e86d5fcb0ccd9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a6966745d50d40f1362e86d5fcb0ccd9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a6966745d50d40f1362e86d5fcb0ccd9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_e9c4e2ebaa0b1f02607e723383994479;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_GTKEmbed_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_a6966745d50d40f1362e86d5fcb0ccd9->m_frame.f_lineno = 253;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 253;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_ipykernel$eventloops,
                const_str_plain_GTKEmbed,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_GTKEmbed );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 253;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_GTKEmbed == NULL );
        var_GTKEmbed = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_GTKEmbed );
        tmp_called_name_1 = var_GTKEmbed;
        CHECK_OBJECT( par_kernel );
        tmp_args_element_name_1 = par_kernel;
        frame_a6966745d50d40f1362e86d5fcb0ccd9->m_frame.f_lineno = 255;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 255;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_gtk_kernel == NULL );
        var_gtk_kernel = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_gtk_kernel );
        tmp_called_instance_1 = var_gtk_kernel;
        frame_a6966745d50d40f1362e86d5fcb0ccd9->m_frame.f_lineno = 256;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_start );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_gtk_kernel );
        tmp_assattr_name_1 = var_gtk_kernel;
        CHECK_OBJECT( par_kernel );
        tmp_assattr_target_1 = par_kernel;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__gtk, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a6966745d50d40f1362e86d5fcb0ccd9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a6966745d50d40f1362e86d5fcb0ccd9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a6966745d50d40f1362e86d5fcb0ccd9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a6966745d50d40f1362e86d5fcb0ccd9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a6966745d50d40f1362e86d5fcb0ccd9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a6966745d50d40f1362e86d5fcb0ccd9,
        type_description_1,
        par_kernel,
        var_GTKEmbed,
        var_gtk_kernel
    );


    // Release cached frame.
    if ( frame_a6966745d50d40f1362e86d5fcb0ccd9 == cache_frame_a6966745d50d40f1362e86d5fcb0ccd9 )
    {
        Py_DECREF( frame_a6966745d50d40f1362e86d5fcb0ccd9 );
    }
    cache_frame_a6966745d50d40f1362e86d5fcb0ccd9 = NULL;

    assertFrameObject( frame_a6966745d50d40f1362e86d5fcb0ccd9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_13_loop_gtk );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_GTKEmbed );
    Py_DECREF( var_GTKEmbed );
    var_GTKEmbed = NULL;

    CHECK_OBJECT( (PyObject *)var_gtk_kernel );
    Py_DECREF( var_gtk_kernel );
    var_gtk_kernel = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_GTKEmbed );
    var_GTKEmbed = NULL;

    Py_XDECREF( var_gtk_kernel );
    var_gtk_kernel = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_13_loop_gtk );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_14_loop_gtk_exit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_e58cd038bd2e1fa086681096028429f2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e58cd038bd2e1fa086681096028429f2 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e58cd038bd2e1fa086681096028429f2, codeobj_e58cd038bd2e1fa086681096028429f2, module_ipykernel$eventloops, sizeof(void *) );
    frame_e58cd038bd2e1fa086681096028429f2 = cache_frame_e58cd038bd2e1fa086681096028429f2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e58cd038bd2e1fa086681096028429f2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e58cd038bd2e1fa086681096028429f2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_1 = par_kernel;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__gtk );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_e58cd038bd2e1fa086681096028429f2->m_frame.f_lineno = 262;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_stop );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e58cd038bd2e1fa086681096028429f2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e58cd038bd2e1fa086681096028429f2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e58cd038bd2e1fa086681096028429f2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e58cd038bd2e1fa086681096028429f2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e58cd038bd2e1fa086681096028429f2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e58cd038bd2e1fa086681096028429f2,
        type_description_1,
        par_kernel
    );


    // Release cached frame.
    if ( frame_e58cd038bd2e1fa086681096028429f2 == cache_frame_e58cd038bd2e1fa086681096028429f2 )
    {
        Py_DECREF( frame_e58cd038bd2e1fa086681096028429f2 );
    }
    cache_frame_e58cd038bd2e1fa086681096028429f2 = NULL;

    assertFrameObject( frame_e58cd038bd2e1fa086681096028429f2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_14_loop_gtk_exit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_14_loop_gtk_exit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_15_loop_gtk3( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *var_GTKEmbed = NULL;
    PyObject *var_gtk_kernel = NULL;
    struct Nuitka_FrameObject *frame_ba8f45f98f9cdde044b5032bf0075e62;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_ba8f45f98f9cdde044b5032bf0075e62 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ba8f45f98f9cdde044b5032bf0075e62, codeobj_ba8f45f98f9cdde044b5032bf0075e62, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ba8f45f98f9cdde044b5032bf0075e62 = cache_frame_ba8f45f98f9cdde044b5032bf0075e62;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ba8f45f98f9cdde044b5032bf0075e62 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ba8f45f98f9cdde044b5032bf0075e62 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_00c3f8c06b14a3722161887dd5baae41;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_GTKEmbed_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_ba8f45f98f9cdde044b5032bf0075e62->m_frame.f_lineno = 268;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 268;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_ipykernel$eventloops,
                const_str_plain_GTKEmbed,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_GTKEmbed );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 268;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_GTKEmbed == NULL );
        var_GTKEmbed = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_GTKEmbed );
        tmp_called_name_1 = var_GTKEmbed;
        CHECK_OBJECT( par_kernel );
        tmp_args_element_name_1 = par_kernel;
        frame_ba8f45f98f9cdde044b5032bf0075e62->m_frame.f_lineno = 270;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 270;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_gtk_kernel == NULL );
        var_gtk_kernel = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_gtk_kernel );
        tmp_called_instance_1 = var_gtk_kernel;
        frame_ba8f45f98f9cdde044b5032bf0075e62->m_frame.f_lineno = 271;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_start );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_gtk_kernel );
        tmp_assattr_name_1 = var_gtk_kernel;
        CHECK_OBJECT( par_kernel );
        tmp_assattr_target_1 = par_kernel;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__gtk, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ba8f45f98f9cdde044b5032bf0075e62 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ba8f45f98f9cdde044b5032bf0075e62 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ba8f45f98f9cdde044b5032bf0075e62, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ba8f45f98f9cdde044b5032bf0075e62->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ba8f45f98f9cdde044b5032bf0075e62, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ba8f45f98f9cdde044b5032bf0075e62,
        type_description_1,
        par_kernel,
        var_GTKEmbed,
        var_gtk_kernel
    );


    // Release cached frame.
    if ( frame_ba8f45f98f9cdde044b5032bf0075e62 == cache_frame_ba8f45f98f9cdde044b5032bf0075e62 )
    {
        Py_DECREF( frame_ba8f45f98f9cdde044b5032bf0075e62 );
    }
    cache_frame_ba8f45f98f9cdde044b5032bf0075e62 = NULL;

    assertFrameObject( frame_ba8f45f98f9cdde044b5032bf0075e62 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_15_loop_gtk3 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_GTKEmbed );
    Py_DECREF( var_GTKEmbed );
    var_GTKEmbed = NULL;

    CHECK_OBJECT( (PyObject *)var_gtk_kernel );
    Py_DECREF( var_gtk_kernel );
    var_gtk_kernel = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_GTKEmbed );
    var_GTKEmbed = NULL;

    Py_XDECREF( var_gtk_kernel );
    var_gtk_kernel = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_15_loop_gtk3 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_16_loop_gtk3_exit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_bedfe9919dfee0533970a7bfb93739af;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_bedfe9919dfee0533970a7bfb93739af = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bedfe9919dfee0533970a7bfb93739af, codeobj_bedfe9919dfee0533970a7bfb93739af, module_ipykernel$eventloops, sizeof(void *) );
    frame_bedfe9919dfee0533970a7bfb93739af = cache_frame_bedfe9919dfee0533970a7bfb93739af;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bedfe9919dfee0533970a7bfb93739af );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bedfe9919dfee0533970a7bfb93739af ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_1 = par_kernel;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__gtk );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_bedfe9919dfee0533970a7bfb93739af->m_frame.f_lineno = 277;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_stop );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bedfe9919dfee0533970a7bfb93739af );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bedfe9919dfee0533970a7bfb93739af );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bedfe9919dfee0533970a7bfb93739af, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bedfe9919dfee0533970a7bfb93739af->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bedfe9919dfee0533970a7bfb93739af, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bedfe9919dfee0533970a7bfb93739af,
        type_description_1,
        par_kernel
    );


    // Release cached frame.
    if ( frame_bedfe9919dfee0533970a7bfb93739af == cache_frame_bedfe9919dfee0533970a7bfb93739af )
    {
        Py_DECREF( frame_bedfe9919dfee0533970a7bfb93739af );
    }
    cache_frame_bedfe9919dfee0533970a7bfb93739af = NULL;

    assertFrameObject( frame_bedfe9919dfee0533970a7bfb93739af );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_16_loop_gtk3_exit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_16_loop_gtk3_exit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_17_loop_cocoa( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *var_mainloop = NULL;
    struct Nuitka_CellObject *var_stop = PyCell_EMPTY();
    struct Nuitka_CellObject *var_real_excepthook = PyCell_EMPTY();
    PyObject *var_handle_int = NULL;
    PyObject *var_stream = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_1d13d22c75e3ccb389eca1f15497fc26;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_preserved_type_3;
    PyObject *exception_preserved_value_3;
    PyTracebackObject *exception_preserved_tb_3;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    static struct Nuitka_FrameObject *cache_frame_1d13d22c75e3ccb389eca1f15497fc26 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1d13d22c75e3ccb389eca1f15497fc26, codeobj_1d13d22c75e3ccb389eca1f15497fc26, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1d13d22c75e3ccb389eca1f15497fc26 = cache_frame_1d13d22c75e3ccb389eca1f15497fc26;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1d13d22c75e3ccb389eca1f15497fc26 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1d13d22c75e3ccb389eca1f15497fc26 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain__eventloop_macos;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_mainloop_str_plain_stop_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame.f_lineno = 285;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 285;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_2 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_ipykernel$eventloops,
                const_str_plain_mainloop,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_mainloop );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 285;
            type_description_1 = "ooccoo";
            goto try_except_handler_2;
        }
        assert( var_mainloop == NULL );
        var_mainloop = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_3 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_ipykernel$eventloops,
                const_str_plain_stop,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_3 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_stop );
        }

        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 285;
            type_description_1 = "ooccoo";
            goto try_except_handler_2;
        }
        assert( PyCell_GET( var_stop ) == NULL );
        PyCell_SET( var_stop, tmp_assign_source_3 );

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 287;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_excepthook );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 287;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_real_excepthook ) == NULL );
        PyCell_SET( var_real_excepthook, tmp_assign_source_4 );

    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = MAKE_FUNCTION_ipykernel$eventloops$$$function_17_loop_cocoa$$$function_1_handle_int(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[0] = var_real_excepthook;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[1] = var_stop;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[1] );


        assert( var_handle_int == NULL );
        var_handle_int = tmp_assign_source_5;
    }
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_3 = par_kernel;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_shell );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 297;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_exit_now );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 297;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 297;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res == 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 297;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_1;
        branch_no_1:;
    }
    // Tried code:
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_handle_int );
        tmp_assattr_name_1 = var_handle_int;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 303;
            type_description_1 = "ooccoo";
            goto try_except_handler_5;
        }

        tmp_assattr_target_1 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_excepthook, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 303;
            type_description_1 = "ooccoo";
            goto try_except_handler_5;
        }
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_mainloop );
        tmp_called_name_1 = var_mainloop;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_4 = par_kernel;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__poll_interval );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "ooccoo";
            goto try_except_handler_5;
        }
        frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame.f_lineno = 304;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "ooccoo";
            goto try_except_handler_5;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_5 = par_kernel;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_shell_streams );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 305;
            type_description_1 = "ooccoo";
            goto try_except_handler_5;
        }
        tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 305;
            type_description_1 = "ooccoo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_for_loop_1__for_iterator;
            tmp_for_loop_1__for_iterator = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooccoo";
                exception_lineno = 305;
                goto try_except_handler_6;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_8 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_stream;
            var_stream = tmp_assign_source_8;
            Py_INCREF( var_stream );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_call_result_2;
        PyObject *tmp_kw_name_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_stream );
        tmp_source_name_6 = var_stream;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_flush );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 306;
            type_description_1 = "ooccoo";
            goto try_except_handler_6;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_f22dd43ccca7e02b2f29370abbf82a84 );
        frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame.f_lineno = 306;
        tmp_call_result_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 306;
            type_description_1 = "ooccoo";
            goto try_except_handler_6;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 306;
            type_description_1 = "ooccoo";
            goto try_except_handler_6;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_6;
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 305;
        type_description_1 = "ooccoo";
        goto try_except_handler_6;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_2;
    // Return handler code:
    try_return_handler_6:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto try_return_handler_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_5;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_1d13d22c75e3ccb389eca1f15497fc26, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_1d13d22c75e3ccb389eca1f15497fc26, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 310;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame) frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "ooccoo";
    goto try_except_handler_7;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_17_loop_cocoa );
    return NULL;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_4;
    // End of try:
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_5 == NULL )
    {
        exception_keeper_tb_5 = MAKE_TRACEBACK( frame_1d13d22c75e3ccb389eca1f15497fc26, exception_keeper_lineno_5 );
    }
    else if ( exception_keeper_lineno_5 != 0 )
    {
        exception_keeper_tb_5 = ADD_TRACEBACK( exception_keeper_tb_5, frame_1d13d22c75e3ccb389eca1f15497fc26, exception_keeper_lineno_5 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
    PyException_SetTraceback( exception_keeper_value_5, (PyObject *)exception_keeper_tb_5 );
    PUBLISH_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_KeyboardInterrupt;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 311;
            type_description_1 = "ooccoo";
            goto try_except_handler_8;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_name_1;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_3;
            tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_print );
            assert( tmp_called_name_3 != NULL );
            tmp_args_name_1 = const_tuple_str_digest_08a8820022fa063f6c6662cf23ef51be_tuple;
            tmp_dict_key_1 = const_str_plain_file;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 313;
                type_description_1 = "ooccoo";
                goto try_except_handler_8;
            }

            tmp_source_name_7 = tmp_mvar_value_3;
            tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain___stdout__ );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;
                type_description_1 = "ooccoo";
                goto try_except_handler_8;
            }
            tmp_kw_name_2 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame.f_lineno = 313;
            tmp_call_result_3 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;
                type_description_1 = "ooccoo";
                goto try_except_handler_8;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        goto branch_end_3;
        branch_no_3:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 298;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame) frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooccoo";
        goto try_except_handler_8;
        branch_end_3:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto try_except_handler_3;
    // End of try:
    try_end_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_4;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_17_loop_cocoa );
    return NULL;
    // End of try:
    try_end_4:;
    goto try_end_6;
    // Return handler code:
    try_return_handler_3:;
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_4;
        CHECK_OBJECT( PyCell_GET( var_real_excepthook ) );
        tmp_assattr_name_2 = PyCell_GET( var_real_excepthook );
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 316;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_2 = tmp_mvar_value_4;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_excepthook, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }
    }
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_3 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_3 );
    exception_preserved_value_3 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_3 );
    exception_preserved_tb_3 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_3 );

    if ( exception_keeper_tb_7 == NULL )
    {
        exception_keeper_tb_7 = MAKE_TRACEBACK( frame_1d13d22c75e3ccb389eca1f15497fc26, exception_keeper_lineno_7 );
    }
    else if ( exception_keeper_lineno_7 != 0 )
    {
        exception_keeper_tb_7 = ADD_TRACEBACK( exception_keeper_tb_7, frame_1d13d22c75e3ccb389eca1f15497fc26, exception_keeper_lineno_7 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    PyException_SetTraceback( exception_keeper_value_7, (PyObject *)exception_keeper_tb_7 );
    PUBLISH_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    // Tried code:
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        PyObject *tmp_mvar_value_5;
        CHECK_OBJECT( PyCell_GET( var_real_excepthook ) );
        tmp_assattr_name_3 = PyCell_GET( var_real_excepthook );
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 316;
            type_description_1 = "ooccoo";
            goto try_except_handler_9;
        }

        tmp_assattr_target_3 = tmp_mvar_value_5;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_excepthook, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_1 = "ooccoo";
            goto try_except_handler_9;
        }
    }
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 298;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame) frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "ooccoo";
    goto try_except_handler_9;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_17_loop_cocoa );
    return NULL;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_6:;
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        PyObject *tmp_mvar_value_6;
        CHECK_OBJECT( PyCell_GET( var_real_excepthook ) );
        tmp_assattr_name_4 = PyCell_GET( var_real_excepthook );
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 316;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_4 = tmp_mvar_value_6;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_excepthook, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_1 = "ooccoo";
            goto frame_exception_exit_1;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 297;
        type_description_1 = "ooccoo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d13d22c75e3ccb389eca1f15497fc26 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d13d22c75e3ccb389eca1f15497fc26 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d13d22c75e3ccb389eca1f15497fc26 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1d13d22c75e3ccb389eca1f15497fc26, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1d13d22c75e3ccb389eca1f15497fc26->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1d13d22c75e3ccb389eca1f15497fc26, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1d13d22c75e3ccb389eca1f15497fc26,
        type_description_1,
        par_kernel,
        var_mainloop,
        var_stop,
        var_real_excepthook,
        var_handle_int,
        var_stream
    );


    // Release cached frame.
    if ( frame_1d13d22c75e3ccb389eca1f15497fc26 == cache_frame_1d13d22c75e3ccb389eca1f15497fc26 )
    {
        Py_DECREF( frame_1d13d22c75e3ccb389eca1f15497fc26 );
    }
    cache_frame_1d13d22c75e3ccb389eca1f15497fc26 = NULL;

    assertFrameObject( frame_1d13d22c75e3ccb389eca1f15497fc26 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_17_loop_cocoa );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_mainloop );
    Py_DECREF( var_mainloop );
    var_mainloop = NULL;

    CHECK_OBJECT( (PyObject *)var_stop );
    Py_DECREF( var_stop );
    var_stop = NULL;

    CHECK_OBJECT( (PyObject *)var_real_excepthook );
    Py_DECREF( var_real_excepthook );
    var_real_excepthook = NULL;

    CHECK_OBJECT( (PyObject *)var_handle_int );
    Py_DECREF( var_handle_int );
    var_handle_int = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_mainloop );
    var_mainloop = NULL;

    CHECK_OBJECT( (PyObject *)var_stop );
    Py_DECREF( var_stop );
    var_stop = NULL;

    CHECK_OBJECT( (PyObject *)var_real_excepthook );
    Py_DECREF( var_real_excepthook );
    var_real_excepthook = NULL;

    Py_XDECREF( var_handle_int );
    var_handle_int = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_17_loop_cocoa );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_17_loop_cocoa$$$function_1_handle_int( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_etype = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    PyObject *par_tb = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_4d51db9e71dac7e5d3872e993acf1eff;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_4d51db9e71dac7e5d3872e993acf1eff = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4d51db9e71dac7e5d3872e993acf1eff, codeobj_4d51db9e71dac7e5d3872e993acf1eff, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4d51db9e71dac7e5d3872e993acf1eff = cache_frame_4d51db9e71dac7e5d3872e993acf1eff;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4d51db9e71dac7e5d3872e993acf1eff );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4d51db9e71dac7e5d3872e993acf1eff ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "stop" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 291;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[1] );
        frame_4d51db9e71dac7e5d3872e993acf1eff->m_frame.f_lineno = 291;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 291;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_etype );
        tmp_compexpr_left_1 = par_etype;
        tmp_compexpr_right_1 = PyExc_KeyboardInterrupt;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            tmp_called_name_2 = LOOKUP_BUILTIN( const_str_plain_print );
            assert( tmp_called_name_2 != NULL );
            tmp_args_name_1 = const_tuple_str_digest_62efe02fdb3e422cbc100b1b6331c18f_tuple;
            tmp_dict_key_1 = const_str_plain_file;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 293;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___stdout__ );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 293;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_4d51db9e71dac7e5d3872e993acf1eff->m_frame.f_lineno = 293;
            tmp_call_result_2 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 293;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "real_excepthook" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 295;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_3 = PyCell_GET( self->m_closure[0] );
            CHECK_OBJECT( par_etype );
            tmp_args_element_name_1 = par_etype;
            CHECK_OBJECT( par_value );
            tmp_args_element_name_2 = par_value;
            CHECK_OBJECT( par_tb );
            tmp_args_element_name_3 = par_tb;
            frame_4d51db9e71dac7e5d3872e993acf1eff->m_frame.f_lineno = 295;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 295;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4d51db9e71dac7e5d3872e993acf1eff );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4d51db9e71dac7e5d3872e993acf1eff );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4d51db9e71dac7e5d3872e993acf1eff, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4d51db9e71dac7e5d3872e993acf1eff->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4d51db9e71dac7e5d3872e993acf1eff, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4d51db9e71dac7e5d3872e993acf1eff,
        type_description_1,
        par_etype,
        par_value,
        par_tb,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_4d51db9e71dac7e5d3872e993acf1eff == cache_frame_4d51db9e71dac7e5d3872e993acf1eff )
    {
        Py_DECREF( frame_4d51db9e71dac7e5d3872e993acf1eff );
    }
    cache_frame_4d51db9e71dac7e5d3872e993acf1eff = NULL;

    assertFrameObject( frame_4d51db9e71dac7e5d3872e993acf1eff );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_17_loop_cocoa$$$function_1_handle_int );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_etype );
    Py_DECREF( par_etype );
    par_etype = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_tb );
    Py_DECREF( par_tb );
    par_tb = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_etype );
    Py_DECREF( par_etype );
    par_etype = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_tb );
    Py_DECREF( par_tb );
    par_tb = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_17_loop_cocoa$$$function_1_handle_int );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_18_loop_cocoa_exit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *var_stop = NULL;
    struct Nuitka_FrameObject *frame_18411978282a4eead2ffa6a85c3c3ec8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_18411978282a4eead2ffa6a85c3c3ec8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_18411978282a4eead2ffa6a85c3c3ec8, codeobj_18411978282a4eead2ffa6a85c3c3ec8, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *) );
    frame_18411978282a4eead2ffa6a85c3c3ec8 = cache_frame_18411978282a4eead2ffa6a85c3c3ec8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_18411978282a4eead2ffa6a85c3c3ec8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_18411978282a4eead2ffa6a85c3c3ec8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain__eventloop_macos;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_stop_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_18411978282a4eead2ffa6a85c3c3ec8->m_frame.f_lineno = 321;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 321;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_ipykernel$eventloops,
                const_str_plain_stop,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_stop );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 321;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_stop == NULL );
        var_stop = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_stop );
        tmp_called_name_1 = var_stop;
        frame_18411978282a4eead2ffa6a85c3c3ec8->m_frame.f_lineno = 322;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 322;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18411978282a4eead2ffa6a85c3c3ec8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18411978282a4eead2ffa6a85c3c3ec8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_18411978282a4eead2ffa6a85c3c3ec8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_18411978282a4eead2ffa6a85c3c3ec8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_18411978282a4eead2ffa6a85c3c3ec8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_18411978282a4eead2ffa6a85c3c3ec8,
        type_description_1,
        par_kernel,
        var_stop
    );


    // Release cached frame.
    if ( frame_18411978282a4eead2ffa6a85c3c3ec8 == cache_frame_18411978282a4eead2ffa6a85c3c3ec8 )
    {
        Py_DECREF( frame_18411978282a4eead2ffa6a85c3c3ec8 );
    }
    cache_frame_18411978282a4eead2ffa6a85c3c3ec8 = NULL;

    assertFrameObject( frame_18411978282a4eead2ffa6a85c3c3ec8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_18_loop_cocoa_exit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_stop );
    Py_DECREF( var_stop );
    var_stop = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_stop );
    var_stop = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_18_loop_cocoa_exit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_19_loop_asyncio( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *var_asyncio = NULL;
    struct Nuitka_CellObject *var_loop = PyCell_EMPTY();
    PyObject *var_process_stream_events = NULL;
    PyObject *var_stream = NULL;
    PyObject *var_fd = NULL;
    PyObject *var_notifier = NULL;
    PyObject *var_error = NULL;
    PyObject *var_e = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_2eda2a1510ed33ba9a8d5b7587967ab6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_2eda2a1510ed33ba9a8d5b7587967ab6 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2eda2a1510ed33ba9a8d5b7587967ab6, codeobj_2eda2a1510ed33ba9a8d5b7587967ab6, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2eda2a1510ed33ba9a8d5b7587967ab6 = cache_frame_2eda2a1510ed33ba9a8d5b7587967ab6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2eda2a1510ed33ba9a8d5b7587967ab6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2eda2a1510ed33ba9a8d5b7587967ab6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_asyncio;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 328;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 328;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        assert( var_asyncio == NULL );
        var_asyncio = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_asyncio );
        tmp_called_instance_1 = var_asyncio;
        frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 329;
        tmp_assign_source_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_event_loop );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 329;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_loop ) == NULL );
        PyCell_SET( var_loop, tmp_assign_source_2 );

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( PyCell_GET( var_loop ) );
        tmp_called_instance_2 = PyCell_GET( var_loop );
        frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 331;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_is_running );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 331;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 331;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( PyCell_GET( var_loop ) );
        tmp_called_instance_3 = PyCell_GET( var_loop );
        frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 334;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_is_closed );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 334;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_instance_4;
            CHECK_OBJECT( var_asyncio );
            tmp_called_instance_4 = var_asyncio;
            frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 336;
            tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_new_event_loop );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 336;
                type_description_1 = "oocoooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = PyCell_GET( var_loop );
                PyCell_SET( var_loop, tmp_assign_source_3 );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_instance_5;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var_asyncio );
            tmp_called_instance_5 = var_asyncio;
            CHECK_OBJECT( PyCell_GET( var_loop ) );
            tmp_args_element_name_1 = PyCell_GET( var_loop );
            frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 337;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_set_event_loop, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 337;
                type_description_1 = "oocoooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_False;
        CHECK_OBJECT( PyCell_GET( var_loop ) );
        tmp_assattr_target_1 = PyCell_GET( var_loop );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__should_close, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 338;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = MAKE_FUNCTION_ipykernel$eventloops$$$function_19_loop_asyncio$$$function_1_process_stream_events(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] = var_loop;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] );


        assert( var_process_stream_events == NULL );
        var_process_stream_events = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_kernel );
        tmp_source_name_1 = par_kernel;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_shell_streams );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_5;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oocoooooo";
                exception_lineno = 346;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_7 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_stream;
            var_stream = tmp_assign_source_7;
            Py_INCREF( var_stream );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( var_stream );
        tmp_source_name_2 = var_stream;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_getsockopt );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 347;
            type_description_1 = "oocoooooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_zmq );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zmq );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zmq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 347;
            type_description_1 = "oocoooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_3 = tmp_mvar_value_1;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_FD );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 347;
            type_description_1 = "oocoooooo";
            goto try_except_handler_2;
        }
        frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 347;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 347;
            type_description_1 = "oocoooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_fd;
            var_fd = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_partial );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_partial );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "partial" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 348;
            type_description_1 = "oocoooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( var_process_stream_events );
        tmp_args_element_name_3 = var_process_stream_events;
        CHECK_OBJECT( var_stream );
        tmp_args_element_name_4 = var_stream;
        frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 348;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 348;
            type_description_1 = "oocoooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_notifier;
            var_notifier = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_6;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        CHECK_OBJECT( PyCell_GET( var_loop ) );
        tmp_called_instance_6 = PyCell_GET( var_loop );
        CHECK_OBJECT( var_fd );
        tmp_args_element_name_5 = var_fd;
        CHECK_OBJECT( var_notifier );
        tmp_args_element_name_6 = var_notifier;
        frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 349;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_call_result_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_6, const_str_plain_add_reader, call_args );
        }

        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 349;
            type_description_1 = "oocoooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_instance_7;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_7;
        CHECK_OBJECT( PyCell_GET( var_loop ) );
        tmp_called_instance_7 = PyCell_GET( var_loop );
        CHECK_OBJECT( var_notifier );
        tmp_args_element_name_7 = var_notifier;
        frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 350;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_call_soon, call_args );
        }

        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 350;
            type_description_1 = "oocoooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 346;
        type_description_1 = "oocoooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    loop_start_2:;
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = Py_None;
        {
            PyObject *old = var_error;
            var_error = tmp_assign_source_10;
            Py_INCREF( var_error );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_called_instance_8;
        PyObject *tmp_call_result_6;
        CHECK_OBJECT( PyCell_GET( var_loop ) );
        tmp_called_instance_8 = PyCell_GET( var_loop );
        frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 355;
        tmp_call_result_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_run_forever );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 355;
            type_description_1 = "oocoooooo";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_2eda2a1510ed33ba9a8d5b7587967ab6, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_2eda2a1510ed33ba9a8d5b7587967ab6, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_KeyboardInterrupt;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 356;
            type_description_1 = "oocoooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        goto try_continue_handler_4;
        goto branch_end_3;
        branch_no_3:;
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = PyExc_Exception;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 358;
                type_description_1 = "oocoooooo";
                goto try_except_handler_4;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_11;
                tmp_assign_source_11 = EXC_VALUE(PyThreadState_GET());
                {
                    PyObject *old = var_e;
                    var_e = tmp_assign_source_11;
                    Py_INCREF( var_e );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_12;
                CHECK_OBJECT( var_e );
                tmp_assign_source_12 = var_e;
                {
                    PyObject *old = var_error;
                    assert( old != NULL );
                    var_error = tmp_assign_source_12;
                    Py_INCREF( var_error );
                    Py_DECREF( old );
                }

            }
            Py_XDECREF( var_e );
            var_e = NULL;

            goto branch_end_4;
            branch_no_4:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 354;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame) frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oocoooooo";
            goto try_except_handler_4;
            branch_end_4:;
        }
        branch_end_3:;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // try continue handler code:
    try_continue_handler_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto loop_start_2;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_19_loop_asyncio );
    return NULL;
    // End of try:
    try_end_2:;
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_4;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_3;
        CHECK_OBJECT( PyCell_GET( var_loop ) );
        tmp_source_name_4 = PyCell_GET( var_loop );
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__should_close );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 360;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_5 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_called_instance_9;
            PyObject *tmp_call_result_7;
            CHECK_OBJECT( PyCell_GET( var_loop ) );
            tmp_called_instance_9 = PyCell_GET( var_loop );
            frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame.f_lineno = 361;
            tmp_call_result_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_close );
            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_1 = "oocoooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        if ( var_error == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "error" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 362;
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_3 = var_error;
        tmp_compexpr_right_3 = Py_None;
        tmp_condition_result_6 = ( tmp_compexpr_left_3 != tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_raise_type_1;
            if ( var_error == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "error" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 363;
                type_description_1 = "oocoooooo";
                goto frame_exception_exit_1;
            }

            tmp_raise_type_1 = var_error;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 363;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oocoooooo";
            goto frame_exception_exit_1;
        }
        branch_no_6:;
    }
    goto loop_end_2;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 352;
        type_description_1 = "oocoooooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_2;
    loop_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2eda2a1510ed33ba9a8d5b7587967ab6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2eda2a1510ed33ba9a8d5b7587967ab6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2eda2a1510ed33ba9a8d5b7587967ab6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2eda2a1510ed33ba9a8d5b7587967ab6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2eda2a1510ed33ba9a8d5b7587967ab6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2eda2a1510ed33ba9a8d5b7587967ab6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2eda2a1510ed33ba9a8d5b7587967ab6,
        type_description_1,
        par_kernel,
        var_asyncio,
        var_loop,
        var_process_stream_events,
        var_stream,
        var_fd,
        var_notifier,
        var_error,
        var_e
    );


    // Release cached frame.
    if ( frame_2eda2a1510ed33ba9a8d5b7587967ab6 == cache_frame_2eda2a1510ed33ba9a8d5b7587967ab6 )
    {
        Py_DECREF( frame_2eda2a1510ed33ba9a8d5b7587967ab6 );
    }
    cache_frame_2eda2a1510ed33ba9a8d5b7587967ab6 = NULL;

    assertFrameObject( frame_2eda2a1510ed33ba9a8d5b7587967ab6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_19_loop_asyncio );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_asyncio );
    Py_DECREF( var_asyncio );
    var_asyncio = NULL;

    CHECK_OBJECT( (PyObject *)var_loop );
    Py_DECREF( var_loop );
    var_loop = NULL;

    Py_XDECREF( var_process_stream_events );
    var_process_stream_events = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    Py_XDECREF( var_fd );
    var_fd = NULL;

    Py_XDECREF( var_notifier );
    var_notifier = NULL;

    Py_XDECREF( var_error );
    var_error = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_asyncio );
    var_asyncio = NULL;

    CHECK_OBJECT( (PyObject *)var_loop );
    Py_DECREF( var_loop );
    var_loop = NULL;

    Py_XDECREF( var_process_stream_events );
    var_process_stream_events = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    Py_XDECREF( var_fd );
    var_fd = NULL;

    Py_XDECREF( var_notifier );
    var_notifier = NULL;

    Py_XDECREF( var_error );
    var_error = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_19_loop_asyncio );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_19_loop_asyncio$$$function_1_process_stream_events( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stream = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c819a73e87849bc6bee0f826b7d6670c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c819a73e87849bc6bee0f826b7d6670c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c819a73e87849bc6bee0f826b7d6670c, codeobj_c819a73e87849bc6bee0f826b7d6670c, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *) );
    frame_c819a73e87849bc6bee0f826b7d6670c = cache_frame_c819a73e87849bc6bee0f826b7d6670c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c819a73e87849bc6bee0f826b7d6670c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c819a73e87849bc6bee0f826b7d6670c ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_stream );
        tmp_source_name_1 = par_stream;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_flush );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 343;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_f22dd43ccca7e02b2f29370abbf82a84 );
        frame_c819a73e87849bc6bee0f826b7d6670c->m_frame.f_lineno = 343;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 343;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 343;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_2;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "loop" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 344;
                type_description_1 = "oc";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
            frame_c819a73e87849bc6bee0f826b7d6670c->m_frame.f_lineno = 344;
            tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_stop );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 344;
                type_description_1 = "oc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c819a73e87849bc6bee0f826b7d6670c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c819a73e87849bc6bee0f826b7d6670c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c819a73e87849bc6bee0f826b7d6670c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c819a73e87849bc6bee0f826b7d6670c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c819a73e87849bc6bee0f826b7d6670c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c819a73e87849bc6bee0f826b7d6670c,
        type_description_1,
        par_stream,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_c819a73e87849bc6bee0f826b7d6670c == cache_frame_c819a73e87849bc6bee0f826b7d6670c )
    {
        Py_DECREF( frame_c819a73e87849bc6bee0f826b7d6670c );
    }
    cache_frame_c819a73e87849bc6bee0f826b7d6670c = NULL;

    assertFrameObject( frame_c819a73e87849bc6bee0f826b7d6670c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_19_loop_asyncio$$$function_1_process_stream_events );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_19_loop_asyncio$$$function_1_process_stream_events );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_20_loop_asyncio_exit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kernel = python_pars[ 0 ];
    PyObject *var_asyncio = NULL;
    struct Nuitka_CellObject *var_loop = PyCell_EMPTY();
    PyObject *var_close_loop = NULL;
    struct Nuitka_FrameObject *frame_232eec1d13930f531fd7f1fd0bc255a3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_232eec1d13930f531fd7f1fd0bc255a3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_232eec1d13930f531fd7f1fd0bc255a3, codeobj_232eec1d13930f531fd7f1fd0bc255a3, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_232eec1d13930f531fd7f1fd0bc255a3 = cache_frame_232eec1d13930f531fd7f1fd0bc255a3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_232eec1d13930f531fd7f1fd0bc255a3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_232eec1d13930f531fd7f1fd0bc255a3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_asyncio;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_232eec1d13930f531fd7f1fd0bc255a3->m_frame.f_lineno = 370;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 370;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        assert( var_asyncio == NULL );
        var_asyncio = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_asyncio );
        tmp_called_instance_1 = var_asyncio;
        frame_232eec1d13930f531fd7f1fd0bc255a3->m_frame.f_lineno = 371;
        tmp_assign_source_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_event_loop );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 371;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_loop ) == NULL );
        PyCell_SET( var_loop, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_asyncio );
        tmp_called_instance_2 = var_asyncio;
        tmp_args_element_name_1 = MAKE_FUNCTION_ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] = var_loop;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] );


        frame_232eec1d13930f531fd7f1fd0bc255a3->m_frame.f_lineno = 373;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_coroutine, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 373;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        assert( var_close_loop == NULL );
        var_close_loop = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( PyCell_GET( var_loop ) );
        tmp_called_instance_3 = PyCell_GET( var_loop );
        frame_232eec1d13930f531fd7f1fd0bc255a3->m_frame.f_lineno = 380;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_is_running );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 380;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 380;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( var_close_loop );
            tmp_called_name_1 = var_close_loop;
            frame_232eec1d13930f531fd7f1fd0bc255a3->m_frame.f_lineno = 381;
            tmp_call_result_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_1 = "ooco";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_instance_4;
            CHECK_OBJECT( PyCell_GET( var_loop ) );
            tmp_called_instance_4 = PyCell_GET( var_loop );
            frame_232eec1d13930f531fd7f1fd0bc255a3->m_frame.f_lineno = 383;
            tmp_operand_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_is_closed );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 383;
                type_description_1 = "ooco";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 383;
                type_description_1 = "ooco";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_instance_5;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_2;
                CHECK_OBJECT( PyCell_GET( var_loop ) );
                tmp_called_instance_5 = PyCell_GET( var_loop );
                CHECK_OBJECT( var_close_loop );
                tmp_args_element_name_2 = var_close_loop;
                frame_232eec1d13930f531fd7f1fd0bc255a3->m_frame.f_lineno = 384;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_run_until_complete, call_args );
                }

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "ooco";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            {
                PyObject *tmp_called_instance_6;
                PyObject *tmp_call_result_4;
                CHECK_OBJECT( PyCell_GET( var_loop ) );
                tmp_called_instance_6 = PyCell_GET( var_loop );
                frame_232eec1d13930f531fd7f1fd0bc255a3->m_frame.f_lineno = 385;
                tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_close );
                if ( tmp_call_result_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 385;
                    type_description_1 = "ooco";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_4 );
            }
            branch_no_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_232eec1d13930f531fd7f1fd0bc255a3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_232eec1d13930f531fd7f1fd0bc255a3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_232eec1d13930f531fd7f1fd0bc255a3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_232eec1d13930f531fd7f1fd0bc255a3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_232eec1d13930f531fd7f1fd0bc255a3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_232eec1d13930f531fd7f1fd0bc255a3,
        type_description_1,
        par_kernel,
        var_asyncio,
        var_loop,
        var_close_loop
    );


    // Release cached frame.
    if ( frame_232eec1d13930f531fd7f1fd0bc255a3 == cache_frame_232eec1d13930f531fd7f1fd0bc255a3 )
    {
        Py_DECREF( frame_232eec1d13930f531fd7f1fd0bc255a3 );
    }
    cache_frame_232eec1d13930f531fd7f1fd0bc255a3 = NULL;

    assertFrameObject( frame_232eec1d13930f531fd7f1fd0bc255a3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_20_loop_asyncio_exit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_asyncio );
    Py_DECREF( var_asyncio );
    var_asyncio = NULL;

    CHECK_OBJECT( (PyObject *)var_loop );
    Py_DECREF( var_loop );
    var_loop = NULL;

    CHECK_OBJECT( (PyObject *)var_close_loop );
    Py_DECREF( var_close_loop );
    var_close_loop = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kernel );
    Py_DECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_asyncio );
    var_asyncio = NULL;

    CHECK_OBJECT( (PyObject *)var_loop );
    Py_DECREF( var_loop );
    var_loop = NULL;

    Py_XDECREF( var_close_loop );
    var_close_loop = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_20_loop_asyncio_exit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop$$$genobj_1_close_loop_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = self->m_closure[0];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );


    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop$$$genobj_1_close_loop_locals {
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    bool tmp_result;
};

static PyObject *ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop$$$genobj_1_close_loop_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop$$$genobj_1_close_loop_locals *generator_heap = (struct ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop$$$genobj_1_close_loop_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_e06efd7b945f078458bf864669e9f639, module_ipykernel$eventloops, sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_name_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "loop" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 375;
            generator_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( generator->m_closure[0] );
        tmp_attribute_name_1 = const_str_plain_shutdown_asyncgens;
        generator_heap->tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 375;
            generator_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_expression_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_yieldfrom_result_1;
            if ( PyCell_GET( generator->m_closure[0] ) == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "loop" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 376;
                generator_heap->type_description_1 = "c";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = PyCell_GET( generator->m_closure[0] );
            generator->m_frame->m_frame.f_lineno = 376;
            tmp_expression_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_shutdown_asyncgens );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 376;
                generator_heap->type_description_1 = "c";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
            generator->m_yield_return_index = 1;
            generator->m_yieldfrom = tmp_expression_name_1;
            return NULL;

            yield_return_1:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 376;
                generator_heap->type_description_1 = "c";
                goto frame_exception_exit_1;
            }
            tmp_yieldfrom_result_1 = yield_return_value;
            Py_DECREF( tmp_yieldfrom_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_True;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "loop" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 377;
            generator_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_1 = PyCell_GET( generator->m_closure[0] );
        generator_heap->tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__should_close, tmp_assattr_name_1 );
        if ( generator_heap->tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 377;
            generator_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "loop" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 378;
            generator_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = PyCell_GET( generator->m_closure[0] );
        generator->m_frame->m_frame.f_lineno = 378;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_stop );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 378;
            generator_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[0]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;


    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop$$$genobj_1_close_loop_maker( void )
{
    return Nuitka_Generator_New(
        ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop$$$genobj_1_close_loop_context,
        module_ipykernel$eventloops,
        const_str_plain_close_loop,
#if PYTHON_VERSION >= 350
        const_str_digest_68e19db4deb3cfd9bcff9bbba59a16fc,
#endif
        codeobj_e06efd7b945f078458bf864669e9f639,
        1,
        sizeof(struct ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop$$$genobj_1_close_loop_locals)
    );
}


static PyObject *impl_ipykernel$eventloops$$$function_21_enable_gui( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_gui = python_pars[ 0 ];
    PyObject *par_kernel = python_pars[ 1 ];
    PyObject *var_e = NULL;
    PyObject *var_loop = NULL;
    struct Nuitka_FrameObject *frame_d538afe2a4994bbddb928a1283d3e3fb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_d538afe2a4994bbddb928a1283d3e3fb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d538afe2a4994bbddb928a1283d3e3fb, codeobj_d538afe2a4994bbddb928a1283d3e3fb, module_ipykernel$eventloops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d538afe2a4994bbddb928a1283d3e3fb = cache_frame_d538afe2a4994bbddb928a1283d3e3fb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d538afe2a4994bbddb928a1283d3e3fb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d538afe2a4994bbddb928a1283d3e3fb ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_gui );
        tmp_compexpr_left_1 = par_gui;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_map );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_map );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "loop_map" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 390;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_1;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 390;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_2;
            tmp_left_name_1 = const_str_digest_14f4623b35f4ef5cde8d7a15e5f93278;
            CHECK_OBJECT( par_gui );
            tmp_tuple_element_1 = par_gui;
            tmp_right_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_map );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_map );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_right_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "loop_map" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 391;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_2;
            frame_d538afe2a4994bbddb928a1283d3e3fb->m_frame.f_lineno = 391;
            tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_keys );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_right_name_1 );

                exception_lineno = 391;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
            tmp_assign_source_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 391;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_e == NULL );
            var_e = tmp_assign_source_1;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( var_e );
            tmp_make_exception_arg_1 = var_e;
            frame_d538afe2a4994bbddb928a1283d3e3fb->m_frame.f_lineno = 392;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 392;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_kernel );
        tmp_compexpr_left_2 = par_kernel;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_call_result_1;
            int tmp_truth_name_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_Application );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Application );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Application" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 394;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_2 = tmp_mvar_value_3;
            frame_d538afe2a4994bbddb928a1283d3e3fb->m_frame.f_lineno = 394;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_initialized );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 394;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_1 );

                exception_lineno = 394;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_1 );
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_Application );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Application );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Application" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 395;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_3 = tmp_mvar_value_4;
                frame_d538afe2a4994bbddb928a1283d3e3fb->m_frame.f_lineno = 395;
                tmp_getattr_target_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_instance );
                if ( tmp_getattr_target_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 395;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_getattr_attr_1 = const_str_plain_kernel;
                tmp_getattr_default_1 = Py_None;
                tmp_assign_source_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                Py_DECREF( tmp_getattr_target_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 395;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_kernel;
                    assert( old != NULL );
                    par_kernel = tmp_assign_source_2;
                    Py_DECREF( old );
                }

            }
            branch_no_3:;
        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( par_kernel );
            tmp_compexpr_left_3 = par_kernel;
            tmp_compexpr_right_3 = Py_None;
            tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                tmp_make_exception_arg_2 = const_str_digest_b9c9692660661ee5f06ab97d344bf8fc;
                frame_d538afe2a4994bbddb928a1283d3e3fb->m_frame.f_lineno = 397;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
                }

                assert( !(tmp_raise_type_2 == NULL) );
                exception_type = tmp_raise_type_2;
                exception_lineno = 397;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            branch_no_4:;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_map );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_map );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "loop_map" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 400;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_5;
        CHECK_OBJECT( par_gui );
        tmp_subscript_name_1 = par_gui;
        tmp_assign_source_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 400;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_loop == NULL );
        var_loop = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_5;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_2;
        int tmp_and_left_truth_2;
        nuitka_bool tmp_and_left_value_2;
        nuitka_bool tmp_and_right_value_2;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_1;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_loop );
        tmp_truth_name_2 = CHECK_IF_TRUE( var_loop );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 401;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        if ( par_kernel == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kernel" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 401;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = par_kernel;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_eventloop );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 401;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = Py_None;
        tmp_and_left_value_2 = ( tmp_compexpr_left_4 != tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_4 );
        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        if ( par_kernel == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kernel" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 401;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = par_kernel;
        tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_eventloop );
        if ( tmp_compexpr_left_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 401;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_loop );
        tmp_compexpr_right_5 = var_loop;
        tmp_and_right_value_2 = ( tmp_compexpr_left_5 != tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_5 );
        tmp_and_right_value_1 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_and_right_value_1 = tmp_and_left_value_2;
        and_end_2:;
        tmp_condition_result_5 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_5 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_raise_type_3;
            PyObject *tmp_make_exception_arg_3;
            tmp_make_exception_arg_3 = const_str_digest_b2d60b51f38a7258ac0dfb3c34db5b9c;
            frame_d538afe2a4994bbddb928a1283d3e3fb->m_frame.f_lineno = 402;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_3 };
                tmp_raise_type_3 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
            }

            assert( !(tmp_raise_type_3 == NULL) );
            exception_type = tmp_raise_type_3;
            exception_lineno = 402;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_5:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_loop );
        tmp_assattr_name_1 = var_loop;
        if ( par_kernel == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kernel" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 403;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_1 = par_kernel;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_eventloop, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 403;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d538afe2a4994bbddb928a1283d3e3fb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d538afe2a4994bbddb928a1283d3e3fb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d538afe2a4994bbddb928a1283d3e3fb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d538afe2a4994bbddb928a1283d3e3fb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d538afe2a4994bbddb928a1283d3e3fb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d538afe2a4994bbddb928a1283d3e3fb,
        type_description_1,
        par_gui,
        par_kernel,
        var_e,
        var_loop
    );


    // Release cached frame.
    if ( frame_d538afe2a4994bbddb928a1283d3e3fb == cache_frame_d538afe2a4994bbddb928a1283d3e3fb )
    {
        Py_DECREF( frame_d538afe2a4994bbddb928a1283d3e3fb );
    }
    cache_frame_d538afe2a4994bbddb928a1283d3e3fb = NULL;

    assertFrameObject( frame_d538afe2a4994bbddb928a1283d3e3fb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_21_enable_gui );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_gui );
    Py_DECREF( par_gui );
    par_gui = NULL;

    Py_XDECREF( par_kernel );
    par_kernel = NULL;

    CHECK_OBJECT( (PyObject *)var_loop );
    Py_DECREF( var_loop );
    var_loop = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_gui );
    Py_DECREF( par_gui );
    par_gui = NULL;

    Py_XDECREF( par_kernel );
    par_kernel = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    Py_XDECREF( var_loop );
    var_loop = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$eventloops$$$function_21_enable_gui );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_10_loop_wx_exit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_10_loop_wx_exit,
        const_str_plain_loop_wx_exit,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9ef5d2ec27d9f4286940ad398de57237,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_11_loop_tk(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_11_loop_tk,
        const_str_plain_loop_tk,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_089c561f81035276a66d83f080abe4a3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_7d7c4a4759da263eab1d31f52e3667fa,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_11_loop_tk$$$function_1_process_stream_events(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_11_loop_tk$$$function_1_process_stream_events,
        const_str_plain_process_stream_events,
#if PYTHON_VERSION >= 300
        const_str_digest_2c4c8d69412c28219166b7ab6b355ef6,
#endif
        codeobj_eb819a4b239d753061a6f7a6c8cd87b6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_ad8cb7ff5767f8253cefcb2821b0e89d,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_12_loop_tk_exit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_12_loop_tk_exit,
        const_str_plain_loop_tk_exit,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bcf0b356f68cb06ad5cf5300eb83ba82,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_13_loop_gtk(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_13_loop_gtk,
        const_str_plain_loop_gtk,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a6966745d50d40f1362e86d5fcb0ccd9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_5fc1751031eed6aa9529864cd935023c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_14_loop_gtk_exit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_14_loop_gtk_exit,
        const_str_plain_loop_gtk_exit,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e58cd038bd2e1fa086681096028429f2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_15_loop_gtk3(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_15_loop_gtk3,
        const_str_plain_loop_gtk3,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ba8f45f98f9cdde044b5032bf0075e62,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_5fc1751031eed6aa9529864cd935023c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_16_loop_gtk3_exit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_16_loop_gtk3_exit,
        const_str_plain_loop_gtk3_exit,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bedfe9919dfee0533970a7bfb93739af,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_17_loop_cocoa(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_17_loop_cocoa,
        const_str_plain_loop_cocoa,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1d13d22c75e3ccb389eca1f15497fc26,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_36c984a8ee7390941177662bc489b714,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_17_loop_cocoa$$$function_1_handle_int(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_17_loop_cocoa$$$function_1_handle_int,
        const_str_plain_handle_int,
#if PYTHON_VERSION >= 300
        const_str_digest_84a6e22bb74dc37ec7c374a46500eff1,
#endif
        codeobj_4d51db9e71dac7e5d3872e993acf1eff,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_2d4ee65694e57b933f5856d0c282944c,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_18_loop_cocoa_exit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_18_loop_cocoa_exit,
        const_str_plain_loop_cocoa_exit,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_18411978282a4eead2ffa6a85c3c3ec8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_19_loop_asyncio(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_19_loop_asyncio,
        const_str_plain_loop_asyncio,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2eda2a1510ed33ba9a8d5b7587967ab6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_67e3e3acb75f1a3055eb1098afd63fe5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_19_loop_asyncio$$$function_1_process_stream_events(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_19_loop_asyncio$$$function_1_process_stream_events,
        const_str_plain_process_stream_events,
#if PYTHON_VERSION >= 300
        const_str_digest_8af48764ad8e59ae5784fe3e7c8bbca2,
#endif
        codeobj_c819a73e87849bc6bee0f826b7d6670c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_ad8cb7ff5767f8253cefcb2821b0e89d,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_1__use_appnope(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_1__use_appnope,
        const_str_plain__use_appnope,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_eb9114483babb5cba293d09910cc0e3d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_a71365800d2c40a6537ed3cf18b749d3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_20_loop_asyncio_exit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_20_loop_asyncio_exit,
        const_str_plain_loop_asyncio_exit,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_232eec1d13930f531fd7f1fd0bc255a3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_ed580d2cf2fea27977dfab5a03530f45,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_20_loop_asyncio_exit$$$function_1_close_loop,
        const_str_plain_close_loop,
#if PYTHON_VERSION >= 300
        const_str_digest_68e19db4deb3cfd9bcff9bbba59a16fc,
#endif
        codeobj_e06efd7b945f078458bf864669e9f639,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_21_enable_gui( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_21_enable_gui,
        const_str_plain_enable_gui,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d538afe2a4994bbddb928a1283d3e3fb,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_5b8c27a5a48ef7b8219cedc05addca03,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_2__notify_stream_qt(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_2__notify_stream_qt,
        const_str_plain__notify_stream_qt,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0d616b9dc042b49f480d025cc2d7c3e3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_2__notify_stream_qt$$$function_1_process_stream_events(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_2__notify_stream_qt$$$function_1_process_stream_events,
        const_str_plain_process_stream_events,
#if PYTHON_VERSION >= 300
        const_str_digest_b14ca5a183b199cb4df97fbc116e19b1,
#endif
        codeobj_203dcfe25c3932f8754558dd2fb4869a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_ad8cb7ff5767f8253cefcb2821b0e89d,
        3
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_3_register_integration,
        const_str_plain_register_integration,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1b61d167e0cdc8739dabddcbaf924ee0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_f5f0542cf37066ad9d393b3514b1c31a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator,
        const_str_plain_decorator,
#if PYTHON_VERSION >= 300
        const_str_digest_965aac207295f6aebed813fb543a2cb5,
#endif
        codeobj_7e19846afe5e9d2916c5dbc889506ede,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0f638692e21be1921314f231ba0d5918,
#endif
        codeobj_44d9758b33450b795557bcb678629425,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_2_exit_decorator(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_3_register_integration$$$function_1_decorator$$$function_2_exit_decorator,
        const_str_plain_exit_decorator,
#if PYTHON_VERSION >= 300
        const_str_digest_445cf3bf72607f9eb9f9bebe979fd350,
#endif
        codeobj_6c3782dfac5fe6f28012060808633334,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_039ffb3d0d7bf635ae665cf0b1b4e3f4,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_4__loop_qt(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_4__loop_qt,
        const_str_plain__loop_qt,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c570518f8534bcd077635f7d0a8d980e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_ac333f5dcf9852c64fda641f168e8125,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_5_loop_qt4(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_5_loop_qt4,
        const_str_plain_loop_qt4,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_32c99576c1a7129c4aba701ed0853e09,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_dc4f4e7db006731ca43e14f0afdcb9c0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_6_loop_qt5(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_6_loop_qt5,
        const_str_plain_loop_qt5,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_000e41d9ce9829957831b5b836d0dc9f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_4b7ea5a266a558058bbe98690987425b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_7_loop_qt_exit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_7_loop_qt_exit,
        const_str_plain_loop_qt_exit,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_194db13e896a67e6d6de4d8d750d049a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_8__loop_wx(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_8__loop_wx,
        const_str_plain__loop_wx,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0eaf4d8ea64e3113f2bc3765e8af0dca,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_41c30847a44603e18da0fe3098acb1cb,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_9_loop_wx,
        const_str_plain_loop_wx,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e0045759633cb60da5cf039dffd820f4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_b8c1173120a40eec7a007296f19f6fe0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_1_wake(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_9_loop_wx$$$function_1_wake,
        const_str_plain_wake,
#if PYTHON_VERSION >= 300
        const_str_digest_c14f651ff533bc3e31bd18bcef523a7c,
#endif
        codeobj_985b5f9f962a775ddfc017b24c9d5d9d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        const_str_digest_e2c7751dfcc49ea3b9e68c3deaf94833,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_2___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_9_loop_wx$$$function_2___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_cce2d97ed5b6d1fec07023491fdede4e,
#endif
        codeobj_c64f104a35c606358749dca5cc999162,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_3_on_timer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_9_loop_wx$$$function_3_on_timer,
        const_str_plain_on_timer,
#if PYTHON_VERSION >= 300
        const_str_digest_95d07a8a573117c8a04622ad0ece989a,
#endif
        codeobj_ec60cab8fde565006ea3662ae47508de,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx$$$function_4_OnInit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$eventloops$$$function_9_loop_wx$$$function_4_OnInit,
        const_str_plain_OnInit,
#if PYTHON_VERSION >= 300
        const_str_digest_73c970d07f1cc7f805ecc5b6fc3991d6,
#endif
        codeobj_9de51be6fe5a74046c8ebc9781101da5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$eventloops,
        NULL,
        2
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_ipykernel$eventloops =
{
    PyModuleDef_HEAD_INIT,
    "ipykernel.eventloops",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(ipykernel$eventloops)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(ipykernel$eventloops)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_ipykernel$eventloops );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("ipykernel.eventloops: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("ipykernel.eventloops: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("ipykernel.eventloops: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initipykernel$eventloops" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_ipykernel$eventloops = Py_InitModule4(
        "ipykernel.eventloops",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_ipykernel$eventloops = PyModule_Create( &mdef_ipykernel$eventloops );
#endif

    moduledict_ipykernel$eventloops = MODULE_DICT( module_ipykernel$eventloops );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_ipykernel$eventloops,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_ipykernel$eventloops,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_ipykernel$eventloops,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_ipykernel$eventloops,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_ipykernel$eventloops );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_f0b65ebf32c38a05199c60f79a951b9a, module_ipykernel$eventloops );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_fecfb080a26871ce28075a2556bd2706;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_4a347beb0791a92fa68aec31d137b09d;
        UPDATE_STRING_DICT0( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_fecfb080a26871ce28075a2556bd2706 = MAKE_MODULE_FRAME( codeobj_fecfb080a26871ce28075a2556bd2706, module_ipykernel$eventloops );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_fecfb080a26871ce28075a2556bd2706 );
    assert( Py_REFCNT( frame_fecfb080a26871ce28075a2556bd2706 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_functools;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_partial_tuple;
        tmp_level_name_1 = const_int_0;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 7;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_partial );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_partial, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_os;
        tmp_globals_name_2 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 8;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_sys;
        tmp_globals_name_3 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 9;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        assert( !(tmp_assign_source_6 == NULL) );
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_platform;
        tmp_globals_name_4 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 10;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_platform, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_zmq;
        tmp_globals_name_5 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 12;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_zmq, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_4af59da437d2f21ccb08423e5fb98074;
        tmp_globals_name_6 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_LooseVersion_tuple;
        tmp_level_name_6 = const_int_0;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 14;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_LooseVersion );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_V, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_7103caa43923d1b024b5f303388aff54;
        tmp_globals_name_7 = (PyObject *)moduledict_ipykernel$eventloops;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_Application_tuple;
        tmp_level_name_7 = const_int_0;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 15;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_Application );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_Application, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_ipykernel$eventloops$$$function_1__use_appnope(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain__use_appnope, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_ipykernel$eventloops$$$function_2__notify_stream_qt(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain__notify_stream_qt, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = PyDict_Copy( const_dict_dfc6781eab05c37013adf5c506a72c2c );
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_map, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_ipykernel$eventloops$$$function_3_register_integration(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_register_integration, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_ipykernel$eventloops$$$function_4__loop_qt(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain__loop_qt, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_register_integration );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_integration );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_2 = tmp_mvar_value_3;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 110;
        tmp_called_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_plain_qt4_tuple, 0 ) );

        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = MAKE_FUNCTION_ipykernel$eventloops$$$function_5_loop_qt4(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_qt4, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_called_name_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_register_integration );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_integration );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register_integration" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;

            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 125;
        tmp_called_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_str_plain_qt_str_plain_qt5_tuple, 0 ) );

        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = MAKE_FUNCTION_ipykernel$eventloops$$$function_6_loop_qt5(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 125;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_qt5, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_qt4 );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_qt4 );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "loop_qt4" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;

            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_5;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_exit );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_qt5 );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_qt5 );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "loop_qt5" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_6;
        tmp_args_element_name_4 = MAKE_FUNCTION_ipykernel$eventloops$$$function_7_loop_qt_exit(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 134;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_args_element_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_exit, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 134;

            goto frame_exception_exit_1;
        }
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 133;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_qt_exit, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_ipykernel$eventloops$$$function_8__loop_wx(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain__loop_wx, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_called_name_6;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_register_integration );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_integration );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register_integration" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 151;

            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_7;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 151;
        tmp_called_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_str_plain_wx_tuple, 0 ) );

        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_5 = MAKE_FUNCTION_ipykernel$eventloops$$$function_9_loop_wx(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 151;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_assign_source_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_wx, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_wx );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_wx );
        }

        CHECK_OBJECT( tmp_mvar_value_8 );
        tmp_called_instance_2 = tmp_mvar_value_8;
        tmp_args_element_name_6 = MAKE_FUNCTION_ipykernel$eventloops$$$function_10_loop_wx_exit(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 213;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_assign_source_21 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_exit, call_args );
        }

        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_wx_exit, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_called_name_8;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_register_integration );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_integration );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register_integration" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 219;

            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_9;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 219;
        tmp_called_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_plain_tk_tuple, 0 ) );

        if ( tmp_called_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_7 = MAKE_FUNCTION_ipykernel$eventloops$$$function_11_loop_tk(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 219;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_assign_source_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_called_name_8 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_tk, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_tk );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_tk );
        }

        CHECK_OBJECT( tmp_mvar_value_10 );
        tmp_called_instance_3 = tmp_mvar_value_10;
        tmp_args_element_name_8 = MAKE_FUNCTION_ipykernel$eventloops$$$function_12_loop_tk_exit(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 245;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_assign_source_23 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_exit, call_args );
        }

        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 245;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_tk_exit, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_called_name_10;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_9;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_register_integration );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_integration );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register_integration" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 250;

            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_11;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 250;
        tmp_called_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_str_plain_gtk_tuple, 0 ) );

        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 250;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_9 = MAKE_FUNCTION_ipykernel$eventloops$$$function_13_loop_gtk(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 250;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 250;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_gtk, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_gtk );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_gtk );
        }

        CHECK_OBJECT( tmp_mvar_value_12 );
        tmp_called_instance_4 = tmp_mvar_value_12;
        tmp_args_element_name_10 = MAKE_FUNCTION_ipykernel$eventloops$$$function_14_loop_gtk_exit(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 260;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_assign_source_25 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_exit, call_args );
        }

        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 260;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_gtk_exit, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_called_name_12;
        PyObject *tmp_called_name_13;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_11;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_register_integration );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_integration );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register_integration" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 265;

            goto frame_exception_exit_1;
        }

        tmp_called_name_13 = tmp_mvar_value_13;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 265;
        tmp_called_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_plain_gtk3_tuple, 0 ) );

        if ( tmp_called_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_11 = MAKE_FUNCTION_ipykernel$eventloops$$$function_15_loop_gtk3(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 265;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_assign_source_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
        }

        Py_DECREF( tmp_called_name_12 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_gtk3, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_12;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_gtk3 );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_gtk3 );
        }

        CHECK_OBJECT( tmp_mvar_value_14 );
        tmp_called_instance_5 = tmp_mvar_value_14;
        tmp_args_element_name_12 = MAKE_FUNCTION_ipykernel$eventloops$$$function_16_loop_gtk3_exit(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 275;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_assign_source_27 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_exit, call_args );
        }

        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_gtk3_exit, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_called_name_14;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_args_element_name_13;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_register_integration );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_integration );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register_integration" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 280;

            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_15;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 280;
        tmp_called_name_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_plain_osx_tuple, 0 ) );

        if ( tmp_called_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 280;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_13 = MAKE_FUNCTION_ipykernel$eventloops$$$function_17_loop_cocoa(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 280;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_called_name_14 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 280;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_cocoa, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_args_element_name_14;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_cocoa );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_cocoa );
        }

        CHECK_OBJECT( tmp_mvar_value_16 );
        tmp_called_instance_6 = tmp_mvar_value_16;
        tmp_args_element_name_14 = MAKE_FUNCTION_ipykernel$eventloops$$$function_18_loop_cocoa_exit(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 319;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_assign_source_29 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_exit, call_args );
        }

        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_cocoa_exit, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_called_name_16;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_args_element_name_15;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_register_integration );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_integration );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register_integration" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 325;

            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_17;
        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 325;
        tmp_called_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_plain_asyncio_tuple, 0 ) );

        if ( tmp_called_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 325;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_15 = MAKE_FUNCTION_ipykernel$eventloops$$$function_19_loop_asyncio(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 325;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_assign_source_30 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_called_name_16 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 325;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_asyncio, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_args_element_name_16;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_asyncio );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_loop_asyncio );
        }

        CHECK_OBJECT( tmp_mvar_value_18 );
        tmp_called_instance_7 = tmp_mvar_value_18;
        tmp_args_element_name_16 = MAKE_FUNCTION_ipykernel$eventloops$$$function_20_loop_asyncio_exit(  );



        frame_fecfb080a26871ce28075a2556bd2706->m_frame.f_lineno = 367;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_assign_source_31 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_exit, call_args );
        }

        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 367;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_loop_asyncio_exit, tmp_assign_source_31 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_fecfb080a26871ce28075a2556bd2706 );
#endif
    popFrameStack();

    assertFrameObject( frame_fecfb080a26871ce28075a2556bd2706 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_fecfb080a26871ce28075a2556bd2706 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_fecfb080a26871ce28075a2556bd2706, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_fecfb080a26871ce28075a2556bd2706->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_fecfb080a26871ce28075a2556bd2706, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_32 = MAKE_FUNCTION_ipykernel$eventloops$$$function_21_enable_gui( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_ipykernel$eventloops, (Nuitka_StringObject *)const_str_plain_enable_gui, tmp_assign_source_32 );
    }

    return MOD_RETURN_VALUE( module_ipykernel$eventloops );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
