/* Generated code for Python module 'tornado.util'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_tornado$util" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_tornado$util;
PyDictObject *moduledict_tornado$util;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_8f99bee2489684f691aababdb3f30f4e;
static PyObject *const_str_digest_bbb5efbc373f0147c801e308dfebf5f7;
extern PyObject *const_str_plain_TimeoutError;
static PyObject *const_str_plain_unmasked_arr;
extern PyObject *const_str_plain_TracebackType;
extern PyObject *const_str_plain_index;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_str_plain_TORNADO_EXTENSION_tuple;
extern PyObject *const_str_plain_array;
static PyObject *const_str_digest_c77a45b24a39e6d29e17d3a208233a24;
extern PyObject *const_str_plain___name__;
static PyObject *const_tuple_b3b1bf409412ba58beb1be52f2215c54_tuple;
extern PyObject *const_str_plain_initialize;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_dict_056a293e2058d56276328e53ff09a8b9;
extern PyObject *const_str_plain__websocket_mask;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_cls_tuple;
extern PyObject *const_str_plain_0;
extern PyObject *const_str_plain_group;
static PyObject *const_tuple_str_plain_self_str_plain_value_str_plain_max_length_tuple;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain_bytes;
extern PyObject *const_str_digest_574ab65c4003836f8d427db97abdb817;
static PyObject *const_tuple_str_plain_exc_info_tuple;
static PyObject *const_str_digest_47bc36a6ced69c6c9d22df57da5ecc74;
extern PyObject *const_str_plain_default;
extern PyObject *const_str_digest_f18fcfc7c7c8b1a40c9dd32185e7a4c4;
extern PyObject *const_str_plain_exec_in;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_Callable;
static PyObject *const_str_plain_mask_arr;
extern PyObject *const_str_plain_configurable_default;
static PyObject *const_str_digest_fd037e2200e52621e5904f73e2f65817;
extern PyObject *const_tuple_str_plain_s_tuple;
extern PyObject *const_str_plain_td;
extern PyObject *const_str_plain_instance;
static PyObject *const_tuple_str_plain__Configurable__impl_class_tuple;
extern PyObject *const_str_digest_e077944e15accbec54ecb40fd81dafde;
extern PyObject *const_str_plain_sub;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_Optional;
static PyObject *const_str_digest_a450a944395eb8127f5d662fed728c44;
extern PyObject *const_str_plain_Dict;
extern PyObject *const_str_plain_return;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_saved;
extern PyObject *const_str_plain_classmethod;
extern PyObject *const_str_plain_List;
static PyObject *const_str_plain__getargnames;
extern PyObject *const_str_plain_cls;
extern PyObject *const_str_plain_func;
extern PyObject *const_str_plain_glob;
extern PyObject *const_str_plain_flush;
extern PyObject *const_str_plain_join;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_int_pos_16;
extern PyObject *const_str_plain_typing;
extern PyObject *const_str_digest_1b4769834b8adf886823f16c27fff0a0;
static PyObject *const_str_plain__re_unescape_replacement;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain__Configurable__impl_class;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_dot;
static PyObject *const_dict_70add3a5fe8092827ca301f27493d991;
extern PyObject *const_str_plain_Match;
extern PyObject *const_str_plain_errno_from_exception;
static PyObject *const_tuple_str_plain_match_str_plain_group_tuple;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_tuple_str_plain_code_str_plain_glob_str_plain_loc_tuple;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_errno;
static PyObject *const_str_plain_TORNADO_NO_EXTENSION;
extern PyObject *const_str_plain_environ;
extern PyObject *const_str_plain_data;
static PyObject *const_str_plain_arg_pos;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_plain_str;
static PyObject *const_tuple_str_plain_is_finalizing_tuple;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain__restore_configuration;
static PyObject *const_str_plain__alphanum;
extern PyObject *const_str_plain___setattr__;
extern PyObject *const_str_plain_MAX_WBITS;
static PyObject *const_tuple_84febd244317fb811a69dca764de7038_tuple;
static PyObject *const_str_digest_a57c123a560c6211ae39bbf72ce5c737;
static PyObject *const_str_digest_ade23dbff600199e604f0d0ba80bf77d;
static PyObject *const_str_plain_is_finalizing;
extern PyObject *const_str_plain_func_code;
extern PyObject *const_str_plain_int;
extern PyObject *const_str_plain_value;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_old_value;
extern PyObject *const_str_plain_Tuple;
static PyObject *const_str_digest_79dedeb07d6730dcdd470d61ac4804cc;
extern PyObject *const_str_plain_TYPE_CHECKING;
static PyObject *const_str_digest_aa8e14ce57bf148b353f4b6d303a0453;
extern PyObject *const_str_plain_unittest;
static PyObject *const_str_plain_get_old_value;
extern PyObject *const_str_plain_re_unescape;
extern PyObject *const_str_plain_e;
static PyObject *const_str_digest_fadf4c2caad38d1b66f5de5591ac5ff3;
static PyObject *const_str_digest_cc6c48138d09feaa33845c6570d39498;
extern PyObject *const_str_plain_decompress;
extern PyObject *const_str_plain_loc;
extern PyObject *const_str_plain_parts;
extern PyObject *const_str_plain_mask;
extern PyObject *const_tuple_str_dot_tuple;
extern PyObject *const_str_plain_Type;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_impl;
extern PyObject *const_str_plain___getattr__;
static PyObject *const_str_digest_3ab6a951884266fa8a14bc07798b51f6;
extern PyObject *const_str_plain_issubclass;
extern PyObject *const_str_plain_datetime;
extern PyObject *const_str_digest_ebcdac424df8af7b8c4b22d8cd1278fe;
static PyObject *const_str_plain_ArgReplacer;
extern PyObject *const_str_plain_name;
static PyObject *const_str_digest_cc1cb68a0f40a41e31766338816a6f4c;
extern PyObject *const_str_digest_5a48d4a73c11265f4ad8a7c657f1c1a7;
extern PyObject *const_tuple_str_plain_cls_str_plain_impl_str_plain_kwargs_str_plain_base_tuple;
extern PyObject *const_str_plain_zlib;
extern PyObject *const_str_plain_Any;
extern PyObject *const_tuple_str_plain_getfullargspec_tuple;
extern PyObject *const_tuple_str_plain_self_str_plain_name_str_plain_value_tuple;
extern PyObject *const_str_plain_compile;
extern PyObject *const_str_plain_decompressobj;
extern PyObject *const_str_plain_split;
extern PyObject *const_tuple_str_plain_e_tuple;
extern PyObject *const_str_plain_match;
extern PyObject *const_str_digest_19c968eae53f4211f9d52acdbf25a2db;
extern PyObject *const_tuple_int_0_tuple;
static PyObject *const_str_digest_e20c1a587c95de70a21e32f7935d9619;
extern PyObject *const_str_digest_ddc876bf1486902775cf08194098942b;
static PyObject *const_tuple_88b55cc351f0865b38898b5650856ad1_tuple;
static PyObject *const_str_digest_935608fc17ea27c1bbd653d406b87466;
static PyObject *const_str_plain_websocket_mask;
extern PyObject *const_str_digest_d98e06b0a1609bcfa4ac9ce4e8c2e123;
extern PyObject *const_str_plain_False;
static PyObject *const_str_digest_2f9db9dd8f2cd34d9565b9b0d7c0e8d8;
extern PyObject *const_str_plain_bytes_type;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_init_kwargs;
extern PyObject *const_str_plain___new__;
extern PyObject *const_str_plain_count;
extern PyObject *const_str_digest_935bde0817e8592d5b39269641a93bc0;
extern PyObject *const_int_0;
extern PyObject *const_tuple_41aed66016e3cd5a32ef4f0c8fd91702_tuple;
extern PyObject *const_str_plain_Sequence;
static PyObject *const_str_digest_f8bf07e5367861fc790a9ef86cc7c733;
extern PyObject *const_str_plain_code;
extern PyObject *const_str_plain_unconsumed_tail;
static PyObject *const_str_digest_b4b0675d44b07c3b99d7c6d699ec8a1d;
static PyObject *const_tuple_str_plain_websocket_mask_tuple;
extern PyObject *const_tuple_str_plain_TracebackType_tuple;
extern PyObject *const_str_plain_co_argcount;
extern PyObject *const_str_plain_import_object;
static PyObject *const_str_digest_626dd83f20da33c799a1173cc02257ff;
extern PyObject *const_str_plain_basestring_type;
extern PyObject *const_str_plain_B;
extern PyObject *const_str_plain_origin;
extern PyObject *const_tuple_str_plain_self_str_plain_name_tuple;
static PyObject *const_tuple_f22aadd742848050d71087ffa4e123ea_tuple;
extern PyObject *const_str_plain_doctest;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_ObjectDict;
extern PyObject *const_str_plain_exc_info;
extern PyObject *const_str_plain_base;
static PyObject *const_str_digest_dfbc515db61895e3c8b945f78f7aee54;
static PyObject *const_str_plain__re_unescape_pattern;
extern PyObject *const_int_pos_4;
extern PyObject *const_str_plain_cast;
extern PyObject *const_str_plain_type;
static PyObject *const_tuple_str_plain_Union_tuple;
extern PyObject *const_str_plain_getfullargspec;
extern PyObject *const_str_plain_unicode_type;
extern PyObject *const_str_plain_timedelta_to_seconds;
static PyObject *const_tuple_str_plain_TORNADO_NO_EXTENSION_tuple;
extern PyObject *const_str_plain_configurable_base;
extern PyObject *const_tuple_str_plain_name_str_plain_parts_str_plain_obj_tuple;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_max_length;
static PyObject *const_tuple_str_plain_self_str_plain_func_str_plain_code_tuple;
extern PyObject *const_str_plain_Union;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_tuple_type_Exception_tuple;
extern PyObject *const_str_plain_raise_exc_info;
extern PyObject *const_str_plain___module__;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain_GzipDecompressor;
static PyObject *const_frozenset_17f37ed7ef1fa0685d5320e9c039ff8d;
extern PyObject *const_str_plain_sys;
static PyObject *const_str_digest_9f60da2cb4b1130e5280ffc6e52b6956;
extern PyObject *const_str_digest_dbe92e62e5d9e0a6a62a95576c758970;
extern PyObject *const_str_digest_81ac163e5e6bb2013de6b611a30e2aa4;
extern PyObject *const_str_plain_doctests;
extern PyObject *const_slice_none_int_neg_1_none;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_update;
extern PyObject *const_str_plain_replace;
extern PyObject *const_tuple_str_plain_td_tuple;
extern PyObject *const_str_plain_DocTestSuite;
extern PyObject *const_str_plain_with_traceback;
extern PyObject *const_str_plain__initialize;
extern PyObject *const_str_plain_1;
extern PyObject *const_str_plain_Mapping;
extern PyObject *const_tuple_str_plain_cls_str_plain_saved_str_plain_base_tuple;
extern PyObject *const_str_plain_Configurable;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_types;
static PyObject *const_str_digest_6bd7cca3362e253efd8bcd5e7df1f748;
extern PyObject *const_str_digest_7a2c19df49e284f40d3fa8d5fbd4afce;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain__websocket_mask_python;
extern PyObject *const_str_plain_inspect;
extern PyObject *const_str_plain_total_seconds;
static PyObject *const_str_digest_3c8ff2d5d3f005bbdbf00d3f7622174c;
extern PyObject *const_str_plain_co_varnames;
static PyObject *const_str_digest_055f1f027909dea18d22a0d894d1dccd;
static PyObject *const_str_digest_6f69bd8b8ae3e2f5791f34f2b5329e8c;
extern PyObject *const_str_plain_configured_class;
extern PyObject *const_str_plain__Configurable__impl_kwargs;
static PyObject *const_str_digest_c68e49781bd27a934da377701a611f69;
extern PyObject *const_str_plain_DOTALL;
extern PyObject *const_tuple_str_plain_doctest_tuple;
extern PyObject *const_tuple_str_plain_cls_str_plain_base_tuple;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_get;
static PyObject *const_str_digest_e64394b644787251958d63ad9f861876;
extern PyObject *const_str_plain_has_location;
static PyObject *const_tuple_str_plain_self_str_plain_func_str_plain_name_tuple;
static PyObject *const_str_digest_82535f6c4ce38866e752a9b4363412df;
static PyObject *const_str_digest_20c0dd37e7ef89358b9d393e4e00b9a6;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_digest_829c095ccd3a9904c416c67e20611573;
extern PyObject *const_str_plain_obj;
static PyObject *const_str_digest_2cbd99f500eda850728e88ec283dd8e7;
extern PyObject *const_str_plain_tobytes;
extern PyObject *const_str_plain_property;
extern PyObject *const_str_plain_atexit;
extern PyObject *const_str_plain_configure;
extern PyObject *const_str_angle_string;
static PyObject *const_str_digest_89b117bb54827b46396fd05fd8c8f20b;
extern PyObject *const_str_plain__save_configuration;
extern PyObject *const_str_plain_new_value;
static PyObject *const_str_digest_9e1addef3003778e105e2f66316a89b9;
extern PyObject *const_dict_f12d39973b0594a0adb574dbe1e8f5f7;
static PyObject *const_str_plain_TORNADO_EXTENSION;
extern PyObject *const_str_digest_88a6dfa9fdff870b7640d663cf05e5b9;
extern PyObject *const_str_plain_exec;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_8f99bee2489684f691aababdb3f30f4e = UNSTREAM_STRING_ASCII( &constant_bin[ 5514878 ], 259, 0 );
    const_str_digest_bbb5efbc373f0147c801e308dfebf5f7 = UNSTREAM_STRING_ASCII( &constant_bin[ 5515137 ], 22, 0 );
    const_str_plain_unmasked_arr = UNSTREAM_STRING_ASCII( &constant_bin[ 5515159 ], 12, 1 );
    const_tuple_str_plain_TORNADO_EXTENSION_tuple = PyTuple_New( 1 );
    const_str_plain_TORNADO_EXTENSION = UNSTREAM_STRING_ASCII( &constant_bin[ 5515171 ], 17, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_TORNADO_EXTENSION_tuple, 0, const_str_plain_TORNADO_EXTENSION ); Py_INCREF( const_str_plain_TORNADO_EXTENSION );
    const_str_digest_c77a45b24a39e6d29e17d3a208233a24 = UNSTREAM_STRING_ASCII( &constant_bin[ 5515188 ], 16, 0 );
    const_tuple_b3b1bf409412ba58beb1be52f2215c54_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_b3b1bf409412ba58beb1be52f2215c54_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_b3b1bf409412ba58beb1be52f2215c54_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_b3b1bf409412ba58beb1be52f2215c54_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_b3b1bf409412ba58beb1be52f2215c54_tuple, 3, const_str_plain_default ); Py_INCREF( const_str_plain_default );
    const_tuple_str_plain_self_str_plain_value_str_plain_max_length_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_max_length_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_max_length_tuple, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_max_length_tuple, 2, const_str_plain_max_length ); Py_INCREF( const_str_plain_max_length );
    const_tuple_str_plain_exc_info_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_exc_info_tuple, 0, const_str_plain_exc_info ); Py_INCREF( const_str_plain_exc_info );
    const_str_digest_47bc36a6ced69c6c9d22df57da5ecc74 = UNSTREAM_STRING_ASCII( &constant_bin[ 5515204 ], 222, 0 );
    const_str_plain_mask_arr = UNSTREAM_STRING_ASCII( &constant_bin[ 5515426 ], 8, 1 );
    const_str_digest_fd037e2200e52621e5904f73e2f65817 = UNSTREAM_STRING_ASCII( &constant_bin[ 5515434 ], 26, 0 );
    const_tuple_str_plain__Configurable__impl_class_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__Configurable__impl_class_tuple, 0, const_str_plain__Configurable__impl_class ); Py_INCREF( const_str_plain__Configurable__impl_class );
    const_str_digest_a450a944395eb8127f5d662fed728c44 = UNSTREAM_STRING_ASCII( &constant_bin[ 5515460 ], 404, 0 );
    const_str_plain__getargnames = UNSTREAM_STRING_ASCII( &constant_bin[ 5515864 ], 12, 1 );
    const_str_plain__re_unescape_replacement = UNSTREAM_STRING_ASCII( &constant_bin[ 5515876 ], 24, 1 );
    const_dict_70add3a5fe8092827ca301f27493d991 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_70add3a5fe8092827ca301f27493d991, const_str_plain_mask, (PyObject *)&PyBytes_Type );
    PyDict_SetItem( const_dict_70add3a5fe8092827ca301f27493d991, const_str_plain_data, (PyObject *)&PyBytes_Type );
    PyDict_SetItem( const_dict_70add3a5fe8092827ca301f27493d991, const_str_plain_return, (PyObject *)&PyBytes_Type );
    assert( PyDict_Size( const_dict_70add3a5fe8092827ca301f27493d991 ) == 3 );
    const_tuple_str_plain_match_str_plain_group_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_match_str_plain_group_tuple, 0, const_str_plain_match ); Py_INCREF( const_str_plain_match );
    PyTuple_SET_ITEM( const_tuple_str_plain_match_str_plain_group_tuple, 1, const_str_plain_group ); Py_INCREF( const_str_plain_group );
    const_tuple_str_plain_code_str_plain_glob_str_plain_loc_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_code_str_plain_glob_str_plain_loc_tuple, 0, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_str_plain_code_str_plain_glob_str_plain_loc_tuple, 1, const_str_plain_glob ); Py_INCREF( const_str_plain_glob );
    PyTuple_SET_ITEM( const_tuple_str_plain_code_str_plain_glob_str_plain_loc_tuple, 2, const_str_plain_loc ); Py_INCREF( const_str_plain_loc );
    const_str_plain_TORNADO_NO_EXTENSION = UNSTREAM_STRING_ASCII( &constant_bin[ 5515900 ], 20, 1 );
    const_str_plain_arg_pos = UNSTREAM_STRING_ASCII( &constant_bin[ 5515920 ], 7, 1 );
    const_tuple_str_plain_is_finalizing_tuple = PyTuple_New( 1 );
    const_str_plain_is_finalizing = UNSTREAM_STRING_ASCII( &constant_bin[ 5515927 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_is_finalizing_tuple, 0, const_str_plain_is_finalizing ); Py_INCREF( const_str_plain_is_finalizing );
    const_str_plain__alphanum = UNSTREAM_STRING_ASCII( &constant_bin[ 5515940 ], 9, 1 );
    const_tuple_84febd244317fb811a69dca764de7038_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 0, const_str_plain_Any ); Py_INCREF( const_str_plain_Any );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 1, const_str_plain_Optional ); Py_INCREF( const_str_plain_Optional );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 2, const_str_plain_Dict ); Py_INCREF( const_str_plain_Dict );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 3, const_str_plain_Mapping ); Py_INCREF( const_str_plain_Mapping );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 4, const_str_plain_List ); Py_INCREF( const_str_plain_List );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 5, const_str_plain_Tuple ); Py_INCREF( const_str_plain_Tuple );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 6, const_str_plain_Match ); Py_INCREF( const_str_plain_Match );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 7, const_str_plain_Callable ); Py_INCREF( const_str_plain_Callable );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 8, const_str_plain_Type ); Py_INCREF( const_str_plain_Type );
    PyTuple_SET_ITEM( const_tuple_84febd244317fb811a69dca764de7038_tuple, 9, const_str_plain_Sequence ); Py_INCREF( const_str_plain_Sequence );
    const_str_digest_a57c123a560c6211ae39bbf72ce5c737 = UNSTREAM_STRING_ASCII( &constant_bin[ 5515949 ], 22, 0 );
    const_str_digest_ade23dbff600199e604f0d0ba80bf77d = UNSTREAM_STRING_ASCII( &constant_bin[ 5515971 ], 1066, 0 );
    const_str_digest_79dedeb07d6730dcdd470d61ac4804cc = UNSTREAM_STRING_ASCII( &constant_bin[ 5517037 ], 32, 0 );
    const_str_digest_aa8e14ce57bf148b353f4b6d303a0453 = UNSTREAM_STRING_ASCII( &constant_bin[ 5517069 ], 201, 0 );
    const_str_plain_get_old_value = UNSTREAM_STRING_ASCII( &constant_bin[ 5517270 ], 13, 1 );
    const_str_digest_fadf4c2caad38d1b66f5de5591ac5ff3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5517283 ], 22, 0 );
    const_str_digest_cc6c48138d09feaa33845c6570d39498 = UNSTREAM_STRING_ASCII( &constant_bin[ 5517305 ], 247, 0 );
    const_str_digest_3ab6a951884266fa8a14bc07798b51f6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5517552 ], 215, 0 );
    const_str_plain_ArgReplacer = UNSTREAM_STRING_ASCII( &constant_bin[ 5517767 ], 11, 1 );
    const_str_digest_cc1cb68a0f40a41e31766338816a6f4c = UNSTREAM_STRING_ASCII( &constant_bin[ 5517778 ], 27, 0 );
    const_str_digest_e20c1a587c95de70a21e32f7935d9619 = UNSTREAM_STRING_ASCII( &constant_bin[ 5517805 ], 15, 0 );
    const_tuple_88b55cc351f0865b38898b5650856ad1_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_88b55cc351f0865b38898b5650856ad1_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_88b55cc351f0865b38898b5650856ad1_tuple, 1, const_str_plain_new_value ); Py_INCREF( const_str_plain_new_value );
    PyTuple_SET_ITEM( const_tuple_88b55cc351f0865b38898b5650856ad1_tuple, 2, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_88b55cc351f0865b38898b5650856ad1_tuple, 3, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_88b55cc351f0865b38898b5650856ad1_tuple, 4, const_str_plain_old_value ); Py_INCREF( const_str_plain_old_value );
    const_str_digest_935608fc17ea27c1bbd653d406b87466 = UNSTREAM_STRING_ASCII( &constant_bin[ 5517820 ], 24, 0 );
    const_str_plain_websocket_mask = UNSTREAM_STRING_ASCII( &constant_bin[ 5517844 ], 14, 1 );
    const_str_digest_2f9db9dd8f2cd34d9565b9b0d7c0e8d8 = UNSTREAM_STRING_ASCII( &constant_bin[ 5517858 ], 536, 0 );
    const_str_digest_f8bf07e5367861fc790a9ef86cc7c733 = UNSTREAM_STRING_ASCII( &constant_bin[ 5518394 ], 181, 0 );
    const_str_digest_b4b0675d44b07c3b99d7c6d699ec8a1d = UNSTREAM_STRING_ASCII( &constant_bin[ 5518575 ], 24, 0 );
    const_tuple_str_plain_websocket_mask_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_websocket_mask_tuple, 0, const_str_plain_websocket_mask ); Py_INCREF( const_str_plain_websocket_mask );
    const_str_digest_626dd83f20da33c799a1173cc02257ff = UNSTREAM_STRING_ASCII( &constant_bin[ 5518599 ], 21, 0 );
    const_tuple_f22aadd742848050d71087ffa4e123ea_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_f22aadd742848050d71087ffa4e123ea_tuple, 0, const_str_plain_mask ); Py_INCREF( const_str_plain_mask );
    PyTuple_SET_ITEM( const_tuple_f22aadd742848050d71087ffa4e123ea_tuple, 1, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_f22aadd742848050d71087ffa4e123ea_tuple, 2, const_str_plain_mask_arr ); Py_INCREF( const_str_plain_mask_arr );
    PyTuple_SET_ITEM( const_tuple_f22aadd742848050d71087ffa4e123ea_tuple, 3, const_str_plain_unmasked_arr ); Py_INCREF( const_str_plain_unmasked_arr );
    PyTuple_SET_ITEM( const_tuple_f22aadd742848050d71087ffa4e123ea_tuple, 4, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    const_str_digest_dfbc515db61895e3c8b945f78f7aee54 = UNSTREAM_STRING_ASCII( &constant_bin[ 5518620 ], 49, 0 );
    const_str_plain__re_unescape_pattern = UNSTREAM_STRING_ASCII( &constant_bin[ 5518669 ], 20, 1 );
    const_tuple_str_plain_Union_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Union_tuple, 0, const_str_plain_Union ); Py_INCREF( const_str_plain_Union );
    const_tuple_str_plain_TORNADO_NO_EXTENSION_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_TORNADO_NO_EXTENSION_tuple, 0, const_str_plain_TORNADO_NO_EXTENSION ); Py_INCREF( const_str_plain_TORNADO_NO_EXTENSION );
    const_tuple_str_plain_self_str_plain_func_str_plain_code_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_func_str_plain_code_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_func_str_plain_code_tuple, 1, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_func_str_plain_code_tuple, 2, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    const_frozenset_17f37ed7ef1fa0685d5320e9c039ff8d = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 5518689 ], 191 );
    const_str_digest_9f60da2cb4b1130e5280ffc6e52b6956 = UNSTREAM_STRING_ASCII( &constant_bin[ 5518880 ], 64, 0 );
    const_str_digest_6bd7cca3362e253efd8bcd5e7df1f748 = UNSTREAM_STRING_ASCII( &constant_bin[ 5518944 ], 75, 0 );
    const_str_plain__websocket_mask_python = UNSTREAM_STRING_ASCII( &constant_bin[ 5519019 ], 22, 1 );
    const_str_digest_3c8ff2d5d3f005bbdbf00d3f7622174c = UNSTREAM_STRING_ASCII( &constant_bin[ 5519041 ], 135, 0 );
    const_str_digest_055f1f027909dea18d22a0d894d1dccd = UNSTREAM_STRING_ASCII( &constant_bin[ 5519176 ], 39, 0 );
    const_str_digest_6f69bd8b8ae3e2f5791f34f2b5329e8c = UNSTREAM_STRING_ASCII( &constant_bin[ 5519215 ], 22, 0 );
    const_str_digest_c68e49781bd27a934da377701a611f69 = UNSTREAM_STRING_ASCII( &constant_bin[ 5519237 ], 337, 0 );
    const_str_digest_e64394b644787251958d63ad9f861876 = UNSTREAM_STRING_ASCII( &constant_bin[ 5519574 ], 25, 0 );
    const_tuple_str_plain_self_str_plain_func_str_plain_name_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_func_str_plain_name_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_func_str_plain_name_tuple, 1, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_func_str_plain_name_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_digest_82535f6c4ce38866e752a9b4363412df = UNSTREAM_STRING_ASCII( &constant_bin[ 5519599 ], 437, 0 );
    const_str_digest_20c0dd37e7ef89358b9d393e4e00b9a6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5520036 ], 20, 0 );
    const_str_digest_2cbd99f500eda850728e88ec283dd8e7 = UNSTREAM_STRING_ASCII( &constant_bin[ 5520056 ], 19, 0 );
    const_str_digest_89b117bb54827b46396fd05fd8c8f20b = UNSTREAM_STRING_ASCII( &constant_bin[ 5520075 ], 5, 0 );
    const_str_digest_9e1addef3003778e105e2f66316a89b9 = UNSTREAM_STRING_ASCII( &constant_bin[ 5520080 ], 25, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_tornado$util( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_a9082c6ea4640ddf3d0abc86d3d62007;
static PyCodeObject *codeobj_0fc66817e130a11d02ef07ff3a593999;
static PyCodeObject *codeobj_379191b0739aade14519ed48ff03178b;
static PyCodeObject *codeobj_e425237ef3be6f1f62689b31b6ae3b2a;
static PyCodeObject *codeobj_8669ad0f885f7ec8b6e63b5e494f408d;
static PyCodeObject *codeobj_f38be7bf9b09aa9609f3600214426748;
static PyCodeObject *codeobj_6ac873c49303f625aa5136e5b23e17e9;
static PyCodeObject *codeobj_63b6152ad6827cdf3d3c6b3cfbf3e2c0;
static PyCodeObject *codeobj_a2ed4f3e006fcf9e294bc18f3c4f4149;
static PyCodeObject *codeobj_a3b46041ab19d64777912bcb7918b623;
static PyCodeObject *codeobj_723fc94d060ebeac9cdffa9728b5fd1c;
static PyCodeObject *codeobj_05f3e7ba7b4a8c405a6dca3b3493902a;
static PyCodeObject *codeobj_1b00f2100869eb9199ddf9717b4f1ce3;
static PyCodeObject *codeobj_6410c089076f8efe977f5e67b4a772e1;
static PyCodeObject *codeobj_09ff696e4dd27517039c78810447018d;
static PyCodeObject *codeobj_0c70212ce02c85a2b782370c4930fb55;
static PyCodeObject *codeobj_84384e4a4872c31258d50896b891c2f1;
static PyCodeObject *codeobj_980fe3ca36fafcffb6ecb31e32e0123e;
static PyCodeObject *codeobj_d4e9a1d0a4784dfbe1b1369f4150d79e;
static PyCodeObject *codeobj_e7efdb96952213371a786ae45a9ca0b2;
static PyCodeObject *codeobj_f2605a13068cc4eeec560fbbb234fd45;
static PyCodeObject *codeobj_31d78de1af917a9cbaa6954c3648fb9e;
static PyCodeObject *codeobj_cd092f1f8d845da96187aab3d6b2f9d1;
static PyCodeObject *codeobj_09bba29479aa3dd8961d4c90ebf41215;
static PyCodeObject *codeobj_b8281247d40d9b17e45d20a264446d50;
static PyCodeObject *codeobj_7c9ad147041b211cf995a6f84c911a2c;
static PyCodeObject *codeobj_eb34eda37ebba44ab9440b9f65023306;
static PyCodeObject *codeobj_5c05df6f3541e877d3f2485f49d7fe72;
static PyCodeObject *codeobj_257d45a16f36f6acf4b1c6d6951f102e;
static PyCodeObject *codeobj_d97dd586f751fcc872be542c19f9ebae;
static PyCodeObject *codeobj_a6832ba0c6dc6b531750fcc2b37dc5d8;
static PyCodeObject *codeobj_b7907cccddd37366ccb6e47ac66fb87e;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_e20c1a587c95de70a21e32f7935d9619 );
    codeobj_a9082c6ea4640ddf3d0abc86d3d62007 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_626dd83f20da33c799a1173cc02257ff, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_0fc66817e130a11d02ef07ff3a593999 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ArgReplacer, 366, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_379191b0739aade14519ed48ff03178b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Configurable, 231, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_e425237ef3be6f1f62689b31b6ae3b2a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_GzipDecompressor, 90, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_8669ad0f885f7ec8b6e63b5e494f408d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ObjectDict, 76, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_f38be7bf9b09aa9609f3600214426748 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___getattr__, 80, const_tuple_str_plain_self_str_plain_name_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6ac873c49303f625aa5136e5b23e17e9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 97, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_63b6152ad6827cdf3d3c6b3cfbf3e2c0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 374, const_tuple_str_plain_self_str_plain_func_str_plain_name_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a2ed4f3e006fcf9e294bc18f3c4f4149 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___new__, 269, const_tuple_41aed66016e3cd5a32ef4f0c8fd91702_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
    codeobj_a3b46041ab19d64777912bcb7918b623 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___setattr__, 86, const_tuple_str_plain_self_str_plain_name_str_plain_value_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_723fc94d060ebeac9cdffa9728b5fd1c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__getargnames, 382, const_tuple_str_plain_self_str_plain_func_str_plain_code_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_05f3e7ba7b4a8c405a6dca3b3493902a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__initialize, 307, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE );
    codeobj_1b00f2100869eb9199ddf9717b4f1ce3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__re_unescape_replacement, 209, const_tuple_str_plain_match_str_plain_group_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6410c089076f8efe977f5e67b4a772e1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__restore_configuration, 358, const_tuple_str_plain_cls_str_plain_saved_str_plain_base_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_09ff696e4dd27517039c78810447018d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__save_configuration, 352, const_tuple_str_plain_cls_str_plain_base_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0c70212ce02c85a2b782370c4930fb55 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__websocket_mask_python, 439, const_tuple_f22aadd742848050d71087ffa4e123ea_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_84384e4a4872c31258d50896b891c2f1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_configurable_base, 289, const_tuple_str_plain_cls_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_980fe3ca36fafcffb6ecb31e32e0123e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_configurable_default, 301, const_tuple_str_plain_cls_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d4e9a1d0a4784dfbe1b1369f4150d79e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_configure, 319, const_tuple_str_plain_cls_str_plain_impl_str_plain_kwargs_str_plain_base_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_e7efdb96952213371a786ae45a9ca0b2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_configured_class, 336, const_tuple_str_plain_cls_str_plain_base_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f2605a13068cc4eeec560fbbb234fd45 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_decompress, 103, const_tuple_str_plain_self_str_plain_value_str_plain_max_length_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_31d78de1af917a9cbaa6954c3648fb9e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_doctests, 468, const_tuple_str_plain_doctest_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cd092f1f8d845da96187aab3d6b2f9d1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_errno_from_exception, 188, const_tuple_str_plain_e_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_09bba29479aa3dd8961d4c90ebf41215 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_exec_in, 160, const_tuple_str_plain_code_str_plain_glob_str_plain_loc_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b8281247d40d9b17e45d20a264446d50 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_flush, 122, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7c9ad147041b211cf995a6f84c911a2c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_old_value, 397, const_tuple_b3b1bf409412ba58beb1be52f2215c54_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_eb34eda37ebba44ab9440b9f65023306 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_import_object, 131, const_tuple_str_plain_name_str_plain_parts_str_plain_obj_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5c05df6f3541e877d3f2485f49d7fe72 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_raise_exc_info, 168, const_tuple_str_plain_exc_info_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_257d45a16f36f6acf4b1c6d6951f102e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_re_unescape, 219, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d97dd586f751fcc872be542c19f9ebae = MAKE_CODEOBJ( module_filename_obj, const_str_plain_replace, 409, const_tuple_88b55cc351f0865b38898b5650856ad1_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a6832ba0c6dc6b531750fcc2b37dc5d8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_timedelta_to_seconds, 433, const_tuple_str_plain_td_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b7907cccddd37366ccb6e47ac66fb87e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unconsumed_tail, 116, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_10_raise_exc_info(  );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_11_errno_from_exception( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_12__re_unescape_replacement( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_13_re_unescape( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_14___new__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_15_configurable_base(  );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_16_configurable_default(  );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_17__initialize( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_18_configure(  );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_19_configured_class(  );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_20__save_configuration(  );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_21__restore_configuration(  );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_22___init__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_23__getargnames( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_24_get_old_value( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_25_replace( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_26_timedelta_to_seconds(  );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_27__websocket_mask_python( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_28_doctests(  );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_2___getattr__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_3___setattr__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_4___init__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_5_decompress( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_6_unconsumed_tail( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_7_flush( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_8_import_object( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$util$$$function_9_exec_in( PyObject *defaults, PyObject *annotations );


// The module function definitions.
static PyObject *impl_tornado$util$$$function_2___getattr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_f38be7bf9b09aa9609f3600214426748;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_f38be7bf9b09aa9609f3600214426748 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f38be7bf9b09aa9609f3600214426748, codeobj_f38be7bf9b09aa9609f3600214426748, module_tornado$util, sizeof(void *)+sizeof(void *) );
    frame_f38be7bf9b09aa9609f3600214426748 = cache_frame_f38be7bf9b09aa9609f3600214426748;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f38be7bf9b09aa9609f3600214426748 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f38be7bf9b09aa9609f3600214426748 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_subscribed_name_1 = par_self;
        CHECK_OBJECT( par_name );
        tmp_subscript_name_1 = par_name;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_2___getattr__ );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_f38be7bf9b09aa9609f3600214426748, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_f38be7bf9b09aa9609f3600214426748, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_name );
            tmp_make_exception_arg_1 = par_name;
            frame_f38be7bf9b09aa9609f3600214426748->m_frame.f_lineno = 84;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AttributeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 84;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 81;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_f38be7bf9b09aa9609f3600214426748->m_frame) frame_f38be7bf9b09aa9609f3600214426748->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_2___getattr__ );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f38be7bf9b09aa9609f3600214426748 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f38be7bf9b09aa9609f3600214426748 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f38be7bf9b09aa9609f3600214426748 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f38be7bf9b09aa9609f3600214426748, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f38be7bf9b09aa9609f3600214426748->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f38be7bf9b09aa9609f3600214426748, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f38be7bf9b09aa9609f3600214426748,
        type_description_1,
        par_self,
        par_name
    );


    // Release cached frame.
    if ( frame_f38be7bf9b09aa9609f3600214426748 == cache_frame_f38be7bf9b09aa9609f3600214426748 )
    {
        Py_DECREF( frame_f38be7bf9b09aa9609f3600214426748 );
    }
    cache_frame_f38be7bf9b09aa9609f3600214426748 = NULL;

    assertFrameObject( frame_f38be7bf9b09aa9609f3600214426748 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_2___getattr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_2___getattr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_3___setattr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *par_value = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_a3b46041ab19d64777912bcb7918b623;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a3b46041ab19d64777912bcb7918b623 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a3b46041ab19d64777912bcb7918b623, codeobj_a3b46041ab19d64777912bcb7918b623, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a3b46041ab19d64777912bcb7918b623 = cache_frame_a3b46041ab19d64777912bcb7918b623;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a3b46041ab19d64777912bcb7918b623 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a3b46041ab19d64777912bcb7918b623 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_ass_subscript_1;
        CHECK_OBJECT( par_value );
        tmp_ass_subvalue_1 = par_value;
        CHECK_OBJECT( par_self );
        tmp_ass_subscribed_1 = par_self;
        CHECK_OBJECT( par_name );
        tmp_ass_subscript_1 = par_name;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3b46041ab19d64777912bcb7918b623 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3b46041ab19d64777912bcb7918b623 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a3b46041ab19d64777912bcb7918b623, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a3b46041ab19d64777912bcb7918b623->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a3b46041ab19d64777912bcb7918b623, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a3b46041ab19d64777912bcb7918b623,
        type_description_1,
        par_self,
        par_name,
        par_value
    );


    // Release cached frame.
    if ( frame_a3b46041ab19d64777912bcb7918b623 == cache_frame_a3b46041ab19d64777912bcb7918b623 )
    {
        Py_DECREF( frame_a3b46041ab19d64777912bcb7918b623 );
    }
    cache_frame_a3b46041ab19d64777912bcb7918b623 = NULL;

    assertFrameObject( frame_a3b46041ab19d64777912bcb7918b623 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_3___setattr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_3___setattr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_4___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6ac873c49303f625aa5136e5b23e17e9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_6ac873c49303f625aa5136e5b23e17e9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6ac873c49303f625aa5136e5b23e17e9, codeobj_6ac873c49303f625aa5136e5b23e17e9, module_tornado$util, sizeof(void *) );
    frame_6ac873c49303f625aa5136e5b23e17e9 = cache_frame_6ac873c49303f625aa5136e5b23e17e9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6ac873c49303f625aa5136e5b23e17e9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6ac873c49303f625aa5136e5b23e17e9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_zlib );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zlib );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_decompressobj );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = const_int_pos_16;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_zlib );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zlib );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_MAX_WBITS );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_ADD_LONG_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_6ac873c49303f625aa5136e5b23e17e9->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_decompressobj, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6ac873c49303f625aa5136e5b23e17e9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6ac873c49303f625aa5136e5b23e17e9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6ac873c49303f625aa5136e5b23e17e9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6ac873c49303f625aa5136e5b23e17e9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6ac873c49303f625aa5136e5b23e17e9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6ac873c49303f625aa5136e5b23e17e9,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_6ac873c49303f625aa5136e5b23e17e9 == cache_frame_6ac873c49303f625aa5136e5b23e17e9 )
    {
        Py_DECREF( frame_6ac873c49303f625aa5136e5b23e17e9 );
    }
    cache_frame_6ac873c49303f625aa5136e5b23e17e9 = NULL;

    assertFrameObject( frame_6ac873c49303f625aa5136e5b23e17e9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_4___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_4___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_5_decompress( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    PyObject *par_max_length = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_f2605a13068cc4eeec560fbbb234fd45;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f2605a13068cc4eeec560fbbb234fd45 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f2605a13068cc4eeec560fbbb234fd45, codeobj_f2605a13068cc4eeec560fbbb234fd45, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f2605a13068cc4eeec560fbbb234fd45 = cache_frame_f2605a13068cc4eeec560fbbb234fd45;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f2605a13068cc4eeec560fbbb234fd45 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f2605a13068cc4eeec560fbbb234fd45 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_decompressobj );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_value );
        tmp_args_element_name_1 = par_value;
        CHECK_OBJECT( par_max_length );
        tmp_args_element_name_2 = par_max_length;
        frame_f2605a13068cc4eeec560fbbb234fd45->m_frame.f_lineno = 114;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_decompress, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f2605a13068cc4eeec560fbbb234fd45 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f2605a13068cc4eeec560fbbb234fd45 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f2605a13068cc4eeec560fbbb234fd45 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f2605a13068cc4eeec560fbbb234fd45, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f2605a13068cc4eeec560fbbb234fd45->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f2605a13068cc4eeec560fbbb234fd45, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f2605a13068cc4eeec560fbbb234fd45,
        type_description_1,
        par_self,
        par_value,
        par_max_length
    );


    // Release cached frame.
    if ( frame_f2605a13068cc4eeec560fbbb234fd45 == cache_frame_f2605a13068cc4eeec560fbbb234fd45 )
    {
        Py_DECREF( frame_f2605a13068cc4eeec560fbbb234fd45 );
    }
    cache_frame_f2605a13068cc4eeec560fbbb234fd45 = NULL;

    assertFrameObject( frame_f2605a13068cc4eeec560fbbb234fd45 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_5_decompress );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_max_length );
    Py_DECREF( par_max_length );
    par_max_length = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_max_length );
    Py_DECREF( par_max_length );
    par_max_length = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_5_decompress );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_6_unconsumed_tail( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b7907cccddd37366ccb6e47ac66fb87e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b7907cccddd37366ccb6e47ac66fb87e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b7907cccddd37366ccb6e47ac66fb87e, codeobj_b7907cccddd37366ccb6e47ac66fb87e, module_tornado$util, sizeof(void *) );
    frame_b7907cccddd37366ccb6e47ac66fb87e = cache_frame_b7907cccddd37366ccb6e47ac66fb87e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b7907cccddd37366ccb6e47ac66fb87e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b7907cccddd37366ccb6e47ac66fb87e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_decompressobj );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_unconsumed_tail );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b7907cccddd37366ccb6e47ac66fb87e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b7907cccddd37366ccb6e47ac66fb87e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b7907cccddd37366ccb6e47ac66fb87e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b7907cccddd37366ccb6e47ac66fb87e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b7907cccddd37366ccb6e47ac66fb87e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b7907cccddd37366ccb6e47ac66fb87e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b7907cccddd37366ccb6e47ac66fb87e,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_b7907cccddd37366ccb6e47ac66fb87e == cache_frame_b7907cccddd37366ccb6e47ac66fb87e )
    {
        Py_DECREF( frame_b7907cccddd37366ccb6e47ac66fb87e );
    }
    cache_frame_b7907cccddd37366ccb6e47ac66fb87e = NULL;

    assertFrameObject( frame_b7907cccddd37366ccb6e47ac66fb87e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_6_unconsumed_tail );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_6_unconsumed_tail );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_7_flush( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b8281247d40d9b17e45d20a264446d50;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b8281247d40d9b17e45d20a264446d50 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b8281247d40d9b17e45d20a264446d50, codeobj_b8281247d40d9b17e45d20a264446d50, module_tornado$util, sizeof(void *) );
    frame_b8281247d40d9b17e45d20a264446d50 = cache_frame_b8281247d40d9b17e45d20a264446d50;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b8281247d40d9b17e45d20a264446d50 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b8281247d40d9b17e45d20a264446d50 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_decompressobj );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_b8281247d40d9b17e45d20a264446d50->m_frame.f_lineno = 128;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_flush );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b8281247d40d9b17e45d20a264446d50 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b8281247d40d9b17e45d20a264446d50 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b8281247d40d9b17e45d20a264446d50 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b8281247d40d9b17e45d20a264446d50, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b8281247d40d9b17e45d20a264446d50->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b8281247d40d9b17e45d20a264446d50, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b8281247d40d9b17e45d20a264446d50,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_b8281247d40d9b17e45d20a264446d50 == cache_frame_b8281247d40d9b17e45d20a264446d50 )
    {
        Py_DECREF( frame_b8281247d40d9b17e45d20a264446d50 );
    }
    cache_frame_b8281247d40d9b17e45d20a264446d50 = NULL;

    assertFrameObject( frame_b8281247d40d9b17e45d20a264446d50 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_7_flush );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_7_flush );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_8_import_object( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    PyObject *var_parts = NULL;
    PyObject *var_obj = NULL;
    struct Nuitka_FrameObject *frame_eb34eda37ebba44ab9440b9f65023306;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_eb34eda37ebba44ab9440b9f65023306 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_eb34eda37ebba44ab9440b9f65023306, codeobj_eb34eda37ebba44ab9440b9f65023306, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_eb34eda37ebba44ab9440b9f65023306 = cache_frame_eb34eda37ebba44ab9440b9f65023306;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eb34eda37ebba44ab9440b9f65023306 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eb34eda37ebba44ab9440b9f65023306 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_name );
        tmp_called_instance_1 = par_name;
        frame_eb34eda37ebba44ab9440b9f65023306->m_frame.f_lineno = 149;
        tmp_compexpr_left_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_count, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_name_name_1;
            CHECK_OBJECT( par_name );
            tmp_name_name_1 = par_name;
            frame_eb34eda37ebba44ab9440b9f65023306->m_frame.f_lineno = 150;
            tmp_return_value = IMPORT_MODULE1( tmp_name_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 150;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_name );
        tmp_called_instance_2 = par_name;
        frame_eb34eda37ebba44ab9440b9f65023306->m_frame.f_lineno = 152;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_parts == NULL );
        var_parts = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_name_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_list_element_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        tmp_source_name_1 = const_str_dot;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        assert( !(tmp_called_name_1 == NULL) );
        CHECK_OBJECT( var_parts );
        tmp_subscribed_name_1 = var_parts;
        tmp_subscript_name_1 = const_slice_none_int_neg_1_none;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_eb34eda37ebba44ab9440b9f65023306->m_frame.f_lineno = 153;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_name_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_name_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_parts );
        tmp_subscribed_name_2 = var_parts;
        tmp_subscript_name_2 = const_int_neg_1;
        tmp_list_element_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, -1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_name_name_2 );

            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_fromlist_name_1 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_fromlist_name_1, 0, tmp_list_element_1 );
        frame_eb34eda37ebba44ab9440b9f65023306->m_frame.f_lineno = 153;
        tmp_assign_source_2 = IMPORT_MODULE_KW( tmp_name_name_2, NULL, NULL, tmp_fromlist_name_1, NULL );
        Py_DECREF( tmp_name_name_2 );
        Py_DECREF( tmp_fromlist_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_obj == NULL );
        var_obj = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        CHECK_OBJECT( var_obj );
        tmp_getattr_target_1 = var_obj;
        CHECK_OBJECT( var_parts );
        tmp_subscribed_name_3 = var_parts;
        tmp_subscript_name_3 = const_int_neg_1;
        tmp_getattr_attr_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, -1 );
        if ( tmp_getattr_attr_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        tmp_return_value = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        Py_DECREF( tmp_getattr_attr_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_8_import_object );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_eb34eda37ebba44ab9440b9f65023306, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_eb34eda37ebba44ab9440b9f65023306, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_AttributeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_subscript_name_4;
            tmp_left_name_1 = const_str_digest_81ac163e5e6bb2013de6b611a30e2aa4;
            CHECK_OBJECT( var_parts );
            tmp_subscribed_name_4 = var_parts;
            tmp_subscript_name_4 = const_int_neg_1;
            tmp_right_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, -1 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
            frame_eb34eda37ebba44ab9440b9f65023306->m_frame.f_lineno = 157;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ImportError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 157;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 154;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_eb34eda37ebba44ab9440b9f65023306->m_frame) frame_eb34eda37ebba44ab9440b9f65023306->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooo";
        goto try_except_handler_3;
        branch_end_2:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_8_import_object );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb34eda37ebba44ab9440b9f65023306 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb34eda37ebba44ab9440b9f65023306 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb34eda37ebba44ab9440b9f65023306 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eb34eda37ebba44ab9440b9f65023306, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eb34eda37ebba44ab9440b9f65023306->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eb34eda37ebba44ab9440b9f65023306, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eb34eda37ebba44ab9440b9f65023306,
        type_description_1,
        par_name,
        var_parts,
        var_obj
    );


    // Release cached frame.
    if ( frame_eb34eda37ebba44ab9440b9f65023306 == cache_frame_eb34eda37ebba44ab9440b9f65023306 )
    {
        Py_DECREF( frame_eb34eda37ebba44ab9440b9f65023306 );
    }
    cache_frame_eb34eda37ebba44ab9440b9f65023306 = NULL;

    assertFrameObject( frame_eb34eda37ebba44ab9440b9f65023306 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_8_import_object );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_parts );
    var_parts = NULL;

    Py_XDECREF( var_obj );
    var_obj = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_parts );
    var_parts = NULL;

    Py_XDECREF( var_obj );
    var_obj = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_8_import_object );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_9_exec_in( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_code = python_pars[ 0 ];
    PyObject *par_glob = python_pars[ 1 ];
    PyObject *par_loc = python_pars[ 2 ];
    PyObject *tmp_exec_call_1__globals = NULL;
    PyObject *tmp_exec_call_1__locals = NULL;
    struct Nuitka_FrameObject *frame_09bba29479aa3dd8961d4c90ebf41215;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *locals_tornado$util$$$function_9_exec_in = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_09bba29479aa3dd8961d4c90ebf41215 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_09bba29479aa3dd8961d4c90ebf41215, codeobj_09bba29479aa3dd8961d4c90ebf41215, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_09bba29479aa3dd8961d4c90ebf41215 = cache_frame_09bba29479aa3dd8961d4c90ebf41215;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_09bba29479aa3dd8961d4c90ebf41215 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_09bba29479aa3dd8961d4c90ebf41215 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_code );
        tmp_isinstance_inst_1 = par_code;
        tmp_isinstance_cls_1 = (PyObject *)&PyUnicode_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_compile_source_1;
            PyObject *tmp_compile_filename_1;
            PyObject *tmp_compile_mode_1;
            PyObject *tmp_compile_dont_inherit_1;
            CHECK_OBJECT( par_code );
            tmp_compile_source_1 = par_code;
            tmp_compile_filename_1 = const_str_angle_string;
            tmp_compile_mode_1 = const_str_plain_exec;
            tmp_compile_dont_inherit_1 = Py_True;
            tmp_assign_source_1 = COMPILE_CODE( tmp_compile_source_1, tmp_compile_filename_1, tmp_compile_mode_1, NULL, tmp_compile_dont_inherit_1, NULL );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 164;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_code;
                assert( old != NULL );
                par_code = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_outline_return_value_1;
        {
            PyObject *tmp_assign_source_2;
            CHECK_OBJECT( par_glob );
            tmp_assign_source_2 = par_glob;
            assert( tmp_exec_call_1__globals == NULL );
            Py_INCREF( tmp_assign_source_2 );
            tmp_exec_call_1__globals = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            CHECK_OBJECT( par_loc );
            tmp_assign_source_3 = par_loc;
            assert( tmp_exec_call_1__locals == NULL );
            Py_INCREF( tmp_assign_source_3 );
            tmp_exec_call_1__locals = tmp_assign_source_3;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_exec_call_1__locals );
            tmp_compexpr_left_1 = tmp_exec_call_1__locals;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_4;
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                CHECK_OBJECT( tmp_exec_call_1__globals );
                tmp_compexpr_left_2 = tmp_exec_call_1__globals;
                tmp_compexpr_right_2 = Py_None;
                tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_1;
                }
                else
                {
                    goto condexpr_false_1;
                }
                condexpr_true_1:;
                if (locals_tornado$util$$$function_9_exec_in == NULL) locals_tornado$util$$$function_9_exec_in = PyDict_New();
                tmp_assign_source_4 = locals_tornado$util$$$function_9_exec_in;
                Py_INCREF( tmp_assign_source_4 );
                if ( par_code != NULL )
                {
                    PyObject *value;
                    CHECK_OBJECT( par_code );
                    value = par_code;

                    UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_4, (Nuitka_StringObject *)const_str_plain_code, value );
                }
                else
                {
                    int res = PyDict_DelItem( tmp_assign_source_4, const_str_plain_code );

                    if ( res != 0 )
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                }

                if ( par_glob != NULL )
                {
                    PyObject *value;
                    CHECK_OBJECT( par_glob );
                    value = par_glob;

                    UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_4, (Nuitka_StringObject *)const_str_plain_glob, value );
                }
                else
                {
                    int res = PyDict_DelItem( tmp_assign_source_4, const_str_plain_glob );

                    if ( res != 0 )
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                }

                if ( par_loc != NULL )
                {
                    PyObject *value;
                    CHECK_OBJECT( par_loc );
                    value = par_loc;

                    UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_4, (Nuitka_StringObject *)const_str_plain_loc, value );
                }
                else
                {
                    int res = PyDict_DelItem( tmp_assign_source_4, const_str_plain_loc );

                    if ( res != 0 )
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                }

                goto condexpr_end_1;
                condexpr_false_1:;
                CHECK_OBJECT( tmp_exec_call_1__globals );
                tmp_assign_source_4 = tmp_exec_call_1__globals;
                Py_INCREF( tmp_assign_source_4 );
                condexpr_end_1:;
                {
                    PyObject *old = tmp_exec_call_1__locals;
                    assert( old != NULL );
                    tmp_exec_call_1__locals = tmp_assign_source_4;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_exec_call_1__globals );
            tmp_compexpr_left_3 = tmp_exec_call_1__globals;
            tmp_compexpr_right_3 = Py_None;
            tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_5;
                tmp_assign_source_5 = (PyObject *)moduledict_tornado$util;
                {
                    PyObject *old = tmp_exec_call_1__globals;
                    assert( old != NULL );
                    tmp_exec_call_1__globals = tmp_assign_source_5;
                    Py_INCREF( tmp_exec_call_1__globals );
                    Py_DECREF( old );
                }

            }
            branch_no_3:;
        }
        // Tried code:
        {
            PyObject *tmp_eval_source_1;
            PyObject *tmp_eval_globals_1;
            PyObject *tmp_eval_locals_1;
            PyObject *tmp_eval_compiled_1;
            if ( par_code == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "code" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 165;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }

            tmp_eval_source_1 = par_code;
            CHECK_OBJECT( tmp_exec_call_1__globals );
            tmp_eval_globals_1 = tmp_exec_call_1__globals;
            CHECK_OBJECT( tmp_exec_call_1__locals );
            tmp_eval_locals_1 = tmp_exec_call_1__locals;
            tmp_eval_compiled_1 = COMPILE_CODE( tmp_eval_source_1, const_str_angle_string, const_str_plain_exec, NULL, NULL, NULL );
            if ( tmp_eval_compiled_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 165;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            tmp_outline_return_value_1 = EVAL_CODE( tmp_eval_compiled_1, tmp_eval_globals_1, tmp_eval_locals_1 );
            Py_DECREF( tmp_eval_compiled_1 );
            if ( tmp_outline_return_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 165;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            goto try_return_handler_2;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util$$$function_9_exec_in );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__globals );
        Py_DECREF( tmp_exec_call_1__globals );
        tmp_exec_call_1__globals = NULL;

        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__locals );
        Py_DECREF( tmp_exec_call_1__locals );
        tmp_exec_call_1__locals = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__globals );
        Py_DECREF( tmp_exec_call_1__globals );
        tmp_exec_call_1__globals = NULL;

        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__locals );
        Py_DECREF( tmp_exec_call_1__locals );
        tmp_exec_call_1__locals = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$util$$$function_9_exec_in );
        return NULL;
        outline_result_1:;
        Py_DECREF( tmp_outline_return_value_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_09bba29479aa3dd8961d4c90ebf41215 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_09bba29479aa3dd8961d4c90ebf41215 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_09bba29479aa3dd8961d4c90ebf41215, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_09bba29479aa3dd8961d4c90ebf41215->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_09bba29479aa3dd8961d4c90ebf41215, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_09bba29479aa3dd8961d4c90ebf41215,
        type_description_1,
        par_code,
        par_glob,
        par_loc
    );


    // Release cached frame.
    if ( frame_09bba29479aa3dd8961d4c90ebf41215 == cache_frame_09bba29479aa3dd8961d4c90ebf41215 )
    {
        Py_DECREF( frame_09bba29479aa3dd8961d4c90ebf41215 );
    }
    cache_frame_09bba29479aa3dd8961d4c90ebf41215 = NULL;

    assertFrameObject( frame_09bba29479aa3dd8961d4c90ebf41215 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_9_exec_in );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_code );
    par_code = NULL;

    Py_XDECREF( par_glob );
    par_glob = NULL;

    Py_XDECREF( par_loc );
    par_loc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_code );
    par_code = NULL;

    Py_XDECREF( par_glob );
    par_glob = NULL;

    Py_XDECREF( par_loc );
    par_loc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_9_exec_in );
    return NULL;

function_exception_exit:
    Py_XDECREF( locals_tornado$util$$$function_9_exec_in );
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.
    Py_XDECREF( locals_tornado$util$$$function_9_exec_in );


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_10_raise_exc_info( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_exc_info = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5c05df6f3541e877d3f2485f49d7fe72;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_5c05df6f3541e877d3f2485f49d7fe72 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_5c05df6f3541e877d3f2485f49d7fe72, codeobj_5c05df6f3541e877d3f2485f49d7fe72, module_tornado$util, sizeof(void *) );
    frame_5c05df6f3541e877d3f2485f49d7fe72 = cache_frame_5c05df6f3541e877d3f2485f49d7fe72;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5c05df6f3541e877d3f2485f49d7fe72 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5c05df6f3541e877d3f2485f49d7fe72 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_exc_info );
        tmp_subscribed_name_1 = par_exc_info;
        tmp_subscript_name_1 = const_int_pos_1;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_1 = "o";
            goto try_except_handler_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_subscript_name_3;
            CHECK_OBJECT( par_exc_info );
            tmp_subscribed_name_2 = par_exc_info;
            tmp_subscript_name_2 = const_int_pos_1;
            tmp_source_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 1 );
            if ( tmp_source_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_1 = "o";
                goto try_except_handler_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_with_traceback );
            Py_DECREF( tmp_source_name_1 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_1 = "o";
                goto try_except_handler_1;
            }
            CHECK_OBJECT( par_exc_info );
            tmp_subscribed_name_3 = par_exc_info;
            tmp_subscript_name_3 = const_int_pos_2;
            tmp_args_element_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 2 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 179;
                type_description_1 = "o";
                goto try_except_handler_1;
            }
            frame_5c05df6f3541e877d3f2485f49d7fe72->m_frame.f_lineno = 179;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_1 = "o";
                goto try_except_handler_1;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 179;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "o";
            goto try_except_handler_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_055f1f027909dea18d22a0d894d1dccd;
            frame_5c05df6f3541e877d3f2485f49d7fe72->m_frame.f_lineno = 181;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
            }

            assert( !(tmp_raise_type_2 == NULL) );
            exception_type = tmp_raise_type_2;
            exception_lineno = 181;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "o";
            goto try_except_handler_1;
        }
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_10_raise_exc_info );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_5c05df6f3541e877d3f2485f49d7fe72, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_5c05df6f3541e877d3f2485f49d7fe72, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    Py_XDECREF( par_exc_info );
    par_exc_info = NULL;

    // Tried code:
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 177;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_5c05df6f3541e877d3f2485f49d7fe72->m_frame) frame_5c05df6f3541e877d3f2485f49d7fe72->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "o";
    goto try_except_handler_2;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_10_raise_exc_info );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5c05df6f3541e877d3f2485f49d7fe72 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5c05df6f3541e877d3f2485f49d7fe72 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5c05df6f3541e877d3f2485f49d7fe72, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5c05df6f3541e877d3f2485f49d7fe72->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5c05df6f3541e877d3f2485f49d7fe72, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5c05df6f3541e877d3f2485f49d7fe72,
        type_description_1,
        par_exc_info
    );


    // Release cached frame.
    if ( frame_5c05df6f3541e877d3f2485f49d7fe72 == cache_frame_5c05df6f3541e877d3f2485f49d7fe72 )
    {
        Py_DECREF( frame_5c05df6f3541e877d3f2485f49d7fe72 );
    }
    cache_frame_5c05df6f3541e877d3f2485f49d7fe72 = NULL;

    assertFrameObject( frame_5c05df6f3541e877d3f2485f49d7fe72 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_10_raise_exc_info );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_tornado$util$$$function_11_errno_from_exception( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_e = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_cd092f1f8d845da96187aab3d6b2f9d1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_cd092f1f8d845da96187aab3d6b2f9d1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cd092f1f8d845da96187aab3d6b2f9d1, codeobj_cd092f1f8d845da96187aab3d6b2f9d1, module_tornado$util, sizeof(void *) );
    frame_cd092f1f8d845da96187aab3d6b2f9d1 = cache_frame_cd092f1f8d845da96187aab3d6b2f9d1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cd092f1f8d845da96187aab3d6b2f9d1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cd092f1f8d845da96187aab3d6b2f9d1 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_name_1;
        CHECK_OBJECT( par_e );
        tmp_source_name_1 = par_e;
        tmp_attribute_name_1 = const_str_plain_errno;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_e );
            tmp_source_name_2 = par_e;
            tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_errno );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 199;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_attribute_value_1;
            int tmp_truth_name_1;
            CHECK_OBJECT( par_e );
            tmp_source_name_3 = par_e;
            tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_args );
            if ( tmp_attribute_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 200;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_attribute_value_1 );

                exception_lineno = 200;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_attribute_value_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( par_e );
                tmp_source_name_4 = par_e;
                tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_args );
                if ( tmp_subscribed_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 201;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                tmp_subscript_name_1 = const_int_0;
                tmp_return_value = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
                Py_DECREF( tmp_subscribed_name_1 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 201;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            goto branch_end_2;
            branch_no_2:;
            tmp_return_value = Py_None;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            branch_end_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cd092f1f8d845da96187aab3d6b2f9d1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cd092f1f8d845da96187aab3d6b2f9d1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cd092f1f8d845da96187aab3d6b2f9d1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cd092f1f8d845da96187aab3d6b2f9d1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cd092f1f8d845da96187aab3d6b2f9d1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cd092f1f8d845da96187aab3d6b2f9d1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cd092f1f8d845da96187aab3d6b2f9d1,
        type_description_1,
        par_e
    );


    // Release cached frame.
    if ( frame_cd092f1f8d845da96187aab3d6b2f9d1 == cache_frame_cd092f1f8d845da96187aab3d6b2f9d1 )
    {
        Py_DECREF( frame_cd092f1f8d845da96187aab3d6b2f9d1 );
    }
    cache_frame_cd092f1f8d845da96187aab3d6b2f9d1 = NULL;

    assertFrameObject( frame_cd092f1f8d845da96187aab3d6b2f9d1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_11_errno_from_exception );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_11_errno_from_exception );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_12__re_unescape_replacement( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_match = python_pars[ 0 ];
    PyObject *var_group = NULL;
    struct Nuitka_FrameObject *frame_1b00f2100869eb9199ddf9717b4f1ce3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_1b00f2100869eb9199ddf9717b4f1ce3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1b00f2100869eb9199ddf9717b4f1ce3, codeobj_1b00f2100869eb9199ddf9717b4f1ce3, module_tornado$util, sizeof(void *)+sizeof(void *) );
    frame_1b00f2100869eb9199ddf9717b4f1ce3 = cache_frame_1b00f2100869eb9199ddf9717b4f1ce3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1b00f2100869eb9199ddf9717b4f1ce3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1b00f2100869eb9199ddf9717b4f1ce3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_match );
        tmp_called_instance_1 = par_match;
        frame_1b00f2100869eb9199ddf9717b4f1ce3->m_frame.f_lineno = 210;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_group == NULL );
        var_group = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( var_group );
        tmp_subscribed_name_1 = var_group;
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__alphanum );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__alphanum );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_compexpr_left_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_alphanum" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 211;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_1;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            tmp_left_name_1 = const_str_digest_bbb5efbc373f0147c801e308dfebf5f7;
            CHECK_OBJECT( var_group );
            tmp_subscribed_name_2 = var_group;
            tmp_subscript_name_2 = const_int_0;
            tmp_right_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_1b00f2100869eb9199ddf9717b4f1ce3->m_frame.f_lineno = 212;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 212;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b00f2100869eb9199ddf9717b4f1ce3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b00f2100869eb9199ddf9717b4f1ce3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1b00f2100869eb9199ddf9717b4f1ce3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1b00f2100869eb9199ddf9717b4f1ce3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1b00f2100869eb9199ddf9717b4f1ce3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1b00f2100869eb9199ddf9717b4f1ce3,
        type_description_1,
        par_match,
        var_group
    );


    // Release cached frame.
    if ( frame_1b00f2100869eb9199ddf9717b4f1ce3 == cache_frame_1b00f2100869eb9199ddf9717b4f1ce3 )
    {
        Py_DECREF( frame_1b00f2100869eb9199ddf9717b4f1ce3 );
    }
    cache_frame_1b00f2100869eb9199ddf9717b4f1ce3 = NULL;

    assertFrameObject( frame_1b00f2100869eb9199ddf9717b4f1ce3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_group );
    tmp_return_value = var_group;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_12__re_unescape_replacement );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_match );
    Py_DECREF( par_match );
    par_match = NULL;

    CHECK_OBJECT( (PyObject *)var_group );
    Py_DECREF( var_group );
    var_group = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_match );
    Py_DECREF( par_match );
    par_match = NULL;

    Py_XDECREF( var_group );
    var_group = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_12__re_unescape_replacement );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_13_re_unescape( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_257d45a16f36f6acf4b1c6d6951f102e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_257d45a16f36f6acf4b1c6d6951f102e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_257d45a16f36f6acf4b1c6d6951f102e, codeobj_257d45a16f36f6acf4b1c6d6951f102e, module_tornado$util, sizeof(void *) );
    frame_257d45a16f36f6acf4b1c6d6951f102e = cache_frame_257d45a16f36f6acf4b1c6d6951f102e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_257d45a16f36f6acf4b1c6d6951f102e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_257d45a16f36f6acf4b1c6d6951f102e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__re_unescape_pattern );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__re_unescape_pattern );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_re_unescape_pattern" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 228;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sub );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__re_unescape_replacement );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__re_unescape_replacement );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_re_unescape_replacement" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 228;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_s );
        tmp_args_element_name_2 = par_s;
        frame_257d45a16f36f6acf4b1c6d6951f102e->m_frame.f_lineno = 228;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_257d45a16f36f6acf4b1c6d6951f102e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_257d45a16f36f6acf4b1c6d6951f102e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_257d45a16f36f6acf4b1c6d6951f102e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_257d45a16f36f6acf4b1c6d6951f102e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_257d45a16f36f6acf4b1c6d6951f102e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_257d45a16f36f6acf4b1c6d6951f102e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_257d45a16f36f6acf4b1c6d6951f102e,
        type_description_1,
        par_s
    );


    // Release cached frame.
    if ( frame_257d45a16f36f6acf4b1c6d6951f102e == cache_frame_257d45a16f36f6acf4b1c6d6951f102e )
    {
        Py_DECREF( frame_257d45a16f36f6acf4b1c6d6951f102e );
    }
    cache_frame_257d45a16f36f6acf4b1c6d6951f102e = NULL;

    assertFrameObject( frame_257d45a16f36f6acf4b1c6d6951f102e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_13_re_unescape );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_13_re_unescape );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_14___new__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_base = NULL;
    PyObject *var_init_kwargs = NULL;
    PyObject *var_impl = NULL;
    PyObject *var_instance = NULL;
    struct Nuitka_FrameObject *frame_a2ed4f3e006fcf9e294bc18f3c4f4149;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_a2ed4f3e006fcf9e294bc18f3c4f4149 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a2ed4f3e006fcf9e294bc18f3c4f4149, codeobj_a2ed4f3e006fcf9e294bc18f3c4f4149, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a2ed4f3e006fcf9e294bc18f3c4f4149 = cache_frame_a2ed4f3e006fcf9e294bc18f3c4f4149;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a2ed4f3e006fcf9e294bc18f3c4f4149 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a2ed4f3e006fcf9e294bc18f3c4f4149 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_cls );
        tmp_called_instance_1 = par_cls;
        frame_a2ed4f3e006fcf9e294bc18f3c4f4149->m_frame.f_lineno = 270;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_configurable_base );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 270;
            type_description_1 = "oooooooN";
            goto frame_exception_exit_1;
        }
        assert( var_base == NULL );
        var_base = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyDict_New();
        assert( var_init_kwargs == NULL );
        var_init_kwargs = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_cls );
        tmp_compexpr_left_1 = par_cls;
        CHECK_OBJECT( var_base );
        tmp_compexpr_right_1 = var_base;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( par_cls );
            tmp_called_instance_2 = par_cls;
            frame_a2ed4f3e006fcf9e294bc18f3c4f4149->m_frame.f_lineno = 273;
            tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_configured_class );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 273;
                type_description_1 = "oooooooN";
                goto frame_exception_exit_1;
            }
            assert( var_impl == NULL );
            var_impl = tmp_assign_source_3;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_attribute_value_1;
            int tmp_truth_name_1;
            CHECK_OBJECT( var_base );
            tmp_source_name_1 = var_base;
            tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__Configurable__impl_kwargs );
            if ( tmp_attribute_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 274;
                type_description_1 = "oooooooN";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_attribute_value_1 );

                exception_lineno = 274;
                type_description_1 = "oooooooN";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_attribute_value_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_2;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_source_name_3;
                CHECK_OBJECT( var_init_kwargs );
                tmp_source_name_2 = var_init_kwargs;
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_update );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 275;
                    type_description_1 = "oooooooN";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_base );
                tmp_source_name_3 = var_base;
                tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__Configurable__impl_kwargs );
                if ( tmp_args_element_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );

                    exception_lineno = 275;
                    type_description_1 = "oooooooN";
                    goto frame_exception_exit_1;
                }
                frame_a2ed4f3e006fcf9e294bc18f3c4f4149->m_frame.f_lineno = 275;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1 };
                    tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_1 );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 275;
                    type_description_1 = "oooooooN";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( par_cls );
            tmp_assign_source_4 = par_cls;
            assert( var_impl == NULL );
            Py_INCREF( tmp_assign_source_4 );
            var_impl = tmp_assign_source_4;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_init_kwargs );
        tmp_called_instance_3 = var_init_kwargs;
        CHECK_OBJECT( par_kwargs );
        tmp_args_element_name_2 = par_kwargs;
        frame_a2ed4f3e006fcf9e294bc18f3c4f4149->m_frame.f_lineno = 278;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_update, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 278;
            type_description_1 = "oooooooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_called_instance_4;
        CHECK_OBJECT( var_impl );
        tmp_called_instance_4 = var_impl;
        frame_a2ed4f3e006fcf9e294bc18f3c4f4149->m_frame.f_lineno = 279;
        tmp_compexpr_left_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_configurable_base );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 279;
            type_description_1 = "oooooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_base );
        tmp_compexpr_right_2 = var_base;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_dircall_arg1_1;
            PyObject *tmp_dircall_arg2_1;
            PyObject *tmp_dircall_arg3_1;
            CHECK_OBJECT( var_impl );
            tmp_dircall_arg1_1 = var_impl;
            CHECK_OBJECT( par_args );
            tmp_dircall_arg2_1 = par_args;
            CHECK_OBJECT( var_init_kwargs );
            tmp_dircall_arg3_1 = var_init_kwargs;
            Py_INCREF( tmp_dircall_arg1_1 );
            Py_INCREF( tmp_dircall_arg2_1 );
            Py_INCREF( tmp_dircall_arg3_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
                tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
            }
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 281;
                type_description_1 = "oooooooN";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Configurable );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Configurable );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Configurable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 282;
            type_description_1 = "oooooooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_cls );
        tmp_object_name_1 = par_cls;
        tmp_called_instance_5 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 282;
            type_description_1 = "oooooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_impl );
        tmp_args_element_name_3 = var_impl;
        frame_a2ed4f3e006fcf9e294bc18f3c4f4149->m_frame.f_lineno = 282;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain___new__, call_args );
        }

        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 282;
            type_description_1 = "oooooooN";
            goto frame_exception_exit_1;
        }
        assert( var_instance == NULL );
        var_instance = tmp_assign_source_5;
    }
    {
        PyObject *tmp_dircall_arg1_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_dircall_arg2_2;
        PyObject *tmp_dircall_arg3_2;
        PyObject *tmp_call_result_3;
        CHECK_OBJECT( var_instance );
        tmp_source_name_4 = var_instance;
        tmp_dircall_arg1_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_initialize );
        if ( tmp_dircall_arg1_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 286;
            type_description_1 = "oooooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_2 = par_args;
        CHECK_OBJECT( var_init_kwargs );
        tmp_dircall_arg3_2 = var_init_kwargs;
        Py_INCREF( tmp_dircall_arg2_2 );
        Py_INCREF( tmp_dircall_arg3_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2, tmp_dircall_arg3_2};
            tmp_call_result_3 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 286;
            type_description_1 = "oooooooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a2ed4f3e006fcf9e294bc18f3c4f4149 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a2ed4f3e006fcf9e294bc18f3c4f4149 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a2ed4f3e006fcf9e294bc18f3c4f4149 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a2ed4f3e006fcf9e294bc18f3c4f4149, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a2ed4f3e006fcf9e294bc18f3c4f4149->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a2ed4f3e006fcf9e294bc18f3c4f4149, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a2ed4f3e006fcf9e294bc18f3c4f4149,
        type_description_1,
        par_cls,
        par_args,
        par_kwargs,
        var_base,
        var_init_kwargs,
        var_impl,
        var_instance,
        NULL
    );


    // Release cached frame.
    if ( frame_a2ed4f3e006fcf9e294bc18f3c4f4149 == cache_frame_a2ed4f3e006fcf9e294bc18f3c4f4149 )
    {
        Py_DECREF( frame_a2ed4f3e006fcf9e294bc18f3c4f4149 );
    }
    cache_frame_a2ed4f3e006fcf9e294bc18f3c4f4149 = NULL;

    assertFrameObject( frame_a2ed4f3e006fcf9e294bc18f3c4f4149 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_instance );
    tmp_return_value = var_instance;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_14___new__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_base );
    Py_DECREF( var_base );
    var_base = NULL;

    CHECK_OBJECT( (PyObject *)var_init_kwargs );
    Py_DECREF( var_init_kwargs );
    var_init_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_impl );
    Py_DECREF( var_impl );
    var_impl = NULL;

    Py_XDECREF( var_instance );
    var_instance = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_base );
    var_base = NULL;

    Py_XDECREF( var_init_kwargs );
    var_init_kwargs = NULL;

    Py_XDECREF( var_impl );
    var_impl = NULL;

    Py_XDECREF( var_instance );
    var_instance = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_14___new__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_15_configurable_base( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_84384e4a4872c31258d50896b891c2f1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_84384e4a4872c31258d50896b891c2f1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_84384e4a4872c31258d50896b891c2f1, codeobj_84384e4a4872c31258d50896b891c2f1, module_tornado$util, sizeof(void *) );
    frame_84384e4a4872c31258d50896b891c2f1 = cache_frame_84384e4a4872c31258d50896b891c2f1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_84384e4a4872c31258d50896b891c2f1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_84384e4a4872c31258d50896b891c2f1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_raise_type_1;
        frame_84384e4a4872c31258d50896b891c2f1->m_frame.f_lineno = 299;
        tmp_raise_type_1 = CALL_FUNCTION_NO_ARGS( PyExc_NotImplementedError );
        assert( !(tmp_raise_type_1 == NULL) );
        exception_type = tmp_raise_type_1;
        exception_lineno = 299;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_84384e4a4872c31258d50896b891c2f1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_84384e4a4872c31258d50896b891c2f1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_84384e4a4872c31258d50896b891c2f1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_84384e4a4872c31258d50896b891c2f1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_84384e4a4872c31258d50896b891c2f1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_84384e4a4872c31258d50896b891c2f1,
        type_description_1,
        par_cls
    );


    // Release cached frame.
    if ( frame_84384e4a4872c31258d50896b891c2f1 == cache_frame_84384e4a4872c31258d50896b891c2f1 )
    {
        Py_DECREF( frame_84384e4a4872c31258d50896b891c2f1 );
    }
    cache_frame_84384e4a4872c31258d50896b891c2f1 = NULL;

    assertFrameObject( frame_84384e4a4872c31258d50896b891c2f1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_15_configurable_base );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_15_configurable_base );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_tornado$util$$$function_16_configurable_default( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_980fe3ca36fafcffb6ecb31e32e0123e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_980fe3ca36fafcffb6ecb31e32e0123e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_980fe3ca36fafcffb6ecb31e32e0123e, codeobj_980fe3ca36fafcffb6ecb31e32e0123e, module_tornado$util, sizeof(void *) );
    frame_980fe3ca36fafcffb6ecb31e32e0123e = cache_frame_980fe3ca36fafcffb6ecb31e32e0123e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_980fe3ca36fafcffb6ecb31e32e0123e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_980fe3ca36fafcffb6ecb31e32e0123e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_raise_type_1;
        frame_980fe3ca36fafcffb6ecb31e32e0123e->m_frame.f_lineno = 305;
        tmp_raise_type_1 = CALL_FUNCTION_NO_ARGS( PyExc_NotImplementedError );
        assert( !(tmp_raise_type_1 == NULL) );
        exception_type = tmp_raise_type_1;
        exception_lineno = 305;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_980fe3ca36fafcffb6ecb31e32e0123e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_980fe3ca36fafcffb6ecb31e32e0123e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_980fe3ca36fafcffb6ecb31e32e0123e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_980fe3ca36fafcffb6ecb31e32e0123e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_980fe3ca36fafcffb6ecb31e32e0123e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_980fe3ca36fafcffb6ecb31e32e0123e,
        type_description_1,
        par_cls
    );


    // Release cached frame.
    if ( frame_980fe3ca36fafcffb6ecb31e32e0123e == cache_frame_980fe3ca36fafcffb6ecb31e32e0123e )
    {
        Py_DECREF( frame_980fe3ca36fafcffb6ecb31e32e0123e );
    }
    cache_frame_980fe3ca36fafcffb6ecb31e32e0123e = NULL;

    assertFrameObject( frame_980fe3ca36fafcffb6ecb31e32e0123e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_16_configurable_default );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_16_configurable_default );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_tornado$util$$$function_17__initialize( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_17__initialize );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_17__initialize );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_18_configure( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_impl = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_base = NULL;
    struct Nuitka_FrameObject *frame_d4e9a1d0a4784dfbe1b1369f4150d79e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_d4e9a1d0a4784dfbe1b1369f4150d79e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d4e9a1d0a4784dfbe1b1369f4150d79e, codeobj_d4e9a1d0a4784dfbe1b1369f4150d79e, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d4e9a1d0a4784dfbe1b1369f4150d79e = cache_frame_d4e9a1d0a4784dfbe1b1369f4150d79e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d4e9a1d0a4784dfbe1b1369f4150d79e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d4e9a1d0a4784dfbe1b1369f4150d79e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_cls );
        tmp_called_instance_1 = par_cls;
        frame_d4e9a1d0a4784dfbe1b1369f4150d79e->m_frame.f_lineno = 328;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_configurable_base );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 328;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_base == NULL );
        var_base = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_impl );
        tmp_isinstance_inst_1 = par_impl;
        tmp_isinstance_cls_1 = (PyObject *)&PyUnicode_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 329;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_typing );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 330;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_cast );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 330;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Type );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Type );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Type" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 330;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_1 = tmp_mvar_value_2;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Configurable );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Configurable );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Configurable" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 330;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_subscript_name_1 = tmp_mvar_value_3;
            tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 330;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_import_object );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_import_object );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "import_object" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 330;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_4;
            CHECK_OBJECT( par_impl );
            tmp_args_element_name_3 = par_impl;
            frame_d4e9a1d0a4784dfbe1b1369f4150d79e->m_frame.f_lineno = 330;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_1 );

                exception_lineno = 330;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_d4e9a1d0a4784dfbe1b1369f4150d79e->m_frame.f_lineno = 330;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 330;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_impl;
                assert( old != NULL );
                par_impl = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( par_impl );
        tmp_compexpr_left_1 = par_impl;
        tmp_compexpr_right_1 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_issubclass );
        assert( tmp_called_name_3 != NULL );
        CHECK_OBJECT( par_impl );
        tmp_args_element_name_4 = par_impl;
        CHECK_OBJECT( par_cls );
        tmp_args_element_name_5 = par_cls;
        frame_d4e9a1d0a4784dfbe1b1369f4150d79e->m_frame.f_lineno = 331;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 331;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 331;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_left_name_1 = const_str_digest_d98e06b0a1609bcfa4ac9ce4e8c2e123;
            CHECK_OBJECT( par_cls );
            tmp_right_name_1 = par_cls;
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 332;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_d4e9a1d0a4784dfbe1b1369f4150d79e->m_frame.f_lineno = 332;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 332;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_impl );
        tmp_assattr_name_1 = par_impl;
        CHECK_OBJECT( var_base );
        tmp_assattr_target_1 = var_base;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__Configurable__impl_class, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_kwargs );
        tmp_assattr_name_2 = par_kwargs;
        CHECK_OBJECT( var_base );
        tmp_assattr_target_2 = var_base;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__Configurable__impl_kwargs, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4e9a1d0a4784dfbe1b1369f4150d79e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4e9a1d0a4784dfbe1b1369f4150d79e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d4e9a1d0a4784dfbe1b1369f4150d79e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d4e9a1d0a4784dfbe1b1369f4150d79e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d4e9a1d0a4784dfbe1b1369f4150d79e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d4e9a1d0a4784dfbe1b1369f4150d79e,
        type_description_1,
        par_cls,
        par_impl,
        par_kwargs,
        var_base
    );


    // Release cached frame.
    if ( frame_d4e9a1d0a4784dfbe1b1369f4150d79e == cache_frame_d4e9a1d0a4784dfbe1b1369f4150d79e )
    {
        Py_DECREF( frame_d4e9a1d0a4784dfbe1b1369f4150d79e );
    }
    cache_frame_d4e9a1d0a4784dfbe1b1369f4150d79e = NULL;

    assertFrameObject( frame_d4e9a1d0a4784dfbe1b1369f4150d79e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_18_configure );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_impl );
    Py_DECREF( par_impl );
    par_impl = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_base );
    Py_DECREF( var_base );
    var_base = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( par_impl );
    par_impl = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_base );
    var_base = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_18_configure );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_19_configured_class( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *var_base = NULL;
    struct Nuitka_FrameObject *frame_e7efdb96952213371a786ae45a9ca0b2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_e7efdb96952213371a786ae45a9ca0b2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e7efdb96952213371a786ae45a9ca0b2, codeobj_e7efdb96952213371a786ae45a9ca0b2, module_tornado$util, sizeof(void *)+sizeof(void *) );
    frame_e7efdb96952213371a786ae45a9ca0b2 = cache_frame_e7efdb96952213371a786ae45a9ca0b2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e7efdb96952213371a786ae45a9ca0b2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e7efdb96952213371a786ae45a9ca0b2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_cls );
        tmp_called_instance_1 = par_cls;
        frame_e7efdb96952213371a786ae45a9ca0b2->m_frame.f_lineno = 340;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_configurable_base );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 340;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_base == NULL );
        var_base = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_base );
        tmp_source_name_1 = var_base;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE_DICT_SLOT( tmp_source_name_1 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 344;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_e7efdb96952213371a786ae45a9ca0b2->m_frame.f_lineno = 344;
        tmp_compexpr_left_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain__Configurable__impl_class_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 344;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( par_cls );
            tmp_called_instance_3 = par_cls;
            frame_e7efdb96952213371a786ae45a9ca0b2->m_frame.f_lineno = 345;
            tmp_assattr_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_configurable_default );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 345;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_base );
            tmp_assattr_target_1 = var_base;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__Configurable__impl_class, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 345;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_base );
        tmp_source_name_2 = var_base;
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__Configurable__impl_class );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( var_base );
            tmp_source_name_3 = var_base;
            tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__Configurable__impl_class );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 347;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_fd037e2200e52621e5904f73e2f65817;
            frame_e7efdb96952213371a786ae45a9ca0b2->m_frame.f_lineno = 350;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 350;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        branch_end_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e7efdb96952213371a786ae45a9ca0b2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e7efdb96952213371a786ae45a9ca0b2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e7efdb96952213371a786ae45a9ca0b2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e7efdb96952213371a786ae45a9ca0b2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e7efdb96952213371a786ae45a9ca0b2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e7efdb96952213371a786ae45a9ca0b2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e7efdb96952213371a786ae45a9ca0b2,
        type_description_1,
        par_cls,
        var_base
    );


    // Release cached frame.
    if ( frame_e7efdb96952213371a786ae45a9ca0b2 == cache_frame_e7efdb96952213371a786ae45a9ca0b2 )
    {
        Py_DECREF( frame_e7efdb96952213371a786ae45a9ca0b2 );
    }
    cache_frame_e7efdb96952213371a786ae45a9ca0b2 = NULL;

    assertFrameObject( frame_e7efdb96952213371a786ae45a9ca0b2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_19_configured_class );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_base );
    Py_DECREF( var_base );
    var_base = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( var_base );
    var_base = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_19_configured_class );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_20__save_configuration( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *var_base = NULL;
    struct Nuitka_FrameObject *frame_09ff696e4dd27517039c78810447018d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_09ff696e4dd27517039c78810447018d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_09ff696e4dd27517039c78810447018d, codeobj_09ff696e4dd27517039c78810447018d, module_tornado$util, sizeof(void *)+sizeof(void *) );
    frame_09ff696e4dd27517039c78810447018d = cache_frame_09ff696e4dd27517039c78810447018d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_09ff696e4dd27517039c78810447018d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_09ff696e4dd27517039c78810447018d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_cls );
        tmp_called_instance_1 = par_cls;
        frame_09ff696e4dd27517039c78810447018d->m_frame.f_lineno = 355;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_configurable_base );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 355;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_base == NULL );
        var_base = tmp_assign_source_1;
    }
    {
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_base );
        tmp_source_name_1 = var_base;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__Configurable__impl_class );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 356;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_base );
        tmp_source_name_2 = var_base;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__Configurable__impl_kwargs );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 356;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_09ff696e4dd27517039c78810447018d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_09ff696e4dd27517039c78810447018d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_09ff696e4dd27517039c78810447018d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_09ff696e4dd27517039c78810447018d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_09ff696e4dd27517039c78810447018d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_09ff696e4dd27517039c78810447018d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_09ff696e4dd27517039c78810447018d,
        type_description_1,
        par_cls,
        var_base
    );


    // Release cached frame.
    if ( frame_09ff696e4dd27517039c78810447018d == cache_frame_09ff696e4dd27517039c78810447018d )
    {
        Py_DECREF( frame_09ff696e4dd27517039c78810447018d );
    }
    cache_frame_09ff696e4dd27517039c78810447018d = NULL;

    assertFrameObject( frame_09ff696e4dd27517039c78810447018d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_20__save_configuration );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_base );
    Py_DECREF( var_base );
    var_base = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( var_base );
    var_base = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_20__save_configuration );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_21__restore_configuration( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_saved = python_pars[ 1 ];
    PyObject *var_base = NULL;
    struct Nuitka_FrameObject *frame_6410c089076f8efe977f5e67b4a772e1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_6410c089076f8efe977f5e67b4a772e1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6410c089076f8efe977f5e67b4a772e1, codeobj_6410c089076f8efe977f5e67b4a772e1, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6410c089076f8efe977f5e67b4a772e1 = cache_frame_6410c089076f8efe977f5e67b4a772e1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6410c089076f8efe977f5e67b4a772e1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6410c089076f8efe977f5e67b4a772e1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_cls );
        tmp_called_instance_1 = par_cls;
        frame_6410c089076f8efe977f5e67b4a772e1->m_frame.f_lineno = 361;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_configurable_base );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_base == NULL );
        var_base = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_saved );
        tmp_subscribed_name_1 = par_saved;
        tmp_subscript_name_1 = const_int_0;
        tmp_assattr_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 362;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_base );
        tmp_assattr_target_1 = var_base;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__Configurable__impl_class, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 362;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_saved );
        tmp_subscribed_name_2 = par_saved;
        tmp_subscript_name_2 = const_int_pos_1;
        tmp_assattr_name_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 363;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_base );
        tmp_assattr_target_2 = var_base;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__Configurable__impl_kwargs, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 363;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6410c089076f8efe977f5e67b4a772e1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6410c089076f8efe977f5e67b4a772e1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6410c089076f8efe977f5e67b4a772e1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6410c089076f8efe977f5e67b4a772e1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6410c089076f8efe977f5e67b4a772e1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6410c089076f8efe977f5e67b4a772e1,
        type_description_1,
        par_cls,
        par_saved,
        var_base
    );


    // Release cached frame.
    if ( frame_6410c089076f8efe977f5e67b4a772e1 == cache_frame_6410c089076f8efe977f5e67b4a772e1 )
    {
        Py_DECREF( frame_6410c089076f8efe977f5e67b4a772e1 );
    }
    cache_frame_6410c089076f8efe977f5e67b4a772e1 = NULL;

    assertFrameObject( frame_6410c089076f8efe977f5e67b4a772e1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_21__restore_configuration );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_saved );
    Py_DECREF( par_saved );
    par_saved = NULL;

    CHECK_OBJECT( (PyObject *)var_base );
    Py_DECREF( var_base );
    var_base = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_saved );
    Py_DECREF( par_saved );
    par_saved = NULL;

    Py_XDECREF( var_base );
    var_base = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_21__restore_configuration );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_22___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_func = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0, codeobj_63b6152ad6827cdf3d3c6b3cfbf3e2c0, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 = cache_frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_name );
        tmp_assattr_name_1 = par_name;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_name, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 375;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( par_func );
        tmp_args_element_name_1 = par_func;
        frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0->m_frame.f_lineno = 377;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_instance_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain__getargnames, call_args );
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 377;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_name );
        tmp_args_element_name_2 = par_name;
        frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0->m_frame.f_lineno = 377;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assattr_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_index, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 377;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_arg_pos, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 377;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ValueError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 378;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_3;
            PyObject *tmp_assattr_target_3;
            tmp_assattr_name_3 = Py_None;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_3 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_arg_pos, tmp_assattr_name_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 380;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 376;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0->m_frame) frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_22___init__ );
    return NULL;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0,
        type_description_1,
        par_self,
        par_func,
        par_name
    );


    // Release cached frame.
    if ( frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 == cache_frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 )
    {
        Py_DECREF( frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 );
    }
    cache_frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 = NULL;

    assertFrameObject( frame_63b6152ad6827cdf3d3c6b3cfbf3e2c0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_22___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_22___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_23__getargnames( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_func = python_pars[ 1 ];
    PyObject *var_code = NULL;
    struct Nuitka_FrameObject *frame_723fc94d060ebeac9cdffa9728b5fd1c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_723fc94d060ebeac9cdffa9728b5fd1c = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_723fc94d060ebeac9cdffa9728b5fd1c, codeobj_723fc94d060ebeac9cdffa9728b5fd1c, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_723fc94d060ebeac9cdffa9728b5fd1c = cache_frame_723fc94d060ebeac9cdffa9728b5fd1c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_723fc94d060ebeac9cdffa9728b5fd1c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_723fc94d060ebeac9cdffa9728b5fd1c ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_getfullargspec );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_getfullargspec );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "getfullargspec" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 384;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_func );
        tmp_args_element_name_1 = par_func;
        frame_723fc94d060ebeac9cdffa9728b5fd1c->m_frame.f_lineno = 384;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_source_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 384;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_args );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 384;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_23__getargnames );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_723fc94d060ebeac9cdffa9728b5fd1c, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_723fc94d060ebeac9cdffa9728b5fd1c, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_TypeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 385;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_attribute_name_1;
            CHECK_OBJECT( par_func );
            tmp_source_name_2 = par_func;
            tmp_attribute_name_1 = const_str_plain_func_code;
            tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_2, tmp_attribute_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 386;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_1;
                PyObject *tmp_source_name_3;
                CHECK_OBJECT( par_func );
                tmp_source_name_3 = par_func;
                tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_func_code );
                if ( tmp_assign_source_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 393;
                    type_description_1 = "ooo";
                    goto try_except_handler_3;
                }
                assert( var_code == NULL );
                var_code = tmp_assign_source_1;
            }
            {
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_subscript_name_1;
                PyObject *tmp_start_name_1;
                PyObject *tmp_stop_name_1;
                PyObject *tmp_source_name_5;
                PyObject *tmp_step_name_1;
                CHECK_OBJECT( var_code );
                tmp_source_name_4 = var_code;
                tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_co_varnames );
                if ( tmp_subscribed_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 394;
                    type_description_1 = "ooo";
                    goto try_except_handler_3;
                }
                tmp_start_name_1 = Py_None;
                CHECK_OBJECT( var_code );
                tmp_source_name_5 = var_code;
                tmp_stop_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_co_argcount );
                if ( tmp_stop_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_subscribed_name_1 );

                    exception_lineno = 394;
                    type_description_1 = "ooo";
                    goto try_except_handler_3;
                }
                tmp_step_name_1 = Py_None;
                tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
                Py_DECREF( tmp_stop_name_1 );
                assert( !(tmp_subscript_name_1 == NULL) );
                tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                Py_DECREF( tmp_subscribed_name_1 );
                Py_DECREF( tmp_subscript_name_1 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 394;
                    type_description_1 = "ooo";
                    goto try_except_handler_3;
                }
                goto try_return_handler_3;
            }
            branch_no_2:;
        }
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 395;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_723fc94d060ebeac9cdffa9728b5fd1c->m_frame) frame_723fc94d060ebeac9cdffa9728b5fd1c->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooo";
        goto try_except_handler_3;
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 383;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_723fc94d060ebeac9cdffa9728b5fd1c->m_frame) frame_723fc94d060ebeac9cdffa9728b5fd1c->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_23__getargnames );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_723fc94d060ebeac9cdffa9728b5fd1c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_723fc94d060ebeac9cdffa9728b5fd1c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_723fc94d060ebeac9cdffa9728b5fd1c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_723fc94d060ebeac9cdffa9728b5fd1c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_723fc94d060ebeac9cdffa9728b5fd1c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_723fc94d060ebeac9cdffa9728b5fd1c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_723fc94d060ebeac9cdffa9728b5fd1c,
        type_description_1,
        par_self,
        par_func,
        var_code
    );


    // Release cached frame.
    if ( frame_723fc94d060ebeac9cdffa9728b5fd1c == cache_frame_723fc94d060ebeac9cdffa9728b5fd1c )
    {
        Py_DECREF( frame_723fc94d060ebeac9cdffa9728b5fd1c );
    }
    cache_frame_723fc94d060ebeac9cdffa9728b5fd1c = NULL;

    assertFrameObject( frame_723fc94d060ebeac9cdffa9728b5fd1c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_23__getargnames );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    Py_XDECREF( var_code );
    var_code = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    Py_XDECREF( var_code );
    var_code = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_23__getargnames );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_24_get_old_value( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *par_default = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_7c9ad147041b211cf995a6f84c911a2c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_7c9ad147041b211cf995a6f84c911a2c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7c9ad147041b211cf995a6f84c911a2c, codeobj_7c9ad147041b211cf995a6f84c911a2c, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7c9ad147041b211cf995a6f84c911a2c = cache_frame_7c9ad147041b211cf995a6f84c911a2c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7c9ad147041b211cf995a6f84c911a2c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7c9ad147041b211cf995a6f84c911a2c ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_arg_pos );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 404;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_args );
        tmp_len_arg_1 = par_args;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 404;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg_pos );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_2 );

            exception_lineno = 404;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 404;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_args );
            tmp_subscribed_name_1 = par_args;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_subscript_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg_pos );
            if ( tmp_subscript_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 405;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscript_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 405;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_5;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_kwargs );
            tmp_source_name_4 = par_kwargs;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_get );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 407;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_name );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 407;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_default );
            tmp_args_element_name_2 = par_default;
            frame_7c9ad147041b211cf995a6f84c911a2c->m_frame.f_lineno = 407;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 407;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7c9ad147041b211cf995a6f84c911a2c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7c9ad147041b211cf995a6f84c911a2c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7c9ad147041b211cf995a6f84c911a2c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7c9ad147041b211cf995a6f84c911a2c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7c9ad147041b211cf995a6f84c911a2c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7c9ad147041b211cf995a6f84c911a2c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7c9ad147041b211cf995a6f84c911a2c,
        type_description_1,
        par_self,
        par_args,
        par_kwargs,
        par_default
    );


    // Release cached frame.
    if ( frame_7c9ad147041b211cf995a6f84c911a2c == cache_frame_7c9ad147041b211cf995a6f84c911a2c )
    {
        Py_DECREF( frame_7c9ad147041b211cf995a6f84c911a2c );
    }
    cache_frame_7c9ad147041b211cf995a6f84c911a2c = NULL;

    assertFrameObject( frame_7c9ad147041b211cf995a6f84c911a2c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_24_get_old_value );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_24_get_old_value );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_25_replace( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_new_value = python_pars[ 1 ];
    PyObject *par_args = python_pars[ 2 ];
    PyObject *par_kwargs = python_pars[ 3 ];
    PyObject *var_old_value = NULL;
    struct Nuitka_FrameObject *frame_d97dd586f751fcc872be542c19f9ebae;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_d97dd586f751fcc872be542c19f9ebae = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d97dd586f751fcc872be542c19f9ebae, codeobj_d97dd586f751fcc872be542c19f9ebae, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d97dd586f751fcc872be542c19f9ebae = cache_frame_d97dd586f751fcc872be542c19f9ebae;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d97dd586f751fcc872be542c19f9ebae );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d97dd586f751fcc872be542c19f9ebae ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_arg_pos );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 421;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_args );
        tmp_len_arg_1 = par_args;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 421;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg_pos );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_2 );

            exception_lineno = 421;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 421;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_args );
            tmp_subscribed_name_1 = par_args;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_subscript_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg_pos );
            if ( tmp_subscript_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 423;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscript_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 423;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_old_value == NULL );
            var_old_value = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_list_arg_1;
            CHECK_OBJECT( par_args );
            tmp_list_arg_1 = par_args;
            tmp_assign_source_2 = PySequence_List( tmp_list_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 424;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_args;
                assert( old != NULL );
                par_args = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_new_value );
            tmp_ass_subvalue_1 = par_new_value;
            CHECK_OBJECT( par_args );
            tmp_ass_subscribed_1 = par_args;
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_ass_subscript_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_arg_pos );
            if ( tmp_ass_subscript_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 425;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subscript_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 425;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_5;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( par_kwargs );
            tmp_source_name_5 = par_kwargs;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_get );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 428;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_6 = par_self;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_name );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 428;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            frame_d97dd586f751fcc872be542c19f9ebae->m_frame.f_lineno = 428;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 428;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_old_value == NULL );
            var_old_value = tmp_assign_source_3;
        }
        {
            PyObject *tmp_ass_subvalue_2;
            PyObject *tmp_ass_subscribed_2;
            PyObject *tmp_ass_subscript_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( par_new_value );
            tmp_ass_subvalue_2 = par_new_value;
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_2 = par_kwargs;
            CHECK_OBJECT( par_self );
            tmp_source_name_7 = par_self;
            tmp_ass_subscript_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_name );
            if ( tmp_ass_subscript_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 429;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
            Py_DECREF( tmp_ass_subscript_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 429;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d97dd586f751fcc872be542c19f9ebae );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d97dd586f751fcc872be542c19f9ebae );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d97dd586f751fcc872be542c19f9ebae, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d97dd586f751fcc872be542c19f9ebae->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d97dd586f751fcc872be542c19f9ebae, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d97dd586f751fcc872be542c19f9ebae,
        type_description_1,
        par_self,
        par_new_value,
        par_args,
        par_kwargs,
        var_old_value
    );


    // Release cached frame.
    if ( frame_d97dd586f751fcc872be542c19f9ebae == cache_frame_d97dd586f751fcc872be542c19f9ebae )
    {
        Py_DECREF( frame_d97dd586f751fcc872be542c19f9ebae );
    }
    cache_frame_d97dd586f751fcc872be542c19f9ebae = NULL;

    assertFrameObject( frame_d97dd586f751fcc872be542c19f9ebae );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    {
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( var_old_value );
        tmp_tuple_element_1 = var_old_value;
        tmp_return_value = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_args );
        tmp_tuple_element_1 = par_args;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_tuple_element_1 = par_kwargs;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_1 );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_25_replace );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_new_value );
    Py_DECREF( par_new_value );
    par_new_value = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_old_value );
    Py_DECREF( var_old_value );
    var_old_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_new_value );
    Py_DECREF( par_new_value );
    par_new_value = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_old_value );
    var_old_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_25_replace );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_26_timedelta_to_seconds( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_td = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a6832ba0c6dc6b531750fcc2b37dc5d8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a6832ba0c6dc6b531750fcc2b37dc5d8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a6832ba0c6dc6b531750fcc2b37dc5d8, codeobj_a6832ba0c6dc6b531750fcc2b37dc5d8, module_tornado$util, sizeof(void *) );
    frame_a6832ba0c6dc6b531750fcc2b37dc5d8 = cache_frame_a6832ba0c6dc6b531750fcc2b37dc5d8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a6832ba0c6dc6b531750fcc2b37dc5d8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a6832ba0c6dc6b531750fcc2b37dc5d8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_td );
        tmp_called_instance_1 = par_td;
        frame_a6832ba0c6dc6b531750fcc2b37dc5d8->m_frame.f_lineno = 436;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_total_seconds );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 436;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a6832ba0c6dc6b531750fcc2b37dc5d8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a6832ba0c6dc6b531750fcc2b37dc5d8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a6832ba0c6dc6b531750fcc2b37dc5d8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a6832ba0c6dc6b531750fcc2b37dc5d8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a6832ba0c6dc6b531750fcc2b37dc5d8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a6832ba0c6dc6b531750fcc2b37dc5d8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a6832ba0c6dc6b531750fcc2b37dc5d8,
        type_description_1,
        par_td
    );


    // Release cached frame.
    if ( frame_a6832ba0c6dc6b531750fcc2b37dc5d8 == cache_frame_a6832ba0c6dc6b531750fcc2b37dc5d8 )
    {
        Py_DECREF( frame_a6832ba0c6dc6b531750fcc2b37dc5d8 );
    }
    cache_frame_a6832ba0c6dc6b531750fcc2b37dc5d8 = NULL;

    assertFrameObject( frame_a6832ba0c6dc6b531750fcc2b37dc5d8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_26_timedelta_to_seconds );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_td );
    Py_DECREF( par_td );
    par_td = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_td );
    Py_DECREF( par_td );
    par_td = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_26_timedelta_to_seconds );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_27__websocket_mask_python( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mask = python_pars[ 0 ];
    PyObject *par_data = python_pars[ 1 ];
    PyObject *var_mask_arr = NULL;
    PyObject *var_unmasked_arr = NULL;
    PyObject *var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_0c70212ce02c85a2b782370c4930fb55;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_0c70212ce02c85a2b782370c4930fb55 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0c70212ce02c85a2b782370c4930fb55, codeobj_0c70212ce02c85a2b782370c4930fb55, module_tornado$util, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0c70212ce02c85a2b782370c4930fb55 = cache_frame_0c70212ce02c85a2b782370c4930fb55;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0c70212ce02c85a2b782370c4930fb55 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0c70212ce02c85a2b782370c4930fb55 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_array );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_array );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "array" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 448;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        tmp_args_element_name_1 = const_str_plain_B;
        CHECK_OBJECT( par_mask );
        tmp_args_element_name_2 = par_mask;
        frame_0c70212ce02c85a2b782370c4930fb55->m_frame.f_lineno = 448;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_array, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 448;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_mask_arr == NULL );
        var_mask_arr = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_array );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_array );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "array" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 449;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        tmp_args_element_name_3 = const_str_plain_B;
        CHECK_OBJECT( par_data );
        tmp_args_element_name_4 = par_data;
        frame_0c70212ce02c85a2b782370c4930fb55->m_frame.f_lineno = 449;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_array, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 449;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_unmasked_arr == NULL );
        var_unmasked_arr = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_xrange_low_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( par_data );
        tmp_len_arg_1 = par_data;
        tmp_xrange_low_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_xrange_low_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 450;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_iter_arg_1 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
        Py_DECREF( tmp_xrange_low_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 450;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 450;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                exception_lineno = 450;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_5;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_ass_subscript_1;
        CHECK_OBJECT( var_unmasked_arr );
        tmp_subscribed_name_1 = var_unmasked_arr;
        CHECK_OBJECT( var_i );
        tmp_subscript_name_1 = var_i;
        tmp_left_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 451;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_mask_arr );
        tmp_subscribed_name_2 = var_mask_arr;
        CHECK_OBJECT( var_i );
        tmp_left_name_2 = var_i;
        tmp_right_name_2 = const_int_pos_4;
        tmp_subscript_name_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_subscript_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 451;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_right_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        Py_DECREF( tmp_subscript_name_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 451;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_ass_subvalue_1 = BINARY_OPERATION( PyNumber_Xor, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_ass_subvalue_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 451;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_unmasked_arr );
        tmp_ass_subscribed_1 = var_unmasked_arr;
        CHECK_OBJECT( var_i );
        tmp_ass_subscript_1 = var_i;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 451;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 450;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( var_unmasked_arr );
        tmp_called_instance_3 = var_unmasked_arr;
        frame_0c70212ce02c85a2b782370c4930fb55->m_frame.f_lineno = 452;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_tobytes );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 452;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c70212ce02c85a2b782370c4930fb55 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c70212ce02c85a2b782370c4930fb55 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c70212ce02c85a2b782370c4930fb55 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0c70212ce02c85a2b782370c4930fb55, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0c70212ce02c85a2b782370c4930fb55->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0c70212ce02c85a2b782370c4930fb55, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0c70212ce02c85a2b782370c4930fb55,
        type_description_1,
        par_mask,
        par_data,
        var_mask_arr,
        var_unmasked_arr,
        var_i
    );


    // Release cached frame.
    if ( frame_0c70212ce02c85a2b782370c4930fb55 == cache_frame_0c70212ce02c85a2b782370c4930fb55 )
    {
        Py_DECREF( frame_0c70212ce02c85a2b782370c4930fb55 );
    }
    cache_frame_0c70212ce02c85a2b782370c4930fb55 = NULL;

    assertFrameObject( frame_0c70212ce02c85a2b782370c4930fb55 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_27__websocket_mask_python );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_mask );
    Py_DECREF( par_mask );
    par_mask = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    CHECK_OBJECT( (PyObject *)var_mask_arr );
    Py_DECREF( var_mask_arr );
    var_mask_arr = NULL;

    CHECK_OBJECT( (PyObject *)var_unmasked_arr );
    Py_DECREF( var_unmasked_arr );
    var_unmasked_arr = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_mask );
    Py_DECREF( par_mask );
    par_mask = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    Py_XDECREF( var_mask_arr );
    var_mask_arr = NULL;

    Py_XDECREF( var_unmasked_arr );
    var_unmasked_arr = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_27__websocket_mask_python );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$util$$$function_28_doctests( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_doctest = NULL;
    struct Nuitka_FrameObject *frame_31d78de1af917a9cbaa6954c3648fb9e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_31d78de1af917a9cbaa6954c3648fb9e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_31d78de1af917a9cbaa6954c3648fb9e, codeobj_31d78de1af917a9cbaa6954c3648fb9e, module_tornado$util, sizeof(void *) );
    frame_31d78de1af917a9cbaa6954c3648fb9e = cache_frame_31d78de1af917a9cbaa6954c3648fb9e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_31d78de1af917a9cbaa6954c3648fb9e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_31d78de1af917a9cbaa6954c3648fb9e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_doctest;
        tmp_globals_name_1 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_31d78de1af917a9cbaa6954c3648fb9e->m_frame.f_lineno = 470;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 470;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        assert( var_doctest == NULL );
        var_doctest = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_doctest );
        tmp_called_instance_1 = var_doctest;
        frame_31d78de1af917a9cbaa6954c3648fb9e->m_frame.f_lineno = 472;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_DocTestSuite );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 472;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_31d78de1af917a9cbaa6954c3648fb9e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_31d78de1af917a9cbaa6954c3648fb9e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_31d78de1af917a9cbaa6954c3648fb9e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_31d78de1af917a9cbaa6954c3648fb9e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_31d78de1af917a9cbaa6954c3648fb9e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_31d78de1af917a9cbaa6954c3648fb9e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_31d78de1af917a9cbaa6954c3648fb9e,
        type_description_1,
        var_doctest
    );


    // Release cached frame.
    if ( frame_31d78de1af917a9cbaa6954c3648fb9e == cache_frame_31d78de1af917a9cbaa6954c3648fb9e )
    {
        Py_DECREF( frame_31d78de1af917a9cbaa6954c3648fb9e );
    }
    cache_frame_31d78de1af917a9cbaa6954c3648fb9e = NULL;

    assertFrameObject( frame_31d78de1af917a9cbaa6954c3648fb9e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_28_doctests );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_doctest );
    Py_DECREF( var_doctest );
    var_doctest = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_doctest );
    var_doctest = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$util$$$function_28_doctests );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_10_raise_exc_info(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_10_raise_exc_info,
        const_str_plain_raise_exc_info,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5c05df6f3541e877d3f2485f49d7fe72,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_11_errno_from_exception( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_11_errno_from_exception,
        const_str_plain_errno_from_exception,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cd092f1f8d845da96187aab3d6b2f9d1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        const_str_digest_1b4769834b8adf886823f16c27fff0a0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_12__re_unescape_replacement( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_12__re_unescape_replacement,
        const_str_plain__re_unescape_replacement,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1b00f2100869eb9199ddf9717b4f1ce3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_13_re_unescape( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_13_re_unescape,
        const_str_plain_re_unescape,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_257d45a16f36f6acf4b1c6d6951f102e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        const_str_digest_cc6c48138d09feaa33845c6570d39498,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_14___new__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_14___new__,
        const_str_plain___new__,
#if PYTHON_VERSION >= 300
        const_str_digest_88a6dfa9fdff870b7640d663cf05e5b9,
#endif
        codeobj_a2ed4f3e006fcf9e294bc18f3c4f4149,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_15_configurable_base(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_15_configurable_base,
        const_str_plain_configurable_base,
#if PYTHON_VERSION >= 300
        const_str_digest_ebcdac424df8af7b8c4b22d8cd1278fe,
#endif
        codeobj_84384e4a4872c31258d50896b891c2f1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$util,
        const_str_digest_47bc36a6ced69c6c9d22df57da5ecc74,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_16_configurable_default(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_16_configurable_default,
        const_str_plain_configurable_default,
#if PYTHON_VERSION >= 300
        const_str_digest_7a2c19df49e284f40d3fa8d5fbd4afce,
#endif
        codeobj_980fe3ca36fafcffb6ecb31e32e0123e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$util,
        const_str_digest_5a48d4a73c11265f4ad8a7c657f1c1a7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_17__initialize( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_17__initialize,
        const_str_plain__initialize,
#if PYTHON_VERSION >= 300
        const_str_digest_b4b0675d44b07c3b99d7c6d699ec8a1d,
#endif
        codeobj_05f3e7ba7b4a8c405a6dca3b3493902a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_18_configure(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_18_configure,
        const_str_plain_configure,
#if PYTHON_VERSION >= 300
        const_str_digest_dbe92e62e5d9e0a6a62a95576c758970,
#endif
        codeobj_d4e9a1d0a4784dfbe1b1369f4150d79e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$util,
        const_str_digest_ddc876bf1486902775cf08194098942b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_19_configured_class(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_19_configured_class,
        const_str_plain_configured_class,
#if PYTHON_VERSION >= 300
        const_str_digest_935bde0817e8592d5b39269641a93bc0,
#endif
        codeobj_e7efdb96952213371a786ae45a9ca0b2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$util,
        const_str_digest_f18fcfc7c7c8b1a40c9dd32185e7a4c4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_20__save_configuration(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_20__save_configuration,
        const_str_plain__save_configuration,
#if PYTHON_VERSION >= 300
        const_str_digest_19c968eae53f4211f9d52acdbf25a2db,
#endif
        codeobj_09ff696e4dd27517039c78810447018d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_21__restore_configuration(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_21__restore_configuration,
        const_str_plain__restore_configuration,
#if PYTHON_VERSION >= 300
        const_str_digest_574ab65c4003836f8d427db97abdb817,
#endif
        codeobj_6410c089076f8efe977f5e67b4a772e1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_22___init__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_22___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_20c0dd37e7ef89358b9d393e4e00b9a6,
#endif
        codeobj_63b6152ad6827cdf3d3c6b3cfbf3e2c0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_23__getargnames( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_23__getargnames,
        const_str_plain__getargnames,
#if PYTHON_VERSION >= 300
        const_str_digest_935608fc17ea27c1bbd653d406b87466,
#endif
        codeobj_723fc94d060ebeac9cdffa9728b5fd1c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_24_get_old_value( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_24_get_old_value,
        const_str_plain_get_old_value,
#if PYTHON_VERSION >= 300
        const_str_digest_e64394b644787251958d63ad9f861876,
#endif
        codeobj_7c9ad147041b211cf995a6f84c911a2c,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        const_str_digest_3c8ff2d5d3f005bbdbf00d3f7622174c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_25_replace( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_25_replace,
        const_str_plain_replace,
#if PYTHON_VERSION >= 300
        const_str_digest_2cbd99f500eda850728e88ec283dd8e7,
#endif
        codeobj_d97dd586f751fcc872be542c19f9ebae,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        const_str_digest_a450a944395eb8127f5d662fed728c44,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_26_timedelta_to_seconds(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_26_timedelta_to_seconds,
        const_str_plain_timedelta_to_seconds,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a6832ba0c6dc6b531750fcc2b37dc5d8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$util,
        const_str_digest_9f60da2cb4b1130e5280ffc6e52b6956,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_27__websocket_mask_python( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_27__websocket_mask_python,
        const_str_plain__websocket_mask_python,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0c70212ce02c85a2b782370c4930fb55,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        const_str_digest_c68e49781bd27a934da377701a611f69,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_28_doctests(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_28_doctests,
        const_str_plain_doctests,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_31d78de1af917a9cbaa6954c3648fb9e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_2___getattr__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_2___getattr__,
        const_str_plain___getattr__,
#if PYTHON_VERSION >= 300
        const_str_digest_a57c123a560c6211ae39bbf72ce5c737,
#endif
        codeobj_f38be7bf9b09aa9609f3600214426748,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_3___setattr__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_3___setattr__,
        const_str_plain___setattr__,
#if PYTHON_VERSION >= 300
        const_str_digest_6f69bd8b8ae3e2f5791f34f2b5329e8c,
#endif
        codeobj_a3b46041ab19d64777912bcb7918b623,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_4___init__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_4___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_9e1addef3003778e105e2f66316a89b9,
#endif
        codeobj_6ac873c49303f625aa5136e5b23e17e9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_5_decompress( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_5_decompress,
        const_str_plain_decompress,
#if PYTHON_VERSION >= 300
        const_str_digest_cc1cb68a0f40a41e31766338816a6f4c,
#endif
        codeobj_f2605a13068cc4eeec560fbbb234fd45,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        const_str_digest_82535f6c4ce38866e752a9b4363412df,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_6_unconsumed_tail( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_6_unconsumed_tail,
        const_str_plain_unconsumed_tail,
#if PYTHON_VERSION >= 300
        const_str_digest_79dedeb07d6730dcdd470d61ac4804cc,
#endif
        codeobj_b7907cccddd37366ccb6e47ac66fb87e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        const_str_digest_dfbc515db61895e3c8b945f78f7aee54,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_7_flush( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_7_flush,
        const_str_plain_flush,
#if PYTHON_VERSION >= 300
        const_str_digest_fadf4c2caad38d1b66f5de5591ac5ff3,
#endif
        codeobj_b8281247d40d9b17e45d20a264446d50,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        const_str_digest_aa8e14ce57bf148b353f4b6d303a0453,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_8_import_object( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_8_import_object,
        const_str_plain_import_object,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_eb34eda37ebba44ab9440b9f65023306,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        const_str_digest_2f9db9dd8f2cd34d9565b9b0d7c0e8d8,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$util$$$function_9_exec_in( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$util$$$function_9_exec_in,
        const_str_plain_exec_in,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_09bba29479aa3dd8961d4c90ebf41215,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$util,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_tornado$util =
{
    PyModuleDef_HEAD_INIT,
    "tornado.util",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(tornado$util)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(tornado$util)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_tornado$util );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("tornado.util: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.util: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.util: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in inittornado$util" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_tornado$util = Py_InitModule4(
        "tornado.util",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_tornado$util = PyModule_Create( &mdef_tornado$util );
#endif

    moduledict_tornado$util = MODULE_DICT( module_tornado$util );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_tornado$util,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_tornado$util,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$util,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$util,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_tornado$util );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_e077944e15accbec54ecb40fd81dafde, module_tornado$util );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *outline_3_var___class__ = NULL;
    PyObject *outline_4_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_class_creation_4__bases = NULL;
    PyObject *tmp_class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_class_creation_4__metaclass = NULL;
    PyObject *tmp_class_creation_4__prepared = NULL;
    PyObject *tmp_class_creation_5__bases = NULL;
    PyObject *tmp_class_creation_5__class_decl_dict = NULL;
    PyObject *tmp_class_creation_5__metaclass = NULL;
    PyObject *tmp_class_creation_5__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_a9082c6ea4640ddf3d0abc86d3d62007;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_tornado$util_66 = NULL;
    PyObject *tmp_dictset_value;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *locals_tornado$util_76 = NULL;
    struct Nuitka_FrameObject *frame_8669ad0f885f7ec8b6e63b5e494f408d_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_8669ad0f885f7ec8b6e63b5e494f408d_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *locals_tornado$util_90 = NULL;
    struct Nuitka_FrameObject *frame_e425237ef3be6f1f62689b31b6ae3b2a_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_e425237ef3be6f1f62689b31b6ae3b2a_3 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *locals_tornado$util_231 = NULL;
    struct Nuitka_FrameObject *frame_379191b0739aade14519ed48ff03178b_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_379191b0739aade14519ed48ff03178b_4 = NULL;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *locals_tornado$util_366 = NULL;
    struct Nuitka_FrameObject *frame_0fc66817e130a11d02ef07ff3a593999_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    static struct Nuitka_FrameObject *cache_frame_0fc66817e130a11d02ef07ff3a593999_5 = NULL;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_829c095ccd3a9904c416c67e20611573;
        UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_a9082c6ea4640ddf3d0abc86d3d62007 = MAKE_MODULE_FRAME( codeobj_a9082c6ea4640ddf3d0abc86d3d62007, module_tornado$util );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_a9082c6ea4640ddf3d0abc86d3d62007 );
    assert( Py_REFCNT( frame_a9082c6ea4640ddf3d0abc86d3d62007 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_array;
        tmp_globals_name_1 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 13;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_array, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_atexit;
        tmp_globals_name_2 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 14;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        assert( !(tmp_assign_source_5 == NULL) );
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_atexit, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_inspect;
        tmp_globals_name_3 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_getfullargspec_tuple;
        tmp_level_name_3 = const_int_0;
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 15;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_getfullargspec );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_getfullargspec, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_os;
        tmp_globals_name_4 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 16;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_re;
        tmp_globals_name_5 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 17;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_typing;
        tmp_globals_name_6 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 18;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_typing, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_zlib;
        tmp_globals_name_7 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = Py_None;
        tmp_level_name_7 = const_int_0;
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 19;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_zlib, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_plain_typing;
        tmp_globals_name_8 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_84febd244317fb811a69dca764de7038_tuple;
        tmp_level_name_8 = const_int_0;
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 21;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_11;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Any );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_Optional );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Optional, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Dict );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Dict, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_Mapping );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Mapping, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_6 = tmp_import_from_1__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_List );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_List, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_7 = tmp_import_from_1__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_Tuple );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Tuple, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_8 = tmp_import_from_1__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_Match );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Match, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_9 = tmp_import_from_1__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_Callable );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Callable, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_10 = tmp_import_from_1__module;
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_Type );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Type, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_11 = tmp_import_from_1__module;
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_Sequence );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Sequence, tmp_assign_source_21 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 34;

            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_TYPE_CHECKING );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_name_name_9;
            PyObject *tmp_globals_name_9;
            PyObject *tmp_locals_name_9;
            PyObject *tmp_fromlist_name_9;
            PyObject *tmp_level_name_9;
            tmp_name_name_9 = const_str_plain_datetime;
            tmp_globals_name_9 = (PyObject *)moduledict_tornado$util;
            tmp_locals_name_9 = Py_None;
            tmp_fromlist_name_9 = Py_None;
            tmp_level_name_9 = const_int_0;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 37;
            tmp_assign_source_22 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_datetime, tmp_assign_source_22 );
        }
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_import_name_from_12;
            PyObject *tmp_name_name_10;
            PyObject *tmp_globals_name_10;
            PyObject *tmp_locals_name_10;
            PyObject *tmp_fromlist_name_10;
            PyObject *tmp_level_name_10;
            tmp_name_name_10 = const_str_plain_types;
            tmp_globals_name_10 = (PyObject *)moduledict_tornado$util;
            tmp_locals_name_10 = Py_None;
            tmp_fromlist_name_10 = const_tuple_str_plain_TracebackType_tuple;
            tmp_level_name_10 = const_int_0;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 38;
            tmp_import_name_from_12 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
            if ( tmp_import_name_from_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;

                goto frame_exception_exit_1;
            }
            tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_TracebackType );
            Py_DECREF( tmp_import_name_from_12 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_TracebackType, tmp_assign_source_23 );
        }
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_import_name_from_13;
            PyObject *tmp_name_name_11;
            PyObject *tmp_globals_name_11;
            PyObject *tmp_locals_name_11;
            PyObject *tmp_fromlist_name_11;
            PyObject *tmp_level_name_11;
            tmp_name_name_11 = const_str_plain_typing;
            tmp_globals_name_11 = (PyObject *)moduledict_tornado$util;
            tmp_locals_name_11 = Py_None;
            tmp_fromlist_name_11 = const_tuple_str_plain_Union_tuple;
            tmp_level_name_11 = const_int_0;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 39;
            tmp_import_name_from_13 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
            if ( tmp_import_name_from_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;

                goto frame_exception_exit_1;
            }
            tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_Union );
            Py_DECREF( tmp_import_name_from_13 );
            if ( tmp_assign_source_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Union, tmp_assign_source_24 );
        }
        {
            PyObject *tmp_assign_source_25;
            PyObject *tmp_name_name_12;
            PyObject *tmp_globals_name_12;
            PyObject *tmp_locals_name_12;
            PyObject *tmp_fromlist_name_12;
            PyObject *tmp_level_name_12;
            tmp_name_name_12 = const_str_plain_unittest;
            tmp_globals_name_12 = (PyObject *)moduledict_tornado$util;
            tmp_locals_name_12 = Py_None;
            tmp_fromlist_name_12 = Py_None;
            tmp_level_name_12 = const_int_0;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 40;
            tmp_assign_source_25 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
            if ( tmp_assign_source_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_unittest, tmp_assign_source_25 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = (PyObject *)&PyBytes_Type;
        UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_bytes_type, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = (PyObject *)&PyUnicode_Type;
        UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_unicode_type, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = (PyObject *)&PyUnicode_Type;
        UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_basestring_type, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_import_name_from_14;
        PyObject *tmp_name_name_13;
        PyObject *tmp_globals_name_13;
        PyObject *tmp_locals_name_13;
        PyObject *tmp_fromlist_name_13;
        PyObject *tmp_level_name_13;
        tmp_name_name_13 = const_str_plain_sys;
        tmp_globals_name_13 = (PyObject *)moduledict_tornado$util;
        tmp_locals_name_13 = Py_None;
        tmp_fromlist_name_13 = const_tuple_str_plain_is_finalizing_tuple;
        tmp_level_name_13 = const_int_0;
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 50;
        tmp_import_name_from_14 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
        assert( !(tmp_import_name_from_14 == NULL) );
        tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_is_finalizing );
        Py_DECREF( tmp_import_name_from_14 );
        assert( !(tmp_assign_source_29 == NULL) );
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_is_finalizing, tmp_assign_source_29 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_Exception_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_30 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_30;
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_2;
        }
        tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_32 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_32;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_2;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_2;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_2 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_2, const_str_plain___prepare__ );
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_3 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_2;
            }
            tmp_tuple_element_1 = const_str_plain_TimeoutError;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 66;
            tmp_assign_source_33 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_33;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_4 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_4, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_2;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_5;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 66;

                    goto try_except_handler_2;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_5 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_5 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 66;

                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 66;

                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 66;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_2;
            }
            branch_no_4:;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_34;
            tmp_assign_source_34 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_34;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_35;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_tornado$util_66 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_e077944e15accbec54ecb40fd81dafde;
        tmp_res = PyObject_SetItem( locals_tornado$util_66, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_digest_8f99bee2489684f691aababdb3f30f4e;
        tmp_res = PyObject_SetItem( locals_tornado$util_66, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_plain_TimeoutError;
        tmp_res = PyObject_SetItem( locals_tornado$util_66, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto try_except_handler_4;
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_Exception_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_4;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_dictset_value = const_tuple_type_Exception_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$util_66, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_4;
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_TimeoutError;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_tornado$util_66;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 66;
            tmp_assign_source_36 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_4;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_36;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_35 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_35 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_tornado$util_66 );
        locals_tornado$util_66 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$util_66 );
        locals_tornado$util_66 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 66;
        goto try_except_handler_2;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_TimeoutError, tmp_assign_source_35 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Dict );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;

            goto try_except_handler_5;
        }

        tmp_subscribed_name_2 = tmp_mvar_value_4;
        tmp_tuple_element_5 = (PyObject *)&PyUnicode_Type;
        tmp_subscript_name_2 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_subscript_name_2, 0, tmp_tuple_element_5 );
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_subscript_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;

            goto try_except_handler_5;
        }

        tmp_tuple_element_5 = tmp_mvar_value_5;
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_subscript_name_2, 1, tmp_tuple_element_5 );
        tmp_tuple_element_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        Py_DECREF( tmp_subscript_name_2 );
        if ( tmp_tuple_element_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        tmp_assign_source_37 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_37, 0, tmp_tuple_element_4 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_37;
    }
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_38 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_38;
    }
    {
        PyObject *tmp_assign_source_39;
        tmp_assign_source_39 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_39;
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        tmp_condition_result_9 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_3 = tmp_class_creation_2__bases;
        tmp_subscript_name_3 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_40 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_40;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_5;
        }
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_6 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_6, const_str_plain___prepare__ );
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_41;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_7;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_7 = tmp_class_creation_2__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;

                goto try_except_handler_5;
            }
            tmp_tuple_element_6 = const_str_plain_ObjectDict;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 76;
            tmp_assign_source_41 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_41 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_41;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_8 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_8, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;

                goto try_except_handler_5;
            }
            tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_9;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 76;

                    goto try_except_handler_5;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_9 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_9 == NULL) );
                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_9 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 76;

                    goto try_except_handler_5;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_7 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 76;

                    goto try_except_handler_5;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 76;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_5;
            }
            branch_no_8:;
        }
        goto branch_end_7;
        branch_no_7:;
        {
            PyObject *tmp_assign_source_42;
            tmp_assign_source_42 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_42;
        }
        branch_end_7:;
    }
    {
        PyObject *tmp_assign_source_43;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_tornado$util_76 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_e077944e15accbec54ecb40fd81dafde;
        tmp_res = PyObject_SetItem( locals_tornado$util_76, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_digest_6bd7cca3362e253efd8bcd5e7df1f748;
        tmp_res = PyObject_SetItem( locals_tornado$util_76, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_plain_ObjectDict;
        tmp_res = PyObject_SetItem( locals_tornado$util_76, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto try_except_handler_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_8669ad0f885f7ec8b6e63b5e494f408d_2, codeobj_8669ad0f885f7ec8b6e63b5e494f408d, module_tornado$util, sizeof(void *) );
        frame_8669ad0f885f7ec8b6e63b5e494f408d_2 = cache_frame_8669ad0f885f7ec8b6e63b5e494f408d_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_8669ad0f885f7ec8b6e63b5e494f408d_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_8669ad0f885f7ec8b6e63b5e494f408d_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_annotations_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_mvar_value_6;
            tmp_dict_key_1 = const_str_plain_name;
            tmp_dict_value_1 = PyObject_GetItem( locals_tornado$util_76, const_str_plain_str );

            if ( tmp_dict_value_1 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_1 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_1 );
                }
            }

            tmp_annotations_1 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_2 = const_str_plain_return;
            tmp_dict_value_2 = PyObject_GetItem( locals_tornado$util_76, const_str_plain_Any );

            if ( tmp_dict_value_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_6 == NULL )
                {
                    Py_DECREF( tmp_annotations_1 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 80;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_dict_value_2 = tmp_mvar_value_6;
                Py_INCREF( tmp_dict_value_2 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_2___getattr__( tmp_annotations_1 );



            tmp_res = PyObject_SetItem( locals_tornado$util_76, const_str_plain___getattr__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_2;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            PyObject *tmp_dict_key_4;
            PyObject *tmp_dict_value_4;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_dict_key_5;
            PyObject *tmp_dict_value_5;
            tmp_dict_key_3 = const_str_plain_name;
            tmp_dict_value_3 = PyObject_GetItem( locals_tornado$util_76, const_str_plain_str );

            if ( tmp_dict_value_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_3 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_3 );
                }
            }

            tmp_annotations_2 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_2, tmp_dict_key_3, tmp_dict_value_3 );
            Py_DECREF( tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_4 = const_str_plain_value;
            tmp_dict_value_4 = PyObject_GetItem( locals_tornado$util_76, const_str_plain_Any );

            if ( tmp_dict_value_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_7 == NULL )
                {
                    Py_DECREF( tmp_annotations_2 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 86;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_dict_value_4 = tmp_mvar_value_7;
                Py_INCREF( tmp_dict_value_4 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_2, tmp_dict_key_4, tmp_dict_value_4 );
            Py_DECREF( tmp_dict_value_4 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_5 = const_str_plain_return;
            tmp_dict_value_5 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_2, tmp_dict_key_5, tmp_dict_value_5 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_3___setattr__( tmp_annotations_2 );



            tmp_res = PyObject_SetItem( locals_tornado$util_76, const_str_plain___setattr__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8669ad0f885f7ec8b6e63b5e494f408d_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8669ad0f885f7ec8b6e63b5e494f408d_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_8669ad0f885f7ec8b6e63b5e494f408d_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_8669ad0f885f7ec8b6e63b5e494f408d_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_8669ad0f885f7ec8b6e63b5e494f408d_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_8669ad0f885f7ec8b6e63b5e494f408d_2,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_8669ad0f885f7ec8b6e63b5e494f408d_2 == cache_frame_8669ad0f885f7ec8b6e63b5e494f408d_2 )
        {
            Py_DECREF( frame_8669ad0f885f7ec8b6e63b5e494f408d_2 );
        }
        cache_frame_8669ad0f885f7ec8b6e63b5e494f408d_2 = NULL;

        assertFrameObject( frame_8669ad0f885f7ec8b6e63b5e494f408d_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_7;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;

                goto try_except_handler_7;
            }
            tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_tornado$util_76, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;

                goto try_except_handler_7;
            }
            branch_no_9:;
        }
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_4 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_8 = const_str_plain_ObjectDict;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_8 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
            tmp_tuple_element_8 = locals_tornado$util_76;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 76;
            tmp_assign_source_44 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;

                goto try_except_handler_7;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_44;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_43 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_43 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        Py_DECREF( locals_tornado$util_76 );
        locals_tornado$util_76 = NULL;
        goto try_return_handler_6;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$util_76 );
        locals_tornado$util_76 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_6;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 76;
        goto try_except_handler_5;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_ObjectDict, tmp_assign_source_43 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_dircall_arg1_3;
        tmp_dircall_arg1_3 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_45 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_45;
    }
    {
        PyObject *tmp_assign_source_46;
        tmp_assign_source_46 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_46;
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_15;
        int tmp_truth_name_4;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_8;
        }
        tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_8;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_8;
        }
        tmp_condition_result_15 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_4 = tmp_class_creation_3__bases;
        tmp_subscript_name_4 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_8;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_8;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_6:;
        condexpr_end_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_47 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_47;
    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_8;
        }
        tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_8;
        }
        branch_no_10:;
    }
    {
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_source_name_10;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_10 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_10, const_str_plain___prepare__ );
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_assign_source_48;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_11;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_11 = tmp_class_creation_3__metaclass;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain___prepare__ );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;

                goto try_except_handler_8;
            }
            tmp_tuple_element_9 = const_str_plain_GzipDecompressor;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_9 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 90;
            tmp_assign_source_48 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_48 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;

                goto try_except_handler_8;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_48;
        }
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_12;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_12 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_12, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;

                goto try_except_handler_8;
            }
            tmp_condition_result_18 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_10;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_13;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_10 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 90;

                    goto try_except_handler_8;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_10 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_13 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_13 == NULL) );
                tmp_tuple_element_10 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_13 );
                if ( tmp_tuple_element_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 90;

                    goto try_except_handler_8;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_10 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 90;

                    goto try_except_handler_8;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 90;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_8;
            }
            branch_no_12:;
        }
        goto branch_end_11;
        branch_no_11:;
        {
            PyObject *tmp_assign_source_49;
            tmp_assign_source_49 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_49;
        }
        branch_end_11:;
    }
    {
        PyObject *tmp_assign_source_50;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_tornado$util_90 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_e077944e15accbec54ecb40fd81dafde;
        tmp_res = PyObject_SetItem( locals_tornado$util_90, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_10;
        }
        tmp_dictset_value = const_str_digest_f8bf07e5367861fc790a9ef86cc7c733;
        tmp_res = PyObject_SetItem( locals_tornado$util_90, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_10;
        }
        tmp_dictset_value = const_str_plain_GzipDecompressor;
        tmp_res = PyObject_SetItem( locals_tornado$util_90, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto try_except_handler_10;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_e425237ef3be6f1f62689b31b6ae3b2a_3, codeobj_e425237ef3be6f1f62689b31b6ae3b2a, module_tornado$util, sizeof(void *) );
        frame_e425237ef3be6f1f62689b31b6ae3b2a_3 = cache_frame_e425237ef3be6f1f62689b31b6ae3b2a_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_e425237ef3be6f1f62689b31b6ae3b2a_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_e425237ef3be6f1f62689b31b6ae3b2a_3 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_annotations_3;
            tmp_annotations_3 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_4___init__( tmp_annotations_3 );



            tmp_res = PyObject_SetItem( locals_tornado$util_90, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 97;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_1;
            PyObject *tmp_annotations_4;
            PyObject *tmp_dict_key_6;
            PyObject *tmp_dict_value_6;
            PyObject *tmp_dict_key_7;
            PyObject *tmp_dict_value_7;
            PyObject *tmp_dict_key_8;
            PyObject *tmp_dict_value_8;
            tmp_defaults_1 = const_tuple_int_0_tuple;
            tmp_dict_key_6 = const_str_plain_value;
            tmp_dict_value_6 = PyObject_GetItem( locals_tornado$util_90, const_str_plain_bytes );

            if ( tmp_dict_value_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_6 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_dict_value_6 );
                }
            }

            tmp_annotations_4 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_6, tmp_dict_value_6 );
            Py_DECREF( tmp_dict_value_6 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_7 = const_str_plain_max_length;
            tmp_dict_value_7 = PyObject_GetItem( locals_tornado$util_90, const_str_plain_int );

            if ( tmp_dict_value_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_7 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_7 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_7, tmp_dict_value_7 );
            Py_DECREF( tmp_dict_value_7 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_8 = const_str_plain_return;
            tmp_dict_value_8 = PyObject_GetItem( locals_tornado$util_90, const_str_plain_bytes );

            if ( tmp_dict_value_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_8 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_dict_value_8 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_8, tmp_dict_value_8 );
            Py_DECREF( tmp_dict_value_8 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_5_decompress( tmp_defaults_1, tmp_annotations_4 );



            tmp_res = PyObject_SetItem( locals_tornado$util_90, const_str_plain_decompress, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 103;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_annotations_5;
            PyObject *tmp_dict_key_9;
            PyObject *tmp_dict_value_9;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_annotations_6;
            PyObject *tmp_dict_key_10;
            PyObject *tmp_dict_value_10;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$util_90, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_condition_result_19 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_7;
            }
            else
            {
                goto condexpr_false_7;
            }
            condexpr_true_7:;
            tmp_called_name_6 = PyObject_GetItem( locals_tornado$util_90, const_str_plain_property );

            if ( tmp_called_name_6 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

            if ( tmp_called_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_dict_key_9 = const_str_plain_return;
            tmp_dict_value_9 = PyObject_GetItem( locals_tornado$util_90, const_str_plain_bytes );

            if ( tmp_dict_value_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_9 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_dict_value_9 );
                }
            }

            tmp_annotations_5 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_5, tmp_dict_key_9, tmp_dict_value_9 );
            Py_DECREF( tmp_dict_value_9 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_1 = MAKE_FUNCTION_tornado$util$$$function_6_unconsumed_tail( tmp_annotations_5 );



            frame_e425237ef3be6f1f62689b31b6ae3b2a_3->m_frame.f_lineno = 116;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            goto condexpr_end_7;
            condexpr_false_7:;
            tmp_called_name_7 = (PyObject *)&PyProperty_Type;
            tmp_dict_key_10 = const_str_plain_return;
            tmp_dict_value_10 = PyObject_GetItem( locals_tornado$util_90, const_str_plain_bytes );

            if ( tmp_dict_value_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_10 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_dict_value_10 );
                }
            }

            tmp_annotations_6 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_10, tmp_dict_value_10 );
            Py_DECREF( tmp_dict_value_10 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_2 = MAKE_FUNCTION_tornado$util$$$function_6_unconsumed_tail( tmp_annotations_6 );



            frame_e425237ef3be6f1f62689b31b6ae3b2a_3->m_frame.f_lineno = 116;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            condexpr_end_7:;
            tmp_res = PyObject_SetItem( locals_tornado$util_90, const_str_plain_unconsumed_tail, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_annotations_7;
            PyObject *tmp_dict_key_11;
            PyObject *tmp_dict_value_11;
            tmp_dict_key_11 = const_str_plain_return;
            tmp_dict_value_11 = PyObject_GetItem( locals_tornado$util_90, const_str_plain_bytes );

            if ( tmp_dict_value_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_11 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_dict_value_11 );
                }
            }

            tmp_annotations_7 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_11, tmp_dict_value_11 );
            Py_DECREF( tmp_dict_value_11 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_7_flush( tmp_annotations_7 );



            tmp_res = PyObject_SetItem( locals_tornado$util_90, const_str_plain_flush, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e425237ef3be6f1f62689b31b6ae3b2a_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e425237ef3be6f1f62689b31b6ae3b2a_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_e425237ef3be6f1f62689b31b6ae3b2a_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_e425237ef3be6f1f62689b31b6ae3b2a_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_e425237ef3be6f1f62689b31b6ae3b2a_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_e425237ef3be6f1f62689b31b6ae3b2a_3,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_e425237ef3be6f1f62689b31b6ae3b2a_3 == cache_frame_e425237ef3be6f1f62689b31b6ae3b2a_3 )
        {
            Py_DECREF( frame_e425237ef3be6f1f62689b31b6ae3b2a_3 );
        }
        cache_frame_e425237ef3be6f1f62689b31b6ae3b2a_3 = NULL;

        assertFrameObject( frame_e425237ef3be6f1f62689b31b6ae3b2a_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_10;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_20;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_3 = tmp_class_creation_3__bases;
            tmp_compexpr_right_3 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;

                goto try_except_handler_10;
            }
            tmp_condition_result_20 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_13;
            }
            else
            {
                goto branch_no_13;
            }
            branch_yes_13:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$util_90, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;

                goto try_except_handler_10;
            }
            branch_no_13:;
        }
        {
            PyObject *tmp_assign_source_51;
            PyObject *tmp_called_name_8;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_11;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_8 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_11 = const_str_plain_GzipDecompressor;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_11 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_11 );
            tmp_tuple_element_11 = locals_tornado$util_90;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 90;
            tmp_assign_source_51 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_51 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;

                goto try_except_handler_10;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_51;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_50 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_50 );
        goto try_return_handler_10;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        Py_DECREF( locals_tornado$util_90 );
        locals_tornado$util_90 = NULL;
        goto try_return_handler_9;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$util_90 );
        locals_tornado$util_90 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto try_except_handler_9;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 90;
        goto try_except_handler_8;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_GzipDecompressor, tmp_assign_source_50 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    {
        PyObject *tmp_assign_source_52;
        PyObject *tmp_annotations_8;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_mvar_value_8;
        tmp_dict_key_12 = const_str_plain_name;
        tmp_dict_value_12 = (PyObject *)&PyUnicode_Type;
        tmp_annotations_8 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_8, tmp_dict_key_12, tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_13 = const_str_plain_return;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_annotations_8 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_13 = tmp_mvar_value_8;
        tmp_res = PyDict_SetItem( tmp_annotations_8, tmp_dict_key_13, tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_52 = MAKE_FUNCTION_tornado$util$$$function_8_import_object( tmp_annotations_8 );



        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_import_object, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        PyObject *tmp_defaults_2;
        PyObject *tmp_annotations_9;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_tuple_element_12;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_tuple_element_13;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        tmp_defaults_2 = const_tuple_none_tuple;
        tmp_dict_key_14 = const_str_plain_code;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_14 = tmp_mvar_value_9;
        tmp_annotations_9 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_14, tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_15 = const_str_plain_glob;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Dict );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_annotations_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_5 = tmp_mvar_value_10;
        tmp_tuple_element_12 = (PyObject *)&PyUnicode_Type;
        tmp_subscript_name_5 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_12 );
        PyTuple_SET_ITEM( tmp_subscript_name_5, 0, tmp_tuple_element_12 );
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_annotations_9 );
            Py_DECREF( tmp_subscript_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_tuple_element_12 = tmp_mvar_value_11;
        Py_INCREF( tmp_tuple_element_12 );
        PyTuple_SET_ITEM( tmp_subscript_name_5, 1, tmp_tuple_element_12 );
        tmp_dict_value_15 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
        Py_DECREF( tmp_subscript_name_5 );
        if ( tmp_dict_value_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_annotations_9 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_15, tmp_dict_value_15 );
        Py_DECREF( tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_16 = const_str_plain_loc;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Mapping );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Mapping );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_annotations_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Mapping" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_6 = tmp_mvar_value_12;
        tmp_tuple_element_13 = (PyObject *)&PyUnicode_Type;
        tmp_subscript_name_6 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_13 );
        PyTuple_SET_ITEM( tmp_subscript_name_6, 0, tmp_tuple_element_13 );
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_annotations_9 );
            Py_DECREF( tmp_subscript_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_tuple_element_13 = tmp_mvar_value_13;
        Py_INCREF( tmp_tuple_element_13 );
        PyTuple_SET_ITEM( tmp_subscript_name_6, 1, tmp_tuple_element_13 );
        tmp_dict_value_16 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
        Py_DECREF( tmp_subscript_name_6 );
        if ( tmp_dict_value_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_annotations_9 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_16, tmp_dict_value_16 );
        Py_DECREF( tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_17 = const_str_plain_return;
        tmp_dict_value_17 = Py_None;
        tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_17, tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_53 = MAKE_FUNCTION_tornado$util$$$function_9_exec_in( tmp_defaults_2, tmp_annotations_9 );



        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_exec_in, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        tmp_assign_source_54 = MAKE_FUNCTION_tornado$util$$$function_10_raise_exc_info(  );



        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_raise_exc_info, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_annotations_10;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_subscribed_name_7;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_subscript_name_7;
        tmp_dict_key_18 = const_str_plain_e;
        tmp_dict_value_18 = PyExc_BaseException;
        tmp_annotations_10 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_18, tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_19 = const_str_plain_return;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Optional );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Optional );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_annotations_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Optional" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 188;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_7 = tmp_mvar_value_14;
        tmp_subscript_name_7 = (PyObject *)&PyLong_Type;
        tmp_dict_value_19 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
        if ( tmp_dict_value_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_annotations_10 );

            exception_lineno = 188;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_19, tmp_dict_value_19 );
        Py_DECREF( tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_55 = MAKE_FUNCTION_tornado$util$$$function_11_errno_from_exception( tmp_annotations_10 );



        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_errno_from_exception, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        tmp_assign_source_56 = const_frozenset_17f37ed7ef1fa0685d5320e9c039ff8d;
        UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__alphanum, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        PyObject *tmp_annotations_11;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_subscribed_name_8;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_subscript_name_8;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        tmp_dict_key_20 = const_str_plain_match;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Match );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Match );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Match" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 209;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_8 = tmp_mvar_value_15;
        tmp_subscript_name_8 = (PyObject *)&PyUnicode_Type;
        tmp_dict_value_20 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_8 );
        if ( tmp_dict_value_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 209;

            goto frame_exception_exit_1;
        }
        tmp_annotations_11 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_11, tmp_dict_key_20, tmp_dict_value_20 );
        Py_DECREF( tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_21 = const_str_plain_return;
        tmp_dict_value_21 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_11, tmp_dict_key_21, tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_57 = MAKE_FUNCTION_tornado$util$$$function_12__re_unescape_replacement( tmp_annotations_11 );



        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__re_unescape_replacement, tmp_assign_source_57 );
    }
    {
        PyObject *tmp_assign_source_58;
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_15;
        PyObject *tmp_mvar_value_17;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 216;

            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_16;
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_compile );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = const_str_digest_89b117bb54827b46396fd05fd8c8f20b;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_called_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 216;

            goto frame_exception_exit_1;
        }

        tmp_source_name_15 = tmp_mvar_value_17;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_DOTALL );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );

            exception_lineno = 216;

            goto frame_exception_exit_1;
        }
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 216;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_58 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__re_unescape_pattern, tmp_assign_source_58 );
    }
    {
        PyObject *tmp_assign_source_59;
        PyObject *tmp_annotations_12;
        tmp_annotations_12 = PyDict_Copy( const_dict_f12d39973b0594a0adb574dbe1e8f5f7 );
        tmp_assign_source_59 = MAKE_FUNCTION_tornado$util$$$function_13_re_unescape( tmp_annotations_12 );



        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_re_unescape, tmp_assign_source_59 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_60;
        PyObject *tmp_dircall_arg1_4;
        tmp_dircall_arg1_4 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_4 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_4};
            tmp_assign_source_60 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_11;
        }
        assert( tmp_class_creation_4__bases == NULL );
        tmp_class_creation_4__bases = tmp_assign_source_60;
    }
    {
        PyObject *tmp_assign_source_61;
        tmp_assign_source_61 = PyDict_New();
        assert( tmp_class_creation_4__class_decl_dict == NULL );
        tmp_class_creation_4__class_decl_dict = tmp_assign_source_61;
    }
    {
        PyObject *tmp_assign_source_62;
        PyObject *tmp_metaclass_name_4;
        nuitka_bool tmp_condition_result_21;
        PyObject *tmp_key_name_10;
        PyObject *tmp_dict_name_10;
        PyObject *tmp_dict_name_11;
        PyObject *tmp_key_name_11;
        nuitka_bool tmp_condition_result_22;
        int tmp_truth_name_5;
        PyObject *tmp_type_arg_7;
        PyObject *tmp_subscribed_name_9;
        PyObject *tmp_subscript_name_9;
        PyObject *tmp_bases_name_4;
        tmp_key_name_10 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_10 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_10, tmp_key_name_10 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_11;
        }
        tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_8;
        }
        else
        {
            goto condexpr_false_8;
        }
        condexpr_true_8:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_11 = tmp_class_creation_4__class_decl_dict;
        tmp_key_name_11 = const_str_plain_metaclass;
        tmp_metaclass_name_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_11;
        }
        goto condexpr_end_8;
        condexpr_false_8:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_class_creation_4__bases );
        if ( tmp_truth_name_5 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_11;
        }
        tmp_condition_result_22 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_9;
        }
        else
        {
            goto condexpr_false_9;
        }
        condexpr_true_9:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_subscribed_name_9 = tmp_class_creation_4__bases;
        tmp_subscript_name_9 = const_int_0;
        tmp_type_arg_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_9, tmp_subscript_name_9, 0 );
        if ( tmp_type_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_11;
        }
        tmp_metaclass_name_4 = BUILTIN_TYPE1( tmp_type_arg_7 );
        Py_DECREF( tmp_type_arg_7 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_11;
        }
        goto condexpr_end_9;
        condexpr_false_9:;
        tmp_metaclass_name_4 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_4 );
        condexpr_end_9:;
        condexpr_end_8:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_bases_name_4 = tmp_class_creation_4__bases;
        tmp_assign_source_62 = SELECT_METACLASS( tmp_metaclass_name_4, tmp_bases_name_4 );
        Py_DECREF( tmp_metaclass_name_4 );
        if ( tmp_assign_source_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_11;
        }
        assert( tmp_class_creation_4__metaclass == NULL );
        tmp_class_creation_4__metaclass = tmp_assign_source_62;
    }
    {
        nuitka_bool tmp_condition_result_23;
        PyObject *tmp_key_name_12;
        PyObject *tmp_dict_name_12;
        tmp_key_name_12 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_12 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_12, tmp_key_name_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_11;
        }
        tmp_condition_result_23 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_4__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_11;
        }
        branch_no_14:;
    }
    {
        nuitka_bool tmp_condition_result_24;
        PyObject *tmp_source_name_16;
        CHECK_OBJECT( tmp_class_creation_4__metaclass );
        tmp_source_name_16 = tmp_class_creation_4__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_16, const_str_plain___prepare__ );
        tmp_condition_result_24 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_15;
        }
        else
        {
            goto branch_no_15;
        }
        branch_yes_15:;
        {
            PyObject *tmp_assign_source_63;
            PyObject *tmp_called_name_10;
            PyObject *tmp_source_name_17;
            PyObject *tmp_args_name_7;
            PyObject *tmp_tuple_element_14;
            PyObject *tmp_kw_name_7;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_source_name_17 = tmp_class_creation_4__metaclass;
            tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain___prepare__ );
            if ( tmp_called_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;

                goto try_except_handler_11;
            }
            tmp_tuple_element_14 = const_str_plain_Configurable;
            tmp_args_name_7 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_14 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_7 = tmp_class_creation_4__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 231;
            tmp_assign_source_63 = CALL_FUNCTION( tmp_called_name_10, tmp_args_name_7, tmp_kw_name_7 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_args_name_7 );
            if ( tmp_assign_source_63 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;

                goto try_except_handler_11;
            }
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_63;
        }
        {
            nuitka_bool tmp_condition_result_25;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_source_name_18;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_source_name_18 = tmp_class_creation_4__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_18, const_str_plain___getitem__ );
            tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;

                goto try_except_handler_11;
            }
            tmp_condition_result_25 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_16;
            }
            else
            {
                goto branch_no_16;
            }
            branch_yes_16:;
            {
                PyObject *tmp_raise_type_4;
                PyObject *tmp_raise_value_4;
                PyObject *tmp_left_name_4;
                PyObject *tmp_right_name_4;
                PyObject *tmp_tuple_element_15;
                PyObject *tmp_getattr_target_4;
                PyObject *tmp_getattr_attr_4;
                PyObject *tmp_getattr_default_4;
                PyObject *tmp_source_name_19;
                PyObject *tmp_type_arg_8;
                tmp_raise_type_4 = PyExc_TypeError;
                tmp_left_name_4 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_4__metaclass );
                tmp_getattr_target_4 = tmp_class_creation_4__metaclass;
                tmp_getattr_attr_4 = const_str_plain___name__;
                tmp_getattr_default_4 = const_str_angle_metaclass;
                tmp_tuple_element_15 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, tmp_getattr_default_4 );
                if ( tmp_tuple_element_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 231;

                    goto try_except_handler_11;
                }
                tmp_right_name_4 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_15 );
                CHECK_OBJECT( tmp_class_creation_4__prepared );
                tmp_type_arg_8 = tmp_class_creation_4__prepared;
                tmp_source_name_19 = BUILTIN_TYPE1( tmp_type_arg_8 );
                assert( !(tmp_source_name_19 == NULL) );
                tmp_tuple_element_15 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_19 );
                if ( tmp_tuple_element_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_4 );

                    exception_lineno = 231;

                    goto try_except_handler_11;
                }
                PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_15 );
                tmp_raise_value_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                Py_DECREF( tmp_right_name_4 );
                if ( tmp_raise_value_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 231;

                    goto try_except_handler_11;
                }
                exception_type = tmp_raise_type_4;
                Py_INCREF( tmp_raise_type_4 );
                exception_value = tmp_raise_value_4;
                exception_lineno = 231;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_11;
            }
            branch_no_16:;
        }
        goto branch_end_15;
        branch_no_15:;
        {
            PyObject *tmp_assign_source_64;
            tmp_assign_source_64 = PyDict_New();
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_64;
        }
        branch_end_15:;
    }
    {
        PyObject *tmp_assign_source_65;
        {
            PyObject *tmp_set_locals_4;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_set_locals_4 = tmp_class_creation_4__prepared;
            locals_tornado$util_231 = tmp_set_locals_4;
            Py_INCREF( tmp_set_locals_4 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_e077944e15accbec54ecb40fd81dafde;
        tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_13;
        }
        tmp_dictset_value = const_str_digest_ade23dbff600199e604f0d0ba80bf77d;
        tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_13;
        }
        tmp_dictset_value = const_str_plain_Configurable;
        tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto try_except_handler_13;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_379191b0739aade14519ed48ff03178b_4, codeobj_379191b0739aade14519ed48ff03178b, module_tornado$util, sizeof(void *) );
        frame_379191b0739aade14519ed48ff03178b_4 = cache_frame_379191b0739aade14519ed48ff03178b_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_379191b0739aade14519ed48ff03178b_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_379191b0739aade14519ed48ff03178b_4 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain__Configurable__impl_class, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain__Configurable__impl_kwargs, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 267;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        {
            PyObject *tmp_annotations_13;
            PyObject *tmp_dict_key_22;
            PyObject *tmp_dict_value_22;
            PyObject *tmp_mvar_value_18;
            PyObject *tmp_dict_key_23;
            PyObject *tmp_dict_value_23;
            PyObject *tmp_mvar_value_19;
            PyObject *tmp_dict_key_24;
            PyObject *tmp_dict_value_24;
            PyObject *tmp_mvar_value_20;
            tmp_dict_key_22 = const_str_plain_args;
            tmp_dict_value_22 = PyObject_GetItem( locals_tornado$util_231, const_str_plain_Any );

            if ( tmp_dict_value_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_18 == NULL ))
                {
                    tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_18 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 269;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_dict_value_22 = tmp_mvar_value_18;
                Py_INCREF( tmp_dict_value_22 );
                }
            }

            tmp_annotations_13 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_13, tmp_dict_key_22, tmp_dict_value_22 );
            Py_DECREF( tmp_dict_value_22 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_23 = const_str_plain_kwargs;
            tmp_dict_value_23 = PyObject_GetItem( locals_tornado$util_231, const_str_plain_Any );

            if ( tmp_dict_value_23 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_19 == NULL ))
                {
                    tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_19 == NULL )
                {
                    Py_DECREF( tmp_annotations_13 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 269;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_dict_value_23 = tmp_mvar_value_19;
                Py_INCREF( tmp_dict_value_23 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_13, tmp_dict_key_23, tmp_dict_value_23 );
            Py_DECREF( tmp_dict_value_23 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_24 = const_str_plain_return;
            tmp_dict_value_24 = PyObject_GetItem( locals_tornado$util_231, const_str_plain_Any );

            if ( tmp_dict_value_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_20 == NULL ))
                {
                    tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_20 == NULL )
                {
                    Py_DECREF( tmp_annotations_13 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 269;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_dict_value_24 = tmp_mvar_value_20;
                Py_INCREF( tmp_dict_value_24 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_13, tmp_dict_key_24, tmp_dict_value_24 );
            Py_DECREF( tmp_dict_value_24 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_14___new__( tmp_annotations_13 );



            tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain___new__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 269;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_26;
            PyObject *tmp_called_name_11;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_classmethod_arg_1;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_26 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_10;
            }
            else
            {
                goto condexpr_false_10;
            }
            condexpr_true_10:;
            tmp_called_name_11 = PyObject_GetItem( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_called_name_11 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 289;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_5 = MAKE_FUNCTION_tornado$util$$$function_15_configurable_base(  );



            frame_379191b0739aade14519ed48ff03178b_4->m_frame.f_lineno = 289;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
            }

            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_10;
            condexpr_false_10:;
            tmp_classmethod_arg_1 = MAKE_FUNCTION_tornado$util$$$function_15_configurable_base(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_1 );
            Py_DECREF( tmp_classmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_10:;
            tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain_configurable_base, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_27;
            PyObject *tmp_called_name_12;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_classmethod_arg_2;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 301;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_27 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_11;
            }
            else
            {
                goto condexpr_false_11;
            }
            condexpr_true_11:;
            tmp_called_name_12 = PyObject_GetItem( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_called_name_12 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 301;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 301;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_6 = MAKE_FUNCTION_tornado$util$$$function_16_configurable_default(  );



            frame_379191b0739aade14519ed48ff03178b_4->m_frame.f_lineno = 301;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
            }

            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 301;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_11;
            condexpr_false_11:;
            tmp_classmethod_arg_2 = MAKE_FUNCTION_tornado$util$$$function_16_configurable_default(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_2 );
            Py_DECREF( tmp_classmethod_arg_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 301;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_11:;
            tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain_configurable_default, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 301;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_annotations_14;
            tmp_annotations_14 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_17__initialize( tmp_annotations_14 );



            tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain__initialize, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 307;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        tmp_dictset_value = PyObject_GetItem( locals_tornado$util_231, const_str_plain__initialize );

        if ( tmp_dictset_value == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_initialize" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 310;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }

        if ( tmp_dictset_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain_initialize, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        {
            nuitka_bool tmp_condition_result_28;
            PyObject *tmp_called_name_13;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_classmethod_arg_3;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 319;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_28 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_28 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_12;
            }
            else
            {
                goto condexpr_false_12;
            }
            condexpr_true_12:;
            tmp_called_name_13 = PyObject_GetItem( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_called_name_13 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 319;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 319;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_7 = MAKE_FUNCTION_tornado$util$$$function_18_configure(  );



            frame_379191b0739aade14519ed48ff03178b_4->m_frame.f_lineno = 319;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
            }

            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 319;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_12;
            condexpr_false_12:;
            tmp_classmethod_arg_3 = MAKE_FUNCTION_tornado$util$$$function_18_configure(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_3 );
            Py_DECREF( tmp_classmethod_arg_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 319;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_12:;
            tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain_configure, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 319;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_29;
            PyObject *tmp_called_name_14;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_classmethod_arg_4;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 336;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_29 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_29 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_13;
            }
            else
            {
                goto condexpr_false_13;
            }
            condexpr_true_13:;
            tmp_called_name_14 = PyObject_GetItem( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_called_name_14 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 336;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 336;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_8 = MAKE_FUNCTION_tornado$util$$$function_19_configured_class(  );



            frame_379191b0739aade14519ed48ff03178b_4->m_frame.f_lineno = 336;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
            }

            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 336;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_13;
            condexpr_false_13:;
            tmp_classmethod_arg_4 = MAKE_FUNCTION_tornado$util$$$function_19_configured_class(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_4 );
            Py_DECREF( tmp_classmethod_arg_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 336;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_13:;
            tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain_configured_class, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 336;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_30;
            PyObject *tmp_called_name_15;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_classmethod_arg_5;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 352;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_30 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_30 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_14;
            }
            else
            {
                goto condexpr_false_14;
            }
            condexpr_true_14:;
            tmp_called_name_15 = PyObject_GetItem( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_called_name_15 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 352;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 352;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_9 = MAKE_FUNCTION_tornado$util$$$function_20__save_configuration(  );



            frame_379191b0739aade14519ed48ff03178b_4->m_frame.f_lineno = 352;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
            }

            Py_DECREF( tmp_called_name_15 );
            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 352;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_14;
            condexpr_false_14:;
            tmp_classmethod_arg_5 = MAKE_FUNCTION_tornado$util$$$function_20__save_configuration(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_5 );
            Py_DECREF( tmp_classmethod_arg_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 352;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_14:;
            tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain__save_configuration, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 352;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_31;
            PyObject *tmp_called_name_16;
            PyObject *tmp_args_element_name_10;
            PyObject *tmp_classmethod_arg_6;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 358;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_31 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_31 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_15;
            }
            else
            {
                goto condexpr_false_15;
            }
            condexpr_true_15:;
            tmp_called_name_16 = PyObject_GetItem( locals_tornado$util_231, const_str_plain_classmethod );

            if ( tmp_called_name_16 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 358;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 358;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_10 = MAKE_FUNCTION_tornado$util$$$function_21__restore_configuration(  );



            frame_379191b0739aade14519ed48ff03178b_4->m_frame.f_lineno = 358;
            {
                PyObject *call_args[] = { tmp_args_element_name_10 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
            }

            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_args_element_name_10 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 358;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_15;
            condexpr_false_15:;
            tmp_classmethod_arg_6 = MAKE_FUNCTION_tornado$util$$$function_21__restore_configuration(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_6 );
            Py_DECREF( tmp_classmethod_arg_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 358;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_15:;
            tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain__restore_configuration, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 358;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_379191b0739aade14519ed48ff03178b_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_379191b0739aade14519ed48ff03178b_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_379191b0739aade14519ed48ff03178b_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_379191b0739aade14519ed48ff03178b_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_379191b0739aade14519ed48ff03178b_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_379191b0739aade14519ed48ff03178b_4,
            type_description_2,
            outline_3_var___class__
        );


        // Release cached frame.
        if ( frame_379191b0739aade14519ed48ff03178b_4 == cache_frame_379191b0739aade14519ed48ff03178b_4 )
        {
            Py_DECREF( frame_379191b0739aade14519ed48ff03178b_4 );
        }
        cache_frame_379191b0739aade14519ed48ff03178b_4 = NULL;

        assertFrameObject( frame_379191b0739aade14519ed48ff03178b_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_13;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_32;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_compexpr_left_4 = tmp_class_creation_4__bases;
            tmp_compexpr_right_4 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;

                goto try_except_handler_13;
            }
            tmp_condition_result_32 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_32 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_17;
            }
            else
            {
                goto branch_no_17;
            }
            branch_yes_17:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$util_231, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;

                goto try_except_handler_13;
            }
            branch_no_17:;
        }
        {
            PyObject *tmp_assign_source_66;
            PyObject *tmp_called_name_17;
            PyObject *tmp_args_name_8;
            PyObject *tmp_tuple_element_16;
            PyObject *tmp_kw_name_8;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_called_name_17 = tmp_class_creation_4__metaclass;
            tmp_tuple_element_16 = const_str_plain_Configurable;
            tmp_args_name_8 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_16 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_16 );
            tmp_tuple_element_16 = locals_tornado$util_231;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_8, 2, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_8 = tmp_class_creation_4__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 231;
            tmp_assign_source_66 = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_8, tmp_kw_name_8 );
            Py_DECREF( tmp_args_name_8 );
            if ( tmp_assign_source_66 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;

                goto try_except_handler_13;
            }
            assert( outline_3_var___class__ == NULL );
            outline_3_var___class__ = tmp_assign_source_66;
        }
        CHECK_OBJECT( outline_3_var___class__ );
        tmp_assign_source_65 = outline_3_var___class__;
        Py_INCREF( tmp_assign_source_65 );
        goto try_return_handler_13;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_13:;
        Py_DECREF( locals_tornado$util_231 );
        locals_tornado$util_231 = NULL;
        goto try_return_handler_12;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$util_231 );
        locals_tornado$util_231 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;
        exception_lineno = exception_keeper_lineno_11;

        goto try_except_handler_12;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_12:;
        CHECK_OBJECT( (PyObject *)outline_3_var___class__ );
        Py_DECREF( outline_3_var___class__ );
        outline_3_var___class__ = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_4:;
        exception_lineno = 231;
        goto try_except_handler_11;
        outline_result_4:;
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Configurable, tmp_assign_source_65 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    Py_XDECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases );
    Py_DECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__class_decl_dict );
    Py_DECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__metaclass );
    Py_DECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__prepared );
    Py_DECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_67;
        PyObject *tmp_dircall_arg1_5;
        tmp_dircall_arg1_5 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_5 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_5};
            tmp_assign_source_67 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_14;
        }
        assert( tmp_class_creation_5__bases == NULL );
        tmp_class_creation_5__bases = tmp_assign_source_67;
    }
    {
        PyObject *tmp_assign_source_68;
        tmp_assign_source_68 = PyDict_New();
        assert( tmp_class_creation_5__class_decl_dict == NULL );
        tmp_class_creation_5__class_decl_dict = tmp_assign_source_68;
    }
    {
        PyObject *tmp_assign_source_69;
        PyObject *tmp_metaclass_name_5;
        nuitka_bool tmp_condition_result_33;
        PyObject *tmp_key_name_13;
        PyObject *tmp_dict_name_13;
        PyObject *tmp_dict_name_14;
        PyObject *tmp_key_name_14;
        nuitka_bool tmp_condition_result_34;
        int tmp_truth_name_6;
        PyObject *tmp_type_arg_9;
        PyObject *tmp_subscribed_name_10;
        PyObject *tmp_subscript_name_10;
        PyObject *tmp_bases_name_5;
        tmp_key_name_13 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_13 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_13, tmp_key_name_13 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_14;
        }
        tmp_condition_result_33 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_33 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_16;
        }
        else
        {
            goto condexpr_false_16;
        }
        condexpr_true_16:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_14 = tmp_class_creation_5__class_decl_dict;
        tmp_key_name_14 = const_str_plain_metaclass;
        tmp_metaclass_name_5 = DICT_GET_ITEM( tmp_dict_name_14, tmp_key_name_14 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_14;
        }
        goto condexpr_end_16;
        condexpr_false_16:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_truth_name_6 = CHECK_IF_TRUE( tmp_class_creation_5__bases );
        if ( tmp_truth_name_6 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_14;
        }
        tmp_condition_result_34 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_34 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_17;
        }
        else
        {
            goto condexpr_false_17;
        }
        condexpr_true_17:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_subscribed_name_10 = tmp_class_creation_5__bases;
        tmp_subscript_name_10 = const_int_0;
        tmp_type_arg_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_10, tmp_subscript_name_10, 0 );
        if ( tmp_type_arg_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_14;
        }
        tmp_metaclass_name_5 = BUILTIN_TYPE1( tmp_type_arg_9 );
        Py_DECREF( tmp_type_arg_9 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_14;
        }
        goto condexpr_end_17;
        condexpr_false_17:;
        tmp_metaclass_name_5 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_5 );
        condexpr_end_17:;
        condexpr_end_16:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_bases_name_5 = tmp_class_creation_5__bases;
        tmp_assign_source_69 = SELECT_METACLASS( tmp_metaclass_name_5, tmp_bases_name_5 );
        Py_DECREF( tmp_metaclass_name_5 );
        if ( tmp_assign_source_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_14;
        }
        assert( tmp_class_creation_5__metaclass == NULL );
        tmp_class_creation_5__metaclass = tmp_assign_source_69;
    }
    {
        nuitka_bool tmp_condition_result_35;
        PyObject *tmp_key_name_15;
        PyObject *tmp_dict_name_15;
        tmp_key_name_15 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_15 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_15, tmp_key_name_15 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_14;
        }
        tmp_condition_result_35 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_35 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_18;
        }
        else
        {
            goto branch_no_18;
        }
        branch_yes_18:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_5__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_14;
        }
        branch_no_18:;
    }
    {
        nuitka_bool tmp_condition_result_36;
        PyObject *tmp_source_name_20;
        CHECK_OBJECT( tmp_class_creation_5__metaclass );
        tmp_source_name_20 = tmp_class_creation_5__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_20, const_str_plain___prepare__ );
        tmp_condition_result_36 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_36 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_19;
        }
        else
        {
            goto branch_no_19;
        }
        branch_yes_19:;
        {
            PyObject *tmp_assign_source_70;
            PyObject *tmp_called_name_18;
            PyObject *tmp_source_name_21;
            PyObject *tmp_args_name_9;
            PyObject *tmp_tuple_element_17;
            PyObject *tmp_kw_name_9;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_source_name_21 = tmp_class_creation_5__metaclass;
            tmp_called_name_18 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain___prepare__ );
            if ( tmp_called_name_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 366;

                goto try_except_handler_14;
            }
            tmp_tuple_element_17 = const_str_plain_ArgReplacer;
            tmp_args_name_9 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_17 );
            PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_17 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_17 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_17 );
            PyTuple_SET_ITEM( tmp_args_name_9, 1, tmp_tuple_element_17 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_9 = tmp_class_creation_5__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 366;
            tmp_assign_source_70 = CALL_FUNCTION( tmp_called_name_18, tmp_args_name_9, tmp_kw_name_9 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_args_name_9 );
            if ( tmp_assign_source_70 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 366;

                goto try_except_handler_14;
            }
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_70;
        }
        {
            nuitka_bool tmp_condition_result_37;
            PyObject *tmp_operand_name_5;
            PyObject *tmp_source_name_22;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_source_name_22 = tmp_class_creation_5__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_22, const_str_plain___getitem__ );
            tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 366;

                goto try_except_handler_14;
            }
            tmp_condition_result_37 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_37 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            {
                PyObject *tmp_raise_type_5;
                PyObject *tmp_raise_value_5;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                PyObject *tmp_tuple_element_18;
                PyObject *tmp_getattr_target_5;
                PyObject *tmp_getattr_attr_5;
                PyObject *tmp_getattr_default_5;
                PyObject *tmp_source_name_23;
                PyObject *tmp_type_arg_10;
                tmp_raise_type_5 = PyExc_TypeError;
                tmp_left_name_5 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_5__metaclass );
                tmp_getattr_target_5 = tmp_class_creation_5__metaclass;
                tmp_getattr_attr_5 = const_str_plain___name__;
                tmp_getattr_default_5 = const_str_angle_metaclass;
                tmp_tuple_element_18 = BUILTIN_GETATTR( tmp_getattr_target_5, tmp_getattr_attr_5, tmp_getattr_default_5 );
                if ( tmp_tuple_element_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 366;

                    goto try_except_handler_14;
                }
                tmp_right_name_5 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_5, 0, tmp_tuple_element_18 );
                CHECK_OBJECT( tmp_class_creation_5__prepared );
                tmp_type_arg_10 = tmp_class_creation_5__prepared;
                tmp_source_name_23 = BUILTIN_TYPE1( tmp_type_arg_10 );
                assert( !(tmp_source_name_23 == NULL) );
                tmp_tuple_element_18 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_23 );
                if ( tmp_tuple_element_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_5 );

                    exception_lineno = 366;

                    goto try_except_handler_14;
                }
                PyTuple_SET_ITEM( tmp_right_name_5, 1, tmp_tuple_element_18 );
                tmp_raise_value_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                Py_DECREF( tmp_right_name_5 );
                if ( tmp_raise_value_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 366;

                    goto try_except_handler_14;
                }
                exception_type = tmp_raise_type_5;
                Py_INCREF( tmp_raise_type_5 );
                exception_value = tmp_raise_value_5;
                exception_lineno = 366;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_14;
            }
            branch_no_20:;
        }
        goto branch_end_19;
        branch_no_19:;
        {
            PyObject *tmp_assign_source_71;
            tmp_assign_source_71 = PyDict_New();
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_71;
        }
        branch_end_19:;
    }
    {
        PyObject *tmp_assign_source_72;
        {
            PyObject *tmp_set_locals_5;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_set_locals_5 = tmp_class_creation_5__prepared;
            locals_tornado$util_366 = tmp_set_locals_5;
            Py_INCREF( tmp_set_locals_5 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_e077944e15accbec54ecb40fd81dafde;
        tmp_res = PyObject_SetItem( locals_tornado$util_366, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_16;
        }
        tmp_dictset_value = const_str_digest_3ab6a951884266fa8a14bc07798b51f6;
        tmp_res = PyObject_SetItem( locals_tornado$util_366, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_16;
        }
        tmp_dictset_value = const_str_plain_ArgReplacer;
        tmp_res = PyObject_SetItem( locals_tornado$util_366, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto try_except_handler_16;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_0fc66817e130a11d02ef07ff3a593999_5, codeobj_0fc66817e130a11d02ef07ff3a593999, module_tornado$util, sizeof(void *) );
        frame_0fc66817e130a11d02ef07ff3a593999_5 = cache_frame_0fc66817e130a11d02ef07ff3a593999_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_0fc66817e130a11d02ef07ff3a593999_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_0fc66817e130a11d02ef07ff3a593999_5 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_annotations_15;
            PyObject *tmp_dict_key_25;
            PyObject *tmp_dict_value_25;
            PyObject *tmp_mvar_value_21;
            PyObject *tmp_dict_key_26;
            PyObject *tmp_dict_value_26;
            PyObject *tmp_dict_key_27;
            PyObject *tmp_dict_value_27;
            tmp_dict_key_25 = const_str_plain_func;
            tmp_dict_value_25 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Callable );

            if ( tmp_dict_value_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Callable );

                if (unlikely( tmp_mvar_value_21 == NULL ))
                {
                    tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
                }

                if ( tmp_mvar_value_21 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 374;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_dict_value_25 = tmp_mvar_value_21;
                Py_INCREF( tmp_dict_value_25 );
                }
            }

            tmp_annotations_15 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_15, tmp_dict_key_25, tmp_dict_value_25 );
            Py_DECREF( tmp_dict_value_25 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_26 = const_str_plain_name;
            tmp_dict_value_26 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_str );

            if ( tmp_dict_value_26 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_26 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_26 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_15, tmp_dict_key_26, tmp_dict_value_26 );
            Py_DECREF( tmp_dict_value_26 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_27 = const_str_plain_return;
            tmp_dict_value_27 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_15, tmp_dict_key_27, tmp_dict_value_27 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_22___init__( tmp_annotations_15 );



            tmp_res = PyObject_SetItem( locals_tornado$util_366, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        {
            PyObject *tmp_annotations_16;
            PyObject *tmp_dict_key_28;
            PyObject *tmp_dict_value_28;
            PyObject *tmp_mvar_value_22;
            PyObject *tmp_dict_key_29;
            PyObject *tmp_dict_value_29;
            PyObject *tmp_subscribed_name_11;
            PyObject *tmp_mvar_value_23;
            PyObject *tmp_subscript_name_11;
            tmp_dict_key_28 = const_str_plain_func;
            tmp_dict_value_28 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Callable );

            if ( tmp_dict_value_28 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Callable );

                if (unlikely( tmp_mvar_value_22 == NULL ))
                {
                    tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
                }

                if ( tmp_mvar_value_22 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 382;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_dict_value_28 = tmp_mvar_value_22;
                Py_INCREF( tmp_dict_value_28 );
                }
            }

            tmp_annotations_16 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_28, tmp_dict_value_28 );
            Py_DECREF( tmp_dict_value_28 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_29 = const_str_plain_return;
            tmp_subscribed_name_11 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_List );

            if ( tmp_subscribed_name_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_List );

                if (unlikely( tmp_mvar_value_23 == NULL ))
                {
                    tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
                }

                if ( tmp_mvar_value_23 == NULL )
                {
                    Py_DECREF( tmp_annotations_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 382;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscribed_name_11 = tmp_mvar_value_23;
                Py_INCREF( tmp_subscribed_name_11 );
                }
            }

            tmp_subscript_name_11 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_str );

            if ( tmp_subscript_name_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_subscript_name_11 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_subscript_name_11 );
                }
            }

            tmp_dict_value_29 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_11, tmp_subscript_name_11 );
            Py_DECREF( tmp_subscribed_name_11 );
            Py_DECREF( tmp_subscript_name_11 );
            if ( tmp_dict_value_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_16 );

                exception_lineno = 382;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_29, tmp_dict_value_29 );
            Py_DECREF( tmp_dict_value_29 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_23__getargnames( tmp_annotations_16 );



            tmp_res = PyObject_SetItem( locals_tornado$util_366, const_str_plain__getargnames, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 382;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        {
            PyObject *tmp_defaults_3;
            PyObject *tmp_annotations_17;
            PyObject *tmp_dict_key_30;
            PyObject *tmp_dict_value_30;
            PyObject *tmp_subscribed_name_12;
            PyObject *tmp_mvar_value_24;
            PyObject *tmp_subscript_name_12;
            PyObject *tmp_mvar_value_25;
            PyObject *tmp_dict_key_31;
            PyObject *tmp_dict_value_31;
            PyObject *tmp_subscribed_name_13;
            PyObject *tmp_mvar_value_26;
            PyObject *tmp_subscript_name_13;
            PyObject *tmp_tuple_element_19;
            PyObject *tmp_mvar_value_27;
            PyObject *tmp_dict_key_32;
            PyObject *tmp_dict_value_32;
            PyObject *tmp_mvar_value_28;
            PyObject *tmp_dict_key_33;
            PyObject *tmp_dict_value_33;
            PyObject *tmp_mvar_value_29;
            tmp_defaults_3 = const_tuple_none_tuple;
            tmp_dict_key_30 = const_str_plain_args;
            tmp_subscribed_name_12 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Sequence );

            if ( tmp_subscribed_name_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Sequence );

                if (unlikely( tmp_mvar_value_24 == NULL ))
                {
                    tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Sequence );
                }

                if ( tmp_mvar_value_24 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Sequence" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 398;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscribed_name_12 = tmp_mvar_value_24;
                Py_INCREF( tmp_subscribed_name_12 );
                }
            }

            tmp_subscript_name_12 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_subscript_name_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_25 == NULL ))
                {
                    tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_25 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_12 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 398;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscript_name_12 = tmp_mvar_value_25;
                Py_INCREF( tmp_subscript_name_12 );
                }
            }

            tmp_dict_value_30 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_12, tmp_subscript_name_12 );
            Py_DECREF( tmp_subscribed_name_12 );
            Py_DECREF( tmp_subscript_name_12 );
            if ( tmp_dict_value_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 398;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_annotations_17 = _PyDict_NewPresized( 4 );
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_30, tmp_dict_value_30 );
            Py_DECREF( tmp_dict_value_30 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_31 = const_str_plain_kwargs;
            tmp_subscribed_name_13 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Dict );

            if ( tmp_subscribed_name_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_26 == NULL ))
                {
                    tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_26 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 398;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscribed_name_13 = tmp_mvar_value_26;
                Py_INCREF( tmp_subscribed_name_13 );
                }
            }

            tmp_tuple_element_19 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_str );

            if ( tmp_tuple_element_19 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_19 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_19 );
                }
            }

            tmp_subscript_name_13 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_13, 0, tmp_tuple_element_19 );
            tmp_tuple_element_19 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_tuple_element_19 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_27 == NULL ))
                {
                    tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_27 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    Py_DECREF( tmp_subscribed_name_13 );
                    Py_DECREF( tmp_subscript_name_13 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 398;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_tuple_element_19 = tmp_mvar_value_27;
                Py_INCREF( tmp_tuple_element_19 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_13, 1, tmp_tuple_element_19 );
            tmp_dict_value_31 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_13, tmp_subscript_name_13 );
            Py_DECREF( tmp_subscribed_name_13 );
            Py_DECREF( tmp_subscript_name_13 );
            if ( tmp_dict_value_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_17 );

                exception_lineno = 398;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_31, tmp_dict_value_31 );
            Py_DECREF( tmp_dict_value_31 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_32 = const_str_plain_default;
            tmp_dict_value_32 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_dict_value_32 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_28 == NULL ))
                {
                    tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_28 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 398;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_dict_value_32 = tmp_mvar_value_28;
                Py_INCREF( tmp_dict_value_32 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_32, tmp_dict_value_32 );
            Py_DECREF( tmp_dict_value_32 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_33 = const_str_plain_return;
            tmp_dict_value_33 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_dict_value_33 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_29 == NULL ))
                {
                    tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_29 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 399;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_dict_value_33 = tmp_mvar_value_29;
                Py_INCREF( tmp_dict_value_33 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_33, tmp_dict_value_33 );
            Py_DECREF( tmp_dict_value_33 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_3 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_24_get_old_value( tmp_defaults_3, tmp_annotations_17 );



            tmp_res = PyObject_SetItem( locals_tornado$util_366, const_str_plain_get_old_value, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 397;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        {
            PyObject *tmp_annotations_18;
            PyObject *tmp_dict_key_34;
            PyObject *tmp_dict_value_34;
            PyObject *tmp_mvar_value_30;
            PyObject *tmp_dict_key_35;
            PyObject *tmp_dict_value_35;
            PyObject *tmp_subscribed_name_14;
            PyObject *tmp_mvar_value_31;
            PyObject *tmp_subscript_name_14;
            PyObject *tmp_mvar_value_32;
            PyObject *tmp_dict_key_36;
            PyObject *tmp_dict_value_36;
            PyObject *tmp_subscribed_name_15;
            PyObject *tmp_mvar_value_33;
            PyObject *tmp_subscript_name_15;
            PyObject *tmp_tuple_element_20;
            PyObject *tmp_mvar_value_34;
            PyObject *tmp_dict_key_37;
            PyObject *tmp_dict_value_37;
            PyObject *tmp_subscribed_name_16;
            PyObject *tmp_mvar_value_35;
            PyObject *tmp_subscript_name_16;
            PyObject *tmp_tuple_element_21;
            PyObject *tmp_mvar_value_36;
            PyObject *tmp_subscribed_name_17;
            PyObject *tmp_mvar_value_37;
            PyObject *tmp_subscript_name_17;
            PyObject *tmp_mvar_value_38;
            PyObject *tmp_subscribed_name_18;
            PyObject *tmp_mvar_value_39;
            PyObject *tmp_subscript_name_18;
            PyObject *tmp_tuple_element_22;
            PyObject *tmp_mvar_value_40;
            tmp_dict_key_34 = const_str_plain_new_value;
            tmp_dict_value_34 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_dict_value_34 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_30 == NULL ))
                {
                    tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_30 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 410;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_dict_value_34 = tmp_mvar_value_30;
                Py_INCREF( tmp_dict_value_34 );
                }
            }

            tmp_annotations_18 = _PyDict_NewPresized( 4 );
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_34, tmp_dict_value_34 );
            Py_DECREF( tmp_dict_value_34 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_35 = const_str_plain_args;
            tmp_subscribed_name_14 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Sequence );

            if ( tmp_subscribed_name_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Sequence );

                if (unlikely( tmp_mvar_value_31 == NULL ))
                {
                    tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Sequence );
                }

                if ( tmp_mvar_value_31 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Sequence" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 410;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscribed_name_14 = tmp_mvar_value_31;
                Py_INCREF( tmp_subscribed_name_14 );
                }
            }

            tmp_subscript_name_14 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_subscript_name_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_32 == NULL ))
                {
                    tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_32 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_14 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 410;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscript_name_14 = tmp_mvar_value_32;
                Py_INCREF( tmp_subscript_name_14 );
                }
            }

            tmp_dict_value_35 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_14, tmp_subscript_name_14 );
            Py_DECREF( tmp_subscribed_name_14 );
            Py_DECREF( tmp_subscript_name_14 );
            if ( tmp_dict_value_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 410;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_35, tmp_dict_value_35 );
            Py_DECREF( tmp_dict_value_35 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_36 = const_str_plain_kwargs;
            tmp_subscribed_name_15 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Dict );

            if ( tmp_subscribed_name_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_33 == NULL ))
                {
                    tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_33 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 410;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscribed_name_15 = tmp_mvar_value_33;
                Py_INCREF( tmp_subscribed_name_15 );
                }
            }

            tmp_tuple_element_20 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_str );

            if ( tmp_tuple_element_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_20 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_20 );
                }
            }

            tmp_subscript_name_15 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_15, 0, tmp_tuple_element_20 );
            tmp_tuple_element_20 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_tuple_element_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_34 == NULL ))
                {
                    tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_34 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_15 );
                    Py_DECREF( tmp_subscript_name_15 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 410;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_tuple_element_20 = tmp_mvar_value_34;
                Py_INCREF( tmp_tuple_element_20 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_15, 1, tmp_tuple_element_20 );
            tmp_dict_value_36 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_15, tmp_subscript_name_15 );
            Py_DECREF( tmp_subscribed_name_15 );
            Py_DECREF( tmp_subscript_name_15 );
            if ( tmp_dict_value_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 410;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_36, tmp_dict_value_36 );
            Py_DECREF( tmp_dict_value_36 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_37 = const_str_plain_return;
            tmp_subscribed_name_16 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Tuple );

            if ( tmp_subscribed_name_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_35 == NULL ))
                {
                    tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_35 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 411;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscribed_name_16 = tmp_mvar_value_35;
                Py_INCREF( tmp_subscribed_name_16 );
                }
            }

            tmp_tuple_element_21 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_tuple_element_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_36 == NULL ))
                {
                    tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_36 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 411;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_tuple_element_21 = tmp_mvar_value_36;
                Py_INCREF( tmp_tuple_element_21 );
                }
            }

            tmp_subscript_name_16 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_subscript_name_16, 0, tmp_tuple_element_21 );
            tmp_subscribed_name_17 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Sequence );

            if ( tmp_subscribed_name_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Sequence );

                if (unlikely( tmp_mvar_value_37 == NULL ))
                {
                    tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Sequence );
                }

                if ( tmp_mvar_value_37 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_16 );
                    Py_DECREF( tmp_subscript_name_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Sequence" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 411;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscribed_name_17 = tmp_mvar_value_37;
                Py_INCREF( tmp_subscribed_name_17 );
                }
            }

            tmp_subscript_name_17 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_subscript_name_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_38 == NULL ))
                {
                    tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_38 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_16 );
                    Py_DECREF( tmp_subscript_name_16 );
                    Py_DECREF( tmp_subscribed_name_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 411;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscript_name_17 = tmp_mvar_value_38;
                Py_INCREF( tmp_subscript_name_17 );
                }
            }

            tmp_tuple_element_21 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_17, tmp_subscript_name_17 );
            Py_DECREF( tmp_subscribed_name_17 );
            Py_DECREF( tmp_subscript_name_17 );
            if ( tmp_tuple_element_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );
                Py_DECREF( tmp_subscribed_name_16 );
                Py_DECREF( tmp_subscript_name_16 );

                exception_lineno = 411;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_16, 1, tmp_tuple_element_21 );
            tmp_subscribed_name_18 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Dict );

            if ( tmp_subscribed_name_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_39 == NULL ))
                {
                    tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_39 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_16 );
                    Py_DECREF( tmp_subscript_name_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 411;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscribed_name_18 = tmp_mvar_value_39;
                Py_INCREF( tmp_subscribed_name_18 );
                }
            }

            tmp_tuple_element_22 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_str );

            if ( tmp_tuple_element_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_22 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_22 );
                }
            }

            tmp_subscript_name_18 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_18, 0, tmp_tuple_element_22 );
            tmp_tuple_element_22 = PyObject_GetItem( locals_tornado$util_366, const_str_plain_Any );

            if ( tmp_tuple_element_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_40 == NULL ))
                {
                    tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_40 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_16 );
                    Py_DECREF( tmp_subscript_name_16 );
                    Py_DECREF( tmp_subscribed_name_18 );
                    Py_DECREF( tmp_subscript_name_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 411;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_tuple_element_22 = tmp_mvar_value_40;
                Py_INCREF( tmp_tuple_element_22 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_18, 1, tmp_tuple_element_22 );
            tmp_tuple_element_21 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_18, tmp_subscript_name_18 );
            Py_DECREF( tmp_subscribed_name_18 );
            Py_DECREF( tmp_subscript_name_18 );
            if ( tmp_tuple_element_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );
                Py_DECREF( tmp_subscribed_name_16 );
                Py_DECREF( tmp_subscript_name_16 );

                exception_lineno = 411;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_16, 2, tmp_tuple_element_21 );
            tmp_dict_value_37 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_16, tmp_subscript_name_16 );
            Py_DECREF( tmp_subscribed_name_16 );
            Py_DECREF( tmp_subscript_name_16 );
            if ( tmp_dict_value_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 411;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_37, tmp_dict_value_37 );
            Py_DECREF( tmp_dict_value_37 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$util$$$function_25_replace( tmp_annotations_18 );



            tmp_res = PyObject_SetItem( locals_tornado$util_366, const_str_plain_replace, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 409;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0fc66817e130a11d02ef07ff3a593999_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0fc66817e130a11d02ef07ff3a593999_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_0fc66817e130a11d02ef07ff3a593999_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_0fc66817e130a11d02ef07ff3a593999_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_0fc66817e130a11d02ef07ff3a593999_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_0fc66817e130a11d02ef07ff3a593999_5,
            type_description_2,
            outline_4_var___class__
        );


        // Release cached frame.
        if ( frame_0fc66817e130a11d02ef07ff3a593999_5 == cache_frame_0fc66817e130a11d02ef07ff3a593999_5 )
        {
            Py_DECREF( frame_0fc66817e130a11d02ef07ff3a593999_5 );
        }
        cache_frame_0fc66817e130a11d02ef07ff3a593999_5 = NULL;

        assertFrameObject( frame_0fc66817e130a11d02ef07ff3a593999_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_4:;

        goto try_except_handler_16;
        skip_nested_handling_4:;
        {
            nuitka_bool tmp_condition_result_38;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_compexpr_left_5 = tmp_class_creation_5__bases;
            tmp_compexpr_right_5 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 366;

                goto try_except_handler_16;
            }
            tmp_condition_result_38 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_38 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_21;
            }
            else
            {
                goto branch_no_21;
            }
            branch_yes_21:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$util_366, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 366;

                goto try_except_handler_16;
            }
            branch_no_21:;
        }
        {
            PyObject *tmp_assign_source_73;
            PyObject *tmp_called_name_19;
            PyObject *tmp_args_name_10;
            PyObject *tmp_tuple_element_23;
            PyObject *tmp_kw_name_10;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_called_name_19 = tmp_class_creation_5__metaclass;
            tmp_tuple_element_23 = const_str_plain_ArgReplacer;
            tmp_args_name_10 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_23 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_23 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_10, 1, tmp_tuple_element_23 );
            tmp_tuple_element_23 = locals_tornado$util_366;
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_10, 2, tmp_tuple_element_23 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_10 = tmp_class_creation_5__class_decl_dict;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 366;
            tmp_assign_source_73 = CALL_FUNCTION( tmp_called_name_19, tmp_args_name_10, tmp_kw_name_10 );
            Py_DECREF( tmp_args_name_10 );
            if ( tmp_assign_source_73 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 366;

                goto try_except_handler_16;
            }
            assert( outline_4_var___class__ == NULL );
            outline_4_var___class__ = tmp_assign_source_73;
        }
        CHECK_OBJECT( outline_4_var___class__ );
        tmp_assign_source_72 = outline_4_var___class__;
        Py_INCREF( tmp_assign_source_72 );
        goto try_return_handler_16;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_16:;
        Py_DECREF( locals_tornado$util_366 );
        locals_tornado$util_366 = NULL;
        goto try_return_handler_15;
        // Exception handler code:
        try_except_handler_16:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$util_366 );
        locals_tornado$util_366 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto try_except_handler_15;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_15:;
        CHECK_OBJECT( (PyObject *)outline_4_var___class__ );
        Py_DECREF( outline_4_var___class__ );
        outline_4_var___class__ = NULL;

        goto outline_result_5;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_15 = exception_type;
        exception_keeper_value_15 = exception_value;
        exception_keeper_tb_15 = exception_tb;
        exception_keeper_lineno_15 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_15;
        exception_value = exception_keeper_value_15;
        exception_tb = exception_keeper_tb_15;
        exception_lineno = exception_keeper_lineno_15;

        goto outline_exception_5;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_5:;
        exception_lineno = 366;
        goto try_except_handler_14;
        outline_result_5:;
        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_ArgReplacer, tmp_assign_source_72 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_16 = exception_type;
    exception_keeper_value_16 = exception_value;
    exception_keeper_tb_16 = exception_tb;
    exception_keeper_lineno_16 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    Py_XDECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_16;
    exception_value = exception_keeper_value_16;
    exception_tb = exception_keeper_tb_16;
    exception_lineno = exception_keeper_lineno_16;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases );
    Py_DECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__class_decl_dict );
    Py_DECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__metaclass );
    Py_DECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__prepared );
    Py_DECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    {
        PyObject *tmp_assign_source_74;
        tmp_assign_source_74 = MAKE_FUNCTION_tornado$util$$$function_26_timedelta_to_seconds(  );



        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_timedelta_to_seconds, tmp_assign_source_74 );
    }
    {
        PyObject *tmp_assign_source_75;
        PyObject *tmp_annotations_19;
        tmp_annotations_19 = PyDict_Copy( const_dict_70add3a5fe8092827ca301f27493d991 );
        tmp_assign_source_75 = MAKE_FUNCTION_tornado$util$$$function_27__websocket_mask_python( tmp_annotations_19 );



        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__websocket_mask_python, tmp_assign_source_75 );
    }
    {
        nuitka_bool tmp_condition_result_39;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_24;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_7;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_25;
        PyObject *tmp_mvar_value_42;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_41 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 455;

            goto frame_exception_exit_1;
        }

        tmp_source_name_24 = tmp_mvar_value_41;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_environ );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 455;

            goto frame_exception_exit_1;
        }
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 455;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_TORNADO_NO_EXTENSION_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 455;

            goto frame_exception_exit_1;
        }
        tmp_truth_name_7 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_7 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 455;

            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_42 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 455;

            goto frame_exception_exit_1;
        }

        tmp_source_name_25 = tmp_mvar_value_42;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_environ );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 455;

            goto frame_exception_exit_1;
        }
        frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 455;
        tmp_compexpr_left_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_TORNADO_EXTENSION_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_compexpr_left_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 455;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_6 = const_str_plain_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        Py_DECREF( tmp_compexpr_left_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 455;

            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_39 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_39 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_39 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_22;
        }
        else
        {
            goto branch_no_22;
        }
        branch_yes_22:;
        {
            PyObject *tmp_assign_source_76;
            PyObject *tmp_mvar_value_43;
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__websocket_mask_python );

            if (unlikely( tmp_mvar_value_43 == NULL ))
            {
                tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__websocket_mask_python );
            }

            if ( tmp_mvar_value_43 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_websocket_mask_python" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 458;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_76 = tmp_mvar_value_43;
            UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__websocket_mask, tmp_assign_source_76 );
        }
        goto branch_end_22;
        branch_no_22:;
        // Tried code:
        {
            PyObject *tmp_assign_source_77;
            PyObject *tmp_import_name_from_15;
            PyObject *tmp_name_name_14;
            PyObject *tmp_globals_name_14;
            PyObject *tmp_locals_name_14;
            PyObject *tmp_fromlist_name_14;
            PyObject *tmp_level_name_14;
            tmp_name_name_14 = const_str_digest_c77a45b24a39e6d29e17d3a208233a24;
            tmp_globals_name_14 = (PyObject *)moduledict_tornado$util;
            tmp_locals_name_14 = Py_None;
            tmp_fromlist_name_14 = const_tuple_str_plain_websocket_mask_tuple;
            tmp_level_name_14 = const_int_0;
            frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 461;
            tmp_import_name_from_15 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
            if ( tmp_import_name_from_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 461;

                goto try_except_handler_17;
            }
            tmp_assign_source_77 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_websocket_mask );
            Py_DECREF( tmp_import_name_from_15 );
            if ( tmp_assign_source_77 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 461;

                goto try_except_handler_17;
            }
            UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__websocket_mask, tmp_assign_source_77 );
        }
        goto try_end_7;
        // Exception handler code:
        try_except_handler_17:;
        exception_keeper_type_17 = exception_type;
        exception_keeper_value_17 = exception_value;
        exception_keeper_tb_17 = exception_tb;
        exception_keeper_lineno_17 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_2 );
        exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_2 );
        exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_2 );

        if ( exception_keeper_tb_17 == NULL )
        {
            exception_keeper_tb_17 = MAKE_TRACEBACK( frame_a9082c6ea4640ddf3d0abc86d3d62007, exception_keeper_lineno_17 );
        }
        else if ( exception_keeper_lineno_17 != 0 )
        {
            exception_keeper_tb_17 = ADD_TRACEBACK( exception_keeper_tb_17, frame_a9082c6ea4640ddf3d0abc86d3d62007, exception_keeper_lineno_17 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_17, &exception_keeper_value_17, &exception_keeper_tb_17 );
        PyException_SetTraceback( exception_keeper_value_17, (PyObject *)exception_keeper_tb_17 );
        PUBLISH_EXCEPTION( &exception_keeper_type_17, &exception_keeper_value_17, &exception_keeper_tb_17 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_40;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            tmp_compexpr_left_7 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_7 = PyExc_ImportError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_7, tmp_compexpr_right_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 462;

                goto try_except_handler_18;
            }
            tmp_condition_result_40 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_40 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_23;
            }
            else
            {
                goto branch_no_23;
            }
            branch_yes_23:;
            {
                nuitka_bool tmp_condition_result_41;
                PyObject *tmp_compexpr_left_8;
                PyObject *tmp_compexpr_right_8;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_source_name_26;
                PyObject *tmp_mvar_value_44;
                tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_os );

                if (unlikely( tmp_mvar_value_44 == NULL ))
                {
                    tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
                }

                if ( tmp_mvar_value_44 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 463;

                    goto try_except_handler_18;
                }

                tmp_source_name_26 = tmp_mvar_value_44;
                tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_environ );
                if ( tmp_called_instance_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 463;

                    goto try_except_handler_18;
                }
                frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = 463;
                tmp_compexpr_left_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_TORNADO_EXTENSION_tuple, 0 ) );

                Py_DECREF( tmp_called_instance_3 );
                if ( tmp_compexpr_left_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 463;

                    goto try_except_handler_18;
                }
                tmp_compexpr_right_8 = const_str_plain_1;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                Py_DECREF( tmp_compexpr_left_8 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 463;

                    goto try_except_handler_18;
                }
                tmp_condition_result_41 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_41 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_24;
                }
                else
                {
                    goto branch_no_24;
                }
                branch_yes_24:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 464;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame) frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = exception_tb->tb_lineno;

                goto try_except_handler_18;
                branch_no_24:;
            }
            {
                PyObject *tmp_assign_source_78;
                PyObject *tmp_mvar_value_45;
                tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__websocket_mask_python );

                if (unlikely( tmp_mvar_value_45 == NULL ))
                {
                    tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__websocket_mask_python );
                }

                if ( tmp_mvar_value_45 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_websocket_mask_python" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 465;

                    goto try_except_handler_18;
                }

                tmp_assign_source_78 = tmp_mvar_value_45;
                UPDATE_STRING_DICT0( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain__websocket_mask, tmp_assign_source_78 );
            }
            goto branch_end_23;
            branch_no_23:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 460;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame) frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame.f_lineno = exception_tb->tb_lineno;

            goto try_except_handler_18;
            branch_end_23:;
        }
        goto try_end_8;
        // Exception handler code:
        try_except_handler_18:;
        exception_keeper_type_18 = exception_type;
        exception_keeper_value_18 = exception_value;
        exception_keeper_tb_18 = exception_tb;
        exception_keeper_lineno_18 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        // Re-raise.
        exception_type = exception_keeper_type_18;
        exception_value = exception_keeper_value_18;
        exception_tb = exception_keeper_tb_18;
        exception_lineno = exception_keeper_lineno_18;

        goto frame_exception_exit_1;
        // End of try:
        try_end_8:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        goto try_end_7;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$util );
        return MOD_RETURN_VALUE( NULL );
        // End of try:
        try_end_7:;
        branch_end_22:;
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9082c6ea4640ddf3d0abc86d3d62007 );
#endif
    popFrameStack();

    assertFrameObject( frame_a9082c6ea4640ddf3d0abc86d3d62007 );

    goto frame_no_exception_5;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9082c6ea4640ddf3d0abc86d3d62007 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a9082c6ea4640ddf3d0abc86d3d62007, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a9082c6ea4640ddf3d0abc86d3d62007->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a9082c6ea4640ddf3d0abc86d3d62007, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_5:;
    {
        PyObject *tmp_assign_source_79;
        tmp_assign_source_79 = MAKE_FUNCTION_tornado$util$$$function_28_doctests(  );



        UPDATE_STRING_DICT1( moduledict_tornado$util, (Nuitka_StringObject *)const_str_plain_doctests, tmp_assign_source_79 );
    }

    return MOD_RETURN_VALUE( module_tornado$util );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
