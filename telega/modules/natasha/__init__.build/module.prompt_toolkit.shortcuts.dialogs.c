/* Generated code for Python module 'prompt_toolkit.shortcuts.dialogs'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$shortcuts$dialogs" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$shortcuts$dialogs;
PyDictObject *moduledict_prompt_toolkit$shortcuts$dialogs;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_OK;
extern PyObject *const_str_plain_Dialog;
extern PyObject *const_str_plain_result;
static PyObject *const_str_digest_00adb138dbc4962a99897221179c07fb;
extern PyObject *const_str_plain_run;
static PyObject *const_str_plain__run_dialog;
static PyObject *const_str_digest_b865b95b92ee5587af32698f14d1ee31;
static PyObject *const_str_digest_b67e59f31ed46d146d93d05a2fadb57e;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain_ok_text;
static PyObject *const_str_digest_ea977410fa617cb647c11199ca9cd109;
extern PyObject *const_str_plain_completer;
static PyObject *const_str_digest_25b4b23050e1bde354aff4cd5280b74c;
static PyObject *const_str_plain_yes_handler;
extern PyObject *const_tuple_false_tuple;
static PyObject *const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple;
extern PyObject *const_str_plain_Box;
extern PyObject *const_str_plain_D;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_HSplit;
static PyObject *const_str_plain_radio_list;
static PyObject *const_str_digest_7c55f71047ede12b9691f43347657fb9;
extern PyObject *const_str_plain_Layout;
extern PyObject *const_str_plain_max;
extern PyObject *const_tuple_str_plain_load_key_bindings_tuple;
static PyObject *const_str_plain_button_handler;
static PyObject *const_tuple_str_plain_HSplit_tuple;
static PyObject *const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple;
static PyObject *const_str_digest_8ad52ee9504df01eeaa236df6e1f68e0;
extern PyObject *const_str_plain_layout;
static PyObject *const_str_plain_Cancel;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_no_text;
extern PyObject *const_str_plain_Label;
extern PyObject *const_str_plain_accept_handler;
static PyObject *const_str_plain_ok_handler;
extern PyObject *const_str_plain_Application;
extern PyObject *const_str_plain_callable;
static PyObject *const_tuple_str_plain_text_str_plain_text_area_str_plain_app_tuple;
static PyObject *const_str_plain_cancel_text;
extern PyObject *const_str_plain_bindings;
static PyObject *const_str_plain_Yes;
static PyObject *const_tuple_str_empty_str_empty_none_none_false_tuple;
extern PyObject *const_str_plain_body;
static PyObject *const_tuple_str_plain_radio_list_tuple;
extern PyObject *const_str_plain_start;
extern PyObject *const_tuple_str_plain_v_tuple;
extern PyObject *const_str_digest_be077fe6fbb1013afb24e5934140597e;
static PyObject *const_str_digest_541d7a5b9567a476df80b136fba0693e;
extern PyObject *const_tuple_int_pos_1_tuple;
static PyObject *const_str_digest_9beaef72d01a9bfa2fc3089ffaa9d78d;
extern PyObject *const_str_plain_multiline;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_run_in_executor;
extern PyObject *const_tuple_str_plain_Dimension_tuple;
extern PyObject *const_str_digest_6695b0a823c8cd6b75b68a2528261696;
extern PyObject *const_str_plain_button_dialog;
extern PyObject *const_str_plain_current_value;
static PyObject *const_str_plain__create_app;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_full_screen;
static PyObject *const_tuple_89f30d2e0ed26884257e8ba320607132_tuple;
static PyObject *const_str_plain_textfield;
extern PyObject *const_str_plain_dont_extend_height;
extern PyObject *const_str_plain_padding;
extern PyObject *const_str_plain_percentage;
extern PyObject *const_str_plain_add;
static PyObject *const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple;
static PyObject *const_list_59e83616a8423114a44c9acf8cb2b915_list;
extern PyObject *const_str_plain_t;
extern PyObject *const_tuple_str_plain_run_in_executor_tuple;
extern PyObject *const_str_plain_merge_key_bindings;
extern PyObject *const_str_plain_mouse_support;
extern PyObject *const_str_digest_f832e38dcf1aa78f989188cdc625749d;
extern PyObject *const_str_plain_value;
extern PyObject *const_tuple_str_plain_focus_next_str_plain_focus_previous_tuple;
extern PyObject *const_str_plain_style;
extern PyObject *const_str_plain_focus;
extern PyObject *const_str_plain_input_dialog;
extern PyObject *const_str_plain_height;
static PyObject *const_str_plain_text_area;
extern PyObject *const_str_plain_async_;
static PyObject *const_str_plain_run_callback;
static PyObject *const_tuple_aeedd08a996836ddaf285036861c9e7a_tuple;
static PyObject *const_str_plain_No;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_focus_next;
extern PyObject *const_str_plain_message_dialog;
static PyObject *const_tuple_str_plain_dialog_str_plain_style_str_plain_bindings_tuple;
extern PyObject *const_str_plain_buttons;
extern PyObject *const_str_plain_ProgressBar;
extern PyObject *const_str_plain_application;
extern PyObject *const_str_plain_title;
extern PyObject *const_str_digest_388882ed5238bd9499b19d10d66c5ccd;
extern PyObject *const_dict_d9f37cf1134b2ef5e7e66a4fa42a97ac;
extern PyObject *const_tuple_str_plain_Application_tuple;
static PyObject *const_tuple_str_empty_str_empty_str_plain_Yes_str_plain_No_none_false_tuple;
extern PyObject *const_str_digest_3bb6d283c2212a91e00ea390103bbbae;
static PyObject *const_tuple_str_plain_textfield_tuple;
extern PyObject *const_str_digest_6a25fd7559aacbd9227f71754f616c06;
static PyObject *const_tuple_str_plain_buf_str_plain_ok_button_tuple;
static PyObject *const_str_plain_ok_button;
extern PyObject *const_str_plain_KeyBindings;
static PyObject *const_str_plain_yes_text;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_buffer;
static PyObject *const_tuple_5e1d1400760fc77656248084888d7faa_tuple;
extern PyObject *const_str_plain___all__;
static PyObject *const_str_plain_cancel_button;
static PyObject *const_str_plain__return_none;
extern PyObject *const_str_plain_TextArea;
static PyObject *const_str_digest_9ecd81565588f69489be3aa1647544be;
extern PyObject *const_str_digest_a9cf0ebcb31ab127e55d3f37cd8b3e84;
extern PyObject *const_str_plain_buf;
static PyObject *const_str_plain_log_text;
extern PyObject *const_int_0;
static PyObject *const_str_plain_no_handler;
extern PyObject *const_str_plain_load_key_bindings;
static PyObject *const_int_pos_10000000000;
extern PyObject *const_str_plain_get_app;
static PyObject *const_tuple_str_plain_t_str_plain_v_str_plain_button_handler_tuple;
static PyObject *const_tuple_str_plain_value_str_plain_progressbar_str_plain_app_tuple;
extern PyObject *const_str_plain_text;
static PyObject *const_str_digest_27702084fce6b6368ae1f82d384c22f5;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_set_percentage;
extern PyObject *const_tuple_str_plain_get_app_tuple;
static PyObject *const_str_digest_d4b81b3d7645a1f61a8122f20834f9fa;
extern PyObject *const_str_plain_radiolist_dialog;
static PyObject *const_str_digest_efbb2c4267df7c70f47fe9b08f26ea33;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_dict_23ccf90c8ca98c47117f7093afc959ff;
static PyObject *const_tuple_74cd98960c1ec89551efe1885d570643_tuple;
static PyObject *const_str_digest_c9d6aebd9471d31a46e188fa9296528c;
extern PyObject *const_tuple_str_plain_KeyBindings_str_plain_merge_key_bindings_tuple;
static PyObject *const_str_digest_64498a81685e814fc9fc39130c3d7ce7;
extern PyObject *const_str_plain_Button;
extern PyObject *const_tuple_str_plain_tab_tuple;
static PyObject *const_tuple_ba13261d05d4ed65a5cab5e523ef587a_tuple;
extern PyObject *const_str_plain_app;
extern PyObject *const_str_plain_exact;
static PyObject *const_dict_a6ea4a217604cb05f2ef400af8988a95;
extern PyObject *const_str_plain_with_background;
static PyObject *const_str_digest_c33282d27179475fc074978e914787a0;
static PyObject *const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple;
extern PyObject *const_str_plain_key_bindings;
extern PyObject *const_str_plain_exit;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_v;
static PyObject *const_str_digest_e632148272daf49b56417378cbcf5300;
static PyObject *const_tuple_9184ebf5ca42597c688ca4fe11812865_tuple;
extern PyObject *const_str_plain_password;
extern PyObject *const_str_digest_722e5e8c7f53b34663d9669ca7ddc056;
extern PyObject *const_str_plain_functools;
static PyObject *const_str_digest_8d36d60ec2614c656ae4b94337782f8e;
extern PyObject *const_str_plain_focusable;
extern PyObject *const_str_plain_accept;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_yes_no_dialog;
extern PyObject *const_str_plain_tab;
extern PyObject *const_str_plain_RadioList;
extern PyObject *const_str_plain_partial;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_values;
static PyObject *const_dict_b8f897b0905c8c3e70aba22b8f4e6f20;
static PyObject *const_tuple_str_empty_str_empty_str_plain_Ok_none_false_tuple;
extern PyObject *const_str_plain_Dimension;
static PyObject *const_str_plain_Ok;
extern PyObject *const_tuple_str_digest_be077fe6fbb1013afb24e5934140597e_tuple;
extern PyObject *const_str_digest_24b1c11e869e4a3b8d838ff0fe7758f4;
extern PyObject *const_str_plain_dialog;
extern PyObject *const_tuple_str_plain_Layout_tuple;
extern PyObject *const_str_plain_run_async;
static PyObject *const_str_plain_progressbar;
extern PyObject *const_str_plain_progress_dialog;
extern PyObject *const_str_digest_6441b0d62e9f3dcafad087d802e7f46c;
static PyObject *const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple;
static PyObject *const_str_digest_9611fcfd9c74c54c03226adf0f4399a4;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_focus_previous;
extern PyObject *const_dict_0e72014dbc4554f47c43b25c4b9dcc5b;
extern PyObject *const_str_plain_invalidate;
extern PyObject *const_str_plain_preferred;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_insert_text;
static PyObject *const_str_digest_741097b7fb0c533e295e73a60416273b;
extern PyObject *const_str_digest_7037044602504fe42542b695cfd14bf8;
extern PyObject *const_str_plain_handler;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_00adb138dbc4962a99897221179c07fb = UNSTREAM_STRING_ASCII( &constant_bin[ 4831945 ], 28, 0 );
    const_str_plain__run_dialog = UNSTREAM_STRING_ASCII( &constant_bin[ 4831973 ], 11, 1 );
    const_str_digest_b865b95b92ee5587af32698f14d1ee31 = UNSTREAM_STRING_ASCII( &constant_bin[ 4831984 ], 56, 0 );
    const_str_digest_b67e59f31ed46d146d93d05a2fadb57e = UNSTREAM_STRING_ASCII( &constant_bin[ 4832040 ], 39, 0 );
    const_str_plain_ok_text = UNSTREAM_STRING_ASCII( &constant_bin[ 2714222 ], 7, 1 );
    const_str_digest_ea977410fa617cb647c11199ca9cd109 = UNSTREAM_STRING_ASCII( &constant_bin[ 4832079 ], 36, 0 );
    const_str_digest_25b4b23050e1bde354aff4cd5280b74c = UNSTREAM_STRING_ASCII( &constant_bin[ 4832115 ], 86, 0 );
    const_str_plain_yes_handler = UNSTREAM_STRING_ASCII( &constant_bin[ 4832201 ], 11, 1 );
    const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple, 0, const_str_plain_title ); Py_INCREF( const_str_plain_title );
    PyTuple_SET_ITEM( const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple, 2, const_str_plain_buttons ); Py_INCREF( const_str_plain_buttons );
    PyTuple_SET_ITEM( const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple, 3, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple, 4, const_str_plain_async_ ); Py_INCREF( const_str_plain_async_ );
    const_str_plain_button_handler = UNSTREAM_STRING_ASCII( &constant_bin[ 4832212 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple, 5, const_str_plain_button_handler ); Py_INCREF( const_str_plain_button_handler );
    PyTuple_SET_ITEM( const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple, 6, const_str_plain_dialog ); Py_INCREF( const_str_plain_dialog );
    const_str_plain_radio_list = UNSTREAM_STRING_ASCII( &constant_bin[ 4832226 ], 10, 1 );
    const_str_digest_7c55f71047ede12b9691f43347657fb9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4832236 ], 35, 0 );
    const_tuple_str_plain_HSplit_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_HSplit_tuple, 0, const_str_plain_HSplit ); Py_INCREF( const_str_plain_HSplit );
    const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple, 0, const_str_plain_ProgressBar ); Py_INCREF( const_str_plain_ProgressBar );
    PyTuple_SET_ITEM( const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple, 1, const_str_plain_Dialog ); Py_INCREF( const_str_plain_Dialog );
    PyTuple_SET_ITEM( const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple, 2, const_str_plain_Button ); Py_INCREF( const_str_plain_Button );
    PyTuple_SET_ITEM( const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple, 3, const_str_plain_Label ); Py_INCREF( const_str_plain_Label );
    PyTuple_SET_ITEM( const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple, 4, const_str_plain_Box ); Py_INCREF( const_str_plain_Box );
    PyTuple_SET_ITEM( const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple, 5, const_str_plain_TextArea ); Py_INCREF( const_str_plain_TextArea );
    PyTuple_SET_ITEM( const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple, 6, const_str_plain_RadioList ); Py_INCREF( const_str_plain_RadioList );
    const_str_digest_8ad52ee9504df01eeaa236df6e1f68e0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4832271 ], 37, 0 );
    const_str_plain_Cancel = UNSTREAM_STRING_ASCII( &constant_bin[ 874270 ], 6, 1 );
    const_str_plain_ok_handler = UNSTREAM_STRING_ASCII( &constant_bin[ 4832105 ], 10, 1 );
    const_tuple_str_plain_text_str_plain_text_area_str_plain_app_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_text_area_str_plain_app_tuple, 0, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    const_str_plain_text_area = UNSTREAM_STRING_ASCII( &constant_bin[ 4832308 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_text_area_str_plain_app_tuple, 1, const_str_plain_text_area ); Py_INCREF( const_str_plain_text_area );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_text_area_str_plain_app_tuple, 2, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    const_str_plain_cancel_text = UNSTREAM_STRING_ASCII( &constant_bin[ 4832317 ], 11, 1 );
    const_str_plain_Yes = UNSTREAM_STRING_ASCII( &constant_bin[ 4831999 ], 3, 1 );
    const_tuple_str_empty_str_empty_none_none_false_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_none_none_false_tuple, 0, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_none_none_false_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_none_none_false_tuple, 2, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_none_none_false_tuple, 3, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_none_none_false_tuple, 4, Py_False ); Py_INCREF( Py_False );
    const_tuple_str_plain_radio_list_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_radio_list_tuple, 0, const_str_plain_radio_list ); Py_INCREF( const_str_plain_radio_list );
    const_str_digest_541d7a5b9567a476df80b136fba0693e = UNSTREAM_STRING_ASCII( &constant_bin[ 4832328 ], 120, 0 );
    const_str_digest_9beaef72d01a9bfa2fc3089ffaa9d78d = UNSTREAM_STRING_ASCII( &constant_bin[ 4832448 ], 53, 0 );
    const_str_plain__create_app = UNSTREAM_STRING_ASCII( &constant_bin[ 4832501 ], 11, 1 );
    const_tuple_89f30d2e0ed26884257e8ba320607132_tuple = PyTuple_New( 14 );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 0, const_str_plain_title ); Py_INCREF( const_str_plain_title );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 2, const_str_plain_ok_text ); Py_INCREF( const_str_plain_ok_text );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 3, const_str_plain_cancel_text ); Py_INCREF( const_str_plain_cancel_text );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 4, const_str_plain_completer ); Py_INCREF( const_str_plain_completer );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 5, const_str_plain_password ); Py_INCREF( const_str_plain_password );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 6, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 7, const_str_plain_async_ ); Py_INCREF( const_str_plain_async_ );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 8, const_str_plain_accept ); Py_INCREF( const_str_plain_accept );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 9, const_str_plain_ok_handler ); Py_INCREF( const_str_plain_ok_handler );
    const_str_plain_ok_button = UNSTREAM_STRING_ASCII( &constant_bin[ 4832512 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 10, const_str_plain_ok_button ); Py_INCREF( const_str_plain_ok_button );
    const_str_plain_cancel_button = UNSTREAM_STRING_ASCII( &constant_bin[ 4832521 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 11, const_str_plain_cancel_button ); Py_INCREF( const_str_plain_cancel_button );
    const_str_plain_textfield = UNSTREAM_STRING_ASCII( &constant_bin[ 4832534 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 12, const_str_plain_textfield ); Py_INCREF( const_str_plain_textfield );
    PyTuple_SET_ITEM( const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 13, const_str_plain_dialog ); Py_INCREF( const_str_plain_dialog );
    const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 0, const_str_plain_title ); Py_INCREF( const_str_plain_title );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 2, const_str_plain_ok_text ); Py_INCREF( const_str_plain_ok_text );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 3, const_str_plain_cancel_text ); Py_INCREF( const_str_plain_cancel_text );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 4, const_str_plain_values ); Py_INCREF( const_str_plain_values );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 5, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 6, const_str_plain_async_ ); Py_INCREF( const_str_plain_async_ );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 7, const_str_plain_ok_handler ); Py_INCREF( const_str_plain_ok_handler );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 8, const_str_plain_radio_list ); Py_INCREF( const_str_plain_radio_list );
    PyTuple_SET_ITEM( const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 9, const_str_plain_dialog ); Py_INCREF( const_str_plain_dialog );
    const_list_59e83616a8423114a44c9acf8cb2b915_list = PyList_New( 6 );
    PyList_SET_ITEM( const_list_59e83616a8423114a44c9acf8cb2b915_list, 0, const_str_plain_yes_no_dialog ); Py_INCREF( const_str_plain_yes_no_dialog );
    PyList_SET_ITEM( const_list_59e83616a8423114a44c9acf8cb2b915_list, 1, const_str_plain_button_dialog ); Py_INCREF( const_str_plain_button_dialog );
    PyList_SET_ITEM( const_list_59e83616a8423114a44c9acf8cb2b915_list, 2, const_str_plain_input_dialog ); Py_INCREF( const_str_plain_input_dialog );
    PyList_SET_ITEM( const_list_59e83616a8423114a44c9acf8cb2b915_list, 3, const_str_plain_message_dialog ); Py_INCREF( const_str_plain_message_dialog );
    PyList_SET_ITEM( const_list_59e83616a8423114a44c9acf8cb2b915_list, 4, const_str_plain_radiolist_dialog ); Py_INCREF( const_str_plain_radiolist_dialog );
    PyList_SET_ITEM( const_list_59e83616a8423114a44c9acf8cb2b915_list, 5, const_str_plain_progress_dialog ); Py_INCREF( const_str_plain_progress_dialog );
    const_str_plain_run_callback = UNSTREAM_STRING_ASCII( &constant_bin[ 4832543 ], 12, 1 );
    const_tuple_aeedd08a996836ddaf285036861c9e7a_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_aeedd08a996836ddaf285036861c9e7a_tuple, 0, const_str_plain_run_callback ); Py_INCREF( const_str_plain_run_callback );
    const_str_plain_set_percentage = UNSTREAM_STRING_ASCII( &constant_bin[ 4832065 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_aeedd08a996836ddaf285036861c9e7a_tuple, 1, const_str_plain_set_percentage ); Py_INCREF( const_str_plain_set_percentage );
    const_str_plain_log_text = UNSTREAM_STRING_ASCII( &constant_bin[ 4832555 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_aeedd08a996836ddaf285036861c9e7a_tuple, 2, const_str_plain_log_text ); Py_INCREF( const_str_plain_log_text );
    PyTuple_SET_ITEM( const_tuple_aeedd08a996836ddaf285036861c9e7a_tuple, 3, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    const_str_plain_No = UNSTREAM_STRING_ASCII( &constant_bin[ 2187 ], 2, 1 );
    const_tuple_str_plain_dialog_str_plain_style_str_plain_bindings_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dialog_str_plain_style_str_plain_bindings_tuple, 0, const_str_plain_dialog ); Py_INCREF( const_str_plain_dialog );
    PyTuple_SET_ITEM( const_tuple_str_plain_dialog_str_plain_style_str_plain_bindings_tuple, 1, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_str_plain_dialog_str_plain_style_str_plain_bindings_tuple, 2, const_str_plain_bindings ); Py_INCREF( const_str_plain_bindings );
    const_tuple_str_empty_str_empty_str_plain_Yes_str_plain_No_none_false_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Yes_str_plain_No_none_false_tuple, 0, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Yes_str_plain_No_none_false_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Yes_str_plain_No_none_false_tuple, 2, const_str_plain_Yes ); Py_INCREF( const_str_plain_Yes );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Yes_str_plain_No_none_false_tuple, 3, const_str_plain_No ); Py_INCREF( const_str_plain_No );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Yes_str_plain_No_none_false_tuple, 4, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Yes_str_plain_No_none_false_tuple, 5, Py_False ); Py_INCREF( Py_False );
    const_tuple_str_plain_textfield_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_textfield_tuple, 0, const_str_plain_textfield ); Py_INCREF( const_str_plain_textfield );
    const_tuple_str_plain_buf_str_plain_ok_button_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_buf_str_plain_ok_button_tuple, 0, const_str_plain_buf ); Py_INCREF( const_str_plain_buf );
    PyTuple_SET_ITEM( const_tuple_str_plain_buf_str_plain_ok_button_tuple, 1, const_str_plain_ok_button ); Py_INCREF( const_str_plain_ok_button );
    const_str_plain_yes_text = UNSTREAM_STRING_ASCII( &constant_bin[ 4832563 ], 8, 1 );
    const_tuple_5e1d1400760fc77656248084888d7faa_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_5e1d1400760fc77656248084888d7faa_tuple, 0, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_5e1d1400760fc77656248084888d7faa_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    const_str_plain_Ok = UNSTREAM_STRING_ASCII( &constant_bin[ 4832571 ], 2, 1 );
    PyTuple_SET_ITEM( const_tuple_5e1d1400760fc77656248084888d7faa_tuple, 2, const_str_plain_Ok ); Py_INCREF( const_str_plain_Ok );
    PyTuple_SET_ITEM( const_tuple_5e1d1400760fc77656248084888d7faa_tuple, 3, const_str_plain_Cancel ); Py_INCREF( const_str_plain_Cancel );
    PyTuple_SET_ITEM( const_tuple_5e1d1400760fc77656248084888d7faa_tuple, 4, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_5e1d1400760fc77656248084888d7faa_tuple, 5, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_5e1d1400760fc77656248084888d7faa_tuple, 6, Py_False ); Py_INCREF( Py_False );
    const_str_plain__return_none = UNSTREAM_STRING_ASCII( &constant_bin[ 4832573 ], 12, 1 );
    const_str_digest_9ecd81565588f69489be3aa1647544be = UNSTREAM_STRING_ASCII( &constant_bin[ 4832585 ], 33, 0 );
    const_str_plain_no_handler = UNSTREAM_STRING_ASCII( &constant_bin[ 4832618 ], 10, 1 );
    const_int_pos_10000000000 = PyLong_FromUnsignedLong( 10000000000ul );
    const_tuple_str_plain_t_str_plain_v_str_plain_button_handler_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_t_str_plain_v_str_plain_button_handler_tuple, 0, const_str_plain_t ); Py_INCREF( const_str_plain_t );
    PyTuple_SET_ITEM( const_tuple_str_plain_t_str_plain_v_str_plain_button_handler_tuple, 1, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    PyTuple_SET_ITEM( const_tuple_str_plain_t_str_plain_v_str_plain_button_handler_tuple, 2, const_str_plain_button_handler ); Py_INCREF( const_str_plain_button_handler );
    const_tuple_str_plain_value_str_plain_progressbar_str_plain_app_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_progressbar_str_plain_app_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_str_plain_progressbar = UNSTREAM_STRING_ASCII( &constant_bin[ 4832628 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_progressbar_str_plain_app_tuple, 1, const_str_plain_progressbar ); Py_INCREF( const_str_plain_progressbar );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_progressbar_str_plain_app_tuple, 2, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    const_str_digest_27702084fce6b6368ae1f82d384c22f5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4832639 ], 33, 0 );
    const_str_digest_d4b81b3d7645a1f61a8122f20834f9fa = UNSTREAM_STRING_ASCII( &constant_bin[ 4832672 ], 32, 0 );
    const_str_digest_efbb2c4267df7c70f47fe9b08f26ea33 = UNSTREAM_STRING_ASCII( &constant_bin[ 4832704 ], 35, 0 );
    const_tuple_74cd98960c1ec89551efe1885d570643_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 0, const_str_plain_title ); Py_INCREF( const_str_plain_title );
    PyTuple_SET_ITEM( const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 2, const_str_plain_yes_text ); Py_INCREF( const_str_plain_yes_text );
    PyTuple_SET_ITEM( const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 3, const_str_plain_no_text ); Py_INCREF( const_str_plain_no_text );
    PyTuple_SET_ITEM( const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 4, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 5, const_str_plain_async_ ); Py_INCREF( const_str_plain_async_ );
    PyTuple_SET_ITEM( const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 6, const_str_plain_yes_handler ); Py_INCREF( const_str_plain_yes_handler );
    PyTuple_SET_ITEM( const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 7, const_str_plain_no_handler ); Py_INCREF( const_str_plain_no_handler );
    PyTuple_SET_ITEM( const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 8, const_str_plain_dialog ); Py_INCREF( const_str_plain_dialog );
    const_str_digest_c9d6aebd9471d31a46e188fa9296528c = UNSTREAM_STRING_ASCII( &constant_bin[ 4832739 ], 122, 0 );
    const_str_digest_64498a81685e814fc9fc39130c3d7ce7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4832861 ], 77, 0 );
    const_tuple_ba13261d05d4ed65a5cab5e523ef587a_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_ba13261d05d4ed65a5cab5e523ef587a_tuple, 0, const_str_plain_title ); Py_INCREF( const_str_plain_title );
    PyTuple_SET_ITEM( const_tuple_ba13261d05d4ed65a5cab5e523ef587a_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_ba13261d05d4ed65a5cab5e523ef587a_tuple, 2, const_str_plain_ok_text ); Py_INCREF( const_str_plain_ok_text );
    PyTuple_SET_ITEM( const_tuple_ba13261d05d4ed65a5cab5e523ef587a_tuple, 3, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_ba13261d05d4ed65a5cab5e523ef587a_tuple, 4, const_str_plain_async_ ); Py_INCREF( const_str_plain_async_ );
    PyTuple_SET_ITEM( const_tuple_ba13261d05d4ed65a5cab5e523ef587a_tuple, 5, const_str_plain_dialog ); Py_INCREF( const_str_plain_dialog );
    const_dict_a6ea4a217604cb05f2ef400af8988a95 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_a6ea4a217604cb05f2ef400af8988a95, const_str_plain_preferred, const_int_pos_10000000000 );
    assert( PyDict_Size( const_dict_a6ea4a217604cb05f2ef400af8988a95 ) == 1 );
    const_str_digest_c33282d27179475fc074978e914787a0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4832938 ], 30, 0 );
    const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple, 0, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple, 2, const_str_plain_OK ); Py_INCREF( const_str_plain_OK );
    PyTuple_SET_ITEM( const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple, 3, const_str_plain_Cancel ); Py_INCREF( const_str_plain_Cancel );
    PyTuple_SET_ITEM( const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple, 4, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple, 5, Py_False ); Py_INCREF( Py_False );
    PyTuple_SET_ITEM( const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple, 6, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple, 7, Py_False ); Py_INCREF( Py_False );
    const_str_digest_e632148272daf49b56417378cbcf5300 = UNSTREAM_STRING_ASCII( &constant_bin[ 4832968 ], 226, 0 );
    const_tuple_9184ebf5ca42597c688ca4fe11812865_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_9184ebf5ca42597c688ca4fe11812865_tuple, 0, const_str_plain_dialog ); Py_INCREF( const_str_plain_dialog );
    PyTuple_SET_ITEM( const_tuple_9184ebf5ca42597c688ca4fe11812865_tuple, 1, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_9184ebf5ca42597c688ca4fe11812865_tuple, 2, const_str_plain_async_ ); Py_INCREF( const_str_plain_async_ );
    PyTuple_SET_ITEM( const_tuple_9184ebf5ca42597c688ca4fe11812865_tuple, 3, const_str_plain_application ); Py_INCREF( const_str_plain_application );
    const_str_digest_8d36d60ec2614c656ae4b94337782f8e = UNSTREAM_STRING_ASCII( &constant_bin[ 4833194 ], 34, 0 );
    const_dict_b8f897b0905c8c3e70aba22b8f4e6f20 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_b8f897b0905c8c3e70aba22b8f4e6f20, const_str_plain_padding, const_int_pos_1 );
    assert( PyDict_Size( const_dict_b8f897b0905c8c3e70aba22b8f4e6f20 ) == 1 );
    const_tuple_str_empty_str_empty_str_plain_Ok_none_false_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Ok_none_false_tuple, 0, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Ok_none_false_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Ok_none_false_tuple, 2, const_str_plain_Ok ); Py_INCREF( const_str_plain_Ok );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Ok_none_false_tuple, 3, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_str_empty_str_empty_str_plain_Ok_none_false_tuple, 4, Py_False ); Py_INCREF( Py_False );
    const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 0, const_str_plain_title ); Py_INCREF( const_str_plain_title );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 2, const_str_plain_run_callback ); Py_INCREF( const_str_plain_run_callback );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 3, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 4, const_str_plain_async_ ); Py_INCREF( const_str_plain_async_ );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 5, const_str_plain_progressbar ); Py_INCREF( const_str_plain_progressbar );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 6, const_str_plain_text_area ); Py_INCREF( const_str_plain_text_area );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 7, const_str_plain_dialog ); Py_INCREF( const_str_plain_dialog );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 8, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 9, const_str_plain_set_percentage ); Py_INCREF( const_str_plain_set_percentage );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 10, const_str_plain_log_text ); Py_INCREF( const_str_plain_log_text );
    PyTuple_SET_ITEM( const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 11, const_str_plain_start ); Py_INCREF( const_str_plain_start );
    const_str_digest_9611fcfd9c74c54c03226adf0f4399a4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4833228 ], 32, 0 );
    const_str_digest_741097b7fb0c533e295e73a60416273b = UNSTREAM_STRING_ASCII( &constant_bin[ 4833260 ], 41, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$shortcuts$dialogs( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_587a21c0f80f4b96bca0bc02fc882a1b;
static PyCodeObject *codeobj_948733170074654b40985584c6267e00;
static PyCodeObject *codeobj_b118c56da081178845e85b4a1e8275ad;
static PyCodeObject *codeobj_15611240512706b1f7d1f44edf4f7c5d;
static PyCodeObject *codeobj_79a124d5bc6a11f1ebdd8117fc31775e;
static PyCodeObject *codeobj_7a14a1af4d5058d392287df08648a4ac;
static PyCodeObject *codeobj_1ebedea81c8a323955e7279e71f3865f;
static PyCodeObject *codeobj_709ce2a349fe0833cb11189bf34e2cc4;
static PyCodeObject *codeobj_f863a866304dd219d8d2c6204706b5da;
static PyCodeObject *codeobj_2a63f0c01a953744bec03a77141be4cf;
static PyCodeObject *codeobj_3bc665b84144dd10f046a53206507308;
static PyCodeObject *codeobj_b2cd95c7f0d3faa0663e45805f4aa9fa;
static PyCodeObject *codeobj_add1375c7621f268bf2b1755dd073032;
static PyCodeObject *codeobj_d234236132b1f124df3d75d6fc6fd2eb;
static PyCodeObject *codeobj_006843f430ec7450834504ea72f9e7e3;
static PyCodeObject *codeobj_270799572cd271803b1a93f83c27b058;
static PyCodeObject *codeobj_6126135c7cbb61c3fa1c32e8b5013c8f;
static PyCodeObject *codeobj_7016a14e5f3bff65e47b9d4ca6f6c078;
static PyCodeObject *codeobj_a3ef0ce21f63423ad057489594a2dc1c;
static PyCodeObject *codeobj_a11bc85289aa6c8449e6b355b07ed4c8;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_7c55f71047ede12b9691f43347657fb9 );
    codeobj_587a21c0f80f4b96bca0bc02fc882a1b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 59, const_tuple_str_plain_t_str_plain_v_str_plain_button_handler_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_948733170074654b40985584c6267e00 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_741097b7fb0c533e295e73a60416273b, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b118c56da081178845e85b4a1e8275ad = MAKE_CODEOBJ( module_filename_obj, const_str_plain__create_app, 200, const_tuple_str_plain_dialog_str_plain_style_str_plain_bindings_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_15611240512706b1f7d1f44edf4f7c5d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__return_none, 217, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_79a124d5bc6a11f1ebdd8117fc31775e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__run_dialog, 191, const_tuple_9184ebf5ca42597c688ca4fe11812865_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7a14a1af4d5058d392287df08648a4ac = MAKE_CODEOBJ( module_filename_obj, const_str_plain_accept, 71, const_tuple_str_plain_buf_str_plain_ok_button_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1ebedea81c8a323955e7279e71f3865f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_button_dialog, 47, const_tuple_513edd6d15a3c5a197b02b2bf04b1a26_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_709ce2a349fe0833cb11189bf34e2cc4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_button_handler, 53, const_tuple_str_plain_v_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f863a866304dd219d8d2c6204706b5da = MAKE_CODEOBJ( module_filename_obj, const_str_plain_input_dialog, 65, const_tuple_89f30d2e0ed26884257e8ba320607132_tuple, 8, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2a63f0c01a953744bec03a77141be4cf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_log_text, 171, const_tuple_str_plain_text_str_plain_text_area_str_plain_app_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3bc665b84144dd10f046a53206507308 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_message_dialog, 99, const_tuple_ba13261d05d4ed65a5cab5e523ef587a_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b2cd95c7f0d3faa0663e45805f4aa9fa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_no_handler, 33, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_add1375c7621f268bf2b1755dd073032 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ok_handler, 122, const_tuple_str_plain_radio_list_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d234236132b1f124df3d75d6fc6fd2eb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ok_handler, 75, const_tuple_str_plain_textfield_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_006843f430ec7450834504ea72f9e7e3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_progress_dialog, 142, const_tuple_4dab21bd40cb3b9ed883d482e6be81cf_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_270799572cd271803b1a93f83c27b058 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_radiolist_dialog, 114, const_tuple_5304eb3bc5fc98c677681933e5d1278d_tuple, 7, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6126135c7cbb61c3fa1c32e8b5013c8f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_percentage, 167, const_tuple_str_plain_value_str_plain_progressbar_str_plain_app_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7016a14e5f3bff65e47b9d4ca6f6c078 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_start, 177, const_tuple_aeedd08a996836ddaf285036861c9e7a_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a3ef0ce21f63423ad057489594a2dc1c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_yes_handler, 30, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a11bc85289aa6c8449e6b355b07ed4c8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_yes_no_dialog, 24, const_tuple_74cd98960c1ec89551efe1885d570643_tuple, 6, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_1_yes_handler(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_2_no_handler(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog$$$function_1_button_handler(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_1_accept(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_2_ok_handler(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_4_message_dialog( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog$$$function_1_ok_handler(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_1_set_percentage(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_2_log_text(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_3_start(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_7__run_dialog( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_8__create_app(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_9__return_none(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_title = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *par_yes_text = python_pars[ 2 ];
    PyObject *par_no_text = python_pars[ 3 ];
    PyObject *par_style = python_pars[ 4 ];
    PyObject *par_async_ = python_pars[ 5 ];
    PyObject *var_yes_handler = NULL;
    PyObject *var_no_handler = NULL;
    PyObject *var_dialog = NULL;
    struct Nuitka_FrameObject *frame_a11bc85289aa6c8449e6b355b07ed4c8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_a11bc85289aa6c8449e6b355b07ed4c8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_1_yes_handler(  );



        assert( var_yes_handler == NULL );
        var_yes_handler = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_2_no_handler(  );



        assert( var_no_handler == NULL );
        var_no_handler = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a11bc85289aa6c8449e6b355b07ed4c8, codeobj_a11bc85289aa6c8449e6b355b07ed4c8, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a11bc85289aa6c8449e6b355b07ed4c8 = cache_frame_a11bc85289aa6c8449e6b355b07ed4c8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a11bc85289aa6c8449e6b355b07ed4c8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a11bc85289aa6c8449e6b355b07ed4c8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Dialog );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dialog );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_title;
        CHECK_OBJECT( par_title );
        tmp_dict_value_1 = par_title;
        tmp_kw_name_1 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_body;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Label );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Label );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Label" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 38;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_dict_key_3 = const_str_plain_text;
        CHECK_OBJECT( par_text );
        tmp_dict_value_3 = par_text;
        tmp_kw_name_2 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_dont_extend_height;
        tmp_dict_value_4 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_a11bc85289aa6c8449e6b355b07ed4c8->m_frame.f_lineno = 38;
        tmp_dict_value_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 38;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_buttons;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Button );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Button );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Button" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        tmp_dict_key_6 = const_str_plain_text;
        CHECK_OBJECT( par_yes_text );
        tmp_dict_value_6 = par_yes_text;
        tmp_kw_name_3 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_handler;
        CHECK_OBJECT( var_yes_handler );
        tmp_dict_value_7 = var_yes_handler;
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        frame_a11bc85289aa6c8449e6b355b07ed4c8->m_frame.f_lineno = 40;
        tmp_list_element_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 40;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_5 = PyList_New( 2 );
        PyList_SET_ITEM( tmp_dict_value_5, 0, tmp_list_element_1 );
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Button );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Button );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            Py_DECREF( tmp_dict_value_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Button" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        tmp_dict_key_8 = const_str_plain_text;
        CHECK_OBJECT( par_no_text );
        tmp_dict_value_8 = par_no_text;
        tmp_kw_name_4 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_plain_handler;
        CHECK_OBJECT( var_no_handler );
        tmp_dict_value_9 = var_no_handler;
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        frame_a11bc85289aa6c8449e6b355b07ed4c8->m_frame.f_lineno = 41;
        tmp_list_element_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_4 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );
            Py_DECREF( tmp_dict_value_5 );

            exception_lineno = 41;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_dict_value_5, 1, tmp_list_element_1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_with_background;
        tmp_dict_value_10 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        frame_a11bc85289aa6c8449e6b355b07ed4c8->m_frame.f_lineno = 36;
        tmp_assign_source_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_dialog == NULL );
        var_dialog = tmp_assign_source_3;
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_5;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__run_dialog );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__run_dialog );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_run_dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_5;
        CHECK_OBJECT( var_dialog );
        tmp_tuple_element_1 = var_dialog;
        tmp_args_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_style );
        tmp_tuple_element_1 = par_style;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_dict_key_11 = const_str_plain_async_;
        CHECK_OBJECT( par_async_ );
        tmp_dict_value_11 = par_async_;
        tmp_kw_name_5 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        frame_a11bc85289aa6c8449e6b355b07ed4c8->m_frame.f_lineno = 44;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_5 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a11bc85289aa6c8449e6b355b07ed4c8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a11bc85289aa6c8449e6b355b07ed4c8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a11bc85289aa6c8449e6b355b07ed4c8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a11bc85289aa6c8449e6b355b07ed4c8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a11bc85289aa6c8449e6b355b07ed4c8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a11bc85289aa6c8449e6b355b07ed4c8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a11bc85289aa6c8449e6b355b07ed4c8,
        type_description_1,
        par_title,
        par_text,
        par_yes_text,
        par_no_text,
        par_style,
        par_async_,
        var_yes_handler,
        var_no_handler,
        var_dialog
    );


    // Release cached frame.
    if ( frame_a11bc85289aa6c8449e6b355b07ed4c8 == cache_frame_a11bc85289aa6c8449e6b355b07ed4c8 )
    {
        Py_DECREF( frame_a11bc85289aa6c8449e6b355b07ed4c8 );
    }
    cache_frame_a11bc85289aa6c8449e6b355b07ed4c8 = NULL;

    assertFrameObject( frame_a11bc85289aa6c8449e6b355b07ed4c8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_yes_text );
    Py_DECREF( par_yes_text );
    par_yes_text = NULL;

    CHECK_OBJECT( (PyObject *)par_no_text );
    Py_DECREF( par_no_text );
    par_no_text = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_yes_handler );
    Py_DECREF( var_yes_handler );
    var_yes_handler = NULL;

    CHECK_OBJECT( (PyObject *)var_no_handler );
    Py_DECREF( var_no_handler );
    var_no_handler = NULL;

    CHECK_OBJECT( (PyObject *)var_dialog );
    Py_DECREF( var_dialog );
    var_dialog = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_yes_text );
    Py_DECREF( par_yes_text );
    par_yes_text = NULL;

    CHECK_OBJECT( (PyObject *)par_no_text );
    Py_DECREF( par_no_text );
    par_no_text = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_yes_handler );
    Py_DECREF( var_yes_handler );
    var_yes_handler = NULL;

    CHECK_OBJECT( (PyObject *)var_no_handler );
    Py_DECREF( var_no_handler );
    var_no_handler = NULL;

    Py_XDECREF( var_dialog );
    var_dialog = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_1_yes_handler( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_a3ef0ce21f63423ad057489594a2dc1c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a3ef0ce21f63423ad057489594a2dc1c = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_a3ef0ce21f63423ad057489594a2dc1c, codeobj_a3ef0ce21f63423ad057489594a2dc1c, module_prompt_toolkit$shortcuts$dialogs, 0 );
    frame_a3ef0ce21f63423ad057489594a2dc1c = cache_frame_a3ef0ce21f63423ad057489594a2dc1c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a3ef0ce21f63423ad057489594a2dc1c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a3ef0ce21f63423ad057489594a2dc1c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 31;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        frame_a3ef0ce21f63423ad057489594a2dc1c->m_frame.f_lineno = 31;
        tmp_source_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_exit );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_23ccf90c8ca98c47117f7093afc959ff );
        frame_a3ef0ce21f63423ad057489594a2dc1c->m_frame.f_lineno = 31;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3ef0ce21f63423ad057489594a2dc1c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3ef0ce21f63423ad057489594a2dc1c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a3ef0ce21f63423ad057489594a2dc1c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a3ef0ce21f63423ad057489594a2dc1c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a3ef0ce21f63423ad057489594a2dc1c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a3ef0ce21f63423ad057489594a2dc1c,
        type_description_1
    );


    // Release cached frame.
    if ( frame_a3ef0ce21f63423ad057489594a2dc1c == cache_frame_a3ef0ce21f63423ad057489594a2dc1c )
    {
        Py_DECREF( frame_a3ef0ce21f63423ad057489594a2dc1c );
    }
    cache_frame_a3ef0ce21f63423ad057489594a2dc1c = NULL;

    assertFrameObject( frame_a3ef0ce21f63423ad057489594a2dc1c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_1_yes_handler );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_2_no_handler( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_b2cd95c7f0d3faa0663e45805f4aa9fa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b2cd95c7f0d3faa0663e45805f4aa9fa = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_b2cd95c7f0d3faa0663e45805f4aa9fa, codeobj_b2cd95c7f0d3faa0663e45805f4aa9fa, module_prompt_toolkit$shortcuts$dialogs, 0 );
    frame_b2cd95c7f0d3faa0663e45805f4aa9fa = cache_frame_b2cd95c7f0d3faa0663e45805f4aa9fa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b2cd95c7f0d3faa0663e45805f4aa9fa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b2cd95c7f0d3faa0663e45805f4aa9fa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 34;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        frame_b2cd95c7f0d3faa0663e45805f4aa9fa->m_frame.f_lineno = 34;
        tmp_source_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_exit );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_d9f37cf1134b2ef5e7e66a4fa42a97ac );
        frame_b2cd95c7f0d3faa0663e45805f4aa9fa->m_frame.f_lineno = 34;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2cd95c7f0d3faa0663e45805f4aa9fa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2cd95c7f0d3faa0663e45805f4aa9fa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b2cd95c7f0d3faa0663e45805f4aa9fa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b2cd95c7f0d3faa0663e45805f4aa9fa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b2cd95c7f0d3faa0663e45805f4aa9fa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b2cd95c7f0d3faa0663e45805f4aa9fa,
        type_description_1
    );


    // Release cached frame.
    if ( frame_b2cd95c7f0d3faa0663e45805f4aa9fa == cache_frame_b2cd95c7f0d3faa0663e45805f4aa9fa )
    {
        Py_DECREF( frame_b2cd95c7f0d3faa0663e45805f4aa9fa );
    }
    cache_frame_b2cd95c7f0d3faa0663e45805f4aa9fa = NULL;

    assertFrameObject( frame_b2cd95c7f0d3faa0663e45805f4aa9fa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_2_no_handler );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_title = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *par_buttons = python_pars[ 2 ];
    PyObject *par_style = python_pars[ 3 ];
    PyObject *par_async_ = python_pars[ 4 ];
    PyObject *var_button_handler = NULL;
    PyObject *var_dialog = NULL;
    PyObject *outline_0_var_t = NULL;
    PyObject *outline_0_var_v = NULL;
    PyObject *tmp_listcomp$tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_listcomp$tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_listcomp$tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_1ebedea81c8a323955e7279e71f3865f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    struct Nuitka_FrameObject *frame_587a21c0f80f4b96bca0bc02fc882a1b_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_587a21c0f80f4b96bca0bc02fc882a1b_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_1ebedea81c8a323955e7279e71f3865f = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog$$$function_1_button_handler(  );



        assert( var_button_handler == NULL );
        var_button_handler = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1ebedea81c8a323955e7279e71f3865f, codeobj_1ebedea81c8a323955e7279e71f3865f, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1ebedea81c8a323955e7279e71f3865f = cache_frame_1ebedea81c8a323955e7279e71f3865f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1ebedea81c8a323955e7279e71f3865f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1ebedea81c8a323955e7279e71f3865f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Dialog );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dialog );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 56;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_title;
        CHECK_OBJECT( par_title );
        tmp_dict_value_1 = par_title;
        tmp_kw_name_1 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_body;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Label );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Label );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Label" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_dict_key_3 = const_str_plain_text;
        CHECK_OBJECT( par_text );
        tmp_dict_value_3 = par_text;
        tmp_kw_name_2 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_dont_extend_height;
        tmp_dict_value_4 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_1ebedea81c8a323955e7279e71f3865f->m_frame.f_lineno = 58;
        tmp_dict_value_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 58;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_buttons;
        // Tried code:
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_buttons );
            tmp_iter_arg_1 = par_buttons;
            tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            tmp_assign_source_4 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_587a21c0f80f4b96bca0bc02fc882a1b_2, codeobj_587a21c0f80f4b96bca0bc02fc882a1b, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *) );
        frame_587a21c0f80f4b96bca0bc02fc882a1b_2 = cache_frame_587a21c0f80f4b96bca0bc02fc882a1b_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_587a21c0f80f4b96bca0bc02fc882a1b_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_587a21c0f80f4b96bca0bc02fc882a1b_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "ooo";
                    exception_lineno = 59;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_5;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_2;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_iter_arg_2 = tmp_listcomp_1__iter_value_0;
            tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_2 = "ooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = tmp_listcomp$tuple_unpack_1__source_iter;
                tmp_listcomp$tuple_unpack_1__source_iter = tmp_assign_source_6;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_unpack_1;
            CHECK_OBJECT( tmp_listcomp$tuple_unpack_1__source_iter );
            tmp_unpack_1 = tmp_listcomp$tuple_unpack_1__source_iter;
            tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
            if ( tmp_assign_source_7 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_2 = "ooo";
                exception_lineno = 59;
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_listcomp$tuple_unpack_1__element_1;
                tmp_listcomp$tuple_unpack_1__element_1 = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_unpack_2;
            CHECK_OBJECT( tmp_listcomp$tuple_unpack_1__source_iter );
            tmp_unpack_2 = tmp_listcomp$tuple_unpack_1__source_iter;
            tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
            if ( tmp_assign_source_8 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_2 = "ooo";
                exception_lineno = 59;
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_listcomp$tuple_unpack_1__element_2;
                tmp_listcomp$tuple_unpack_1__element_2 = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_1;
            CHECK_OBJECT( tmp_listcomp$tuple_unpack_1__source_iter );
            tmp_iterator_name_1 = tmp_listcomp$tuple_unpack_1__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_2 = "ooo";
                        exception_lineno = 59;
                        goto try_except_handler_5;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_2 = "ooo";
                exception_lineno = 59;
                goto try_except_handler_5;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp$tuple_unpack_1__source_iter );
        Py_DECREF( tmp_listcomp$tuple_unpack_1__source_iter );
        tmp_listcomp$tuple_unpack_1__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_4;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_listcomp$tuple_unpack_1__element_1 );
        tmp_listcomp$tuple_unpack_1__element_1 = NULL;

        Py_XDECREF( tmp_listcomp$tuple_unpack_1__element_2 );
        tmp_listcomp$tuple_unpack_1__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp$tuple_unpack_1__source_iter );
        Py_DECREF( tmp_listcomp$tuple_unpack_1__source_iter );
        tmp_listcomp$tuple_unpack_1__source_iter = NULL;

        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_listcomp$tuple_unpack_1__element_1 );
            tmp_assign_source_9 = tmp_listcomp$tuple_unpack_1__element_1;
            {
                PyObject *old = outline_0_var_t;
                outline_0_var_t = tmp_assign_source_9;
                Py_INCREF( outline_0_var_t );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_listcomp$tuple_unpack_1__element_1 );
        tmp_listcomp$tuple_unpack_1__element_1 = NULL;

        {
            PyObject *tmp_assign_source_10;
            CHECK_OBJECT( tmp_listcomp$tuple_unpack_1__element_2 );
            tmp_assign_source_10 = tmp_listcomp$tuple_unpack_1__element_2;
            {
                PyObject *old = outline_0_var_v;
                outline_0_var_v = tmp_assign_source_10;
                Py_INCREF( outline_0_var_v );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_listcomp$tuple_unpack_1__element_2 );
        tmp_listcomp$tuple_unpack_1__element_2 = NULL;

        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_kw_name_3;
            PyObject *tmp_dict_key_6;
            PyObject *tmp_dict_value_6;
            PyObject *tmp_dict_key_7;
            PyObject *tmp_dict_value_7;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Button );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Button );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Button" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 59;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }

            tmp_called_name_3 = tmp_mvar_value_3;
            tmp_dict_key_6 = const_str_plain_text;
            CHECK_OBJECT( outline_0_var_t );
            tmp_dict_value_6 = outline_0_var_t;
            tmp_kw_name_3 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_6, tmp_dict_value_6 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_7 = const_str_plain_handler;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_functools );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_kw_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 59;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }

            tmp_called_instance_1 = tmp_mvar_value_4;
            CHECK_OBJECT( var_button_handler );
            tmp_args_element_name_1 = var_button_handler;
            CHECK_OBJECT( outline_0_var_v );
            tmp_args_element_name_2 = outline_0_var_v;
            frame_587a21c0f80f4b96bca0bc02fc882a1b_2->m_frame.f_lineno = 59;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_dict_value_7 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_partial, call_args );
            }

            if ( tmp_dict_value_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_kw_name_3 );

                exception_lineno = 59;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_7, tmp_dict_value_7 );
            Py_DECREF( tmp_dict_value_7 );
            assert( !(tmp_res != 0) );
            frame_587a21c0f80f4b96bca0bc02fc882a1b_2->m_frame.f_lineno = 59;
            tmp_append_value_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_kw_name_3 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_2 = "ooo";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_dict_value_5 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_dict_value_5 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_587a21c0f80f4b96bca0bc02fc882a1b_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_587a21c0f80f4b96bca0bc02fc882a1b_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_587a21c0f80f4b96bca0bc02fc882a1b_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_587a21c0f80f4b96bca0bc02fc882a1b_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_587a21c0f80f4b96bca0bc02fc882a1b_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_587a21c0f80f4b96bca0bc02fc882a1b_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_587a21c0f80f4b96bca0bc02fc882a1b_2,
            type_description_2,
            outline_0_var_t,
            outline_0_var_v,
            var_button_handler
        );


        // Release cached frame.
        if ( frame_587a21c0f80f4b96bca0bc02fc882a1b_2 == cache_frame_587a21c0f80f4b96bca0bc02fc882a1b_2 )
        {
            Py_DECREF( frame_587a21c0f80f4b96bca0bc02fc882a1b_2 );
        }
        cache_frame_587a21c0f80f4b96bca0bc02fc882a1b_2 = NULL;

        assertFrameObject( frame_587a21c0f80f4b96bca0bc02fc882a1b_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooooooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_t );
        outline_0_var_t = NULL;

        Py_XDECREF( outline_0_var_v );
        outline_0_var_v = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_t );
        outline_0_var_t = NULL;

        Py_XDECREF( outline_0_var_v );
        outline_0_var_v = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog );
        return NULL;
        outline_exception_1:;
        exception_lineno = 59;
        goto frame_exception_exit_1;
        outline_result_1:;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_with_background;
        tmp_dict_value_8 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        frame_1ebedea81c8a323955e7279e71f3865f->m_frame.f_lineno = 56;
        tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_dialog == NULL );
        var_dialog = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__run_dialog );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__run_dialog );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_run_dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_5;
        CHECK_OBJECT( var_dialog );
        tmp_tuple_element_1 = var_dialog;
        tmp_args_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_style );
        tmp_tuple_element_1 = par_style;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_dict_key_9 = const_str_plain_async_;
        CHECK_OBJECT( par_async_ );
        tmp_dict_value_9 = par_async_;
        tmp_kw_name_4 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        frame_1ebedea81c8a323955e7279e71f3865f->m_frame.f_lineno = 62;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_1, tmp_kw_name_4 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ebedea81c8a323955e7279e71f3865f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ebedea81c8a323955e7279e71f3865f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ebedea81c8a323955e7279e71f3865f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1ebedea81c8a323955e7279e71f3865f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1ebedea81c8a323955e7279e71f3865f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1ebedea81c8a323955e7279e71f3865f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1ebedea81c8a323955e7279e71f3865f,
        type_description_1,
        par_title,
        par_text,
        par_buttons,
        par_style,
        par_async_,
        var_button_handler,
        var_dialog
    );


    // Release cached frame.
    if ( frame_1ebedea81c8a323955e7279e71f3865f == cache_frame_1ebedea81c8a323955e7279e71f3865f )
    {
        Py_DECREF( frame_1ebedea81c8a323955e7279e71f3865f );
    }
    cache_frame_1ebedea81c8a323955e7279e71f3865f = NULL;

    assertFrameObject( frame_1ebedea81c8a323955e7279e71f3865f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_buttons );
    Py_DECREF( par_buttons );
    par_buttons = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_button_handler );
    Py_DECREF( var_button_handler );
    var_button_handler = NULL;

    CHECK_OBJECT( (PyObject *)var_dialog );
    Py_DECREF( var_dialog );
    var_dialog = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_buttons );
    Py_DECREF( par_buttons );
    par_buttons = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_button_handler );
    Py_DECREF( var_button_handler );
    var_button_handler = NULL;

    Py_XDECREF( var_dialog );
    var_dialog = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog$$$function_1_button_handler( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_v = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_709ce2a349fe0833cb11189bf34e2cc4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_709ce2a349fe0833cb11189bf34e2cc4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_709ce2a349fe0833cb11189bf34e2cc4, codeobj_709ce2a349fe0833cb11189bf34e2cc4, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *) );
    frame_709ce2a349fe0833cb11189bf34e2cc4 = cache_frame_709ce2a349fe0833cb11189bf34e2cc4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_709ce2a349fe0833cb11189bf34e2cc4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_709ce2a349fe0833cb11189bf34e2cc4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        frame_709ce2a349fe0833cb11189bf34e2cc4->m_frame.f_lineno = 54;
        tmp_source_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_exit );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_result;
        CHECK_OBJECT( par_v );
        tmp_dict_value_1 = par_v;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_709ce2a349fe0833cb11189bf34e2cc4->m_frame.f_lineno = 54;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_709ce2a349fe0833cb11189bf34e2cc4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_709ce2a349fe0833cb11189bf34e2cc4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_709ce2a349fe0833cb11189bf34e2cc4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_709ce2a349fe0833cb11189bf34e2cc4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_709ce2a349fe0833cb11189bf34e2cc4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_709ce2a349fe0833cb11189bf34e2cc4,
        type_description_1,
        par_v
    );


    // Release cached frame.
    if ( frame_709ce2a349fe0833cb11189bf34e2cc4 == cache_frame_709ce2a349fe0833cb11189bf34e2cc4 )
    {
        Py_DECREF( frame_709ce2a349fe0833cb11189bf34e2cc4 );
    }
    cache_frame_709ce2a349fe0833cb11189bf34e2cc4 = NULL;

    assertFrameObject( frame_709ce2a349fe0833cb11189bf34e2cc4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog$$$function_1_button_handler );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_v );
    Py_DECREF( par_v );
    par_v = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_v );
    Py_DECREF( par_v );
    par_v = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog$$$function_1_button_handler );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_title = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *par_ok_text = python_pars[ 2 ];
    PyObject *par_cancel_text = python_pars[ 3 ];
    PyObject *par_completer = python_pars[ 4 ];
    PyObject *par_password = python_pars[ 5 ];
    PyObject *par_style = python_pars[ 6 ];
    PyObject *par_async_ = python_pars[ 7 ];
    PyObject *var_accept = NULL;
    PyObject *var_ok_handler = NULL;
    struct Nuitka_CellObject *var_ok_button = PyCell_EMPTY();
    PyObject *var_cancel_button = NULL;
    struct Nuitka_CellObject *var_textfield = PyCell_EMPTY();
    PyObject *var_dialog = NULL;
    struct Nuitka_FrameObject *frame_f863a866304dd219d8d2c6204706b5da;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f863a866304dd219d8d2c6204706b5da = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_1_accept(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = var_ok_button;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_accept == NULL );
        var_accept = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_2_ok_handler(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = var_textfield;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );


        assert( var_ok_handler == NULL );
        var_ok_handler = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f863a866304dd219d8d2c6204706b5da, codeobj_f863a866304dd219d8d2c6204706b5da, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f863a866304dd219d8d2c6204706b5da = cache_frame_f863a866304dd219d8d2c6204706b5da;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f863a866304dd219d8d2c6204706b5da );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f863a866304dd219d8d2c6204706b5da ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Button );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Button );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Button" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 78;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_text;
        CHECK_OBJECT( par_ok_text );
        tmp_dict_value_1 = par_ok_text;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_handler;
        CHECK_OBJECT( var_ok_handler );
        tmp_dict_value_2 = var_ok_handler;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_f863a866304dd219d8d2c6204706b5da->m_frame.f_lineno = 78;
        tmp_assign_source_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_ok_button ) == NULL );
        PyCell_SET( var_ok_button, tmp_assign_source_3 );

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Button );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Button );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Button" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_dict_key_3 = const_str_plain_text;
        CHECK_OBJECT( par_cancel_text );
        tmp_dict_value_3 = par_cancel_text;
        tmp_kw_name_2 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_handler;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__return_none );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__return_none );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_kw_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_return_none" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_4 = tmp_mvar_value_3;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_f863a866304dd219d8d2c6204706b5da->m_frame.f_lineno = 79;
        tmp_assign_source_4 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }
        assert( var_cancel_button == NULL );
        var_cancel_button = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_TextArea );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TextArea );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TextArea" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        tmp_dict_key_5 = const_str_plain_multiline;
        tmp_dict_value_5 = Py_False;
        tmp_kw_name_3 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_password;
        CHECK_OBJECT( par_password );
        tmp_dict_value_6 = par_password;
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_completer;
        CHECK_OBJECT( par_completer );
        tmp_dict_value_7 = par_completer;
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_accept_handler;
        CHECK_OBJECT( var_accept );
        tmp_dict_value_8 = var_accept;
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        frame_f863a866304dd219d8d2c6204706b5da->m_frame.f_lineno = 81;
        tmp_assign_source_5 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_textfield ) == NULL );
        PyCell_SET( var_textfield, tmp_assign_source_5 );

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_kw_name_5;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_kw_name_6;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_kw_name_7;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_list_element_2;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Dialog );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dialog );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 87;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_5;
        tmp_dict_key_9 = const_str_plain_title;
        CHECK_OBJECT( par_title );
        tmp_dict_value_9 = par_title;
        tmp_kw_name_4 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_body;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_HSplit );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HSplit );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_kw_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HSplit" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_6;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Label );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Label );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_kw_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Label" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 90;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_7;
        tmp_dict_key_11 = const_str_plain_text;
        CHECK_OBJECT( par_text );
        tmp_dict_value_11 = par_text;
        tmp_kw_name_5 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_12 = const_str_plain_dont_extend_height;
        tmp_dict_value_12 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_12, tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        frame_f863a866304dd219d8d2c6204706b5da->m_frame.f_lineno = 90;
        tmp_list_element_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_6, tmp_kw_name_5 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_4 );

            exception_lineno = 90;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = PyList_New( 2 );
        PyList_SET_ITEM( tmp_tuple_element_1, 0, tmp_list_element_1 );
        CHECK_OBJECT( PyCell_GET( var_textfield ) );
        tmp_list_element_1 = PyCell_GET( var_textfield );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_tuple_element_1, 1, tmp_list_element_1 );
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_13 = const_str_plain_padding;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_D );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_D );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "D" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_8;
        tmp_kw_name_7 = PyDict_Copy( const_dict_0e72014dbc4554f47c43b25c4b9dcc5b );
        frame_f863a866304dd219d8d2c6204706b5da->m_frame.f_lineno = 92;
        tmp_dict_value_13 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_7, tmp_kw_name_7 );
        Py_DECREF( tmp_kw_name_7 );
        if ( tmp_dict_value_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 92;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_6 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_6, tmp_dict_key_13, tmp_dict_value_13 );
        Py_DECREF( tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        frame_f863a866304dd219d8d2c6204706b5da->m_frame.f_lineno = 89;
        tmp_dict_value_10 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_6 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_6 );
        if ( tmp_dict_value_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_4 );

            exception_lineno = 89;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_10, tmp_dict_value_10 );
        Py_DECREF( tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_14 = const_str_plain_buttons;
        CHECK_OBJECT( PyCell_GET( var_ok_button ) );
        tmp_list_element_2 = PyCell_GET( var_ok_button );
        tmp_dict_value_14 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_dict_value_14, 0, tmp_list_element_2 );
        CHECK_OBJECT( var_cancel_button );
        tmp_list_element_2 = var_cancel_button;
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_dict_value_14, 1, tmp_list_element_2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_14, tmp_dict_value_14 );
        Py_DECREF( tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_15 = const_str_plain_with_background;
        tmp_dict_value_15 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_15, tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        frame_f863a866304dd219d8d2c6204706b5da->m_frame.f_lineno = 87;
        tmp_assign_source_6 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_4 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }
        assert( var_dialog == NULL );
        var_dialog = tmp_assign_source_6;
    }
    {
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_name_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_kw_name_8;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__run_dialog );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__run_dialog );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_run_dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_9;
        CHECK_OBJECT( var_dialog );
        tmp_tuple_element_2 = var_dialog;
        tmp_args_name_2 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( par_style );
        tmp_tuple_element_2 = par_style;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_2 );
        tmp_dict_key_16 = const_str_plain_async_;
        CHECK_OBJECT( par_async_ );
        tmp_dict_value_16 = par_async_;
        tmp_kw_name_8 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_8, tmp_dict_key_16, tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        frame_f863a866304dd219d8d2c6204706b5da->m_frame.f_lineno = 96;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_2, tmp_kw_name_8 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_8 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "oooooooooococo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f863a866304dd219d8d2c6204706b5da );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f863a866304dd219d8d2c6204706b5da );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f863a866304dd219d8d2c6204706b5da );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f863a866304dd219d8d2c6204706b5da, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f863a866304dd219d8d2c6204706b5da->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f863a866304dd219d8d2c6204706b5da, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f863a866304dd219d8d2c6204706b5da,
        type_description_1,
        par_title,
        par_text,
        par_ok_text,
        par_cancel_text,
        par_completer,
        par_password,
        par_style,
        par_async_,
        var_accept,
        var_ok_handler,
        var_ok_button,
        var_cancel_button,
        var_textfield,
        var_dialog
    );


    // Release cached frame.
    if ( frame_f863a866304dd219d8d2c6204706b5da == cache_frame_f863a866304dd219d8d2c6204706b5da )
    {
        Py_DECREF( frame_f863a866304dd219d8d2c6204706b5da );
    }
    cache_frame_f863a866304dd219d8d2c6204706b5da = NULL;

    assertFrameObject( frame_f863a866304dd219d8d2c6204706b5da );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_ok_text );
    Py_DECREF( par_ok_text );
    par_ok_text = NULL;

    CHECK_OBJECT( (PyObject *)par_cancel_text );
    Py_DECREF( par_cancel_text );
    par_cancel_text = NULL;

    CHECK_OBJECT( (PyObject *)par_completer );
    Py_DECREF( par_completer );
    par_completer = NULL;

    CHECK_OBJECT( (PyObject *)par_password );
    Py_DECREF( par_password );
    par_password = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_accept );
    Py_DECREF( var_accept );
    var_accept = NULL;

    CHECK_OBJECT( (PyObject *)var_ok_handler );
    Py_DECREF( var_ok_handler );
    var_ok_handler = NULL;

    CHECK_OBJECT( (PyObject *)var_ok_button );
    Py_DECREF( var_ok_button );
    var_ok_button = NULL;

    CHECK_OBJECT( (PyObject *)var_cancel_button );
    Py_DECREF( var_cancel_button );
    var_cancel_button = NULL;

    CHECK_OBJECT( (PyObject *)var_textfield );
    Py_DECREF( var_textfield );
    var_textfield = NULL;

    CHECK_OBJECT( (PyObject *)var_dialog );
    Py_DECREF( var_dialog );
    var_dialog = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_ok_text );
    Py_DECREF( par_ok_text );
    par_ok_text = NULL;

    CHECK_OBJECT( (PyObject *)par_cancel_text );
    Py_DECREF( par_cancel_text );
    par_cancel_text = NULL;

    CHECK_OBJECT( (PyObject *)par_completer );
    Py_DECREF( par_completer );
    par_completer = NULL;

    CHECK_OBJECT( (PyObject *)par_password );
    Py_DECREF( par_password );
    par_password = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_accept );
    Py_DECREF( var_accept );
    var_accept = NULL;

    CHECK_OBJECT( (PyObject *)var_ok_handler );
    Py_DECREF( var_ok_handler );
    var_ok_handler = NULL;

    CHECK_OBJECT( (PyObject *)var_ok_button );
    Py_DECREF( var_ok_button );
    var_ok_button = NULL;

    Py_XDECREF( var_cancel_button );
    var_cancel_button = NULL;

    CHECK_OBJECT( (PyObject *)var_textfield );
    Py_DECREF( var_textfield );
    var_textfield = NULL;

    Py_XDECREF( var_dialog );
    var_dialog = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_1_accept( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_buf = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7a14a1af4d5058d392287df08648a4ac;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7a14a1af4d5058d392287df08648a4ac = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7a14a1af4d5058d392287df08648a4ac, codeobj_7a14a1af4d5058d392287df08648a4ac, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *) );
    frame_7a14a1af4d5058d392287df08648a4ac = cache_frame_7a14a1af4d5058d392287df08648a4ac;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7a14a1af4d5058d392287df08648a4ac );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7a14a1af4d5058d392287df08648a4ac ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        frame_7a14a1af4d5058d392287df08648a4ac->m_frame.f_lineno = 72;
        tmp_source_name_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_layout );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_focus );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "ok_button" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[0] );
        frame_7a14a1af4d5058d392287df08648a4ac->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a14a1af4d5058d392287df08648a4ac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a14a1af4d5058d392287df08648a4ac );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7a14a1af4d5058d392287df08648a4ac, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7a14a1af4d5058d392287df08648a4ac->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7a14a1af4d5058d392287df08648a4ac, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7a14a1af4d5058d392287df08648a4ac,
        type_description_1,
        par_buf,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_7a14a1af4d5058d392287df08648a4ac == cache_frame_7a14a1af4d5058d392287df08648a4ac )
    {
        Py_DECREF( frame_7a14a1af4d5058d392287df08648a4ac );
    }
    cache_frame_7a14a1af4d5058d392287df08648a4ac = NULL;

    assertFrameObject( frame_7a14a1af4d5058d392287df08648a4ac );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_True;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_1_accept );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_buf );
    Py_DECREF( par_buf );
    par_buf = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_buf );
    Py_DECREF( par_buf );
    par_buf = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_1_accept );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_2_ok_handler( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_d234236132b1f124df3d75d6fc6fd2eb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_d234236132b1f124df3d75d6fc6fd2eb = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_d234236132b1f124df3d75d6fc6fd2eb, codeobj_d234236132b1f124df3d75d6fc6fd2eb, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *) );
    frame_d234236132b1f124df3d75d6fc6fd2eb = cache_frame_d234236132b1f124df3d75d6fc6fd2eb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d234236132b1f124df3d75d6fc6fd2eb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d234236132b1f124df3d75d6fc6fd2eb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        frame_d234236132b1f124df3d75d6fc6fd2eb->m_frame.f_lineno = 76;
        tmp_source_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_exit );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_result;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "textfield" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = PyCell_GET( self->m_closure[0] );
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_text );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 76;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_d234236132b1f124df3d75d6fc6fd2eb->m_frame.f_lineno = 76;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d234236132b1f124df3d75d6fc6fd2eb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d234236132b1f124df3d75d6fc6fd2eb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d234236132b1f124df3d75d6fc6fd2eb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d234236132b1f124df3d75d6fc6fd2eb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d234236132b1f124df3d75d6fc6fd2eb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d234236132b1f124df3d75d6fc6fd2eb,
        type_description_1,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_d234236132b1f124df3d75d6fc6fd2eb == cache_frame_d234236132b1f124df3d75d6fc6fd2eb )
    {
        Py_DECREF( frame_d234236132b1f124df3d75d6fc6fd2eb );
    }
    cache_frame_d234236132b1f124df3d75d6fc6fd2eb = NULL;

    assertFrameObject( frame_d234236132b1f124df3d75d6fc6fd2eb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_2_ok_handler );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_4_message_dialog( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_title = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *par_ok_text = python_pars[ 2 ];
    PyObject *par_style = python_pars[ 3 ];
    PyObject *par_async_ = python_pars[ 4 ];
    PyObject *var_dialog = NULL;
    struct Nuitka_FrameObject *frame_3bc665b84144dd10f046a53206507308;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_3bc665b84144dd10f046a53206507308 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3bc665b84144dd10f046a53206507308, codeobj_3bc665b84144dd10f046a53206507308, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3bc665b84144dd10f046a53206507308 = cache_frame_3bc665b84144dd10f046a53206507308;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3bc665b84144dd10f046a53206507308 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3bc665b84144dd10f046a53206507308 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Dialog );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dialog );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 103;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_title;
        CHECK_OBJECT( par_title );
        tmp_dict_value_1 = par_title;
        tmp_kw_name_1 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_body;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Label );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Label );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Label" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_dict_key_3 = const_str_plain_text;
        CHECK_OBJECT( par_text );
        tmp_dict_value_3 = par_text;
        tmp_kw_name_2 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_dont_extend_height;
        tmp_dict_value_4 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_3bc665b84144dd10f046a53206507308->m_frame.f_lineno = 105;
        tmp_dict_value_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 105;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_buttons;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Button );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Button );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Button" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        tmp_dict_key_6 = const_str_plain_text;
        CHECK_OBJECT( par_ok_text );
        tmp_dict_value_6 = par_ok_text;
        tmp_kw_name_3 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_handler;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__return_none );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__return_none );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            Py_DECREF( tmp_kw_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_return_none" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_7 = tmp_mvar_value_4;
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        frame_3bc665b84144dd10f046a53206507308->m_frame.f_lineno = 107;
        tmp_list_element_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 107;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_5 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_dict_value_5, 0, tmp_list_element_1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_with_background;
        tmp_dict_value_8 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        frame_3bc665b84144dd10f046a53206507308->m_frame.f_lineno = 103;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_dialog == NULL );
        var_dialog = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__run_dialog );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__run_dialog );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_run_dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_5;
        CHECK_OBJECT( var_dialog );
        tmp_tuple_element_1 = var_dialog;
        tmp_args_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_style );
        tmp_tuple_element_1 = par_style;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_dict_key_9 = const_str_plain_async_;
        CHECK_OBJECT( par_async_ );
        tmp_dict_value_9 = par_async_;
        tmp_kw_name_4 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        frame_3bc665b84144dd10f046a53206507308->m_frame.f_lineno = 111;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_1, tmp_kw_name_4 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3bc665b84144dd10f046a53206507308 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3bc665b84144dd10f046a53206507308 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3bc665b84144dd10f046a53206507308 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3bc665b84144dd10f046a53206507308, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3bc665b84144dd10f046a53206507308->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3bc665b84144dd10f046a53206507308, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3bc665b84144dd10f046a53206507308,
        type_description_1,
        par_title,
        par_text,
        par_ok_text,
        par_style,
        par_async_,
        var_dialog
    );


    // Release cached frame.
    if ( frame_3bc665b84144dd10f046a53206507308 == cache_frame_3bc665b84144dd10f046a53206507308 )
    {
        Py_DECREF( frame_3bc665b84144dd10f046a53206507308 );
    }
    cache_frame_3bc665b84144dd10f046a53206507308 = NULL;

    assertFrameObject( frame_3bc665b84144dd10f046a53206507308 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_4_message_dialog );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_ok_text );
    Py_DECREF( par_ok_text );
    par_ok_text = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_dialog );
    Py_DECREF( var_dialog );
    var_dialog = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_ok_text );
    Py_DECREF( par_ok_text );
    par_ok_text = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    Py_XDECREF( var_dialog );
    var_dialog = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_4_message_dialog );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_title = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *par_ok_text = python_pars[ 2 ];
    PyObject *par_cancel_text = python_pars[ 3 ];
    PyObject *par_values = python_pars[ 4 ];
    PyObject *par_style = python_pars[ 5 ];
    PyObject *par_async_ = python_pars[ 6 ];
    PyObject *var_ok_handler = NULL;
    struct Nuitka_CellObject *var_radio_list = PyCell_EMPTY();
    PyObject *var_dialog = NULL;
    struct Nuitka_FrameObject *frame_270799572cd271803b1a93f83c27b058;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_270799572cd271803b1a93f83c27b058 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog$$$function_1_ok_handler(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = var_radio_list;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_ok_handler == NULL );
        var_ok_handler = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_270799572cd271803b1a93f83c27b058, codeobj_270799572cd271803b1a93f83c27b058, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_270799572cd271803b1a93f83c27b058 = cache_frame_270799572cd271803b1a93f83c27b058;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_270799572cd271803b1a93f83c27b058 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_270799572cd271803b1a93f83c27b058 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_RadioList );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RadioList );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RadioList" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_values );
        tmp_args_element_name_1 = par_values;
        frame_270799572cd271803b1a93f83c27b058->m_frame.f_lineno = 125;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_radio_list ) == NULL );
        PyCell_SET( var_radio_list, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_list_element_2;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_kw_name_5;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Dialog );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dialog );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_dict_key_1 = const_str_plain_title;
        CHECK_OBJECT( par_title );
        tmp_dict_value_1 = par_title;
        tmp_kw_name_1 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_body;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_HSplit );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HSplit );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HSplit" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Label );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Label );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Label" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        tmp_dict_key_3 = const_str_plain_text;
        CHECK_OBJECT( par_text );
        tmp_dict_value_3 = par_text;
        tmp_kw_name_2 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_dont_extend_height;
        tmp_dict_value_4 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_270799572cd271803b1a93f83c27b058->m_frame.f_lineno = 130;
        tmp_list_element_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 130;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = PyList_New( 2 );
        PyList_SET_ITEM( tmp_tuple_element_1, 0, tmp_list_element_1 );
        CHECK_OBJECT( PyCell_GET( var_radio_list ) );
        tmp_list_element_1 = PyCell_GET( var_radio_list );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_tuple_element_1, 1, tmp_list_element_1 );
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_3 = PyDict_Copy( const_dict_b8f897b0905c8c3e70aba22b8f4e6f20 );
        frame_270799572cd271803b1a93f83c27b058->m_frame.f_lineno = 129;
        tmp_dict_value_2 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_3 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 129;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_buttons;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Button );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Button );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Button" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_5;
        tmp_dict_key_6 = const_str_plain_text;
        CHECK_OBJECT( par_ok_text );
        tmp_dict_value_6 = par_ok_text;
        tmp_kw_name_4 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_handler;
        CHECK_OBJECT( var_ok_handler );
        tmp_dict_value_7 = var_ok_handler;
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        frame_270799572cd271803b1a93f83c27b058->m_frame.f_lineno = 134;
        tmp_list_element_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_5, tmp_kw_name_4 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 134;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_5 = PyList_New( 2 );
        PyList_SET_ITEM( tmp_dict_value_5, 0, tmp_list_element_2 );
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Button );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Button );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            Py_DECREF( tmp_dict_value_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Button" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_6;
        tmp_dict_key_8 = const_str_plain_text;
        CHECK_OBJECT( par_cancel_text );
        tmp_dict_value_8 = par_cancel_text;
        tmp_kw_name_5 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_plain_handler;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__return_none );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__return_none );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            Py_DECREF( tmp_dict_value_5 );
            Py_DECREF( tmp_kw_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_return_none" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_9 = tmp_mvar_value_7;
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        frame_270799572cd271803b1a93f83c27b058->m_frame.f_lineno = 135;
        tmp_list_element_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_6, tmp_kw_name_5 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );
            Py_DECREF( tmp_dict_value_5 );

            exception_lineno = 135;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_dict_value_5, 1, tmp_list_element_2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_with_background;
        tmp_dict_value_10 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        frame_270799572cd271803b1a93f83c27b058->m_frame.f_lineno = 127;
        tmp_assign_source_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }
        assert( var_dialog == NULL );
        var_dialog = tmp_assign_source_3;
    }
    {
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_name_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_kw_name_6;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__run_dialog );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__run_dialog );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_run_dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_8;
        CHECK_OBJECT( var_dialog );
        tmp_tuple_element_2 = var_dialog;
        tmp_args_name_2 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( par_style );
        tmp_tuple_element_2 = par_style;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_2 );
        tmp_dict_key_11 = const_str_plain_async_;
        CHECK_OBJECT( par_async_ );
        tmp_dict_value_11 = par_async_;
        tmp_kw_name_6 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_6, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        frame_270799572cd271803b1a93f83c27b058->m_frame.f_lineno = 139;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_2, tmp_kw_name_6 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_6 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "ooooooooco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_270799572cd271803b1a93f83c27b058 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_270799572cd271803b1a93f83c27b058 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_270799572cd271803b1a93f83c27b058 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_270799572cd271803b1a93f83c27b058, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_270799572cd271803b1a93f83c27b058->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_270799572cd271803b1a93f83c27b058, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_270799572cd271803b1a93f83c27b058,
        type_description_1,
        par_title,
        par_text,
        par_ok_text,
        par_cancel_text,
        par_values,
        par_style,
        par_async_,
        var_ok_handler,
        var_radio_list,
        var_dialog
    );


    // Release cached frame.
    if ( frame_270799572cd271803b1a93f83c27b058 == cache_frame_270799572cd271803b1a93f83c27b058 )
    {
        Py_DECREF( frame_270799572cd271803b1a93f83c27b058 );
    }
    cache_frame_270799572cd271803b1a93f83c27b058 = NULL;

    assertFrameObject( frame_270799572cd271803b1a93f83c27b058 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_ok_text );
    Py_DECREF( par_ok_text );
    par_ok_text = NULL;

    CHECK_OBJECT( (PyObject *)par_cancel_text );
    Py_DECREF( par_cancel_text );
    par_cancel_text = NULL;

    CHECK_OBJECT( (PyObject *)par_values );
    Py_DECREF( par_values );
    par_values = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_ok_handler );
    Py_DECREF( var_ok_handler );
    var_ok_handler = NULL;

    CHECK_OBJECT( (PyObject *)var_radio_list );
    Py_DECREF( var_radio_list );
    var_radio_list = NULL;

    CHECK_OBJECT( (PyObject *)var_dialog );
    Py_DECREF( var_dialog );
    var_dialog = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_ok_text );
    Py_DECREF( par_ok_text );
    par_ok_text = NULL;

    CHECK_OBJECT( (PyObject *)par_cancel_text );
    Py_DECREF( par_cancel_text );
    par_cancel_text = NULL;

    CHECK_OBJECT( (PyObject *)par_values );
    Py_DECREF( par_values );
    par_values = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_ok_handler );
    Py_DECREF( var_ok_handler );
    var_ok_handler = NULL;

    CHECK_OBJECT( (PyObject *)var_radio_list );
    Py_DECREF( var_radio_list );
    var_radio_list = NULL;

    Py_XDECREF( var_dialog );
    var_dialog = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog$$$function_1_ok_handler( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_add1375c7621f268bf2b1755dd073032;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_add1375c7621f268bf2b1755dd073032 = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_add1375c7621f268bf2b1755dd073032, codeobj_add1375c7621f268bf2b1755dd073032, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *) );
    frame_add1375c7621f268bf2b1755dd073032 = cache_frame_add1375c7621f268bf2b1755dd073032;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_add1375c7621f268bf2b1755dd073032 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_add1375c7621f268bf2b1755dd073032 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        frame_add1375c7621f268bf2b1755dd073032->m_frame.f_lineno = 123;
        tmp_source_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_exit );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_result;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "radio_list" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = PyCell_GET( self->m_closure[0] );
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_value );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 123;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_add1375c7621f268bf2b1755dd073032->m_frame.f_lineno = 123;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_add1375c7621f268bf2b1755dd073032 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_add1375c7621f268bf2b1755dd073032 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_add1375c7621f268bf2b1755dd073032, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_add1375c7621f268bf2b1755dd073032->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_add1375c7621f268bf2b1755dd073032, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_add1375c7621f268bf2b1755dd073032,
        type_description_1,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_add1375c7621f268bf2b1755dd073032 == cache_frame_add1375c7621f268bf2b1755dd073032 )
    {
        Py_DECREF( frame_add1375c7621f268bf2b1755dd073032 );
    }
    cache_frame_add1375c7621f268bf2b1755dd073032 = NULL;

    assertFrameObject( frame_add1375c7621f268bf2b1755dd073032 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog$$$function_1_ok_handler );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_title = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    struct Nuitka_CellObject *par_run_callback = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *par_style = python_pars[ 3 ];
    PyObject *par_async_ = python_pars[ 4 ];
    struct Nuitka_CellObject *var_progressbar = PyCell_EMPTY();
    struct Nuitka_CellObject *var_text_area = PyCell_EMPTY();
    PyObject *var_dialog = NULL;
    struct Nuitka_CellObject *var_app = PyCell_EMPTY();
    struct Nuitka_CellObject *var_set_percentage = PyCell_EMPTY();
    struct Nuitka_CellObject *var_log_text = PyCell_EMPTY();
    PyObject *var_start = NULL;
    struct Nuitka_FrameObject *frame_006843f430ec7450834504ea72f9e7e3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_006843f430ec7450834504ea72f9e7e3 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_006843f430ec7450834504ea72f9e7e3, codeobj_006843f430ec7450834504ea72f9e7e3, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_006843f430ec7450834504ea72f9e7e3 = cache_frame_006843f430ec7450834504ea72f9e7e3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_006843f430ec7450834504ea72f9e7e3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_006843f430ec7450834504ea72f9e7e3 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_callable );
        assert( tmp_called_name_1 != NULL );
        CHECK_OBJECT( PyCell_GET( par_run_callback ) );
        tmp_args_element_name_1 = PyCell_GET( par_run_callback );
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 147;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 147;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_ProgressBar );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ProgressBar );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ProgressBar" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 149;
        tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_progressbar ) == NULL );
        PyCell_SET( var_progressbar, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_kw_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_TextArea );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TextArea );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TextArea" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_2;
        tmp_dict_key_1 = const_str_plain_focusable;
        tmp_dict_value_1 = Py_False;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_height;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_D );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_D );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "D" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_3;
        tmp_kw_name_2 = PyDict_Copy( const_dict_a6ea4a217604cb05f2ef400af8988a95 );
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 155;
        tmp_dict_value_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 155;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 150;
        tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_text_area ) == NULL );
        PyCell_SET( var_text_area, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_5;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Dialog );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dialog );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dialog" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 157;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_4;
        tmp_dict_key_3 = const_str_plain_body;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_HSplit );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HSplit );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HSplit" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 158;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_5;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Box );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Box );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Box" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_6;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Label );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Label );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Label" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_7;
        tmp_dict_key_4 = const_str_plain_text;
        CHECK_OBJECT( par_text );
        tmp_dict_value_4 = par_text;
        tmp_kw_name_4 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 159;
        tmp_args_element_name_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_8, tmp_kw_name_4 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 159;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = PyList_New( 3 );
        PyList_SET_ITEM( tmp_args_element_name_2, 0, tmp_list_element_1 );
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Box );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Box );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_args_element_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Box" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_8;
        CHECK_OBJECT( PyCell_GET( var_text_area ) );
        tmp_tuple_element_1 = PyCell_GET( var_text_area );
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_5 = const_str_plain_padding;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_D );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_D );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "D" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_9;
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 160;
        tmp_dict_value_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_exact, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_dict_value_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 160;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_5 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 160;
        tmp_list_element_1 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_1, tmp_kw_name_5 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 160;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_2, 1, tmp_list_element_1 );
        CHECK_OBJECT( PyCell_GET( var_progressbar ) );
        tmp_list_element_1 = PyCell_GET( var_progressbar );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_2, 2, tmp_list_element_1 );
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 158;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_dict_value_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_3 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_title;
        CHECK_OBJECT( par_title );
        tmp_dict_value_6 = par_title;
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_with_background;
        tmp_dict_value_7 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 157;
        tmp_assign_source_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_5, tmp_kw_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        assert( var_dialog == NULL );
        var_dialog = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__create_app );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__create_app );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_create_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_10;
        CHECK_OBJECT( var_dialog );
        tmp_args_element_name_4 = var_dialog;
        CHECK_OBJECT( par_style );
        tmp_args_element_name_5 = par_style;
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 165;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, call_args );
        }

        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_app ) == NULL );
        PyCell_SET( var_app, tmp_assign_source_4 );

    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_1_set_percentage(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[0] = var_app;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[1] = var_progressbar;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[1] );


        assert( PyCell_GET( var_set_percentage ) == NULL );
        PyCell_SET( var_set_percentage, tmp_assign_source_5 );

    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_2_log_text(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_6)->m_closure[0] = var_app;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_6)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_6)->m_closure[1] = var_text_area;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_6)->m_closure[1] );


        assert( PyCell_GET( var_log_text ) == NULL );
        PyCell_SET( var_log_text, tmp_assign_source_6 );

    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_3_start(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[0] = var_app;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[1] = var_log_text;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[2] = par_run_callback;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[3] = var_set_percentage;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[3] );


        assert( var_start == NULL );
        var_start = tmp_assign_source_7;
    }
    {
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_run_in_executor );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_run_in_executor );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "run_in_executor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_11;
        CHECK_OBJECT( var_start );
        tmp_args_element_name_6 = var_start;
        frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 183;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_async_ );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_async_ );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "oocooccoccco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( PyCell_GET( var_app ) );
            tmp_called_instance_2 = PyCell_GET( var_app );
            frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 186;
            tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_run_async );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 186;
                type_description_1 = "oocooccoccco";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_called_instance_3;
            CHECK_OBJECT( PyCell_GET( var_app ) );
            tmp_called_instance_3 = PyCell_GET( var_app );
            frame_006843f430ec7450834504ea72f9e7e3->m_frame.f_lineno = 188;
            tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_run );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 188;
                type_description_1 = "oocooccoccco";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_006843f430ec7450834504ea72f9e7e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_006843f430ec7450834504ea72f9e7e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_006843f430ec7450834504ea72f9e7e3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_006843f430ec7450834504ea72f9e7e3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_006843f430ec7450834504ea72f9e7e3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_006843f430ec7450834504ea72f9e7e3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_006843f430ec7450834504ea72f9e7e3,
        type_description_1,
        par_title,
        par_text,
        par_run_callback,
        par_style,
        par_async_,
        var_progressbar,
        var_text_area,
        var_dialog,
        var_app,
        var_set_percentage,
        var_log_text,
        var_start
    );


    // Release cached frame.
    if ( frame_006843f430ec7450834504ea72f9e7e3 == cache_frame_006843f430ec7450834504ea72f9e7e3 )
    {
        Py_DECREF( frame_006843f430ec7450834504ea72f9e7e3 );
    }
    cache_frame_006843f430ec7450834504ea72f9e7e3 = NULL;

    assertFrameObject( frame_006843f430ec7450834504ea72f9e7e3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_run_callback );
    Py_DECREF( par_run_callback );
    par_run_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_progressbar );
    Py_DECREF( var_progressbar );
    var_progressbar = NULL;

    CHECK_OBJECT( (PyObject *)var_text_area );
    Py_DECREF( var_text_area );
    var_text_area = NULL;

    CHECK_OBJECT( (PyObject *)var_dialog );
    Py_DECREF( var_dialog );
    var_dialog = NULL;

    CHECK_OBJECT( (PyObject *)var_app );
    Py_DECREF( var_app );
    var_app = NULL;

    CHECK_OBJECT( (PyObject *)var_set_percentage );
    Py_DECREF( var_set_percentage );
    var_set_percentage = NULL;

    CHECK_OBJECT( (PyObject *)var_log_text );
    Py_DECREF( var_log_text );
    var_log_text = NULL;

    CHECK_OBJECT( (PyObject *)var_start );
    Py_DECREF( var_start );
    var_start = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_run_callback );
    Py_DECREF( par_run_callback );
    par_run_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_progressbar );
    Py_DECREF( var_progressbar );
    var_progressbar = NULL;

    CHECK_OBJECT( (PyObject *)var_text_area );
    Py_DECREF( var_text_area );
    var_text_area = NULL;

    Py_XDECREF( var_dialog );
    var_dialog = NULL;

    CHECK_OBJECT( (PyObject *)var_app );
    Py_DECREF( var_app );
    var_app = NULL;

    CHECK_OBJECT( (PyObject *)var_set_percentage );
    Py_DECREF( var_set_percentage );
    var_set_percentage = NULL;

    CHECK_OBJECT( (PyObject *)var_log_text );
    Py_DECREF( var_log_text );
    var_log_text = NULL;

    Py_XDECREF( var_start );
    var_start = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_1_set_percentage( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6126135c7cbb61c3fa1c32e8b5013c8f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_6126135c7cbb61c3fa1c32e8b5013c8f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6126135c7cbb61c3fa1c32e8b5013c8f, codeobj_6126135c7cbb61c3fa1c32e8b5013c8f, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6126135c7cbb61c3fa1c32e8b5013c8f = cache_frame_6126135c7cbb61c3fa1c32e8b5013c8f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6126135c7cbb61c3fa1c32e8b5013c8f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6126135c7cbb61c3fa1c32e8b5013c8f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_int_arg_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_value );
        tmp_int_arg_1 = par_value;
        tmp_assattr_name_1 = PyNumber_Int( tmp_int_arg_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_assattr_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "progressbar" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 168;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_1 = PyCell_GET( self->m_closure[1] );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_percentage, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 169;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
        frame_6126135c7cbb61c3fa1c32e8b5013c8f->m_frame.f_lineno = 169;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_invalidate );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6126135c7cbb61c3fa1c32e8b5013c8f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6126135c7cbb61c3fa1c32e8b5013c8f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6126135c7cbb61c3fa1c32e8b5013c8f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6126135c7cbb61c3fa1c32e8b5013c8f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6126135c7cbb61c3fa1c32e8b5013c8f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6126135c7cbb61c3fa1c32e8b5013c8f,
        type_description_1,
        par_value,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_6126135c7cbb61c3fa1c32e8b5013c8f == cache_frame_6126135c7cbb61c3fa1c32e8b5013c8f )
    {
        Py_DECREF( frame_6126135c7cbb61c3fa1c32e8b5013c8f );
    }
    cache_frame_6126135c7cbb61c3fa1c32e8b5013c8f = NULL;

    assertFrameObject( frame_6126135c7cbb61c3fa1c32e8b5013c8f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_1_set_percentage );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_1_set_percentage );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_2_log_text( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2a63f0c01a953744bec03a77141be4cf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2a63f0c01a953744bec03a77141be4cf = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2a63f0c01a953744bec03a77141be4cf, codeobj_2a63f0c01a953744bec03a77141be4cf, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2a63f0c01a953744bec03a77141be4cf = cache_frame_2a63f0c01a953744bec03a77141be4cf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2a63f0c01a953744bec03a77141be4cf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2a63f0c01a953744bec03a77141be4cf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "text_area" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[1] );
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_2a63f0c01a953744bec03a77141be4cf->m_frame.f_lineno = 172;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_insert_text, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = PyCell_GET( self->m_closure[0] );
        frame_2a63f0c01a953744bec03a77141be4cf->m_frame.f_lineno = 173;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_invalidate );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a63f0c01a953744bec03a77141be4cf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a63f0c01a953744bec03a77141be4cf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2a63f0c01a953744bec03a77141be4cf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2a63f0c01a953744bec03a77141be4cf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2a63f0c01a953744bec03a77141be4cf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2a63f0c01a953744bec03a77141be4cf,
        type_description_1,
        par_text,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_2a63f0c01a953744bec03a77141be4cf == cache_frame_2a63f0c01a953744bec03a77141be4cf )
    {
        Py_DECREF( frame_2a63f0c01a953744bec03a77141be4cf );
    }
    cache_frame_2a63f0c01a953744bec03a77141be4cf = NULL;

    assertFrameObject( frame_2a63f0c01a953744bec03a77141be4cf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_2_log_text );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_2_log_text );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_3_start( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_7016a14e5f3bff65e47b9d4ca6f6c078;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_7016a14e5f3bff65e47b9d4ca6f6c078 = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_7016a14e5f3bff65e47b9d4ca6f6c078, codeobj_7016a14e5f3bff65e47b9d4ca6f6c078, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7016a14e5f3bff65e47b9d4ca6f6c078 = cache_frame_7016a14e5f3bff65e47b9d4ca6f6c078;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7016a14e5f3bff65e47b9d4ca6f6c078 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7016a14e5f3bff65e47b9d4ca6f6c078 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "run_callback" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 179;
            type_description_1 = "cccc";
            goto try_except_handler_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[2] );
        if ( PyCell_GET( self->m_closure[3] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "set_percentage" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 179;
            type_description_1 = "cccc";
            goto try_except_handler_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[3] );
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "log_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 179;
            type_description_1 = "cccc";
            goto try_except_handler_1;
        }

        tmp_args_element_name_2 = PyCell_GET( self->m_closure[1] );
        frame_7016a14e5f3bff65e47b9d4ca6f6c078->m_frame.f_lineno = 179;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "cccc";
            goto try_except_handler_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_7016a14e5f3bff65e47b9d4ca6f6c078, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_7016a14e5f3bff65e47b9d4ca6f6c078, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;
            type_description_1 = "cccc";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
        frame_7016a14e5f3bff65e47b9d4ca6f6c078->m_frame.f_lineno = 181;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_exit );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "cccc";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 178;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_7016a14e5f3bff65e47b9d4ca6f6c078->m_frame) frame_7016a14e5f3bff65e47b9d4ca6f6c078->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "cccc";
    goto try_except_handler_2;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_3_start );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_3;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;
            type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = PyCell_GET( self->m_closure[0] );
        frame_7016a14e5f3bff65e47b9d4ca6f6c078->m_frame.f_lineno = 181;
        tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_exit );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7016a14e5f3bff65e47b9d4ca6f6c078 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7016a14e5f3bff65e47b9d4ca6f6c078 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7016a14e5f3bff65e47b9d4ca6f6c078, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7016a14e5f3bff65e47b9d4ca6f6c078->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7016a14e5f3bff65e47b9d4ca6f6c078, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7016a14e5f3bff65e47b9d4ca6f6c078,
        type_description_1,
        self->m_closure[2],
        self->m_closure[3],
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_7016a14e5f3bff65e47b9d4ca6f6c078 == cache_frame_7016a14e5f3bff65e47b9d4ca6f6c078 )
    {
        Py_DECREF( frame_7016a14e5f3bff65e47b9d4ca6f6c078 );
    }
    cache_frame_7016a14e5f3bff65e47b9d4ca6f6c078 = NULL;

    assertFrameObject( frame_7016a14e5f3bff65e47b9d4ca6f6c078 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_3_start );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_7__run_dialog( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_dialog = python_pars[ 0 ];
    PyObject *par_style = python_pars[ 1 ];
    PyObject *par_async_ = python_pars[ 2 ];
    PyObject *var_application = NULL;
    struct Nuitka_FrameObject *frame_79a124d5bc6a11f1ebdd8117fc31775e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_79a124d5bc6a11f1ebdd8117fc31775e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_79a124d5bc6a11f1ebdd8117fc31775e, codeobj_79a124d5bc6a11f1ebdd8117fc31775e, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_79a124d5bc6a11f1ebdd8117fc31775e = cache_frame_79a124d5bc6a11f1ebdd8117fc31775e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_79a124d5bc6a11f1ebdd8117fc31775e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_79a124d5bc6a11f1ebdd8117fc31775e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__create_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__create_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_create_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 193;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_dialog );
        tmp_args_element_name_1 = par_dialog;
        CHECK_OBJECT( par_style );
        tmp_args_element_name_2 = par_style;
        frame_79a124d5bc6a11f1ebdd8117fc31775e->m_frame.f_lineno = 193;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_application == NULL );
        var_application = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_async_ );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_async_ );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( var_application );
            tmp_called_instance_1 = var_application;
            frame_79a124d5bc6a11f1ebdd8117fc31775e->m_frame.f_lineno = 195;
            tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_run_async );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 195;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( var_application );
            tmp_called_instance_2 = var_application;
            frame_79a124d5bc6a11f1ebdd8117fc31775e->m_frame.f_lineno = 197;
            tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_run );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 197;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_79a124d5bc6a11f1ebdd8117fc31775e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_79a124d5bc6a11f1ebdd8117fc31775e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_79a124d5bc6a11f1ebdd8117fc31775e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_79a124d5bc6a11f1ebdd8117fc31775e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_79a124d5bc6a11f1ebdd8117fc31775e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_79a124d5bc6a11f1ebdd8117fc31775e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_79a124d5bc6a11f1ebdd8117fc31775e,
        type_description_1,
        par_dialog,
        par_style,
        par_async_,
        var_application
    );


    // Release cached frame.
    if ( frame_79a124d5bc6a11f1ebdd8117fc31775e == cache_frame_79a124d5bc6a11f1ebdd8117fc31775e )
    {
        Py_DECREF( frame_79a124d5bc6a11f1ebdd8117fc31775e );
    }
    cache_frame_79a124d5bc6a11f1ebdd8117fc31775e = NULL;

    assertFrameObject( frame_79a124d5bc6a11f1ebdd8117fc31775e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_7__run_dialog );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_dialog );
    Py_DECREF( par_dialog );
    par_dialog = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    CHECK_OBJECT( (PyObject *)var_application );
    Py_DECREF( var_application );
    var_application = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_dialog );
    Py_DECREF( par_dialog );
    par_dialog = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)par_async_ );
    Py_DECREF( par_async_ );
    par_async_ = NULL;

    Py_XDECREF( var_application );
    var_application = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_7__run_dialog );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_8__create_app( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_dialog = python_pars[ 0 ];
    PyObject *par_style = python_pars[ 1 ];
    PyObject *var_bindings = NULL;
    struct Nuitka_FrameObject *frame_b118c56da081178845e85b4a1e8275ad;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_b118c56da081178845e85b4a1e8275ad = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b118c56da081178845e85b4a1e8275ad, codeobj_b118c56da081178845e85b4a1e8275ad, module_prompt_toolkit$shortcuts$dialogs, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_b118c56da081178845e85b4a1e8275ad = cache_frame_b118c56da081178845e85b4a1e8275ad;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b118c56da081178845e85b4a1e8275ad );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b118c56da081178845e85b4a1e8275ad ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_KeyBindings );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KeyBindings );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "KeyBindings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 202;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_b118c56da081178845e85b4a1e8275ad->m_frame.f_lineno = 202;
        tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_bindings == NULL );
        var_bindings = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_bindings );
        tmp_called_instance_1 = var_bindings;
        frame_b118c56da081178845e85b4a1e8275ad->m_frame.f_lineno = 203;
        tmp_called_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_add, &PyTuple_GET_ITEM( const_tuple_str_plain_tab_tuple, 0 ) );

        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_focus_next );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_focus_next );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "focus_next" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 203;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        frame_b118c56da081178845e85b4a1e8275ad->m_frame.f_lineno = 203;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_3;
        CHECK_OBJECT( var_bindings );
        tmp_called_instance_2 = var_bindings;
        frame_b118c56da081178845e85b4a1e8275ad->m_frame.f_lineno = 204;
        tmp_called_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_add, &PyTuple_GET_ITEM( const_tuple_str_digest_be077fe6fbb1013afb24e5934140597e_tuple, 0 ) );

        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_focus_previous );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_focus_previous );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "focus_previous" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 204;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_3;
        frame_b118c56da081178845e85b4a1e8275ad->m_frame.f_lineno = 204;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Application );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Application );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Application" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 206;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        tmp_dict_key_1 = const_str_plain_layout;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Layout );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Layout );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Layout" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 207;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_5;
        CHECK_OBJECT( par_dialog );
        tmp_args_element_name_3 = par_dialog;
        frame_b118c56da081178845e85b4a1e8275ad->m_frame.f_lineno = 207;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 5 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_key_bindings;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_merge_key_bindings );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_merge_key_bindings );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "merge_key_bindings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 208;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_6;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_load_key_bindings );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_load_key_bindings );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "load_key_bindings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 209;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_7;
        frame_b118c56da081178845e85b4a1e8275ad->m_frame.f_lineno = 209;
        tmp_list_element_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 209;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_4 = PyList_New( 2 );
        PyList_SET_ITEM( tmp_args_element_name_4, 0, tmp_list_element_1 );
        CHECK_OBJECT( var_bindings );
        tmp_list_element_1 = var_bindings;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_4, 1, tmp_list_element_1 );
        frame_b118c56da081178845e85b4a1e8275ad->m_frame.f_lineno = 208;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_dict_value_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 208;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_mouse_support;
        tmp_dict_value_3 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_style;
        CHECK_OBJECT( par_style );
        tmp_dict_value_4 = par_style;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_full_screen;
        tmp_dict_value_5 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        frame_b118c56da081178845e85b4a1e8275ad->m_frame.f_lineno = 206;
        tmp_return_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 206;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b118c56da081178845e85b4a1e8275ad );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b118c56da081178845e85b4a1e8275ad );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b118c56da081178845e85b4a1e8275ad );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b118c56da081178845e85b4a1e8275ad, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b118c56da081178845e85b4a1e8275ad->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b118c56da081178845e85b4a1e8275ad, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b118c56da081178845e85b4a1e8275ad,
        type_description_1,
        par_dialog,
        par_style,
        var_bindings
    );


    // Release cached frame.
    if ( frame_b118c56da081178845e85b4a1e8275ad == cache_frame_b118c56da081178845e85b4a1e8275ad )
    {
        Py_DECREF( frame_b118c56da081178845e85b4a1e8275ad );
    }
    cache_frame_b118c56da081178845e85b4a1e8275ad = NULL;

    assertFrameObject( frame_b118c56da081178845e85b4a1e8275ad );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_8__create_app );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_dialog );
    Py_DECREF( par_dialog );
    par_dialog = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    CHECK_OBJECT( (PyObject *)var_bindings );
    Py_DECREF( var_bindings );
    var_bindings = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_dialog );
    Py_DECREF( par_dialog );
    par_dialog = NULL;

    CHECK_OBJECT( (PyObject *)par_style );
    Py_DECREF( par_style );
    par_style = NULL;

    Py_XDECREF( var_bindings );
    var_bindings = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_8__create_app );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$shortcuts$dialogs$$$function_9__return_none( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_15611240512706b1f7d1f44edf4f7c5d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_15611240512706b1f7d1f44edf4f7c5d = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_15611240512706b1f7d1f44edf4f7c5d, codeobj_15611240512706b1f7d1f44edf4f7c5d, module_prompt_toolkit$shortcuts$dialogs, 0 );
    frame_15611240512706b1f7d1f44edf4f7c5d = cache_frame_15611240512706b1f7d1f44edf4f7c5d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_15611240512706b1f7d1f44edf4f7c5d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_15611240512706b1f7d1f44edf4f7c5d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 219;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_15611240512706b1f7d1f44edf4f7c5d->m_frame.f_lineno = 219;
        tmp_called_instance_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;

            goto frame_exception_exit_1;
        }
        frame_15611240512706b1f7d1f44edf4f7c5d->m_frame.f_lineno = 219;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_exit );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_15611240512706b1f7d1f44edf4f7c5d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_15611240512706b1f7d1f44edf4f7c5d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_15611240512706b1f7d1f44edf4f7c5d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_15611240512706b1f7d1f44edf4f7c5d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_15611240512706b1f7d1f44edf4f7c5d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_15611240512706b1f7d1f44edf4f7c5d,
        type_description_1
    );


    // Release cached frame.
    if ( frame_15611240512706b1f7d1f44edf4f7c5d == cache_frame_15611240512706b1f7d1f44edf4f7c5d )
    {
        Py_DECREF( frame_15611240512706b1f7d1f44edf4f7c5d );
    }
    cache_frame_15611240512706b1f7d1f44edf4f7c5d = NULL;

    assertFrameObject( frame_15611240512706b1f7d1f44edf4f7c5d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$shortcuts$dialogs$$$function_9__return_none );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog,
        const_str_plain_yes_no_dialog,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a11bc85289aa6c8449e6b355b07ed4c8,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        const_str_digest_b865b95b92ee5587af32698f14d1ee31,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_1_yes_handler(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_1_yes_handler,
        const_str_plain_yes_handler,
#if PYTHON_VERSION >= 300
        const_str_digest_8d36d60ec2614c656ae4b94337782f8e,
#endif
        codeobj_a3ef0ce21f63423ad057489594a2dc1c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_2_no_handler(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog$$$function_2_no_handler,
        const_str_plain_no_handler,
#if PYTHON_VERSION >= 300
        const_str_digest_27702084fce6b6368ae1f82d384c22f5,
#endif
        codeobj_b2cd95c7f0d3faa0663e45805f4aa9fa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog,
        const_str_plain_button_dialog,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1ebedea81c8a323955e7279e71f3865f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        const_str_digest_541d7a5b9567a476df80b136fba0693e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog$$$function_1_button_handler(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog$$$function_1_button_handler,
        const_str_plain_button_handler,
#if PYTHON_VERSION >= 300
        const_str_digest_8ad52ee9504df01eeaa236df6e1f68e0,
#endif
        codeobj_709ce2a349fe0833cb11189bf34e2cc4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog,
        const_str_plain_input_dialog,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f863a866304dd219d8d2c6204706b5da,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        const_str_digest_25b4b23050e1bde354aff4cd5280b74c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_1_accept(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_1_accept,
        const_str_plain_accept,
#if PYTHON_VERSION >= 300
        const_str_digest_00adb138dbc4962a99897221179c07fb,
#endif
        codeobj_7a14a1af4d5058d392287df08648a4ac,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_2_ok_handler(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog$$$function_2_ok_handler,
        const_str_plain_ok_handler,
#if PYTHON_VERSION >= 300
        const_str_digest_d4b81b3d7645a1f61a8122f20834f9fa,
#endif
        codeobj_d234236132b1f124df3d75d6fc6fd2eb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_4_message_dialog( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_4_message_dialog,
        const_str_plain_message_dialog,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3bc665b84144dd10f046a53206507308,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        const_str_digest_64498a81685e814fc9fc39130c3d7ce7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog,
        const_str_plain_radiolist_dialog,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_270799572cd271803b1a93f83c27b058,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        const_str_digest_e632148272daf49b56417378cbcf5300,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog$$$function_1_ok_handler(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog$$$function_1_ok_handler,
        const_str_plain_ok_handler,
#if PYTHON_VERSION >= 300
        const_str_digest_ea977410fa617cb647c11199ca9cd109,
#endif
        codeobj_add1375c7621f268bf2b1755dd073032,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog,
        const_str_plain_progress_dialog,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_006843f430ec7450834504ea72f9e7e3,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        const_str_digest_c9d6aebd9471d31a46e188fa9296528c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_1_set_percentage(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_1_set_percentage,
        const_str_plain_set_percentage,
#if PYTHON_VERSION >= 300
        const_str_digest_b67e59f31ed46d146d93d05a2fadb57e,
#endif
        codeobj_6126135c7cbb61c3fa1c32e8b5013c8f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_2_log_text(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_2_log_text,
        const_str_plain_log_text,
#if PYTHON_VERSION >= 300
        const_str_digest_9ecd81565588f69489be3aa1647544be,
#endif
        codeobj_2a63f0c01a953744bec03a77141be4cf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_3_start(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog$$$function_3_start,
        const_str_plain_start,
#if PYTHON_VERSION >= 300
        const_str_digest_c33282d27179475fc074978e914787a0,
#endif
        codeobj_7016a14e5f3bff65e47b9d4ca6f6c078,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        4
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_7__run_dialog( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_7__run_dialog,
        const_str_plain__run_dialog,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_79a124d5bc6a11f1ebdd8117fc31775e,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        const_str_digest_9beaef72d01a9bfa2fc3089ffaa9d78d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_8__create_app(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_8__create_app,
        const_str_plain__create_app,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b118c56da081178845e85b4a1e8275ad,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_9__return_none(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$shortcuts$dialogs$$$function_9__return_none,
        const_str_plain__return_none,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_15611240512706b1f7d1f44edf4f7c5d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$shortcuts$dialogs,
        const_str_digest_efbb2c4267df7c70f47fe9b08f26ea33,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$shortcuts$dialogs =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.shortcuts.dialogs",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$shortcuts$dialogs)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$shortcuts$dialogs)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$shortcuts$dialogs );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.shortcuts.dialogs: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.shortcuts.dialogs: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.shortcuts.dialogs: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$shortcuts$dialogs" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$shortcuts$dialogs = Py_InitModule4(
        "prompt_toolkit.shortcuts.dialogs",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$shortcuts$dialogs = PyModule_Create( &mdef_prompt_toolkit$shortcuts$dialogs );
#endif

    moduledict_prompt_toolkit$shortcuts$dialogs = MODULE_DICT( module_prompt_toolkit$shortcuts$dialogs );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$shortcuts$dialogs,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$shortcuts$dialogs,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$shortcuts$dialogs,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$shortcuts$dialogs,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$shortcuts$dialogs );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_9611fcfd9c74c54c03226adf0f4399a4, module_prompt_toolkit$shortcuts$dialogs );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_948733170074654b40985584c6267e00;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_948733170074654b40985584c6267e00 = MAKE_MODULE_FRAME( codeobj_948733170074654b40985584c6267e00, module_prompt_toolkit$shortcuts$dialogs );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_948733170074654b40985584c6267e00 );
    assert( Py_REFCNT( frame_948733170074654b40985584c6267e00 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 1;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_functools;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 2;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_functools, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_6441b0d62e9f3dcafad087d802e7f46c;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_Application_tuple;
        tmp_level_name_2 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 3;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Application );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Application, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_722e5e8c7f53b34663d9669ca7ddc056;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_get_app_tuple;
        tmp_level_name_3 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 4;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_get_app );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_get_app, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_a9cf0ebcb31ab127e55d3f37cd8b3e84;
        tmp_globals_name_4 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_run_in_executor_tuple;
        tmp_level_name_4 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 5;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_run_in_executor );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_run_in_executor, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_7037044602504fe42542b695cfd14bf8;
        tmp_globals_name_5 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_focus_next_str_plain_focus_previous_tuple;
        tmp_level_name_5 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 6;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_9;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_focus_next );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_focus_next, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_6 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_focus_previous );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_focus_previous, tmp_assign_source_11 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_24b1c11e869e4a3b8d838ff0fe7758f4;
        tmp_globals_name_6 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_load_key_bindings_tuple;
        tmp_level_name_6 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 7;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_load_key_bindings );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_load_key_bindings, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_6695b0a823c8cd6b75b68a2528261696;
        tmp_globals_name_7 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_KeyBindings_str_plain_merge_key_bindings_tuple;
        tmp_level_name_7 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 8;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_13;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_8 = tmp_import_from_2__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_KeyBindings );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_KeyBindings, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_9 = tmp_import_from_2__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_merge_key_bindings );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_merge_key_bindings, tmp_assign_source_15 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_10;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_388882ed5238bd9499b19d10d66c5ccd;
        tmp_globals_name_8 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_Layout_tuple;
        tmp_level_name_8 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 9;
        tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_Layout );
        Py_DECREF( tmp_import_name_from_10 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Layout, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_11;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_f832e38dcf1aa78f989188cdc625749d;
        tmp_globals_name_9 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_HSplit_tuple;
        tmp_level_name_9 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 10;
        tmp_import_name_from_11 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_HSplit );
        Py_DECREF( tmp_import_name_from_11 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_HSplit, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_12;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_6a25fd7559aacbd9227f71754f616c06;
        tmp_globals_name_10 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_Dimension_tuple;
        tmp_level_name_10 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 11;
        tmp_import_name_from_12 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_Dimension );
        Py_DECREF( tmp_import_name_from_12 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_D, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_digest_3bb6d283c2212a91e00ea390103bbbae;
        tmp_globals_name_11 = (PyObject *)moduledict_prompt_toolkit$shortcuts$dialogs;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_57e0e1a1d3dfb5ed6cab3f231483c22f_tuple;
        tmp_level_name_11 = const_int_0;
        frame_948733170074654b40985584c6267e00->m_frame.f_lineno = 12;
        tmp_assign_source_19 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_19;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_13 = tmp_import_from_3__module;
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_ProgressBar );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_ProgressBar, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_14 = tmp_import_from_3__module;
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_Dialog );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Dialog, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_15;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_15 = tmp_import_from_3__module;
        tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_Button );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Button, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_16;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_16 = tmp_import_from_3__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_Label );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Label, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_17;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_17 = tmp_import_from_3__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_Box );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_Box, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_import_name_from_18;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_18 = tmp_import_from_3__module;
        tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_TextArea );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_TextArea, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_19;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_19 = tmp_import_from_3__module;
        tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_RadioList );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_RadioList, tmp_assign_source_26 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_948733170074654b40985584c6267e00 );
#endif
    popFrameStack();

    assertFrameObject( frame_948733170074654b40985584c6267e00 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_948733170074654b40985584c6267e00 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_948733170074654b40985584c6267e00, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_948733170074654b40985584c6267e00->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_948733170074654b40985584c6267e00, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = LIST_COPY( const_list_59e83616a8423114a44c9acf8cb2b915_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_str_empty_str_empty_str_plain_Yes_str_plain_No_none_false_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_28 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_1_yes_no_dialog( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_yes_no_dialog, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_defaults_2;
        PyObject *tmp_tuple_element_1;
        tmp_tuple_element_1 = const_str_empty;
        tmp_defaults_2 = PyTuple_New( 5 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_2, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_empty;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_2, 1, tmp_tuple_element_1 );
        tmp_tuple_element_1 = PyList_New( 0 );
        PyTuple_SET_ITEM( tmp_defaults_2, 2, tmp_tuple_element_1 );
        tmp_tuple_element_1 = Py_None;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_2, 3, tmp_tuple_element_1 );
        tmp_tuple_element_1 = Py_False;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_2, 4, tmp_tuple_element_1 );
        tmp_assign_source_29 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_2_button_dialog( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_button_dialog, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_0b5906710d2bcb79e2c89d7d0037545a_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_30 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_3_input_dialog( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_input_dialog, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_defaults_4;
        tmp_defaults_4 = const_tuple_str_empty_str_empty_str_plain_Ok_none_false_tuple;
        Py_INCREF( tmp_defaults_4 );
        tmp_assign_source_31 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_4_message_dialog( tmp_defaults_4 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_message_dialog, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_defaults_5;
        tmp_defaults_5 = const_tuple_5e1d1400760fc77656248084888d7faa_tuple;
        Py_INCREF( tmp_defaults_5 );
        tmp_assign_source_32 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_5_radiolist_dialog( tmp_defaults_5 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_radiolist_dialog, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_defaults_6;
        tmp_defaults_6 = const_tuple_str_empty_str_empty_none_none_false_tuple;
        Py_INCREF( tmp_defaults_6 );
        tmp_assign_source_33 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_6_progress_dialog( tmp_defaults_6 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain_progress_dialog, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_defaults_7;
        tmp_defaults_7 = const_tuple_false_tuple;
        Py_INCREF( tmp_defaults_7 );
        tmp_assign_source_34 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_7__run_dialog( tmp_defaults_7 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__run_dialog, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        tmp_assign_source_35 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_8__create_app(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__create_app, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        tmp_assign_source_36 = MAKE_FUNCTION_prompt_toolkit$shortcuts$dialogs$$$function_9__return_none(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$shortcuts$dialogs, (Nuitka_StringObject *)const_str_plain__return_none, tmp_assign_source_36 );
    }

    return MOD_RETURN_VALUE( module_prompt_toolkit$shortcuts$dialogs );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
