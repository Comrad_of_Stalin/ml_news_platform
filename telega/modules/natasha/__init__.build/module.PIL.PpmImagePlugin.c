/* Generated code for Python module 'PIL.PpmImagePlugin'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_PIL$PpmImagePlugin" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_PIL$PpmImagePlugin;
PyDictObject *moduledict_PIL$PpmImagePlugin;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_bytes_digest_068100f800f166e1a0984415b535ee78_tuple;
extern PyObject *const_str_plain___name__;
extern PyObject *const_tuple_str_plain___class___tuple;
static PyObject *const_tuple_bytes_digest_fbe8492d80df0fb1bb573875f5249835_tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_bytes_digest_15a4b5d63f8cee016e1f852eb529c3c5;
extern PyObject *const_int_pos_9;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_bd42c614d83fea2dbd2cd1bdfe9eeb47;
static PyObject *const_str_digest_2a629e2a918e961449be2f112e1df3fb;
extern PyObject *const_str_plain_xsize;
extern PyObject *const_str_plain_readline;
extern PyObject *const_str_plain_encode;
extern PyObject *const_str_plain_prefix;
extern PyObject *const_str_plain_rawmode;
extern PyObject *const_str_plain_mode;
static PyObject *const_str_digest_e453b5f0942d6161b96a72c0a9b41c4f;
extern PyObject *const_str_plain_size;
static PyObject *const_str_digest_9e7af868e4dac328abfc04f72aa7a337;
static PyObject *const_str_digest_55728bf1730bd489e2c9a9f90895f3fc;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_e66d80aae91f4378e8bf40b0596e62ea;
static PyObject *const_bytes_digest_fbe8492d80df0fb1bb573875f5249835;
extern PyObject *const_str_plain__size;
static PyObject *const_str_digest_26b9a5a3d68fe5cecdcbf7d45c9dd081;
extern PyObject *const_str_plain_RGB;
extern PyObject *const_tuple_int_0_int_0_tuple;
extern PyObject *const_str_plain_read;
static PyObject *const_tuple_91a874ce28495e8181687ffac7d90272_tuple;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_fp;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_L;
static PyObject *const_str_digest_3f4df5fa3b892d97c96cde850b8756db;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_tuple_str_plain_ascii_tuple;
extern PyObject *const_tuple_str_plain_prefix_tuple;
extern PyObject *const_str_plain_s;
extern PyObject *const_slice_int_0_int_pos_1_none;
static PyObject *const_tuple_e1f19c398fe21752819008b9fd6914da_tuple;
extern PyObject *const_str_plain_ix;
extern PyObject *const_str_plain_PPM;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_bytes_chr_121;
static PyObject *const_list_d08d468687a0ede0ef0ffde271e6d29a_list;
static PyObject *const_bytes_digest_068100f800f166e1a0984415b535ee78;
static PyObject *const_tuple_0cdcf938fa0a75fd02b2b45c25cdea09_tuple;
static PyObject *const_str_digest_a6870d3c367ef4f3568ba7bc65a36f7e;
extern PyObject *const_str_plain_head;
static PyObject *const_str_digest_e6a706ec6ea5349b76d787641b38682c;
static PyObject *const_str_digest_2c3f8b973aceb10398a274ee7c3d19f3;
extern PyObject *const_str_plain_getextrema;
static PyObject *const_bytes_digest_08121475ad2f818d9212057ba6579294;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_digest_a847edfea52ac2580b31c3747424369d;
extern PyObject *const_str_plain_CMYK;
static PyObject *const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2;
static PyObject *const_str_digest_1a36541f2170f7e60a55a4635b306514;
static PyObject *const_str_digest_b24527cbddb5a6705a734e7ff3590a1a;
static PyObject *const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple;
static PyObject *const_bytes_chr_80;
static PyObject *const_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b;
extern PyObject *const_str_plain_raw;
extern PyObject *const_str_plain_tell;
extern PyObject *const_str_plain_P;
static PyObject *const_str_digest_43d789eedc74e03e69d2688118d8a06f;
static PyObject *const_str_plain_magic_number;
static PyObject *const_str_plain__token;
static PyObject *const_bytes_digest_4d2dc43101e5d0fc30c8b1bfb65ec16c;
static PyObject *const_str_digest_32d3907b730c15f18611da338f7983f1;
extern PyObject *const_str_plain__open;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_xrange_0_3;
static PyObject *const_str_digest_cef19ca0844be5e9a19600a12469f64b;
extern PyObject *const_str_plain_format_description;
extern PyObject *const_str_plain_I;
extern PyObject *const_str_plain___version__;
extern PyObject *const_int_0;
static PyObject *const_str_digest_ad0990c1df7a84639097776f6273455b;
static PyObject *const_str_digest_3c05dd9c36a31a84904e36ab0e4c89f7;
static PyObject *const_tuple_dbf41cef26db995a4f33d593a6e8a77b_tuple;
extern PyObject *const_str_plain__accept;
extern PyObject *const_str_plain_tile;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_tuple_str_plain_RGB_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b_tuple;
static PyObject *const_str_digest_eb7da27fe2946d16642c09f15d05e1c2;
extern PyObject *const_str_plain_c;
extern PyObject *const_str_plain_register_mime;
extern PyObject *const_str_plain_register_extensions;
static PyObject *const_bytes_digest_00e6c0c3954f108ce216ad15549a73be;
extern PyObject *const_str_plain_type;
extern PyObject *const_bytes_empty;
static PyObject *const_tuple_str_plain_Image_str_plain_ImageFile_tuple;
extern PyObject *const_str_plain_custom_mimetype;
static PyObject *const_str_plain_PpmImageFile;
static PyObject *const_bytes_digest_a9b0e0403305e2f34eed7f4cb54fb529;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_digest_772c0402ef22c7a4d610f4443976d74a;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_digest_679d6818068454a9e371d2b3ba6d34cc;
extern PyObject *const_str_plain___module__;
static PyObject *const_tuple_str_plain_L_bytes_digest_16ced812a77049cdfddb5a13d721e7b2_tuple;
extern PyObject *const_str_plain_filename;
extern PyObject *const_int_pos_1;
static PyObject *const_dict_27ba4113e9eac4d66d1ace3452229792;
static PyObject *const_bytes_digest_3a9760a0c39cb7a262c45b1122f69397;
static PyObject *const_str_digest_118eef0c52909dfd5eda837274266484;
extern PyObject *const_str_plain_1;
extern PyObject *const_str_plain_ImageFile;
extern PyObject *const_str_plain_register_save;
extern PyObject *const_str_plain__save;
static PyObject *const_bytes_digest_a4c38feed075c17accfeb9feeaeae825;
static PyObject *const_tuple_bytes_digest_00e6c0c3954f108ce216ad15549a73be_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_ysize;
extern PyObject *const_int_pos_255;
extern PyObject *const_str_digest_a43c96e669182759de7b6dc21baa30ee;
extern PyObject *const_str_plain_im;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_RGBA;
extern PyObject *const_str_plain_write;
extern PyObject *const_bytes_chr_35;
extern PyObject *const_str_plain_get;
static PyObject *const_str_plain_b_whitespace;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_2;
static PyObject *const_tuple_str_plain_self_str_plain_s_str_plain_c_tuple;
static PyObject *const_dict_29f0cf79590a0d506be80209104966ab;
extern PyObject *const_str_plain_ascii;
extern PyObject *const_str_plain_Image;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_MODES;
extern PyObject *const_str_empty;
extern PyObject *const_int_pos_65536;
extern PyObject *const_str_plain_register_open;
static PyObject *const_str_digest_648cc61aa0181976297f663d5722d0a7;
static PyObject *const_bytes_digest_2711fba61fd27335c28d1e0fd3c6ae51;
extern PyObject *const_tuple_bytes_empty_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_bytes_digest_068100f800f166e1a0984415b535ee78_tuple = PyTuple_New( 1 );
    const_bytes_digest_068100f800f166e1a0984415b535ee78 = UNSTREAM_BYTES( &constant_bin[ 93823 ], 6 );
    PyTuple_SET_ITEM( const_tuple_bytes_digest_068100f800f166e1a0984415b535ee78_tuple, 0, const_bytes_digest_068100f800f166e1a0984415b535ee78 ); Py_INCREF( const_bytes_digest_068100f800f166e1a0984415b535ee78 );
    const_tuple_bytes_digest_fbe8492d80df0fb1bb573875f5249835_tuple = PyTuple_New( 1 );
    const_bytes_digest_fbe8492d80df0fb1bb573875f5249835 = UNSTREAM_BYTES( &constant_bin[ 93829 ], 4 );
    PyTuple_SET_ITEM( const_tuple_bytes_digest_fbe8492d80df0fb1bb573875f5249835_tuple, 0, const_bytes_digest_fbe8492d80df0fb1bb573875f5249835 ); Py_INCREF( const_bytes_digest_fbe8492d80df0fb1bb573875f5249835 );
    const_bytes_digest_15a4b5d63f8cee016e1f852eb529c3c5 = UNSTREAM_BYTES( &constant_bin[ 93833 ], 6 );
    const_str_digest_bd42c614d83fea2dbd2cd1bdfe9eeb47 = UNSTREAM_STRING_ASCII( &constant_bin[ 93839 ], 4, 0 );
    const_str_digest_2a629e2a918e961449be2f112e1df3fb = UNSTREAM_STRING_ASCII( &constant_bin[ 93843 ], 4, 0 );
    const_str_digest_e453b5f0942d6161b96a72c0a9b41c4f = UNSTREAM_STRING_ASCII( &constant_bin[ 93847 ], 40, 0 );
    const_str_digest_9e7af868e4dac328abfc04f72aa7a337 = UNSTREAM_STRING_ASCII( &constant_bin[ 93887 ], 18, 0 );
    const_str_digest_55728bf1730bd489e2c9a9f90895f3fc = UNSTREAM_STRING_ASCII( &constant_bin[ 93905 ], 21, 0 );
    const_str_digest_e66d80aae91f4378e8bf40b0596e62ea = UNSTREAM_STRING_ASCII( &constant_bin[ 93926 ], 27, 0 );
    const_str_digest_26b9a5a3d68fe5cecdcbf7d45c9dd081 = UNSTREAM_STRING_ASCII( &constant_bin[ 93953 ], 4, 0 );
    const_tuple_91a874ce28495e8181687ffac7d90272_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_91a874ce28495e8181687ffac7d90272_tuple, 0, const_str_digest_a847edfea52ac2580b31c3747424369d ); Py_INCREF( const_str_digest_a847edfea52ac2580b31c3747424369d );
    const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2 = UNSTREAM_BYTES( &constant_bin[ 77368 ], 2 );
    PyTuple_SET_ITEM( const_tuple_91a874ce28495e8181687ffac7d90272_tuple, 1, const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2 ); Py_INCREF( const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2 );
    const_str_digest_3f4df5fa3b892d97c96cde850b8756db = UNSTREAM_STRING_ASCII( &constant_bin[ 93957 ], 28, 0 );
    const_tuple_e1f19c398fe21752819008b9fd6914da_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_e1f19c398fe21752819008b9fd6914da_tuple, 0, const_str_plain_im ); Py_INCREF( const_str_plain_im );
    PyTuple_SET_ITEM( const_tuple_e1f19c398fe21752819008b9fd6914da_tuple, 1, const_str_plain_fp ); Py_INCREF( const_str_plain_fp );
    PyTuple_SET_ITEM( const_tuple_e1f19c398fe21752819008b9fd6914da_tuple, 2, const_str_plain_filename ); Py_INCREF( const_str_plain_filename );
    PyTuple_SET_ITEM( const_tuple_e1f19c398fe21752819008b9fd6914da_tuple, 3, const_str_plain_rawmode ); Py_INCREF( const_str_plain_rawmode );
    PyTuple_SET_ITEM( const_tuple_e1f19c398fe21752819008b9fd6914da_tuple, 4, const_str_plain_head ); Py_INCREF( const_str_plain_head );
    const_bytes_chr_121 = UNSTREAM_BYTES( &constant_bin[ 14 ], 1 );
    const_list_d08d468687a0ede0ef0ffde271e6d29a_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_d08d468687a0ede0ef0ffde271e6d29a_list, 0, const_str_digest_26b9a5a3d68fe5cecdcbf7d45c9dd081 ); Py_INCREF( const_str_digest_26b9a5a3d68fe5cecdcbf7d45c9dd081 );
    const_str_digest_2c3f8b973aceb10398a274ee7c3d19f3 = UNSTREAM_STRING_ASCII( &constant_bin[ 93985 ], 4, 0 );
    PyList_SET_ITEM( const_list_d08d468687a0ede0ef0ffde271e6d29a_list, 1, const_str_digest_2c3f8b973aceb10398a274ee7c3d19f3 ); Py_INCREF( const_str_digest_2c3f8b973aceb10398a274ee7c3d19f3 );
    PyList_SET_ITEM( const_list_d08d468687a0ede0ef0ffde271e6d29a_list, 2, const_str_digest_2a629e2a918e961449be2f112e1df3fb ); Py_INCREF( const_str_digest_2a629e2a918e961449be2f112e1df3fb );
    PyList_SET_ITEM( const_list_d08d468687a0ede0ef0ffde271e6d29a_list, 3, const_str_digest_bd42c614d83fea2dbd2cd1bdfe9eeb47 ); Py_INCREF( const_str_digest_bd42c614d83fea2dbd2cd1bdfe9eeb47 );
    const_tuple_0cdcf938fa0a75fd02b2b45c25cdea09_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_0cdcf938fa0a75fd02b2b45c25cdea09_tuple, 0, const_str_digest_772c0402ef22c7a4d610f4443976d74a ); Py_INCREF( const_str_digest_772c0402ef22c7a4d610f4443976d74a );
    PyTuple_SET_ITEM( const_tuple_0cdcf938fa0a75fd02b2b45c25cdea09_tuple, 1, const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2 ); Py_INCREF( const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2 );
    const_str_digest_a6870d3c367ef4f3568ba7bc65a36f7e = UNSTREAM_STRING_ASCII( &constant_bin[ 93989 ], 23, 0 );
    const_str_digest_e6a706ec6ea5349b76d787641b38682c = UNSTREAM_STRING_ASCII( &constant_bin[ 94012 ], 7, 0 );
    const_bytes_digest_08121475ad2f818d9212057ba6579294 = UNSTREAM_BYTES( &constant_bin[ 80443 ], 2 );
    const_str_digest_1a36541f2170f7e60a55a4635b306514 = UNSTREAM_STRING_ASCII( &constant_bin[ 94019 ], 34, 0 );
    const_str_digest_b24527cbddb5a6705a734e7ff3590a1a = UNSTREAM_STRING_ASCII( &constant_bin[ 94053 ], 19, 0 );
    const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple, 1, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    const_str_plain_magic_number = UNSTREAM_STRING_ASCII( &constant_bin[ 94072 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple, 2, const_str_plain_magic_number ); Py_INCREF( const_str_plain_magic_number );
    PyTuple_SET_ITEM( const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple, 3, const_str_plain_mode ); Py_INCREF( const_str_plain_mode );
    PyTuple_SET_ITEM( const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple, 4, const_str_plain_rawmode ); Py_INCREF( const_str_plain_rawmode );
    PyTuple_SET_ITEM( const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple, 5, const_str_plain_ix ); Py_INCREF( const_str_plain_ix );
    PyTuple_SET_ITEM( const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple, 6, const_str_plain_xsize ); Py_INCREF( const_str_plain_xsize );
    PyTuple_SET_ITEM( const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple, 7, const_str_plain_ysize ); Py_INCREF( const_str_plain_ysize );
    const_bytes_chr_80 = UNSTREAM_BYTES( &constant_bin[ 0 ], 1 );
    const_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b = UNSTREAM_BYTES( &constant_bin[ 80560 ], 2 );
    const_str_digest_43d789eedc74e03e69d2688118d8a06f = UNSTREAM_STRING_ASCII( &constant_bin[ 94084 ], 23, 0 );
    const_str_plain__token = UNSTREAM_STRING_ASCII( &constant_bin[ 94066 ], 6, 1 );
    const_bytes_digest_4d2dc43101e5d0fc30c8b1bfb65ec16c = UNSTREAM_BYTES( &constant_bin[ 94107 ], 6 );
    const_str_digest_32d3907b730c15f18611da338f7983f1 = UNSTREAM_STRING_ASCII( &constant_bin[ 94113 ], 27, 0 );
    const_str_digest_cef19ca0844be5e9a19600a12469f64b = UNSTREAM_STRING_ASCII( &constant_bin[ 94140 ], 24, 0 );
    const_str_digest_ad0990c1df7a84639097776f6273455b = UNSTREAM_STRING_ASCII( &constant_bin[ 93934 ], 18, 0 );
    const_str_digest_3c05dd9c36a31a84904e36ab0e4c89f7 = UNSTREAM_STRING_ASCII( &constant_bin[ 94164 ], 13, 0 );
    const_tuple_dbf41cef26db995a4f33d593a6e8a77b_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_dbf41cef26db995a4f33d593a6e8a77b_tuple, 0, const_str_digest_a43c96e669182759de7b6dc21baa30ee ); Py_INCREF( const_str_digest_a43c96e669182759de7b6dc21baa30ee );
    PyTuple_SET_ITEM( const_tuple_dbf41cef26db995a4f33d593a6e8a77b_tuple, 1, const_bytes_digest_08121475ad2f818d9212057ba6579294 ); Py_INCREF( const_bytes_digest_08121475ad2f818d9212057ba6579294 );
    const_tuple_str_plain_RGB_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_RGB_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b_tuple, 0, const_str_plain_RGB ); Py_INCREF( const_str_plain_RGB );
    PyTuple_SET_ITEM( const_tuple_str_plain_RGB_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b_tuple, 1, const_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b ); Py_INCREF( const_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b );
    const_str_digest_eb7da27fe2946d16642c09f15d05e1c2 = UNSTREAM_STRING_ASCII( &constant_bin[ 94177 ], 23, 0 );
    const_bytes_digest_00e6c0c3954f108ce216ad15549a73be = UNSTREAM_BYTES( &constant_bin[ 94200 ], 11 );
    const_tuple_str_plain_Image_str_plain_ImageFile_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Image_str_plain_ImageFile_tuple, 0, const_str_plain_Image ); Py_INCREF( const_str_plain_Image );
    PyTuple_SET_ITEM( const_tuple_str_plain_Image_str_plain_ImageFile_tuple, 1, const_str_plain_ImageFile ); Py_INCREF( const_str_plain_ImageFile );
    const_str_plain_PpmImageFile = UNSTREAM_STRING_ASCII( &constant_bin[ 93887 ], 12, 1 );
    const_bytes_digest_a9b0e0403305e2f34eed7f4cb54fb529 = UNSTREAM_BYTES( &constant_bin[ 94211 ], 5 );
    const_str_digest_679d6818068454a9e371d2b3ba6d34cc = UNSTREAM_STRING_ASCII( &constant_bin[ 94216 ], 3, 0 );
    const_tuple_str_plain_L_bytes_digest_16ced812a77049cdfddb5a13d721e7b2_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_L_bytes_digest_16ced812a77049cdfddb5a13d721e7b2_tuple, 0, const_str_plain_L ); Py_INCREF( const_str_plain_L );
    PyTuple_SET_ITEM( const_tuple_str_plain_L_bytes_digest_16ced812a77049cdfddb5a13d721e7b2_tuple, 1, const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2 ); Py_INCREF( const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2 );
    const_dict_27ba4113e9eac4d66d1ace3452229792 = _PyDict_NewPresized( 7 );
    PyDict_SetItem( const_dict_27ba4113e9eac4d66d1ace3452229792, const_bytes_digest_08121475ad2f818d9212057ba6579294, const_str_plain_1 );
    PyDict_SetItem( const_dict_27ba4113e9eac4d66d1ace3452229792, const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2, const_str_plain_L );
    PyDict_SetItem( const_dict_27ba4113e9eac4d66d1ace3452229792, const_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b, const_str_plain_RGB );
    PyDict_SetItem( const_dict_27ba4113e9eac4d66d1ace3452229792, const_bytes_digest_15a4b5d63f8cee016e1f852eb529c3c5, const_str_plain_CMYK );
    const_bytes_digest_a4c38feed075c17accfeb9feeaeae825 = UNSTREAM_BYTES( &constant_bin[ 94219 ], 3 );
    PyDict_SetItem( const_dict_27ba4113e9eac4d66d1ace3452229792, const_bytes_digest_a4c38feed075c17accfeb9feeaeae825, const_str_plain_P );
    const_bytes_digest_3a9760a0c39cb7a262c45b1122f69397 = UNSTREAM_BYTES( &constant_bin[ 94222 ], 6 );
    PyDict_SetItem( const_dict_27ba4113e9eac4d66d1ace3452229792, const_bytes_digest_3a9760a0c39cb7a262c45b1122f69397, const_str_plain_RGBA );
    const_bytes_digest_2711fba61fd27335c28d1e0fd3c6ae51 = UNSTREAM_BYTES( &constant_bin[ 94228 ], 6 );
    PyDict_SetItem( const_dict_27ba4113e9eac4d66d1ace3452229792, const_bytes_digest_2711fba61fd27335c28d1e0fd3c6ae51, const_str_plain_CMYK );
    assert( PyDict_Size( const_dict_27ba4113e9eac4d66d1ace3452229792 ) == 7 );
    const_str_digest_118eef0c52909dfd5eda837274266484 = UNSTREAM_STRING_ASCII( &constant_bin[ 94234 ], 14, 0 );
    const_tuple_bytes_digest_00e6c0c3954f108ce216ad15549a73be_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_bytes_digest_00e6c0c3954f108ce216ad15549a73be_tuple, 0, const_bytes_digest_00e6c0c3954f108ce216ad15549a73be ); Py_INCREF( const_bytes_digest_00e6c0c3954f108ce216ad15549a73be );
    const_str_plain_b_whitespace = UNSTREAM_STRING_ASCII( &constant_bin[ 94248 ], 12, 1 );
    const_tuple_str_plain_self_str_plain_s_str_plain_c_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_s_str_plain_c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_s_str_plain_c_tuple, 1, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_s_str_plain_c_tuple, 2, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_dict_29f0cf79590a0d506be80209104966ab = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_29f0cf79590a0d506be80209104966ab, const_bytes_digest_08121475ad2f818d9212057ba6579294, const_str_digest_43d789eedc74e03e69d2688118d8a06f );
    PyDict_SetItem( const_dict_29f0cf79590a0d506be80209104966ab, const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2, const_str_digest_cef19ca0844be5e9a19600a12469f64b );
    PyDict_SetItem( const_dict_29f0cf79590a0d506be80209104966ab, const_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b, const_str_digest_eb7da27fe2946d16642c09f15d05e1c2 );
    assert( PyDict_Size( const_dict_29f0cf79590a0d506be80209104966ab ) == 3 );
    const_str_digest_648cc61aa0181976297f663d5722d0a7 = UNSTREAM_STRING_ASCII( &constant_bin[ 94260 ], 28, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_PIL$PpmImagePlugin( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_54a179603fbff795e0c7198497ba6a46;
static PyCodeObject *codeobj_622c0ba4b492b66a6921975cf4ca8a0d;
static PyCodeObject *codeobj_c808db28b95858ee432b290bb51ec2c5;
static PyCodeObject *codeobj_8ba6508e69fd0ebf2cd800e36304af34;
static PyCodeObject *codeobj_c9f23e91cc092cf6ae6adb2d5ee8a434;
static PyCodeObject *codeobj_90282576f9587754e22a5abdb328d373;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_55728bf1730bd489e2c9a9f90895f3fc );
    codeobj_54a179603fbff795e0c7198497ba6a46 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_e66d80aae91f4378e8bf40b0596e62ea, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_622c0ba4b492b66a6921975cf4ca8a0d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_PpmImageFile, 50, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_c808db28b95858ee432b290bb51ec2c5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__accept, 43, const_tuple_str_plain_prefix_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8ba6508e69fd0ebf2cd800e36304af34 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__open, 67, const_tuple_3a07134106fc2c4d4c7d9193f6d4cbf9_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c9f23e91cc092cf6ae6adb2d5ee8a434 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__save, 129, const_tuple_e1f19c398fe21752819008b9fd6914da_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_90282576f9587754e22a5abdb328d373 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__token, 55, const_tuple_str_plain_self_str_plain_s_str_plain_c_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_1__accept(  );


static PyObject *MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_2__token( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_3__open(  );


static PyObject *MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_4__save(  );


// The module function definitions.
static PyObject *impl_PIL$PpmImagePlugin$$$function_1__accept( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_prefix = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c808db28b95858ee432b290bb51ec2c5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_c808db28b95858ee432b290bb51ec2c5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c808db28b95858ee432b290bb51ec2c5, codeobj_c808db28b95858ee432b290bb51ec2c5, module_PIL$PpmImagePlugin, sizeof(void *) );
    frame_c808db28b95858ee432b290bb51ec2c5 = cache_frame_c808db28b95858ee432b290bb51ec2c5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c808db28b95858ee432b290bb51ec2c5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c808db28b95858ee432b290bb51ec2c5 ) == 2 ); // Frame stack

    // Framed code:
    {
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( par_prefix );
        tmp_subscribed_name_1 = par_prefix;
        tmp_subscript_name_1 = const_slice_int_0_int_pos_1_none;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_bytes_chr_80;
        tmp_and_left_value_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_and_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_and_left_value_1 );

            exception_lineno = 44;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        Py_DECREF( tmp_and_left_value_1 );
        CHECK_OBJECT( par_prefix );
        tmp_subscribed_name_2 = par_prefix;
        tmp_subscript_name_2 = const_int_pos_1;
        tmp_compexpr_left_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_bytes_digest_a9b0e0403305e2f34eed7f4cb54fb529;
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_and_right_value_1 );
        tmp_return_value = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_return_value = tmp_and_left_value_1;
        and_end_1:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c808db28b95858ee432b290bb51ec2c5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c808db28b95858ee432b290bb51ec2c5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c808db28b95858ee432b290bb51ec2c5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c808db28b95858ee432b290bb51ec2c5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c808db28b95858ee432b290bb51ec2c5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c808db28b95858ee432b290bb51ec2c5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c808db28b95858ee432b290bb51ec2c5,
        type_description_1,
        par_prefix
    );


    // Release cached frame.
    if ( frame_c808db28b95858ee432b290bb51ec2c5 == cache_frame_c808db28b95858ee432b290bb51ec2c5 )
    {
        Py_DECREF( frame_c808db28b95858ee432b290bb51ec2c5 );
    }
    cache_frame_c808db28b95858ee432b290bb51ec2c5 = NULL;

    assertFrameObject( frame_c808db28b95858ee432b290bb51ec2c5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin$$$function_1__accept );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_prefix );
    Py_DECREF( par_prefix );
    par_prefix = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_prefix );
    Py_DECREF( par_prefix );
    par_prefix = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin$$$function_1__accept );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$PpmImagePlugin$$$function_2__token( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_s = python_pars[ 1 ];
    PyObject *var_c = NULL;
    struct Nuitka_FrameObject *frame_90282576f9587754e22a5abdb328d373;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_90282576f9587754e22a5abdb328d373 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_90282576f9587754e22a5abdb328d373, codeobj_90282576f9587754e22a5abdb328d373, module_PIL$PpmImagePlugin, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_90282576f9587754e22a5abdb328d373 = cache_frame_90282576f9587754e22a5abdb328d373;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_90282576f9587754e22a5abdb328d373 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_90282576f9587754e22a5abdb328d373 ) == 2 ); // Frame stack

    // Framed code:
    loop_start_1:;
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_fp );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_90282576f9587754e22a5abdb328d373->m_frame.f_lineno = 57;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_read, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_c;
            var_c = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( var_c );
        tmp_operand_name_1 = var_c;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( var_c );
        tmp_compexpr_left_1 = var_c;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_b_whitespace );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_b_whitespace );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "b_whitespace" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_1;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_1 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_c );
        tmp_compexpr_left_2 = var_c;
        tmp_compexpr_right_2 = const_bytes_chr_121;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_1a36541f2170f7e60a55a4635b306514;
            frame_90282576f9587754e22a5abdb328d373->m_frame.f_lineno = 61;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 61;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_s );
        tmp_left_name_1 = par_s;
        CHECK_OBJECT( var_c );
        tmp_right_name_1 = var_c;
        tmp_assign_source_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_s;
            assert( old != NULL );
            par_s = tmp_assign_source_2;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( par_s );
        tmp_len_arg_1 = par_s;
        tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_3 = const_int_pos_9;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_make_exception_arg_2;
            tmp_make_exception_arg_2 = const_str_digest_648cc61aa0181976297f663d5722d0a7;
            frame_90282576f9587754e22a5abdb328d373->m_frame.f_lineno = 64;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_2 };
                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_2 == NULL) );
            exception_type = tmp_raise_type_2;
            exception_lineno = 64;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_3:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 56;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_90282576f9587754e22a5abdb328d373 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_90282576f9587754e22a5abdb328d373 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_90282576f9587754e22a5abdb328d373, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_90282576f9587754e22a5abdb328d373->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_90282576f9587754e22a5abdb328d373, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_90282576f9587754e22a5abdb328d373,
        type_description_1,
        par_self,
        par_s,
        var_c
    );


    // Release cached frame.
    if ( frame_90282576f9587754e22a5abdb328d373 == cache_frame_90282576f9587754e22a5abdb328d373 )
    {
        Py_DECREF( frame_90282576f9587754e22a5abdb328d373 );
    }
    cache_frame_90282576f9587754e22a5abdb328d373 = NULL;

    assertFrameObject( frame_90282576f9587754e22a5abdb328d373 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_s );
    tmp_return_value = par_s;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin$$$function_2__token );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)var_c );
    Py_DECREF( var_c );
    var_c = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_s );
    par_s = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin$$$function_2__token );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$PpmImagePlugin$$$function_3__open( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_s = NULL;
    PyObject *var_magic_number = NULL;
    PyObject *var_mode = NULL;
    PyObject *var_rawmode = NULL;
    PyObject *var_ix = NULL;
    PyObject *var_xsize = NULL;
    PyObject *var_ysize = NULL;
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_8ba6508e69fd0ebf2cd800e36304af34;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_8ba6508e69fd0ebf2cd800e36304af34 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8ba6508e69fd0ebf2cd800e36304af34, codeobj_8ba6508e69fd0ebf2cd800e36304af34, module_PIL$PpmImagePlugin, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8ba6508e69fd0ebf2cd800e36304af34 = cache_frame_8ba6508e69fd0ebf2cd800e36304af34;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8ba6508e69fd0ebf2cd800e36304af34 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8ba6508e69fd0ebf2cd800e36304af34 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_fp );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 70;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_read, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_s == NULL );
        var_s = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_s );
        tmp_compexpr_left_1 = var_s;
        tmp_compexpr_right_1 = const_bytes_chr_80;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_118eef0c52909dfd5eda837274266484;
            frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 72;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_SyntaxError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 72;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( var_s );
        tmp_args_element_name_1 = var_s;
        frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 73;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain__token, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_magic_number == NULL );
        var_magic_number = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_MODES );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MODES );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MODES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 74;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_magic_number );
        tmp_subscript_name_1 = var_magic_number;
        tmp_assign_source_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_mode == NULL );
        var_mode = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_assattr_target_1;
        tmp_called_instance_3 = PyDict_Copy( const_dict_29f0cf79590a0d506be80209104966ab );
        CHECK_OBJECT( var_magic_number );
        tmp_args_element_name_2 = var_magic_number;
        frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 76;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assattr_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_custom_mimetype, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_mode );
        tmp_compexpr_left_2 = var_mode;
        tmp_compexpr_right_2 = const_str_plain_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_assattr_target_2;
            tmp_assattr_name_2 = const_str_plain_1;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_2 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_mode, tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_assign_source_4;
            tmp_assign_source_4 = const_str_digest_a43c96e669182759de7b6dc21baa30ee;
            assert( var_rawmode == NULL );
            Py_INCREF( tmp_assign_source_4 );
            var_rawmode = tmp_assign_source_4;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( var_mode );
            tmp_assign_source_5 = var_mode;
            assert( tmp_assign_unpack_1__assign_source == NULL );
            Py_INCREF( tmp_assign_source_5 );
            tmp_assign_unpack_1__assign_source = tmp_assign_source_5;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_3;
            PyObject *tmp_assattr_target_3;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_assattr_name_3 = tmp_assign_unpack_1__assign_source;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_3 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_mode, tmp_assattr_name_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
        Py_DECREF( tmp_assign_unpack_1__assign_source );
        tmp_assign_unpack_1__assign_source = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        {
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_assign_source_6 = tmp_assign_unpack_1__assign_source;
            assert( var_rawmode == NULL );
            Py_INCREF( tmp_assign_source_6 );
            var_rawmode = tmp_assign_source_6;
        }
        CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
        Py_DECREF( tmp_assign_unpack_1__assign_source );
        tmp_assign_unpack_1__assign_source = NULL;

        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_iter_arg_1;
        tmp_iter_arg_1 = const_xrange_0_3;
        tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
        assert( !(tmp_assign_source_7 == NULL) );
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_7;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 88;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_9 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_ix;
            var_ix = tmp_assign_source_9;
            Py_INCREF( var_ix );
            Py_XDECREF( old );
        }

    }
    loop_start_2:;
    loop_start_3:;
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_fp );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 91;
        tmp_assign_source_10 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_read, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = var_s;
            assert( old != NULL );
            var_s = tmp_assign_source_10;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_s );
        tmp_compexpr_left_3 = var_s;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_b_whitespace );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_b_whitespace );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "b_whitespace" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }

        tmp_compexpr_right_3 = tmp_mvar_value_2;
        tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        goto loop_end_3;
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        CHECK_OBJECT( var_s );
        tmp_compexpr_left_4 = var_s;
        tmp_compexpr_right_4 = const_bytes_empty;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_make_exception_arg_2;
            tmp_make_exception_arg_2 = const_str_digest_e453b5f0942d6161b96a72c0a9b41c4f;
            frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 95;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_2 };
                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_2 == NULL) );
            exception_type = tmp_raise_type_2;
            exception_lineno = 95;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        branch_no_4:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 90;
        type_description_1 = "oooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_3;
    loop_end_3:;
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        CHECK_OBJECT( var_s );
        tmp_compexpr_left_5 = var_s;
        tmp_compexpr_right_5 = const_bytes_chr_35;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        goto loop_end_2;
        branch_no_5:;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_called_instance_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_fp );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 99;
        tmp_assign_source_11 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_readline );
        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = var_s;
            assert( old != NULL );
            var_s = tmp_assign_source_11;
            Py_DECREF( old );
        }

    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 89;
        type_description_1 = "oooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_int_arg_1;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_self );
        tmp_called_instance_6 = par_self;
        CHECK_OBJECT( var_s );
        tmp_args_element_name_3 = var_s;
        frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_int_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain__token, call_args );
        }

        if ( tmp_int_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_12 = PyNumber_Int( tmp_int_arg_1 );
        Py_DECREF( tmp_int_arg_1 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = var_s;
            assert( old != NULL );
            var_s = tmp_assign_source_12;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        CHECK_OBJECT( var_ix );
        tmp_compexpr_left_6 = var_ix;
        tmp_compexpr_right_6 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_13;
            CHECK_OBJECT( var_s );
            tmp_assign_source_13 = var_s;
            {
                PyObject *old = var_xsize;
                var_xsize = tmp_assign_source_13;
                Py_INCREF( var_xsize );
                Py_XDECREF( old );
            }

        }
        goto branch_end_6;
        branch_no_6:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            CHECK_OBJECT( var_ix );
            tmp_compexpr_left_7 = var_ix;
            tmp_compexpr_right_7 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 103;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_assign_source_14;
                CHECK_OBJECT( var_s );
                tmp_assign_source_14 = var_s;
                {
                    PyObject *old = var_ysize;
                    var_ysize = tmp_assign_source_14;
                    Py_INCREF( var_ysize );
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_8;
                PyObject *tmp_compexpr_left_8;
                PyObject *tmp_compexpr_right_8;
                CHECK_OBJECT( var_mode );
                tmp_compexpr_left_8 = var_mode;
                tmp_compexpr_right_8 = const_str_plain_1;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 105;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                goto loop_end_1;
                branch_no_8:;
            }
            goto branch_end_7;
            branch_no_7:;
            {
                nuitka_bool tmp_condition_result_9;
                PyObject *tmp_compexpr_left_9;
                PyObject *tmp_compexpr_right_9;
                CHECK_OBJECT( var_ix );
                tmp_compexpr_left_9 = var_ix;
                tmp_compexpr_right_9 = const_int_pos_2;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 107;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_9;
                }
                else
                {
                    goto branch_no_9;
                }
                branch_yes_9:;
                {
                    nuitka_bool tmp_condition_result_10;
                    PyObject *tmp_compexpr_left_10;
                    PyObject *tmp_compexpr_right_10;
                    CHECK_OBJECT( var_s );
                    tmp_compexpr_left_10 = var_s;
                    tmp_compexpr_right_10 = const_int_pos_255;
                    tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 109;
                        type_description_1 = "oooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_10;
                    }
                    else
                    {
                        goto branch_no_10;
                    }
                    branch_yes_10:;
                    {
                        nuitka_bool tmp_condition_result_11;
                        PyObject *tmp_operand_name_1;
                        PyObject *tmp_compexpr_left_11;
                        PyObject *tmp_compexpr_right_11;
                        CHECK_OBJECT( var_mode );
                        tmp_compexpr_left_11 = var_mode;
                        tmp_compexpr_right_11 = const_str_plain_L;
                        tmp_operand_name_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
                        if ( tmp_operand_name_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 110;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                        Py_DECREF( tmp_operand_name_1 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 110;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_11;
                        }
                        else
                        {
                            goto branch_no_11;
                        }
                        branch_yes_11:;
                        {
                            PyObject *tmp_raise_type_3;
                            PyObject *tmp_make_exception_arg_3;
                            PyObject *tmp_left_name_1;
                            PyObject *tmp_right_name_1;
                            tmp_left_name_1 = const_str_digest_3f4df5fa3b892d97c96cde850b8756db;
                            CHECK_OBJECT( var_s );
                            tmp_right_name_1 = var_s;
                            tmp_make_exception_arg_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                            if ( tmp_make_exception_arg_3 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 111;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_3;
                            }
                            frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 111;
                            {
                                PyObject *call_args[] = { tmp_make_exception_arg_3 };
                                tmp_raise_type_3 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                            }

                            Py_DECREF( tmp_make_exception_arg_3 );
                            assert( !(tmp_raise_type_3 == NULL) );
                            exception_type = tmp_raise_type_3;
                            exception_lineno = 111;
                            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        branch_no_11:;
                    }
                    {
                        nuitka_bool tmp_condition_result_12;
                        PyObject *tmp_compexpr_left_12;
                        PyObject *tmp_compexpr_right_12;
                        CHECK_OBJECT( var_s );
                        tmp_compexpr_left_12 = var_s;
                        tmp_compexpr_right_12 = const_int_pos_65536;
                        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 112;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_12;
                        }
                        else
                        {
                            goto branch_no_12;
                        }
                        branch_yes_12:;
                        {
                            PyObject *tmp_assattr_name_4;
                            PyObject *tmp_assattr_target_4;
                            tmp_assattr_name_4 = const_str_plain_I;
                            CHECK_OBJECT( par_self );
                            tmp_assattr_target_4 = par_self;
                            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_mode, tmp_assattr_name_4 );
                            if ( tmp_result == false )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 113;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_3;
                            }
                        }
                        {
                            PyObject *tmp_assign_source_15;
                            tmp_assign_source_15 = const_str_digest_772c0402ef22c7a4d610f4443976d74a;
                            {
                                PyObject *old = var_rawmode;
                                var_rawmode = tmp_assign_source_15;
                                Py_INCREF( var_rawmode );
                                Py_XDECREF( old );
                            }

                        }
                        goto branch_end_12;
                        branch_no_12:;
                        {
                            PyObject *tmp_assattr_name_5;
                            PyObject *tmp_assattr_target_5;
                            tmp_assattr_name_5 = const_str_plain_I;
                            CHECK_OBJECT( par_self );
                            tmp_assattr_target_5 = par_self;
                            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_mode, tmp_assattr_name_5 );
                            if ( tmp_result == false )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 116;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_3;
                            }
                        }
                        {
                            PyObject *tmp_assign_source_16;
                            tmp_assign_source_16 = const_str_digest_a847edfea52ac2580b31c3747424369d;
                            {
                                PyObject *old = var_rawmode;
                                var_rawmode = tmp_assign_source_16;
                                Py_INCREF( var_rawmode );
                                Py_XDECREF( old );
                            }

                        }
                        branch_end_12:;
                    }
                    branch_no_10:;
                }
                branch_no_9:;
            }
            branch_end_7:;
        }
        branch_end_6:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 88;
        type_description_1 = "oooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_assattr_target_6;
        if ( var_xsize == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "xsize" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = var_xsize;
        tmp_assattr_name_6 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assattr_name_6, 0, tmp_tuple_element_1 );
        if ( var_ysize == NULL )
        {
            Py_DECREF( tmp_assattr_name_6 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ysize" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = var_ysize;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assattr_name_6, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__size, tmp_assattr_name_6 );
        Py_DECREF( tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_list_element_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_source_name_4;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_assattr_target_7;
        tmp_tuple_element_2 = const_str_plain_raw;
        tmp_list_element_1 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_2 );
        tmp_tuple_element_3 = const_int_0;
        tmp_tuple_element_2 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_tuple_element_2, 0, tmp_tuple_element_3 );
        tmp_tuple_element_3 = const_int_0;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_tuple_element_2, 1, tmp_tuple_element_3 );
        if ( var_xsize == NULL )
        {
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_2 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "xsize" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_3 = var_xsize;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_tuple_element_2, 2, tmp_tuple_element_3 );
        if ( var_ysize == NULL )
        {
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_2 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ysize" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_3 = var_ysize;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_tuple_element_2, 3, tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_2 );
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_called_instance_7 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_fp );
        if ( tmp_called_instance_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 122;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame.f_lineno = 122;
        tmp_tuple_element_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_tell );
        Py_DECREF( tmp_called_instance_7 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 122;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 2, tmp_tuple_element_2 );
        if ( var_rawmode == NULL )
        {
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "rawmode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_4 = var_rawmode;
        tmp_tuple_element_2 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_tuple_element_2, 0, tmp_tuple_element_4 );
        tmp_tuple_element_4 = const_int_0;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_tuple_element_2, 1, tmp_tuple_element_4 );
        tmp_tuple_element_4 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_tuple_element_2, 2, tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_list_element_1, 3, tmp_tuple_element_2 );
        tmp_assattr_name_7 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_assattr_name_7, 0, tmp_list_element_1 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_tile, tmp_assattr_name_7 );
        Py_DECREF( tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ba6508e69fd0ebf2cd800e36304af34 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ba6508e69fd0ebf2cd800e36304af34 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8ba6508e69fd0ebf2cd800e36304af34, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8ba6508e69fd0ebf2cd800e36304af34->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8ba6508e69fd0ebf2cd800e36304af34, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8ba6508e69fd0ebf2cd800e36304af34,
        type_description_1,
        par_self,
        var_s,
        var_magic_number,
        var_mode,
        var_rawmode,
        var_ix,
        var_xsize,
        var_ysize
    );


    // Release cached frame.
    if ( frame_8ba6508e69fd0ebf2cd800e36304af34 == cache_frame_8ba6508e69fd0ebf2cd800e36304af34 )
    {
        Py_DECREF( frame_8ba6508e69fd0ebf2cd800e36304af34 );
    }
    cache_frame_8ba6508e69fd0ebf2cd800e36304af34 = NULL;

    assertFrameObject( frame_8ba6508e69fd0ebf2cd800e36304af34 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin$$$function_3__open );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    CHECK_OBJECT( (PyObject *)var_magic_number );
    Py_DECREF( var_magic_number );
    var_magic_number = NULL;

    CHECK_OBJECT( (PyObject *)var_mode );
    Py_DECREF( var_mode );
    var_mode = NULL;

    Py_XDECREF( var_rawmode );
    var_rawmode = NULL;

    Py_XDECREF( var_ix );
    var_ix = NULL;

    Py_XDECREF( var_xsize );
    var_xsize = NULL;

    Py_XDECREF( var_ysize );
    var_ysize = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_magic_number );
    var_magic_number = NULL;

    Py_XDECREF( var_mode );
    var_mode = NULL;

    Py_XDECREF( var_rawmode );
    var_rawmode = NULL;

    Py_XDECREF( var_ix );
    var_ix = NULL;

    Py_XDECREF( var_xsize );
    var_xsize = NULL;

    Py_XDECREF( var_ysize );
    var_ysize = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin$$$function_3__open );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$PpmImagePlugin$$$function_4__save( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_im = python_pars[ 0 ];
    PyObject *par_fp = python_pars[ 1 ];
    PyObject *par_filename = python_pars[ 2 ];
    PyObject *var_rawmode = NULL;
    PyObject *var_head = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_tuple_unpack_4__element_1 = NULL;
    PyObject *tmp_tuple_unpack_4__element_2 = NULL;
    PyObject *tmp_tuple_unpack_4__source_iter = NULL;
    PyObject *tmp_tuple_unpack_5__element_1 = NULL;
    PyObject *tmp_tuple_unpack_5__element_2 = NULL;
    PyObject *tmp_tuple_unpack_5__source_iter = NULL;
    PyObject *tmp_tuple_unpack_6__element_1 = NULL;
    PyObject *tmp_tuple_unpack_6__element_2 = NULL;
    PyObject *tmp_tuple_unpack_6__source_iter = NULL;
    struct Nuitka_FrameObject *frame_c9f23e91cc092cf6ae6adb2d5ee8a434;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    static struct Nuitka_FrameObject *cache_frame_c9f23e91cc092cf6ae6adb2d5ee8a434 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c9f23e91cc092cf6ae6adb2d5ee8a434, codeobj_c9f23e91cc092cf6ae6adb2d5ee8a434, module_PIL$PpmImagePlugin, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c9f23e91cc092cf6ae6adb2d5ee8a434 = cache_frame_c9f23e91cc092cf6ae6adb2d5ee8a434;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c9f23e91cc092cf6ae6adb2d5ee8a434 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c9f23e91cc092cf6ae6adb2d5ee8a434 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_im );
        tmp_source_name_1 = par_im;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_mode );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            tmp_iter_arg_1 = const_tuple_dbf41cef26db995a4f33d593a6e8a77b_tuple;
            tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
            assert( !(tmp_assign_source_1 == NULL) );
            assert( tmp_tuple_unpack_1__source_iter == NULL );
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_unpack_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
            if ( tmp_assign_source_2 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooo";
                exception_lineno = 131;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_1 == NULL );
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_unpack_2;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
            if ( tmp_assign_source_3 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooo";
                exception_lineno = 131;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_2 == NULL );
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        {
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
            tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
            assert( var_rawmode == NULL );
            Py_INCREF( tmp_assign_source_4 );
            var_rawmode = tmp_assign_source_4;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
            tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
            assert( var_head == NULL );
            Py_INCREF( tmp_assign_source_5 );
            var_head = tmp_assign_source_5;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_im );
            tmp_source_name_2 = par_im;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_mode );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 132;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_str_plain_L;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 132;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_iter_arg_2;
                tmp_iter_arg_2 = const_tuple_str_plain_L_bytes_digest_16ced812a77049cdfddb5a13d721e7b2_tuple;
                tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
                assert( !(tmp_assign_source_6 == NULL) );
                assert( tmp_tuple_unpack_2__source_iter == NULL );
                tmp_tuple_unpack_2__source_iter = tmp_assign_source_6;
            }
            // Tried code:
            // Tried code:
            {
                PyObject *tmp_assign_source_7;
                PyObject *tmp_unpack_3;
                CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
                tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
                if ( tmp_assign_source_7 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooo";
                    exception_lineno = 133;
                    goto try_except_handler_5;
                }
                assert( tmp_tuple_unpack_2__element_1 == NULL );
                tmp_tuple_unpack_2__element_1 = tmp_assign_source_7;
            }
            {
                PyObject *tmp_assign_source_8;
                PyObject *tmp_unpack_4;
                CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
                tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
                if ( tmp_assign_source_8 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooo";
                    exception_lineno = 133;
                    goto try_except_handler_5;
                }
                assert( tmp_tuple_unpack_2__element_2 == NULL );
                tmp_tuple_unpack_2__element_2 = tmp_assign_source_8;
            }
            goto try_end_3;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
            Py_DECREF( tmp_tuple_unpack_2__source_iter );
            tmp_tuple_unpack_2__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto try_except_handler_4;
            // End of try:
            try_end_3:;
            goto try_end_4;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_tuple_unpack_2__element_1 );
            tmp_tuple_unpack_2__element_1 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_4;
            exception_value = exception_keeper_value_4;
            exception_tb = exception_keeper_tb_4;
            exception_lineno = exception_keeper_lineno_4;

            goto frame_exception_exit_1;
            // End of try:
            try_end_4:;
            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
            Py_DECREF( tmp_tuple_unpack_2__source_iter );
            tmp_tuple_unpack_2__source_iter = NULL;

            {
                PyObject *tmp_assign_source_9;
                CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
                tmp_assign_source_9 = tmp_tuple_unpack_2__element_1;
                assert( var_rawmode == NULL );
                Py_INCREF( tmp_assign_source_9 );
                var_rawmode = tmp_assign_source_9;
            }
            Py_XDECREF( tmp_tuple_unpack_2__element_1 );
            tmp_tuple_unpack_2__element_1 = NULL;

            {
                PyObject *tmp_assign_source_10;
                CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
                tmp_assign_source_10 = tmp_tuple_unpack_2__element_2;
                assert( var_head == NULL );
                Py_INCREF( tmp_assign_source_10 );
                var_head = tmp_assign_source_10;
            }
            Py_XDECREF( tmp_tuple_unpack_2__element_2 );
            tmp_tuple_unpack_2__element_2 = NULL;

            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                PyObject *tmp_source_name_3;
                CHECK_OBJECT( par_im );
                tmp_source_name_3 = par_im;
                tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_mode );
                if ( tmp_compexpr_left_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 134;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_3 = const_str_plain_I;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                Py_DECREF( tmp_compexpr_left_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 134;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_compexpr_left_4;
                    PyObject *tmp_compexpr_right_4;
                    PyObject *tmp_subscribed_name_1;
                    PyObject *tmp_called_instance_1;
                    PyObject *tmp_subscript_name_1;
                    CHECK_OBJECT( par_im );
                    tmp_called_instance_1 = par_im;
                    frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame.f_lineno = 135;
                    tmp_subscribed_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_getextrema );
                    if ( tmp_subscribed_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 135;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_subscript_name_1 = const_int_pos_1;
                    tmp_compexpr_left_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
                    Py_DECREF( tmp_subscribed_name_1 );
                    if ( tmp_compexpr_left_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 135;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_4 = const_int_pos_65536;
                    tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                    Py_DECREF( tmp_compexpr_left_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 135;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    {
                        PyObject *tmp_assign_source_11;
                        PyObject *tmp_iter_arg_3;
                        tmp_iter_arg_3 = const_tuple_0cdcf938fa0a75fd02b2b45c25cdea09_tuple;
                        tmp_assign_source_11 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
                        assert( !(tmp_assign_source_11 == NULL) );
                        assert( tmp_tuple_unpack_3__source_iter == NULL );
                        tmp_tuple_unpack_3__source_iter = tmp_assign_source_11;
                    }
                    // Tried code:
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_12;
                        PyObject *tmp_unpack_5;
                        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                        tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
                        tmp_assign_source_12 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
                        if ( tmp_assign_source_12 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooo";
                            exception_lineno = 136;
                            goto try_except_handler_7;
                        }
                        assert( tmp_tuple_unpack_3__element_1 == NULL );
                        tmp_tuple_unpack_3__element_1 = tmp_assign_source_12;
                    }
                    {
                        PyObject *tmp_assign_source_13;
                        PyObject *tmp_unpack_6;
                        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                        tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
                        tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
                        if ( tmp_assign_source_13 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooo";
                            exception_lineno = 136;
                            goto try_except_handler_7;
                        }
                        assert( tmp_tuple_unpack_3__element_2 == NULL );
                        tmp_tuple_unpack_3__element_2 = tmp_assign_source_13;
                    }
                    goto try_end_5;
                    // Exception handler code:
                    try_except_handler_7:;
                    exception_keeper_type_5 = exception_type;
                    exception_keeper_value_5 = exception_value;
                    exception_keeper_tb_5 = exception_tb;
                    exception_keeper_lineno_5 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
                    Py_DECREF( tmp_tuple_unpack_3__source_iter );
                    tmp_tuple_unpack_3__source_iter = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_5;
                    exception_value = exception_keeper_value_5;
                    exception_tb = exception_keeper_tb_5;
                    exception_lineno = exception_keeper_lineno_5;

                    goto try_except_handler_6;
                    // End of try:
                    try_end_5:;
                    goto try_end_6;
                    // Exception handler code:
                    try_except_handler_6:;
                    exception_keeper_type_6 = exception_type;
                    exception_keeper_value_6 = exception_value;
                    exception_keeper_tb_6 = exception_tb;
                    exception_keeper_lineno_6 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
                    tmp_tuple_unpack_3__element_1 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_6;
                    exception_value = exception_keeper_value_6;
                    exception_tb = exception_keeper_tb_6;
                    exception_lineno = exception_keeper_lineno_6;

                    goto frame_exception_exit_1;
                    // End of try:
                    try_end_6:;
                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
                    Py_DECREF( tmp_tuple_unpack_3__source_iter );
                    tmp_tuple_unpack_3__source_iter = NULL;

                    {
                        PyObject *tmp_assign_source_14;
                        CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
                        tmp_assign_source_14 = tmp_tuple_unpack_3__element_1;
                        assert( var_rawmode == NULL );
                        Py_INCREF( tmp_assign_source_14 );
                        var_rawmode = tmp_assign_source_14;
                    }
                    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
                    tmp_tuple_unpack_3__element_1 = NULL;

                    {
                        PyObject *tmp_assign_source_15;
                        CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
                        tmp_assign_source_15 = tmp_tuple_unpack_3__element_2;
                        assert( var_head == NULL );
                        Py_INCREF( tmp_assign_source_15 );
                        var_head = tmp_assign_source_15;
                    }
                    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
                    tmp_tuple_unpack_3__element_2 = NULL;

                    goto branch_end_4;
                    branch_no_4:;
                    {
                        PyObject *tmp_assign_source_16;
                        PyObject *tmp_iter_arg_4;
                        tmp_iter_arg_4 = const_tuple_91a874ce28495e8181687ffac7d90272_tuple;
                        tmp_assign_source_16 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
                        assert( !(tmp_assign_source_16 == NULL) );
                        assert( tmp_tuple_unpack_4__source_iter == NULL );
                        tmp_tuple_unpack_4__source_iter = tmp_assign_source_16;
                    }
                    // Tried code:
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_17;
                        PyObject *tmp_unpack_7;
                        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
                        tmp_unpack_7 = tmp_tuple_unpack_4__source_iter;
                        tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_7, 0, 2 );
                        if ( tmp_assign_source_17 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooo";
                            exception_lineno = 138;
                            goto try_except_handler_9;
                        }
                        assert( tmp_tuple_unpack_4__element_1 == NULL );
                        tmp_tuple_unpack_4__element_1 = tmp_assign_source_17;
                    }
                    {
                        PyObject *tmp_assign_source_18;
                        PyObject *tmp_unpack_8;
                        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
                        tmp_unpack_8 = tmp_tuple_unpack_4__source_iter;
                        tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_8, 1, 2 );
                        if ( tmp_assign_source_18 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooo";
                            exception_lineno = 138;
                            goto try_except_handler_9;
                        }
                        assert( tmp_tuple_unpack_4__element_2 == NULL );
                        tmp_tuple_unpack_4__element_2 = tmp_assign_source_18;
                    }
                    goto try_end_7;
                    // Exception handler code:
                    try_except_handler_9:;
                    exception_keeper_type_7 = exception_type;
                    exception_keeper_value_7 = exception_value;
                    exception_keeper_tb_7 = exception_tb;
                    exception_keeper_lineno_7 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
                    Py_DECREF( tmp_tuple_unpack_4__source_iter );
                    tmp_tuple_unpack_4__source_iter = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_7;
                    exception_value = exception_keeper_value_7;
                    exception_tb = exception_keeper_tb_7;
                    exception_lineno = exception_keeper_lineno_7;

                    goto try_except_handler_8;
                    // End of try:
                    try_end_7:;
                    goto try_end_8;
                    // Exception handler code:
                    try_except_handler_8:;
                    exception_keeper_type_8 = exception_type;
                    exception_keeper_value_8 = exception_value;
                    exception_keeper_tb_8 = exception_tb;
                    exception_keeper_lineno_8 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_tuple_unpack_4__element_1 );
                    tmp_tuple_unpack_4__element_1 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_8;
                    exception_value = exception_keeper_value_8;
                    exception_tb = exception_keeper_tb_8;
                    exception_lineno = exception_keeper_lineno_8;

                    goto frame_exception_exit_1;
                    // End of try:
                    try_end_8:;
                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
                    Py_DECREF( tmp_tuple_unpack_4__source_iter );
                    tmp_tuple_unpack_4__source_iter = NULL;

                    {
                        PyObject *tmp_assign_source_19;
                        CHECK_OBJECT( tmp_tuple_unpack_4__element_1 );
                        tmp_assign_source_19 = tmp_tuple_unpack_4__element_1;
                        assert( var_rawmode == NULL );
                        Py_INCREF( tmp_assign_source_19 );
                        var_rawmode = tmp_assign_source_19;
                    }
                    Py_XDECREF( tmp_tuple_unpack_4__element_1 );
                    tmp_tuple_unpack_4__element_1 = NULL;

                    {
                        PyObject *tmp_assign_source_20;
                        CHECK_OBJECT( tmp_tuple_unpack_4__element_2 );
                        tmp_assign_source_20 = tmp_tuple_unpack_4__element_2;
                        assert( var_head == NULL );
                        Py_INCREF( tmp_assign_source_20 );
                        var_head = tmp_assign_source_20;
                    }
                    Py_XDECREF( tmp_tuple_unpack_4__element_2 );
                    tmp_tuple_unpack_4__element_2 = NULL;

                    branch_end_4:;
                }
                goto branch_end_3;
                branch_no_3:;
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    PyObject *tmp_source_name_4;
                    CHECK_OBJECT( par_im );
                    tmp_source_name_4 = par_im;
                    tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_mode );
                    if ( tmp_compexpr_left_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 139;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_5 = const_str_plain_RGB;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                    Py_DECREF( tmp_compexpr_left_5 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 139;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_assign_source_21;
                        PyObject *tmp_iter_arg_5;
                        tmp_iter_arg_5 = const_tuple_str_plain_RGB_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b_tuple;
                        tmp_assign_source_21 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_5 );
                        assert( !(tmp_assign_source_21 == NULL) );
                        assert( tmp_tuple_unpack_5__source_iter == NULL );
                        tmp_tuple_unpack_5__source_iter = tmp_assign_source_21;
                    }
                    // Tried code:
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_22;
                        PyObject *tmp_unpack_9;
                        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
                        tmp_unpack_9 = tmp_tuple_unpack_5__source_iter;
                        tmp_assign_source_22 = UNPACK_NEXT( tmp_unpack_9, 0, 2 );
                        if ( tmp_assign_source_22 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooo";
                            exception_lineno = 140;
                            goto try_except_handler_11;
                        }
                        assert( tmp_tuple_unpack_5__element_1 == NULL );
                        tmp_tuple_unpack_5__element_1 = tmp_assign_source_22;
                    }
                    {
                        PyObject *tmp_assign_source_23;
                        PyObject *tmp_unpack_10;
                        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
                        tmp_unpack_10 = tmp_tuple_unpack_5__source_iter;
                        tmp_assign_source_23 = UNPACK_NEXT( tmp_unpack_10, 1, 2 );
                        if ( tmp_assign_source_23 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooo";
                            exception_lineno = 140;
                            goto try_except_handler_11;
                        }
                        assert( tmp_tuple_unpack_5__element_2 == NULL );
                        tmp_tuple_unpack_5__element_2 = tmp_assign_source_23;
                    }
                    goto try_end_9;
                    // Exception handler code:
                    try_except_handler_11:;
                    exception_keeper_type_9 = exception_type;
                    exception_keeper_value_9 = exception_value;
                    exception_keeper_tb_9 = exception_tb;
                    exception_keeper_lineno_9 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
                    Py_DECREF( tmp_tuple_unpack_5__source_iter );
                    tmp_tuple_unpack_5__source_iter = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_9;
                    exception_value = exception_keeper_value_9;
                    exception_tb = exception_keeper_tb_9;
                    exception_lineno = exception_keeper_lineno_9;

                    goto try_except_handler_10;
                    // End of try:
                    try_end_9:;
                    goto try_end_10;
                    // Exception handler code:
                    try_except_handler_10:;
                    exception_keeper_type_10 = exception_type;
                    exception_keeper_value_10 = exception_value;
                    exception_keeper_tb_10 = exception_tb;
                    exception_keeper_lineno_10 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_tuple_unpack_5__element_1 );
                    tmp_tuple_unpack_5__element_1 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_10;
                    exception_value = exception_keeper_value_10;
                    exception_tb = exception_keeper_tb_10;
                    exception_lineno = exception_keeper_lineno_10;

                    goto frame_exception_exit_1;
                    // End of try:
                    try_end_10:;
                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
                    Py_DECREF( tmp_tuple_unpack_5__source_iter );
                    tmp_tuple_unpack_5__source_iter = NULL;

                    {
                        PyObject *tmp_assign_source_24;
                        CHECK_OBJECT( tmp_tuple_unpack_5__element_1 );
                        tmp_assign_source_24 = tmp_tuple_unpack_5__element_1;
                        assert( var_rawmode == NULL );
                        Py_INCREF( tmp_assign_source_24 );
                        var_rawmode = tmp_assign_source_24;
                    }
                    Py_XDECREF( tmp_tuple_unpack_5__element_1 );
                    tmp_tuple_unpack_5__element_1 = NULL;

                    {
                        PyObject *tmp_assign_source_25;
                        CHECK_OBJECT( tmp_tuple_unpack_5__element_2 );
                        tmp_assign_source_25 = tmp_tuple_unpack_5__element_2;
                        assert( var_head == NULL );
                        Py_INCREF( tmp_assign_source_25 );
                        var_head = tmp_assign_source_25;
                    }
                    Py_XDECREF( tmp_tuple_unpack_5__element_2 );
                    tmp_tuple_unpack_5__element_2 = NULL;

                    goto branch_end_5;
                    branch_no_5:;
                    {
                        nuitka_bool tmp_condition_result_6;
                        PyObject *tmp_compexpr_left_6;
                        PyObject *tmp_compexpr_right_6;
                        PyObject *tmp_source_name_5;
                        CHECK_OBJECT( par_im );
                        tmp_source_name_5 = par_im;
                        tmp_compexpr_left_6 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_mode );
                        if ( tmp_compexpr_left_6 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 141;
                            type_description_1 = "ooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_compexpr_right_6 = const_str_plain_RGBA;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                        Py_DECREF( tmp_compexpr_left_6 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 141;
                            type_description_1 = "ooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_6;
                        }
                        else
                        {
                            goto branch_no_6;
                        }
                        branch_yes_6:;
                        {
                            PyObject *tmp_assign_source_26;
                            PyObject *tmp_iter_arg_6;
                            tmp_iter_arg_6 = const_tuple_str_plain_RGB_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b_tuple;
                            tmp_assign_source_26 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_6 );
                            assert( !(tmp_assign_source_26 == NULL) );
                            assert( tmp_tuple_unpack_6__source_iter == NULL );
                            tmp_tuple_unpack_6__source_iter = tmp_assign_source_26;
                        }
                        // Tried code:
                        // Tried code:
                        {
                            PyObject *tmp_assign_source_27;
                            PyObject *tmp_unpack_11;
                            CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
                            tmp_unpack_11 = tmp_tuple_unpack_6__source_iter;
                            tmp_assign_source_27 = UNPACK_NEXT( tmp_unpack_11, 0, 2 );
                            if ( tmp_assign_source_27 == NULL )
                            {
                                if ( !ERROR_OCCURRED() )
                                {
                                    exception_type = PyExc_StopIteration;
                                    Py_INCREF( exception_type );
                                    exception_value = NULL;
                                    exception_tb = NULL;
                                }
                                else
                                {
                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                }


                                type_description_1 = "ooooo";
                                exception_lineno = 142;
                                goto try_except_handler_13;
                            }
                            assert( tmp_tuple_unpack_6__element_1 == NULL );
                            tmp_tuple_unpack_6__element_1 = tmp_assign_source_27;
                        }
                        {
                            PyObject *tmp_assign_source_28;
                            PyObject *tmp_unpack_12;
                            CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
                            tmp_unpack_12 = tmp_tuple_unpack_6__source_iter;
                            tmp_assign_source_28 = UNPACK_NEXT( tmp_unpack_12, 1, 2 );
                            if ( tmp_assign_source_28 == NULL )
                            {
                                if ( !ERROR_OCCURRED() )
                                {
                                    exception_type = PyExc_StopIteration;
                                    Py_INCREF( exception_type );
                                    exception_value = NULL;
                                    exception_tb = NULL;
                                }
                                else
                                {
                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                }


                                type_description_1 = "ooooo";
                                exception_lineno = 142;
                                goto try_except_handler_13;
                            }
                            assert( tmp_tuple_unpack_6__element_2 == NULL );
                            tmp_tuple_unpack_6__element_2 = tmp_assign_source_28;
                        }
                        goto try_end_11;
                        // Exception handler code:
                        try_except_handler_13:;
                        exception_keeper_type_11 = exception_type;
                        exception_keeper_value_11 = exception_value;
                        exception_keeper_tb_11 = exception_tb;
                        exception_keeper_lineno_11 = exception_lineno;
                        exception_type = NULL;
                        exception_value = NULL;
                        exception_tb = NULL;
                        exception_lineno = 0;

                        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_6__source_iter );
                        Py_DECREF( tmp_tuple_unpack_6__source_iter );
                        tmp_tuple_unpack_6__source_iter = NULL;

                        // Re-raise.
                        exception_type = exception_keeper_type_11;
                        exception_value = exception_keeper_value_11;
                        exception_tb = exception_keeper_tb_11;
                        exception_lineno = exception_keeper_lineno_11;

                        goto try_except_handler_12;
                        // End of try:
                        try_end_11:;
                        goto try_end_12;
                        // Exception handler code:
                        try_except_handler_12:;
                        exception_keeper_type_12 = exception_type;
                        exception_keeper_value_12 = exception_value;
                        exception_keeper_tb_12 = exception_tb;
                        exception_keeper_lineno_12 = exception_lineno;
                        exception_type = NULL;
                        exception_value = NULL;
                        exception_tb = NULL;
                        exception_lineno = 0;

                        Py_XDECREF( tmp_tuple_unpack_6__element_1 );
                        tmp_tuple_unpack_6__element_1 = NULL;

                        // Re-raise.
                        exception_type = exception_keeper_type_12;
                        exception_value = exception_keeper_value_12;
                        exception_tb = exception_keeper_tb_12;
                        exception_lineno = exception_keeper_lineno_12;

                        goto frame_exception_exit_1;
                        // End of try:
                        try_end_12:;
                        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_6__source_iter );
                        Py_DECREF( tmp_tuple_unpack_6__source_iter );
                        tmp_tuple_unpack_6__source_iter = NULL;

                        {
                            PyObject *tmp_assign_source_29;
                            CHECK_OBJECT( tmp_tuple_unpack_6__element_1 );
                            tmp_assign_source_29 = tmp_tuple_unpack_6__element_1;
                            assert( var_rawmode == NULL );
                            Py_INCREF( tmp_assign_source_29 );
                            var_rawmode = tmp_assign_source_29;
                        }
                        Py_XDECREF( tmp_tuple_unpack_6__element_1 );
                        tmp_tuple_unpack_6__element_1 = NULL;

                        {
                            PyObject *tmp_assign_source_30;
                            CHECK_OBJECT( tmp_tuple_unpack_6__element_2 );
                            tmp_assign_source_30 = tmp_tuple_unpack_6__element_2;
                            assert( var_head == NULL );
                            Py_INCREF( tmp_assign_source_30 );
                            var_head = tmp_assign_source_30;
                        }
                        Py_XDECREF( tmp_tuple_unpack_6__element_2 );
                        tmp_tuple_unpack_6__element_2 = NULL;

                        goto branch_end_6;
                        branch_no_6:;
                        {
                            PyObject *tmp_raise_type_1;
                            PyObject *tmp_make_exception_arg_1;
                            PyObject *tmp_left_name_1;
                            PyObject *tmp_right_name_1;
                            PyObject *tmp_source_name_6;
                            tmp_left_name_1 = const_str_digest_32d3907b730c15f18611da338f7983f1;
                            CHECK_OBJECT( par_im );
                            tmp_source_name_6 = par_im;
                            tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_mode );
                            if ( tmp_right_name_1 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 144;
                                type_description_1 = "ooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                            Py_DECREF( tmp_right_name_1 );
                            if ( tmp_make_exception_arg_1 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 144;
                                type_description_1 = "ooooo";
                                goto frame_exception_exit_1;
                            }
                            frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame.f_lineno = 144;
                            {
                                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
                            }

                            Py_DECREF( tmp_make_exception_arg_1 );
                            assert( !(tmp_raise_type_1 == NULL) );
                            exception_type = tmp_raise_type_1;
                            exception_lineno = 144;
                            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "ooooo";
                            goto frame_exception_exit_1;
                        }
                        branch_end_6:;
                    }
                    branch_end_5:;
                }
                branch_end_3:;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_7;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_source_name_8;
        CHECK_OBJECT( par_fp );
        tmp_source_name_7 = par_fp;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_write );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        if ( var_head == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "head" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 145;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_2 = var_head;
        tmp_left_name_3 = const_str_digest_e6a706ec6ea5349b76d787641b38682c;
        CHECK_OBJECT( par_im );
        tmp_source_name_8 = par_im;
        tmp_right_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_size );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 145;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 145;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame.f_lineno = 145;
        tmp_right_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_plain_ascii_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 145;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 145;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame.f_lineno = 145;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        if ( var_head == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "head" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_7 = var_head;
        tmp_compexpr_right_7 = const_bytes_digest_7cc1e60162d1d07cfdc4c8b78086a00b;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( par_fp );
            tmp_called_instance_3 = par_fp;
            frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame.f_lineno = 147;
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_write, &PyTuple_GET_ITEM( const_tuple_bytes_digest_fbe8492d80df0fb1bb573875f5249835_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_7:;
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_compexpr_left_8;
        PyObject *tmp_compexpr_right_8;
        if ( var_head == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "head" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_8 = var_head;
        tmp_compexpr_right_8 = const_bytes_digest_16ced812a77049cdfddb5a13d721e7b2;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_9;
            PyObject *tmp_compexpr_right_9;
            if ( var_rawmode == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "rawmode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 149;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_left_9 = var_rawmode;
            tmp_compexpr_right_9 = const_str_plain_L;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 149;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                PyObject *tmp_called_instance_4;
                PyObject *tmp_call_result_3;
                CHECK_OBJECT( par_fp );
                tmp_called_instance_4 = par_fp;
                frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame.f_lineno = 150;
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_write, &PyTuple_GET_ITEM( const_tuple_bytes_digest_fbe8492d80df0fb1bb573875f5249835_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 150;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            goto branch_end_9;
            branch_no_9:;
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_compexpr_left_10;
                PyObject *tmp_compexpr_right_10;
                if ( var_rawmode == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "rawmode" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 151;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }

                tmp_compexpr_left_10 = var_rawmode;
                tmp_compexpr_right_10 = const_str_digest_772c0402ef22c7a4d610f4443976d74a;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 151;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_10;
                }
                else
                {
                    goto branch_no_10;
                }
                branch_yes_10:;
                {
                    PyObject *tmp_called_instance_5;
                    PyObject *tmp_call_result_4;
                    CHECK_OBJECT( par_fp );
                    tmp_called_instance_5 = par_fp;
                    frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame.f_lineno = 152;
                    tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_write, &PyTuple_GET_ITEM( const_tuple_bytes_digest_068100f800f166e1a0984415b535ee78_tuple, 0 ) );

                    if ( tmp_call_result_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 152;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_4 );
                }
                goto branch_end_10;
                branch_no_10:;
                {
                    nuitka_bool tmp_condition_result_11;
                    PyObject *tmp_compexpr_left_11;
                    PyObject *tmp_compexpr_right_11;
                    if ( var_rawmode == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "rawmode" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 153;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_compexpr_left_11 = var_rawmode;
                    tmp_compexpr_right_11 = const_str_digest_a847edfea52ac2580b31c3747424369d;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 153;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_11;
                    }
                    else
                    {
                        goto branch_no_11;
                    }
                    branch_yes_11:;
                    {
                        PyObject *tmp_called_instance_6;
                        PyObject *tmp_call_result_5;
                        CHECK_OBJECT( par_fp );
                        tmp_called_instance_6 = par_fp;
                        frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame.f_lineno = 154;
                        tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_write, &PyTuple_GET_ITEM( const_tuple_bytes_digest_00e6c0c3954f108ce216ad15549a73be_tuple, 0 ) );

                        if ( tmp_call_result_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 154;
                            type_description_1 = "ooooo";
                            goto frame_exception_exit_1;
                        }
                        Py_DECREF( tmp_call_result_5 );
                    }
                    branch_no_11:;
                }
                branch_end_10:;
            }
            branch_end_9:;
        }
        branch_no_8:;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_list_element_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_4;
        PyObject *tmp_source_name_10;
        PyObject *tmp_tuple_element_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_ImageFile );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ImageFile );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_1;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain__save );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_im );
        tmp_args_element_name_2 = par_im;
        CHECK_OBJECT( par_fp );
        tmp_args_element_name_3 = par_fp;
        tmp_tuple_element_1 = const_str_plain_raw;
        tmp_list_element_1 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_1 );
        tmp_left_name_4 = const_tuple_int_0_int_0_tuple;
        CHECK_OBJECT( par_im );
        tmp_source_name_10 = par_im;
        tmp_right_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_size );
        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 155;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = BINARY_OPERATION_ADD_TUPLE_OBJECT( tmp_left_name_4, tmp_right_name_4 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 155;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_int_0;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_list_element_1, 2, tmp_tuple_element_1 );
        if ( var_rawmode == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "rawmode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_2 = var_rawmode;
        tmp_tuple_element_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_tuple_element_1, 0, tmp_tuple_element_2 );
        tmp_tuple_element_2 = const_int_0;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_tuple_element_1, 1, tmp_tuple_element_2 );
        tmp_tuple_element_2 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_tuple_element_1, 2, tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_list_element_1, 3, tmp_tuple_element_1 );
        tmp_args_element_name_4 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_args_element_name_4, 0, tmp_list_element_1 );
        frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame.f_lineno = 155;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c9f23e91cc092cf6ae6adb2d5ee8a434 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c9f23e91cc092cf6ae6adb2d5ee8a434 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c9f23e91cc092cf6ae6adb2d5ee8a434, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c9f23e91cc092cf6ae6adb2d5ee8a434->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c9f23e91cc092cf6ae6adb2d5ee8a434, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c9f23e91cc092cf6ae6adb2d5ee8a434,
        type_description_1,
        par_im,
        par_fp,
        par_filename,
        var_rawmode,
        var_head
    );


    // Release cached frame.
    if ( frame_c9f23e91cc092cf6ae6adb2d5ee8a434 == cache_frame_c9f23e91cc092cf6ae6adb2d5ee8a434 )
    {
        Py_DECREF( frame_c9f23e91cc092cf6ae6adb2d5ee8a434 );
    }
    cache_frame_c9f23e91cc092cf6ae6adb2d5ee8a434 = NULL;

    assertFrameObject( frame_c9f23e91cc092cf6ae6adb2d5ee8a434 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin$$$function_4__save );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    CHECK_OBJECT( (PyObject *)par_fp );
    Py_DECREF( par_fp );
    par_fp = NULL;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    Py_XDECREF( var_rawmode );
    var_rawmode = NULL;

    Py_XDECREF( var_head );
    var_head = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    CHECK_OBJECT( (PyObject *)par_fp );
    Py_DECREF( par_fp );
    par_fp = NULL;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    Py_XDECREF( var_rawmode );
    var_rawmode = NULL;

    Py_XDECREF( var_head );
    var_head = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin$$$function_4__save );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_1__accept(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$PpmImagePlugin$$$function_1__accept,
        const_str_plain__accept,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c808db28b95858ee432b290bb51ec2c5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$PpmImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_2__token( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$PpmImagePlugin$$$function_2__token,
        const_str_plain__token,
#if PYTHON_VERSION >= 300
        const_str_digest_b24527cbddb5a6705a734e7ff3590a1a,
#endif
        codeobj_90282576f9587754e22a5abdb328d373,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$PpmImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_3__open(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$PpmImagePlugin$$$function_3__open,
        const_str_plain__open,
#if PYTHON_VERSION >= 300
        const_str_digest_9e7af868e4dac328abfc04f72aa7a337,
#endif
        codeobj_8ba6508e69fd0ebf2cd800e36304af34,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$PpmImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_4__save(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$PpmImagePlugin$$$function_4__save,
        const_str_plain__save,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c9f23e91cc092cf6ae6adb2d5ee8a434,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$PpmImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_PIL$PpmImagePlugin =
{
    PyModuleDef_HEAD_INIT,
    "PIL.PpmImagePlugin",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(PIL$PpmImagePlugin)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(PIL$PpmImagePlugin)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_PIL$PpmImagePlugin );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("PIL.PpmImagePlugin: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.PpmImagePlugin: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.PpmImagePlugin: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initPIL$PpmImagePlugin" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_PIL$PpmImagePlugin = Py_InitModule4(
        "PIL.PpmImagePlugin",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_PIL$PpmImagePlugin = PyModule_Create( &mdef_PIL$PpmImagePlugin );
#endif

    moduledict_PIL$PpmImagePlugin = MODULE_DICT( module_PIL$PpmImagePlugin );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_PIL$PpmImagePlugin,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_PIL$PpmImagePlugin,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$PpmImagePlugin,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$PpmImagePlugin,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_PIL$PpmImagePlugin );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_ad0990c1df7a84639097776f6273455b, module_PIL$PpmImagePlugin );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_54a179603fbff795e0c7198497ba6a46;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_PIL$PpmImagePlugin_50 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_622c0ba4b492b66a6921975cf4ca8a0d_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_622c0ba4b492b66a6921975cf4ca8a0d_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_54a179603fbff795e0c7198497ba6a46 = MAKE_MODULE_FRAME( codeobj_54a179603fbff795e0c7198497ba6a46, module_PIL$PpmImagePlugin );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_54a179603fbff795e0c7198497ba6a46 );
    assert( Py_REFCNT( frame_54a179603fbff795e0c7198497ba6a46 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_empty;
        tmp_globals_name_1 = (PyObject *)moduledict_PIL$PpmImagePlugin;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Image_str_plain_ImageFile_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_54a179603fbff795e0c7198497ba6a46->m_frame.f_lineno = 18;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_5 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_PIL$PpmImagePlugin,
                const_str_plain_Image,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Image );
        }

        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_Image, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_6 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_PIL$PpmImagePlugin,
                const_str_plain_ImageFile,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_ImageFile );
        }

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_ImageFile, tmp_assign_source_6 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = const_str_digest_679d6818068454a9e371d2b3ba6d34cc;
        UPDATE_STRING_DICT0( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = const_bytes_digest_4d2dc43101e5d0fc30c8b1bfb65ec16c;
        UPDATE_STRING_DICT0( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_b_whitespace, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = PyDict_Copy( const_dict_27ba4113e9eac4d66d1ace3452229792 );
        UPDATE_STRING_DICT1( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_MODES, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_1__accept(  );



        UPDATE_STRING_DICT1( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain__accept, tmp_assign_source_10 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_ImageFile );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ImageFile );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 50;

            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ImageFile );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        tmp_assign_source_11 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_11, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_11;
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_12 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_14 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_14;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_2 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_2, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_3 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;

                goto try_except_handler_2;
            }
            tmp_tuple_element_2 = const_str_plain_PpmImageFile;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_54a179603fbff795e0c7198497ba6a46->m_frame.f_lineno = 50;
            tmp_assign_source_15 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;

                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_15;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_4 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_4, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;

                goto try_except_handler_2;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_5;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 50;

                    goto try_except_handler_2;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_5 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_5 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 50;

                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 50;

                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 50;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_2;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_16;
            tmp_assign_source_16 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_16;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_17;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_PIL$PpmImagePlugin_50 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_ad0990c1df7a84639097776f6273455b;
        tmp_res = PyObject_SetItem( locals_PIL$PpmImagePlugin_50, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_plain_PpmImageFile;
        tmp_res = PyObject_SetItem( locals_PIL$PpmImagePlugin_50, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_622c0ba4b492b66a6921975cf4ca8a0d_2, codeobj_622c0ba4b492b66a6921975cf4ca8a0d, module_PIL$PpmImagePlugin, sizeof(void *) );
        frame_622c0ba4b492b66a6921975cf4ca8a0d_2 = cache_frame_622c0ba4b492b66a6921975cf4ca8a0d_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_622c0ba4b492b66a6921975cf4ca8a0d_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_622c0ba4b492b66a6921975cf4ca8a0d_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = const_str_plain_PPM;
        tmp_res = PyObject_SetItem( locals_PIL$PpmImagePlugin_50, const_str_plain_format, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = const_str_digest_3c05dd9c36a31a84904e36ab0e4c89f7;
        tmp_res = PyObject_SetItem( locals_PIL$PpmImagePlugin_50, const_str_plain_format_description, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_bytes_empty_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_2__token( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_PIL$PpmImagePlugin_50, const_str_plain__token, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_3__open(  );



        tmp_res = PyObject_SetItem( locals_PIL$PpmImagePlugin_50, const_str_plain__open, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_622c0ba4b492b66a6921975cf4ca8a0d_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_622c0ba4b492b66a6921975cf4ca8a0d_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_622c0ba4b492b66a6921975cf4ca8a0d_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_622c0ba4b492b66a6921975cf4ca8a0d_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_622c0ba4b492b66a6921975cf4ca8a0d_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_622c0ba4b492b66a6921975cf4ca8a0d_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_622c0ba4b492b66a6921975cf4ca8a0d_2 == cache_frame_622c0ba4b492b66a6921975cf4ca8a0d_2 )
        {
            Py_DECREF( frame_622c0ba4b492b66a6921975cf4ca8a0d_2 );
        }
        cache_frame_622c0ba4b492b66a6921975cf4ca8a0d_2 = NULL;

        assertFrameObject( frame_622c0ba4b492b66a6921975cf4ca8a0d_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_4;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;

                goto try_except_handler_4;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_PIL$PpmImagePlugin_50, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;

                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_PpmImageFile;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_PIL$PpmImagePlugin_50;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_54a179603fbff795e0c7198497ba6a46->m_frame.f_lineno = 50;
            tmp_assign_source_18 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;

                goto try_except_handler_4;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_18;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_17 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_17 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_PIL$PpmImagePlugin_50 );
        locals_PIL$PpmImagePlugin_50 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_PIL$PpmImagePlugin_50 );
        locals_PIL$PpmImagePlugin_50 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( PIL$PpmImagePlugin );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 50;
        goto try_except_handler_2;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_PpmImageFile, tmp_assign_source_17 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_PIL$PpmImagePlugin$$$function_4__save(  );



        UPDATE_STRING_DICT1( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain__save, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_4;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_register_open );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PpmImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_5;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_format );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PpmImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_6;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain__accept );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__accept );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_accept" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = tmp_mvar_value_7;
        frame_54a179603fbff795e0c7198497ba6a46->m_frame.f_lineno = 164;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_8;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_register_save );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PpmImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_9;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_format );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain__save );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__save );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_save" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = tmp_mvar_value_10;
        frame_54a179603fbff795e0c7198497ba6a46->m_frame.f_lineno = 165;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_11;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_register_extensions );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PpmImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_12;
        tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_format );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_7 = LIST_COPY( const_list_d08d468687a0ede0ef0ffde271e6d29a_list );
        frame_54a179603fbff795e0c7198497ba6a46->m_frame.f_lineno = 167;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_6 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_source_name_13;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_9;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_13;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_register_mime );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_PIL$PpmImagePlugin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PpmImageFile );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PpmImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }

        tmp_source_name_13 = tmp_mvar_value_14;
        tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_format );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_9 = const_str_digest_a6870d3c367ef4f3568ba7bc65a36f7e;
        frame_54a179603fbff795e0c7198497ba6a46->m_frame.f_lineno = 169;
        {
            PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_54a179603fbff795e0c7198497ba6a46 );
#endif
    popFrameStack();

    assertFrameObject( frame_54a179603fbff795e0c7198497ba6a46 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_54a179603fbff795e0c7198497ba6a46 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_54a179603fbff795e0c7198497ba6a46, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_54a179603fbff795e0c7198497ba6a46->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_54a179603fbff795e0c7198497ba6a46, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_PIL$PpmImagePlugin );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
