/* Generated code for Python module 'nbformat.v2.nbxml'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_nbformat$v2$nbxml" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_nbformat$v2$nbxml;
PyDictObject *moduledict_nbformat$v2$nbxml;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_warn;
extern PyObject *const_str_plain_tag;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_output_text;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_digest_f87cf22b2ff8d81082b89ed72f489fb1;
static PyObject *const_tuple_fc383372e004d29aac698a72e7b64731_tuple;
static PyObject *const_str_digest_84b8e2888e5228ada3b86d4538fa1742;
extern PyObject *const_str_plain_new_text_cell;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_rwbase;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_traceback;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_0;
extern PyObject *const_str_plain_collapsed;
extern PyObject *const_tuple_str_plain_markdown_tuple;
static PyObject *const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple;
extern PyObject *const_str_plain_json;
static PyObject *const_tuple_str_plain_worksheet_tuple;
extern PyObject *const_str_plain_reads;
extern PyObject *const_str_plain_javascript;
extern PyObject *const_str_plain_email;
extern PyObject *const_tuple_str_plain_encodestring_str_plain_decodestring_tuple;
extern PyObject *const_str_plain_saved;
extern PyObject *const_str_plain_input;
extern PyObject *const_str_plain_strip;
extern PyObject *const_str_plain_author;
extern PyObject *const_str_plain_NotebookReader;
extern PyObject *const_str_plain_codecell;
extern PyObject *const_str_plain_png;
static PyObject *const_str_plain__set_int;
extern PyObject *const_str_plain_new_worksheet;
extern PyObject *const_str_plain_svg;
extern PyObject *const_str_plain_output_jpeg;
extern PyObject *const_str_plain_read;
static PyObject *const_tuple_str_plain_elem_str_plain_level_str_plain_i_tuple;
extern PyObject *const_str_plain_frame;
static PyObject *const_str_digest_3d48c52b0336b3281a48b8cc1d8f36ab;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_tuple_str_plain_html_tuple;
static PyObject *const_tuple_str_plain_traceback_tuple;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_root;
extern PyObject *const_str_plain_nbbase;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_plain_warnings;
extern PyObject *const_str_plain_metadata;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_22b9e01565e0e7ca69301d289839dfd5;
static PyObject *const_str_plain_XMLReader;
extern PyObject *const_str_plain_NotebookWriter;
extern PyObject *const_str_plain_evalue;
static PyObject *const_str_plain_SubElement;
extern PyObject *const_str_plain_output_javascript;
extern PyObject *const_str_plain_output_png;
extern PyObject *const_str_plain_DeprecationWarning;
extern PyObject *const_str_plain_parent;
static PyObject *const_str_plain__get_binary;
extern PyObject *const_str_plain_outputs;
extern PyObject *const_str_plain_base64;
extern PyObject *const_str_plain_e;
extern PyObject *const_tuple_str_plain_frame_tuple;
extern PyObject *const_str_plain_indent;
extern PyObject *const_tuple_empty;
extern PyObject *const_tuple_str_plain_cells_tuple;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain__set_text;
extern PyObject *const_str_plain_output_json;
static PyObject *const_tuple_str_plain_e_str_plain_tag_str_plain_sub_e_tuple;
extern PyObject *const_str_plain_tail;
extern PyObject *const_str_plain_name;
static PyObject *const_str_plain_getiterator;
extern PyObject *const_str_plain_prompt_number;
extern PyObject *const_str_plain_attr;
static PyObject *const_str_digest_202eaa02524072004d16920aea9f34fd;
extern PyObject *const_tuple_str_plain_outputs_tuple;
static PyObject *const_str_plain__set_binary;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_etype;
extern PyObject *const_str_plain_encodestring;
static PyObject *const_tuple_str_plain_self_str_plain_s_str_plain_kwargs_str_plain_root_tuple;
extern PyObject *const_str_plain_ElementTree;
extern PyObject *const_str_plain_False;
extern PyObject *const_tuple_str_plain_unicode_type_tuple;
extern PyObject *const_tuple_str_plain_NotebookReader_str_plain_NotebookWriter_tuple;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_output;
extern PyObject *const_int_0;
static PyObject *const_str_digest_1cd3c33c063c9b780c36910c2ee6412b;
extern PyObject *const_str_plain_text;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_output_type;
extern PyObject *const_str_plain_htmlcell;
static PyObject *const_tuple_str_plain_worksheets_tuple;
extern PyObject *const_str_plain_html;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_plain__get_int;
static PyObject *const_str_digest_1250e599804cefcb7c13e31b8545b2d1;
extern PyObject *const_str_plain__reader;
static PyObject *const_str_plain_worksheet;
extern PyObject *const_str_plain_latex;
extern PyObject *const_str_digest_b8872718382dd39ffa4013e303d20ce5;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_output_svg;
static PyObject *const_str_plain_nbnode;
static PyObject *const_str_plain__get_bool;
extern PyObject *const_str_plain_unicode_type;
extern PyObject *const_str_plain_cells;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_fromstring;
extern PyObject *const_str_plain_rendered;
static PyObject *const_str_plain__set_bool;
extern PyObject *const_str_plain_created;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_source;
extern PyObject *const_str_plain_new_output;
extern PyObject *const_str_digest_34e3966f9679fde95e40f5931481245c;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_534db1498040f307714baa41a4cabd77;
extern PyObject *const_str_plain_language;
extern PyObject *const_str_plain_output_html;
extern PyObject *const_str_plain_1;
extern PyObject *const_str_plain_worksheets;
extern PyObject *const_str_newline;
extern PyObject *const_str_plain_elem;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_tuple_652858bbf8356d4af2bfe933ccb7dc27_tuple;
extern PyObject *const_str_plain_license;
extern PyObject *const_str_plain_output_latex;
extern PyObject *const_str_plain_level;
extern PyObject *const_str_plain_to_notebook;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_decodestring;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_ET;
extern PyObject *const_str_plain_new_notebook;
extern PyObject *const_str_plain_new_code_cell;
static PyObject *const_str_plain_sub_e;
extern PyObject *const_tuple_str_plain_ElementTree_tuple;
extern PyObject *const_str_plain_markdowncell;
extern PyObject *const_tuple_str_plain_output_tuple;
extern PyObject *const_str_plain_find;
static PyObject *const_str_plain__get_text;
extern PyObject *const_str_plain_new_metadata;
extern PyObject *const_str_plain_markdown;
extern PyObject *const_str_plain_jpeg;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_f87cf22b2ff8d81082b89ed72f489fb1 = UNSTREAM_STRING_ASCII( &constant_bin[ 2800248 ], 17, 0 );
    const_tuple_fc383372e004d29aac698a72e7b64731_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_fc383372e004d29aac698a72e7b64731_tuple, 0, const_str_plain_new_code_cell ); Py_INCREF( const_str_plain_new_code_cell );
    PyTuple_SET_ITEM( const_tuple_fc383372e004d29aac698a72e7b64731_tuple, 1, const_str_plain_new_text_cell ); Py_INCREF( const_str_plain_new_text_cell );
    PyTuple_SET_ITEM( const_tuple_fc383372e004d29aac698a72e7b64731_tuple, 2, const_str_plain_new_worksheet ); Py_INCREF( const_str_plain_new_worksheet );
    PyTuple_SET_ITEM( const_tuple_fc383372e004d29aac698a72e7b64731_tuple, 3, const_str_plain_new_notebook ); Py_INCREF( const_str_plain_new_notebook );
    PyTuple_SET_ITEM( const_tuple_fc383372e004d29aac698a72e7b64731_tuple, 4, const_str_plain_new_output ); Py_INCREF( const_str_plain_new_output );
    PyTuple_SET_ITEM( const_tuple_fc383372e004d29aac698a72e7b64731_tuple, 5, const_str_plain_new_metadata ); Py_INCREF( const_str_plain_new_metadata );
    const_str_digest_84b8e2888e5228ada3b86d4538fa1742 = UNSTREAM_STRING_ASCII( &constant_bin[ 2800265 ], 21, 0 );
    const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple = PyTuple_New( 5 );
    const_str_plain_nbnode = UNSTREAM_STRING_ASCII( &constant_bin[ 2800286 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple, 0, const_str_plain_nbnode ); Py_INCREF( const_str_plain_nbnode );
    PyTuple_SET_ITEM( const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple, 1, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );
    PyTuple_SET_ITEM( const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple, 2, const_str_plain_parent ); Py_INCREF( const_str_plain_parent );
    PyTuple_SET_ITEM( const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple, 3, const_str_plain_tag ); Py_INCREF( const_str_plain_tag );
    PyTuple_SET_ITEM( const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple, 4, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_tuple_str_plain_worksheet_tuple = PyTuple_New( 1 );
    const_str_plain_worksheet = UNSTREAM_STRING_ASCII( &constant_bin[ 2800292 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_worksheet_tuple, 0, const_str_plain_worksheet ); Py_INCREF( const_str_plain_worksheet );
    const_str_plain__set_int = UNSTREAM_STRING_ASCII( &constant_bin[ 1772637 ], 8, 1 );
    const_tuple_str_plain_elem_str_plain_level_str_plain_i_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_elem_str_plain_level_str_plain_i_tuple, 0, const_str_plain_elem ); Py_INCREF( const_str_plain_elem );
    PyTuple_SET_ITEM( const_tuple_str_plain_elem_str_plain_level_str_plain_i_tuple, 1, const_str_plain_level ); Py_INCREF( const_str_plain_level );
    PyTuple_SET_ITEM( const_tuple_str_plain_elem_str_plain_level_str_plain_i_tuple, 2, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    const_str_digest_3d48c52b0336b3281a48b8cc1d8f36ab = UNSTREAM_STRING_ASCII( &constant_bin[ 2800301 ], 65, 0 );
    const_tuple_str_plain_traceback_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_traceback_tuple, 0, const_str_plain_traceback ); Py_INCREF( const_str_plain_traceback );
    const_str_digest_22b9e01565e0e7ca69301d289839dfd5 = UNSTREAM_STRING_ASCII( &constant_bin[ 2800366 ], 86, 0 );
    const_str_plain_XMLReader = UNSTREAM_STRING_ASCII( &constant_bin[ 2800265 ], 9, 1 );
    const_str_plain_SubElement = UNSTREAM_STRING_ASCII( &constant_bin[ 2800452 ], 10, 1 );
    const_str_plain__get_binary = UNSTREAM_STRING_ASCII( &constant_bin[ 2800462 ], 11, 1 );
    const_tuple_str_plain_e_str_plain_tag_str_plain_sub_e_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_e_str_plain_tag_str_plain_sub_e_tuple, 0, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    PyTuple_SET_ITEM( const_tuple_str_plain_e_str_plain_tag_str_plain_sub_e_tuple, 1, const_str_plain_tag ); Py_INCREF( const_str_plain_tag );
    const_str_plain_sub_e = UNSTREAM_STRING_ASCII( &constant_bin[ 2800473 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_e_str_plain_tag_str_plain_sub_e_tuple, 2, const_str_plain_sub_e ); Py_INCREF( const_str_plain_sub_e );
    const_str_plain_getiterator = UNSTREAM_STRING_ASCII( &constant_bin[ 2800478 ], 11, 1 );
    const_str_digest_202eaa02524072004d16920aea9f34fd = UNSTREAM_STRING_ASCII( &constant_bin[ 661899 ], 9, 0 );
    const_str_plain__set_binary = UNSTREAM_STRING_ASCII( &constant_bin[ 2800489 ], 11, 1 );
    const_tuple_str_plain_self_str_plain_s_str_plain_kwargs_str_plain_root_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_s_str_plain_kwargs_str_plain_root_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_s_str_plain_kwargs_str_plain_root_tuple, 1, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_s_str_plain_kwargs_str_plain_root_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_s_str_plain_kwargs_str_plain_root_tuple, 3, const_str_plain_root ); Py_INCREF( const_str_plain_root );
    const_str_digest_1cd3c33c063c9b780c36910c2ee6412b = UNSTREAM_STRING_ASCII( &constant_bin[ 2800500 ], 15, 0 );
    const_tuple_str_plain_worksheets_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_worksheets_tuple, 0, const_str_plain_worksheets ); Py_INCREF( const_str_plain_worksheets );
    const_str_plain__get_int = UNSTREAM_STRING_ASCII( &constant_bin[ 2011656 ], 8, 1 );
    const_str_digest_1250e599804cefcb7c13e31b8545b2d1 = UNSTREAM_STRING_ASCII( &constant_bin[ 2800515 ], 20, 0 );
    const_str_plain__get_bool = UNSTREAM_STRING_ASCII( &constant_bin[ 2800535 ], 9, 1 );
    const_str_plain__set_bool = UNSTREAM_STRING_ASCII( &constant_bin[ 2800544 ], 9, 1 );
    const_str_digest_534db1498040f307714baa41a4cabd77 = UNSTREAM_STRING_ASCII( &constant_bin[ 2800553 ], 26, 0 );
    const_tuple_652858bbf8356d4af2bfe933ccb7dc27_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 2800579 ], 414 );
    const_str_plain__get_text = UNSTREAM_STRING_ASCII( &constant_bin[ 1722421 ], 9, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_nbformat$v2$nbxml( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_469c6406a5c1f89ac02c6d0f96b06278;
static PyCodeObject *codeobj_da11a46d87c768e4104c2d50d40cf627;
static PyCodeObject *codeobj_f31596e16b641385310487f796070b0d;
static PyCodeObject *codeobj_0945479eac88f74f06a10fb8ae89d096;
static PyCodeObject *codeobj_ef7a1aeda24d53e09bc43610296031c7;
static PyCodeObject *codeobj_2316235d97a3f227b3496f5484712293;
static PyCodeObject *codeobj_ed579f58491884418235415725222633;
static PyCodeObject *codeobj_903956b8cc7e6df6b43d2b911f702563;
static PyCodeObject *codeobj_0c3ddbca37aecc5bca70c65f8c9dcd42;
static PyCodeObject *codeobj_7f1719620fc5d294f47615805c83b4c9;
static PyCodeObject *codeobj_98eba8e5731ffd748477f5cb1779cce9;
static PyCodeObject *codeobj_48bb4b9f9b9d42e649de9aa4f098b0bf;
static PyCodeObject *codeobj_69115cbb40d0e69d1113ff7a4e0b0717;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_1250e599804cefcb7c13e31b8545b2d1 );
    codeobj_469c6406a5c1f89ac02c6d0f96b06278 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_534db1498040f307714baa41a4cabd77, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_da11a46d87c768e4104c2d50d40cf627 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_XMLReader, 109, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_f31596e16b641385310487f796070b0d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_binary, 95, const_tuple_str_plain_e_str_plain_tag_str_plain_sub_e_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0945479eac88f74f06a10fb8ae89d096 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_bool, 78, const_tuple_str_plain_e_str_plain_tag_str_plain_sub_e_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ef7a1aeda24d53e09bc43610296031c7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_int, 64, const_tuple_str_plain_e_str_plain_tag_str_plain_sub_e_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2316235d97a3f227b3496f5484712293 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_text, 50, const_tuple_str_plain_e_str_plain_tag_str_plain_sub_e_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ed579f58491884418235415725222633 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__set_binary, 103, const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_903956b8cc7e6df6b43d2b911f702563 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__set_bool, 86, const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0c3ddbca37aecc5bca70c65f8c9dcd42 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__set_int, 72, const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7f1719620fc5d294f47615805c83b4c9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__set_text, 58, const_tuple_fe11c6748ae7eb33445934ad50910c35_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_98eba8e5731ffd748477f5cb1779cce9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_indent, 34, const_tuple_str_plain_elem_str_plain_level_str_plain_i_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_48bb4b9f9b9d42e649de9aa4f098b0bf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reads, 111, const_tuple_str_plain_self_str_plain_s_str_plain_kwargs_str_plain_root_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_69115cbb40d0e69d1113ff7a4e0b0717 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_to_notebook, 115, const_tuple_652858bbf8356d4af2bfe933ccb7dc27_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_10_reads(  );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_11_to_notebook(  );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_1_indent( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_2__get_text(  );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_3__set_text(  );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_4__get_int(  );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_5__set_int(  );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_6__get_bool(  );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_7__set_bool(  );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_8__get_binary(  );


static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_9__set_binary(  );


// The module function definitions.
static PyObject *impl_nbformat$v2$nbxml$$$function_1_indent( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_elem = python_pars[ 0 ];
    PyObject *par_level = python_pars[ 1 ];
    PyObject *var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_98eba8e5731ffd748477f5cb1779cce9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_98eba8e5731ffd748477f5cb1779cce9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_98eba8e5731ffd748477f5cb1779cce9, codeobj_98eba8e5731ffd748477f5cb1779cce9, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_98eba8e5731ffd748477f5cb1779cce9 = cache_frame_98eba8e5731ffd748477f5cb1779cce9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_98eba8e5731ffd748477f5cb1779cce9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_98eba8e5731ffd748477f5cb1779cce9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        tmp_left_name_1 = const_str_newline;
        CHECK_OBJECT( par_level );
        tmp_left_name_2 = par_level;
        tmp_right_name_2 = const_str_digest_b8872718382dd39ffa4013e303d20ce5;
        tmp_right_name_1 = BINARY_OPERATION_MUL_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_i == NULL );
        var_i = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_capi_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_elem );
        tmp_len_arg_1 = par_elem;
        tmp_capi_result_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_capi_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_capi_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_capi_result_1 );

            exception_lineno = 36;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_capi_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_or_left_truth_1;
            nuitka_bool tmp_or_left_value_1;
            nuitka_bool tmp_or_right_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_elem );
            tmp_source_name_1 = par_elem;
            tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_text );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_or_left_truth_1 == 1 )
            {
                goto or_left_1;
            }
            else
            {
                goto or_right_1;
            }
            or_right_1:;
            CHECK_OBJECT( par_elem );
            tmp_source_name_2 = par_elem;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_text );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_98eba8e5731ffd748477f5cb1779cce9->m_frame.f_lineno = 37;
            tmp_operand_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_strip );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_operand_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            Py_DECREF( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_or_right_value_1;
            goto or_end_1;
            or_left_1:;
            tmp_condition_result_2 = tmp_or_left_value_1;
            or_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assattr_name_1;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_assattr_target_1;
                CHECK_OBJECT( var_i );
                tmp_left_name_3 = var_i;
                tmp_right_name_3 = const_str_digest_b8872718382dd39ffa4013e303d20ce5;
                tmp_assattr_name_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_3, tmp_right_name_3 );
                if ( tmp_assattr_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 38;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_elem );
                tmp_assattr_target_1 = par_elem;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_text, tmp_assattr_name_1 );
                Py_DECREF( tmp_assattr_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 38;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_2:;
        }
        {
            nuitka_bool tmp_condition_result_3;
            int tmp_or_left_truth_2;
            nuitka_bool tmp_or_left_value_2;
            nuitka_bool tmp_or_right_value_2;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_3;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_elem );
            tmp_source_name_3 = par_elem;
            tmp_operand_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_tail );
            if ( tmp_operand_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            Py_DECREF( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_left_value_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_or_left_truth_2 = tmp_or_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_or_left_truth_2 == 1 )
            {
                goto or_left_2;
            }
            else
            {
                goto or_right_2;
            }
            or_right_2:;
            CHECK_OBJECT( par_elem );
            tmp_source_name_4 = par_elem;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_tail );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_98eba8e5731ffd748477f5cb1779cce9->m_frame.f_lineno = 39;
            tmp_operand_name_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_strip );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_operand_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            Py_DECREF( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_right_value_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_3 = tmp_or_right_value_2;
            goto or_end_2;
            or_left_2:;
            tmp_condition_result_3 = tmp_or_left_value_2;
            or_end_2:;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assattr_name_2;
                PyObject *tmp_assattr_target_2;
                CHECK_OBJECT( var_i );
                tmp_assattr_name_2 = var_i;
                CHECK_OBJECT( par_elem );
                tmp_assattr_target_2 = par_elem;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_tail, tmp_assattr_name_2 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 40;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_3:;
        }
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_elem );
            tmp_iter_arg_1 = par_elem;
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_2;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_3;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooo";
                    exception_lineno = 41;
                    goto try_except_handler_2;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_3;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_4 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = par_elem;
                assert( old != NULL );
                par_elem = tmp_assign_source_4;
                Py_INCREF( par_elem );
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_indent );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_indent );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "indent" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 42;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_elem );
            tmp_args_element_name_1 = par_elem;
            CHECK_OBJECT( par_level );
            tmp_left_name_4 = par_level;
            tmp_right_name_4 = const_int_pos_1;
            tmp_args_element_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_4, tmp_right_name_4 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            frame_98eba8e5731ffd748477f5cb1779cce9->m_frame.f_lineno = 42;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        {
            nuitka_bool tmp_condition_result_4;
            int tmp_or_left_truth_3;
            nuitka_bool tmp_or_left_value_3;
            nuitka_bool tmp_or_right_value_3;
            PyObject *tmp_operand_name_5;
            PyObject *tmp_source_name_5;
            PyObject *tmp_operand_name_6;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( par_elem );
            tmp_source_name_5 = par_elem;
            tmp_operand_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_tail );
            if ( tmp_operand_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
            Py_DECREF( tmp_operand_name_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_left_value_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_or_left_truth_3 = tmp_or_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_or_left_truth_3 == 1 )
            {
                goto or_left_3;
            }
            else
            {
                goto or_right_3;
            }
            or_right_3:;
            CHECK_OBJECT( par_elem );
            tmp_source_name_6 = par_elem;
            tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_tail );
            if ( tmp_called_instance_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_98eba8e5731ffd748477f5cb1779cce9->m_frame.f_lineno = 43;
            tmp_operand_name_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_strip );
            Py_DECREF( tmp_called_instance_3 );
            if ( tmp_operand_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_6 );
            Py_DECREF( tmp_operand_name_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_right_value_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_4 = tmp_or_right_value_3;
            goto or_end_3;
            or_left_3:;
            tmp_condition_result_4 = tmp_or_left_value_3;
            or_end_3:;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assattr_name_3;
                PyObject *tmp_assattr_target_3;
                CHECK_OBJECT( var_i );
                tmp_assattr_name_3 = var_i;
                CHECK_OBJECT( par_elem );
                tmp_assattr_target_3 = par_elem;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_tail, tmp_assattr_name_3 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 44;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_4:;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_5;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            int tmp_truth_name_2;
            int tmp_or_left_truth_4;
            nuitka_bool tmp_or_left_value_4;
            nuitka_bool tmp_or_right_value_4;
            PyObject *tmp_operand_name_7;
            PyObject *tmp_source_name_7;
            PyObject *tmp_operand_name_8;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( par_level );
            tmp_truth_name_2 = CHECK_IF_TRUE( par_level );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( par_elem );
            tmp_source_name_7 = par_elem;
            tmp_operand_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_tail );
            if ( tmp_operand_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_7 );
            Py_DECREF( tmp_operand_name_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_left_value_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_or_left_truth_4 = tmp_or_left_value_4 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_or_left_truth_4 == 1 )
            {
                goto or_left_4;
            }
            else
            {
                goto or_right_4;
            }
            or_right_4:;
            CHECK_OBJECT( par_elem );
            tmp_source_name_8 = par_elem;
            tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_tail );
            if ( tmp_called_instance_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_98eba8e5731ffd748477f5cb1779cce9->m_frame.f_lineno = 46;
            tmp_operand_name_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_strip );
            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_operand_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_8 );
            Py_DECREF( tmp_operand_name_8 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_right_value_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_right_value_1 = tmp_or_right_value_4;
            goto or_end_4;
            or_left_4:;
            tmp_and_right_value_1 = tmp_or_left_value_4;
            or_end_4:;
            tmp_condition_result_5 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_5 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assattr_name_4;
                PyObject *tmp_assattr_target_4;
                CHECK_OBJECT( var_i );
                tmp_assattr_name_4 = var_i;
                CHECK_OBJECT( par_elem );
                tmp_assattr_target_4 = par_elem;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_tail, tmp_assattr_name_4 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 47;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_5:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_98eba8e5731ffd748477f5cb1779cce9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_98eba8e5731ffd748477f5cb1779cce9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_98eba8e5731ffd748477f5cb1779cce9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_98eba8e5731ffd748477f5cb1779cce9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_98eba8e5731ffd748477f5cb1779cce9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_98eba8e5731ffd748477f5cb1779cce9,
        type_description_1,
        par_elem,
        par_level,
        var_i
    );


    // Release cached frame.
    if ( frame_98eba8e5731ffd748477f5cb1779cce9 == cache_frame_98eba8e5731ffd748477f5cb1779cce9 )
    {
        Py_DECREF( frame_98eba8e5731ffd748477f5cb1779cce9 );
    }
    cache_frame_98eba8e5731ffd748477f5cb1779cce9 = NULL;

    assertFrameObject( frame_98eba8e5731ffd748477f5cb1779cce9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_1_indent );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_elem );
    par_elem = NULL;

    CHECK_OBJECT( (PyObject *)par_level );
    Py_DECREF( par_level );
    par_level = NULL;

    CHECK_OBJECT( (PyObject *)var_i );
    Py_DECREF( var_i );
    var_i = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_elem );
    par_elem = NULL;

    CHECK_OBJECT( (PyObject *)par_level );
    Py_DECREF( par_level );
    par_level = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_1_indent );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_2__get_text( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_e = python_pars[ 0 ];
    PyObject *par_tag = python_pars[ 1 ];
    PyObject *var_sub_e = NULL;
    struct Nuitka_FrameObject *frame_2316235d97a3f227b3496f5484712293;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_2316235d97a3f227b3496f5484712293 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2316235d97a3f227b3496f5484712293, codeobj_2316235d97a3f227b3496f5484712293, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2316235d97a3f227b3496f5484712293 = cache_frame_2316235d97a3f227b3496f5484712293;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2316235d97a3f227b3496f5484712293 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2316235d97a3f227b3496f5484712293 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_e );
        tmp_called_instance_1 = par_e;
        CHECK_OBJECT( par_tag );
        tmp_args_element_name_1 = par_tag;
        frame_2316235d97a3f227b3496f5484712293->m_frame.f_lineno = 51;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_find, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_sub_e == NULL );
        var_sub_e = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_sub_e );
        tmp_compexpr_left_1 = var_sub_e;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( var_sub_e );
            tmp_source_name_1 = var_sub_e;
            tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_text );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2316235d97a3f227b3496f5484712293 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2316235d97a3f227b3496f5484712293 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2316235d97a3f227b3496f5484712293 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2316235d97a3f227b3496f5484712293, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2316235d97a3f227b3496f5484712293->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2316235d97a3f227b3496f5484712293, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2316235d97a3f227b3496f5484712293,
        type_description_1,
        par_e,
        par_tag,
        var_sub_e
    );


    // Release cached frame.
    if ( frame_2316235d97a3f227b3496f5484712293 == cache_frame_2316235d97a3f227b3496f5484712293 )
    {
        Py_DECREF( frame_2316235d97a3f227b3496f5484712293 );
    }
    cache_frame_2316235d97a3f227b3496f5484712293 = NULL;

    assertFrameObject( frame_2316235d97a3f227b3496f5484712293 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_2__get_text );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    CHECK_OBJECT( (PyObject *)var_sub_e );
    Py_DECREF( var_sub_e );
    var_sub_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_sub_e );
    var_sub_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_2__get_text );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_3__set_text( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_nbnode = python_pars[ 0 ];
    PyObject *par_attr = python_pars[ 1 ];
    PyObject *par_parent = python_pars[ 2 ];
    PyObject *par_tag = python_pars[ 3 ];
    PyObject *var_e = NULL;
    struct Nuitka_FrameObject *frame_7f1719620fc5d294f47615805c83b4c9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_7f1719620fc5d294f47615805c83b4c9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7f1719620fc5d294f47615805c83b4c9, codeobj_7f1719620fc5d294f47615805c83b4c9, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7f1719620fc5d294f47615805c83b4c9 = cache_frame_7f1719620fc5d294f47615805c83b4c9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7f1719620fc5d294f47615805c83b4c9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7f1719620fc5d294f47615805c83b4c9 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_attr );
        tmp_compexpr_left_1 = par_attr;
        CHECK_OBJECT( par_nbnode );
        tmp_compexpr_right_1 = par_nbnode;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_ET );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ET );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ET" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 60;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_parent );
            tmp_args_element_name_1 = par_parent;
            CHECK_OBJECT( par_tag );
            tmp_args_element_name_2 = par_tag;
            frame_7f1719620fc5d294f47615805c83b4c9->m_frame.f_lineno = 60;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_SubElement, call_args );
            }

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_e == NULL );
            var_e = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( par_nbnode );
            tmp_subscribed_name_1 = par_nbnode;
            CHECK_OBJECT( par_attr );
            tmp_subscript_name_1 = par_attr;
            tmp_assattr_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_e );
            tmp_assattr_target_1 = var_e;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_text, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7f1719620fc5d294f47615805c83b4c9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7f1719620fc5d294f47615805c83b4c9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7f1719620fc5d294f47615805c83b4c9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7f1719620fc5d294f47615805c83b4c9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7f1719620fc5d294f47615805c83b4c9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7f1719620fc5d294f47615805c83b4c9,
        type_description_1,
        par_nbnode,
        par_attr,
        par_parent,
        par_tag,
        var_e
    );


    // Release cached frame.
    if ( frame_7f1719620fc5d294f47615805c83b4c9 == cache_frame_7f1719620fc5d294f47615805c83b4c9 )
    {
        Py_DECREF( frame_7f1719620fc5d294f47615805c83b4c9 );
    }
    cache_frame_7f1719620fc5d294f47615805c83b4c9 = NULL;

    assertFrameObject( frame_7f1719620fc5d294f47615805c83b4c9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_3__set_text );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_nbnode );
    Py_DECREF( par_nbnode );
    par_nbnode = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_parent );
    Py_DECREF( par_parent );
    par_parent = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_nbnode );
    Py_DECREF( par_nbnode );
    par_nbnode = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_parent );
    Py_DECREF( par_parent );
    par_parent = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_3__set_text );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_4__get_int( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_e = python_pars[ 0 ];
    PyObject *par_tag = python_pars[ 1 ];
    PyObject *var_sub_e = NULL;
    struct Nuitka_FrameObject *frame_ef7a1aeda24d53e09bc43610296031c7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_ef7a1aeda24d53e09bc43610296031c7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ef7a1aeda24d53e09bc43610296031c7, codeobj_ef7a1aeda24d53e09bc43610296031c7, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ef7a1aeda24d53e09bc43610296031c7 = cache_frame_ef7a1aeda24d53e09bc43610296031c7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ef7a1aeda24d53e09bc43610296031c7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ef7a1aeda24d53e09bc43610296031c7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_e );
        tmp_called_instance_1 = par_e;
        CHECK_OBJECT( par_tag );
        tmp_args_element_name_1 = par_tag;
        frame_ef7a1aeda24d53e09bc43610296031c7->m_frame.f_lineno = 65;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_find, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_sub_e == NULL );
        var_sub_e = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_sub_e );
        tmp_compexpr_left_1 = var_sub_e;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_int_arg_1;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( var_sub_e );
            tmp_source_name_1 = var_sub_e;
            tmp_int_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_text );
            if ( tmp_int_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_return_value = PyNumber_Int( tmp_int_arg_1 );
            Py_DECREF( tmp_int_arg_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ef7a1aeda24d53e09bc43610296031c7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ef7a1aeda24d53e09bc43610296031c7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ef7a1aeda24d53e09bc43610296031c7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ef7a1aeda24d53e09bc43610296031c7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ef7a1aeda24d53e09bc43610296031c7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ef7a1aeda24d53e09bc43610296031c7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ef7a1aeda24d53e09bc43610296031c7,
        type_description_1,
        par_e,
        par_tag,
        var_sub_e
    );


    // Release cached frame.
    if ( frame_ef7a1aeda24d53e09bc43610296031c7 == cache_frame_ef7a1aeda24d53e09bc43610296031c7 )
    {
        Py_DECREF( frame_ef7a1aeda24d53e09bc43610296031c7 );
    }
    cache_frame_ef7a1aeda24d53e09bc43610296031c7 = NULL;

    assertFrameObject( frame_ef7a1aeda24d53e09bc43610296031c7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_4__get_int );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    CHECK_OBJECT( (PyObject *)var_sub_e );
    Py_DECREF( var_sub_e );
    var_sub_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_sub_e );
    var_sub_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_4__get_int );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_5__set_int( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_nbnode = python_pars[ 0 ];
    PyObject *par_attr = python_pars[ 1 ];
    PyObject *par_parent = python_pars[ 2 ];
    PyObject *par_tag = python_pars[ 3 ];
    PyObject *var_e = NULL;
    struct Nuitka_FrameObject *frame_0c3ddbca37aecc5bca70c65f8c9dcd42;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_0c3ddbca37aecc5bca70c65f8c9dcd42 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0c3ddbca37aecc5bca70c65f8c9dcd42, codeobj_0c3ddbca37aecc5bca70c65f8c9dcd42, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0c3ddbca37aecc5bca70c65f8c9dcd42 = cache_frame_0c3ddbca37aecc5bca70c65f8c9dcd42;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0c3ddbca37aecc5bca70c65f8c9dcd42 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0c3ddbca37aecc5bca70c65f8c9dcd42 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_attr );
        tmp_compexpr_left_1 = par_attr;
        CHECK_OBJECT( par_nbnode );
        tmp_compexpr_right_1 = par_nbnode;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_ET );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ET );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ET" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 74;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_parent );
            tmp_args_element_name_1 = par_parent;
            CHECK_OBJECT( par_tag );
            tmp_args_element_name_2 = par_tag;
            frame_0c3ddbca37aecc5bca70c65f8c9dcd42->m_frame.f_lineno = 74;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_SubElement, call_args );
            }

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_e == NULL );
            var_e = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_assattr_target_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_unicode_type );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode_type );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode_type" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 75;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_2;
            CHECK_OBJECT( par_nbnode );
            tmp_subscribed_name_1 = par_nbnode;
            CHECK_OBJECT( par_attr );
            tmp_subscript_name_1 = par_attr;
            tmp_args_element_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 75;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            frame_0c3ddbca37aecc5bca70c65f8c9dcd42->m_frame.f_lineno = 75;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 75;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_e );
            tmp_assattr_target_1 = var_e;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_text, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 75;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c3ddbca37aecc5bca70c65f8c9dcd42 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c3ddbca37aecc5bca70c65f8c9dcd42 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0c3ddbca37aecc5bca70c65f8c9dcd42, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0c3ddbca37aecc5bca70c65f8c9dcd42->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0c3ddbca37aecc5bca70c65f8c9dcd42, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0c3ddbca37aecc5bca70c65f8c9dcd42,
        type_description_1,
        par_nbnode,
        par_attr,
        par_parent,
        par_tag,
        var_e
    );


    // Release cached frame.
    if ( frame_0c3ddbca37aecc5bca70c65f8c9dcd42 == cache_frame_0c3ddbca37aecc5bca70c65f8c9dcd42 )
    {
        Py_DECREF( frame_0c3ddbca37aecc5bca70c65f8c9dcd42 );
    }
    cache_frame_0c3ddbca37aecc5bca70c65f8c9dcd42 = NULL;

    assertFrameObject( frame_0c3ddbca37aecc5bca70c65f8c9dcd42 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_5__set_int );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_nbnode );
    Py_DECREF( par_nbnode );
    par_nbnode = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_parent );
    Py_DECREF( par_parent );
    par_parent = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_nbnode );
    Py_DECREF( par_nbnode );
    par_nbnode = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_parent );
    Py_DECREF( par_parent );
    par_parent = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_5__set_int );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_6__get_bool( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_e = python_pars[ 0 ];
    PyObject *par_tag = python_pars[ 1 ];
    PyObject *var_sub_e = NULL;
    struct Nuitka_FrameObject *frame_0945479eac88f74f06a10fb8ae89d096;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_0945479eac88f74f06a10fb8ae89d096 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0945479eac88f74f06a10fb8ae89d096, codeobj_0945479eac88f74f06a10fb8ae89d096, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0945479eac88f74f06a10fb8ae89d096 = cache_frame_0945479eac88f74f06a10fb8ae89d096;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0945479eac88f74f06a10fb8ae89d096 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0945479eac88f74f06a10fb8ae89d096 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_e );
        tmp_called_instance_1 = par_e;
        CHECK_OBJECT( par_tag );
        tmp_args_element_name_1 = par_tag;
        frame_0945479eac88f74f06a10fb8ae89d096->m_frame.f_lineno = 79;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_find, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_sub_e == NULL );
        var_sub_e = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_sub_e );
        tmp_compexpr_left_1 = var_sub_e;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_value_name_1;
            PyObject *tmp_int_arg_1;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( var_sub_e );
            tmp_source_name_1 = var_sub_e;
            tmp_int_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_text );
            if ( tmp_int_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_value_name_1 = PyNumber_Int( tmp_int_arg_1 );
            Py_DECREF( tmp_int_arg_1 );
            if ( tmp_value_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_value_name_1 );
            Py_DECREF( tmp_value_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0945479eac88f74f06a10fb8ae89d096 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0945479eac88f74f06a10fb8ae89d096 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0945479eac88f74f06a10fb8ae89d096 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0945479eac88f74f06a10fb8ae89d096, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0945479eac88f74f06a10fb8ae89d096->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0945479eac88f74f06a10fb8ae89d096, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0945479eac88f74f06a10fb8ae89d096,
        type_description_1,
        par_e,
        par_tag,
        var_sub_e
    );


    // Release cached frame.
    if ( frame_0945479eac88f74f06a10fb8ae89d096 == cache_frame_0945479eac88f74f06a10fb8ae89d096 )
    {
        Py_DECREF( frame_0945479eac88f74f06a10fb8ae89d096 );
    }
    cache_frame_0945479eac88f74f06a10fb8ae89d096 = NULL;

    assertFrameObject( frame_0945479eac88f74f06a10fb8ae89d096 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_6__get_bool );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    CHECK_OBJECT( (PyObject *)var_sub_e );
    Py_DECREF( var_sub_e );
    var_sub_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_sub_e );
    var_sub_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_6__get_bool );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_7__set_bool( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_nbnode = python_pars[ 0 ];
    PyObject *par_attr = python_pars[ 1 ];
    PyObject *par_parent = python_pars[ 2 ];
    PyObject *par_tag = python_pars[ 3 ];
    PyObject *var_e = NULL;
    struct Nuitka_FrameObject *frame_903956b8cc7e6df6b43d2b911f702563;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_903956b8cc7e6df6b43d2b911f702563 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_903956b8cc7e6df6b43d2b911f702563, codeobj_903956b8cc7e6df6b43d2b911f702563, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_903956b8cc7e6df6b43d2b911f702563 = cache_frame_903956b8cc7e6df6b43d2b911f702563;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_903956b8cc7e6df6b43d2b911f702563 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_903956b8cc7e6df6b43d2b911f702563 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_attr );
        tmp_compexpr_left_1 = par_attr;
        CHECK_OBJECT( par_nbnode );
        tmp_compexpr_right_1 = par_nbnode;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_ET );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ET );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ET" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 88;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_parent );
            tmp_args_element_name_1 = par_parent;
            CHECK_OBJECT( par_tag );
            tmp_args_element_name_2 = par_tag;
            frame_903956b8cc7e6df6b43d2b911f702563->m_frame.f_lineno = 88;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_SubElement, call_args );
            }

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_e == NULL );
            var_e = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_subscript_result_1;
            int tmp_truth_name_1;
            CHECK_OBJECT( par_nbnode );
            tmp_subscribed_name_1 = par_nbnode;
            CHECK_OBJECT( par_attr );
            tmp_subscript_name_1 = par_attr;
            tmp_subscript_result_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_subscript_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_subscript_result_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_subscript_result_1 );

                exception_lineno = 89;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_subscript_result_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assattr_name_1;
                PyObject *tmp_assattr_target_1;
                tmp_assattr_name_1 = const_str_plain_1;
                CHECK_OBJECT( var_e );
                tmp_assattr_target_1 = var_e;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_text, tmp_assattr_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 90;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_assattr_name_2;
                PyObject *tmp_assattr_target_2;
                tmp_assattr_name_2 = const_str_plain_0;
                CHECK_OBJECT( var_e );
                tmp_assattr_target_2 = var_e;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_text, tmp_assattr_name_2 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 92;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_end_2:;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_903956b8cc7e6df6b43d2b911f702563 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_903956b8cc7e6df6b43d2b911f702563 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_903956b8cc7e6df6b43d2b911f702563, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_903956b8cc7e6df6b43d2b911f702563->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_903956b8cc7e6df6b43d2b911f702563, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_903956b8cc7e6df6b43d2b911f702563,
        type_description_1,
        par_nbnode,
        par_attr,
        par_parent,
        par_tag,
        var_e
    );


    // Release cached frame.
    if ( frame_903956b8cc7e6df6b43d2b911f702563 == cache_frame_903956b8cc7e6df6b43d2b911f702563 )
    {
        Py_DECREF( frame_903956b8cc7e6df6b43d2b911f702563 );
    }
    cache_frame_903956b8cc7e6df6b43d2b911f702563 = NULL;

    assertFrameObject( frame_903956b8cc7e6df6b43d2b911f702563 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_7__set_bool );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_nbnode );
    Py_DECREF( par_nbnode );
    par_nbnode = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_parent );
    Py_DECREF( par_parent );
    par_parent = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_nbnode );
    Py_DECREF( par_nbnode );
    par_nbnode = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_parent );
    Py_DECREF( par_parent );
    par_parent = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_7__set_bool );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_8__get_binary( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_e = python_pars[ 0 ];
    PyObject *par_tag = python_pars[ 1 ];
    PyObject *var_sub_e = NULL;
    struct Nuitka_FrameObject *frame_f31596e16b641385310487f796070b0d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f31596e16b641385310487f796070b0d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f31596e16b641385310487f796070b0d, codeobj_f31596e16b641385310487f796070b0d, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f31596e16b641385310487f796070b0d = cache_frame_f31596e16b641385310487f796070b0d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f31596e16b641385310487f796070b0d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f31596e16b641385310487f796070b0d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_e );
        tmp_called_instance_1 = par_e;
        CHECK_OBJECT( par_tag );
        tmp_args_element_name_1 = par_tag;
        frame_f31596e16b641385310487f796070b0d->m_frame.f_lineno = 96;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_find, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_sub_e == NULL );
        var_sub_e = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_sub_e );
        tmp_compexpr_left_1 = var_sub_e;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_decodestring );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_decodestring );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "decodestring" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 100;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( var_sub_e );
            tmp_source_name_1 = var_sub_e;
            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_text );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_f31596e16b641385310487f796070b0d->m_frame.f_lineno = 100;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f31596e16b641385310487f796070b0d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f31596e16b641385310487f796070b0d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f31596e16b641385310487f796070b0d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f31596e16b641385310487f796070b0d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f31596e16b641385310487f796070b0d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f31596e16b641385310487f796070b0d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f31596e16b641385310487f796070b0d,
        type_description_1,
        par_e,
        par_tag,
        var_sub_e
    );


    // Release cached frame.
    if ( frame_f31596e16b641385310487f796070b0d == cache_frame_f31596e16b641385310487f796070b0d )
    {
        Py_DECREF( frame_f31596e16b641385310487f796070b0d );
    }
    cache_frame_f31596e16b641385310487f796070b0d = NULL;

    assertFrameObject( frame_f31596e16b641385310487f796070b0d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_8__get_binary );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    CHECK_OBJECT( (PyObject *)var_sub_e );
    Py_DECREF( var_sub_e );
    var_sub_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_sub_e );
    var_sub_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_8__get_binary );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_9__set_binary( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_nbnode = python_pars[ 0 ];
    PyObject *par_attr = python_pars[ 1 ];
    PyObject *par_parent = python_pars[ 2 ];
    PyObject *par_tag = python_pars[ 3 ];
    PyObject *var_e = NULL;
    struct Nuitka_FrameObject *frame_ed579f58491884418235415725222633;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_ed579f58491884418235415725222633 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ed579f58491884418235415725222633, codeobj_ed579f58491884418235415725222633, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ed579f58491884418235415725222633 = cache_frame_ed579f58491884418235415725222633;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ed579f58491884418235415725222633 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ed579f58491884418235415725222633 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_attr );
        tmp_compexpr_left_1 = par_attr;
        CHECK_OBJECT( par_nbnode );
        tmp_compexpr_right_1 = par_nbnode;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_ET );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ET );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ET" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 105;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_parent );
            tmp_args_element_name_1 = par_parent;
            CHECK_OBJECT( par_tag );
            tmp_args_element_name_2 = par_tag;
            frame_ed579f58491884418235415725222633->m_frame.f_lineno = 105;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_SubElement, call_args );
            }

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_e == NULL );
            var_e = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_assattr_target_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_encodestring );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_encodestring );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "encodestring" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 106;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_2;
            CHECK_OBJECT( par_nbnode );
            tmp_subscribed_name_1 = par_nbnode;
            CHECK_OBJECT( par_attr );
            tmp_subscript_name_1 = par_attr;
            tmp_args_element_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            frame_ed579f58491884418235415725222633->m_frame.f_lineno = 106;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_e );
            tmp_assattr_target_1 = var_e;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_text, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed579f58491884418235415725222633 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed579f58491884418235415725222633 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ed579f58491884418235415725222633, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ed579f58491884418235415725222633->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ed579f58491884418235415725222633, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ed579f58491884418235415725222633,
        type_description_1,
        par_nbnode,
        par_attr,
        par_parent,
        par_tag,
        var_e
    );


    // Release cached frame.
    if ( frame_ed579f58491884418235415725222633 == cache_frame_ed579f58491884418235415725222633 )
    {
        Py_DECREF( frame_ed579f58491884418235415725222633 );
    }
    cache_frame_ed579f58491884418235415725222633 = NULL;

    assertFrameObject( frame_ed579f58491884418235415725222633 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_9__set_binary );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_nbnode );
    Py_DECREF( par_nbnode );
    par_nbnode = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_parent );
    Py_DECREF( par_parent );
    par_parent = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_nbnode );
    Py_DECREF( par_nbnode );
    par_nbnode = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_parent );
    Py_DECREF( par_parent );
    par_parent = NULL;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_9__set_binary );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_10_reads( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_s = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_root = NULL;
    struct Nuitka_FrameObject *frame_48bb4b9f9b9d42e649de9aa4f098b0bf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_48bb4b9f9b9d42e649de9aa4f098b0bf = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_48bb4b9f9b9d42e649de9aa4f098b0bf, codeobj_48bb4b9f9b9d42e649de9aa4f098b0bf, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_48bb4b9f9b9d42e649de9aa4f098b0bf = cache_frame_48bb4b9f9b9d42e649de9aa4f098b0bf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_48bb4b9f9b9d42e649de9aa4f098b0bf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_48bb4b9f9b9d42e649de9aa4f098b0bf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_ET );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ET );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ET" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_s );
        tmp_args_element_name_1 = par_s;
        frame_48bb4b9f9b9d42e649de9aa4f098b0bf->m_frame.f_lineno = 112;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_fromstring, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_root == NULL );
        var_root = tmp_assign_source_1;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_to_notebook );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_root );
        tmp_tuple_element_1 = var_root;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_48bb4b9f9b9d42e649de9aa4f098b0bf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_48bb4b9f9b9d42e649de9aa4f098b0bf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_48bb4b9f9b9d42e649de9aa4f098b0bf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_48bb4b9f9b9d42e649de9aa4f098b0bf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_48bb4b9f9b9d42e649de9aa4f098b0bf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_48bb4b9f9b9d42e649de9aa4f098b0bf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_48bb4b9f9b9d42e649de9aa4f098b0bf,
        type_description_1,
        par_self,
        par_s,
        par_kwargs,
        var_root
    );


    // Release cached frame.
    if ( frame_48bb4b9f9b9d42e649de9aa4f098b0bf == cache_frame_48bb4b9f9b9d42e649de9aa4f098b0bf )
    {
        Py_DECREF( frame_48bb4b9f9b9d42e649de9aa4f098b0bf );
    }
    cache_frame_48bb4b9f9b9d42e649de9aa4f098b0bf = NULL;

    assertFrameObject( frame_48bb4b9f9b9d42e649de9aa4f098b0bf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_10_reads );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_root );
    Py_DECREF( var_root );
    var_root = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_root );
    var_root = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_10_reads );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v2$nbxml$$$function_11_to_notebook( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_root = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_nbname = NULL;
    PyObject *var_nbauthor = NULL;
    PyObject *var_nbemail = NULL;
    PyObject *var_nblicense = NULL;
    PyObject *var_nbcreated = NULL;
    PyObject *var_nbsaved = NULL;
    PyObject *var_worksheets = NULL;
    PyObject *var_ws_e = NULL;
    PyObject *var_wsname = NULL;
    PyObject *var_cells = NULL;
    PyObject *var_cell_e = NULL;
    PyObject *var_input = NULL;
    PyObject *var_prompt_number = NULL;
    PyObject *var_collapsed = NULL;
    PyObject *var_language = NULL;
    PyObject *var_outputs = NULL;
    PyObject *var_output_e = NULL;
    PyObject *var_output_type = NULL;
    PyObject *var_output_text = NULL;
    PyObject *var_output_png = NULL;
    PyObject *var_output_jpeg = NULL;
    PyObject *var_output_svg = NULL;
    PyObject *var_output_html = NULL;
    PyObject *var_output_latex = NULL;
    PyObject *var_output_json = NULL;
    PyObject *var_output_javascript = NULL;
    PyObject *var_out_prompt_number = NULL;
    PyObject *var_etype = NULL;
    PyObject *var_evalue = NULL;
    PyObject *var_traceback = NULL;
    PyObject *var_traceback_e = NULL;
    PyObject *var_frame_e = NULL;
    PyObject *var_output = NULL;
    PyObject *var_cc = NULL;
    PyObject *var_source = NULL;
    PyObject *var_rendered = NULL;
    PyObject *var_ws = NULL;
    PyObject *var_md = NULL;
    PyObject *var_nb = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_for_loop_4__for_iterator = NULL;
    PyObject *tmp_for_loop_4__iter_value = NULL;
    struct Nuitka_FrameObject *frame_69115cbb40d0e69d1113ff7a4e0b0717;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_69115cbb40d0e69d1113ff7a4e0b0717 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_69115cbb40d0e69d1113ff7a4e0b0717, codeobj_69115cbb40d0e69d1113ff7a4e0b0717, module_nbformat$v2$nbxml, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_69115cbb40d0e69d1113ff7a4e0b0717 = cache_frame_69115cbb40d0e69d1113ff7a4e0b0717;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_69115cbb40d0e69d1113ff7a4e0b0717 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_69115cbb40d0e69d1113ff7a4e0b0717 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_warnings );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warnings );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warnings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 116;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = const_str_digest_22b9e01565e0e7ca69301d289839dfd5;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DeprecationWarning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_2;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 116;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        CHECK_OBJECT( par_root );
        tmp_args_element_name_3 = par_root;
        tmp_args_element_name_4 = const_str_plain_name;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_nbname == NULL );
        var_nbname = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        CHECK_OBJECT( par_root );
        tmp_args_element_name_5 = par_root;
        tmp_args_element_name_6 = const_str_plain_author;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 119;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_nbauthor == NULL );
        var_nbauthor = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_5;
        CHECK_OBJECT( par_root );
        tmp_args_element_name_7 = par_root;
        tmp_args_element_name_8 = const_str_plain_email;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 120;
        {
            PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_nbemail == NULL );
        var_nbemail = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_6;
        CHECK_OBJECT( par_root );
        tmp_args_element_name_9 = par_root;
        tmp_args_element_name_10 = const_str_plain_license;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 121;
        {
            PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
        }

        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_nblicense == NULL );
        var_nblicense = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_args_element_name_12;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_7;
        CHECK_OBJECT( par_root );
        tmp_args_element_name_11 = par_root;
        tmp_args_element_name_12 = const_str_plain_created;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
        }

        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_nbcreated == NULL );
        var_nbcreated = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_args_element_name_14;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_8;
        CHECK_OBJECT( par_root );
        tmp_args_element_name_13 = par_root;
        tmp_args_element_name_14 = const_str_plain_saved;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 123;
        {
            PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14 };
            tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
        }

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_nbsaved == NULL );
        var_nbsaved = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = PyList_New( 0 );
        assert( var_worksheets == NULL );
        var_worksheets = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_root );
        tmp_called_instance_2 = par_root;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 126;
        tmp_called_instance_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_find, &PyTuple_GET_ITEM( const_tuple_str_plain_worksheets_tuple, 0 ) );

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 126;
        tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_getiterator, &PyTuple_GET_ITEM( const_tuple_str_plain_worksheet_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_8;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_9 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 126;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_10 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_ws_e;
            var_ws_e = tmp_assign_source_10;
            Py_INCREF( var_ws_e );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_args_element_name_16;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_8 = tmp_mvar_value_9;
        CHECK_OBJECT( var_ws_e );
        tmp_args_element_name_15 = var_ws_e;
        tmp_args_element_name_16 = const_str_plain_name;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 127;
        {
            PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_16 };
            tmp_assign_source_11 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_wsname;
            var_wsname = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = PyList_New( 0 );
        {
            PyObject *old = var_cells;
            var_cells = tmp_assign_source_12;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_called_instance_4;
        CHECK_OBJECT( var_ws_e );
        tmp_called_instance_4 = var_ws_e;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 129;
        tmp_called_instance_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_find, &PyTuple_GET_ITEM( const_tuple_str_plain_cells_tuple, 0 ) );

        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 129;
        tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_getiterator );
        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = tmp_for_loop_2__for_iterator;
            tmp_for_loop_2__for_iterator = tmp_assign_source_13;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_14;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_14 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_14 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 129;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_14;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_15;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_15 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_cell_e;
            var_cell_e = tmp_assign_source_15;
            Py_INCREF( var_cell_e );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_cell_e );
        tmp_source_name_2 = var_cell_e;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_tag );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_compexpr_right_1 = const_str_plain_codecell;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_called_name_9;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_args_element_name_17;
            PyObject *tmp_args_element_name_18;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 131;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_9 = tmp_mvar_value_10;
            CHECK_OBJECT( var_cell_e );
            tmp_args_element_name_17 = var_cell_e;
            tmp_args_element_name_18 = const_str_plain_input;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 131;
            {
                PyObject *call_args[] = { tmp_args_element_name_17, tmp_args_element_name_18 };
                tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, call_args );
            }

            if ( tmp_assign_source_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_input;
                var_input = tmp_assign_source_16;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_called_name_10;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_args_element_name_19;
            PyObject *tmp_args_element_name_20;
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_int );

            if (unlikely( tmp_mvar_value_11 == NULL ))
            {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_int );
            }

            if ( tmp_mvar_value_11 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_int" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 132;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_10 = tmp_mvar_value_11;
            CHECK_OBJECT( var_cell_e );
            tmp_args_element_name_19 = var_cell_e;
            tmp_args_element_name_20 = const_str_plain_prompt_number;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 132;
            {
                PyObject *call_args[] = { tmp_args_element_name_19, tmp_args_element_name_20 };
                tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, call_args );
            }

            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 132;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_prompt_number;
                var_prompt_number = tmp_assign_source_17;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_called_name_11;
            PyObject *tmp_mvar_value_12;
            PyObject *tmp_args_element_name_21;
            PyObject *tmp_args_element_name_22;
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_bool );

            if (unlikely( tmp_mvar_value_12 == NULL ))
            {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_bool );
            }

            if ( tmp_mvar_value_12 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_bool" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 133;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_11 = tmp_mvar_value_12;
            CHECK_OBJECT( var_cell_e );
            tmp_args_element_name_21 = var_cell_e;
            tmp_args_element_name_22 = const_str_plain_collapsed;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 133;
            {
                PyObject *call_args[] = { tmp_args_element_name_21, tmp_args_element_name_22 };
                tmp_assign_source_18 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
            }

            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_collapsed;
                var_collapsed = tmp_assign_source_18;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_called_name_12;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_args_element_name_23;
            PyObject *tmp_args_element_name_24;
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_13 == NULL ))
            {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_13 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 134;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_12 = tmp_mvar_value_13;
            CHECK_OBJECT( var_cell_e );
            tmp_args_element_name_23 = var_cell_e;
            tmp_args_element_name_24 = const_str_plain_language;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 134;
            {
                PyObject *call_args[] = { tmp_args_element_name_23, tmp_args_element_name_24 };
                tmp_assign_source_19 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_12, call_args );
            }

            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_language;
                var_language = tmp_assign_source_19;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_20;
            tmp_assign_source_20 = PyList_New( 0 );
            {
                PyObject *old = var_outputs;
                var_outputs = tmp_assign_source_20;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_iter_arg_3;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_called_instance_6;
            CHECK_OBJECT( var_cell_e );
            tmp_called_instance_6 = var_cell_e;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 136;
            tmp_called_instance_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_find, &PyTuple_GET_ITEM( const_tuple_str_plain_outputs_tuple, 0 ) );

            if ( tmp_called_instance_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 136;
            tmp_iter_arg_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_getiterator, &PyTuple_GET_ITEM( const_tuple_str_plain_output_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_5 );
            if ( tmp_iter_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            tmp_assign_source_21 = MAKE_ITERATOR( tmp_iter_arg_3 );
            Py_DECREF( tmp_iter_arg_3 );
            if ( tmp_assign_source_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = tmp_for_loop_3__for_iterator;
                tmp_for_loop_3__for_iterator = tmp_assign_source_21;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_3:;
        {
            PyObject *tmp_next_source_3;
            PyObject *tmp_assign_source_22;
            CHECK_OBJECT( tmp_for_loop_3__for_iterator );
            tmp_next_source_3 = tmp_for_loop_3__for_iterator;
            tmp_assign_source_22 = ITERATOR_NEXT( tmp_next_source_3 );
            if ( tmp_assign_source_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_3;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                    exception_lineno = 136;
                    goto try_except_handler_4;
                }
            }

            {
                PyObject *old = tmp_for_loop_3__iter_value;
                tmp_for_loop_3__iter_value = tmp_assign_source_22;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_23;
            CHECK_OBJECT( tmp_for_loop_3__iter_value );
            tmp_assign_source_23 = tmp_for_loop_3__iter_value;
            {
                PyObject *old = var_output_e;
                var_output_e = tmp_assign_source_23;
                Py_INCREF( var_output_e );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_called_name_13;
            PyObject *tmp_mvar_value_14;
            PyObject *tmp_args_element_name_25;
            PyObject *tmp_args_element_name_26;
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_14 == NULL ))
            {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_14 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 137;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_13 = tmp_mvar_value_14;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_25 = var_output_e;
            tmp_args_element_name_26 = const_str_plain_output_type;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 137;
            {
                PyObject *call_args[] = { tmp_args_element_name_25, tmp_args_element_name_26 };
                tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_13, call_args );
            }

            if ( tmp_assign_source_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output_type;
                var_output_type = tmp_assign_source_24;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_25;
            PyObject *tmp_called_name_14;
            PyObject *tmp_mvar_value_15;
            PyObject *tmp_args_element_name_27;
            PyObject *tmp_args_element_name_28;
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_15 == NULL ))
            {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_15 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 138;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_14 = tmp_mvar_value_15;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_27 = var_output_e;
            tmp_args_element_name_28 = const_str_plain_text;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 138;
            {
                PyObject *call_args[] = { tmp_args_element_name_27, tmp_args_element_name_28 };
                tmp_assign_source_25 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_14, call_args );
            }

            if ( tmp_assign_source_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output_text;
                var_output_text = tmp_assign_source_25;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_called_name_15;
            PyObject *tmp_mvar_value_16;
            PyObject *tmp_args_element_name_29;
            PyObject *tmp_args_element_name_30;
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_binary );

            if (unlikely( tmp_mvar_value_16 == NULL ))
            {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_binary );
            }

            if ( tmp_mvar_value_16 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_binary" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 139;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_15 = tmp_mvar_value_16;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_29 = var_output_e;
            tmp_args_element_name_30 = const_str_plain_png;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 139;
            {
                PyObject *call_args[] = { tmp_args_element_name_29, tmp_args_element_name_30 };
                tmp_assign_source_26 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_15, call_args );
            }

            if ( tmp_assign_source_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 139;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output_png;
                var_output_png = tmp_assign_source_26;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_called_name_16;
            PyObject *tmp_mvar_value_17;
            PyObject *tmp_args_element_name_31;
            PyObject *tmp_args_element_name_32;
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_binary );

            if (unlikely( tmp_mvar_value_17 == NULL ))
            {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_binary );
            }

            if ( tmp_mvar_value_17 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_binary" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 140;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_16 = tmp_mvar_value_17;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_31 = var_output_e;
            tmp_args_element_name_32 = const_str_plain_jpeg;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 140;
            {
                PyObject *call_args[] = { tmp_args_element_name_31, tmp_args_element_name_32 };
                tmp_assign_source_27 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_16, call_args );
            }

            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output_jpeg;
                var_output_jpeg = tmp_assign_source_27;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_called_name_17;
            PyObject *tmp_mvar_value_18;
            PyObject *tmp_args_element_name_33;
            PyObject *tmp_args_element_name_34;
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_18 == NULL ))
            {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_18 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 141;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_17 = tmp_mvar_value_18;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_33 = var_output_e;
            tmp_args_element_name_34 = const_str_plain_svg;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 141;
            {
                PyObject *call_args[] = { tmp_args_element_name_33, tmp_args_element_name_34 };
                tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_17, call_args );
            }

            if ( tmp_assign_source_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output_svg;
                var_output_svg = tmp_assign_source_28;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_called_name_18;
            PyObject *tmp_mvar_value_19;
            PyObject *tmp_args_element_name_35;
            PyObject *tmp_args_element_name_36;
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_19 == NULL ))
            {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_19 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 142;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_18 = tmp_mvar_value_19;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_35 = var_output_e;
            tmp_args_element_name_36 = const_str_plain_html;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 142;
            {
                PyObject *call_args[] = { tmp_args_element_name_35, tmp_args_element_name_36 };
                tmp_assign_source_29 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_18, call_args );
            }

            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output_html;
                var_output_html = tmp_assign_source_29;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_called_name_19;
            PyObject *tmp_mvar_value_20;
            PyObject *tmp_args_element_name_37;
            PyObject *tmp_args_element_name_38;
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_20 == NULL ))
            {
                tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_20 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 143;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_19 = tmp_mvar_value_20;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_37 = var_output_e;
            tmp_args_element_name_38 = const_str_plain_latex;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 143;
            {
                PyObject *call_args[] = { tmp_args_element_name_37, tmp_args_element_name_38 };
                tmp_assign_source_30 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_19, call_args );
            }

            if ( tmp_assign_source_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output_latex;
                var_output_latex = tmp_assign_source_30;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_called_name_20;
            PyObject *tmp_mvar_value_21;
            PyObject *tmp_args_element_name_39;
            PyObject *tmp_args_element_name_40;
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_21 == NULL ))
            {
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_21 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 144;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_20 = tmp_mvar_value_21;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_39 = var_output_e;
            tmp_args_element_name_40 = const_str_plain_json;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 144;
            {
                PyObject *call_args[] = { tmp_args_element_name_39, tmp_args_element_name_40 };
                tmp_assign_source_31 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_20, call_args );
            }

            if ( tmp_assign_source_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output_json;
                var_output_json = tmp_assign_source_31;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_32;
            PyObject *tmp_called_name_21;
            PyObject *tmp_mvar_value_22;
            PyObject *tmp_args_element_name_41;
            PyObject *tmp_args_element_name_42;
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_22 == NULL ))
            {
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_22 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 145;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_21 = tmp_mvar_value_22;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_41 = var_output_e;
            tmp_args_element_name_42 = const_str_plain_javascript;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 145;
            {
                PyObject *call_args[] = { tmp_args_element_name_41, tmp_args_element_name_42 };
                tmp_assign_source_32 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_21, call_args );
            }

            if ( tmp_assign_source_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 145;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output_javascript;
                var_output_javascript = tmp_assign_source_32;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_called_name_22;
            PyObject *tmp_mvar_value_23;
            PyObject *tmp_args_element_name_43;
            PyObject *tmp_args_element_name_44;
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_int );

            if (unlikely( tmp_mvar_value_23 == NULL ))
            {
                tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_int );
            }

            if ( tmp_mvar_value_23 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_int" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 147;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_22 = tmp_mvar_value_23;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_43 = var_output_e;
            tmp_args_element_name_44 = const_str_plain_prompt_number;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 147;
            {
                PyObject *call_args[] = { tmp_args_element_name_43, tmp_args_element_name_44 };
                tmp_assign_source_33 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_22, call_args );
            }

            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_out_prompt_number;
                var_out_prompt_number = tmp_assign_source_33;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_34;
            PyObject *tmp_called_name_23;
            PyObject *tmp_mvar_value_24;
            PyObject *tmp_args_element_name_45;
            PyObject *tmp_args_element_name_46;
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_24 == NULL ))
            {
                tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_24 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 148;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_23 = tmp_mvar_value_24;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_45 = var_output_e;
            tmp_args_element_name_46 = const_str_plain_etype;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 148;
            {
                PyObject *call_args[] = { tmp_args_element_name_45, tmp_args_element_name_46 };
                tmp_assign_source_34 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_23, call_args );
            }

            if ( tmp_assign_source_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_etype;
                var_etype = tmp_assign_source_34;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_35;
            PyObject *tmp_called_name_24;
            PyObject *tmp_mvar_value_25;
            PyObject *tmp_args_element_name_47;
            PyObject *tmp_args_element_name_48;
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_25 == NULL ))
            {
                tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_25 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 149;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_24 = tmp_mvar_value_25;
            CHECK_OBJECT( var_output_e );
            tmp_args_element_name_47 = var_output_e;
            tmp_args_element_name_48 = const_str_plain_evalue;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 149;
            {
                PyObject *call_args[] = { tmp_args_element_name_47, tmp_args_element_name_48 };
                tmp_assign_source_35 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_24, call_args );
            }

            if ( tmp_assign_source_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 149;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_evalue;
                var_evalue = tmp_assign_source_35;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_36;
            tmp_assign_source_36 = PyList_New( 0 );
            {
                PyObject *old = var_traceback;
                var_traceback = tmp_assign_source_36;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_37;
            PyObject *tmp_called_instance_7;
            CHECK_OBJECT( var_output_e );
            tmp_called_instance_7 = var_output_e;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 151;
            tmp_assign_source_37 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_find, &PyTuple_GET_ITEM( const_tuple_str_plain_traceback_tuple, 0 ) );

            if ( tmp_assign_source_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_traceback_e;
                var_traceback_e = tmp_assign_source_37;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( var_traceback_e );
            tmp_compexpr_left_2 = var_traceback_e;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_38;
                PyObject *tmp_iter_arg_4;
                PyObject *tmp_called_instance_8;
                CHECK_OBJECT( var_traceback_e );
                tmp_called_instance_8 = var_traceback_e;
                frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 153;
                tmp_iter_arg_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_getiterator, &PyTuple_GET_ITEM( const_tuple_str_plain_frame_tuple, 0 ) );

                if ( tmp_iter_arg_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                    goto try_except_handler_4;
                }
                tmp_assign_source_38 = MAKE_ITERATOR( tmp_iter_arg_4 );
                Py_DECREF( tmp_iter_arg_4 );
                if ( tmp_assign_source_38 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                    goto try_except_handler_4;
                }
                {
                    PyObject *old = tmp_for_loop_4__for_iterator;
                    tmp_for_loop_4__for_iterator = tmp_assign_source_38;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_4:;
            {
                PyObject *tmp_next_source_4;
                PyObject *tmp_assign_source_39;
                CHECK_OBJECT( tmp_for_loop_4__for_iterator );
                tmp_next_source_4 = tmp_for_loop_4__for_iterator;
                tmp_assign_source_39 = ITERATOR_NEXT( tmp_next_source_4 );
                if ( tmp_assign_source_39 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_4;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                        exception_lineno = 153;
                        goto try_except_handler_5;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_4__iter_value;
                    tmp_for_loop_4__iter_value = tmp_assign_source_39;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_40;
                CHECK_OBJECT( tmp_for_loop_4__iter_value );
                tmp_assign_source_40 = tmp_for_loop_4__iter_value;
                {
                    PyObject *old = var_frame_e;
                    var_frame_e = tmp_assign_source_40;
                    Py_INCREF( var_frame_e );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_name_25;
                PyObject *tmp_source_name_3;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_49;
                PyObject *tmp_source_name_4;
                CHECK_OBJECT( var_traceback );
                tmp_source_name_3 = var_traceback;
                tmp_called_name_25 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_append );
                if ( tmp_called_name_25 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                    goto try_except_handler_5;
                }
                CHECK_OBJECT( var_frame_e );
                tmp_source_name_4 = var_frame_e;
                tmp_args_element_name_49 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_text );
                if ( tmp_args_element_name_49 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_25 );

                    exception_lineno = 154;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                    goto try_except_handler_5;
                }
                frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 154;
                {
                    PyObject *call_args[] = { tmp_args_element_name_49 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, call_args );
                }

                Py_DECREF( tmp_called_name_25 );
                Py_DECREF( tmp_args_element_name_49 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 153;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_5;
            }
            goto loop_start_4;
            loop_end_4:;
            goto try_end_1;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_4__iter_value );
            tmp_for_loop_4__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
            Py_DECREF( tmp_for_loop_4__for_iterator );
            tmp_for_loop_4__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_1;
            exception_value = exception_keeper_value_1;
            exception_tb = exception_keeper_tb_1;
            exception_lineno = exception_keeper_lineno_1;

            goto try_except_handler_4;
            // End of try:
            try_end_1:;
            Py_XDECREF( tmp_for_loop_4__iter_value );
            tmp_for_loop_4__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
            Py_DECREF( tmp_for_loop_4__for_iterator );
            tmp_for_loop_4__for_iterator = NULL;

            branch_no_2:;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_len_arg_1;
            CHECK_OBJECT( var_traceback );
            tmp_len_arg_1 = var_traceback;
            tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_1 );
            assert( !(tmp_compexpr_left_3 == NULL) );
            tmp_compexpr_right_3 = const_int_0;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_41;
                tmp_assign_source_41 = Py_None;
                {
                    PyObject *old = var_traceback;
                    assert( old != NULL );
                    var_traceback = tmp_assign_source_41;
                    Py_INCREF( var_traceback );
                    Py_DECREF( old );
                }

            }
            branch_no_3:;
        }
        {
            PyObject *tmp_assign_source_42;
            PyObject *tmp_called_name_26;
            PyObject *tmp_mvar_value_26;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            PyObject *tmp_dict_key_4;
            PyObject *tmp_dict_value_4;
            PyObject *tmp_dict_key_5;
            PyObject *tmp_dict_value_5;
            PyObject *tmp_dict_key_6;
            PyObject *tmp_dict_value_6;
            PyObject *tmp_dict_key_7;
            PyObject *tmp_dict_value_7;
            PyObject *tmp_dict_key_8;
            PyObject *tmp_dict_value_8;
            PyObject *tmp_dict_key_9;
            PyObject *tmp_dict_value_9;
            PyObject *tmp_dict_key_10;
            PyObject *tmp_dict_value_10;
            PyObject *tmp_dict_key_11;
            PyObject *tmp_dict_value_11;
            PyObject *tmp_dict_key_12;
            PyObject *tmp_dict_value_12;
            PyObject *tmp_dict_key_13;
            PyObject *tmp_dict_value_13;
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_output );

            if (unlikely( tmp_mvar_value_26 == NULL ))
            {
                tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_output );
            }

            if ( tmp_mvar_value_26 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_output" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 157;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_26 = tmp_mvar_value_26;
            tmp_dict_key_1 = const_str_plain_output_type;
            CHECK_OBJECT( var_output_type );
            tmp_dict_value_1 = var_output_type;
            tmp_kw_name_1 = _PyDict_NewPresized( 13 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_2 = const_str_plain_output_png;
            CHECK_OBJECT( var_output_png );
            tmp_dict_value_2 = var_output_png;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_3 = const_str_plain_output_text;
            CHECK_OBJECT( var_output_text );
            tmp_dict_value_3 = var_output_text;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_4 = const_str_plain_output_svg;
            CHECK_OBJECT( var_output_svg );
            tmp_dict_value_4 = var_output_svg;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_5 = const_str_plain_output_html;
            CHECK_OBJECT( var_output_html );
            tmp_dict_value_5 = var_output_html;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_6 = const_str_plain_output_latex;
            CHECK_OBJECT( var_output_latex );
            tmp_dict_value_6 = var_output_latex;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_6, tmp_dict_value_6 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_7 = const_str_plain_output_json;
            CHECK_OBJECT( var_output_json );
            tmp_dict_value_7 = var_output_json;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_7, tmp_dict_value_7 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_8 = const_str_plain_output_javascript;
            CHECK_OBJECT( var_output_javascript );
            tmp_dict_value_8 = var_output_javascript;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_8, tmp_dict_value_8 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_9 = const_str_plain_output_jpeg;
            CHECK_OBJECT( var_output_jpeg );
            tmp_dict_value_9 = var_output_jpeg;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_9, tmp_dict_value_9 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_10 = const_str_plain_prompt_number;
            CHECK_OBJECT( var_out_prompt_number );
            tmp_dict_value_10 = var_out_prompt_number;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_10, tmp_dict_value_10 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_11 = const_str_plain_etype;
            CHECK_OBJECT( var_etype );
            tmp_dict_value_11 = var_etype;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_11, tmp_dict_value_11 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_12 = const_str_plain_evalue;
            CHECK_OBJECT( var_evalue );
            tmp_dict_value_12 = var_evalue;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_12, tmp_dict_value_12 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_13 = const_str_plain_traceback;
            CHECK_OBJECT( var_traceback );
            tmp_dict_value_13 = var_traceback;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_13, tmp_dict_value_13 );
            assert( !(tmp_res != 0) );
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 157;
            tmp_assign_source_42 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_26, tmp_kw_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_assign_source_42 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_output;
                var_output = tmp_assign_source_42;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_instance_9;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_50;
            CHECK_OBJECT( var_outputs );
            tmp_called_instance_9 = var_outputs;
            CHECK_OBJECT( var_output );
            tmp_args_element_name_50 = var_output;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 164;
            {
                PyObject *call_args[] = { tmp_args_element_name_50 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 164;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_4;
        }
        goto loop_start_3;
        loop_end_3:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_3__iter_value );
        tmp_for_loop_3__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
        Py_DECREF( tmp_for_loop_3__for_iterator );
        tmp_for_loop_3__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        try_end_2:;
        Py_XDECREF( tmp_for_loop_3__iter_value );
        tmp_for_loop_3__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
        Py_DECREF( tmp_for_loop_3__for_iterator );
        tmp_for_loop_3__for_iterator = NULL;

        {
            PyObject *tmp_assign_source_43;
            PyObject *tmp_called_name_27;
            PyObject *tmp_mvar_value_27;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_14;
            PyObject *tmp_dict_value_14;
            PyObject *tmp_dict_key_15;
            PyObject *tmp_dict_value_15;
            PyObject *tmp_dict_key_16;
            PyObject *tmp_dict_value_16;
            PyObject *tmp_dict_key_17;
            PyObject *tmp_dict_value_17;
            PyObject *tmp_dict_key_18;
            PyObject *tmp_dict_value_18;
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_code_cell );

            if (unlikely( tmp_mvar_value_27 == NULL ))
            {
                tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_code_cell );
            }

            if ( tmp_mvar_value_27 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_code_cell" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 165;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_27 = tmp_mvar_value_27;
            tmp_dict_key_14 = const_str_plain_input;
            CHECK_OBJECT( var_input );
            tmp_dict_value_14 = var_input;
            tmp_kw_name_2 = _PyDict_NewPresized( 5 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_14, tmp_dict_value_14 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_15 = const_str_plain_prompt_number;
            CHECK_OBJECT( var_prompt_number );
            tmp_dict_value_15 = var_prompt_number;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_15, tmp_dict_value_15 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_16 = const_str_plain_language;
            CHECK_OBJECT( var_language );
            tmp_dict_value_16 = var_language;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_16, tmp_dict_value_16 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_17 = const_str_plain_outputs;
            CHECK_OBJECT( var_outputs );
            tmp_dict_value_17 = var_outputs;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_17, tmp_dict_value_17 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_18 = const_str_plain_collapsed;
            CHECK_OBJECT( var_collapsed );
            tmp_dict_value_18 = var_collapsed;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_18, tmp_dict_value_18 );
            assert( !(tmp_res != 0) );
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 165;
            tmp_assign_source_43 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_27, tmp_kw_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_assign_source_43 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 165;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_cc;
                var_cc = tmp_assign_source_43;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_instance_10;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_51;
            CHECK_OBJECT( var_cells );
            tmp_called_instance_10 = var_cells;
            CHECK_OBJECT( var_cc );
            tmp_args_element_name_51 = var_cc;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 167;
            {
                PyObject *call_args[] = { tmp_args_element_name_51 };
                tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( var_cell_e );
        tmp_source_name_5 = var_cell_e;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_tag );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_compexpr_right_4 = const_str_plain_htmlcell;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_called_name_28;
            PyObject *tmp_mvar_value_28;
            PyObject *tmp_args_element_name_52;
            PyObject *tmp_args_element_name_53;
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_28 == NULL ))
            {
                tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_28 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 169;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_28 = tmp_mvar_value_28;
            CHECK_OBJECT( var_cell_e );
            tmp_args_element_name_52 = var_cell_e;
            tmp_args_element_name_53 = const_str_plain_source;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 169;
            {
                PyObject *call_args[] = { tmp_args_element_name_52, tmp_args_element_name_53 };
                tmp_assign_source_44 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_28, call_args );
            }

            if ( tmp_assign_source_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 169;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_source;
                var_source = tmp_assign_source_44;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_45;
            PyObject *tmp_called_name_29;
            PyObject *tmp_mvar_value_29;
            PyObject *tmp_args_element_name_54;
            PyObject *tmp_args_element_name_55;
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_29 == NULL ))
            {
                tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_29 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 170;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_29 = tmp_mvar_value_29;
            CHECK_OBJECT( var_cell_e );
            tmp_args_element_name_54 = var_cell_e;
            tmp_args_element_name_55 = const_str_plain_rendered;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 170;
            {
                PyObject *call_args[] = { tmp_args_element_name_54, tmp_args_element_name_55 };
                tmp_assign_source_45 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_29, call_args );
            }

            if ( tmp_assign_source_45 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 170;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_rendered;
                var_rendered = tmp_assign_source_45;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_30;
            PyObject *tmp_source_name_6;
            PyObject *tmp_call_result_5;
            PyObject *tmp_args_element_name_56;
            PyObject *tmp_called_name_31;
            PyObject *tmp_mvar_value_30;
            PyObject *tmp_args_name_1;
            PyObject *tmp_kw_name_3;
            PyObject *tmp_dict_key_19;
            PyObject *tmp_dict_value_19;
            PyObject *tmp_dict_key_20;
            PyObject *tmp_dict_value_20;
            CHECK_OBJECT( var_cells );
            tmp_source_name_6 = var_cells;
            tmp_called_name_30 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_append );
            if ( tmp_called_name_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_text_cell );

            if (unlikely( tmp_mvar_value_30 == NULL ))
            {
                tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_text_cell );
            }

            if ( tmp_mvar_value_30 == NULL )
            {
                Py_DECREF( tmp_called_name_30 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_text_cell" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 171;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_31 = tmp_mvar_value_30;
            tmp_args_name_1 = const_tuple_str_plain_html_tuple;
            tmp_dict_key_19 = const_str_plain_source;
            CHECK_OBJECT( var_source );
            tmp_dict_value_19 = var_source;
            tmp_kw_name_3 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_19, tmp_dict_value_19 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_20 = const_str_plain_rendered;
            CHECK_OBJECT( var_rendered );
            tmp_dict_value_20 = var_rendered;
            tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_20, tmp_dict_value_20 );
            assert( !(tmp_res != 0) );
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 171;
            tmp_args_element_name_56 = CALL_FUNCTION( tmp_called_name_31, tmp_args_name_1, tmp_kw_name_3 );
            Py_DECREF( tmp_kw_name_3 );
            if ( tmp_args_element_name_56 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_30 );

                exception_lineno = 171;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 171;
            {
                PyObject *call_args[] = { tmp_args_element_name_56 };
                tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_30, call_args );
            }

            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_args_element_name_56 );
            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_4:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( var_cell_e );
        tmp_source_name_7 = var_cell_e;
        tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_tag );
        if ( tmp_compexpr_left_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_compexpr_right_5 = const_str_plain_markdowncell;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        Py_DECREF( tmp_compexpr_left_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_called_name_32;
            PyObject *tmp_mvar_value_31;
            PyObject *tmp_args_element_name_57;
            PyObject *tmp_args_element_name_58;
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_31 == NULL ))
            {
                tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_31 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 173;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_32 = tmp_mvar_value_31;
            CHECK_OBJECT( var_cell_e );
            tmp_args_element_name_57 = var_cell_e;
            tmp_args_element_name_58 = const_str_plain_source;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 173;
            {
                PyObject *call_args[] = { tmp_args_element_name_57, tmp_args_element_name_58 };
                tmp_assign_source_46 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_32, call_args );
            }

            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 173;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_source;
                var_source = tmp_assign_source_46;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_47;
            PyObject *tmp_called_name_33;
            PyObject *tmp_mvar_value_32;
            PyObject *tmp_args_element_name_59;
            PyObject *tmp_args_element_name_60;
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text );

            if (unlikely( tmp_mvar_value_32 == NULL ))
            {
                tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_text );
            }

            if ( tmp_mvar_value_32 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_text" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 174;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_33 = tmp_mvar_value_32;
            CHECK_OBJECT( var_cell_e );
            tmp_args_element_name_59 = var_cell_e;
            tmp_args_element_name_60 = const_str_plain_rendered;
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 174;
            {
                PyObject *call_args[] = { tmp_args_element_name_59, tmp_args_element_name_60 };
                tmp_assign_source_47 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_33, call_args );
            }

            if ( tmp_assign_source_47 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_rendered;
                var_rendered = tmp_assign_source_47;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_34;
            PyObject *tmp_source_name_8;
            PyObject *tmp_call_result_6;
            PyObject *tmp_args_element_name_61;
            PyObject *tmp_called_name_35;
            PyObject *tmp_mvar_value_33;
            PyObject *tmp_args_name_2;
            PyObject *tmp_kw_name_4;
            PyObject *tmp_dict_key_21;
            PyObject *tmp_dict_value_21;
            PyObject *tmp_dict_key_22;
            PyObject *tmp_dict_value_22;
            CHECK_OBJECT( var_cells );
            tmp_source_name_8 = var_cells;
            tmp_called_name_34 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_append );
            if ( tmp_called_name_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_text_cell );

            if (unlikely( tmp_mvar_value_33 == NULL ))
            {
                tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_text_cell );
            }

            if ( tmp_mvar_value_33 == NULL )
            {
                Py_DECREF( tmp_called_name_34 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_text_cell" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 175;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_35 = tmp_mvar_value_33;
            tmp_args_name_2 = const_tuple_str_plain_markdown_tuple;
            tmp_dict_key_21 = const_str_plain_source;
            CHECK_OBJECT( var_source );
            tmp_dict_value_21 = var_source;
            tmp_kw_name_4 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_21, tmp_dict_value_21 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_22 = const_str_plain_rendered;
            CHECK_OBJECT( var_rendered );
            tmp_dict_value_22 = var_rendered;
            tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_22, tmp_dict_value_22 );
            assert( !(tmp_res != 0) );
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 175;
            tmp_args_element_name_61 = CALL_FUNCTION( tmp_called_name_35, tmp_args_name_2, tmp_kw_name_4 );
            Py_DECREF( tmp_kw_name_4 );
            if ( tmp_args_element_name_61 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_34 );

                exception_lineno = 175;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 175;
            {
                PyObject *call_args[] = { tmp_args_element_name_61 };
                tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, call_args );
            }

            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_args_element_name_61 );
            if ( tmp_call_result_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_6 );
        }
        branch_no_5:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 129;
        type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_called_name_36;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_kw_name_5;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_worksheet );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_worksheet );
        }

        if ( tmp_mvar_value_34 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_worksheet" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_36 = tmp_mvar_value_34;
        tmp_dict_key_23 = const_str_plain_name;
        CHECK_OBJECT( var_wsname );
        tmp_dict_value_23 = var_wsname;
        tmp_kw_name_5 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_23, tmp_dict_value_23 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_24 = const_str_plain_cells;
        CHECK_OBJECT( var_cells );
        tmp_dict_value_24 = var_cells;
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_24, tmp_dict_value_24 );
        assert( !(tmp_res != 0) );
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 176;
        tmp_assign_source_48 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_36, tmp_kw_name_5 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_ws;
            var_ws = tmp_assign_source_48;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_11;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_62;
        CHECK_OBJECT( var_worksheets );
        tmp_called_instance_11 = var_worksheets;
        CHECK_OBJECT( var_ws );
        tmp_args_element_name_62 = var_ws;
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 177;
        {
            PyObject *call_args[] = { tmp_args_element_name_62 };
            tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_11, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 126;
        type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_called_name_37;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_kw_name_6;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_metadata );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_metadata );
        }

        if ( tmp_mvar_value_35 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_metadata" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 179;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_37 = tmp_mvar_value_35;
        tmp_dict_key_25 = const_str_plain_name;
        CHECK_OBJECT( var_nbname );
        tmp_dict_value_25 = var_nbname;
        tmp_kw_name_6 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_6, tmp_dict_key_25, tmp_dict_value_25 );
        assert( !(tmp_res != 0) );
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 179;
        tmp_assign_source_49 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_37, tmp_kw_name_6 );
        Py_DECREF( tmp_kw_name_6 );
        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_md == NULL );
        var_md = tmp_assign_source_49;
    }
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_called_name_38;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_kw_name_7;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_notebook );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_notebook );
        }

        if ( tmp_mvar_value_36 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_notebook" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 180;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_38 = tmp_mvar_value_36;
        tmp_dict_key_26 = const_str_plain_metadata;
        CHECK_OBJECT( var_md );
        tmp_dict_value_26 = var_md;
        tmp_kw_name_7 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_26, tmp_dict_value_26 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_27 = const_str_plain_worksheets;
        CHECK_OBJECT( var_worksheets );
        tmp_dict_value_27 = var_worksheets;
        tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_27, tmp_dict_value_27 );
        assert( !(tmp_res != 0) );
        frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame.f_lineno = 180;
        tmp_assign_source_50 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_38, tmp_kw_name_7 );
        Py_DECREF( tmp_kw_name_7 );
        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_nb == NULL );
        var_nb = tmp_assign_source_50;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_69115cbb40d0e69d1113ff7a4e0b0717 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_69115cbb40d0e69d1113ff7a4e0b0717 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_69115cbb40d0e69d1113ff7a4e0b0717, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_69115cbb40d0e69d1113ff7a4e0b0717->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_69115cbb40d0e69d1113ff7a4e0b0717, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_69115cbb40d0e69d1113ff7a4e0b0717,
        type_description_1,
        par_self,
        par_root,
        par_kwargs,
        var_nbname,
        var_nbauthor,
        var_nbemail,
        var_nblicense,
        var_nbcreated,
        var_nbsaved,
        var_worksheets,
        var_ws_e,
        var_wsname,
        var_cells,
        var_cell_e,
        var_input,
        var_prompt_number,
        var_collapsed,
        var_language,
        var_outputs,
        var_output_e,
        var_output_type,
        var_output_text,
        var_output_png,
        var_output_jpeg,
        var_output_svg,
        var_output_html,
        var_output_latex,
        var_output_json,
        var_output_javascript,
        var_out_prompt_number,
        var_etype,
        var_evalue,
        var_traceback,
        var_traceback_e,
        var_frame_e,
        var_output,
        var_cc,
        var_source,
        var_rendered,
        var_ws,
        var_md,
        var_nb
    );


    // Release cached frame.
    if ( frame_69115cbb40d0e69d1113ff7a4e0b0717 == cache_frame_69115cbb40d0e69d1113ff7a4e0b0717 )
    {
        Py_DECREF( frame_69115cbb40d0e69d1113ff7a4e0b0717 );
    }
    cache_frame_69115cbb40d0e69d1113ff7a4e0b0717 = NULL;

    assertFrameObject( frame_69115cbb40d0e69d1113ff7a4e0b0717 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_nb );
    tmp_return_value = var_nb;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_11_to_notebook );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_root );
    Py_DECREF( par_root );
    par_root = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_nbname );
    Py_DECREF( var_nbname );
    var_nbname = NULL;

    CHECK_OBJECT( (PyObject *)var_nbauthor );
    Py_DECREF( var_nbauthor );
    var_nbauthor = NULL;

    CHECK_OBJECT( (PyObject *)var_nbemail );
    Py_DECREF( var_nbemail );
    var_nbemail = NULL;

    CHECK_OBJECT( (PyObject *)var_nblicense );
    Py_DECREF( var_nblicense );
    var_nblicense = NULL;

    CHECK_OBJECT( (PyObject *)var_nbcreated );
    Py_DECREF( var_nbcreated );
    var_nbcreated = NULL;

    CHECK_OBJECT( (PyObject *)var_nbsaved );
    Py_DECREF( var_nbsaved );
    var_nbsaved = NULL;

    CHECK_OBJECT( (PyObject *)var_worksheets );
    Py_DECREF( var_worksheets );
    var_worksheets = NULL;

    Py_XDECREF( var_ws_e );
    var_ws_e = NULL;

    Py_XDECREF( var_wsname );
    var_wsname = NULL;

    Py_XDECREF( var_cells );
    var_cells = NULL;

    Py_XDECREF( var_cell_e );
    var_cell_e = NULL;

    Py_XDECREF( var_input );
    var_input = NULL;

    Py_XDECREF( var_prompt_number );
    var_prompt_number = NULL;

    Py_XDECREF( var_collapsed );
    var_collapsed = NULL;

    Py_XDECREF( var_language );
    var_language = NULL;

    Py_XDECREF( var_outputs );
    var_outputs = NULL;

    Py_XDECREF( var_output_e );
    var_output_e = NULL;

    Py_XDECREF( var_output_type );
    var_output_type = NULL;

    Py_XDECREF( var_output_text );
    var_output_text = NULL;

    Py_XDECREF( var_output_png );
    var_output_png = NULL;

    Py_XDECREF( var_output_jpeg );
    var_output_jpeg = NULL;

    Py_XDECREF( var_output_svg );
    var_output_svg = NULL;

    Py_XDECREF( var_output_html );
    var_output_html = NULL;

    Py_XDECREF( var_output_latex );
    var_output_latex = NULL;

    Py_XDECREF( var_output_json );
    var_output_json = NULL;

    Py_XDECREF( var_output_javascript );
    var_output_javascript = NULL;

    Py_XDECREF( var_out_prompt_number );
    var_out_prompt_number = NULL;

    Py_XDECREF( var_etype );
    var_etype = NULL;

    Py_XDECREF( var_evalue );
    var_evalue = NULL;

    Py_XDECREF( var_traceback );
    var_traceback = NULL;

    Py_XDECREF( var_traceback_e );
    var_traceback_e = NULL;

    Py_XDECREF( var_frame_e );
    var_frame_e = NULL;

    Py_XDECREF( var_output );
    var_output = NULL;

    Py_XDECREF( var_cc );
    var_cc = NULL;

    Py_XDECREF( var_source );
    var_source = NULL;

    Py_XDECREF( var_rendered );
    var_rendered = NULL;

    Py_XDECREF( var_ws );
    var_ws = NULL;

    CHECK_OBJECT( (PyObject *)var_md );
    Py_DECREF( var_md );
    var_md = NULL;

    CHECK_OBJECT( (PyObject *)var_nb );
    Py_DECREF( var_nb );
    var_nb = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_root );
    Py_DECREF( par_root );
    par_root = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_nbname );
    var_nbname = NULL;

    Py_XDECREF( var_nbauthor );
    var_nbauthor = NULL;

    Py_XDECREF( var_nbemail );
    var_nbemail = NULL;

    Py_XDECREF( var_nblicense );
    var_nblicense = NULL;

    Py_XDECREF( var_nbcreated );
    var_nbcreated = NULL;

    Py_XDECREF( var_nbsaved );
    var_nbsaved = NULL;

    Py_XDECREF( var_worksheets );
    var_worksheets = NULL;

    Py_XDECREF( var_ws_e );
    var_ws_e = NULL;

    Py_XDECREF( var_wsname );
    var_wsname = NULL;

    Py_XDECREF( var_cells );
    var_cells = NULL;

    Py_XDECREF( var_cell_e );
    var_cell_e = NULL;

    Py_XDECREF( var_input );
    var_input = NULL;

    Py_XDECREF( var_prompt_number );
    var_prompt_number = NULL;

    Py_XDECREF( var_collapsed );
    var_collapsed = NULL;

    Py_XDECREF( var_language );
    var_language = NULL;

    Py_XDECREF( var_outputs );
    var_outputs = NULL;

    Py_XDECREF( var_output_e );
    var_output_e = NULL;

    Py_XDECREF( var_output_type );
    var_output_type = NULL;

    Py_XDECREF( var_output_text );
    var_output_text = NULL;

    Py_XDECREF( var_output_png );
    var_output_png = NULL;

    Py_XDECREF( var_output_jpeg );
    var_output_jpeg = NULL;

    Py_XDECREF( var_output_svg );
    var_output_svg = NULL;

    Py_XDECREF( var_output_html );
    var_output_html = NULL;

    Py_XDECREF( var_output_latex );
    var_output_latex = NULL;

    Py_XDECREF( var_output_json );
    var_output_json = NULL;

    Py_XDECREF( var_output_javascript );
    var_output_javascript = NULL;

    Py_XDECREF( var_out_prompt_number );
    var_out_prompt_number = NULL;

    Py_XDECREF( var_etype );
    var_etype = NULL;

    Py_XDECREF( var_evalue );
    var_evalue = NULL;

    Py_XDECREF( var_traceback );
    var_traceback = NULL;

    Py_XDECREF( var_traceback_e );
    var_traceback_e = NULL;

    Py_XDECREF( var_frame_e );
    var_frame_e = NULL;

    Py_XDECREF( var_output );
    var_output = NULL;

    Py_XDECREF( var_cc );
    var_cc = NULL;

    Py_XDECREF( var_source );
    var_source = NULL;

    Py_XDECREF( var_rendered );
    var_rendered = NULL;

    Py_XDECREF( var_ws );
    var_ws = NULL;

    Py_XDECREF( var_md );
    var_md = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml$$$function_11_to_notebook );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_10_reads(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_10_reads,
        const_str_plain_reads,
#if PYTHON_VERSION >= 300
        const_str_digest_1cd3c33c063c9b780c36910c2ee6412b,
#endif
        codeobj_48bb4b9f9b9d42e649de9aa4f098b0bf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_11_to_notebook(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_11_to_notebook,
        const_str_plain_to_notebook,
#if PYTHON_VERSION >= 300
        const_str_digest_84b8e2888e5228ada3b86d4538fa1742,
#endif
        codeobj_69115cbb40d0e69d1113ff7a4e0b0717,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_1_indent( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_1_indent,
        const_str_plain_indent,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_98eba8e5731ffd748477f5cb1779cce9,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_2__get_text(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_2__get_text,
        const_str_plain__get_text,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2316235d97a3f227b3496f5484712293,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_3__set_text(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_3__set_text,
        const_str_plain__set_text,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7f1719620fc5d294f47615805c83b4c9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_4__get_int(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_4__get_int,
        const_str_plain__get_int,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ef7a1aeda24d53e09bc43610296031c7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_5__set_int(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_5__set_int,
        const_str_plain__set_int,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0c3ddbca37aecc5bca70c65f8c9dcd42,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_6__get_bool(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_6__get_bool,
        const_str_plain__get_bool,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0945479eac88f74f06a10fb8ae89d096,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_7__set_bool(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_7__set_bool,
        const_str_plain__set_bool,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_903956b8cc7e6df6b43d2b911f702563,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_8__get_binary(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_8__get_binary,
        const_str_plain__get_binary,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f31596e16b641385310487f796070b0d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v2$nbxml$$$function_9__set_binary(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v2$nbxml$$$function_9__set_binary,
        const_str_plain__set_binary,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ed579f58491884418235415725222633,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v2$nbxml,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_nbformat$v2$nbxml =
{
    PyModuleDef_HEAD_INIT,
    "nbformat.v2.nbxml",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(nbformat$v2$nbxml)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(nbformat$v2$nbxml)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_nbformat$v2$nbxml );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("nbformat.v2.nbxml: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbformat.v2.nbxml: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbformat.v2.nbxml: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initnbformat$v2$nbxml" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_nbformat$v2$nbxml = Py_InitModule4(
        "nbformat.v2.nbxml",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_nbformat$v2$nbxml = PyModule_Create( &mdef_nbformat$v2$nbxml );
#endif

    moduledict_nbformat$v2$nbxml = MODULE_DICT( module_nbformat$v2$nbxml );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_nbformat$v2$nbxml,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_nbformat$v2$nbxml,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbformat$v2$nbxml,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbformat$v2$nbxml,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_nbformat$v2$nbxml );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_f87cf22b2ff8d81082b89ed72f489fb1, module_nbformat$v2$nbxml );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_469c6406a5c1f89ac02c6d0f96b06278;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_nbformat$v2$nbxml_109 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_da11a46d87c768e4104c2d50d40cf627_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_da11a46d87c768e4104c2d50d40cf627_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_3d48c52b0336b3281a48b8cc1d8f36ab;
        UPDATE_STRING_DICT0( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_469c6406a5c1f89ac02c6d0f96b06278 = MAKE_MODULE_FRAME( codeobj_469c6406a5c1f89ac02c6d0f96b06278, module_nbformat$v2$nbxml );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_469c6406a5c1f89ac02c6d0f96b06278 );
    assert( Py_REFCNT( frame_469c6406a5c1f89ac02c6d0f96b06278 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_base64;
        tmp_globals_name_1 = (PyObject *)moduledict_nbformat$v2$nbxml;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_encodestring_str_plain_decodestring_tuple;
        tmp_level_name_1 = const_int_0;
        frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame.f_lineno = 19;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_encodestring );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_encodestring, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_decodestring );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_decodestring, tmp_assign_source_6 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_warnings;
        tmp_globals_name_2 = (PyObject *)moduledict_nbformat$v2$nbxml;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame.f_lineno = 20;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_warnings, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_202eaa02524072004d16920aea9f34fd;
        tmp_globals_name_3 = (PyObject *)moduledict_nbformat$v2$nbxml;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_ElementTree_tuple;
        tmp_level_name_3 = const_int_0;
        frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame.f_lineno = 21;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_ElementTree );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_ET, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_34e3966f9679fde95e40f5931481245c;
        tmp_globals_name_4 = (PyObject *)moduledict_nbformat$v2$nbxml;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_unicode_type_tuple;
        tmp_level_name_4 = const_int_0;
        frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame.f_lineno = 23;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_unicode_type );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_unicode_type, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_rwbase;
        tmp_globals_name_5 = (PyObject *)moduledict_nbformat$v2$nbxml;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_NotebookReader_str_plain_NotebookWriter_tuple;
        tmp_level_name_5 = const_int_pos_1;
        frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame.f_lineno = 24;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_5 ) )
        {
           tmp_assign_source_11 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_5,
                (PyObject *)moduledict_nbformat$v2$nbxml,
                const_str_plain_NotebookReader,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_NotebookReader );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_NotebookReader, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_6 ) )
        {
           tmp_assign_source_12 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_6,
                (PyObject *)moduledict_nbformat$v2$nbxml,
                const_str_plain_NotebookWriter,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_NotebookWriter );
        }

        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_NotebookWriter, tmp_assign_source_12 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_nbbase;
        tmp_globals_name_6 = (PyObject *)moduledict_nbformat$v2$nbxml;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_fc383372e004d29aac698a72e7b64731_tuple;
        tmp_level_name_6 = const_int_pos_1;
        frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame.f_lineno = 25;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_13;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_7 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_7 ) )
        {
           tmp_assign_source_14 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_7,
                (PyObject *)moduledict_nbformat$v2$nbxml,
                const_str_plain_new_code_cell,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_new_code_cell );
        }

        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_code_cell, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_8 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_15 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_nbformat$v2$nbxml,
                const_str_plain_new_text_cell,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_new_text_cell );
        }

        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_text_cell, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_9 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_9 ) )
        {
           tmp_assign_source_16 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_9,
                (PyObject *)moduledict_nbformat$v2$nbxml,
                const_str_plain_new_worksheet,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_new_worksheet );
        }

        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_worksheet, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_10 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_10 ) )
        {
           tmp_assign_source_17 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_10,
                (PyObject *)moduledict_nbformat$v2$nbxml,
                const_str_plain_new_notebook,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_new_notebook );
        }

        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_notebook, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_11 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_11 ) )
        {
           tmp_assign_source_18 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_11,
                (PyObject *)moduledict_nbformat$v2$nbxml,
                const_str_plain_new_output,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_new_output );
        }

        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_output, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_12 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_12 ) )
        {
           tmp_assign_source_19 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_12,
                (PyObject *)moduledict_nbformat$v2$nbxml,
                const_str_plain_new_metadata,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_new_metadata );
        }

        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_new_metadata, tmp_assign_source_19 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_int_0_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_20 = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_1_indent( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_indent, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_2__get_text(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_text, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_3__set_text(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__set_text, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_4__get_int(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_int, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_5__set_int(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__set_int, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_6__get_bool(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_bool, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_7__set_bool(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__set_bool, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_8__get_binary(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__get_binary, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_9__set_binary(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__set_binary, tmp_assign_source_28 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_NotebookReader );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NotebookReader );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NotebookReader" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 109;

            goto try_except_handler_4;
        }

        tmp_tuple_element_1 = tmp_mvar_value_3;
        tmp_assign_source_29 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_29, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_29;
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_30 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_30;
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_4;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_4;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_4;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_4;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_32 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_32;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_4;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_4;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            tmp_tuple_element_2 = const_str_plain_XMLReader;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame.f_lineno = 109;
            tmp_assign_source_33 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_33;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;

                    goto try_except_handler_4;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 109;

                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;

                    goto try_except_handler_4;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 109;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_4;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_34;
            tmp_assign_source_34 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_34;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_35;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_nbformat$v2$nbxml_109 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_f87cf22b2ff8d81082b89ed72f489fb1;
        tmp_res = PyObject_SetItem( locals_nbformat$v2$nbxml_109, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_plain_XMLReader;
        tmp_res = PyObject_SetItem( locals_nbformat$v2$nbxml_109, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;

            goto try_except_handler_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_da11a46d87c768e4104c2d50d40cf627_2, codeobj_da11a46d87c768e4104c2d50d40cf627, module_nbformat$v2$nbxml, sizeof(void *) );
        frame_da11a46d87c768e4104c2d50d40cf627_2 = cache_frame_da11a46d87c768e4104c2d50d40cf627_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_da11a46d87c768e4104c2d50d40cf627_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_da11a46d87c768e4104c2d50d40cf627_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_10_reads(  );



        tmp_res = PyObject_SetItem( locals_nbformat$v2$nbxml_109, const_str_plain_reads, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbformat$v2$nbxml$$$function_11_to_notebook(  );



        tmp_res = PyObject_SetItem( locals_nbformat$v2$nbxml_109, const_str_plain_to_notebook, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_da11a46d87c768e4104c2d50d40cf627_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_da11a46d87c768e4104c2d50d40cf627_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_da11a46d87c768e4104c2d50d40cf627_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_da11a46d87c768e4104c2d50d40cf627_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_da11a46d87c768e4104c2d50d40cf627_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_da11a46d87c768e4104c2d50d40cf627_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_da11a46d87c768e4104c2d50d40cf627_2 == cache_frame_da11a46d87c768e4104c2d50d40cf627_2 )
        {
            Py_DECREF( frame_da11a46d87c768e4104c2d50d40cf627_2 );
        }
        cache_frame_da11a46d87c768e4104c2d50d40cf627_2 = NULL;

        assertFrameObject( frame_da11a46d87c768e4104c2d50d40cf627_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_6;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_6;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbformat$v2$nbxml_109, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_6;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_XMLReader;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_nbformat$v2$nbxml_109;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame.f_lineno = 109;
            tmp_assign_source_36 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_6;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_36;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_35 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_35 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        Py_DECREF( locals_nbformat$v2$nbxml_109 );
        locals_nbformat$v2$nbxml_109 = NULL;
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbformat$v2$nbxml_109 );
        locals_nbformat$v2$nbxml_109 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbformat$v2$nbxml );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 109;
        goto try_except_handler_4;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_XMLReader, tmp_assign_source_35 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_XMLReader );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XMLReader );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "XMLReader" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 184;

            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame.f_lineno = 184;
        tmp_assign_source_37 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__reader, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__reader );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__reader );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_source_name_5 = tmp_mvar_value_5;
        tmp_assign_source_38 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_reads );
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_reads, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__reader );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__reader );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_reader" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 187;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_6;
        tmp_assign_source_39 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_read );
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_read, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain__reader );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__reader );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_reader" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 188;

            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_7;
        tmp_assign_source_40 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_to_notebook );
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v2$nbxml, (Nuitka_StringObject *)const_str_plain_to_notebook, tmp_assign_source_40 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_469c6406a5c1f89ac02c6d0f96b06278 );
#endif
    popFrameStack();

    assertFrameObject( frame_469c6406a5c1f89ac02c6d0f96b06278 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_469c6406a5c1f89ac02c6d0f96b06278 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_469c6406a5c1f89ac02c6d0f96b06278, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_469c6406a5c1f89ac02c6d0f96b06278->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_469c6406a5c1f89ac02c6d0f96b06278, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_nbformat$v2$nbxml );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
