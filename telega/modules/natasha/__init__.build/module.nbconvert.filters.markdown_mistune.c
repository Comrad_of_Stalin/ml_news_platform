/* Generated code for Python module 'nbconvert.filters.markdown_mistune'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_nbconvert$filters$markdown_mistune" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_nbconvert$filters$markdown_mistune;
PyDictObject *moduledict_nbconvert$filters$markdown_mistune;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_html_escape;
extern PyObject *const_str_plain_Renderer;
static PyObject *const_str_digest_ff0e58051005823f108468304e3f7ff8;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_e853791bb8091160b944c96a7db493d5_tuple;
extern PyObject *const_tuple_str_plain_self_str_plain_text_tuple;
extern PyObject *const_dict_4127e6c33f0203f58d13110adfa0e8b6;
static PyObject *const_str_digest_9176c6761cabb67f1001474fcf51e369;
extern PyObject *const_str_plain___name__;
static PyObject *const_list_str_plain_multiline_math_list;
static PyObject *const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_HtmlFormatter_tuple;
extern PyObject *const_str_plain_group;
extern PyObject *const_str_plain_highlight;
static PyObject *const_str_digest_ff4b6d762ff39011de49b741bfbdc2e5;
extern PyObject *const_str_plain_markdown2html_mistune;
static PyObject *const_str_digest_7639c1606d5b8370bc20bdcf813ecfb7;
extern PyObject *const_int_pos_11;
static PyObject *const_str_digest_f6297e3ecd25b13a4cc416cf99997112;
static PyObject *const_str_digest_70c9477f6cb4d3ada773db3e0bec008c;
extern PyObject *const_tuple_str_plain_escape_tuple;
static PyObject *const_str_digest_038a377797a688052a99e70c40fa02f4;
static PyObject *const_tuple_str_plain_add_anchor_tuple;
extern PyObject *const_str_plain_image;
static PyObject *const_str_plain_output_multiline_math;
extern PyObject *const_str_plain_m;
extern PyObject *const_tuple_str_plain_partial_tuple;
extern PyObject *const_str_plain_None;
static PyObject *const_tuple_21fc239cdac89a817d0b8ee2345062f9_tuple;
extern PyObject *const_str_plain_default_rules;
static PyObject *const_str_digest_e42f92694dce90d8d2e43e2b68bc6035;
static PyObject *const_str_digest_fa84e331e4a03a1a2a772eae49a52bae;
extern PyObject *const_str_plain_block_code;
static PyObject *const_str_plain_latex_environment;
extern PyObject *const_str_digest_62fc664646c5b4f462aaa7001dcc03a6;
extern PyObject *const_str_plain_join;
static PyObject *const_str_plain_escape_html;
extern PyObject *const_tuple_int_pos_1_tuple;
static PyObject *const_str_digest_10e290088cf6ad49dc621f6739f40407;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_digest_9a541cdf94e22855513e5462b2983775;
extern PyObject *const_str_digest_ce1b0803b9e228b4fef82f896164bef7;
extern PyObject *const_str_plain_block;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_digest_70f70e2fc470a2c13c317c77e2540e02;
static PyObject *const_tuple_e1124bd2bcdf0763b36a418c79ac459b_tuple;
extern PyObject *const_str_plain_data;
extern PyObject *const_str_plain_options;
extern PyObject *const_str_plain_quote;
extern PyObject *const_str_plain_render;
static PyObject *const_tuple_str_plain_anchor_link_text_str_chr_182_tuple;
static PyObject *const_str_plain_block_math;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_plain_output_latex_environment;
extern PyObject *const_str_plain_anchor_link_text;
static PyObject *const_str_digest_5508e7b0ee7f27dd0043819f00e7c12e;
extern PyObject *const_str_digest_76b2174b385d7250ca7574c6411a6fc7;
extern PyObject *const_str_digest_da85ef92a56e55578ce4744acd08408a;
static PyObject *const_str_plain_cgi;
extern PyObject *const_str_plain_mistune;
extern PyObject *const_str_plain_src;
static PyObject *const_str_plain_MathBlockGrammar;
extern PyObject *const_str_digest_e58eec2a5ea585c2c4db40f39721d451;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_str_digest_896a50bc98f51828cb4c4b89ca23ce3e;
static PyObject *const_str_digest_768877b5e7da42f81f1a9274ab505df8;
static PyObject *const_tuple_str_digest_e42f92694dce90d8d2e43e2b68bc6035_tuple;
static PyObject *const_str_digest_eecd2b46c1f0e70774ded143ca51617c;
static PyObject *const_str_plain_inline_math;
extern PyObject *const_str_digest_ac502099f2abb6fd9f3fb9c9824906b3;
extern PyObject *const_str_plain_keys;
static PyObject *const_str_digest_2b5a4fa91b35a0840f1e7159057ed9d0;
extern PyObject *const_tuple_str_plain_highlight_tuple;
extern PyObject *const_str_plain_tokens;
static PyObject *const_list_10e6d592a9132363948ffc7cfaafc909_list;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_rules;
extern PyObject *const_str_plain_BlockGrammar;
extern PyObject *const_str_plain_append;
static PyObject *const_str_digest_ef5aab8151cdd2f21b01cbe26730c413;
static PyObject *const_str_digest_218c7a49ffb251f99b37f39dbd8b4537;
extern PyObject *const_str_plain_title;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_raw;
static PyObject *const_str_digest_a1fe045d3e680ae945c1ff74d68fb820;
static PyObject *const_str_plain_output_inline_math;
static PyObject *const_str_plain_output_block_math;
static PyObject *const_str_plain_parse_multiline_math;
static PyObject *const_str_digest_c7a8542447e4f945d1c20a13bff26fc2;
extern PyObject *const_str_plain_IPythonRenderer;
extern PyObject *const_str_plain_compile;
static PyObject *const_str_digest_fddf68e6c71bef586b98da046f2d1410;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_token;
static PyObject *const_str_digest_6f1742d33f705650262a93f5521fdc80;
static PyObject *const_list_7ef32ee98da72c49c75f9beb2c938610_list;
static PyObject *const_str_digest_df9fa7089fd2a8140575d2b5fb4183da;
extern PyObject *const_str_plain_Markdown;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_inline;
extern PyObject *const_str_plain_pygments;
extern PyObject *const_str_digest_0524aa7879841d3ca8f600d345f30f99;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_plain_preferred_mime_type;
static PyObject *const_slice_int_pos_11_none_none;
extern PyObject *const_str_plain_attachment;
extern PyObject *const_str_plain_lang;
static PyObject *const_str_plain_multiline_math;
extern PyObject *const_str_plain_renderer;
static PyObject *const_dict_f0003172cf42aa13871e07dd4396acc8;
extern PyObject *const_str_plain_lexer;
extern PyObject *const_int_0;
static PyObject *const_tuple_str_digest_ca49b59e4b6e238fe9964d441afd018b_tuple;
extern PyObject *const_str_plain_code;
static PyObject *const_str_plain_multi_math_str;
extern PyObject *const_str_chr_182;
extern PyObject *const_tuple_str_plain_get_lexer_by_name_tuple;
extern PyObject *const_str_plain_BlockLexer;
static PyObject *const_str_digest_2a0f2b91133328f91d741d4c687d6005;
extern PyObject *const_tuple_int_pos_2_tuple;
extern PyObject *const_str_plain_text;
static PyObject *const_str_digest_3859a4497c34c2ec614a79f5d78001fe;
extern PyObject *const_str_plain_MarkdownWithMath;
extern PyObject *const_str_plain_ClassNotFound;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_HtmlFormatter;
extern PyObject *const_str_plain_html;
extern PyObject *const_str_plain_InlineGrammar;
static PyObject *const_str_digest_4536f123e7c367dea19c325aba794559;
static PyObject *const_tuple_5bad021fbdcdef8a1c8f24c391390893_tuple;
extern PyObject *const_str_plain_header;
extern PyObject *const_tuple_str_plain_self_str_plain_m_tuple;
extern PyObject *const_tuple_str_plain_ClassNotFound_tuple;
extern PyObject *const_str_plain_type;
static PyObject *const_str_digest_4fd35e0644fa85b2e9419fda3117b018;
static PyObject *const_str_digest_524ef2db848d8af58cf0d46ca6216c8b;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain_MathInlineLexer;
extern PyObject *const_str_plain_print_function;
extern PyObject *const_str_plain___class__;
static PyObject *const_dict_d4840cccbd3dc923b8cb53333db50eda;
extern PyObject *const_str_chr_124;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_source;
extern PyObject *const_str_plain_functools;
static PyObject *const_str_plain_MathBlockLexer;
static PyObject *const_list_a78b385ba4e09462bd434d905623c3c4_list;
static PyObject *const_str_digest_610b22787e9990afc810a8c228534b90;
extern PyObject *const_tuple_str_plain_source_tuple;
extern PyObject *const_str_plain_partial;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_mime_type;
static PyObject *const_str_plain_attachment_prefix;
extern PyObject *const_str_digest_82da718018898901a2bd1c8219eb7ef2;
extern PyObject *const_str_plain_stripall;
extern PyObject *const_str_plain_add_anchor;
static PyObject *const_str_digest_1ef2097e6be2b5889e6a1c84bec73181;
static PyObject *const_str_plain_preferred_mime_types;
extern PyObject *const_str_plain_formatter;
extern PyObject *const_str_digest_bc2c8ffc848309b89316fe6d6faf27c6;
extern PyObject *const_str_newline;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_level;
extern PyObject *const_str_plain_self;
static PyObject *const_str_digest_c35d5fbb9af86cc85bd911e41cddbbc0;
static PyObject *const_str_digest_1ea263c0bb7bc5648dd0b66993cc0bb4;
static PyObject *const_str_digest_ee3d8a1be3441fe200176fa93a9ebafd;
static PyObject *const_tuple_str_plain_self_str_plain_name_str_plain_text_tuple;
static PyObject *const_str_digest_840f3aad4365225677122d88094b4aac;
extern PyObject *const_str_plain_InlineLexer;
extern PyObject *const_str_plain_DOTALL;
static PyObject *const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_attachments;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_757d304647920670206b18097f89b560;
extern PyObject *const_int_pos_2;
static PyObject *const_str_digest_ca49b59e4b6e238fe9964d441afd018b;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_startswith;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_get_lexer_by_name;
static PyObject *const_str_plain_MathInlineGrammar;
extern PyObject *const_str_plain_escape;
extern PyObject *const_str_digest_962b170f60f6eb52f0c210e4bcd8c1dc;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_ff0e58051005823f108468304e3f7ff8 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753290 ], 16, 0 );
    const_tuple_e853791bb8091160b944c96a7db493d5_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_e853791bb8091160b944c96a7db493d5_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_e853791bb8091160b944c96a7db493d5_tuple, 1, const_str_plain_renderer ); Py_INCREF( const_str_plain_renderer );
    PyTuple_SET_ITEM( const_tuple_e853791bb8091160b944c96a7db493d5_tuple, 2, const_str_plain_rules ); Py_INCREF( const_str_plain_rules );
    PyTuple_SET_ITEM( const_tuple_e853791bb8091160b944c96a7db493d5_tuple, 3, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_e853791bb8091160b944c96a7db493d5_tuple, 4, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_9176c6761cabb67f1001474fcf51e369 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753306 ], 168, 0 );
    const_list_str_plain_multiline_math_list = PyList_New( 1 );
    const_str_plain_multiline_math = UNSTREAM_STRING_ASCII( &constant_bin[ 2753474 ], 14, 1 );
    PyList_SET_ITEM( const_list_str_plain_multiline_math_list, 0, const_str_plain_multiline_math ); Py_INCREF( const_str_plain_multiline_math );
    const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple = PyTuple_New( 13 );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 1, const_str_plain_src ); Py_INCREF( const_str_plain_src );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 2, const_str_plain_title ); Py_INCREF( const_str_plain_title );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 3, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 4, const_str_plain_attachments ); Py_INCREF( const_str_plain_attachments );
    const_str_plain_attachment_prefix = UNSTREAM_STRING_ASCII( &constant_bin[ 2753488 ], 17, 1 );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 5, const_str_plain_attachment_prefix ); Py_INCREF( const_str_plain_attachment_prefix );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 6, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 7, const_str_plain_attachment ); Py_INCREF( const_str_plain_attachment );
    const_str_plain_preferred_mime_types = UNSTREAM_STRING_ASCII( &constant_bin[ 2753505 ], 20, 1 );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 8, const_str_plain_preferred_mime_types ); Py_INCREF( const_str_plain_preferred_mime_types );
    const_str_plain_preferred_mime_type = UNSTREAM_STRING_ASCII( &constant_bin[ 2753505 ], 19, 1 );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 9, const_str_plain_preferred_mime_type ); Py_INCREF( const_str_plain_preferred_mime_type );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 10, const_str_plain_mime_type ); Py_INCREF( const_str_plain_mime_type );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 11, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 12, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_ff4b6d762ff39011de49b741bfbdc2e5 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753525 ], 27, 0 );
    const_str_digest_7639c1606d5b8370bc20bdcf813ecfb7 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753552 ], 40, 0 );
    const_str_digest_f6297e3ecd25b13a4cc416cf99997112 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753592 ], 26, 0 );
    const_str_digest_70c9477f6cb4d3ada773db3e0bec008c = UNSTREAM_STRING_ASCII( &constant_bin[ 2753618 ], 43, 0 );
    const_str_digest_038a377797a688052a99e70c40fa02f4 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753661 ], 37, 0 );
    const_tuple_str_plain_add_anchor_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_add_anchor_tuple, 0, const_str_plain_add_anchor ); Py_INCREF( const_str_plain_add_anchor );
    const_str_plain_output_multiline_math = UNSTREAM_STRING_ASCII( &constant_bin[ 2753698 ], 21, 1 );
    const_tuple_21fc239cdac89a817d0b8ee2345062f9_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_21fc239cdac89a817d0b8ee2345062f9_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_21fc239cdac89a817d0b8ee2345062f9_tuple, 1, const_str_plain_renderer ); Py_INCREF( const_str_plain_renderer );
    PyTuple_SET_ITEM( const_tuple_21fc239cdac89a817d0b8ee2345062f9_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_21fc239cdac89a817d0b8ee2345062f9_tuple, 3, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_e42f92694dce90d8d2e43e2b68bc6035 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753719 ], 11, 0 );
    const_str_digest_fa84e331e4a03a1a2a772eae49a52bae = UNSTREAM_STRING_ASCII( &constant_bin[ 2753730 ], 20, 0 );
    const_str_plain_latex_environment = UNSTREAM_STRING_ASCII( &constant_bin[ 2753750 ], 17, 1 );
    const_str_plain_escape_html = UNSTREAM_STRING_ASCII( &constant_bin[ 2753541 ], 11, 1 );
    const_str_digest_10e290088cf6ad49dc621f6739f40407 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753767 ], 47, 0 );
    const_str_digest_9a541cdf94e22855513e5462b2983775 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753814 ], 29, 0 );
    const_str_digest_70f70e2fc470a2c13c317c77e2540e02 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753843 ], 21, 0 );
    const_tuple_e1124bd2bcdf0763b36a418c79ac459b_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_e1124bd2bcdf0763b36a418c79ac459b_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_e1124bd2bcdf0763b36a418c79ac459b_tuple, 1, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_e1124bd2bcdf0763b36a418c79ac459b_tuple, 2, const_str_plain_lang ); Py_INCREF( const_str_plain_lang );
    PyTuple_SET_ITEM( const_tuple_e1124bd2bcdf0763b36a418c79ac459b_tuple, 3, const_str_plain_lexer ); Py_INCREF( const_str_plain_lexer );
    PyTuple_SET_ITEM( const_tuple_e1124bd2bcdf0763b36a418c79ac459b_tuple, 4, const_str_plain_formatter ); Py_INCREF( const_str_plain_formatter );
    const_tuple_str_plain_anchor_link_text_str_chr_182_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_anchor_link_text_str_chr_182_tuple, 0, const_str_plain_anchor_link_text ); Py_INCREF( const_str_plain_anchor_link_text );
    PyTuple_SET_ITEM( const_tuple_str_plain_anchor_link_text_str_chr_182_tuple, 1, const_str_chr_182 ); Py_INCREF( const_str_chr_182 );
    const_str_plain_block_math = UNSTREAM_STRING_ASCII( &constant_bin[ 2753608 ], 10, 1 );
    const_str_plain_output_latex_environment = UNSTREAM_STRING_ASCII( &constant_bin[ 2753864 ], 24, 1 );
    const_str_digest_5508e7b0ee7f27dd0043819f00e7c12e = UNSTREAM_STRING_ASCII( &constant_bin[ 1827503 ], 8, 0 );
    const_str_plain_cgi = UNSTREAM_STRING_ASCII( &constant_bin[ 644794 ], 3, 1 );
    const_str_plain_MathBlockGrammar = UNSTREAM_STRING_ASCII( &constant_bin[ 2753888 ], 16, 1 );
    const_str_digest_896a50bc98f51828cb4c4b89ca23ce3e = UNSTREAM_STRING_ASCII( &constant_bin[ 2753904 ], 26, 0 );
    const_str_digest_768877b5e7da42f81f1a9274ab505df8 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753930 ], 22, 0 );
    const_tuple_str_digest_e42f92694dce90d8d2e43e2b68bc6035_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_e42f92694dce90d8d2e43e2b68bc6035_tuple, 0, const_str_digest_e42f92694dce90d8d2e43e2b68bc6035 ); Py_INCREF( const_str_digest_e42f92694dce90d8d2e43e2b68bc6035 );
    const_str_digest_eecd2b46c1f0e70774ded143ca51617c = UNSTREAM_STRING_ASCII( &constant_bin[ 2753952 ], 33, 0 );
    const_str_plain_inline_math = UNSTREAM_STRING_ASCII( &constant_bin[ 2753985 ], 11, 1 );
    const_str_digest_2b5a4fa91b35a0840f1e7159057ed9d0 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753996 ], 40, 0 );
    const_list_10e6d592a9132363948ffc7cfaafc909_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_10e6d592a9132363948ffc7cfaafc909_list, 0, const_str_plain_block_math ); Py_INCREF( const_str_plain_block_math );
    PyList_SET_ITEM( const_list_10e6d592a9132363948ffc7cfaafc909_list, 1, const_str_plain_inline_math ); Py_INCREF( const_str_plain_inline_math );
    PyList_SET_ITEM( const_list_10e6d592a9132363948ffc7cfaafc909_list, 2, const_str_plain_latex_environment ); Py_INCREF( const_str_plain_latex_environment );
    const_str_digest_ef5aab8151cdd2f21b01cbe26730c413 = UNSTREAM_STRING_ASCII( &constant_bin[ 2754036 ], 24, 0 );
    const_str_digest_218c7a49ffb251f99b37f39dbd8b4537 = UNSTREAM_STRING_ASCII( &constant_bin[ 2754060 ], 34, 0 );
    const_str_digest_a1fe045d3e680ae945c1ff74d68fb820 = UNSTREAM_STRING_ASCII( &constant_bin[ 14571 ], 5, 0 );
    const_str_plain_output_inline_math = UNSTREAM_STRING_ASCII( &constant_bin[ 2754076 ], 18, 1 );
    const_str_plain_output_block_math = UNSTREAM_STRING_ASCII( &constant_bin[ 2754094 ], 17, 1 );
    const_str_plain_parse_multiline_math = UNSTREAM_STRING_ASCII( &constant_bin[ 2754111 ], 20, 1 );
    const_str_digest_c7a8542447e4f945d1c20a13bff26fc2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2754131 ], 27, 0 );
    const_str_digest_fddf68e6c71bef586b98da046f2d1410 = UNSTREAM_STRING_ASCII( &constant_bin[ 2754158 ], 408, 0 );
    const_str_digest_6f1742d33f705650262a93f5521fdc80 = UNSTREAM_STRING_ASCII( &constant_bin[ 2754566 ], 38, 0 );
    const_list_7ef32ee98da72c49c75f9beb2c938610_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_7ef32ee98da72c49c75f9beb2c938610_list, 0, const_str_digest_82da718018898901a2bd1c8219eb7ef2 ); Py_INCREF( const_str_digest_82da718018898901a2bd1c8219eb7ef2 );
    PyList_SET_ITEM( const_list_7ef32ee98da72c49c75f9beb2c938610_list, 1, const_str_digest_62fc664646c5b4f462aaa7001dcc03a6 ); Py_INCREF( const_str_digest_62fc664646c5b4f462aaa7001dcc03a6 );
    PyList_SET_ITEM( const_list_7ef32ee98da72c49c75f9beb2c938610_list, 2, const_str_digest_e58eec2a5ea585c2c4db40f39721d451 ); Py_INCREF( const_str_digest_e58eec2a5ea585c2c4db40f39721d451 );
    const_str_digest_df9fa7089fd2a8140575d2b5fb4183da = UNSTREAM_STRING_ASCII( &constant_bin[ 2754604 ], 6, 0 );
    const_slice_int_pos_11_none_none = PySlice_New( const_int_pos_11, Py_None, Py_None );
    const_dict_f0003172cf42aa13871e07dd4396acc8 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_f0003172cf42aa13871e07dd4396acc8, const_str_plain_quote, Py_False );
    assert( PyDict_Size( const_dict_f0003172cf42aa13871e07dd4396acc8 ) == 1 );
    const_tuple_str_digest_ca49b59e4b6e238fe9964d441afd018b_tuple = PyTuple_New( 1 );
    const_str_digest_ca49b59e4b6e238fe9964d441afd018b = UNSTREAM_STRING_ASCII( &constant_bin[ 2754610 ], 46, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_ca49b59e4b6e238fe9964d441afd018b_tuple, 0, const_str_digest_ca49b59e4b6e238fe9964d441afd018b ); Py_INCREF( const_str_digest_ca49b59e4b6e238fe9964d441afd018b );
    const_str_plain_multi_math_str = UNSTREAM_STRING_ASCII( &constant_bin[ 2754656 ], 14, 1 );
    const_str_digest_2a0f2b91133328f91d741d4c687d6005 = UNSTREAM_STRING_ASCII( &constant_bin[ 2754670 ], 53, 0 );
    const_str_digest_3859a4497c34c2ec614a79f5d78001fe = UNSTREAM_STRING_ASCII( &constant_bin[ 2754723 ], 25, 0 );
    const_str_digest_4536f123e7c367dea19c325aba794559 = UNSTREAM_STRING_ASCII( &constant_bin[ 2754748 ], 155, 0 );
    const_tuple_5bad021fbdcdef8a1c8f24c391390893_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_5bad021fbdcdef8a1c8f24c391390893_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_5bad021fbdcdef8a1c8f24c391390893_tuple, 1, const_str_plain_rules ); Py_INCREF( const_str_plain_rules );
    PyTuple_SET_ITEM( const_tuple_5bad021fbdcdef8a1c8f24c391390893_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_5bad021fbdcdef8a1c8f24c391390893_tuple, 3, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_4fd35e0644fa85b2e9419fda3117b018 = UNSTREAM_STRING_ASCII( &constant_bin[ 2754903 ], 39, 0 );
    const_str_digest_524ef2db848d8af58cf0d46ca6216c8b = UNSTREAM_STRING_ASCII( &constant_bin[ 2754942 ], 149, 0 );
    const_str_plain_MathInlineLexer = UNSTREAM_STRING_ASCII( &constant_bin[ 2753996 ], 15, 1 );
    const_dict_d4840cccbd3dc923b8cb53333db50eda = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_d4840cccbd3dc923b8cb53333db50eda, const_str_plain_escape, Py_False );
    assert( PyDict_Size( const_dict_d4840cccbd3dc923b8cb53333db50eda ) == 1 );
    const_str_plain_MathBlockLexer = UNSTREAM_STRING_ASCII( &constant_bin[ 2753454 ], 14, 1 );
    const_list_a78b385ba4e09462bd434d905623c3c4_list = PyList_New( 3 );
    const_str_digest_c35d5fbb9af86cc85bd911e41cddbbc0 = UNSTREAM_STRING_ASCII( &constant_bin[ 2755091 ], 12, 0 );
    PyList_SET_ITEM( const_list_a78b385ba4e09462bd434d905623c3c4_list, 0, const_str_digest_c35d5fbb9af86cc85bd911e41cddbbc0 ); Py_INCREF( const_str_digest_c35d5fbb9af86cc85bd911e41cddbbc0 );
    PyList_SET_ITEM( const_list_a78b385ba4e09462bd434d905623c3c4_list, 1, const_str_digest_ff0e58051005823f108468304e3f7ff8 ); Py_INCREF( const_str_digest_ff0e58051005823f108468304e3f7ff8 );
    PyList_SET_ITEM( const_list_a78b385ba4e09462bd434d905623c3c4_list, 2, const_str_digest_4fd35e0644fa85b2e9419fda3117b018 ); Py_INCREF( const_str_digest_4fd35e0644fa85b2e9419fda3117b018 );
    const_str_digest_610b22787e9990afc810a8c228534b90 = UNSTREAM_STRING_ASCII( &constant_bin[ 2755103 ], 23, 0 );
    const_str_digest_1ef2097e6be2b5889e6a1c84bec73181 = UNSTREAM_STRING_ASCII( &constant_bin[ 2755126 ], 33, 0 );
    const_str_digest_1ea263c0bb7bc5648dd0b66993cc0bb4 = UNSTREAM_STRING_ASCII( &constant_bin[ 2755159 ], 28, 0 );
    const_str_digest_ee3d8a1be3441fe200176fa93a9ebafd = UNSTREAM_STRING_ASCII( &constant_bin[ 2755187 ], 33, 0 );
    const_tuple_str_plain_self_str_plain_name_str_plain_text_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_name_str_plain_text_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_name_str_plain_text_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_name_str_plain_text_tuple, 2, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    const_str_digest_840f3aad4365225677122d88094b4aac = UNSTREAM_STRING_ASCII( &constant_bin[ 2755220 ], 22, 0 );
    const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple, 2, const_str_plain_level ); Py_INCREF( const_str_plain_level );
    PyTuple_SET_ITEM( const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple, 3, const_str_plain_raw ); Py_INCREF( const_str_plain_raw );
    PyTuple_SET_ITEM( const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple, 4, const_str_plain_html ); Py_INCREF( const_str_plain_html );
    PyTuple_SET_ITEM( const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple, 5, const_str_plain_anchor_link_text ); Py_INCREF( const_str_plain_anchor_link_text );
    PyTuple_SET_ITEM( const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple, 6, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_757d304647920670206b18097f89b560 = UNSTREAM_STRING_ASCII( &constant_bin[ 2755242 ], 35, 0 );
    const_str_plain_MathInlineGrammar = UNSTREAM_STRING_ASCII( &constant_bin[ 2754250 ], 17, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_nbconvert$filters$markdown_mistune( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_f905ee16b1b38b38016191f300fb5513;
static PyCodeObject *codeobj_6c9df3004a4f945b2a5cc5e4db74db3c;
static PyCodeObject *codeobj_8e0c5d8fc4d243989626f7e72a0f84ee;
static PyCodeObject *codeobj_cfb9bfbe17146d73e81d3090ab9d1844;
static PyCodeObject *codeobj_ef211c706c48a3d293c22d394c51d720;
static PyCodeObject *codeobj_4f053877595d1f3290140afa99cb4656;
static PyCodeObject *codeobj_7fdb20c7499f4b745ba4af2c59bcdbb0;
static PyCodeObject *codeobj_be97c2567b421c8ec3c78efe734539ec;
static PyCodeObject *codeobj_09ca13a5cc6332980b542bce4492de44;
static PyCodeObject *codeobj_8ca5e55e5f3b0bcd65c0712daf524aa4;
static PyCodeObject *codeobj_961f29008d3947baa14057c78d8b0413;
static PyCodeObject *codeobj_7c223bac229f8ca7e33d1cb9f4a54776;
static PyCodeObject *codeobj_aa5a37652dfb38618f6d55cdf16fd717;
static PyCodeObject *codeobj_aac067f2ad4647f4e27c91d129f77147;
static PyCodeObject *codeobj_f9197440d6673e79be8fbdc79473457c;
static PyCodeObject *codeobj_53dc04ccf8de089bf43eda4370c1ec4e;
static PyCodeObject *codeobj_1cbd45e92cc98f4883f0764249c3b141;
static PyCodeObject *codeobj_255b62fc4305b050d1322fa8a127815e;
static PyCodeObject *codeobj_62f2f005a414ee547964a5ca9b347c77;
static PyCodeObject *codeobj_875a5fdc3cd36f971365d229668688fe;
static PyCodeObject *codeobj_83ba0bace56faadbd0c173ed53902629;
static PyCodeObject *codeobj_c8ddd7c3b40a2956cf15c2271b442695;
static PyCodeObject *codeobj_5f0627ed59aca43e93d4a1cd6f0c2afe;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_038a377797a688052a99e70c40fa02f4 );
    codeobj_f905ee16b1b38b38016191f300fb5513 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_70c9477f6cb4d3ada773db3e0bec008c, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_6c9df3004a4f945b2a5cc5e4db74db3c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_IPythonRenderer, 115, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_8e0c5d8fc4d243989626f7e72a0f84ee = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MarkdownWithMath, 102, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_cfb9bfbe17146d73e81d3090ab9d1844 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MathBlockGrammar, 31, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_ef211c706c48a3d293c22d394c51d720 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MathBlockLexer, 42, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_4f053877595d1f3290140afa99cb4656 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MathInlineGrammar, 63, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_7fdb20c7499f4b745ba4af2c59bcdbb0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MathInlineLexer, 74, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_be97c2567b421c8ec3c78efe734539ec = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 103, const_tuple_21fc239cdac89a817d0b8ee2345062f9_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS );
    codeobj_09ca13a5cc6332980b542bce4492de44 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 86, const_tuple_e853791bb8091160b944c96a7db493d5_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS );
    codeobj_8ca5e55e5f3b0bcd65c0712daf524aa4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 50, const_tuple_5bad021fbdcdef8a1c8f24c391390893_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS );
    codeobj_961f29008d3947baa14057c78d8b0413 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_block_code, 116, const_tuple_e1124bd2bcdf0763b36a418c79ac459b_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7c223bac229f8ca7e33d1cb9f4a54776 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_block_math, 139, const_tuple_str_plain_self_str_plain_text_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_aa5a37652dfb38618f6d55cdf16fd717 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_escape_html, 136, const_tuple_str_plain_self_str_plain_text_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_aac067f2ad4647f4e27c91d129f77147 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_header, 131, const_tuple_432ed16759dbcfd2cac9b97f2b502104_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_f9197440d6673e79be8fbdc79473457c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_image, 150, const_tuple_187cb4ebc23bd927f00c2b933433ed98_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_53dc04ccf8de089bf43eda4370c1ec4e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_inline_math, 147, const_tuple_str_plain_self_str_plain_text_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1cbd45e92cc98f4883f0764249c3b141 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_latex_environment, 142, const_tuple_str_plain_self_str_plain_name_str_plain_text_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_255b62fc4305b050d1322fa8a127815e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_markdown2html_mistune, 176, const_tuple_str_plain_source_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_62f2f005a414ee547964a5ca9b347c77 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_output_block_math, 94, const_tuple_str_plain_self_str_plain_m_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_875a5fdc3cd36f971365d229668688fe = MAKE_CODEOBJ( module_filename_obj, const_str_plain_output_inline_math, 91, const_tuple_str_plain_self_str_plain_m_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_83ba0bace56faadbd0c173ed53902629 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_output_latex_environment, 97, const_tuple_str_plain_self_str_plain_m_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c8ddd7c3b40a2956cf15c2271b442695 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_output_multiline_math, 111, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5f0627ed59aca43e93d4a1cd6f0c2afe = MAKE_CODEOBJ( module_filename_obj, const_str_plain_parse_multiline_math, 55, const_tuple_str_plain_self_str_plain_m_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_10_header( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_11_escape_html(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_12_block_math(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_13_latex_environment(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_14_inline_math(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_15_image(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_16_markdown2html_mistune(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_1___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_2_parse_multiline_math(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_3___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_4_output_inline_math(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_5_output_block_math(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_6_output_latex_environment(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_7___init__(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_8_output_multiline_math(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_9_block_code(  );


// The module function definitions.
static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_rules = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_8ca5e55e5f3b0bcd65c0712daf524aa4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8ca5e55e5f3b0bcd65c0712daf524aa4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8ca5e55e5f3b0bcd65c0712daf524aa4, codeobj_8ca5e55e5f3b0bcd65c0712daf524aa4, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8ca5e55e5f3b0bcd65c0712daf524aa4 = cache_frame_8ca5e55e5f3b0bcd65c0712daf524aa4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8ca5e55e5f3b0bcd65c0712daf524aa4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8ca5e55e5f3b0bcd65c0712daf524aa4 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_rules );
        tmp_compexpr_left_1 = par_rules;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathBlockGrammar );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MathBlockGrammar );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MathBlockGrammar" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 52;
                type_description_1 = "oooN";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            frame_8ca5e55e5f3b0bcd65c0712daf524aa4->m_frame.f_lineno = 52;
            tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_1 = "oooN";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_rules;
                assert( old != NULL );
                par_rules = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_object_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathBlockLexer );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MathBlockLexer );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MathBlockLexer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_rules );
        tmp_tuple_element_1 = par_rules;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_call_result_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ca5e55e5f3b0bcd65c0712daf524aa4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ca5e55e5f3b0bcd65c0712daf524aa4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8ca5e55e5f3b0bcd65c0712daf524aa4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8ca5e55e5f3b0bcd65c0712daf524aa4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8ca5e55e5f3b0bcd65c0712daf524aa4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8ca5e55e5f3b0bcd65c0712daf524aa4,
        type_description_1,
        par_self,
        par_rules,
        par_kwargs,
        NULL
    );


    // Release cached frame.
    if ( frame_8ca5e55e5f3b0bcd65c0712daf524aa4 == cache_frame_8ca5e55e5f3b0bcd65c0712daf524aa4 )
    {
        Py_DECREF( frame_8ca5e55e5f3b0bcd65c0712daf524aa4 );
    }
    cache_frame_8ca5e55e5f3b0bcd65c0712daf524aa4 = NULL;

    assertFrameObject( frame_8ca5e55e5f3b0bcd65c0712daf524aa4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_rules );
    Py_DECREF( par_rules );
    par_rules = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_rules );
    par_rules = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_2_parse_multiline_math( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_m = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_5f0627ed59aca43e93d4a1cd6f0c2afe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_5f0627ed59aca43e93d4a1cd6f0c2afe = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5f0627ed59aca43e93d4a1cd6f0c2afe, codeobj_5f0627ed59aca43e93d4a1cd6f0c2afe, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *) );
    frame_5f0627ed59aca43e93d4a1cd6f0c2afe = cache_frame_5f0627ed59aca43e93d4a1cd6f0c2afe;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5f0627ed59aca43e93d4a1cd6f0c2afe );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5f0627ed59aca43e93d4a1cd6f0c2afe ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_tokens );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_type;
        tmp_dict_value_1 = const_str_plain_multiline_math;
        tmp_args_element_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_args_element_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_text;
        CHECK_OBJECT( par_m );
        tmp_called_instance_1 = par_m;
        frame_5f0627ed59aca43e93d4a1cd6f0c2afe->m_frame.f_lineno = 59;
        tmp_dict_value_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 59;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_args_element_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_5f0627ed59aca43e93d4a1cd6f0c2afe->m_frame.f_lineno = 57;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f0627ed59aca43e93d4a1cd6f0c2afe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f0627ed59aca43e93d4a1cd6f0c2afe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5f0627ed59aca43e93d4a1cd6f0c2afe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5f0627ed59aca43e93d4a1cd6f0c2afe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5f0627ed59aca43e93d4a1cd6f0c2afe, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5f0627ed59aca43e93d4a1cd6f0c2afe,
        type_description_1,
        par_self,
        par_m
    );


    // Release cached frame.
    if ( frame_5f0627ed59aca43e93d4a1cd6f0c2afe == cache_frame_5f0627ed59aca43e93d4a1cd6f0c2afe )
    {
        Py_DECREF( frame_5f0627ed59aca43e93d4a1cd6f0c2afe );
    }
    cache_frame_5f0627ed59aca43e93d4a1cd6f0c2afe = NULL;

    assertFrameObject( frame_5f0627ed59aca43e93d4a1cd6f0c2afe );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_2_parse_multiline_math );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_2_parse_multiline_math );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_3___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_renderer = python_pars[ 1 ];
    PyObject *par_rules = python_pars[ 2 ];
    PyObject *par_kwargs = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_09ca13a5cc6332980b542bce4492de44;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_09ca13a5cc6332980b542bce4492de44 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_09ca13a5cc6332980b542bce4492de44, codeobj_09ca13a5cc6332980b542bce4492de44, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_09ca13a5cc6332980b542bce4492de44 = cache_frame_09ca13a5cc6332980b542bce4492de44;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_09ca13a5cc6332980b542bce4492de44 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_09ca13a5cc6332980b542bce4492de44 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_rules );
        tmp_compexpr_left_1 = par_rules;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathInlineGrammar );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MathInlineGrammar );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MathInlineGrammar" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 88;
                type_description_1 = "ooooN";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            frame_09ca13a5cc6332980b542bce4492de44->m_frame.f_lineno = 88;
            tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;
                type_description_1 = "ooooN";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_rules;
                assert( old != NULL );
                par_rules = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_object_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathInlineLexer );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MathInlineLexer );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MathInlineLexer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_renderer );
        tmp_tuple_element_1 = par_renderer;
        tmp_dircall_arg2_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_rules );
        tmp_tuple_element_1 = par_rules;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_call_result_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_09ca13a5cc6332980b542bce4492de44 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_09ca13a5cc6332980b542bce4492de44 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_09ca13a5cc6332980b542bce4492de44, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_09ca13a5cc6332980b542bce4492de44->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_09ca13a5cc6332980b542bce4492de44, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_09ca13a5cc6332980b542bce4492de44,
        type_description_1,
        par_self,
        par_renderer,
        par_rules,
        par_kwargs,
        NULL
    );


    // Release cached frame.
    if ( frame_09ca13a5cc6332980b542bce4492de44 == cache_frame_09ca13a5cc6332980b542bce4492de44 )
    {
        Py_DECREF( frame_09ca13a5cc6332980b542bce4492de44 );
    }
    cache_frame_09ca13a5cc6332980b542bce4492de44 = NULL;

    assertFrameObject( frame_09ca13a5cc6332980b542bce4492de44 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_3___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    CHECK_OBJECT( (PyObject *)par_rules );
    Py_DECREF( par_rules );
    par_rules = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    Py_XDECREF( par_rules );
    par_rules = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_3___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_4_output_inline_math( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_m = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_875a5fdc3cd36f971365d229668688fe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_875a5fdc3cd36f971365d229668688fe = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_875a5fdc3cd36f971365d229668688fe, codeobj_875a5fdc3cd36f971365d229668688fe, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *) );
    frame_875a5fdc3cd36f971365d229668688fe = cache_frame_875a5fdc3cd36f971365d229668688fe;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_875a5fdc3cd36f971365d229668688fe );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_875a5fdc3cd36f971365d229668688fe ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_renderer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_inline_math );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_m );
        tmp_called_instance_1 = par_m;
        frame_875a5fdc3cd36f971365d229668688fe->m_frame.f_lineno = 92;
        tmp_or_left_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_or_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 92;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_or_left_value_1 );

            exception_lineno = 92;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        Py_DECREF( tmp_or_left_value_1 );
        CHECK_OBJECT( par_m );
        tmp_called_instance_2 = par_m;
        frame_875a5fdc3cd36f971365d229668688fe->m_frame.f_lineno = 92;
        tmp_or_right_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_or_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 92;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_args_element_name_1 = tmp_or_left_value_1;
        or_end_1:;
        frame_875a5fdc3cd36f971365d229668688fe->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_875a5fdc3cd36f971365d229668688fe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_875a5fdc3cd36f971365d229668688fe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_875a5fdc3cd36f971365d229668688fe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_875a5fdc3cd36f971365d229668688fe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_875a5fdc3cd36f971365d229668688fe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_875a5fdc3cd36f971365d229668688fe, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_875a5fdc3cd36f971365d229668688fe,
        type_description_1,
        par_self,
        par_m
    );


    // Release cached frame.
    if ( frame_875a5fdc3cd36f971365d229668688fe == cache_frame_875a5fdc3cd36f971365d229668688fe )
    {
        Py_DECREF( frame_875a5fdc3cd36f971365d229668688fe );
    }
    cache_frame_875a5fdc3cd36f971365d229668688fe = NULL;

    assertFrameObject( frame_875a5fdc3cd36f971365d229668688fe );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_4_output_inline_math );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_4_output_inline_math );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_5_output_block_math( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_m = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_62f2f005a414ee547964a5ca9b347c77;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_62f2f005a414ee547964a5ca9b347c77 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_62f2f005a414ee547964a5ca9b347c77, codeobj_62f2f005a414ee547964a5ca9b347c77, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *) );
    frame_62f2f005a414ee547964a5ca9b347c77 = cache_frame_62f2f005a414ee547964a5ca9b347c77;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_62f2f005a414ee547964a5ca9b347c77 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_62f2f005a414ee547964a5ca9b347c77 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_called_instance_1;
        int tmp_or_left_truth_2;
        PyObject *tmp_or_left_value_2;
        PyObject *tmp_or_right_value_2;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_renderer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_block_math );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_m );
        tmp_called_instance_1 = par_m;
        frame_62f2f005a414ee547964a5ca9b347c77->m_frame.f_lineno = 95;
        tmp_or_left_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_or_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 95;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_or_left_value_1 );

            exception_lineno = 95;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        Py_DECREF( tmp_or_left_value_1 );
        CHECK_OBJECT( par_m );
        tmp_called_instance_2 = par_m;
        frame_62f2f005a414ee547964a5ca9b347c77->m_frame.f_lineno = 95;
        tmp_or_left_value_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_or_left_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 95;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
        if ( tmp_or_left_truth_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_or_left_value_2 );

            exception_lineno = 95;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_2 == 1 )
        {
            goto or_left_2;
        }
        else
        {
            goto or_right_2;
        }
        or_right_2:;
        Py_DECREF( tmp_or_left_value_2 );
        tmp_or_right_value_2 = const_str_empty;
        Py_INCREF( tmp_or_right_value_2 );
        tmp_or_right_value_1 = tmp_or_right_value_2;
        goto or_end_2;
        or_left_2:;
        tmp_or_right_value_1 = tmp_or_left_value_2;
        or_end_2:;
        tmp_args_element_name_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_args_element_name_1 = tmp_or_left_value_1;
        or_end_1:;
        frame_62f2f005a414ee547964a5ca9b347c77->m_frame.f_lineno = 95;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_62f2f005a414ee547964a5ca9b347c77 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_62f2f005a414ee547964a5ca9b347c77 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_62f2f005a414ee547964a5ca9b347c77 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_62f2f005a414ee547964a5ca9b347c77, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_62f2f005a414ee547964a5ca9b347c77->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_62f2f005a414ee547964a5ca9b347c77, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_62f2f005a414ee547964a5ca9b347c77,
        type_description_1,
        par_self,
        par_m
    );


    // Release cached frame.
    if ( frame_62f2f005a414ee547964a5ca9b347c77 == cache_frame_62f2f005a414ee547964a5ca9b347c77 )
    {
        Py_DECREF( frame_62f2f005a414ee547964a5ca9b347c77 );
    }
    cache_frame_62f2f005a414ee547964a5ca9b347c77 = NULL;

    assertFrameObject( frame_62f2f005a414ee547964a5ca9b347c77 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_5_output_block_math );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_5_output_block_math );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_6_output_latex_environment( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_m = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_83ba0bace56faadbd0c173ed53902629;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_83ba0bace56faadbd0c173ed53902629 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_83ba0bace56faadbd0c173ed53902629, codeobj_83ba0bace56faadbd0c173ed53902629, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *) );
    frame_83ba0bace56faadbd0c173ed53902629 = cache_frame_83ba0bace56faadbd0c173ed53902629;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_83ba0bace56faadbd0c173ed53902629 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_83ba0bace56faadbd0c173ed53902629 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_renderer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_latex_environment );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_m );
        tmp_called_instance_1 = par_m;
        frame_83ba0bace56faadbd0c173ed53902629->m_frame.f_lineno = 98;
        tmp_args_element_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 98;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_m );
        tmp_called_instance_2 = par_m;
        frame_83ba0bace56faadbd0c173ed53902629->m_frame.f_lineno = 99;
        tmp_args_element_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 99;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_83ba0bace56faadbd0c173ed53902629->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_83ba0bace56faadbd0c173ed53902629 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_83ba0bace56faadbd0c173ed53902629 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_83ba0bace56faadbd0c173ed53902629 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_83ba0bace56faadbd0c173ed53902629, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_83ba0bace56faadbd0c173ed53902629->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_83ba0bace56faadbd0c173ed53902629, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_83ba0bace56faadbd0c173ed53902629,
        type_description_1,
        par_self,
        par_m
    );


    // Release cached frame.
    if ( frame_83ba0bace56faadbd0c173ed53902629 == cache_frame_83ba0bace56faadbd0c173ed53902629 )
    {
        Py_DECREF( frame_83ba0bace56faadbd0c173ed53902629 );
    }
    cache_frame_83ba0bace56faadbd0c173ed53902629 = NULL;

    assertFrameObject( frame_83ba0bace56faadbd0c173ed53902629 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_6_output_latex_environment );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_6_output_latex_environment );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_7___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_renderer = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_be97c2567b421c8ec3c78efe734539ec;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_be97c2567b421c8ec3c78efe734539ec = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_be97c2567b421c8ec3c78efe734539ec, codeobj_be97c2567b421c8ec3c78efe734539ec, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_be97c2567b421c8ec3c78efe734539ec = cache_frame_be97c2567b421c8ec3c78efe734539ec;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_be97c2567b421c8ec3c78efe734539ec );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_be97c2567b421c8ec3c78efe734539ec ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = const_str_plain_inline;
        CHECK_OBJECT( par_kwargs );
        tmp_compexpr_right_1 = par_kwargs;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathInlineLexer );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MathInlineLexer );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MathInlineLexer" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 105;
                type_description_1 = "oooN";
                goto frame_exception_exit_1;
            }

            tmp_ass_subvalue_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_1 = par_kwargs;
            tmp_ass_subscript_1 = const_str_plain_inline;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;
                type_description_1 = "oooN";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = const_str_plain_block;
        CHECK_OBJECT( par_kwargs );
        tmp_compexpr_right_2 = par_kwargs;
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_ass_subvalue_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_ass_subscribed_2;
            PyObject *tmp_ass_subscript_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathBlockLexer );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MathBlockLexer );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MathBlockLexer" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 107;
                type_description_1 = "oooN";
                goto frame_exception_exit_1;
            }

            tmp_ass_subvalue_2 = tmp_mvar_value_2;
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_2 = par_kwargs;
            tmp_ass_subscript_2 = const_str_plain_block;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 107;
                type_description_1 = "oooN";
                goto frame_exception_exit_1;
            }
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_object_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MarkdownWithMath );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MarkdownWithMath );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MarkdownWithMath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_3;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_renderer );
        tmp_tuple_element_1 = par_renderer;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_call_result_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be97c2567b421c8ec3c78efe734539ec );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be97c2567b421c8ec3c78efe734539ec );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_be97c2567b421c8ec3c78efe734539ec, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_be97c2567b421c8ec3c78efe734539ec->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_be97c2567b421c8ec3c78efe734539ec, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_be97c2567b421c8ec3c78efe734539ec,
        type_description_1,
        par_self,
        par_renderer,
        par_kwargs,
        NULL
    );


    // Release cached frame.
    if ( frame_be97c2567b421c8ec3c78efe734539ec == cache_frame_be97c2567b421c8ec3c78efe734539ec )
    {
        Py_DECREF( frame_be97c2567b421c8ec3c78efe734539ec );
    }
    cache_frame_be97c2567b421c8ec3c78efe734539ec = NULL;

    assertFrameObject( frame_be97c2567b421c8ec3c78efe734539ec );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_7___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_7___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_8_output_multiline_math( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c8ddd7c3b40a2956cf15c2271b442695;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c8ddd7c3b40a2956cf15c2271b442695 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c8ddd7c3b40a2956cf15c2271b442695, codeobj_c8ddd7c3b40a2956cf15c2271b442695, module_nbconvert$filters$markdown_mistune, sizeof(void *) );
    frame_c8ddd7c3b40a2956cf15c2271b442695 = cache_frame_c8ddd7c3b40a2956cf15c2271b442695;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c8ddd7c3b40a2956cf15c2271b442695 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c8ddd7c3b40a2956cf15c2271b442695 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_inline );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_token );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 112;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_str_plain_text;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 112;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_c8ddd7c3b40a2956cf15c2271b442695->m_frame.f_lineno = 112;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8ddd7c3b40a2956cf15c2271b442695 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8ddd7c3b40a2956cf15c2271b442695 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8ddd7c3b40a2956cf15c2271b442695 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c8ddd7c3b40a2956cf15c2271b442695, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c8ddd7c3b40a2956cf15c2271b442695->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c8ddd7c3b40a2956cf15c2271b442695, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c8ddd7c3b40a2956cf15c2271b442695,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_c8ddd7c3b40a2956cf15c2271b442695 == cache_frame_c8ddd7c3b40a2956cf15c2271b442695 )
    {
        Py_DECREF( frame_c8ddd7c3b40a2956cf15c2271b442695 );
    }
    cache_frame_c8ddd7c3b40a2956cf15c2271b442695 = NULL;

    assertFrameObject( frame_c8ddd7c3b40a2956cf15c2271b442695 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_8_output_multiline_math );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_8_output_multiline_math );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_9_block_code( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_code = python_pars[ 1 ];
    PyObject *par_lang = python_pars[ 2 ];
    PyObject *var_lexer = NULL;
    PyObject *var_formatter = NULL;
    struct Nuitka_FrameObject *frame_961f29008d3947baa14057c78d8b0413;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_961f29008d3947baa14057c78d8b0413 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_961f29008d3947baa14057c78d8b0413, codeobj_961f29008d3947baa14057c78d8b0413, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_961f29008d3947baa14057c78d8b0413 = cache_frame_961f29008d3947baa14057c78d8b0413;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_961f29008d3947baa14057c78d8b0413 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_961f29008d3947baa14057c78d8b0413 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_lang );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_lang );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_get_lexer_by_name );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_lexer_by_name );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_lexer_by_name" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 119;
                type_description_1 = "ooooo";
                goto try_except_handler_2;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_lang );
            tmp_tuple_element_1 = par_lang;
            tmp_args_name_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_kw_name_1 = PyDict_Copy( const_dict_4127e6c33f0203f58d13110adfa0e8b6 );
            frame_961f29008d3947baa14057c78d8b0413->m_frame.f_lineno = 119;
            tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 119;
                type_description_1 = "ooooo";
                goto try_except_handler_2;
            }
            assert( var_lexer == NULL );
            var_lexer = tmp_assign_source_1;
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_961f29008d3947baa14057c78d8b0413, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_961f29008d3947baa14057c78d8b0413, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_mvar_value_2;
            tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_ClassNotFound );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ClassNotFound );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ClassNotFound" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 120;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_compexpr_right_1 = tmp_mvar_value_2;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 120;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_left_name_1;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_1;
                PyObject *tmp_right_name_2;
                CHECK_OBJECT( par_lang );
                tmp_left_name_2 = par_lang;
                tmp_right_name_1 = const_str_newline;
                tmp_left_name_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_1 );
                if ( tmp_left_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;
                    type_description_1 = "ooooo";
                    goto try_except_handler_3;
                }
                CHECK_OBJECT( par_code );
                tmp_right_name_2 = par_code;
                tmp_assign_source_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
                Py_DECREF( tmp_left_name_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;
                    type_description_1 = "ooooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = par_code;
                    assert( old != NULL );
                    par_code = tmp_assign_source_2;
                    Py_DECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_3;
                tmp_assign_source_3 = Py_None;
                {
                    PyObject *old = par_lang;
                    assert( old != NULL );
                    par_lang = tmp_assign_source_3;
                    Py_INCREF( par_lang );
                    Py_DECREF( old );
                }

            }
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 118;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_961f29008d3947baa14057c78d8b0413->m_frame) frame_961f29008d3947baa14057c78d8b0413->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
            branch_end_2:;
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_1;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_9_block_code );
        return NULL;
        // End of try:
        try_end_1:;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_operand_name_1;
        if ( par_lang == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "lang" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_1 = par_lang;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_1;
            tmp_left_name_3 = const_str_digest_1ea263c0bb7bc5648dd0b66993cc0bb4;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mistune );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mistune" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 126;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_3;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_escape );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 126;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            if ( par_code == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "code" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 126;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = par_code;
            frame_961f29008d3947baa14057c78d8b0413->m_frame.f_lineno = 126;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_right_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            if ( tmp_right_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 126;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_HtmlFormatter );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HtmlFormatter );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HtmlFormatter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        frame_961f29008d3947baa14057c78d8b0413->m_frame.f_lineno = 128;
        tmp_assign_source_4 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_formatter == NULL );
        var_formatter = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_highlight );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_highlight );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "highlight" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_5;
        if ( par_code == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "code" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = par_code;
        if ( var_lexer == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "lexer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = var_lexer;
        CHECK_OBJECT( var_formatter );
        tmp_args_element_name_4 = var_formatter;
        frame_961f29008d3947baa14057c78d8b0413->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_961f29008d3947baa14057c78d8b0413 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_961f29008d3947baa14057c78d8b0413 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_961f29008d3947baa14057c78d8b0413 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_961f29008d3947baa14057c78d8b0413, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_961f29008d3947baa14057c78d8b0413->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_961f29008d3947baa14057c78d8b0413, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_961f29008d3947baa14057c78d8b0413,
        type_description_1,
        par_self,
        par_code,
        par_lang,
        var_lexer,
        var_formatter
    );


    // Release cached frame.
    if ( frame_961f29008d3947baa14057c78d8b0413 == cache_frame_961f29008d3947baa14057c78d8b0413 )
    {
        Py_DECREF( frame_961f29008d3947baa14057c78d8b0413 );
    }
    cache_frame_961f29008d3947baa14057c78d8b0413 = NULL;

    assertFrameObject( frame_961f29008d3947baa14057c78d8b0413 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_9_block_code );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_code );
    par_code = NULL;

    Py_XDECREF( par_lang );
    par_lang = NULL;

    Py_XDECREF( var_lexer );
    var_lexer = NULL;

    Py_XDECREF( var_formatter );
    var_formatter = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_code );
    par_code = NULL;

    Py_XDECREF( par_lang );
    par_lang = NULL;

    Py_XDECREF( var_lexer );
    var_lexer = NULL;

    Py_XDECREF( var_formatter );
    var_formatter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_9_block_code );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_10_header( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *par_level = python_pars[ 2 ];
    PyObject *par_raw = python_pars[ 3 ];
    PyObject *var_html = NULL;
    PyObject *var_anchor_link_text = NULL;
    struct Nuitka_FrameObject *frame_aac067f2ad4647f4e27c91d129f77147;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_aac067f2ad4647f4e27c91d129f77147 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_aac067f2ad4647f4e27c91d129f77147, codeobj_aac067f2ad4647f4e27c91d129f77147, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_aac067f2ad4647f4e27c91d129f77147 = cache_frame_aac067f2ad4647f4e27c91d129f77147;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_aac067f2ad4647f4e27c91d129f77147 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_aac067f2ad4647f4e27c91d129f77147 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_IPythonRenderer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IPythonRenderer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IPythonRenderer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_header );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_text );
        tmp_tuple_element_1 = par_text;
        tmp_args_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_level );
        tmp_tuple_element_1 = par_level;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_raw;
        CHECK_OBJECT( par_raw );
        tmp_dict_value_1 = par_raw;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_aac067f2ad4647f4e27c91d129f77147->m_frame.f_lineno = 132;
        tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        assert( var_html == NULL );
        var_html = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_options );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        frame_aac067f2ad4647f4e27c91d129f77147->m_frame.f_lineno = 133;
        tmp_assign_source_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_anchor_link_text_str_chr_182_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        assert( var_anchor_link_text == NULL );
        var_anchor_link_text = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_name_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_add_anchor );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_add_anchor );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "add_anchor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( var_html );
        tmp_tuple_element_2 = var_html;
        tmp_args_name_2 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
        tmp_dict_key_2 = const_str_plain_anchor_link_text;
        CHECK_OBJECT( var_anchor_link_text );
        tmp_dict_value_2 = var_anchor_link_text;
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_aac067f2ad4647f4e27c91d129f77147->m_frame.f_lineno = 134;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aac067f2ad4647f4e27c91d129f77147 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_aac067f2ad4647f4e27c91d129f77147 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aac067f2ad4647f4e27c91d129f77147 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_aac067f2ad4647f4e27c91d129f77147, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_aac067f2ad4647f4e27c91d129f77147->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_aac067f2ad4647f4e27c91d129f77147, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_aac067f2ad4647f4e27c91d129f77147,
        type_description_1,
        par_self,
        par_text,
        par_level,
        par_raw,
        var_html,
        var_anchor_link_text,
        NULL
    );


    // Release cached frame.
    if ( frame_aac067f2ad4647f4e27c91d129f77147 == cache_frame_aac067f2ad4647f4e27c91d129f77147 )
    {
        Py_DECREF( frame_aac067f2ad4647f4e27c91d129f77147 );
    }
    cache_frame_aac067f2ad4647f4e27c91d129f77147 = NULL;

    assertFrameObject( frame_aac067f2ad4647f4e27c91d129f77147 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_10_header );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_level );
    Py_DECREF( par_level );
    par_level = NULL;

    CHECK_OBJECT( (PyObject *)par_raw );
    Py_DECREF( par_raw );
    par_raw = NULL;

    CHECK_OBJECT( (PyObject *)var_html );
    Py_DECREF( var_html );
    var_html = NULL;

    CHECK_OBJECT( (PyObject *)var_anchor_link_text );
    Py_DECREF( var_anchor_link_text );
    var_anchor_link_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_level );
    Py_DECREF( par_level );
    par_level = NULL;

    CHECK_OBJECT( (PyObject *)par_raw );
    Py_DECREF( par_raw );
    par_raw = NULL;

    Py_XDECREF( var_html );
    var_html = NULL;

    Py_XDECREF( var_anchor_link_text );
    var_anchor_link_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_10_header );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_11_escape_html( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_aa5a37652dfb38618f6d55cdf16fd717;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_aa5a37652dfb38618f6d55cdf16fd717 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_aa5a37652dfb38618f6d55cdf16fd717, codeobj_aa5a37652dfb38618f6d55cdf16fd717, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *) );
    frame_aa5a37652dfb38618f6d55cdf16fd717 = cache_frame_aa5a37652dfb38618f6d55cdf16fd717;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_aa5a37652dfb38618f6d55cdf16fd717 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_aa5a37652dfb38618f6d55cdf16fd717 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_html_escape );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_html_escape );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "html_escape" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 137;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_aa5a37652dfb38618f6d55cdf16fd717->m_frame.f_lineno = 137;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa5a37652dfb38618f6d55cdf16fd717 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa5a37652dfb38618f6d55cdf16fd717 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa5a37652dfb38618f6d55cdf16fd717 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_aa5a37652dfb38618f6d55cdf16fd717, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_aa5a37652dfb38618f6d55cdf16fd717->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_aa5a37652dfb38618f6d55cdf16fd717, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_aa5a37652dfb38618f6d55cdf16fd717,
        type_description_1,
        par_self,
        par_text
    );


    // Release cached frame.
    if ( frame_aa5a37652dfb38618f6d55cdf16fd717 == cache_frame_aa5a37652dfb38618f6d55cdf16fd717 )
    {
        Py_DECREF( frame_aa5a37652dfb38618f6d55cdf16fd717 );
    }
    cache_frame_aa5a37652dfb38618f6d55cdf16fd717 = NULL;

    assertFrameObject( frame_aa5a37652dfb38618f6d55cdf16fd717 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_11_escape_html );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_11_escape_html );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_12_block_math( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_7c223bac229f8ca7e33d1cb9f4a54776;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7c223bac229f8ca7e33d1cb9f4a54776 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7c223bac229f8ca7e33d1cb9f4a54776, codeobj_7c223bac229f8ca7e33d1cb9f4a54776, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *) );
    frame_7c223bac229f8ca7e33d1cb9f4a54776 = cache_frame_7c223bac229f8ca7e33d1cb9f4a54776;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7c223bac229f8ca7e33d1cb9f4a54776 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7c223bac229f8ca7e33d1cb9f4a54776 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        tmp_left_name_1 = const_str_digest_df9fa7089fd2a8140575d2b5fb4183da;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_7c223bac229f8ca7e33d1cb9f4a54776->m_frame.f_lineno = 140;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_escape_html, call_args );
        }

        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7c223bac229f8ca7e33d1cb9f4a54776 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7c223bac229f8ca7e33d1cb9f4a54776 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7c223bac229f8ca7e33d1cb9f4a54776 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7c223bac229f8ca7e33d1cb9f4a54776, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7c223bac229f8ca7e33d1cb9f4a54776->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7c223bac229f8ca7e33d1cb9f4a54776, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7c223bac229f8ca7e33d1cb9f4a54776,
        type_description_1,
        par_self,
        par_text
    );


    // Release cached frame.
    if ( frame_7c223bac229f8ca7e33d1cb9f4a54776 == cache_frame_7c223bac229f8ca7e33d1cb9f4a54776 )
    {
        Py_DECREF( frame_7c223bac229f8ca7e33d1cb9f4a54776 );
    }
    cache_frame_7c223bac229f8ca7e33d1cb9f4a54776 = NULL;

    assertFrameObject( frame_7c223bac229f8ca7e33d1cb9f4a54776 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_12_block_math );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_12_block_math );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_13_latex_environment( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *par_text = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_1cbd45e92cc98f4883f0764249c3b141;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_1cbd45e92cc98f4883f0764249c3b141 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1cbd45e92cc98f4883f0764249c3b141, codeobj_1cbd45e92cc98f4883f0764249c3b141, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1cbd45e92cc98f4883f0764249c3b141 = cache_frame_1cbd45e92cc98f4883f0764249c3b141;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1cbd45e92cc98f4883f0764249c3b141 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1cbd45e92cc98f4883f0764249c3b141 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_name );
        tmp_args_element_name_1 = par_name;
        frame_1cbd45e92cc98f4883f0764249c3b141->m_frame.f_lineno = 143;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_escape_html, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_name;
            assert( old != NULL );
            par_name = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_2 = par_text;
        frame_1cbd45e92cc98f4883f0764249c3b141->m_frame.f_lineno = 144;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_escape_html, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_text;
            assert( old != NULL );
            par_text = tmp_assign_source_2;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        tmp_left_name_1 = const_str_digest_fa84e331e4a03a1a2a772eae49a52bae;
        CHECK_OBJECT( par_name );
        tmp_tuple_element_1 = par_name;
        tmp_right_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_text );
        tmp_tuple_element_1 = par_text;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_name );
        tmp_tuple_element_1 = par_name;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cbd45e92cc98f4883f0764249c3b141 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cbd45e92cc98f4883f0764249c3b141 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cbd45e92cc98f4883f0764249c3b141 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1cbd45e92cc98f4883f0764249c3b141, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1cbd45e92cc98f4883f0764249c3b141->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1cbd45e92cc98f4883f0764249c3b141, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1cbd45e92cc98f4883f0764249c3b141,
        type_description_1,
        par_self,
        par_name,
        par_text
    );


    // Release cached frame.
    if ( frame_1cbd45e92cc98f4883f0764249c3b141 == cache_frame_1cbd45e92cc98f4883f0764249c3b141 )
    {
        Py_DECREF( frame_1cbd45e92cc98f4883f0764249c3b141 );
    }
    cache_frame_1cbd45e92cc98f4883f0764249c3b141 = NULL;

    assertFrameObject( frame_1cbd45e92cc98f4883f0764249c3b141 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_13_latex_environment );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_13_latex_environment );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_14_inline_math( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_53dc04ccf8de089bf43eda4370c1ec4e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_53dc04ccf8de089bf43eda4370c1ec4e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_53dc04ccf8de089bf43eda4370c1ec4e, codeobj_53dc04ccf8de089bf43eda4370c1ec4e, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *) );
    frame_53dc04ccf8de089bf43eda4370c1ec4e = cache_frame_53dc04ccf8de089bf43eda4370c1ec4e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_53dc04ccf8de089bf43eda4370c1ec4e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_53dc04ccf8de089bf43eda4370c1ec4e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        tmp_left_name_1 = const_str_digest_da85ef92a56e55578ce4744acd08408a;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_53dc04ccf8de089bf43eda4370c1ec4e->m_frame.f_lineno = 148;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_escape_html, call_args );
        }

        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53dc04ccf8de089bf43eda4370c1ec4e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_53dc04ccf8de089bf43eda4370c1ec4e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53dc04ccf8de089bf43eda4370c1ec4e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_53dc04ccf8de089bf43eda4370c1ec4e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_53dc04ccf8de089bf43eda4370c1ec4e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_53dc04ccf8de089bf43eda4370c1ec4e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_53dc04ccf8de089bf43eda4370c1ec4e,
        type_description_1,
        par_self,
        par_text
    );


    // Release cached frame.
    if ( frame_53dc04ccf8de089bf43eda4370c1ec4e == cache_frame_53dc04ccf8de089bf43eda4370c1ec4e )
    {
        Py_DECREF( frame_53dc04ccf8de089bf43eda4370c1ec4e );
    }
    cache_frame_53dc04ccf8de089bf43eda4370c1ec4e = NULL;

    assertFrameObject( frame_53dc04ccf8de089bf43eda4370c1ec4e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_14_inline_math );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_14_inline_math );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_15_image( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_src = python_pars[ 1 ];
    PyObject *par_title = python_pars[ 2 ];
    PyObject *par_text = python_pars[ 3 ];
    PyObject *var_attachments = NULL;
    PyObject *var_name = NULL;
    PyObject *var_attachment = NULL;
    PyObject *var_preferred_mime_types = NULL;
    PyObject *var_preferred_mime_type = NULL;
    PyObject *var_mime_type = NULL;
    PyObject *var_data = NULL;
    nuitka_bool tmp_for_loop_1__break_indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_f9197440d6673e79be8fbdc79473457c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f9197440d6673e79be8fbdc79473457c = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f9197440d6673e79be8fbdc79473457c, codeobj_f9197440d6673e79be8fbdc79473457c, module_nbconvert$filters$markdown_mistune, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f9197440d6673e79be8fbdc79473457c = cache_frame_f9197440d6673e79be8fbdc79473457c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f9197440d6673e79be8fbdc79473457c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f9197440d6673e79be8fbdc79473457c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_call_arg_element_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_options );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oooooNooooooN";
            goto frame_exception_exit_1;
        }
        tmp_call_arg_element_1 = const_str_plain_attachments;
        tmp_call_arg_element_2 = PyDict_New();
        frame_f9197440d6673e79be8fbdc79473457c->m_frame.f_lineno = 157;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oooooNooooooN";
            goto frame_exception_exit_1;
        }
        assert( var_attachments == NULL );
        var_attachments = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_src );
        tmp_called_instance_2 = par_src;
        frame_f9197440d6673e79be8fbdc79473457c->m_frame.f_lineno = 159;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_e42f92694dce90d8d2e43e2b68bc6035_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oooooNooooooN";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 159;
            type_description_1 = "oooooNooooooN";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( par_src );
            tmp_subscribed_name_1 = par_src;
            tmp_subscript_name_1 = const_slice_int_pos_11_none_none;
            tmp_assign_source_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_1 = "oooooNooooooN";
                goto frame_exception_exit_1;
            }
            assert( var_name == NULL );
            var_name = tmp_assign_source_2;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( var_name );
            tmp_compexpr_left_1 = var_name;
            CHECK_OBJECT( var_attachments );
            tmp_compexpr_right_1 = var_attachments;
            tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_1 = "oooooNooooooN";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_args_element_name_1;
                tmp_raise_type_1 = PyExc_AssertionError;
                tmp_called_instance_3 = const_str_digest_768877b5e7da42f81f1a9274ab505df8;
                CHECK_OBJECT( var_name );
                tmp_args_element_name_1 = var_name;
                frame_f9197440d6673e79be8fbdc79473457c->m_frame.f_lineno = 161;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1 };
                    tmp_tuple_element_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_format, call_args );
                }

                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 161;
                    type_description_1 = "oooooNooooooN";
                    goto frame_exception_exit_1;
                }
                tmp_raise_value_1 = PyTuple_New( 1 );
                PyTuple_SET_ITEM( tmp_raise_value_1, 0, tmp_tuple_element_1 );
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 161;
                RAISE_EXCEPTION_WITH_VALUE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooNooooooN";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            CHECK_OBJECT( var_attachments );
            tmp_subscribed_name_2 = var_attachments;
            CHECK_OBJECT( var_name );
            tmp_subscript_name_2 = var_name;
            tmp_assign_source_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "oooooNooooooN";
                goto frame_exception_exit_1;
            }
            assert( var_attachment == NULL );
            var_attachment = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            tmp_assign_source_4 = LIST_COPY( const_list_7ef32ee98da72c49c75f9beb2c938610_list );
            assert( var_preferred_mime_types == NULL );
            var_preferred_mime_types = tmp_assign_source_4;
        }
        {
            nuitka_bool tmp_assign_source_5;
            tmp_assign_source_5 = NUITKA_BOOL_FALSE;
            tmp_for_loop_1__break_indicator = tmp_assign_source_5;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( var_preferred_mime_types );
            tmp_iter_arg_1 = var_preferred_mime_types;
            tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 165;
                type_description_1 = "oooooNooooooN";
                goto try_except_handler_2;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_6;
        }
        // Tried code:
        loop_start_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_value_name_1;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_value_name_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_7 = ITERATOR_NEXT( tmp_value_name_1 );
            if ( tmp_assign_source_7 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooNooooooN";
                exception_lineno = 165;
                goto try_except_handler_4;
            }
            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = exception_keeper_type_1;
            tmp_compexpr_right_2 = PyExc_StopIteration;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_1 );
                Py_XDECREF( exception_keeper_value_1 );
                Py_XDECREF( exception_keeper_tb_1 );

                exception_lineno = 165;
                type_description_1 = "oooooNooooooN";
                goto try_except_handler_3;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                nuitka_bool tmp_assign_source_8;
                tmp_assign_source_8 = NUITKA_BOOL_TRUE;
                tmp_for_loop_1__break_indicator = tmp_assign_source_8;
            }
            Py_DECREF( exception_keeper_type_1 );
            Py_XDECREF( exception_keeper_value_1 );
            Py_XDECREF( exception_keeper_tb_1 );
            goto loop_end_1;
            goto branch_end_3;
            branch_no_3:;
            // Re-raise.
            exception_type = exception_keeper_type_1;
            exception_value = exception_keeper_value_1;
            exception_tb = exception_keeper_tb_1;
            exception_lineno = exception_keeper_lineno_1;

            goto try_except_handler_3;
            branch_end_3:;
        }
        // End of try:
        try_end_1:;
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_9 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_preferred_mime_type;
                var_preferred_mime_type = tmp_assign_source_9;
                Py_INCREF( var_preferred_mime_type );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( var_preferred_mime_type );
            tmp_compexpr_left_3 = var_preferred_mime_type;
            CHECK_OBJECT( var_attachment );
            tmp_compexpr_right_3 = var_attachment;
            tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;
                type_description_1 = "oooooNooooooN";
                goto try_except_handler_3;
            }
            tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            goto loop_end_1;
            branch_no_4:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oooooNooooooN";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_2;
        // End of try:
        try_end_2:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        {
            nuitka_bool tmp_condition_result_5;
            nuitka_bool tmp_compexpr_left_4;
            nuitka_bool tmp_compexpr_right_4;
            assert( tmp_for_loop_1__break_indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_4 = tmp_for_loop_1__break_indicator;
            tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
            tmp_condition_result_5 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_10;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_subscript_name_3;
                CHECK_OBJECT( var_attachment );
                tmp_called_instance_4 = var_attachment;
                frame_f9197440d6673e79be8fbdc79473457c->m_frame.f_lineno = 169;
                tmp_subscribed_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_keys );
                if ( tmp_subscribed_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 169;
                    type_description_1 = "oooooNooooooN";
                    goto try_except_handler_2;
                }
                tmp_subscript_name_3 = const_int_0;
                tmp_assign_source_10 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
                Py_DECREF( tmp_subscribed_name_3 );
                if ( tmp_assign_source_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 169;
                    type_description_1 = "oooooNooooooN";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = var_preferred_mime_types;
                    assert( old != NULL );
                    var_preferred_mime_types = tmp_assign_source_10;
                    Py_DECREF( old );
                }

            }
            branch_no_5:;
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_1;
        // End of try:
        try_end_3:;
        {
            PyObject *tmp_assign_source_11;
            if ( var_preferred_mime_type == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "preferred_mime_type" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 170;
                type_description_1 = "oooooNooooooN";
                goto frame_exception_exit_1;
            }

            tmp_assign_source_11 = var_preferred_mime_type;
            assert( var_mime_type == NULL );
            Py_INCREF( tmp_assign_source_11 );
            var_mime_type = tmp_assign_source_11;
        }
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_subscript_name_4;
            CHECK_OBJECT( var_attachment );
            tmp_subscribed_name_4 = var_attachment;
            CHECK_OBJECT( var_mime_type );
            tmp_subscript_name_4 = var_mime_type;
            tmp_assign_source_12 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "oooooNooooooN";
                goto frame_exception_exit_1;
            }
            assert( var_data == NULL );
            var_data = tmp_assign_source_12;
        }
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_left_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_1;
            PyObject *tmp_right_name_2;
            PyObject *tmp_right_name_3;
            tmp_left_name_3 = const_str_digest_a1fe045d3e680ae945c1ff74d68fb820;
            CHECK_OBJECT( var_mime_type );
            tmp_right_name_1 = var_mime_type;
            tmp_left_name_2 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_3, tmp_right_name_1 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "oooooNooooooN";
                goto frame_exception_exit_1;
            }
            tmp_right_name_2 = const_str_digest_5508e7b0ee7f27dd0043819f00e7c12e;
            tmp_left_name_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_2 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "oooooNooooooN";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_data );
            tmp_right_name_3 = var_data;
            tmp_assign_source_13 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_3 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_assign_source_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "oooooNooooooN";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_src;
                assert( old != NULL );
                par_src = tmp_assign_source_13;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_5;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_IPythonRenderer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IPythonRenderer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IPythonRenderer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;
            type_description_1 = "oooooNooooooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_called_instance_5 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oooooNooooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_src );
        tmp_args_element_name_2 = par_src;
        CHECK_OBJECT( par_title );
        tmp_args_element_name_3 = par_title;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_4 = par_text;
        frame_f9197440d6673e79be8fbdc79473457c->m_frame.f_lineno = 173;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_return_value = CALL_METHOD_WITH_ARGS3( tmp_called_instance_5, const_str_plain_image, call_args );
        }

        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oooooNooooooN";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f9197440d6673e79be8fbdc79473457c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f9197440d6673e79be8fbdc79473457c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f9197440d6673e79be8fbdc79473457c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f9197440d6673e79be8fbdc79473457c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f9197440d6673e79be8fbdc79473457c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f9197440d6673e79be8fbdc79473457c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f9197440d6673e79be8fbdc79473457c,
        type_description_1,
        par_self,
        par_src,
        par_title,
        par_text,
        var_attachments,
        NULL,
        var_name,
        var_attachment,
        var_preferred_mime_types,
        var_preferred_mime_type,
        var_mime_type,
        var_data,
        NULL
    );


    // Release cached frame.
    if ( frame_f9197440d6673e79be8fbdc79473457c == cache_frame_f9197440d6673e79be8fbdc79473457c )
    {
        Py_DECREF( frame_f9197440d6673e79be8fbdc79473457c );
    }
    cache_frame_f9197440d6673e79be8fbdc79473457c = NULL;

    assertFrameObject( frame_f9197440d6673e79be8fbdc79473457c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_15_image );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_src );
    Py_DECREF( par_src );
    par_src = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)var_attachments );
    Py_DECREF( var_attachments );
    var_attachments = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_attachment );
    var_attachment = NULL;

    Py_XDECREF( var_preferred_mime_types );
    var_preferred_mime_types = NULL;

    Py_XDECREF( var_preferred_mime_type );
    var_preferred_mime_type = NULL;

    Py_XDECREF( var_mime_type );
    var_mime_type = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_src );
    par_src = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_attachments );
    var_attachments = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_attachment );
    var_attachment = NULL;

    Py_XDECREF( var_preferred_mime_types );
    var_preferred_mime_types = NULL;

    Py_XDECREF( var_preferred_mime_type );
    var_preferred_mime_type = NULL;

    Py_XDECREF( var_mime_type );
    var_mime_type = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_15_image );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown_mistune$$$function_16_markdown2html_mistune( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_source = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_255b62fc4305b050d1322fa8a127815e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_255b62fc4305b050d1322fa8a127815e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_255b62fc4305b050d1322fa8a127815e, codeobj_255b62fc4305b050d1322fa8a127815e, module_nbconvert$filters$markdown_mistune, sizeof(void *) );
    frame_255b62fc4305b050d1322fa8a127815e = cache_frame_255b62fc4305b050d1322fa8a127815e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_255b62fc4305b050d1322fa8a127815e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_255b62fc4305b050d1322fa8a127815e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MarkdownWithMath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MarkdownWithMath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MarkdownWithMath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 178;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_renderer;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_IPythonRenderer );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IPythonRenderer );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IPythonRenderer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 178;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_kw_name_2 = PyDict_Copy( const_dict_d4840cccbd3dc923b8cb53333db50eda );
        frame_255b62fc4305b050d1322fa8a127815e->m_frame.f_lineno = 178;
        tmp_dict_value_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_255b62fc4305b050d1322fa8a127815e->m_frame.f_lineno = 178;
        tmp_called_instance_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_source );
        tmp_args_element_name_1 = par_source;
        frame_255b62fc4305b050d1322fa8a127815e->m_frame.f_lineno = 178;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_render, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_255b62fc4305b050d1322fa8a127815e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_255b62fc4305b050d1322fa8a127815e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_255b62fc4305b050d1322fa8a127815e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_255b62fc4305b050d1322fa8a127815e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_255b62fc4305b050d1322fa8a127815e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_255b62fc4305b050d1322fa8a127815e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_255b62fc4305b050d1322fa8a127815e,
        type_description_1,
        par_source
    );


    // Release cached frame.
    if ( frame_255b62fc4305b050d1322fa8a127815e == cache_frame_255b62fc4305b050d1322fa8a127815e )
    {
        Py_DECREF( frame_255b62fc4305b050d1322fa8a127815e );
    }
    cache_frame_255b62fc4305b050d1322fa8a127815e = NULL;

    assertFrameObject( frame_255b62fc4305b050d1322fa8a127815e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_16_markdown2html_mistune );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune$$$function_16_markdown2html_mistune );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_10_header( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_10_header,
        const_str_plain_header,
#if PYTHON_VERSION >= 300
        const_str_digest_840f3aad4365225677122d88094b4aac,
#endif
        codeobj_aac067f2ad4647f4e27c91d129f77147,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_11_escape_html(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_11_escape_html,
        const_str_plain_escape_html,
#if PYTHON_VERSION >= 300
        const_str_digest_ff4b6d762ff39011de49b741bfbdc2e5,
#endif
        codeobj_aa5a37652dfb38618f6d55cdf16fd717,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_12_block_math(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_12_block_math,
        const_str_plain_block_math,
#if PYTHON_VERSION >= 300
        const_str_digest_f6297e3ecd25b13a4cc416cf99997112,
#endif
        codeobj_7c223bac229f8ca7e33d1cb9f4a54776,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_13_latex_environment(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_13_latex_environment,
        const_str_plain_latex_environment,
#if PYTHON_VERSION >= 300
        const_str_digest_eecd2b46c1f0e70774ded143ca51617c,
#endif
        codeobj_1cbd45e92cc98f4883f0764249c3b141,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_14_inline_math(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_14_inline_math,
        const_str_plain_inline_math,
#if PYTHON_VERSION >= 300
        const_str_digest_c7a8542447e4f945d1c20a13bff26fc2,
#endif
        codeobj_53dc04ccf8de089bf43eda4370c1ec4e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_15_image(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_15_image,
        const_str_plain_image,
#if PYTHON_VERSION >= 300
        const_str_digest_70f70e2fc470a2c13c317c77e2540e02,
#endif
        codeobj_f9197440d6673e79be8fbdc79473457c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        const_str_digest_0524aa7879841d3ca8f600d345f30f99,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_16_markdown2html_mistune(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_16_markdown2html_mistune,
        const_str_plain_markdown2html_mistune,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_255b62fc4305b050d1322fa8a127815e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        const_str_digest_10e290088cf6ad49dc621f6739f40407,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_1___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_610b22787e9990afc810a8c228534b90,
#endif
        codeobj_8ca5e55e5f3b0bcd65c0712daf524aa4,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_2_parse_multiline_math(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_2_parse_multiline_math,
        const_str_plain_parse_multiline_math,
#if PYTHON_VERSION >= 300
        const_str_digest_757d304647920670206b18097f89b560,
#endif
        codeobj_5f0627ed59aca43e93d4a1cd6f0c2afe,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        const_str_digest_7639c1606d5b8370bc20bdcf813ecfb7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_3___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_3___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_ef5aab8151cdd2f21b01cbe26730c413,
#endif
        codeobj_09ca13a5cc6332980b542bce4492de44,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_4_output_inline_math(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_4_output_inline_math,
        const_str_plain_output_inline_math,
#if PYTHON_VERSION >= 300
        const_str_digest_218c7a49ffb251f99b37f39dbd8b4537,
#endif
        codeobj_875a5fdc3cd36f971365d229668688fe,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_5_output_block_math(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_5_output_block_math,
        const_str_plain_output_block_math,
#if PYTHON_VERSION >= 300
        const_str_digest_1ef2097e6be2b5889e6a1c84bec73181,
#endif
        codeobj_62f2f005a414ee547964a5ca9b347c77,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_6_output_latex_environment(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_6_output_latex_environment,
        const_str_plain_output_latex_environment,
#if PYTHON_VERSION >= 300
        const_str_digest_2b5a4fa91b35a0840f1e7159057ed9d0,
#endif
        codeobj_83ba0bace56faadbd0c173ed53902629,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_7___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_7___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_3859a4497c34c2ec614a79f5d78001fe,
#endif
        codeobj_be97c2567b421c8ec3c78efe734539ec,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_8_output_multiline_math(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_8_output_multiline_math,
        const_str_plain_output_multiline_math,
#if PYTHON_VERSION >= 300
        const_str_digest_6f1742d33f705650262a93f5521fdc80,
#endif
        codeobj_c8ddd7c3b40a2956cf15c2271b442695,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_9_block_code(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown_mistune$$$function_9_block_code,
        const_str_plain_block_code,
#if PYTHON_VERSION >= 300
        const_str_digest_896a50bc98f51828cb4c4b89ca23ce3e,
#endif
        codeobj_961f29008d3947baa14057c78d8b0413,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown_mistune,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_nbconvert$filters$markdown_mistune =
{
    PyModuleDef_HEAD_INIT,
    "nbconvert.filters.markdown_mistune",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(nbconvert$filters$markdown_mistune)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(nbconvert$filters$markdown_mistune)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_nbconvert$filters$markdown_mistune );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("nbconvert.filters.markdown_mistune: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbconvert.filters.markdown_mistune: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbconvert.filters.markdown_mistune: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initnbconvert$filters$markdown_mistune" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_nbconvert$filters$markdown_mistune = Py_InitModule4(
        "nbconvert.filters.markdown_mistune",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_nbconvert$filters$markdown_mistune = PyModule_Create( &mdef_nbconvert$filters$markdown_mistune );
#endif

    moduledict_nbconvert$filters$markdown_mistune = MODULE_DICT( module_nbconvert$filters$markdown_mistune );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_nbconvert$filters$markdown_mistune,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_nbconvert$filters$markdown_mistune,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbconvert$filters$markdown_mistune,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbconvert$filters$markdown_mistune,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_nbconvert$filters$markdown_mistune );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_76b2174b385d7250ca7574c6411a6fc7, module_nbconvert$filters$markdown_mistune );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *outline_3_var___class__ = NULL;
    PyObject *outline_4_var___class__ = NULL;
    PyObject *outline_5_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_class_creation_4__bases = NULL;
    PyObject *tmp_class_creation_4__bases_orig = NULL;
    PyObject *tmp_class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_class_creation_4__metaclass = NULL;
    PyObject *tmp_class_creation_4__prepared = NULL;
    PyObject *tmp_class_creation_5__bases = NULL;
    PyObject *tmp_class_creation_5__bases_orig = NULL;
    PyObject *tmp_class_creation_5__class_decl_dict = NULL;
    PyObject *tmp_class_creation_5__metaclass = NULL;
    PyObject *tmp_class_creation_5__prepared = NULL;
    PyObject *tmp_class_creation_6__bases = NULL;
    PyObject *tmp_class_creation_6__bases_orig = NULL;
    PyObject *tmp_class_creation_6__class_decl_dict = NULL;
    PyObject *tmp_class_creation_6__metaclass = NULL;
    PyObject *tmp_class_creation_6__prepared = NULL;
    struct Nuitka_FrameObject *frame_f905ee16b1b38b38016191f300fb5513;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_nbconvert$filters$markdown_mistune_31 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_cfb9bfbe17146d73e81d3090ab9d1844_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_cfb9bfbe17146d73e81d3090ab9d1844_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *locals_nbconvert$filters$markdown_mistune_42 = NULL;
    struct Nuitka_FrameObject *frame_ef211c706c48a3d293c22d394c51d720_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_ef211c706c48a3d293c22d394c51d720_3 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *locals_nbconvert$filters$markdown_mistune_63 = NULL;
    struct Nuitka_FrameObject *frame_4f053877595d1f3290140afa99cb4656_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_4f053877595d1f3290140afa99cb4656_4 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *locals_nbconvert$filters$markdown_mistune_74 = NULL;
    struct Nuitka_FrameObject *frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    static struct Nuitka_FrameObject *cache_frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 = NULL;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *locals_nbconvert$filters$markdown_mistune_102 = NULL;
    struct Nuitka_FrameObject *frame_8e0c5d8fc4d243989626f7e72a0f84ee_6;
    NUITKA_MAY_BE_UNUSED char const *type_description_6 = NULL;
    static struct Nuitka_FrameObject *cache_frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 = NULL;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *locals_nbconvert$filters$markdown_mistune_115 = NULL;
    struct Nuitka_FrameObject *frame_6c9df3004a4f945b2a5cc5e4db74db3c_7;
    NUITKA_MAY_BE_UNUSED char const *type_description_7 = NULL;
    static struct Nuitka_FrameObject *cache_frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 = NULL;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_2a0f2b91133328f91d741d4c687d6005;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_f905ee16b1b38b38016191f300fb5513 = MAKE_MODULE_FRAME( codeobj_f905ee16b1b38b38016191f300fb5513, module_nbconvert$filters$markdown_mistune );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_f905ee16b1b38b38016191f300fb5513 );
    assert( Py_REFCNT( frame_f905ee16b1b38b38016191f300fb5513 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 9;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_print_function );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_print_function, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_re;
        tmp_globals_name_1 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 11;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_functools;
        tmp_globals_name_2 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_partial_tuple;
        tmp_level_name_2 = const_int_0;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 12;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_partial );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_partial, tmp_assign_source_6 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_html;
        tmp_globals_name_3 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_escape_tuple;
        tmp_level_name_3 = const_int_0;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 15;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_escape );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_escape, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_partial );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_partial );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "partial" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 16;

            goto try_except_handler_1;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_escape );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_escape );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_tuple_element_1 = tmp_mvar_value_4;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_f0003172cf42aa13871e07dd4396acc8 );
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 16;
        tmp_assign_source_8 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_html_escape, tmp_assign_source_8 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_f905ee16b1b38b38016191f300fb5513, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_f905ee16b1b38b38016191f300fb5513, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_import_name_from_4;
            PyObject *tmp_name_name_4;
            PyObject *tmp_globals_name_4;
            PyObject *tmp_locals_name_4;
            PyObject *tmp_fromlist_name_4;
            PyObject *tmp_level_name_4;
            tmp_name_name_4 = const_str_plain_cgi;
            tmp_globals_name_4 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
            tmp_locals_name_4 = Py_None;
            tmp_fromlist_name_4 = const_tuple_str_plain_escape_tuple;
            tmp_level_name_4 = const_int_0;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 19;
            tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
            if ( tmp_import_name_from_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 19;

                goto try_except_handler_2;
            }
            tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_escape );
            Py_DECREF( tmp_import_name_from_4 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 19;

                goto try_except_handler_2;
            }
            UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_html_escape, tmp_assign_source_9 );
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 14;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_f905ee16b1b38b38016191f300fb5513->m_frame) frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_2;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_mistune;
        tmp_globals_name_5 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 21;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_pygments;
        tmp_globals_name_6 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_highlight_tuple;
        tmp_level_name_6 = const_int_0;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 23;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_highlight );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_highlight, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_bc2c8ffc848309b89316fe6d6faf27c6;
        tmp_globals_name_7 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_get_lexer_by_name_tuple;
        tmp_level_name_7 = const_int_0;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 24;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_get_lexer_by_name );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_get_lexer_by_name, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_962b170f60f6eb52f0c210e4bcd8c1dc;
        tmp_globals_name_8 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_HtmlFormatter_tuple;
        tmp_level_name_8 = const_int_0;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 25;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_HtmlFormatter );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_HtmlFormatter, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_ce1b0803b9e228b4fef82f896164bef7;
        tmp_globals_name_9 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_ClassNotFound_tuple;
        tmp_level_name_9 = const_int_0;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 26;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_ClassNotFound );
        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_ClassNotFound, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_9;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_ac502099f2abb6fd9f3fb9c9824906b3;
        tmp_globals_name_10 = (PyObject *)moduledict_nbconvert$filters$markdown_mistune;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_add_anchor_tuple;
        tmp_level_name_10 = const_int_0;
        frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 28;
        tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_add_anchor );
        Py_DECREF( tmp_import_name_from_9 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_add_anchor, tmp_assign_source_15 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mistune );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mistune" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 31;

            goto try_except_handler_3;
        }

        tmp_source_name_1 = tmp_mvar_value_5;
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_BlockGrammar );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        tmp_assign_source_16 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_16, 0, tmp_tuple_element_2 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_16;
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_17 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_17;
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_18;
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_19 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_19;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_2 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_2, const_str_plain___prepare__ );
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_3 = tmp_class_creation_1__metaclass;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___prepare__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 31;

                goto try_except_handler_3;
            }
            tmp_tuple_element_3 = const_str_plain_MathBlockGrammar;
            tmp_args_name_2 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 31;
            tmp_assign_source_20 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 31;

                goto try_except_handler_3;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_20;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_4 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_4, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 31;

                goto try_except_handler_3;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_4;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_5;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_4 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 31;

                    goto try_except_handler_3;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_4 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_5 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_5 == NULL) );
                tmp_tuple_element_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_tuple_element_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 31;

                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_4 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 31;

                    goto try_except_handler_3;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 31;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_3;
            }
            branch_no_4:;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_21;
            tmp_assign_source_21 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_21;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_22;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_nbconvert$filters$markdown_mistune_31 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_76b2174b385d7250ca7574c6411a6fc7;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_31, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_digest_9176c6761cabb67f1001474fcf51e369;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_31, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_plain_MathBlockGrammar;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_31, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_cfb9bfbe17146d73e81d3090ab9d1844_2, codeobj_cfb9bfbe17146d73e81d3090ab9d1844, module_nbconvert$filters$markdown_mistune, sizeof(void *) );
        frame_cfb9bfbe17146d73e81d3090ab9d1844_2 = cache_frame_cfb9bfbe17146d73e81d3090ab9d1844_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_cfb9bfbe17146d73e81d3090ab9d1844_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_cfb9bfbe17146d73e81d3090ab9d1844_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_arg_element_1;
            tmp_called_instance_1 = const_str_chr_124;
            tmp_call_arg_element_1 = LIST_COPY( const_list_a78b385ba4e09462bd434d905623c3c4_list );
            frame_cfb9bfbe17146d73e81d3090ab9d1844_2->m_frame.f_lineno = 36;
            {
                PyObject *call_args[] = { tmp_call_arg_element_1 };
                tmp_dictset_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_join, call_args );
            }

            Py_DECREF( tmp_call_arg_element_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_31, const_str_plain_multi_math_str, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_7;
            tmp_source_name_6 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_31, const_str_plain_re );

            if ( tmp_source_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 39;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_6 = tmp_mvar_value_6;
                Py_INCREF( tmp_source_name_6 );
                }
            }

            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_compile );
            Py_DECREF( tmp_source_name_6 );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_1 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_31, const_str_plain_multi_math_str );

            if ( tmp_args_element_name_1 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "multi_math_str" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 39;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 39;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_source_name_7 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_31, const_str_plain_re );

            if ( tmp_source_name_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_7 == NULL )
                {
                    Py_DECREF( tmp_called_name_3 );
                    Py_DECREF( tmp_args_element_name_1 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 39;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_7 = tmp_mvar_value_7;
                Py_INCREF( tmp_source_name_7 );
                }
            }

            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_DOTALL );
            Py_DECREF( tmp_source_name_7 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_args_element_name_1 );

                exception_lineno = 39;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            frame_cfb9bfbe17146d73e81d3090ab9d1844_2->m_frame.f_lineno = 39;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_31, const_str_plain_multiline_math, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_cfb9bfbe17146d73e81d3090ab9d1844_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_cfb9bfbe17146d73e81d3090ab9d1844_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_cfb9bfbe17146d73e81d3090ab9d1844_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_cfb9bfbe17146d73e81d3090ab9d1844_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_cfb9bfbe17146d73e81d3090ab9d1844_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_cfb9bfbe17146d73e81d3090ab9d1844_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_cfb9bfbe17146d73e81d3090ab9d1844_2 == cache_frame_cfb9bfbe17146d73e81d3090ab9d1844_2 )
        {
            Py_DECREF( frame_cfb9bfbe17146d73e81d3090ab9d1844_2 );
        }
        cache_frame_cfb9bfbe17146d73e81d3090ab9d1844_2 = NULL;

        assertFrameObject( frame_cfb9bfbe17146d73e81d3090ab9d1844_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_5;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_2 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 31;

                goto try_except_handler_5;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_31, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 31;

                goto try_except_handler_5;
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_4 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_5 = const_str_plain_MathBlockGrammar;
            tmp_args_name_3 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_5 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
            tmp_tuple_element_5 = locals_nbconvert$filters$markdown_mistune_31;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 2, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_1__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 31;
            tmp_assign_source_23 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 31;

                goto try_except_handler_5;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_23;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_22 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_22 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        Py_DECREF( locals_nbconvert$filters$markdown_mistune_31 );
        locals_nbconvert$filters$markdown_mistune_31 = NULL;
        goto try_return_handler_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbconvert$filters$markdown_mistune_31 );
        locals_nbconvert$filters$markdown_mistune_31 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 31;
        goto try_except_handler_3;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathBlockGrammar, tmp_assign_source_22 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_tuple_element_6;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mistune );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mistune" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;

            goto try_except_handler_6;
        }

        tmp_source_name_8 = tmp_mvar_value_8;
        tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_BlockLexer );
        if ( tmp_tuple_element_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_assign_source_24 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_24, 0, tmp_tuple_element_6 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_24;
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_25 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_25;
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_26;
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_condition_result_9 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_27 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_27;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_9 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___prepare__ );
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_10;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_10 = tmp_class_creation_2__metaclass;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___prepare__ );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_6;
            }
            tmp_tuple_element_7 = const_str_plain_MathBlockLexer;
            tmp_args_name_4 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_7 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 42;
            tmp_assign_source_28 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_6;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_28;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_11 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_11, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_6;
            }
            tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_8;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_12;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_8 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 42;

                    goto try_except_handler_6;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_8 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_12 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_12 == NULL) );
                tmp_tuple_element_8 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_12 );
                if ( tmp_tuple_element_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 42;

                    goto try_except_handler_6;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_8 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 42;

                    goto try_except_handler_6;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 42;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_6;
            }
            branch_no_8:;
        }
        goto branch_end_7;
        branch_no_7:;
        {
            PyObject *tmp_assign_source_29;
            tmp_assign_source_29 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_29;
        }
        branch_end_7:;
    }
    {
        PyObject *tmp_assign_source_30;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_nbconvert$filters$markdown_mistune_42 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_76b2174b385d7250ca7574c6411a6fc7;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_42, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_digest_524ef2db848d8af58cf0d46ca6216c8b;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_42, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_plain_MathBlockLexer;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_42, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_8;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_ef211c706c48a3d293c22d394c51d720_3, codeobj_ef211c706c48a3d293c22d394c51d720, module_nbconvert$filters$markdown_mistune, sizeof(void *) );
        frame_ef211c706c48a3d293c22d394c51d720_3 = cache_frame_ef211c706c48a3d293c22d394c51d720_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_ef211c706c48a3d293c22d394c51d720_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_ef211c706c48a3d293c22d394c51d720_3 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            PyObject *tmp_source_name_13;
            PyObject *tmp_source_name_14;
            PyObject *tmp_mvar_value_9;
            tmp_left_name_3 = LIST_COPY( const_list_str_plain_multiline_math_list );
            tmp_source_name_14 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_42, const_str_plain_mistune );

            if ( tmp_source_name_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mistune );
                }

                if ( tmp_mvar_value_9 == NULL )
                {
                    Py_DECREF( tmp_left_name_3 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mistune" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 48;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_source_name_14 = tmp_mvar_value_9;
                Py_INCREF( tmp_source_name_14 );
                }
            }

            tmp_source_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_BlockLexer );
            Py_DECREF( tmp_source_name_14 );
            if ( tmp_source_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_3 );

                exception_lineno = 48;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_right_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_default_rules );
            Py_DECREF( tmp_source_name_13 );
            if ( tmp_right_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_3 );

                exception_lineno = 48;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_dictset_value = BINARY_OPERATION_ADD_LIST_OBJECT( tmp_left_name_3, tmp_right_name_3 );
            Py_DECREF( tmp_left_name_3 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 47;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_42, const_str_plain_default_rules, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 47;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_1___init__( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_42, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_2_parse_multiline_math(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_42, const_str_plain_parse_multiline_math, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ef211c706c48a3d293c22d394c51d720_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ef211c706c48a3d293c22d394c51d720_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_ef211c706c48a3d293c22d394c51d720_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_ef211c706c48a3d293c22d394c51d720_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_ef211c706c48a3d293c22d394c51d720_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_ef211c706c48a3d293c22d394c51d720_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_ef211c706c48a3d293c22d394c51d720_3 == cache_frame_ef211c706c48a3d293c22d394c51d720_3 )
        {
            Py_DECREF( frame_ef211c706c48a3d293c22d394c51d720_3 );
        }
        cache_frame_ef211c706c48a3d293c22d394c51d720_3 = NULL;

        assertFrameObject( frame_ef211c706c48a3d293c22d394c51d720_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_8;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_3 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_8;
            }
            tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_42, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_8;
            }
            branch_no_9:;
        }
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_6 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_9 = const_str_plain_MathBlockLexer;
            tmp_args_name_5 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_9 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_9 );
            tmp_tuple_element_9 = locals_nbconvert$filters$markdown_mistune_42;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_5, 2, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_2__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 42;
            tmp_assign_source_31 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_8;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_31;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_30 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_30 );
        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        Py_DECREF( locals_nbconvert$filters$markdown_mistune_42 );
        locals_nbconvert$filters$markdown_mistune_42 = NULL;
        goto try_return_handler_7;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbconvert$filters$markdown_mistune_42 );
        locals_nbconvert$filters$markdown_mistune_42 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 42;
        goto try_except_handler_6;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathBlockLexer, tmp_assign_source_30 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_tuple_element_10;
        PyObject *tmp_source_name_15;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mistune );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mistune" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;

            goto try_except_handler_9;
        }

        tmp_source_name_15 = tmp_mvar_value_10;
        tmp_tuple_element_10 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_InlineGrammar );
        if ( tmp_tuple_element_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        tmp_assign_source_32 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_32, 0, tmp_tuple_element_10 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_dircall_arg1_3;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_33 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_33;
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_34;
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_15;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        tmp_condition_result_15 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_3 = tmp_class_creation_3__bases;
        tmp_subscript_name_3 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_6:;
        condexpr_end_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_35 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_35;
    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        branch_no_10:;
    }
    {
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_source_name_16;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_16 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_16, const_str_plain___prepare__ );
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_called_name_7;
            PyObject *tmp_source_name_17;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_11;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_17 = tmp_class_creation_3__metaclass;
            tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain___prepare__ );
            if ( tmp_called_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_9;
            }
            tmp_tuple_element_11 = const_str_plain_MathInlineGrammar;
            tmp_args_name_6 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_11 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 63;
            tmp_assign_source_36 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_9;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_36;
        }
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_18;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_18 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_18, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_9;
            }
            tmp_condition_result_18 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_4;
                PyObject *tmp_right_name_4;
                PyObject *tmp_tuple_element_12;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_19;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_4 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_12 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 63;

                    goto try_except_handler_9;
                }
                tmp_right_name_4 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_12 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_19 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_19 == NULL) );
                tmp_tuple_element_12 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_19 );
                if ( tmp_tuple_element_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_4 );

                    exception_lineno = 63;

                    goto try_except_handler_9;
                }
                PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_12 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                Py_DECREF( tmp_right_name_4 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 63;

                    goto try_except_handler_9;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 63;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_9;
            }
            branch_no_12:;
        }
        goto branch_end_11;
        branch_no_11:;
        {
            PyObject *tmp_assign_source_37;
            tmp_assign_source_37 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_37;
        }
        branch_end_11:;
    }
    {
        PyObject *tmp_assign_source_38;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_nbconvert$filters$markdown_mistune_63 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_76b2174b385d7250ca7574c6411a6fc7;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_11;
        }
        tmp_dictset_value = const_str_digest_4536f123e7c367dea19c325aba794559;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_11;
        }
        tmp_dictset_value = const_str_plain_MathInlineGrammar;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_11;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_4f053877595d1f3290140afa99cb4656_4, codeobj_4f053877595d1f3290140afa99cb4656, module_nbconvert$filters$markdown_mistune, sizeof(void *) );
        frame_4f053877595d1f3290140afa99cb4656_4 = cache_frame_4f053877595d1f3290140afa99cb4656_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_4f053877595d1f3290140afa99cb4656_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_4f053877595d1f3290140afa99cb4656_4 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_called_name_8;
            PyObject *tmp_source_name_20;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_source_name_21;
            PyObject *tmp_mvar_value_12;
            tmp_source_name_20 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_re );

            if ( tmp_source_name_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 67;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_20 = tmp_mvar_value_11;
                Py_INCREF( tmp_source_name_20 );
                }
            }

            tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_compile );
            Py_DECREF( tmp_source_name_20 );
            if ( tmp_called_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_3 = const_str_digest_9a541cdf94e22855513e5462b2983775;
            tmp_source_name_21 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_re );

            if ( tmp_source_name_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_12 == NULL )
                {
                    Py_DECREF( tmp_called_name_8 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 67;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_21 = tmp_mvar_value_12;
                Py_INCREF( tmp_source_name_21 );
                }
            }

            tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_DOTALL );
            Py_DECREF( tmp_source_name_21 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_8 );

                exception_lineno = 67;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            frame_4f053877595d1f3290140afa99cb4656_4->m_frame.f_lineno = 67;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_inline_math, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_called_name_9;
            PyObject *tmp_source_name_22;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_source_name_23;
            PyObject *tmp_mvar_value_14;
            tmp_source_name_22 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_re );

            if ( tmp_source_name_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_13 == NULL ))
                {
                    tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_13 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 68;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_22 = tmp_mvar_value_13;
                Py_INCREF( tmp_source_name_22 );
                }
            }

            tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_compile );
            Py_DECREF( tmp_source_name_22 );
            if ( tmp_called_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_5 = const_str_digest_ee3d8a1be3441fe200176fa93a9ebafd;
            tmp_source_name_23 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_re );

            if ( tmp_source_name_23 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_14 == NULL ))
                {
                    tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_14 == NULL )
                {
                    Py_DECREF( tmp_called_name_9 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 68;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_23 = tmp_mvar_value_14;
                Py_INCREF( tmp_source_name_23 );
                }
            }

            tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_DOTALL );
            Py_DECREF( tmp_source_name_23 );
            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_9 );

                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            frame_4f053877595d1f3290140afa99cb4656_4->m_frame.f_lineno = 68;
            {
                PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, call_args );
            }

            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_block_math, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_called_name_10;
            PyObject *tmp_source_name_24;
            PyObject *tmp_mvar_value_15;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_source_name_25;
            PyObject *tmp_mvar_value_16;
            tmp_source_name_24 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_re );

            if ( tmp_source_name_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_15 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 69;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_24 = tmp_mvar_value_15;
                Py_INCREF( tmp_source_name_24 );
                }
            }

            tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_compile );
            Py_DECREF( tmp_source_name_24 );
            if ( tmp_called_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_7 = const_str_digest_4fd35e0644fa85b2e9419fda3117b018;
            tmp_source_name_25 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_re );

            if ( tmp_source_name_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_16 == NULL ))
                {
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_16 == NULL )
                {
                    Py_DECREF( tmp_called_name_10 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 70;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_25 = tmp_mvar_value_16;
                Py_INCREF( tmp_source_name_25 );
                }
            }

            tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_DOTALL );
            Py_DECREF( tmp_source_name_25 );
            if ( tmp_args_element_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_10 );

                exception_lineno = 70;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            frame_4f053877595d1f3290140afa99cb4656_4->m_frame.f_lineno = 69;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, call_args );
            }

            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_latex_environment, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_17;
            tmp_called_instance_2 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_re );

            if ( tmp_called_instance_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_17 == NULL ))
                {
                    tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_17 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 71;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_called_instance_2 = tmp_mvar_value_17;
                Py_INCREF( tmp_called_instance_2 );
                }
            }

            frame_4f053877595d1f3290140afa99cb4656_4->m_frame.f_lineno = 71;
            tmp_dictset_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_ca49b59e4b6e238fe9964d441afd018b_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain_text, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_4f053877595d1f3290140afa99cb4656_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_4f053877595d1f3290140afa99cb4656_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_4f053877595d1f3290140afa99cb4656_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_4f053877595d1f3290140afa99cb4656_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_4f053877595d1f3290140afa99cb4656_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_4f053877595d1f3290140afa99cb4656_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_4f053877595d1f3290140afa99cb4656_4 == cache_frame_4f053877595d1f3290140afa99cb4656_4 )
        {
            Py_DECREF( frame_4f053877595d1f3290140afa99cb4656_4 );
        }
        cache_frame_4f053877595d1f3290140afa99cb4656_4 = NULL;

        assertFrameObject( frame_4f053877595d1f3290140afa99cb4656_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_11;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_4 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_4 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_11;
            }
            tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_13;
            }
            else
            {
                goto branch_no_13;
            }
            branch_yes_13:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_63, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_11;
            }
            branch_no_13:;
        }
        {
            PyObject *tmp_assign_source_39;
            PyObject *tmp_called_name_11;
            PyObject *tmp_args_name_7;
            PyObject *tmp_tuple_element_13;
            PyObject *tmp_kw_name_7;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_11 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_13 = const_str_plain_MathInlineGrammar;
            tmp_args_name_7 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_13 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_13 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_13 );
            tmp_tuple_element_13 = locals_nbconvert$filters$markdown_mistune_63;
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_7, 2, tmp_tuple_element_13 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_7 = tmp_class_creation_3__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 63;
            tmp_assign_source_39 = CALL_FUNCTION( tmp_called_name_11, tmp_args_name_7, tmp_kw_name_7 );
            Py_DECREF( tmp_args_name_7 );
            if ( tmp_assign_source_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_11;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_39;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_38 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_38 );
        goto try_return_handler_11;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_11:;
        Py_DECREF( locals_nbconvert$filters$markdown_mistune_63 );
        locals_nbconvert$filters$markdown_mistune_63 = NULL;
        goto try_return_handler_10;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbconvert$filters$markdown_mistune_63 );
        locals_nbconvert$filters$markdown_mistune_63 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_10;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 63;
        goto try_except_handler_9;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathInlineGrammar, tmp_assign_source_38 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_tuple_element_14;
        PyObject *tmp_source_name_26;
        PyObject *tmp_mvar_value_18;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mistune );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mistune" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 74;

            goto try_except_handler_12;
        }

        tmp_source_name_26 = tmp_mvar_value_18;
        tmp_tuple_element_14 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_InlineLexer );
        if ( tmp_tuple_element_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        tmp_assign_source_40 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_40, 0, tmp_tuple_element_14 );
        assert( tmp_class_creation_4__bases_orig == NULL );
        tmp_class_creation_4__bases_orig = tmp_assign_source_40;
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_dircall_arg1_4;
        CHECK_OBJECT( tmp_class_creation_4__bases_orig );
        tmp_dircall_arg1_4 = tmp_class_creation_4__bases_orig;
        Py_INCREF( tmp_dircall_arg1_4 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_4};
            tmp_assign_source_41 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        assert( tmp_class_creation_4__bases == NULL );
        tmp_class_creation_4__bases = tmp_assign_source_41;
    }
    {
        PyObject *tmp_assign_source_42;
        tmp_assign_source_42 = PyDict_New();
        assert( tmp_class_creation_4__class_decl_dict == NULL );
        tmp_class_creation_4__class_decl_dict = tmp_assign_source_42;
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_metaclass_name_4;
        nuitka_bool tmp_condition_result_20;
        PyObject *tmp_key_name_10;
        PyObject *tmp_dict_name_10;
        PyObject *tmp_dict_name_11;
        PyObject *tmp_key_name_11;
        nuitka_bool tmp_condition_result_21;
        int tmp_truth_name_4;
        PyObject *tmp_type_arg_7;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_bases_name_4;
        tmp_key_name_10 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_10 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_10, tmp_key_name_10 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        tmp_condition_result_20 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_7;
        }
        else
        {
            goto condexpr_false_7;
        }
        condexpr_true_7:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_11 = tmp_class_creation_4__class_decl_dict;
        tmp_key_name_11 = const_str_plain_metaclass;
        tmp_metaclass_name_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        goto condexpr_end_7;
        condexpr_false_7:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_class_creation_4__bases );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        tmp_condition_result_21 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_8;
        }
        else
        {
            goto condexpr_false_8;
        }
        condexpr_true_8:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_subscribed_name_4 = tmp_class_creation_4__bases;
        tmp_subscript_name_4 = const_int_0;
        tmp_type_arg_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
        if ( tmp_type_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        tmp_metaclass_name_4 = BUILTIN_TYPE1( tmp_type_arg_7 );
        Py_DECREF( tmp_type_arg_7 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        goto condexpr_end_8;
        condexpr_false_8:;
        tmp_metaclass_name_4 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_4 );
        condexpr_end_8:;
        condexpr_end_7:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_bases_name_4 = tmp_class_creation_4__bases;
        tmp_assign_source_43 = SELECT_METACLASS( tmp_metaclass_name_4, tmp_bases_name_4 );
        Py_DECREF( tmp_metaclass_name_4 );
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        assert( tmp_class_creation_4__metaclass == NULL );
        tmp_class_creation_4__metaclass = tmp_assign_source_43;
    }
    {
        nuitka_bool tmp_condition_result_22;
        PyObject *tmp_key_name_12;
        PyObject *tmp_dict_name_12;
        tmp_key_name_12 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_12 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_12, tmp_key_name_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        tmp_condition_result_22 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_4__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_12;
        }
        branch_no_14:;
    }
    {
        nuitka_bool tmp_condition_result_23;
        PyObject *tmp_source_name_27;
        CHECK_OBJECT( tmp_class_creation_4__metaclass );
        tmp_source_name_27 = tmp_class_creation_4__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_27, const_str_plain___prepare__ );
        tmp_condition_result_23 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_15;
        }
        else
        {
            goto branch_no_15;
        }
        branch_yes_15:;
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_called_name_12;
            PyObject *tmp_source_name_28;
            PyObject *tmp_args_name_8;
            PyObject *tmp_tuple_element_15;
            PyObject *tmp_kw_name_8;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_source_name_28 = tmp_class_creation_4__metaclass;
            tmp_called_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain___prepare__ );
            if ( tmp_called_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;

                goto try_except_handler_12;
            }
            tmp_tuple_element_15 = const_str_plain_MathInlineLexer;
            tmp_args_name_8 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_15 );
            PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_15 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_15 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_15 );
            PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_15 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_8 = tmp_class_creation_4__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 74;
            tmp_assign_source_44 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_8, tmp_kw_name_8 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_args_name_8 );
            if ( tmp_assign_source_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;

                goto try_except_handler_12;
            }
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_44;
        }
        {
            nuitka_bool tmp_condition_result_24;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_source_name_29;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_source_name_29 = tmp_class_creation_4__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_29, const_str_plain___getitem__ );
            tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;

                goto try_except_handler_12;
            }
            tmp_condition_result_24 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_16;
            }
            else
            {
                goto branch_no_16;
            }
            branch_yes_16:;
            {
                PyObject *tmp_raise_type_4;
                PyObject *tmp_raise_value_4;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                PyObject *tmp_tuple_element_16;
                PyObject *tmp_getattr_target_4;
                PyObject *tmp_getattr_attr_4;
                PyObject *tmp_getattr_default_4;
                PyObject *tmp_source_name_30;
                PyObject *tmp_type_arg_8;
                tmp_raise_type_4 = PyExc_TypeError;
                tmp_left_name_5 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_4__metaclass );
                tmp_getattr_target_4 = tmp_class_creation_4__metaclass;
                tmp_getattr_attr_4 = const_str_plain___name__;
                tmp_getattr_default_4 = const_str_angle_metaclass;
                tmp_tuple_element_16 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, tmp_getattr_default_4 );
                if ( tmp_tuple_element_16 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 74;

                    goto try_except_handler_12;
                }
                tmp_right_name_5 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_5, 0, tmp_tuple_element_16 );
                CHECK_OBJECT( tmp_class_creation_4__prepared );
                tmp_type_arg_8 = tmp_class_creation_4__prepared;
                tmp_source_name_30 = BUILTIN_TYPE1( tmp_type_arg_8 );
                assert( !(tmp_source_name_30 == NULL) );
                tmp_tuple_element_16 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_30 );
                if ( tmp_tuple_element_16 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_5 );

                    exception_lineno = 74;

                    goto try_except_handler_12;
                }
                PyTuple_SET_ITEM( tmp_right_name_5, 1, tmp_tuple_element_16 );
                tmp_raise_value_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                Py_DECREF( tmp_right_name_5 );
                if ( tmp_raise_value_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 74;

                    goto try_except_handler_12;
                }
                exception_type = tmp_raise_type_4;
                Py_INCREF( tmp_raise_type_4 );
                exception_value = tmp_raise_value_4;
                exception_lineno = 74;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_12;
            }
            branch_no_16:;
        }
        goto branch_end_15;
        branch_no_15:;
        {
            PyObject *tmp_assign_source_45;
            tmp_assign_source_45 = PyDict_New();
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_45;
        }
        branch_end_15:;
    }
    {
        PyObject *tmp_assign_source_46;
        {
            PyObject *tmp_set_locals_4;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_set_locals_4 = tmp_class_creation_4__prepared;
            locals_nbconvert$filters$markdown_mistune_74 = tmp_set_locals_4;
            Py_INCREF( tmp_set_locals_4 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_76b2174b385d7250ca7574c6411a6fc7;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_14;
        }
        tmp_dictset_value = const_str_digest_fddf68e6c71bef586b98da046f2d1410;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_14;
        }
        tmp_dictset_value = const_str_plain_MathInlineLexer;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto try_except_handler_14;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5, codeobj_7fdb20c7499f4b745ba4af2c59bcdbb0, module_nbconvert$filters$markdown_mistune, sizeof(void *) );
        frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 = cache_frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_left_name_6;
            PyObject *tmp_right_name_6;
            PyObject *tmp_source_name_31;
            PyObject *tmp_source_name_32;
            PyObject *tmp_mvar_value_19;
            tmp_left_name_6 = LIST_COPY( const_list_10e6d592a9132363948ffc7cfaafc909_list );
            tmp_source_name_32 = PyObject_GetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain_mistune );

            if ( tmp_source_name_32 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune );

                if (unlikely( tmp_mvar_value_19 == NULL ))
                {
                    tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mistune );
                }

                if ( tmp_mvar_value_19 == NULL )
                {
                    Py_DECREF( tmp_left_name_6 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mistune" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 84;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_source_name_32 = tmp_mvar_value_19;
                Py_INCREF( tmp_source_name_32 );
                }
            }

            tmp_source_name_31 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_InlineLexer );
            Py_DECREF( tmp_source_name_32 );
            if ( tmp_source_name_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_6 );

                exception_lineno = 84;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_right_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_default_rules );
            Py_DECREF( tmp_source_name_31 );
            if ( tmp_right_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_6 );

                exception_lineno = 84;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_dictset_value = BINARY_OPERATION_ADD_LIST_OBJECT( tmp_left_name_6, tmp_right_name_6 );
            Py_DECREF( tmp_left_name_6 );
            Py_DECREF( tmp_right_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain_default_rules, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        {
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_3___init__( tmp_defaults_2 );



            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_4_output_inline_math(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain_output_inline_math, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_5_output_block_math(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain_output_block_math, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_6_output_latex_environment(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain_output_latex_environment, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5,
            type_description_2,
            outline_3_var___class__
        );


        // Release cached frame.
        if ( frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 == cache_frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 )
        {
            Py_DECREF( frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 );
        }
        cache_frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 = NULL;

        assertFrameObject( frame_7fdb20c7499f4b745ba4af2c59bcdbb0_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_4:;

        goto try_except_handler_14;
        skip_nested_handling_4:;
        {
            nuitka_bool tmp_condition_result_25;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_compexpr_left_5 = tmp_class_creation_4__bases;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_compexpr_right_5 = tmp_class_creation_4__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;

                goto try_except_handler_14;
            }
            tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_17;
            }
            else
            {
                goto branch_no_17;
            }
            branch_yes_17:;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_dictset_value = tmp_class_creation_4__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_74, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;

                goto try_except_handler_14;
            }
            branch_no_17:;
        }
        {
            PyObject *tmp_assign_source_47;
            PyObject *tmp_called_name_13;
            PyObject *tmp_args_name_9;
            PyObject *tmp_tuple_element_17;
            PyObject *tmp_kw_name_9;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_called_name_13 = tmp_class_creation_4__metaclass;
            tmp_tuple_element_17 = const_str_plain_MathInlineLexer;
            tmp_args_name_9 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_17 );
            PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_17 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_17 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_17 );
            PyTuple_SET_ITEM( tmp_args_name_9, 1, tmp_tuple_element_17 );
            tmp_tuple_element_17 = locals_nbconvert$filters$markdown_mistune_74;
            Py_INCREF( tmp_tuple_element_17 );
            PyTuple_SET_ITEM( tmp_args_name_9, 2, tmp_tuple_element_17 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_9 = tmp_class_creation_4__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 74;
            tmp_assign_source_47 = CALL_FUNCTION( tmp_called_name_13, tmp_args_name_9, tmp_kw_name_9 );
            Py_DECREF( tmp_args_name_9 );
            if ( tmp_assign_source_47 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;

                goto try_except_handler_14;
            }
            assert( outline_3_var___class__ == NULL );
            outline_3_var___class__ = tmp_assign_source_47;
        }
        CHECK_OBJECT( outline_3_var___class__ );
        tmp_assign_source_46 = outline_3_var___class__;
        Py_INCREF( tmp_assign_source_46 );
        goto try_return_handler_14;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_14:;
        Py_DECREF( locals_nbconvert$filters$markdown_mistune_74 );
        locals_nbconvert$filters$markdown_mistune_74 = NULL;
        goto try_return_handler_13;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbconvert$filters$markdown_mistune_74 );
        locals_nbconvert$filters$markdown_mistune_74 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto try_except_handler_13;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_13:;
        CHECK_OBJECT( (PyObject *)outline_3_var___class__ );
        Py_DECREF( outline_3_var___class__ );
        outline_3_var___class__ = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_4:;
        exception_lineno = 74;
        goto try_except_handler_12;
        outline_result_4:;
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MathInlineLexer, tmp_assign_source_46 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    Py_XDECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases_orig );
    Py_DECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases );
    Py_DECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__class_decl_dict );
    Py_DECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__metaclass );
    Py_DECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__prepared );
    Py_DECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_tuple_element_18;
        PyObject *tmp_source_name_33;
        PyObject *tmp_mvar_value_20;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mistune );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mistune" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;

            goto try_except_handler_15;
        }

        tmp_source_name_33 = tmp_mvar_value_20;
        tmp_tuple_element_18 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_Markdown );
        if ( tmp_tuple_element_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        tmp_assign_source_48 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_48, 0, tmp_tuple_element_18 );
        assert( tmp_class_creation_5__bases_orig == NULL );
        tmp_class_creation_5__bases_orig = tmp_assign_source_48;
    }
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_dircall_arg1_5;
        CHECK_OBJECT( tmp_class_creation_5__bases_orig );
        tmp_dircall_arg1_5 = tmp_class_creation_5__bases_orig;
        Py_INCREF( tmp_dircall_arg1_5 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_5};
            tmp_assign_source_49 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        assert( tmp_class_creation_5__bases == NULL );
        tmp_class_creation_5__bases = tmp_assign_source_49;
    }
    {
        PyObject *tmp_assign_source_50;
        tmp_assign_source_50 = PyDict_New();
        assert( tmp_class_creation_5__class_decl_dict == NULL );
        tmp_class_creation_5__class_decl_dict = tmp_assign_source_50;
    }
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_metaclass_name_5;
        nuitka_bool tmp_condition_result_26;
        PyObject *tmp_key_name_13;
        PyObject *tmp_dict_name_13;
        PyObject *tmp_dict_name_14;
        PyObject *tmp_key_name_14;
        nuitka_bool tmp_condition_result_27;
        int tmp_truth_name_5;
        PyObject *tmp_type_arg_9;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_bases_name_5;
        tmp_key_name_13 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_13 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_13, tmp_key_name_13 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        tmp_condition_result_26 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_9;
        }
        else
        {
            goto condexpr_false_9;
        }
        condexpr_true_9:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_14 = tmp_class_creation_5__class_decl_dict;
        tmp_key_name_14 = const_str_plain_metaclass;
        tmp_metaclass_name_5 = DICT_GET_ITEM( tmp_dict_name_14, tmp_key_name_14 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        goto condexpr_end_9;
        condexpr_false_9:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_class_creation_5__bases );
        if ( tmp_truth_name_5 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        tmp_condition_result_27 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_10;
        }
        else
        {
            goto condexpr_false_10;
        }
        condexpr_true_10:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_subscribed_name_5 = tmp_class_creation_5__bases;
        tmp_subscript_name_5 = const_int_0;
        tmp_type_arg_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 0 );
        if ( tmp_type_arg_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        tmp_metaclass_name_5 = BUILTIN_TYPE1( tmp_type_arg_9 );
        Py_DECREF( tmp_type_arg_9 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        goto condexpr_end_10;
        condexpr_false_10:;
        tmp_metaclass_name_5 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_5 );
        condexpr_end_10:;
        condexpr_end_9:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_bases_name_5 = tmp_class_creation_5__bases;
        tmp_assign_source_51 = SELECT_METACLASS( tmp_metaclass_name_5, tmp_bases_name_5 );
        Py_DECREF( tmp_metaclass_name_5 );
        if ( tmp_assign_source_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        assert( tmp_class_creation_5__metaclass == NULL );
        tmp_class_creation_5__metaclass = tmp_assign_source_51;
    }
    {
        nuitka_bool tmp_condition_result_28;
        PyObject *tmp_key_name_15;
        PyObject *tmp_dict_name_15;
        tmp_key_name_15 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_15 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_15, tmp_key_name_15 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        tmp_condition_result_28 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_28 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_18;
        }
        else
        {
            goto branch_no_18;
        }
        branch_yes_18:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_5__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_15;
        }
        branch_no_18:;
    }
    {
        nuitka_bool tmp_condition_result_29;
        PyObject *tmp_source_name_34;
        CHECK_OBJECT( tmp_class_creation_5__metaclass );
        tmp_source_name_34 = tmp_class_creation_5__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_34, const_str_plain___prepare__ );
        tmp_condition_result_29 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_29 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_19;
        }
        else
        {
            goto branch_no_19;
        }
        branch_yes_19:;
        {
            PyObject *tmp_assign_source_52;
            PyObject *tmp_called_name_14;
            PyObject *tmp_source_name_35;
            PyObject *tmp_args_name_10;
            PyObject *tmp_tuple_element_19;
            PyObject *tmp_kw_name_10;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_source_name_35 = tmp_class_creation_5__metaclass;
            tmp_called_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain___prepare__ );
            if ( tmp_called_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;

                goto try_except_handler_15;
            }
            tmp_tuple_element_19 = const_str_plain_MarkdownWithMath;
            tmp_args_name_10 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_19 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_19 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_args_name_10, 1, tmp_tuple_element_19 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_10 = tmp_class_creation_5__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 102;
            tmp_assign_source_52 = CALL_FUNCTION( tmp_called_name_14, tmp_args_name_10, tmp_kw_name_10 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_args_name_10 );
            if ( tmp_assign_source_52 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;

                goto try_except_handler_15;
            }
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_52;
        }
        {
            nuitka_bool tmp_condition_result_30;
            PyObject *tmp_operand_name_5;
            PyObject *tmp_source_name_36;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_source_name_36 = tmp_class_creation_5__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_36, const_str_plain___getitem__ );
            tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;

                goto try_except_handler_15;
            }
            tmp_condition_result_30 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_30 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            {
                PyObject *tmp_raise_type_5;
                PyObject *tmp_raise_value_5;
                PyObject *tmp_left_name_7;
                PyObject *tmp_right_name_7;
                PyObject *tmp_tuple_element_20;
                PyObject *tmp_getattr_target_5;
                PyObject *tmp_getattr_attr_5;
                PyObject *tmp_getattr_default_5;
                PyObject *tmp_source_name_37;
                PyObject *tmp_type_arg_10;
                tmp_raise_type_5 = PyExc_TypeError;
                tmp_left_name_7 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_5__metaclass );
                tmp_getattr_target_5 = tmp_class_creation_5__metaclass;
                tmp_getattr_attr_5 = const_str_plain___name__;
                tmp_getattr_default_5 = const_str_angle_metaclass;
                tmp_tuple_element_20 = BUILTIN_GETATTR( tmp_getattr_target_5, tmp_getattr_attr_5, tmp_getattr_default_5 );
                if ( tmp_tuple_element_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 102;

                    goto try_except_handler_15;
                }
                tmp_right_name_7 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_7, 0, tmp_tuple_element_20 );
                CHECK_OBJECT( tmp_class_creation_5__prepared );
                tmp_type_arg_10 = tmp_class_creation_5__prepared;
                tmp_source_name_37 = BUILTIN_TYPE1( tmp_type_arg_10 );
                assert( !(tmp_source_name_37 == NULL) );
                tmp_tuple_element_20 = LOOKUP_ATTRIBUTE( tmp_source_name_37, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_37 );
                if ( tmp_tuple_element_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_7 );

                    exception_lineno = 102;

                    goto try_except_handler_15;
                }
                PyTuple_SET_ITEM( tmp_right_name_7, 1, tmp_tuple_element_20 );
                tmp_raise_value_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_7, tmp_right_name_7 );
                Py_DECREF( tmp_right_name_7 );
                if ( tmp_raise_value_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 102;

                    goto try_except_handler_15;
                }
                exception_type = tmp_raise_type_5;
                Py_INCREF( tmp_raise_type_5 );
                exception_value = tmp_raise_value_5;
                exception_lineno = 102;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_15;
            }
            branch_no_20:;
        }
        goto branch_end_19;
        branch_no_19:;
        {
            PyObject *tmp_assign_source_53;
            tmp_assign_source_53 = PyDict_New();
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_53;
        }
        branch_end_19:;
    }
    {
        PyObject *tmp_assign_source_54;
        {
            PyObject *tmp_set_locals_5;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_set_locals_5 = tmp_class_creation_5__prepared;
            locals_nbconvert$filters$markdown_mistune_102 = tmp_set_locals_5;
            Py_INCREF( tmp_set_locals_5 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_76b2174b385d7250ca7574c6411a6fc7;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_102, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_17;
        }
        tmp_dictset_value = const_str_plain_MarkdownWithMath;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_102, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto try_except_handler_17;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_8e0c5d8fc4d243989626f7e72a0f84ee_6, codeobj_8e0c5d8fc4d243989626f7e72a0f84ee, module_nbconvert$filters$markdown_mistune, sizeof(void *) );
        frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 = cache_frame_8e0c5d8fc4d243989626f7e72a0f84ee_6;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_7___init__(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_102, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_8_output_multiline_math(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_102, const_str_plain_output_multiline_math, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_5;

        frame_exception_exit_6:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_8e0c5d8fc4d243989626f7e72a0f84ee_6, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_8e0c5d8fc4d243989626f7e72a0f84ee_6->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_8e0c5d8fc4d243989626f7e72a0f84ee_6, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_8e0c5d8fc4d243989626f7e72a0f84ee_6,
            type_description_2,
            outline_4_var___class__
        );


        // Release cached frame.
        if ( frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 == cache_frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 )
        {
            Py_DECREF( frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 );
        }
        cache_frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 = NULL;

        assertFrameObject( frame_8e0c5d8fc4d243989626f7e72a0f84ee_6 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_5;

        frame_no_exception_5:;
        goto skip_nested_handling_5;
        nested_frame_exit_5:;

        goto try_except_handler_17;
        skip_nested_handling_5:;
        {
            nuitka_bool tmp_condition_result_31;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_compexpr_left_6 = tmp_class_creation_5__bases;
            CHECK_OBJECT( tmp_class_creation_5__bases_orig );
            tmp_compexpr_right_6 = tmp_class_creation_5__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;

                goto try_except_handler_17;
            }
            tmp_condition_result_31 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_31 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_21;
            }
            else
            {
                goto branch_no_21;
            }
            branch_yes_21:;
            CHECK_OBJECT( tmp_class_creation_5__bases_orig );
            tmp_dictset_value = tmp_class_creation_5__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_102, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;

                goto try_except_handler_17;
            }
            branch_no_21:;
        }
        {
            PyObject *tmp_assign_source_55;
            PyObject *tmp_called_name_15;
            PyObject *tmp_args_name_11;
            PyObject *tmp_tuple_element_21;
            PyObject *tmp_kw_name_11;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_called_name_15 = tmp_class_creation_5__metaclass;
            tmp_tuple_element_21 = const_str_plain_MarkdownWithMath;
            tmp_args_name_11 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_args_name_11, 0, tmp_tuple_element_21 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_21 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_args_name_11, 1, tmp_tuple_element_21 );
            tmp_tuple_element_21 = locals_nbconvert$filters$markdown_mistune_102;
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_args_name_11, 2, tmp_tuple_element_21 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_11 = tmp_class_creation_5__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 102;
            tmp_assign_source_55 = CALL_FUNCTION( tmp_called_name_15, tmp_args_name_11, tmp_kw_name_11 );
            Py_DECREF( tmp_args_name_11 );
            if ( tmp_assign_source_55 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;

                goto try_except_handler_17;
            }
            assert( outline_4_var___class__ == NULL );
            outline_4_var___class__ = tmp_assign_source_55;
        }
        CHECK_OBJECT( outline_4_var___class__ );
        tmp_assign_source_54 = outline_4_var___class__;
        Py_INCREF( tmp_assign_source_54 );
        goto try_return_handler_17;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_17:;
        Py_DECREF( locals_nbconvert$filters$markdown_mistune_102 );
        locals_nbconvert$filters$markdown_mistune_102 = NULL;
        goto try_return_handler_16;
        // Exception handler code:
        try_except_handler_17:;
        exception_keeper_type_15 = exception_type;
        exception_keeper_value_15 = exception_value;
        exception_keeper_tb_15 = exception_tb;
        exception_keeper_lineno_15 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbconvert$filters$markdown_mistune_102 );
        locals_nbconvert$filters$markdown_mistune_102 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_15;
        exception_value = exception_keeper_value_15;
        exception_tb = exception_keeper_tb_15;
        exception_lineno = exception_keeper_lineno_15;

        goto try_except_handler_16;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_16:;
        CHECK_OBJECT( (PyObject *)outline_4_var___class__ );
        Py_DECREF( outline_4_var___class__ );
        outline_4_var___class__ = NULL;

        goto outline_result_5;
        // Exception handler code:
        try_except_handler_16:;
        exception_keeper_type_16 = exception_type;
        exception_keeper_value_16 = exception_value;
        exception_keeper_tb_16 = exception_tb;
        exception_keeper_lineno_16 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_16;
        exception_value = exception_keeper_value_16;
        exception_tb = exception_keeper_tb_16;
        exception_lineno = exception_keeper_lineno_16;

        goto outline_exception_5;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_5:;
        exception_lineno = 102;
        goto try_except_handler_15;
        outline_result_5:;
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_MarkdownWithMath, tmp_assign_source_54 );
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_15:;
    exception_keeper_type_17 = exception_type;
    exception_keeper_value_17 = exception_value;
    exception_keeper_tb_17 = exception_tb;
    exception_keeper_lineno_17 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_5__bases_orig );
    tmp_class_creation_5__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    Py_XDECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_17;
    exception_value = exception_keeper_value_17;
    exception_tb = exception_keeper_tb_17;
    exception_lineno = exception_keeper_lineno_17;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases_orig );
    Py_DECREF( tmp_class_creation_5__bases_orig );
    tmp_class_creation_5__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases );
    Py_DECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__class_decl_dict );
    Py_DECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__metaclass );
    Py_DECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__prepared );
    Py_DECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_56;
        PyObject *tmp_tuple_element_22;
        PyObject *tmp_source_name_38;
        PyObject *tmp_mvar_value_21;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_mistune );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mistune );
        }

        if ( tmp_mvar_value_21 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mistune" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;

            goto try_except_handler_18;
        }

        tmp_source_name_38 = tmp_mvar_value_21;
        tmp_tuple_element_22 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain_Renderer );
        if ( tmp_tuple_element_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        tmp_assign_source_56 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_56, 0, tmp_tuple_element_22 );
        assert( tmp_class_creation_6__bases_orig == NULL );
        tmp_class_creation_6__bases_orig = tmp_assign_source_56;
    }
    {
        PyObject *tmp_assign_source_57;
        PyObject *tmp_dircall_arg1_6;
        CHECK_OBJECT( tmp_class_creation_6__bases_orig );
        tmp_dircall_arg1_6 = tmp_class_creation_6__bases_orig;
        Py_INCREF( tmp_dircall_arg1_6 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_6};
            tmp_assign_source_57 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        assert( tmp_class_creation_6__bases == NULL );
        tmp_class_creation_6__bases = tmp_assign_source_57;
    }
    {
        PyObject *tmp_assign_source_58;
        tmp_assign_source_58 = PyDict_New();
        assert( tmp_class_creation_6__class_decl_dict == NULL );
        tmp_class_creation_6__class_decl_dict = tmp_assign_source_58;
    }
    {
        PyObject *tmp_assign_source_59;
        PyObject *tmp_metaclass_name_6;
        nuitka_bool tmp_condition_result_32;
        PyObject *tmp_key_name_16;
        PyObject *tmp_dict_name_16;
        PyObject *tmp_dict_name_17;
        PyObject *tmp_key_name_17;
        nuitka_bool tmp_condition_result_33;
        int tmp_truth_name_6;
        PyObject *tmp_type_arg_11;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_bases_name_6;
        tmp_key_name_16 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_16 = tmp_class_creation_6__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_16, tmp_key_name_16 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        tmp_condition_result_32 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_32 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_11;
        }
        else
        {
            goto condexpr_false_11;
        }
        condexpr_true_11:;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_17 = tmp_class_creation_6__class_decl_dict;
        tmp_key_name_17 = const_str_plain_metaclass;
        tmp_metaclass_name_6 = DICT_GET_ITEM( tmp_dict_name_17, tmp_key_name_17 );
        if ( tmp_metaclass_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        goto condexpr_end_11;
        condexpr_false_11:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_truth_name_6 = CHECK_IF_TRUE( tmp_class_creation_6__bases );
        if ( tmp_truth_name_6 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        tmp_condition_result_33 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_33 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_12;
        }
        else
        {
            goto condexpr_false_12;
        }
        condexpr_true_12:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_subscribed_name_6 = tmp_class_creation_6__bases;
        tmp_subscript_name_6 = const_int_0;
        tmp_type_arg_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
        if ( tmp_type_arg_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        tmp_metaclass_name_6 = BUILTIN_TYPE1( tmp_type_arg_11 );
        Py_DECREF( tmp_type_arg_11 );
        if ( tmp_metaclass_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        goto condexpr_end_12;
        condexpr_false_12:;
        tmp_metaclass_name_6 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_6 );
        condexpr_end_12:;
        condexpr_end_11:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_bases_name_6 = tmp_class_creation_6__bases;
        tmp_assign_source_59 = SELECT_METACLASS( tmp_metaclass_name_6, tmp_bases_name_6 );
        Py_DECREF( tmp_metaclass_name_6 );
        if ( tmp_assign_source_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        assert( tmp_class_creation_6__metaclass == NULL );
        tmp_class_creation_6__metaclass = tmp_assign_source_59;
    }
    {
        nuitka_bool tmp_condition_result_34;
        PyObject *tmp_key_name_18;
        PyObject *tmp_dict_name_18;
        tmp_key_name_18 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_18 = tmp_class_creation_6__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_18, tmp_key_name_18 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        tmp_condition_result_34 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_34 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_22;
        }
        else
        {
            goto branch_no_22;
        }
        branch_yes_22:;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_6__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_18;
        }
        branch_no_22:;
    }
    {
        nuitka_bool tmp_condition_result_35;
        PyObject *tmp_source_name_39;
        CHECK_OBJECT( tmp_class_creation_6__metaclass );
        tmp_source_name_39 = tmp_class_creation_6__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_39, const_str_plain___prepare__ );
        tmp_condition_result_35 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_35 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_23;
        }
        else
        {
            goto branch_no_23;
        }
        branch_yes_23:;
        {
            PyObject *tmp_assign_source_60;
            PyObject *tmp_called_name_16;
            PyObject *tmp_source_name_40;
            PyObject *tmp_args_name_12;
            PyObject *tmp_tuple_element_23;
            PyObject *tmp_kw_name_12;
            CHECK_OBJECT( tmp_class_creation_6__metaclass );
            tmp_source_name_40 = tmp_class_creation_6__metaclass;
            tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain___prepare__ );
            if ( tmp_called_name_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;

                goto try_except_handler_18;
            }
            tmp_tuple_element_23 = const_str_plain_IPythonRenderer;
            tmp_args_name_12 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_12, 0, tmp_tuple_element_23 );
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_tuple_element_23 = tmp_class_creation_6__bases;
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_12, 1, tmp_tuple_element_23 );
            CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
            tmp_kw_name_12 = tmp_class_creation_6__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 115;
            tmp_assign_source_60 = CALL_FUNCTION( tmp_called_name_16, tmp_args_name_12, tmp_kw_name_12 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_args_name_12 );
            if ( tmp_assign_source_60 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;

                goto try_except_handler_18;
            }
            assert( tmp_class_creation_6__prepared == NULL );
            tmp_class_creation_6__prepared = tmp_assign_source_60;
        }
        {
            nuitka_bool tmp_condition_result_36;
            PyObject *tmp_operand_name_6;
            PyObject *tmp_source_name_41;
            CHECK_OBJECT( tmp_class_creation_6__prepared );
            tmp_source_name_41 = tmp_class_creation_6__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_41, const_str_plain___getitem__ );
            tmp_operand_name_6 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;

                goto try_except_handler_18;
            }
            tmp_condition_result_36 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_36 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_24;
            }
            else
            {
                goto branch_no_24;
            }
            branch_yes_24:;
            {
                PyObject *tmp_raise_type_6;
                PyObject *tmp_raise_value_6;
                PyObject *tmp_left_name_8;
                PyObject *tmp_right_name_8;
                PyObject *tmp_tuple_element_24;
                PyObject *tmp_getattr_target_6;
                PyObject *tmp_getattr_attr_6;
                PyObject *tmp_getattr_default_6;
                PyObject *tmp_source_name_42;
                PyObject *tmp_type_arg_12;
                tmp_raise_type_6 = PyExc_TypeError;
                tmp_left_name_8 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_6__metaclass );
                tmp_getattr_target_6 = tmp_class_creation_6__metaclass;
                tmp_getattr_attr_6 = const_str_plain___name__;
                tmp_getattr_default_6 = const_str_angle_metaclass;
                tmp_tuple_element_24 = BUILTIN_GETATTR( tmp_getattr_target_6, tmp_getattr_attr_6, tmp_getattr_default_6 );
                if ( tmp_tuple_element_24 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 115;

                    goto try_except_handler_18;
                }
                tmp_right_name_8 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_8, 0, tmp_tuple_element_24 );
                CHECK_OBJECT( tmp_class_creation_6__prepared );
                tmp_type_arg_12 = tmp_class_creation_6__prepared;
                tmp_source_name_42 = BUILTIN_TYPE1( tmp_type_arg_12 );
                assert( !(tmp_source_name_42 == NULL) );
                tmp_tuple_element_24 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_42 );
                if ( tmp_tuple_element_24 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_8 );

                    exception_lineno = 115;

                    goto try_except_handler_18;
                }
                PyTuple_SET_ITEM( tmp_right_name_8, 1, tmp_tuple_element_24 );
                tmp_raise_value_6 = BINARY_OPERATION_REMAINDER( tmp_left_name_8, tmp_right_name_8 );
                Py_DECREF( tmp_right_name_8 );
                if ( tmp_raise_value_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 115;

                    goto try_except_handler_18;
                }
                exception_type = tmp_raise_type_6;
                Py_INCREF( tmp_raise_type_6 );
                exception_value = tmp_raise_value_6;
                exception_lineno = 115;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_18;
            }
            branch_no_24:;
        }
        goto branch_end_23;
        branch_no_23:;
        {
            PyObject *tmp_assign_source_61;
            tmp_assign_source_61 = PyDict_New();
            assert( tmp_class_creation_6__prepared == NULL );
            tmp_class_creation_6__prepared = tmp_assign_source_61;
        }
        branch_end_23:;
    }
    {
        PyObject *tmp_assign_source_62;
        {
            PyObject *tmp_set_locals_6;
            CHECK_OBJECT( tmp_class_creation_6__prepared );
            tmp_set_locals_6 = tmp_class_creation_6__prepared;
            locals_nbconvert$filters$markdown_mistune_115 = tmp_set_locals_6;
            Py_INCREF( tmp_set_locals_6 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_76b2174b385d7250ca7574c6411a6fc7;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_20;
        }
        tmp_dictset_value = const_str_plain_IPythonRenderer;
        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto try_except_handler_20;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_6c9df3004a4f945b2a5cc5e4db74db3c_7, codeobj_6c9df3004a4f945b2a5cc5e4db74db3c, module_nbconvert$filters$markdown_mistune, sizeof(void *) );
        frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 = cache_frame_6c9df3004a4f945b2a5cc5e4db74db3c_7;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_9_block_code(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain_block_code, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }
        {
            PyObject *tmp_defaults_3;
            tmp_defaults_3 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_3 );
            tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_10_header( tmp_defaults_3 );



            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain_header, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_11_escape_html(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain_escape_html, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_12_block_math(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain_block_math, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_13_latex_environment(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain_latex_environment, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_14_inline_math(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain_inline_math, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_15_image(  );



        tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain_image, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_6;

        frame_exception_exit_7:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_6c9df3004a4f945b2a5cc5e4db74db3c_7, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_6c9df3004a4f945b2a5cc5e4db74db3c_7->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_6c9df3004a4f945b2a5cc5e4db74db3c_7, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_6c9df3004a4f945b2a5cc5e4db74db3c_7,
            type_description_2,
            outline_5_var___class__
        );


        // Release cached frame.
        if ( frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 == cache_frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 )
        {
            Py_DECREF( frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 );
        }
        cache_frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 = NULL;

        assertFrameObject( frame_6c9df3004a4f945b2a5cc5e4db74db3c_7 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_6;

        frame_no_exception_6:;
        goto skip_nested_handling_6;
        nested_frame_exit_6:;

        goto try_except_handler_20;
        skip_nested_handling_6:;
        {
            nuitka_bool tmp_condition_result_37;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_compexpr_left_7 = tmp_class_creation_6__bases;
            CHECK_OBJECT( tmp_class_creation_6__bases_orig );
            tmp_compexpr_right_7 = tmp_class_creation_6__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;

                goto try_except_handler_20;
            }
            tmp_condition_result_37 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_37 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_25;
            }
            else
            {
                goto branch_no_25;
            }
            branch_yes_25:;
            CHECK_OBJECT( tmp_class_creation_6__bases_orig );
            tmp_dictset_value = tmp_class_creation_6__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbconvert$filters$markdown_mistune_115, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;

                goto try_except_handler_20;
            }
            branch_no_25:;
        }
        {
            PyObject *tmp_assign_source_63;
            PyObject *tmp_called_name_17;
            PyObject *tmp_args_name_13;
            PyObject *tmp_tuple_element_25;
            PyObject *tmp_kw_name_13;
            CHECK_OBJECT( tmp_class_creation_6__metaclass );
            tmp_called_name_17 = tmp_class_creation_6__metaclass;
            tmp_tuple_element_25 = const_str_plain_IPythonRenderer;
            tmp_args_name_13 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_25 );
            PyTuple_SET_ITEM( tmp_args_name_13, 0, tmp_tuple_element_25 );
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_tuple_element_25 = tmp_class_creation_6__bases;
            Py_INCREF( tmp_tuple_element_25 );
            PyTuple_SET_ITEM( tmp_args_name_13, 1, tmp_tuple_element_25 );
            tmp_tuple_element_25 = locals_nbconvert$filters$markdown_mistune_115;
            Py_INCREF( tmp_tuple_element_25 );
            PyTuple_SET_ITEM( tmp_args_name_13, 2, tmp_tuple_element_25 );
            CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
            tmp_kw_name_13 = tmp_class_creation_6__class_decl_dict;
            frame_f905ee16b1b38b38016191f300fb5513->m_frame.f_lineno = 115;
            tmp_assign_source_63 = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_13, tmp_kw_name_13 );
            Py_DECREF( tmp_args_name_13 );
            if ( tmp_assign_source_63 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;

                goto try_except_handler_20;
            }
            assert( outline_5_var___class__ == NULL );
            outline_5_var___class__ = tmp_assign_source_63;
        }
        CHECK_OBJECT( outline_5_var___class__ );
        tmp_assign_source_62 = outline_5_var___class__;
        Py_INCREF( tmp_assign_source_62 );
        goto try_return_handler_20;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_20:;
        Py_DECREF( locals_nbconvert$filters$markdown_mistune_115 );
        locals_nbconvert$filters$markdown_mistune_115 = NULL;
        goto try_return_handler_19;
        // Exception handler code:
        try_except_handler_20:;
        exception_keeper_type_18 = exception_type;
        exception_keeper_value_18 = exception_value;
        exception_keeper_tb_18 = exception_tb;
        exception_keeper_lineno_18 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbconvert$filters$markdown_mistune_115 );
        locals_nbconvert$filters$markdown_mistune_115 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_18;
        exception_value = exception_keeper_value_18;
        exception_tb = exception_keeper_tb_18;
        exception_lineno = exception_keeper_lineno_18;

        goto try_except_handler_19;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_19:;
        CHECK_OBJECT( (PyObject *)outline_5_var___class__ );
        Py_DECREF( outline_5_var___class__ );
        outline_5_var___class__ = NULL;

        goto outline_result_6;
        // Exception handler code:
        try_except_handler_19:;
        exception_keeper_type_19 = exception_type;
        exception_keeper_value_19 = exception_value;
        exception_keeper_tb_19 = exception_tb;
        exception_keeper_lineno_19 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_19;
        exception_value = exception_keeper_value_19;
        exception_tb = exception_keeper_tb_19;
        exception_lineno = exception_keeper_lineno_19;

        goto outline_exception_6;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown_mistune );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_6:;
        exception_lineno = 115;
        goto try_except_handler_18;
        outline_result_6:;
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_IPythonRenderer, tmp_assign_source_62 );
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_18:;
    exception_keeper_type_20 = exception_type;
    exception_keeper_value_20 = exception_value;
    exception_keeper_tb_20 = exception_tb;
    exception_keeper_lineno_20 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_6__bases_orig );
    tmp_class_creation_6__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_6__bases );
    tmp_class_creation_6__bases = NULL;

    Py_XDECREF( tmp_class_creation_6__class_decl_dict );
    tmp_class_creation_6__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_6__metaclass );
    tmp_class_creation_6__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_6__prepared );
    tmp_class_creation_6__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_20;
    exception_value = exception_keeper_value_20;
    exception_tb = exception_keeper_tb_20;
    exception_lineno = exception_keeper_lineno_20;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f905ee16b1b38b38016191f300fb5513 );
#endif
    popFrameStack();

    assertFrameObject( frame_f905ee16b1b38b38016191f300fb5513 );

    goto frame_no_exception_7;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f905ee16b1b38b38016191f300fb5513 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f905ee16b1b38b38016191f300fb5513, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f905ee16b1b38b38016191f300fb5513->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f905ee16b1b38b38016191f300fb5513, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_7:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__bases_orig );
    Py_DECREF( tmp_class_creation_6__bases_orig );
    tmp_class_creation_6__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__bases );
    Py_DECREF( tmp_class_creation_6__bases );
    tmp_class_creation_6__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__class_decl_dict );
    Py_DECREF( tmp_class_creation_6__class_decl_dict );
    tmp_class_creation_6__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__metaclass );
    Py_DECREF( tmp_class_creation_6__metaclass );
    tmp_class_creation_6__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__prepared );
    Py_DECREF( tmp_class_creation_6__prepared );
    tmp_class_creation_6__prepared = NULL;

    {
        PyObject *tmp_assign_source_64;
        tmp_assign_source_64 = MAKE_FUNCTION_nbconvert$filters$markdown_mistune$$$function_16_markdown2html_mistune(  );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown_mistune, (Nuitka_StringObject *)const_str_plain_markdown2html_mistune, tmp_assign_source_64 );
    }

    return MOD_RETURN_VALUE( module_nbconvert$filters$markdown_mistune );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
