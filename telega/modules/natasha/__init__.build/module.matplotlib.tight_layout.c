/* Generated code for Python module 'matplotlib.tight_layout'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_matplotlib$tight_layout" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_matplotlib$tight_layout;
PyDictObject *moduledict_matplotlib$tight_layout;

/* The declarations of module constants used, if any. */
extern PyObject *const_tuple_str_plain_i_tuple;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain__get_left;
extern PyObject *const_tuple_none_float_1_08_none_none_none_tuple;
extern PyObject *const_str_plain_gs;
static PyObject *const_str_plain__get_right;
extern PyObject *const_str_plain_ymax;
extern PyObject *const_str_plain_zip;
extern PyObject *const_str_plain_get_gridspec;
extern PyObject *const_str_plain_i;
static PyObject *const_str_digest_e11a877378ac93aa8261e847c3379ba0;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_str_digest_779b532b2e2c1b413b3dfeb01f657d05_tuple;
extern PyObject *const_str_plain__cachedRenderer;
extern PyObject *const_str_plain_max;
extern PyObject *const_tuple_str_plain_FigureCanvasAgg_tuple;
static PyObject *const_tuple_str_digest_8b3984fd8500371f6d99a74526a97354_tuple;
extern PyObject *const_tuple_str_plain_ax_tuple;
extern PyObject *const_tuple_str_plain_s_tuple;
extern PyObject *const_str_digest_876c9648523deea8f3845ee2dd49a0d1;
extern PyObject *const_str_plain_size;
static PyObject *const_str_digest_f83ea12fefdf969d517c165f4e427181;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_8f2df5b9323e5a23585d6cb381c79548;
extern PyObject *const_str_plain_get_topmost_subplotspec;
extern PyObject *const_str_plain_transFigure;
extern PyObject *const_str_plain_get_position;
extern PyObject *const_str_plain_get_visible;
extern PyObject *const_str_plain_canvas;
extern PyObject *const_str_plain_locally_modified_subplot_params;
static PyObject *const_str_plain_num1num2_list;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_ymin;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_grid_spec;
static PyObject *const_tuple_7d1afd1bb6d032831efbbcf05bda272f_tuple;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_ax_tuple;
static PyObject *const_tuple_str_digest_f46c46c7a707a2eff8629742c0941615_tuple;
extern PyObject *const_str_plain_matplotlib;
extern PyObject *const_str_plain_s;
extern PyObject *const_tuple_str_plain_FontProperties_tuple;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_digest_d28745151750b44e549d12f294293c52;
static PyObject *const_str_digest_ffd2f47d1bf0b28ba0026578ee1ec340;
static PyObject *const_str_plain_axes_or_locator;
static PyObject *const_str_digest_a55724e1f23ea7bee1b2bc604e3da396;
static PyObject *const_str_digest_81382ed4783c15b20fbc8b2595ebdc58;
extern PyObject *const_str_plain_w_pad;
extern PyObject *const_str_plain_get_subplotspec;
static PyObject *const_str_plain__get_bottom;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
static PyObject *const_str_digest_a2850d87673bb7476bb6ea3595d327b2;
extern PyObject *const_str_digest_8090d9314ba82f55c86e2a009f824e92;
static PyObject *const_tuple_str_digest_a55724e1f23ea7bee1b2bc604e3da396_tuple;
extern PyObject *const_str_plain_FigureCanvasAgg;
static PyObject *const_tuple_str_plain_tight_bbox_str_plain_axes_bbox_tuple;
extern PyObject *const_str_plain_all;
static PyObject *const_str_plain_subplot_list;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_rcParams;
extern PyObject *const_str_plain_get_axes_locator;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_top;
static PyObject *const_tuple_str_plain_ax_str_plain_renderer_tuple;
extern PyObject *const_str_plain_cbook;
extern PyObject *const_str_plain_get_tightbbox;
extern PyObject *const_str_plain_inverted;
extern PyObject *const_str_plain_FontProperties;
static PyObject *const_str_digest_e5414949eeddd63c81115af32fad7475;
extern PyObject *const_str_plain_wspace;
extern PyObject *const_str_plain_False;
static PyObject *const_str_plain__get_top;
extern PyObject *const_str_plain_renderer;
static PyObject *const_str_plain_hspaces;
static PyObject *const_str_plain_nrows_ncols;
extern PyObject *const_int_0;
extern PyObject *const_int_pos_72;
extern PyObject *const_str_plain_rect;
static PyObject *const_tuple_str_digest_9b40ba1484f1ad56c07048067bab993a_tuple;
extern PyObject *const_str_plain_get_renderer;
extern PyObject *const_str_plain_get_geometry;
extern PyObject *const_str_plain_h_pad;
static PyObject *const_str_plain_auto_adjust_subplotpars;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_subplotspec;
static PyObject *const_str_plain_ax_bbox_list;
static PyObject *const_tuple_str_digest_b1a5174aba8b0a3a776575229ad039ae_tuple;
extern PyObject *const_str_plain_right;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_Bbox;
extern PyObject *const_str_plain_get_size_inches;
extern PyObject *const_str_plain_get_tight_layout_figure;
static PyObject *const_str_digest_8b3984fd8500371f6d99a74526a97354;
extern PyObject *const_str_plain_setdefault;
extern PyObject *const_tuple_0e6e30fbbc926b762c3a0f518d6d18fe_tuple;
extern PyObject *const_str_plain_pad;
static PyObject *const_tuple_b2a1b172891d14506cd47aecf06df549_tuple;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_b1a5174aba8b0a3a776575229ad039ae;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_subplotspec_list;
extern PyObject *const_str_plain_xmin;
extern PyObject *const_float_1_08;
static PyObject *const_tuple_47d559e81246febd4c535d4eb1b3e4eb_tuple;
extern PyObject *const_str_plain_xmax;
extern PyObject *const_str_plain__warn_external;
extern PyObject *const_str_plain_bottom;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_0330f3a212733337a30b9276ae2f650b;
extern PyObject *const_list_int_0_list;
static PyObject *const_str_digest_91f231986ceaa1aa2b5332e5f3fa7a03;
static PyObject *const_str_plain_axes_bbox;
static PyObject *const_str_digest_779b532b2e2c1b413b3dfeb01f657d05;
extern PyObject *const_str_plain_fig;
extern PyObject *const_str_plain_left;
extern PyObject *const_str_digest_f27e18bd3454dcd2358f62563ae885e3;
extern PyObject *const_str_plain_TransformedBbox;
static PyObject *const_tuple_str_plain_TransformedBbox_str_plain_Bbox_tuple;
extern PyObject *const_str_plain_cols;
extern PyObject *const_str_plain_union;
extern PyObject *const_str_plain_get_size_in_points;
extern PyObject *const_str_plain_tight_bbox;
extern PyObject *const_tuple_str_plain_cbook_str_plain_rcParams_tuple;
extern PyObject *const_str_plain_hspace;
extern PyObject *const_dict_2c333e641b5bd70d3db5933b70b90929;
extern PyObject *const_str_plain_original;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_tuple_float_1_08_none_none_none_tuple;
static PyObject *const_str_digest_9b40ba1484f1ad56c07048067bab993a;
static PyObject *const_tuple_str_digest_f83ea12fefdf969d517c165f4e427181_tuple;
extern PyObject *const_str_plain_ax;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_s_tuple;
static PyObject *const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple;
extern PyObject *const_str_plain_axes_list;
extern PyObject *const_str_plain_get_subplotspec_list;
static PyObject *const_str_digest_f46c46c7a707a2eff8629742c0941615;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain__get_left = UNSTREAM_STRING_ASCII( &constant_bin[ 2574750 ], 9, 1 );
    const_str_plain__get_right = UNSTREAM_STRING_ASCII( &constant_bin[ 2574759 ], 10, 1 );
    const_str_digest_e11a877378ac93aa8261e847c3379ba0 = UNSTREAM_STRING_ASCII( &constant_bin[ 2574769 ], 521, 0 );
    const_tuple_str_digest_779b532b2e2c1b413b3dfeb01f657d05_tuple = PyTuple_New( 1 );
    const_str_digest_779b532b2e2c1b413b3dfeb01f657d05 = UNSTREAM_STRING_ASCII( &constant_bin[ 2575290 ], 100, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_779b532b2e2c1b413b3dfeb01f657d05_tuple, 0, const_str_digest_779b532b2e2c1b413b3dfeb01f657d05 ); Py_INCREF( const_str_digest_779b532b2e2c1b413b3dfeb01f657d05 );
    const_tuple_str_digest_8b3984fd8500371f6d99a74526a97354_tuple = PyTuple_New( 1 );
    const_str_digest_8b3984fd8500371f6d99a74526a97354 = UNSTREAM_STRING_ASCII( &constant_bin[ 2575390 ], 118, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_8b3984fd8500371f6d99a74526a97354_tuple, 0, const_str_digest_8b3984fd8500371f6d99a74526a97354 ); Py_INCREF( const_str_digest_8b3984fd8500371f6d99a74526a97354 );
    const_str_digest_f83ea12fefdf969d517c165f4e427181 = UNSTREAM_STRING_ASCII( &constant_bin[ 2575508 ], 118, 0 );
    const_str_digest_8f2df5b9323e5a23585d6cb381c79548 = UNSTREAM_STRING_ASCII( &constant_bin[ 2575626 ], 26, 0 );
    const_str_plain_num1num2_list = UNSTREAM_STRING_ASCII( &constant_bin[ 2575652 ], 13, 1 );
    const_tuple_7d1afd1bb6d032831efbbcf05bda272f_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 2575665 ], 369 );
    const_tuple_str_digest_f46c46c7a707a2eff8629742c0941615_tuple = PyTuple_New( 1 );
    const_str_digest_f46c46c7a707a2eff8629742c0941615 = UNSTREAM_STRING_ASCII( &constant_bin[ 2576034 ], 110, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_f46c46c7a707a2eff8629742c0941615_tuple, 0, const_str_digest_f46c46c7a707a2eff8629742c0941615 ); Py_INCREF( const_str_digest_f46c46c7a707a2eff8629742c0941615 );
    const_str_digest_ffd2f47d1bf0b28ba0026578ee1ec340 = UNSTREAM_STRING_ASCII( &constant_bin[ 2576144 ], 249, 0 );
    const_str_plain_axes_or_locator = UNSTREAM_STRING_ASCII( &constant_bin[ 2576393 ], 15, 1 );
    const_str_digest_a55724e1f23ea7bee1b2bc604e3da396 = UNSTREAM_STRING_ASCII( &constant_bin[ 2576408 ], 103, 0 );
    const_str_digest_81382ed4783c15b20fbc8b2595ebdc58 = UNSTREAM_STRING_ASCII( &constant_bin[ 2576511 ], 941, 0 );
    const_str_plain__get_bottom = UNSTREAM_STRING_ASCII( &constant_bin[ 2577452 ], 11, 1 );
    const_str_digest_a2850d87673bb7476bb6ea3595d327b2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2577463 ], 42, 0 );
    const_tuple_str_digest_a55724e1f23ea7bee1b2bc604e3da396_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_a55724e1f23ea7bee1b2bc604e3da396_tuple, 0, const_str_digest_a55724e1f23ea7bee1b2bc604e3da396 ); Py_INCREF( const_str_digest_a55724e1f23ea7bee1b2bc604e3da396 );
    const_tuple_str_plain_tight_bbox_str_plain_axes_bbox_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_tight_bbox_str_plain_axes_bbox_tuple, 0, const_str_plain_tight_bbox ); Py_INCREF( const_str_plain_tight_bbox );
    const_str_plain_axes_bbox = UNSTREAM_STRING_ASCII( &constant_bin[ 2577505 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_tight_bbox_str_plain_axes_bbox_tuple, 1, const_str_plain_axes_bbox ); Py_INCREF( const_str_plain_axes_bbox );
    const_str_plain_subplot_list = UNSTREAM_STRING_ASCII( &constant_bin[ 2575738 ], 12, 1 );
    const_tuple_str_plain_ax_str_plain_renderer_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ax_str_plain_renderer_tuple, 0, const_str_plain_ax ); Py_INCREF( const_str_plain_ax );
    PyTuple_SET_ITEM( const_tuple_str_plain_ax_str_plain_renderer_tuple, 1, const_str_plain_renderer ); Py_INCREF( const_str_plain_renderer );
    const_str_digest_e5414949eeddd63c81115af32fad7475 = UNSTREAM_STRING_ASCII( &constant_bin[ 2577514 ], 23, 0 );
    const_str_plain__get_top = UNSTREAM_STRING_ASCII( &constant_bin[ 2577537 ], 8, 1 );
    const_str_plain_hspaces = UNSTREAM_STRING_ASCII( &constant_bin[ 2069865 ], 7, 1 );
    const_str_plain_nrows_ncols = UNSTREAM_STRING_ASCII( &constant_bin[ 2577545 ], 11, 1 );
    const_tuple_str_digest_9b40ba1484f1ad56c07048067bab993a_tuple = PyTuple_New( 1 );
    const_str_digest_9b40ba1484f1ad56c07048067bab993a = UNSTREAM_STRING_ASCII( &constant_bin[ 2577556 ], 111, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_9b40ba1484f1ad56c07048067bab993a_tuple, 0, const_str_digest_9b40ba1484f1ad56c07048067bab993a ); Py_INCREF( const_str_digest_9b40ba1484f1ad56c07048067bab993a );
    const_str_plain_auto_adjust_subplotpars = UNSTREAM_STRING_ASCII( &constant_bin[ 2577463 ], 23, 1 );
    const_str_plain_ax_bbox_list = UNSTREAM_STRING_ASCII( &constant_bin[ 2575776 ], 12, 1 );
    const_tuple_str_digest_b1a5174aba8b0a3a776575229ad039ae_tuple = PyTuple_New( 1 );
    const_str_digest_b1a5174aba8b0a3a776575229ad039ae = UNSTREAM_STRING_ASCII( &constant_bin[ 2577667 ], 43, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b1a5174aba8b0a3a776575229ad039ae_tuple, 0, const_str_digest_b1a5174aba8b0a3a776575229ad039ae ); Py_INCREF( const_str_digest_b1a5174aba8b0a3a776575229ad039ae );
    const_tuple_b2a1b172891d14506cd47aecf06df549_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b2a1b172891d14506cd47aecf06df549_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_b2a1b172891d14506cd47aecf06df549_tuple, 1, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_b2a1b172891d14506cd47aecf06df549_tuple, 2, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_b2a1b172891d14506cd47aecf06df549_tuple, 3, const_str_plain_hspaces ); Py_INCREF( const_str_plain_hspaces );
    PyTuple_SET_ITEM( const_tuple_b2a1b172891d14506cd47aecf06df549_tuple, 4, const_str_plain_cols ); Py_INCREF( const_str_plain_cols );
    const_tuple_47d559e81246febd4c535d4eb1b3e4eb_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 2577710 ], 429 );
    const_str_digest_0330f3a212733337a30b9276ae2f650b = UNSTREAM_STRING_ASCII( &constant_bin[ 2578139 ], 32, 0 );
    const_str_digest_91f231986ceaa1aa2b5332e5f3fa7a03 = UNSTREAM_STRING_ASCII( &constant_bin[ 2578171 ], 1129, 0 );
    const_tuple_str_plain_TransformedBbox_str_plain_Bbox_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_TransformedBbox_str_plain_Bbox_tuple, 0, const_str_plain_TransformedBbox ); Py_INCREF( const_str_plain_TransformedBbox );
    PyTuple_SET_ITEM( const_tuple_str_plain_TransformedBbox_str_plain_Bbox_tuple, 1, const_str_plain_Bbox ); Py_INCREF( const_str_plain_Bbox );
    const_tuple_str_digest_f83ea12fefdf969d517c165f4e427181_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_f83ea12fefdf969d517c165f4e427181_tuple, 0, const_str_digest_f83ea12fefdf969d517c165f4e427181 ); Py_INCREF( const_str_digest_f83ea12fefdf969d517c165f4e427181 );
    const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple, 0, const_str_plain_axes_list ); Py_INCREF( const_str_plain_axes_list );
    PyTuple_SET_ITEM( const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple, 1, const_str_plain_grid_spec ); Py_INCREF( const_str_plain_grid_spec );
    PyTuple_SET_ITEM( const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple, 2, const_str_plain_subplotspec_list ); Py_INCREF( const_str_plain_subplotspec_list );
    PyTuple_SET_ITEM( const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple, 3, const_str_plain_ax ); Py_INCREF( const_str_plain_ax );
    PyTuple_SET_ITEM( const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple, 4, const_str_plain_axes_or_locator ); Py_INCREF( const_str_plain_axes_or_locator );
    PyTuple_SET_ITEM( const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple, 5, const_str_plain_subplotspec ); Py_INCREF( const_str_plain_subplotspec );
    PyTuple_SET_ITEM( const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple, 6, const_str_plain_gs ); Py_INCREF( const_str_plain_gs );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_matplotlib$tight_layout( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_3af0788c949f194472a6e6612cb18973;
static PyCodeObject *codeobj_ca2c371fef01f5b6aa6b49fb0a213168;
static PyCodeObject *codeobj_c807c51e7958476d3d90a2b4857dd0b2;
static PyCodeObject *codeobj_fdc5cf8d6235923e8fd74553c5bc761c;
static PyCodeObject *codeobj_a7f3cffd0cdb809b88117fa0ae807d17;
static PyCodeObject *codeobj_824ebf8299dfdf1007010b362f6f7952;
static PyCodeObject *codeobj_27f7d61a4c04233c81b9d4136132d885;
static PyCodeObject *codeobj_b02ee1be6b09833b65bcb24d7211f57b;
static PyCodeObject *codeobj_699e36f676ffc125d4d1ecef74569d68;
static PyCodeObject *codeobj_52e777314c8cb5bf5b2e5ade68e5649a;
static PyCodeObject *codeobj_a945046174c9cda7ebe853bb1339324e;
static PyCodeObject *codeobj_c351c7510022255ee6c7be0c2ccc9d1d;
static PyCodeObject *codeobj_c6c522dfe327c281626eca0ebefbaa78;
static PyCodeObject *codeobj_0e8ec8b23f6915730102842c38ac54c7;
static PyCodeObject *codeobj_e4c142720c59242629db15855ec5f99e;
static PyCodeObject *codeobj_fd638e1a1a1d44e380df4f7b4db53dd5;
static PyCodeObject *codeobj_dfe501d74e083632d082979fedbf32ac;
static PyCodeObject *codeobj_ed465ddd478fe10c713c4573bf253c8f;
static PyCodeObject *codeobj_e06e28650b8962f2e6e2d032dd2fa616;
static PyCodeObject *codeobj_929741756295e159ad639a088117b506;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_8f2df5b9323e5a23585d6cb381c79548 );
    codeobj_3af0788c949f194472a6e6612cb18973 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 108, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_ax_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ca2c371fef01f5b6aa6b49fb0a213168 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 188, const_tuple_b2a1b172891d14506cd47aecf06df549_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c807c51e7958476d3d90a2b4857dd0b2 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 203, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_s_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fdc5cf8d6235923e8fd74553c5bc761c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 101, const_tuple_str_plain_ax_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a7f3cffd0cdb809b88117fa0ae807d17 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 111, const_tuple_str_plain_ax_str_plain_renderer_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_824ebf8299dfdf1007010b362f6f7952 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 93, const_tuple_str_plain_i_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_27f7d61a4c04233c81b9d4136132d885 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 94, const_tuple_str_plain_i_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b02ee1be6b09833b65bcb24d7211f57b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 156, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_699e36f676ffc125d4d1ecef74569d68 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 160, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_52e777314c8cb5bf5b2e5ade68e5649a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 164, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a945046174c9cda7ebe853bb1339324e = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 168, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c351c7510022255ee6c7be0c2ccc9d1d = MAKE_CODEOBJ( module_filename_obj, const_str_digest_0330f3a212733337a30b9276ae2f650b, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_c6c522dfe327c281626eca0ebefbaa78 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_bottom, 25, const_tuple_str_plain_tight_bbox_str_plain_axes_bbox_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0e8ec8b23f6915730102842c38ac54c7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_left, 17, const_tuple_str_plain_tight_bbox_str_plain_axes_bbox_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e4c142720c59242629db15855ec5f99e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_right, 21, const_tuple_str_plain_tight_bbox_str_plain_axes_bbox_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fd638e1a1a1d44e380df4f7b4db53dd5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_top, 29, const_tuple_str_plain_tight_bbox_str_plain_axes_bbox_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_dfe501d74e083632d082979fedbf32ac = MAKE_CODEOBJ( module_filename_obj, const_str_plain_auto_adjust_subplotpars, 33, const_tuple_47d559e81246febd4c535d4eb1b3e4eb_tuple, 10, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ed465ddd478fe10c713c4573bf253c8f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_renderer, 217, const_tuple_0e6e30fbbc926b762c3a0f518d6d18fe_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e06e28650b8962f2e6e2d032dd2fa616 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_subplotspec_list, 235, const_tuple_42706b45cd4f83241c2a65af4487c7eb_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_929741756295e159ad639a088117b506 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_tight_layout_figure, 267, const_tuple_7d1afd1bb6d032831efbbcf05bda272f_tuple, 8, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_1_genexpr_maker( void );


static PyObject *matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_2_genexpr_maker( void );


static PyObject *matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_3_genexpr_maker( void );


static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_1__get_left(  );


static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_2__get_right(  );


static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_3__get_bottom(  );


static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_4__get_top(  );


static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_6_get_renderer(  );


static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_7_get_subplotspec_list( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_8_get_tight_layout_figure( PyObject *defaults );


// The module function definitions.
static PyObject *impl_matplotlib$tight_layout$$$function_1__get_left( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tight_bbox = python_pars[ 0 ];
    PyObject *par_axes_bbox = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_0e8ec8b23f6915730102842c38ac54c7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0e8ec8b23f6915730102842c38ac54c7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0e8ec8b23f6915730102842c38ac54c7, codeobj_0e8ec8b23f6915730102842c38ac54c7, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *) );
    frame_0e8ec8b23f6915730102842c38ac54c7 = cache_frame_0e8ec8b23f6915730102842c38ac54c7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0e8ec8b23f6915730102842c38ac54c7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0e8ec8b23f6915730102842c38ac54c7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_axes_bbox );
        tmp_source_name_1 = par_axes_bbox;
        tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_xmin );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_tight_bbox );
        tmp_source_name_2 = par_tight_bbox;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_xmin );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 18;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0e8ec8b23f6915730102842c38ac54c7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0e8ec8b23f6915730102842c38ac54c7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0e8ec8b23f6915730102842c38ac54c7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0e8ec8b23f6915730102842c38ac54c7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0e8ec8b23f6915730102842c38ac54c7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0e8ec8b23f6915730102842c38ac54c7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0e8ec8b23f6915730102842c38ac54c7,
        type_description_1,
        par_tight_bbox,
        par_axes_bbox
    );


    // Release cached frame.
    if ( frame_0e8ec8b23f6915730102842c38ac54c7 == cache_frame_0e8ec8b23f6915730102842c38ac54c7 )
    {
        Py_DECREF( frame_0e8ec8b23f6915730102842c38ac54c7 );
    }
    cache_frame_0e8ec8b23f6915730102842c38ac54c7 = NULL;

    assertFrameObject( frame_0e8ec8b23f6915730102842c38ac54c7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_1__get_left );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_tight_bbox );
    Py_DECREF( par_tight_bbox );
    par_tight_bbox = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_bbox );
    Py_DECREF( par_axes_bbox );
    par_axes_bbox = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_tight_bbox );
    Py_DECREF( par_tight_bbox );
    par_tight_bbox = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_bbox );
    Py_DECREF( par_axes_bbox );
    par_axes_bbox = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_1__get_left );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$tight_layout$$$function_2__get_right( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tight_bbox = python_pars[ 0 ];
    PyObject *par_axes_bbox = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_e4c142720c59242629db15855ec5f99e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e4c142720c59242629db15855ec5f99e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e4c142720c59242629db15855ec5f99e, codeobj_e4c142720c59242629db15855ec5f99e, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *) );
    frame_e4c142720c59242629db15855ec5f99e = cache_frame_e4c142720c59242629db15855ec5f99e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e4c142720c59242629db15855ec5f99e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e4c142720c59242629db15855ec5f99e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_tight_bbox );
        tmp_source_name_1 = par_tight_bbox;
        tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_xmax );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_axes_bbox );
        tmp_source_name_2 = par_axes_bbox;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_xmax );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 22;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4c142720c59242629db15855ec5f99e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4c142720c59242629db15855ec5f99e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4c142720c59242629db15855ec5f99e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e4c142720c59242629db15855ec5f99e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e4c142720c59242629db15855ec5f99e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e4c142720c59242629db15855ec5f99e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e4c142720c59242629db15855ec5f99e,
        type_description_1,
        par_tight_bbox,
        par_axes_bbox
    );


    // Release cached frame.
    if ( frame_e4c142720c59242629db15855ec5f99e == cache_frame_e4c142720c59242629db15855ec5f99e )
    {
        Py_DECREF( frame_e4c142720c59242629db15855ec5f99e );
    }
    cache_frame_e4c142720c59242629db15855ec5f99e = NULL;

    assertFrameObject( frame_e4c142720c59242629db15855ec5f99e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_2__get_right );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_tight_bbox );
    Py_DECREF( par_tight_bbox );
    par_tight_bbox = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_bbox );
    Py_DECREF( par_axes_bbox );
    par_axes_bbox = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_tight_bbox );
    Py_DECREF( par_tight_bbox );
    par_tight_bbox = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_bbox );
    Py_DECREF( par_axes_bbox );
    par_axes_bbox = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_2__get_right );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$tight_layout$$$function_3__get_bottom( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tight_bbox = python_pars[ 0 ];
    PyObject *par_axes_bbox = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_c6c522dfe327c281626eca0ebefbaa78;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c6c522dfe327c281626eca0ebefbaa78 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c6c522dfe327c281626eca0ebefbaa78, codeobj_c6c522dfe327c281626eca0ebefbaa78, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *) );
    frame_c6c522dfe327c281626eca0ebefbaa78 = cache_frame_c6c522dfe327c281626eca0ebefbaa78;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c6c522dfe327c281626eca0ebefbaa78 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c6c522dfe327c281626eca0ebefbaa78 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_axes_bbox );
        tmp_source_name_1 = par_axes_bbox;
        tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ymin );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_tight_bbox );
        tmp_source_name_2 = par_tight_bbox;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_ymin );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 26;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c6c522dfe327c281626eca0ebefbaa78 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c6c522dfe327c281626eca0ebefbaa78 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c6c522dfe327c281626eca0ebefbaa78 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c6c522dfe327c281626eca0ebefbaa78, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c6c522dfe327c281626eca0ebefbaa78->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c6c522dfe327c281626eca0ebefbaa78, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c6c522dfe327c281626eca0ebefbaa78,
        type_description_1,
        par_tight_bbox,
        par_axes_bbox
    );


    // Release cached frame.
    if ( frame_c6c522dfe327c281626eca0ebefbaa78 == cache_frame_c6c522dfe327c281626eca0ebefbaa78 )
    {
        Py_DECREF( frame_c6c522dfe327c281626eca0ebefbaa78 );
    }
    cache_frame_c6c522dfe327c281626eca0ebefbaa78 = NULL;

    assertFrameObject( frame_c6c522dfe327c281626eca0ebefbaa78 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_3__get_bottom );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_tight_bbox );
    Py_DECREF( par_tight_bbox );
    par_tight_bbox = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_bbox );
    Py_DECREF( par_axes_bbox );
    par_axes_bbox = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_tight_bbox );
    Py_DECREF( par_tight_bbox );
    par_tight_bbox = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_bbox );
    Py_DECREF( par_axes_bbox );
    par_axes_bbox = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_3__get_bottom );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$tight_layout$$$function_4__get_top( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tight_bbox = python_pars[ 0 ];
    PyObject *par_axes_bbox = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_fd638e1a1a1d44e380df4f7b4db53dd5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_fd638e1a1a1d44e380df4f7b4db53dd5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_fd638e1a1a1d44e380df4f7b4db53dd5, codeobj_fd638e1a1a1d44e380df4f7b4db53dd5, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *) );
    frame_fd638e1a1a1d44e380df4f7b4db53dd5 = cache_frame_fd638e1a1a1d44e380df4f7b4db53dd5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_fd638e1a1a1d44e380df4f7b4db53dd5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_fd638e1a1a1d44e380df4f7b4db53dd5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_tight_bbox );
        tmp_source_name_1 = par_tight_bbox;
        tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ymax );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_axes_bbox );
        tmp_source_name_2 = par_axes_bbox;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_ymax );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 30;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fd638e1a1a1d44e380df4f7b4db53dd5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_fd638e1a1a1d44e380df4f7b4db53dd5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fd638e1a1a1d44e380df4f7b4db53dd5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_fd638e1a1a1d44e380df4f7b4db53dd5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_fd638e1a1a1d44e380df4f7b4db53dd5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_fd638e1a1a1d44e380df4f7b4db53dd5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_fd638e1a1a1d44e380df4f7b4db53dd5,
        type_description_1,
        par_tight_bbox,
        par_axes_bbox
    );


    // Release cached frame.
    if ( frame_fd638e1a1a1d44e380df4f7b4db53dd5 == cache_frame_fd638e1a1a1d44e380df4f7b4db53dd5 )
    {
        Py_DECREF( frame_fd638e1a1a1d44e380df4f7b4db53dd5 );
    }
    cache_frame_fd638e1a1a1d44e380df4f7b4db53dd5 = NULL;

    assertFrameObject( frame_fd638e1a1a1d44e380df4f7b4db53dd5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_4__get_top );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_tight_bbox );
    Py_DECREF( par_tight_bbox );
    par_tight_bbox = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_bbox );
    Py_DECREF( par_axes_bbox );
    par_axes_bbox = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_tight_bbox );
    Py_DECREF( par_tight_bbox );
    par_tight_bbox = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_bbox );
    Py_DECREF( par_axes_bbox );
    par_axes_bbox = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_4__get_top );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fig = python_pars[ 0 ];
    PyObject *par_renderer = python_pars[ 1 ];
    PyObject *par_nrows_ncols = python_pars[ 2 ];
    PyObject *par_num1num2_list = python_pars[ 3 ];
    PyObject *par_subplot_list = python_pars[ 4 ];
    PyObject *par_ax_bbox_list = python_pars[ 5 ];
    PyObject *par_pad = python_pars[ 6 ];
    PyObject *par_h_pad = python_pars[ 7 ];
    PyObject *par_w_pad = python_pars[ 8 ];
    PyObject *par_rect = python_pars[ 9 ];
    PyObject *var_rows = NULL;
    struct Nuitka_CellObject *var_cols = PyCell_EMPTY();
    PyObject *var_font_size_inches = NULL;
    PyObject *var_pad_inches = NULL;
    PyObject *var_vpad_inches = NULL;
    PyObject *var_hpad_inches = NULL;
    PyObject *var_margin_left = NULL;
    PyObject *var_margin_bottom = NULL;
    PyObject *var_margin_right = NULL;
    PyObject *var_margin_top = NULL;
    PyObject *var__right = NULL;
    PyObject *var__top = NULL;
    PyObject *var_vspaces = NULL;
    struct Nuitka_CellObject *var_hspaces = PyCell_EMPTY();
    PyObject *var_union = NULL;
    PyObject *var_subplots = NULL;
    PyObject *var_ax_bbox = NULL;
    PyObject *var_num1 = NULL;
    PyObject *var_num2 = NULL;
    PyObject *var_tight_bbox_raw = NULL;
    PyObject *var_tight_bbox = NULL;
    PyObject *var_row1 = NULL;
    PyObject *var_col1 = NULL;
    PyObject *var_row2 = NULL;
    PyObject *var_col2 = NULL;
    PyObject *var_row_i = NULL;
    PyObject *var_col_i = NULL;
    PyObject *var_fig_width_inch = NULL;
    PyObject *var_fig_height_inch = NULL;
    PyObject *var_kwargs = NULL;
    PyObject *var_hspace = NULL;
    PyObject *var_h_axes = NULL;
    PyObject *var_vspace = NULL;
    PyObject *var_v_axes = NULL;
    PyObject *outline_0_var_i = NULL;
    PyObject *outline_1_var_i = NULL;
    PyObject *outline_2_var_ax = NULL;
    PyObject *outline_3_var_ax = NULL;
    PyObject *outline_4_var_s = NULL;
    PyObject *outline_5_var_s = NULL;
    PyObject *outline_6_var_s = NULL;
    PyObject *outline_7_var_s = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_for_loop_4__for_iterator = NULL;
    PyObject *tmp_for_loop_4__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_genexpr_2__$0 = NULL;
    PyObject *tmp_genexpr_3__$0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_2__$0 = NULL;
    PyObject *tmp_listcomp_2__contraction = NULL;
    PyObject *tmp_listcomp_2__iter_value_0 = NULL;
    PyObject *tmp_listcomp_3__$0 = NULL;
    PyObject *tmp_listcomp_3__contraction = NULL;
    PyObject *tmp_listcomp_3__iter_value_0 = NULL;
    PyObject *tmp_listcomp_4__$0 = NULL;
    PyObject *tmp_listcomp_4__contraction = NULL;
    PyObject *tmp_listcomp_4__iter_value_0 = NULL;
    PyObject *tmp_listcomp_5__$0 = NULL;
    PyObject *tmp_listcomp_5__contraction = NULL;
    PyObject *tmp_listcomp_5__iter_value_0 = NULL;
    PyObject *tmp_listcomp_6__$0 = NULL;
    PyObject *tmp_listcomp_6__contraction = NULL;
    PyObject *tmp_listcomp_6__iter_value_0 = NULL;
    PyObject *tmp_listcomp_7__$0 = NULL;
    PyObject *tmp_listcomp_7__contraction = NULL;
    PyObject *tmp_listcomp_7__iter_value_0 = NULL;
    PyObject *tmp_listcomp_8__$0 = NULL;
    PyObject *tmp_listcomp_8__contraction = NULL;
    PyObject *tmp_listcomp_8__iter_value_0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__element_3 = NULL;
    PyObject *tmp_tuple_unpack_2__element_4 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__element_3 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_tuple_unpack_4__element_1 = NULL;
    PyObject *tmp_tuple_unpack_4__element_2 = NULL;
    PyObject *tmp_tuple_unpack_4__source_iter = NULL;
    PyObject *tmp_tuple_unpack_5__element_1 = NULL;
    PyObject *tmp_tuple_unpack_5__element_2 = NULL;
    PyObject *tmp_tuple_unpack_5__source_iter = NULL;
    PyObject *tmp_tuple_unpack_6__element_1 = NULL;
    PyObject *tmp_tuple_unpack_6__element_2 = NULL;
    PyObject *tmp_tuple_unpack_6__source_iter = NULL;
    PyObject *tmp_tuple_unpack_7__element_1 = NULL;
    PyObject *tmp_tuple_unpack_7__element_2 = NULL;
    PyObject *tmp_tuple_unpack_7__source_iter = NULL;
    struct Nuitka_FrameObject *frame_dfe501d74e083632d082979fedbf32ac;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    struct Nuitka_FrameObject *frame_824ebf8299dfdf1007010b362f6f7952_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_824ebf8299dfdf1007010b362f6f7952_2 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    struct Nuitka_FrameObject *frame_27f7d61a4c04233c81b9d4136132d885_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_27f7d61a4c04233c81b9d4136132d885_3 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    struct Nuitka_FrameObject *frame_fdc5cf8d6235923e8fd74553c5bc761c_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    static struct Nuitka_FrameObject *cache_frame_fdc5cf8d6235923e8fd74553c5bc761c_4 = NULL;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    struct Nuitka_FrameObject *frame_a7f3cffd0cdb809b88117fa0ae807d17_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    static struct Nuitka_FrameObject *cache_frame_a7f3cffd0cdb809b88117fa0ae807d17_5 = NULL;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *exception_keeper_type_22;
    PyObject *exception_keeper_value_22;
    PyTracebackObject *exception_keeper_tb_22;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_22;
    PyObject *exception_keeper_type_23;
    PyObject *exception_keeper_value_23;
    PyTracebackObject *exception_keeper_tb_23;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_23;
    PyObject *exception_keeper_type_24;
    PyObject *exception_keeper_value_24;
    PyTracebackObject *exception_keeper_tb_24;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_24;
    PyObject *exception_keeper_type_25;
    PyObject *exception_keeper_value_25;
    PyTracebackObject *exception_keeper_tb_25;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_25;
    PyObject *exception_keeper_type_26;
    PyObject *exception_keeper_value_26;
    PyTracebackObject *exception_keeper_tb_26;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_26;
    struct Nuitka_FrameObject *frame_b02ee1be6b09833b65bcb24d7211f57b_6;
    NUITKA_MAY_BE_UNUSED char const *type_description_6 = NULL;
    PyObject *exception_keeper_type_27;
    PyObject *exception_keeper_value_27;
    PyTracebackObject *exception_keeper_tb_27;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_27;
    static struct Nuitka_FrameObject *cache_frame_b02ee1be6b09833b65bcb24d7211f57b_6 = NULL;
    PyObject *exception_keeper_type_28;
    PyObject *exception_keeper_value_28;
    PyTracebackObject *exception_keeper_tb_28;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_28;
    bool tmp_result;
    struct Nuitka_FrameObject *frame_699e36f676ffc125d4d1ecef74569d68_7;
    NUITKA_MAY_BE_UNUSED char const *type_description_7 = NULL;
    PyObject *exception_keeper_type_29;
    PyObject *exception_keeper_value_29;
    PyTracebackObject *exception_keeper_tb_29;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_29;
    static struct Nuitka_FrameObject *cache_frame_699e36f676ffc125d4d1ecef74569d68_7 = NULL;
    PyObject *exception_keeper_type_30;
    PyObject *exception_keeper_value_30;
    PyTracebackObject *exception_keeper_tb_30;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_30;
    struct Nuitka_FrameObject *frame_52e777314c8cb5bf5b2e5ade68e5649a_8;
    NUITKA_MAY_BE_UNUSED char const *type_description_8 = NULL;
    PyObject *exception_keeper_type_31;
    PyObject *exception_keeper_value_31;
    PyTracebackObject *exception_keeper_tb_31;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_31;
    static struct Nuitka_FrameObject *cache_frame_52e777314c8cb5bf5b2e5ade68e5649a_8 = NULL;
    PyObject *exception_keeper_type_32;
    PyObject *exception_keeper_value_32;
    PyTracebackObject *exception_keeper_tb_32;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_32;
    struct Nuitka_FrameObject *frame_a945046174c9cda7ebe853bb1339324e_9;
    NUITKA_MAY_BE_UNUSED char const *type_description_9 = NULL;
    PyObject *exception_keeper_type_33;
    PyObject *exception_keeper_value_33;
    PyTracebackObject *exception_keeper_tb_33;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_33;
    static struct Nuitka_FrameObject *cache_frame_a945046174c9cda7ebe853bb1339324e_9 = NULL;
    PyObject *exception_keeper_type_34;
    PyObject *exception_keeper_value_34;
    PyTracebackObject *exception_keeper_tb_34;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_34;
    PyObject *tmp_return_value = NULL;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    static struct Nuitka_FrameObject *cache_frame_dfe501d74e083632d082979fedbf32ac = NULL;
    PyObject *exception_keeper_type_35;
    PyObject *exception_keeper_value_35;
    PyTracebackObject *exception_keeper_tb_35;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_35;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dfe501d74e083632d082979fedbf32ac, codeobj_dfe501d74e083632d082979fedbf32ac, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_dfe501d74e083632d082979fedbf32ac = cache_frame_dfe501d74e083632d082979fedbf32ac;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dfe501d74e083632d082979fedbf32ac );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dfe501d74e083632d082979fedbf32ac ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_nrows_ncols );
        tmp_iter_arg_1 = par_nrows_ncols;
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 62;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 62;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    exception_lineno = 62;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 62;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_rows == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_rows = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( PyCell_GET( var_cols ) == NULL );
        Py_INCREF( tmp_assign_source_5 );
        PyCell_SET( var_cols, tmp_assign_source_5 );

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_left_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_right_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_FontProperties );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FontProperties );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FontProperties" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 65;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_size;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 65;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_2;
        tmp_subscript_name_1 = const_str_digest_876c9648523deea8f3845ee2dd49a0d1;
        tmp_dict_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 65;
        tmp_called_instance_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 65;
        tmp_left_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_size_in_points );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = const_int_pos_72;
        tmp_assign_source_6 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_font_size_inches == NULL );
        var_font_size_inches = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( par_pad );
        tmp_left_name_2 = par_pad;
        CHECK_OBJECT( var_font_size_inches );
        tmp_right_name_2 = var_font_size_inches;
        tmp_assign_source_7 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_pad_inches == NULL );
        var_pad_inches = tmp_assign_source_7;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_h_pad );
        tmp_compexpr_left_1 = par_h_pad;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            CHECK_OBJECT( par_h_pad );
            tmp_left_name_3 = par_h_pad;
            CHECK_OBJECT( var_font_size_inches );
            tmp_right_name_3 = var_font_size_inches;
            tmp_assign_source_8 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_vpad_inches == NULL );
            var_vpad_inches = tmp_assign_source_8;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( var_pad_inches );
            tmp_assign_source_9 = var_pad_inches;
            assert( var_vpad_inches == NULL );
            Py_INCREF( tmp_assign_source_9 );
            var_vpad_inches = tmp_assign_source_9;
        }
        branch_end_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_w_pad );
        tmp_compexpr_left_2 = par_w_pad;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            CHECK_OBJECT( par_w_pad );
            tmp_left_name_4 = par_w_pad;
            CHECK_OBJECT( var_font_size_inches );
            tmp_right_name_4 = var_font_size_inches;
            tmp_assign_source_10 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_4, tmp_right_name_4 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 73;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_hpad_inches == NULL );
            var_hpad_inches = tmp_assign_source_10;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_11;
            CHECK_OBJECT( var_pad_inches );
            tmp_assign_source_11 = var_pad_inches;
            assert( var_hpad_inches == NULL );
            Py_INCREF( tmp_assign_source_11 );
            var_hpad_inches = tmp_assign_source_11;
        }
        branch_end_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_len_arg_2;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_len_arg_3;
        CHECK_OBJECT( par_num1num2_list );
        tmp_len_arg_1 = par_num1num2_list;
        tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_subplot_list );
        tmp_len_arg_2 = par_subplot_list;
        tmp_compexpr_right_3 = BUILTIN_LEN( tmp_len_arg_2 );
        if ( tmp_compexpr_right_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_3 );

            exception_lineno = 77;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        Py_DECREF( tmp_compexpr_right_3 );
        assert( !(tmp_res == -1) );
        tmp_or_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( par_subplot_list );
        tmp_len_arg_3 = par_subplot_list;
        tmp_compexpr_left_4 = BUILTIN_LEN( tmp_len_arg_3 );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        assert( !(tmp_res == -1) );
        tmp_or_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_3 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_3 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_ValueError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 78;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        CHECK_OBJECT( par_rect );
        tmp_compexpr_left_5 = par_rect;
        tmp_compexpr_right_5 = Py_None;
        tmp_condition_result_4 = ( tmp_compexpr_left_5 == tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = Py_None;
            assert( var_margin_left == NULL );
            Py_INCREF( tmp_assign_source_12 );
            var_margin_left = tmp_assign_source_12;
        }
        {
            PyObject *tmp_assign_source_13;
            tmp_assign_source_13 = Py_None;
            assert( var_margin_bottom == NULL );
            Py_INCREF( tmp_assign_source_13 );
            var_margin_bottom = tmp_assign_source_13;
        }
        {
            PyObject *tmp_assign_source_14;
            tmp_assign_source_14 = Py_None;
            assert( var_margin_right == NULL );
            Py_INCREF( tmp_assign_source_14 );
            var_margin_right = tmp_assign_source_14;
        }
        {
            PyObject *tmp_assign_source_15;
            tmp_assign_source_15 = Py_None;
            assert( var_margin_top == NULL );
            Py_INCREF( tmp_assign_source_15 );
            var_margin_top = tmp_assign_source_15;
        }
        goto branch_end_4;
        branch_no_4:;
        // Tried code:
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_iter_arg_2;
            CHECK_OBJECT( par_rect );
            tmp_iter_arg_2 = par_rect;
            tmp_assign_source_16 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
            if ( tmp_assign_source_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_4;
            }
            assert( tmp_tuple_unpack_2__source_iter == NULL );
            tmp_tuple_unpack_2__source_iter = tmp_assign_source_16;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_3, 0, 4 );
            if ( tmp_assign_source_17 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                exception_lineno = 83;
                goto try_except_handler_5;
            }
            assert( tmp_tuple_unpack_2__element_1 == NULL );
            tmp_tuple_unpack_2__element_1 = tmp_assign_source_17;
        }
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_4, 1, 4 );
            if ( tmp_assign_source_18 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                exception_lineno = 83;
                goto try_except_handler_5;
            }
            assert( tmp_tuple_unpack_2__element_2 == NULL );
            tmp_tuple_unpack_2__element_2 = tmp_assign_source_18;
        }
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_unpack_5;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_5 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_5, 2, 4 );
            if ( tmp_assign_source_19 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                exception_lineno = 83;
                goto try_except_handler_5;
            }
            assert( tmp_tuple_unpack_2__element_3 == NULL );
            tmp_tuple_unpack_2__element_3 = tmp_assign_source_19;
        }
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_unpack_6;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_6 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_20 = UNPACK_NEXT( tmp_unpack_6, 3, 4 );
            if ( tmp_assign_source_20 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                exception_lineno = 83;
                goto try_except_handler_5;
            }
            assert( tmp_tuple_unpack_2__element_4 == NULL );
            tmp_tuple_unpack_2__element_4 = tmp_assign_source_20;
        }
        {
            PyObject *tmp_iterator_name_2;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                        exception_lineno = 83;
                        goto try_except_handler_5;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 4)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                exception_lineno = 83;
                goto try_except_handler_5;
            }
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        try_end_3:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_3 );
        tmp_tuple_unpack_2__element_3 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_4 );
        tmp_tuple_unpack_2__element_4 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        {
            PyObject *tmp_assign_source_21;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
            tmp_assign_source_21 = tmp_tuple_unpack_2__element_1;
            assert( var_margin_left == NULL );
            Py_INCREF( tmp_assign_source_21 );
            var_margin_left = tmp_assign_source_21;
        }
        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        {
            PyObject *tmp_assign_source_22;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
            tmp_assign_source_22 = tmp_tuple_unpack_2__element_2;
            assert( var_margin_bottom == NULL );
            Py_INCREF( tmp_assign_source_22 );
            var_margin_bottom = tmp_assign_source_22;
        }
        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        {
            PyObject *tmp_assign_source_23;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_3 );
            tmp_assign_source_23 = tmp_tuple_unpack_2__element_3;
            assert( var__right == NULL );
            Py_INCREF( tmp_assign_source_23 );
            var__right = tmp_assign_source_23;
        }
        Py_XDECREF( tmp_tuple_unpack_2__element_3 );
        tmp_tuple_unpack_2__element_3 = NULL;

        {
            PyObject *tmp_assign_source_24;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_4 );
            tmp_assign_source_24 = tmp_tuple_unpack_2__element_4;
            assert( var__top == NULL );
            Py_INCREF( tmp_assign_source_24 );
            var__top = tmp_assign_source_24;
        }
        Py_XDECREF( tmp_tuple_unpack_2__element_4 );
        tmp_tuple_unpack_2__element_4 = NULL;

        {
            nuitka_bool tmp_condition_result_5;
            int tmp_truth_name_1;
            CHECK_OBJECT( var__right );
            tmp_truth_name_1 = CHECK_IF_TRUE( var__right );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_25;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                tmp_left_name_5 = const_int_pos_1;
                CHECK_OBJECT( var__right );
                tmp_right_name_5 = var__right;
                tmp_assign_source_25 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_5, tmp_right_name_5 );
                if ( tmp_assign_source_25 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 85;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_margin_right == NULL );
                var_margin_right = tmp_assign_source_25;
            }
            goto branch_end_5;
            branch_no_5:;
            {
                PyObject *tmp_assign_source_26;
                tmp_assign_source_26 = Py_None;
                assert( var_margin_right == NULL );
                Py_INCREF( tmp_assign_source_26 );
                var_margin_right = tmp_assign_source_26;
            }
            branch_end_5:;
        }
        {
            nuitka_bool tmp_condition_result_6;
            int tmp_truth_name_2;
            CHECK_OBJECT( var__top );
            tmp_truth_name_2 = CHECK_IF_TRUE( var__top );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_6 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_assign_source_27;
                PyObject *tmp_left_name_6;
                PyObject *tmp_right_name_6;
                tmp_left_name_6 = const_int_pos_1;
                CHECK_OBJECT( var__top );
                tmp_right_name_6 = var__top;
                tmp_assign_source_27 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_6, tmp_right_name_6 );
                if ( tmp_assign_source_27 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 89;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_margin_top == NULL );
                var_margin_top = tmp_assign_source_27;
            }
            goto branch_end_6;
            branch_no_6:;
            {
                PyObject *tmp_assign_source_28;
                tmp_assign_source_28 = Py_None;
                assert( var_margin_top == NULL );
                Py_INCREF( tmp_assign_source_28 );
                var_margin_top = tmp_assign_source_28;
            }
            branch_end_6:;
        }
        branch_end_4:;
    }
    {
        PyObject *tmp_assign_source_29;
        // Tried code:
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_iter_arg_3;
            PyObject *tmp_xrange_low_1;
            PyObject *tmp_left_name_7;
            PyObject *tmp_left_name_8;
            PyObject *tmp_right_name_7;
            PyObject *tmp_right_name_8;
            CHECK_OBJECT( var_rows );
            tmp_left_name_8 = var_rows;
            tmp_right_name_7 = const_int_pos_1;
            tmp_left_name_7 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_8, tmp_right_name_7 );
            if ( tmp_left_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_6;
            }
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_right_name_8 = PyCell_GET( var_cols );
            tmp_xrange_low_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_8 );
            Py_DECREF( tmp_left_name_7 );
            if ( tmp_xrange_low_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_6;
            }
            tmp_iter_arg_3 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
            Py_DECREF( tmp_xrange_low_1 );
            if ( tmp_iter_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_6;
            }
            tmp_assign_source_30 = MAKE_ITERATOR( tmp_iter_arg_3 );
            Py_DECREF( tmp_iter_arg_3 );
            if ( tmp_assign_source_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_6;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_30;
        }
        {
            PyObject *tmp_assign_source_31;
            tmp_assign_source_31 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_31;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_824ebf8299dfdf1007010b362f6f7952_2, codeobj_824ebf8299dfdf1007010b362f6f7952, module_matplotlib$tight_layout, sizeof(void *) );
        frame_824ebf8299dfdf1007010b362f6f7952_2 = cache_frame_824ebf8299dfdf1007010b362f6f7952_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_824ebf8299dfdf1007010b362f6f7952_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_824ebf8299dfdf1007010b362f6f7952_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_32;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_32 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_32 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 93;
                    goto try_except_handler_7;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_32;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_33;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_33 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_i;
                outline_0_var_i = tmp_assign_source_33;
                Py_INCREF( outline_0_var_i );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_append_value_1 = PyList_New( 0 );
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_2 = "o";
                goto try_except_handler_7;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_2 = "o";
            goto try_except_handler_7;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_29 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_29 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_824ebf8299dfdf1007010b362f6f7952_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_824ebf8299dfdf1007010b362f6f7952_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_6;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_824ebf8299dfdf1007010b362f6f7952_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_824ebf8299dfdf1007010b362f6f7952_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_824ebf8299dfdf1007010b362f6f7952_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_824ebf8299dfdf1007010b362f6f7952_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_824ebf8299dfdf1007010b362f6f7952_2,
            type_description_2,
            outline_0_var_i
        );


        // Release cached frame.
        if ( frame_824ebf8299dfdf1007010b362f6f7952_2 == cache_frame_824ebf8299dfdf1007010b362f6f7952_2 )
        {
            Py_DECREF( frame_824ebf8299dfdf1007010b362f6f7952_2 );
        }
        cache_frame_824ebf8299dfdf1007010b362f6f7952_2 = NULL;

        assertFrameObject( frame_824ebf8299dfdf1007010b362f6f7952_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
        goto try_except_handler_6;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        // Return handler code:
        try_return_handler_6:;
        Py_XDECREF( outline_0_var_i );
        outline_0_var_i = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_i );
        outline_0_var_i = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        outline_exception_1:;
        exception_lineno = 93;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_vspaces == NULL );
        var_vspaces = tmp_assign_source_29;
    }
    {
        PyObject *tmp_assign_source_34;
        // Tried code:
        {
            PyObject *tmp_assign_source_35;
            PyObject *tmp_iter_arg_4;
            PyObject *tmp_xrange_low_2;
            PyObject *tmp_left_name_9;
            PyObject *tmp_right_name_9;
            PyObject *tmp_left_name_10;
            PyObject *tmp_right_name_10;
            CHECK_OBJECT( var_rows );
            tmp_left_name_9 = var_rows;
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_left_name_10 = PyCell_GET( var_cols );
            tmp_right_name_10 = const_int_pos_1;
            tmp_right_name_9 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_10, tmp_right_name_10 );
            if ( tmp_right_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_8;
            }
            tmp_xrange_low_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_9, tmp_right_name_9 );
            Py_DECREF( tmp_right_name_9 );
            if ( tmp_xrange_low_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_8;
            }
            tmp_iter_arg_4 = BUILTIN_XRANGE1( tmp_xrange_low_2 );
            Py_DECREF( tmp_xrange_low_2 );
            if ( tmp_iter_arg_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_8;
            }
            tmp_assign_source_35 = MAKE_ITERATOR( tmp_iter_arg_4 );
            Py_DECREF( tmp_iter_arg_4 );
            if ( tmp_assign_source_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_8;
            }
            assert( tmp_listcomp_2__$0 == NULL );
            tmp_listcomp_2__$0 = tmp_assign_source_35;
        }
        {
            PyObject *tmp_assign_source_36;
            tmp_assign_source_36 = PyList_New( 0 );
            assert( tmp_listcomp_2__contraction == NULL );
            tmp_listcomp_2__contraction = tmp_assign_source_36;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_27f7d61a4c04233c81b9d4136132d885_3, codeobj_27f7d61a4c04233c81b9d4136132d885, module_matplotlib$tight_layout, sizeof(void *) );
        frame_27f7d61a4c04233c81b9d4136132d885_3 = cache_frame_27f7d61a4c04233c81b9d4136132d885_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_27f7d61a4c04233c81b9d4136132d885_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_27f7d61a4c04233c81b9d4136132d885_3 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_37;
            CHECK_OBJECT( tmp_listcomp_2__$0 );
            tmp_next_source_2 = tmp_listcomp_2__$0;
            tmp_assign_source_37 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_37 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 94;
                    goto try_except_handler_9;
                }
            }

            {
                PyObject *old = tmp_listcomp_2__iter_value_0;
                tmp_listcomp_2__iter_value_0 = tmp_assign_source_37;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_38;
            CHECK_OBJECT( tmp_listcomp_2__iter_value_0 );
            tmp_assign_source_38 = tmp_listcomp_2__iter_value_0;
            {
                PyObject *old = outline_1_var_i;
                outline_1_var_i = tmp_assign_source_38;
                Py_INCREF( outline_1_var_i );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_2;
            PyObject *tmp_append_value_2;
            CHECK_OBJECT( tmp_listcomp_2__contraction );
            tmp_append_list_2 = tmp_listcomp_2__contraction;
            tmp_append_value_2 = PyList_New( 0 );
            assert( PyList_Check( tmp_append_list_2 ) );
            tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
            Py_DECREF( tmp_append_value_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_2 = "o";
                goto try_except_handler_9;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_2 = "o";
            goto try_except_handler_9;
        }
        goto loop_start_2;
        loop_end_2:;
        CHECK_OBJECT( tmp_listcomp_2__contraction );
        tmp_assign_source_34 = tmp_listcomp_2__contraction;
        Py_INCREF( tmp_assign_source_34 );
        goto try_return_handler_9;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        // Return handler code:
        try_return_handler_9:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        goto frame_return_exit_3;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto frame_exception_exit_3;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_27f7d61a4c04233c81b9d4136132d885_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_return_exit_3:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_27f7d61a4c04233c81b9d4136132d885_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_8;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_27f7d61a4c04233c81b9d4136132d885_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_27f7d61a4c04233c81b9d4136132d885_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_27f7d61a4c04233c81b9d4136132d885_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_27f7d61a4c04233c81b9d4136132d885_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_27f7d61a4c04233c81b9d4136132d885_3,
            type_description_2,
            outline_1_var_i
        );


        // Release cached frame.
        if ( frame_27f7d61a4c04233c81b9d4136132d885_3 == cache_frame_27f7d61a4c04233c81b9d4136132d885_3 )
        {
            Py_DECREF( frame_27f7d61a4c04233c81b9d4136132d885_3 );
        }
        cache_frame_27f7d61a4c04233c81b9d4136132d885_3 = NULL;

        assertFrameObject( frame_27f7d61a4c04233c81b9d4136132d885_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;
        type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
        goto try_except_handler_8;
        skip_nested_handling_2:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        // Return handler code:
        try_return_handler_8:;
        Py_XDECREF( outline_1_var_i );
        outline_1_var_i = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_1_var_i );
        outline_1_var_i = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        outline_exception_2:;
        exception_lineno = 94;
        goto frame_exception_exit_1;
        outline_result_2:;
        assert( PyCell_GET( var_hspaces ) == NULL );
        PyCell_SET( var_hspaces, tmp_assign_source_34 );

    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_Bbox );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Bbox );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Bbox" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_assign_source_39 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_union );
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_union == NULL );
        var_union = tmp_assign_source_39;
    }
    {
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        CHECK_OBJECT( par_ax_bbox_list );
        tmp_compexpr_left_6 = par_ax_bbox_list;
        tmp_compexpr_right_6 = Py_None;
        tmp_condition_result_7 = ( tmp_compexpr_left_6 == tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_40;
            tmp_assign_source_40 = PyList_New( 0 );
            {
                PyObject *old = par_ax_bbox_list;
                assert( old != NULL );
                par_ax_bbox_list = tmp_assign_source_40;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_41;
            PyObject *tmp_iter_arg_5;
            CHECK_OBJECT( par_subplot_list );
            tmp_iter_arg_5 = par_subplot_list;
            tmp_assign_source_41 = MAKE_ITERATOR( tmp_iter_arg_5 );
            if ( tmp_assign_source_41 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_41;
        }
        // Tried code:
        loop_start_3:;
        {
            PyObject *tmp_next_source_3;
            PyObject *tmp_assign_source_42;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_3 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_42 = ITERATOR_NEXT( tmp_next_source_3 );
            if ( tmp_assign_source_42 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_3;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    exception_lineno = 100;
                    goto try_except_handler_10;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_42;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_43;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_43 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_subplots;
                var_subplots = tmp_assign_source_43;
                Py_INCREF( var_subplots );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var_union );
            tmp_called_name_2 = var_union;
            // Tried code:
            {
                PyObject *tmp_assign_source_45;
                PyObject *tmp_iter_arg_6;
                CHECK_OBJECT( var_subplots );
                tmp_iter_arg_6 = var_subplots;
                tmp_assign_source_45 = MAKE_ITERATOR( tmp_iter_arg_6 );
                if ( tmp_assign_source_45 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 101;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_11;
                }
                {
                    PyObject *old = tmp_listcomp_3__$0;
                    tmp_listcomp_3__$0 = tmp_assign_source_45;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_46;
                tmp_assign_source_46 = PyList_New( 0 );
                {
                    PyObject *old = tmp_listcomp_3__contraction;
                    tmp_listcomp_3__contraction = tmp_assign_source_46;
                    Py_XDECREF( old );
                }

            }
            MAKE_OR_REUSE_FRAME( cache_frame_fdc5cf8d6235923e8fd74553c5bc761c_4, codeobj_fdc5cf8d6235923e8fd74553c5bc761c, module_matplotlib$tight_layout, sizeof(void *) );
            frame_fdc5cf8d6235923e8fd74553c5bc761c_4 = cache_frame_fdc5cf8d6235923e8fd74553c5bc761c_4;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_fdc5cf8d6235923e8fd74553c5bc761c_4 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_fdc5cf8d6235923e8fd74553c5bc761c_4 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_4:;
            {
                PyObject *tmp_next_source_4;
                PyObject *tmp_assign_source_47;
                CHECK_OBJECT( tmp_listcomp_3__$0 );
                tmp_next_source_4 = tmp_listcomp_3__$0;
                tmp_assign_source_47 = ITERATOR_NEXT( tmp_next_source_4 );
                if ( tmp_assign_source_47 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_4;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "o";
                        exception_lineno = 101;
                        goto try_except_handler_12;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_3__iter_value_0;
                    tmp_listcomp_3__iter_value_0 = tmp_assign_source_47;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_48;
                CHECK_OBJECT( tmp_listcomp_3__iter_value_0 );
                tmp_assign_source_48 = tmp_listcomp_3__iter_value_0;
                {
                    PyObject *old = outline_2_var_ax;
                    outline_2_var_ax = tmp_assign_source_48;
                    Py_INCREF( outline_2_var_ax );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_3;
                PyObject *tmp_append_value_3;
                PyObject *tmp_called_name_3;
                PyObject *tmp_source_name_2;
                PyObject *tmp_kw_name_2;
                CHECK_OBJECT( tmp_listcomp_3__contraction );
                tmp_append_list_3 = tmp_listcomp_3__contraction;
                CHECK_OBJECT( outline_2_var_ax );
                tmp_source_name_2 = outline_2_var_ax;
                tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_get_position );
                if ( tmp_called_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 101;
                    type_description_2 = "o";
                    goto try_except_handler_12;
                }
                tmp_kw_name_2 = PyDict_Copy( const_dict_2c333e641b5bd70d3db5933b70b90929 );
                frame_fdc5cf8d6235923e8fd74553c5bc761c_4->m_frame.f_lineno = 101;
                tmp_append_value_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_2 );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_kw_name_2 );
                if ( tmp_append_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 101;
                    type_description_2 = "o";
                    goto try_except_handler_12;
                }
                assert( PyList_Check( tmp_append_list_3 ) );
                tmp_res = PyList_Append( tmp_append_list_3, tmp_append_value_3 );
                Py_DECREF( tmp_append_value_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 101;
                    type_description_2 = "o";
                    goto try_except_handler_12;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 101;
                type_description_2 = "o";
                goto try_except_handler_12;
            }
            goto loop_start_4;
            loop_end_4:;
            CHECK_OBJECT( tmp_listcomp_3__contraction );
            tmp_args_element_name_1 = tmp_listcomp_3__contraction;
            Py_INCREF( tmp_args_element_name_1 );
            goto try_return_handler_12;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_12:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_3__$0 );
            Py_DECREF( tmp_listcomp_3__$0 );
            tmp_listcomp_3__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_3__contraction );
            Py_DECREF( tmp_listcomp_3__contraction );
            tmp_listcomp_3__contraction = NULL;

            Py_XDECREF( tmp_listcomp_3__iter_value_0 );
            tmp_listcomp_3__iter_value_0 = NULL;

            goto frame_return_exit_4;
            // Exception handler code:
            try_except_handler_12:;
            exception_keeper_type_9 = exception_type;
            exception_keeper_value_9 = exception_value;
            exception_keeper_tb_9 = exception_tb;
            exception_keeper_lineno_9 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_3__$0 );
            Py_DECREF( tmp_listcomp_3__$0 );
            tmp_listcomp_3__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_3__contraction );
            Py_DECREF( tmp_listcomp_3__contraction );
            tmp_listcomp_3__contraction = NULL;

            Py_XDECREF( tmp_listcomp_3__iter_value_0 );
            tmp_listcomp_3__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_9;
            exception_value = exception_keeper_value_9;
            exception_tb = exception_keeper_tb_9;
            exception_lineno = exception_keeper_lineno_9;

            goto frame_exception_exit_4;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_fdc5cf8d6235923e8fd74553c5bc761c_4 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_3;

            frame_return_exit_4:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_fdc5cf8d6235923e8fd74553c5bc761c_4 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_11;

            frame_exception_exit_4:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_fdc5cf8d6235923e8fd74553c5bc761c_4 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_fdc5cf8d6235923e8fd74553c5bc761c_4, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_fdc5cf8d6235923e8fd74553c5bc761c_4->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_fdc5cf8d6235923e8fd74553c5bc761c_4, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_fdc5cf8d6235923e8fd74553c5bc761c_4,
                type_description_2,
                outline_2_var_ax
            );


            // Release cached frame.
            if ( frame_fdc5cf8d6235923e8fd74553c5bc761c_4 == cache_frame_fdc5cf8d6235923e8fd74553c5bc761c_4 )
            {
                Py_DECREF( frame_fdc5cf8d6235923e8fd74553c5bc761c_4 );
            }
            cache_frame_fdc5cf8d6235923e8fd74553c5bc761c_4 = NULL;

            assertFrameObject( frame_fdc5cf8d6235923e8fd74553c5bc761c_4 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_3;

            frame_no_exception_3:;
            goto skip_nested_handling_3;
            nested_frame_exit_3:;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_11;
            skip_nested_handling_3:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_11:;
            Py_XDECREF( outline_2_var_ax );
            outline_2_var_ax = NULL;

            goto outline_result_3;
            // Exception handler code:
            try_except_handler_11:;
            exception_keeper_type_10 = exception_type;
            exception_keeper_value_10 = exception_value;
            exception_keeper_tb_10 = exception_tb;
            exception_keeper_lineno_10 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_2_var_ax );
            outline_2_var_ax = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_10;
            exception_value = exception_keeper_value_10;
            exception_tb = exception_keeper_tb_10;
            exception_lineno = exception_keeper_lineno_10;

            goto outline_exception_3;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            outline_exception_3:;
            exception_lineno = 101;
            goto try_except_handler_10;
            outline_result_3:;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 101;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_44 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_assign_source_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 101;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_10;
            }
            {
                PyObject *old = var_ax_bbox;
                var_ax_bbox = tmp_assign_source_44;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_ax_bbox_list );
            tmp_called_instance_2 = par_ax_bbox_list;
            CHECK_OBJECT( var_ax_bbox );
            tmp_args_element_name_2 = var_ax_bbox;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 103;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 103;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_10;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_10;
        }
        goto loop_start_3;
        loop_end_3:;
        goto try_end_5;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;
        exception_lineno = exception_keeper_lineno_11;

        goto frame_exception_exit_1;
        // End of try:
        try_end_5:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        branch_no_7:;
    }
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_iter_arg_7;
        PyObject *tmp_called_name_4;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        tmp_called_name_4 = (PyObject *)&PyZip_Type;
        CHECK_OBJECT( par_subplot_list );
        tmp_args_element_name_3 = par_subplot_list;
        CHECK_OBJECT( par_ax_bbox_list );
        tmp_args_element_name_4 = par_ax_bbox_list;
        CHECK_OBJECT( par_num1num2_list );
        tmp_args_element_name_5 = par_num1num2_list;
        frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 105;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_iter_arg_7 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
        }

        if ( tmp_iter_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_49 = MAKE_ITERATOR( tmp_iter_arg_7 );
        Py_DECREF( tmp_iter_arg_7 );
        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_2__for_iterator == NULL );
        tmp_for_loop_2__for_iterator = tmp_assign_source_49;
    }
    // Tried code:
    loop_start_5:;
    {
        PyObject *tmp_next_source_5;
        PyObject *tmp_assign_source_50;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_5 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_50 = ITERATOR_NEXT( tmp_next_source_5 );
        if ( tmp_assign_source_50 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_5;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                exception_lineno = 105;
                goto try_except_handler_13;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_50;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_iter_arg_8;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_iter_arg_8 = tmp_for_loop_2__iter_value;
        tmp_assign_source_51 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_8 );
        if ( tmp_assign_source_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_14;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__source_iter;
            tmp_tuple_unpack_3__source_iter = tmp_assign_source_51;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_52;
        PyObject *tmp_unpack_7;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_7 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_52 = UNPACK_NEXT( tmp_unpack_7, 0, 3 );
        if ( tmp_assign_source_52 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 105;
            goto try_except_handler_15;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__element_1;
            tmp_tuple_unpack_3__element_1 = tmp_assign_source_52;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_53;
        PyObject *tmp_unpack_8;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_8 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_53 = UNPACK_NEXT( tmp_unpack_8, 1, 3 );
        if ( tmp_assign_source_53 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 105;
            goto try_except_handler_15;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__element_2;
            tmp_tuple_unpack_3__element_2 = tmp_assign_source_53;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_54;
        PyObject *tmp_unpack_9;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_9 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_54 = UNPACK_NEXT( tmp_unpack_9, 2, 3 );
        if ( tmp_assign_source_54 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 105;
            goto try_except_handler_15;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__element_3;
            tmp_tuple_unpack_3__element_3 = tmp_assign_source_54;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_3;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_iterator_name_3 = tmp_tuple_unpack_3__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    exception_lineno = 105;
                    goto try_except_handler_15;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 105;
            goto try_except_handler_15;
        }
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_15:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto try_except_handler_14;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    {
        PyObject *tmp_assign_source_55;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
        tmp_assign_source_55 = tmp_tuple_unpack_3__element_1;
        {
            PyObject *old = var_subplots;
            var_subplots = tmp_assign_source_55;
            Py_INCREF( var_subplots );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    {
        PyObject *tmp_assign_source_56;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
        tmp_assign_source_56 = tmp_tuple_unpack_3__element_2;
        {
            PyObject *old = var_ax_bbox;
            var_ax_bbox = tmp_assign_source_56;
            Py_INCREF( var_ax_bbox );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_57;
        PyObject *tmp_iter_arg_9;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_3 );
        tmp_iter_arg_9 = tmp_tuple_unpack_3__element_3;
        tmp_assign_source_57 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_9 );
        if ( tmp_assign_source_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_16;
        }
        {
            PyObject *old = tmp_tuple_unpack_4__source_iter;
            tmp_tuple_unpack_4__source_iter = tmp_assign_source_57;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_58;
        PyObject *tmp_unpack_10;
        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
        tmp_unpack_10 = tmp_tuple_unpack_4__source_iter;
        tmp_assign_source_58 = UNPACK_NEXT( tmp_unpack_10, 0, 2 );
        if ( tmp_assign_source_58 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 105;
            goto try_except_handler_17;
        }
        {
            PyObject *old = tmp_tuple_unpack_4__element_1;
            tmp_tuple_unpack_4__element_1 = tmp_assign_source_58;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_59;
        PyObject *tmp_unpack_11;
        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
        tmp_unpack_11 = tmp_tuple_unpack_4__source_iter;
        tmp_assign_source_59 = UNPACK_NEXT( tmp_unpack_11, 1, 2 );
        if ( tmp_assign_source_59 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 105;
            goto try_except_handler_17;
        }
        {
            PyObject *old = tmp_tuple_unpack_4__element_2;
            tmp_tuple_unpack_4__element_2 = tmp_assign_source_59;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_4;
        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
        tmp_iterator_name_4 = tmp_tuple_unpack_4__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_4 ); assert( HAS_ITERNEXT( tmp_iterator_name_4 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_4 )->tp_iternext)( tmp_iterator_name_4 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    exception_lineno = 105;
                    goto try_except_handler_17;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 105;
            goto try_except_handler_17;
        }
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_17:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
    Py_DECREF( tmp_tuple_unpack_4__source_iter );
    tmp_tuple_unpack_4__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto try_except_handler_16;
    // End of try:
    try_end_7:;
    goto try_end_8;
    // Exception handler code:
    try_except_handler_16:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_4__element_1 );
    tmp_tuple_unpack_4__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_4__element_2 );
    tmp_tuple_unpack_4__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto try_except_handler_14;
    // End of try:
    try_end_8:;
    goto try_end_9;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_3 );
    tmp_tuple_unpack_3__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto try_except_handler_13;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
    Py_DECREF( tmp_tuple_unpack_4__source_iter );
    tmp_tuple_unpack_4__source_iter = NULL;

    {
        PyObject *tmp_assign_source_60;
        CHECK_OBJECT( tmp_tuple_unpack_4__element_1 );
        tmp_assign_source_60 = tmp_tuple_unpack_4__element_1;
        {
            PyObject *old = var_num1;
            var_num1 = tmp_assign_source_60;
            Py_INCREF( var_num1 );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_4__element_1 );
    tmp_tuple_unpack_4__element_1 = NULL;

    {
        PyObject *tmp_assign_source_61;
        CHECK_OBJECT( tmp_tuple_unpack_4__element_2 );
        tmp_assign_source_61 = tmp_tuple_unpack_4__element_2;
        {
            PyObject *old = var_num2;
            var_num2 = tmp_assign_source_61;
            Py_INCREF( var_num2 );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_4__element_2 );
    tmp_tuple_unpack_4__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_3 );
    tmp_tuple_unpack_3__element_3 = NULL;

    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_called_name_5;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_6;
        int tmp_truth_name_3;
        tmp_called_name_5 = LOOKUP_BUILTIN( const_str_plain_all );
        assert( tmp_called_name_5 != NULL );
        {
            PyObject *tmp_assign_source_62;
            PyObject *tmp_iter_arg_10;
            CHECK_OBJECT( var_subplots );
            tmp_iter_arg_10 = var_subplots;
            tmp_assign_source_62 = MAKE_ITERATOR( tmp_iter_arg_10 );
            if ( tmp_assign_source_62 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 108;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            {
                PyObject *old = tmp_genexpr_1__$0;
                tmp_genexpr_1__$0 = tmp_assign_source_62;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        tmp_args_element_name_6 = matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_6)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_18;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        // Return handler code:
        try_return_handler_18:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_4;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        outline_result_4:;
        frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 108;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_13;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 108;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_13;
        }
        tmp_condition_result_8 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        goto loop_start_5;
        branch_no_8:;
    }
    {
        PyObject *tmp_assign_source_63;
        PyObject *tmp_called_name_6;
        PyObject *tmp_args_element_name_7;
        CHECK_OBJECT( var_union );
        tmp_called_name_6 = var_union;
        // Tried code:
        {
            PyObject *tmp_assign_source_64;
            PyObject *tmp_iter_arg_11;
            CHECK_OBJECT( var_subplots );
            tmp_iter_arg_11 = var_subplots;
            tmp_assign_source_64 = MAKE_ITERATOR( tmp_iter_arg_11 );
            if ( tmp_assign_source_64 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 111;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_19;
            }
            {
                PyObject *old = tmp_listcomp_4__$0;
                tmp_listcomp_4__$0 = tmp_assign_source_64;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_65;
            tmp_assign_source_65 = PyList_New( 0 );
            {
                PyObject *old = tmp_listcomp_4__contraction;
                tmp_listcomp_4__contraction = tmp_assign_source_65;
                Py_XDECREF( old );
            }

        }
        MAKE_OR_REUSE_FRAME( cache_frame_a7f3cffd0cdb809b88117fa0ae807d17_5, codeobj_a7f3cffd0cdb809b88117fa0ae807d17, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *) );
        frame_a7f3cffd0cdb809b88117fa0ae807d17_5 = cache_frame_a7f3cffd0cdb809b88117fa0ae807d17_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_a7f3cffd0cdb809b88117fa0ae807d17_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_a7f3cffd0cdb809b88117fa0ae807d17_5 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_6:;
        {
            PyObject *tmp_next_source_6;
            PyObject *tmp_assign_source_66;
            CHECK_OBJECT( tmp_listcomp_4__$0 );
            tmp_next_source_6 = tmp_listcomp_4__$0;
            tmp_assign_source_66 = ITERATOR_NEXT( tmp_next_source_6 );
            if ( tmp_assign_source_66 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_6;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 111;
                    goto try_except_handler_20;
                }
            }

            {
                PyObject *old = tmp_listcomp_4__iter_value_0;
                tmp_listcomp_4__iter_value_0 = tmp_assign_source_66;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_67;
            CHECK_OBJECT( tmp_listcomp_4__iter_value_0 );
            tmp_assign_source_67 = tmp_listcomp_4__iter_value_0;
            {
                PyObject *old = outline_3_var_ax;
                outline_3_var_ax = tmp_assign_source_67;
                Py_INCREF( outline_3_var_ax );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_3;
            int tmp_truth_name_4;
            CHECK_OBJECT( outline_3_var_ax );
            tmp_called_instance_3 = outline_3_var_ax;
            frame_a7f3cffd0cdb809b88117fa0ae807d17_5->m_frame.f_lineno = 112;
            tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_visible );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "oo";
                goto try_except_handler_20;
            }
            tmp_truth_name_4 = CHECK_IF_TRUE( tmp_call_result_3 );
            if ( tmp_truth_name_4 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_3 );

                exception_lineno = 112;
                type_description_2 = "oo";
                goto try_except_handler_20;
            }
            tmp_condition_result_9 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_3 );
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                PyObject *tmp_append_list_4;
                PyObject *tmp_append_value_4;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_args_element_name_8;
                CHECK_OBJECT( tmp_listcomp_4__contraction );
                tmp_append_list_4 = tmp_listcomp_4__contraction;
                CHECK_OBJECT( outline_3_var_ax );
                tmp_called_instance_4 = outline_3_var_ax;
                CHECK_OBJECT( par_renderer );
                tmp_args_element_name_8 = par_renderer;
                frame_a7f3cffd0cdb809b88117fa0ae807d17_5->m_frame.f_lineno = 111;
                {
                    PyObject *call_args[] = { tmp_args_element_name_8 };
                    tmp_append_value_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_get_tightbbox, call_args );
                }

                if ( tmp_append_value_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;
                    type_description_2 = "oo";
                    goto try_except_handler_20;
                }
                assert( PyList_Check( tmp_append_list_4 ) );
                tmp_res = PyList_Append( tmp_append_list_4, tmp_append_value_4 );
                Py_DECREF( tmp_append_value_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;
                    type_description_2 = "oo";
                    goto try_except_handler_20;
                }
            }
            branch_no_9:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_2 = "oo";
            goto try_except_handler_20;
        }
        goto loop_start_6;
        loop_end_6:;
        CHECK_OBJECT( tmp_listcomp_4__contraction );
        tmp_args_element_name_7 = tmp_listcomp_4__contraction;
        Py_INCREF( tmp_args_element_name_7 );
        goto try_return_handler_20;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        // Return handler code:
        try_return_handler_20:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_4__$0 );
        Py_DECREF( tmp_listcomp_4__$0 );
        tmp_listcomp_4__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_4__contraction );
        Py_DECREF( tmp_listcomp_4__contraction );
        tmp_listcomp_4__contraction = NULL;

        Py_XDECREF( tmp_listcomp_4__iter_value_0 );
        tmp_listcomp_4__iter_value_0 = NULL;

        goto frame_return_exit_5;
        // Exception handler code:
        try_except_handler_20:;
        exception_keeper_type_16 = exception_type;
        exception_keeper_value_16 = exception_value;
        exception_keeper_tb_16 = exception_tb;
        exception_keeper_lineno_16 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_4__$0 );
        Py_DECREF( tmp_listcomp_4__$0 );
        tmp_listcomp_4__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_4__contraction );
        Py_DECREF( tmp_listcomp_4__contraction );
        tmp_listcomp_4__contraction = NULL;

        Py_XDECREF( tmp_listcomp_4__iter_value_0 );
        tmp_listcomp_4__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_16;
        exception_value = exception_keeper_value_16;
        exception_tb = exception_keeper_tb_16;
        exception_lineno = exception_keeper_lineno_16;

        goto frame_exception_exit_5;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_a7f3cffd0cdb809b88117fa0ae807d17_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_return_exit_5:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_a7f3cffd0cdb809b88117fa0ae807d17_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_19;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_a7f3cffd0cdb809b88117fa0ae807d17_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_a7f3cffd0cdb809b88117fa0ae807d17_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_a7f3cffd0cdb809b88117fa0ae807d17_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_a7f3cffd0cdb809b88117fa0ae807d17_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_a7f3cffd0cdb809b88117fa0ae807d17_5,
            type_description_2,
            outline_3_var_ax,
            par_renderer
        );


        // Release cached frame.
        if ( frame_a7f3cffd0cdb809b88117fa0ae807d17_5 == cache_frame_a7f3cffd0cdb809b88117fa0ae807d17_5 )
        {
            Py_DECREF( frame_a7f3cffd0cdb809b88117fa0ae807d17_5 );
        }
        cache_frame_a7f3cffd0cdb809b88117fa0ae807d17_5 = NULL;

        assertFrameObject( frame_a7f3cffd0cdb809b88117fa0ae807d17_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_4:;
        type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
        goto try_except_handler_19;
        skip_nested_handling_4:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        // Return handler code:
        try_return_handler_19:;
        Py_XDECREF( outline_3_var_ax );
        outline_3_var_ax = NULL;

        goto outline_result_5;
        // Exception handler code:
        try_except_handler_19:;
        exception_keeper_type_17 = exception_type;
        exception_keeper_value_17 = exception_value;
        exception_keeper_tb_17 = exception_tb;
        exception_keeper_lineno_17 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_3_var_ax );
        outline_3_var_ax = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_17;
        exception_value = exception_keeper_value_17;
        exception_tb = exception_keeper_tb_17;
        exception_lineno = exception_keeper_lineno_17;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
        return NULL;
        outline_exception_4:;
        exception_lineno = 111;
        goto try_except_handler_13;
        outline_result_5:;
        frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 111;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_assign_source_63 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assign_source_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_13;
        }
        {
            PyObject *old = var_tight_bbox_raw;
            var_tight_bbox_raw = tmp_assign_source_63;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_68;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_source_name_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_TransformedBbox );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TransformedBbox );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TransformedBbox" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 113;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_13;
        }

        tmp_called_name_7 = tmp_mvar_value_4;
        CHECK_OBJECT( var_tight_bbox_raw );
        tmp_args_element_name_9 = var_tight_bbox_raw;
        CHECK_OBJECT( par_fig );
        tmp_source_name_3 = par_fig;
        tmp_called_instance_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_transFigure );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_13;
        }
        frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 114;
        tmp_args_element_name_10 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_inverted );
        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_13;
        }
        frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 113;
        {
            PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_assign_source_68 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_assign_source_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_13;
        }
        {
            PyObject *old = var_tight_bbox;
            var_tight_bbox = tmp_assign_source_68;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_69;
        PyObject *tmp_iter_arg_12;
        PyObject *tmp_left_name_11;
        PyObject *tmp_right_name_11;
        CHECK_OBJECT( var_num1 );
        tmp_left_name_11 = var_num1;
        CHECK_OBJECT( PyCell_GET( var_cols ) );
        tmp_right_name_11 = PyCell_GET( var_cols );
        tmp_iter_arg_12 = BUILTIN_DIVMOD( tmp_left_name_11, tmp_right_name_11 );
        if ( tmp_iter_arg_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_21;
        }
        tmp_assign_source_69 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_12 );
        Py_DECREF( tmp_iter_arg_12 );
        if ( tmp_assign_source_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_21;
        }
        {
            PyObject *old = tmp_tuple_unpack_5__source_iter;
            tmp_tuple_unpack_5__source_iter = tmp_assign_source_69;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_70;
        PyObject *tmp_unpack_12;
        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
        tmp_unpack_12 = tmp_tuple_unpack_5__source_iter;
        tmp_assign_source_70 = UNPACK_NEXT( tmp_unpack_12, 0, 2 );
        if ( tmp_assign_source_70 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 116;
            goto try_except_handler_22;
        }
        {
            PyObject *old = tmp_tuple_unpack_5__element_1;
            tmp_tuple_unpack_5__element_1 = tmp_assign_source_70;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_71;
        PyObject *tmp_unpack_13;
        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
        tmp_unpack_13 = tmp_tuple_unpack_5__source_iter;
        tmp_assign_source_71 = UNPACK_NEXT( tmp_unpack_13, 1, 2 );
        if ( tmp_assign_source_71 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 116;
            goto try_except_handler_22;
        }
        {
            PyObject *old = tmp_tuple_unpack_5__element_2;
            tmp_tuple_unpack_5__element_2 = tmp_assign_source_71;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_5;
        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
        tmp_iterator_name_5 = tmp_tuple_unpack_5__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_5 ); assert( HAS_ITERNEXT( tmp_iterator_name_5 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_5 )->tp_iternext)( tmp_iterator_name_5 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    exception_lineno = 116;
                    goto try_except_handler_22;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 116;
            goto try_except_handler_22;
        }
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_22:;
    exception_keeper_type_18 = exception_type;
    exception_keeper_value_18 = exception_value;
    exception_keeper_tb_18 = exception_tb;
    exception_keeper_lineno_18 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
    Py_DECREF( tmp_tuple_unpack_5__source_iter );
    tmp_tuple_unpack_5__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_18;
    exception_value = exception_keeper_value_18;
    exception_tb = exception_keeper_tb_18;
    exception_lineno = exception_keeper_lineno_18;

    goto try_except_handler_21;
    // End of try:
    try_end_10:;
    goto try_end_11;
    // Exception handler code:
    try_except_handler_21:;
    exception_keeper_type_19 = exception_type;
    exception_keeper_value_19 = exception_value;
    exception_keeper_tb_19 = exception_tb;
    exception_keeper_lineno_19 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_5__element_1 );
    tmp_tuple_unpack_5__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_5__element_2 );
    tmp_tuple_unpack_5__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_19;
    exception_value = exception_keeper_value_19;
    exception_tb = exception_keeper_tb_19;
    exception_lineno = exception_keeper_lineno_19;

    goto try_except_handler_13;
    // End of try:
    try_end_11:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
    Py_DECREF( tmp_tuple_unpack_5__source_iter );
    tmp_tuple_unpack_5__source_iter = NULL;

    {
        PyObject *tmp_assign_source_72;
        CHECK_OBJECT( tmp_tuple_unpack_5__element_1 );
        tmp_assign_source_72 = tmp_tuple_unpack_5__element_1;
        {
            PyObject *old = var_row1;
            var_row1 = tmp_assign_source_72;
            Py_INCREF( var_row1 );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_5__element_1 );
    tmp_tuple_unpack_5__element_1 = NULL;

    {
        PyObject *tmp_assign_source_73;
        CHECK_OBJECT( tmp_tuple_unpack_5__element_2 );
        tmp_assign_source_73 = tmp_tuple_unpack_5__element_2;
        {
            PyObject *old = var_col1;
            var_col1 = tmp_assign_source_73;
            Py_INCREF( var_col1 );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_5__element_2 );
    tmp_tuple_unpack_5__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        CHECK_OBJECT( var_num2 );
        tmp_compexpr_left_7 = var_num2;
        tmp_compexpr_right_7 = Py_None;
        tmp_condition_result_10 = ( tmp_compexpr_left_7 == tmp_compexpr_right_7 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_called_name_8;
            PyObject *tmp_source_name_4;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_left_name_12;
            PyObject *tmp_left_name_13;
            PyObject *tmp_right_name_12;
            PyObject *tmp_left_name_14;
            PyObject *tmp_right_name_13;
            PyObject *tmp_right_name_14;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_called_name_9;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_12;
            PyObject *tmp_args_element_name_13;
            CHECK_OBJECT( PyCell_GET( var_hspaces ) );
            tmp_subscribed_name_2 = PyCell_GET( var_hspaces );
            CHECK_OBJECT( var_row1 );
            tmp_left_name_13 = var_row1;
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_left_name_14 = PyCell_GET( var_cols );
            tmp_right_name_13 = const_int_pos_1;
            tmp_right_name_12 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_14, tmp_right_name_13 );
            if ( tmp_right_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 120;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_left_name_12 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_13, tmp_right_name_12 );
            Py_DECREF( tmp_right_name_12 );
            if ( tmp_left_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 120;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            CHECK_OBJECT( var_col1 );
            tmp_right_name_14 = var_col1;
            tmp_subscript_name_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_12, tmp_right_name_14 );
            Py_DECREF( tmp_left_name_12 );
            if ( tmp_subscript_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 120;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_source_name_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscript_name_2 );
            if ( tmp_source_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 120;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_append );
            Py_DECREF( tmp_source_name_4 );
            if ( tmp_called_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 120;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_left );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_left );
            }

            if ( tmp_mvar_value_5 == NULL )
            {
                Py_DECREF( tmp_called_name_8 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_left" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 121;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }

            tmp_called_name_9 = tmp_mvar_value_5;
            CHECK_OBJECT( var_tight_bbox );
            tmp_args_element_name_12 = var_tight_bbox;
            CHECK_OBJECT( var_ax_bbox );
            tmp_args_element_name_13 = var_ax_bbox;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 121;
            {
                PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13 };
                tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, call_args );
            }

            if ( tmp_args_element_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_8 );

                exception_lineno = 121;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 120;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_11 );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 120;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        {
            PyObject *tmp_called_name_10;
            PyObject *tmp_source_name_5;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_left_name_15;
            PyObject *tmp_left_name_16;
            PyObject *tmp_right_name_15;
            PyObject *tmp_left_name_17;
            PyObject *tmp_right_name_16;
            PyObject *tmp_right_name_17;
            PyObject *tmp_left_name_18;
            PyObject *tmp_right_name_18;
            PyObject *tmp_call_result_5;
            PyObject *tmp_args_element_name_14;
            PyObject *tmp_called_name_11;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_15;
            PyObject *tmp_args_element_name_16;
            CHECK_OBJECT( PyCell_GET( var_hspaces ) );
            tmp_subscribed_name_3 = PyCell_GET( var_hspaces );
            CHECK_OBJECT( var_row1 );
            tmp_left_name_16 = var_row1;
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_left_name_17 = PyCell_GET( var_cols );
            tmp_right_name_16 = const_int_pos_1;
            tmp_right_name_15 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_17, tmp_right_name_16 );
            if ( tmp_right_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_left_name_15 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_16, tmp_right_name_15 );
            Py_DECREF( tmp_right_name_15 );
            if ( tmp_left_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            CHECK_OBJECT( var_col1 );
            tmp_left_name_18 = var_col1;
            tmp_right_name_18 = const_int_pos_1;
            tmp_right_name_17 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_18, tmp_right_name_18 );
            if ( tmp_right_name_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_15 );

                exception_lineno = 123;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_subscript_name_3 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_15, tmp_right_name_17 );
            Py_DECREF( tmp_left_name_15 );
            Py_DECREF( tmp_right_name_17 );
            if ( tmp_subscript_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_source_name_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            Py_DECREF( tmp_subscript_name_3 );
            if ( tmp_source_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_append );
            Py_DECREF( tmp_source_name_5 );
            if ( tmp_called_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_right );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_right );
            }

            if ( tmp_mvar_value_6 == NULL )
            {
                Py_DECREF( tmp_called_name_10 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_right" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 124;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }

            tmp_called_name_11 = tmp_mvar_value_6;
            CHECK_OBJECT( var_tight_bbox );
            tmp_args_element_name_15 = var_tight_bbox;
            CHECK_OBJECT( var_ax_bbox );
            tmp_args_element_name_16 = var_ax_bbox;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 124;
            {
                PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_16 };
                tmp_args_element_name_14 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
            }

            if ( tmp_args_element_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_10 );

                exception_lineno = 124;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 123;
            {
                PyObject *call_args[] = { tmp_args_element_name_14 };
                tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
            }

            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_args_element_name_14 );
            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        {
            PyObject *tmp_called_name_12;
            PyObject *tmp_source_name_6;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_subscript_name_4;
            PyObject *tmp_left_name_19;
            PyObject *tmp_left_name_20;
            PyObject *tmp_right_name_19;
            PyObject *tmp_right_name_20;
            PyObject *tmp_call_result_6;
            PyObject *tmp_args_element_name_17;
            PyObject *tmp_called_name_13;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_args_element_name_18;
            PyObject *tmp_args_element_name_19;
            CHECK_OBJECT( var_vspaces );
            tmp_subscribed_name_4 = var_vspaces;
            CHECK_OBJECT( var_row1 );
            tmp_left_name_20 = var_row1;
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_right_name_19 = PyCell_GET( var_cols );
            tmp_left_name_19 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_20, tmp_right_name_19 );
            if ( tmp_left_name_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 126;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            CHECK_OBJECT( var_col1 );
            tmp_right_name_20 = var_col1;
            tmp_subscript_name_4 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_19, tmp_right_name_20 );
            Py_DECREF( tmp_left_name_19 );
            if ( tmp_subscript_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 126;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_source_name_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
            Py_DECREF( tmp_subscript_name_4 );
            if ( tmp_source_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 126;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_called_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_append );
            Py_DECREF( tmp_source_name_6 );
            if ( tmp_called_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 126;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_top );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_top );
            }

            if ( tmp_mvar_value_7 == NULL )
            {
                Py_DECREF( tmp_called_name_12 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_top" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 127;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }

            tmp_called_name_13 = tmp_mvar_value_7;
            CHECK_OBJECT( var_tight_bbox );
            tmp_args_element_name_18 = var_tight_bbox;
            CHECK_OBJECT( var_ax_bbox );
            tmp_args_element_name_19 = var_ax_bbox;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 127;
            {
                PyObject *call_args[] = { tmp_args_element_name_18, tmp_args_element_name_19 };
                tmp_args_element_name_17 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_13, call_args );
            }

            if ( tmp_args_element_name_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_12 );

                exception_lineno = 127;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 126;
            {
                PyObject *call_args[] = { tmp_args_element_name_17 };
                tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
            }

            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_args_element_name_17 );
            if ( tmp_call_result_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 126;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            Py_DECREF( tmp_call_result_6 );
        }
        {
            PyObject *tmp_called_name_14;
            PyObject *tmp_source_name_7;
            PyObject *tmp_subscribed_name_5;
            PyObject *tmp_subscript_name_5;
            PyObject *tmp_left_name_21;
            PyObject *tmp_left_name_22;
            PyObject *tmp_left_name_23;
            PyObject *tmp_right_name_21;
            PyObject *tmp_right_name_22;
            PyObject *tmp_right_name_23;
            PyObject *tmp_call_result_7;
            PyObject *tmp_args_element_name_20;
            PyObject *tmp_called_name_15;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_args_element_name_21;
            PyObject *tmp_args_element_name_22;
            CHECK_OBJECT( var_vspaces );
            tmp_subscribed_name_5 = var_vspaces;
            CHECK_OBJECT( var_row1 );
            tmp_left_name_23 = var_row1;
            tmp_right_name_21 = const_int_pos_1;
            tmp_left_name_22 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_23, tmp_right_name_21 );
            if ( tmp_left_name_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_right_name_22 = PyCell_GET( var_cols );
            tmp_left_name_21 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_22, tmp_right_name_22 );
            Py_DECREF( tmp_left_name_22 );
            if ( tmp_left_name_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            CHECK_OBJECT( var_col1 );
            tmp_right_name_23 = var_col1;
            tmp_subscript_name_5 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_21, tmp_right_name_23 );
            Py_DECREF( tmp_left_name_21 );
            if ( tmp_subscript_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_source_name_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
            Py_DECREF( tmp_subscript_name_5 );
            if ( tmp_source_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_called_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_append );
            Py_DECREF( tmp_source_name_7 );
            if ( tmp_called_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_bottom );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_bottom );
            }

            if ( tmp_mvar_value_8 == NULL )
            {
                Py_DECREF( tmp_called_name_14 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_bottom" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 130;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }

            tmp_called_name_15 = tmp_mvar_value_8;
            CHECK_OBJECT( var_tight_bbox );
            tmp_args_element_name_21 = var_tight_bbox;
            CHECK_OBJECT( var_ax_bbox );
            tmp_args_element_name_22 = var_ax_bbox;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 130;
            {
                PyObject *call_args[] = { tmp_args_element_name_21, tmp_args_element_name_22 };
                tmp_args_element_name_20 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_15, call_args );
            }

            if ( tmp_args_element_name_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_14 );

                exception_lineno = 130;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 129;
            {
                PyObject *call_args[] = { tmp_args_element_name_20 };
                tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
            }

            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_args_element_name_20 );
            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        goto branch_end_10;
        branch_no_10:;
        // Tried code:
        {
            PyObject *tmp_assign_source_74;
            PyObject *tmp_iter_arg_13;
            PyObject *tmp_left_name_24;
            PyObject *tmp_right_name_24;
            CHECK_OBJECT( var_num2 );
            tmp_left_name_24 = var_num2;
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_right_name_24 = PyCell_GET( var_cols );
            tmp_iter_arg_13 = BUILTIN_DIVMOD( tmp_left_name_24, tmp_right_name_24 );
            if ( tmp_iter_arg_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_23;
            }
            tmp_assign_source_74 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_13 );
            Py_DECREF( tmp_iter_arg_13 );
            if ( tmp_assign_source_74 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_23;
            }
            {
                PyObject *old = tmp_tuple_unpack_6__source_iter;
                tmp_tuple_unpack_6__source_iter = tmp_assign_source_74;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_75;
            PyObject *tmp_unpack_14;
            CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
            tmp_unpack_14 = tmp_tuple_unpack_6__source_iter;
            tmp_assign_source_75 = UNPACK_NEXT( tmp_unpack_14, 0, 2 );
            if ( tmp_assign_source_75 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                exception_lineno = 133;
                goto try_except_handler_24;
            }
            {
                PyObject *old = tmp_tuple_unpack_6__element_1;
                tmp_tuple_unpack_6__element_1 = tmp_assign_source_75;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_76;
            PyObject *tmp_unpack_15;
            CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
            tmp_unpack_15 = tmp_tuple_unpack_6__source_iter;
            tmp_assign_source_76 = UNPACK_NEXT( tmp_unpack_15, 1, 2 );
            if ( tmp_assign_source_76 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                exception_lineno = 133;
                goto try_except_handler_24;
            }
            {
                PyObject *old = tmp_tuple_unpack_6__element_2;
                tmp_tuple_unpack_6__element_2 = tmp_assign_source_76;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_6;
            CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
            tmp_iterator_name_6 = tmp_tuple_unpack_6__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_6 ); assert( HAS_ITERNEXT( tmp_iterator_name_6 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_6 )->tp_iternext)( tmp_iterator_name_6 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                        exception_lineno = 133;
                        goto try_except_handler_24;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                exception_lineno = 133;
                goto try_except_handler_24;
            }
        }
        goto try_end_12;
        // Exception handler code:
        try_except_handler_24:;
        exception_keeper_type_20 = exception_type;
        exception_keeper_value_20 = exception_value;
        exception_keeper_tb_20 = exception_tb;
        exception_keeper_lineno_20 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_6__source_iter );
        Py_DECREF( tmp_tuple_unpack_6__source_iter );
        tmp_tuple_unpack_6__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_20;
        exception_value = exception_keeper_value_20;
        exception_tb = exception_keeper_tb_20;
        exception_lineno = exception_keeper_lineno_20;

        goto try_except_handler_23;
        // End of try:
        try_end_12:;
        goto try_end_13;
        // Exception handler code:
        try_except_handler_23:;
        exception_keeper_type_21 = exception_type;
        exception_keeper_value_21 = exception_value;
        exception_keeper_tb_21 = exception_tb;
        exception_keeper_lineno_21 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_6__element_1 );
        tmp_tuple_unpack_6__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_6__element_2 );
        tmp_tuple_unpack_6__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_21;
        exception_value = exception_keeper_value_21;
        exception_tb = exception_keeper_tb_21;
        exception_lineno = exception_keeper_lineno_21;

        goto try_except_handler_13;
        // End of try:
        try_end_13:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_6__source_iter );
        Py_DECREF( tmp_tuple_unpack_6__source_iter );
        tmp_tuple_unpack_6__source_iter = NULL;

        {
            PyObject *tmp_assign_source_77;
            CHECK_OBJECT( tmp_tuple_unpack_6__element_1 );
            tmp_assign_source_77 = tmp_tuple_unpack_6__element_1;
            {
                PyObject *old = var_row2;
                var_row2 = tmp_assign_source_77;
                Py_INCREF( var_row2 );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_6__element_1 );
        tmp_tuple_unpack_6__element_1 = NULL;

        {
            PyObject *tmp_assign_source_78;
            CHECK_OBJECT( tmp_tuple_unpack_6__element_2 );
            tmp_assign_source_78 = tmp_tuple_unpack_6__element_2;
            {
                PyObject *old = var_col2;
                var_col2 = tmp_assign_source_78;
                Py_INCREF( var_col2 );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_6__element_2 );
        tmp_tuple_unpack_6__element_2 = NULL;

        {
            PyObject *tmp_assign_source_79;
            PyObject *tmp_iter_arg_14;
            PyObject *tmp_xrange_low_3;
            PyObject *tmp_xrange_high_1;
            PyObject *tmp_left_name_25;
            PyObject *tmp_right_name_25;
            CHECK_OBJECT( var_row1 );
            tmp_xrange_low_3 = var_row1;
            CHECK_OBJECT( var_row2 );
            tmp_left_name_25 = var_row2;
            tmp_right_name_25 = const_int_pos_1;
            tmp_xrange_high_1 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_25, tmp_right_name_25 );
            if ( tmp_xrange_high_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_iter_arg_14 = BUILTIN_XRANGE2( tmp_xrange_low_3, tmp_xrange_high_1 );
            Py_DECREF( tmp_xrange_high_1 );
            if ( tmp_iter_arg_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_assign_source_79 = MAKE_ITERATOR( tmp_iter_arg_14 );
            Py_DECREF( tmp_iter_arg_14 );
            if ( tmp_assign_source_79 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            {
                PyObject *old = tmp_for_loop_3__for_iterator;
                tmp_for_loop_3__for_iterator = tmp_assign_source_79;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_7:;
        {
            PyObject *tmp_next_source_7;
            PyObject *tmp_assign_source_80;
            CHECK_OBJECT( tmp_for_loop_3__for_iterator );
            tmp_next_source_7 = tmp_for_loop_3__for_iterator;
            tmp_assign_source_80 = ITERATOR_NEXT( tmp_next_source_7 );
            if ( tmp_assign_source_80 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_7;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    exception_lineno = 135;
                    goto try_except_handler_25;
                }
            }

            {
                PyObject *old = tmp_for_loop_3__iter_value;
                tmp_for_loop_3__iter_value = tmp_assign_source_80;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_81;
            CHECK_OBJECT( tmp_for_loop_3__iter_value );
            tmp_assign_source_81 = tmp_for_loop_3__iter_value;
            {
                PyObject *old = var_row_i;
                var_row_i = tmp_assign_source_81;
                Py_INCREF( var_row_i );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_16;
            PyObject *tmp_source_name_8;
            PyObject *tmp_subscribed_name_6;
            PyObject *tmp_subscript_name_6;
            PyObject *tmp_left_name_26;
            PyObject *tmp_left_name_27;
            PyObject *tmp_right_name_26;
            PyObject *tmp_left_name_28;
            PyObject *tmp_right_name_27;
            PyObject *tmp_right_name_28;
            PyObject *tmp_call_result_8;
            PyObject *tmp_args_element_name_23;
            PyObject *tmp_called_name_17;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_args_element_name_24;
            PyObject *tmp_args_element_name_25;
            CHECK_OBJECT( PyCell_GET( var_hspaces ) );
            tmp_subscribed_name_6 = PyCell_GET( var_hspaces );
            CHECK_OBJECT( var_row_i );
            tmp_left_name_27 = var_row_i;
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_left_name_28 = PyCell_GET( var_cols );
            tmp_right_name_27 = const_int_pos_1;
            tmp_right_name_26 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_28, tmp_right_name_27 );
            if ( tmp_right_name_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            tmp_left_name_26 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_27, tmp_right_name_26 );
            Py_DECREF( tmp_right_name_26 );
            if ( tmp_left_name_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            CHECK_OBJECT( var_col1 );
            tmp_right_name_28 = var_col1;
            tmp_subscript_name_6 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_26, tmp_right_name_28 );
            Py_DECREF( tmp_left_name_26 );
            if ( tmp_subscript_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            tmp_source_name_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
            Py_DECREF( tmp_subscript_name_6 );
            if ( tmp_source_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_append );
            Py_DECREF( tmp_source_name_8 );
            if ( tmp_called_name_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_left );

            if (unlikely( tmp_mvar_value_9 == NULL ))
            {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_left );
            }

            if ( tmp_mvar_value_9 == NULL )
            {
                Py_DECREF( tmp_called_name_16 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_left" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 138;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }

            tmp_called_name_17 = tmp_mvar_value_9;
            CHECK_OBJECT( var_tight_bbox );
            tmp_args_element_name_24 = var_tight_bbox;
            CHECK_OBJECT( var_ax_bbox );
            tmp_args_element_name_25 = var_ax_bbox;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 138;
            {
                PyObject *call_args[] = { tmp_args_element_name_24, tmp_args_element_name_25 };
                tmp_args_element_name_23 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_17, call_args );
            }

            if ( tmp_args_element_name_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_16 );

                exception_lineno = 138;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 137;
            {
                PyObject *call_args[] = { tmp_args_element_name_23 };
                tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
            }

            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_args_element_name_23 );
            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        {
            PyObject *tmp_called_name_18;
            PyObject *tmp_source_name_9;
            PyObject *tmp_subscribed_name_7;
            PyObject *tmp_subscript_name_7;
            PyObject *tmp_left_name_29;
            PyObject *tmp_left_name_30;
            PyObject *tmp_right_name_29;
            PyObject *tmp_left_name_31;
            PyObject *tmp_right_name_30;
            PyObject *tmp_right_name_31;
            PyObject *tmp_left_name_32;
            PyObject *tmp_right_name_32;
            PyObject *tmp_call_result_9;
            PyObject *tmp_args_element_name_26;
            PyObject *tmp_called_name_19;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_args_element_name_27;
            PyObject *tmp_args_element_name_28;
            CHECK_OBJECT( PyCell_GET( var_hspaces ) );
            tmp_subscribed_name_7 = PyCell_GET( var_hspaces );
            CHECK_OBJECT( var_row_i );
            tmp_left_name_30 = var_row_i;
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_left_name_31 = PyCell_GET( var_cols );
            tmp_right_name_30 = const_int_pos_1;
            tmp_right_name_29 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_31, tmp_right_name_30 );
            if ( tmp_right_name_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            tmp_left_name_29 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_30, tmp_right_name_29 );
            Py_DECREF( tmp_right_name_29 );
            if ( tmp_left_name_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            CHECK_OBJECT( var_col2 );
            tmp_left_name_32 = var_col2;
            tmp_right_name_32 = const_int_pos_1;
            tmp_right_name_31 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_32, tmp_right_name_32 );
            if ( tmp_right_name_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_29 );

                exception_lineno = 140;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            tmp_subscript_name_7 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_29, tmp_right_name_31 );
            Py_DECREF( tmp_left_name_29 );
            Py_DECREF( tmp_right_name_31 );
            if ( tmp_subscript_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            tmp_source_name_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
            Py_DECREF( tmp_subscript_name_7 );
            if ( tmp_source_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            tmp_called_name_18 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_append );
            Py_DECREF( tmp_source_name_9 );
            if ( tmp_called_name_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_right );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_right );
            }

            if ( tmp_mvar_value_10 == NULL )
            {
                Py_DECREF( tmp_called_name_18 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_right" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 141;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }

            tmp_called_name_19 = tmp_mvar_value_10;
            CHECK_OBJECT( var_tight_bbox );
            tmp_args_element_name_27 = var_tight_bbox;
            CHECK_OBJECT( var_ax_bbox );
            tmp_args_element_name_28 = var_ax_bbox;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 141;
            {
                PyObject *call_args[] = { tmp_args_element_name_27, tmp_args_element_name_28 };
                tmp_args_element_name_26 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_19, call_args );
            }

            if ( tmp_args_element_name_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_18 );

                exception_lineno = 141;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 140;
            {
                PyObject *call_args[] = { tmp_args_element_name_26 };
                tmp_call_result_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
            }

            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_args_element_name_26 );
            if ( tmp_call_result_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_25;
            }
            Py_DECREF( tmp_call_result_9 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_25;
        }
        goto loop_start_7;
        loop_end_7:;
        goto try_end_14;
        // Exception handler code:
        try_except_handler_25:;
        exception_keeper_type_22 = exception_type;
        exception_keeper_value_22 = exception_value;
        exception_keeper_tb_22 = exception_tb;
        exception_keeper_lineno_22 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_3__iter_value );
        tmp_for_loop_3__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
        Py_DECREF( tmp_for_loop_3__for_iterator );
        tmp_for_loop_3__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_22;
        exception_value = exception_keeper_value_22;
        exception_tb = exception_keeper_tb_22;
        exception_lineno = exception_keeper_lineno_22;

        goto try_except_handler_13;
        // End of try:
        try_end_14:;
        Py_XDECREF( tmp_for_loop_3__iter_value );
        tmp_for_loop_3__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
        Py_DECREF( tmp_for_loop_3__for_iterator );
        tmp_for_loop_3__for_iterator = NULL;

        {
            PyObject *tmp_assign_source_82;
            PyObject *tmp_iter_arg_15;
            PyObject *tmp_xrange_low_4;
            PyObject *tmp_xrange_high_2;
            PyObject *tmp_left_name_33;
            PyObject *tmp_right_name_33;
            CHECK_OBJECT( var_col1 );
            tmp_xrange_low_4 = var_col1;
            CHECK_OBJECT( var_col2 );
            tmp_left_name_33 = var_col2;
            tmp_right_name_33 = const_int_pos_1;
            tmp_xrange_high_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_33, tmp_right_name_33 );
            if ( tmp_xrange_high_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_iter_arg_15 = BUILTIN_XRANGE2( tmp_xrange_low_4, tmp_xrange_high_2 );
            Py_DECREF( tmp_xrange_high_2 );
            if ( tmp_iter_arg_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            tmp_assign_source_82 = MAKE_ITERATOR( tmp_iter_arg_15 );
            Py_DECREF( tmp_iter_arg_15 );
            if ( tmp_assign_source_82 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_13;
            }
            {
                PyObject *old = tmp_for_loop_4__for_iterator;
                tmp_for_loop_4__for_iterator = tmp_assign_source_82;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_8:;
        {
            PyObject *tmp_next_source_8;
            PyObject *tmp_assign_source_83;
            CHECK_OBJECT( tmp_for_loop_4__for_iterator );
            tmp_next_source_8 = tmp_for_loop_4__for_iterator;
            tmp_assign_source_83 = ITERATOR_NEXT( tmp_next_source_8 );
            if ( tmp_assign_source_83 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_8;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    exception_lineno = 142;
                    goto try_except_handler_26;
                }
            }

            {
                PyObject *old = tmp_for_loop_4__iter_value;
                tmp_for_loop_4__iter_value = tmp_assign_source_83;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_84;
            CHECK_OBJECT( tmp_for_loop_4__iter_value );
            tmp_assign_source_84 = tmp_for_loop_4__iter_value;
            {
                PyObject *old = var_col_i;
                var_col_i = tmp_assign_source_84;
                Py_INCREF( var_col_i );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_20;
            PyObject *tmp_source_name_10;
            PyObject *tmp_subscribed_name_8;
            PyObject *tmp_subscript_name_8;
            PyObject *tmp_left_name_34;
            PyObject *tmp_left_name_35;
            PyObject *tmp_right_name_34;
            PyObject *tmp_right_name_35;
            PyObject *tmp_call_result_10;
            PyObject *tmp_args_element_name_29;
            PyObject *tmp_called_name_21;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_args_element_name_30;
            PyObject *tmp_args_element_name_31;
            CHECK_OBJECT( var_vspaces );
            tmp_subscribed_name_8 = var_vspaces;
            CHECK_OBJECT( var_row1 );
            tmp_left_name_35 = var_row1;
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_right_name_34 = PyCell_GET( var_cols );
            tmp_left_name_34 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_35, tmp_right_name_34 );
            if ( tmp_left_name_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            CHECK_OBJECT( var_col_i );
            tmp_right_name_35 = var_col_i;
            tmp_subscript_name_8 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_34, tmp_right_name_35 );
            Py_DECREF( tmp_left_name_34 );
            if ( tmp_subscript_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            tmp_source_name_10 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_8 );
            Py_DECREF( tmp_subscript_name_8 );
            if ( tmp_source_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            tmp_called_name_20 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_append );
            Py_DECREF( tmp_source_name_10 );
            if ( tmp_called_name_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_top );

            if (unlikely( tmp_mvar_value_11 == NULL ))
            {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_top );
            }

            if ( tmp_mvar_value_11 == NULL )
            {
                Py_DECREF( tmp_called_name_20 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_top" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 145;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }

            tmp_called_name_21 = tmp_mvar_value_11;
            CHECK_OBJECT( var_tight_bbox );
            tmp_args_element_name_30 = var_tight_bbox;
            CHECK_OBJECT( var_ax_bbox );
            tmp_args_element_name_31 = var_ax_bbox;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 145;
            {
                PyObject *call_args[] = { tmp_args_element_name_30, tmp_args_element_name_31 };
                tmp_args_element_name_29 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_21, call_args );
            }

            if ( tmp_args_element_name_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_20 );

                exception_lineno = 145;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 144;
            {
                PyObject *call_args[] = { tmp_args_element_name_29 };
                tmp_call_result_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
            }

            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_args_element_name_29 );
            if ( tmp_call_result_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            Py_DECREF( tmp_call_result_10 );
        }
        {
            PyObject *tmp_called_name_22;
            PyObject *tmp_source_name_11;
            PyObject *tmp_subscribed_name_9;
            PyObject *tmp_subscript_name_9;
            PyObject *tmp_left_name_36;
            PyObject *tmp_left_name_37;
            PyObject *tmp_left_name_38;
            PyObject *tmp_right_name_36;
            PyObject *tmp_right_name_37;
            PyObject *tmp_right_name_38;
            PyObject *tmp_call_result_11;
            PyObject *tmp_args_element_name_32;
            PyObject *tmp_called_name_23;
            PyObject *tmp_mvar_value_12;
            PyObject *tmp_args_element_name_33;
            PyObject *tmp_args_element_name_34;
            CHECK_OBJECT( var_vspaces );
            tmp_subscribed_name_9 = var_vspaces;
            CHECK_OBJECT( var_row2 );
            tmp_left_name_38 = var_row2;
            tmp_right_name_36 = const_int_pos_1;
            tmp_left_name_37 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_38, tmp_right_name_36 );
            if ( tmp_left_name_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_right_name_37 = PyCell_GET( var_cols );
            tmp_left_name_36 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_37, tmp_right_name_37 );
            Py_DECREF( tmp_left_name_37 );
            if ( tmp_left_name_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            CHECK_OBJECT( var_col_i );
            tmp_right_name_38 = var_col_i;
            tmp_subscript_name_9 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_36, tmp_right_name_38 );
            Py_DECREF( tmp_left_name_36 );
            if ( tmp_subscript_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            tmp_source_name_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_9, tmp_subscript_name_9 );
            Py_DECREF( tmp_subscript_name_9 );
            if ( tmp_source_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            tmp_called_name_22 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_append );
            Py_DECREF( tmp_source_name_11 );
            if ( tmp_called_name_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_bottom );

            if (unlikely( tmp_mvar_value_12 == NULL ))
            {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_bottom );
            }

            if ( tmp_mvar_value_12 == NULL )
            {
                Py_DECREF( tmp_called_name_22 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_bottom" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 148;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }

            tmp_called_name_23 = tmp_mvar_value_12;
            CHECK_OBJECT( var_tight_bbox );
            tmp_args_element_name_33 = var_tight_bbox;
            CHECK_OBJECT( var_ax_bbox );
            tmp_args_element_name_34 = var_ax_bbox;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 148;
            {
                PyObject *call_args[] = { tmp_args_element_name_33, tmp_args_element_name_34 };
                tmp_args_element_name_32 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_23, call_args );
            }

            if ( tmp_args_element_name_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_22 );

                exception_lineno = 148;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 147;
            {
                PyObject *call_args[] = { tmp_args_element_name_32 };
                tmp_call_result_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
            }

            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_element_name_32 );
            if ( tmp_call_result_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto try_except_handler_26;
            }
            Py_DECREF( tmp_call_result_11 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_26;
        }
        goto loop_start_8;
        loop_end_8:;
        goto try_end_15;
        // Exception handler code:
        try_except_handler_26:;
        exception_keeper_type_23 = exception_type;
        exception_keeper_value_23 = exception_value;
        exception_keeper_tb_23 = exception_tb;
        exception_keeper_lineno_23 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_4__iter_value );
        tmp_for_loop_4__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
        Py_DECREF( tmp_for_loop_4__for_iterator );
        tmp_for_loop_4__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_23;
        exception_value = exception_keeper_value_23;
        exception_tb = exception_keeper_tb_23;
        exception_lineno = exception_keeper_lineno_23;

        goto try_except_handler_13;
        // End of try:
        try_end_15:;
        Py_XDECREF( tmp_for_loop_4__iter_value );
        tmp_for_loop_4__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
        Py_DECREF( tmp_for_loop_4__for_iterator );
        tmp_for_loop_4__for_iterator = NULL;

        branch_end_10:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;
        type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
        goto try_except_handler_13;
    }
    goto loop_start_5;
    loop_end_5:;
    goto try_end_16;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_24 = exception_type;
    exception_keeper_value_24 = exception_value;
    exception_keeper_tb_24 = exception_tb;
    exception_keeper_lineno_24 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_24;
    exception_value = exception_keeper_value_24;
    exception_tb = exception_keeper_tb_24;
    exception_lineno = exception_keeper_lineno_24;

    goto frame_exception_exit_1;
    // End of try:
    try_end_16:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_85;
        PyObject *tmp_iter_arg_16;
        PyObject *tmp_called_instance_6;
        CHECK_OBJECT( par_fig );
        tmp_called_instance_6 = par_fig;
        frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 150;
        tmp_iter_arg_16 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_get_size_inches );
        if ( tmp_iter_arg_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_27;
        }
        tmp_assign_source_85 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_16 );
        Py_DECREF( tmp_iter_arg_16 );
        if ( tmp_assign_source_85 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_27;
        }
        assert( tmp_tuple_unpack_7__source_iter == NULL );
        tmp_tuple_unpack_7__source_iter = tmp_assign_source_85;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_86;
        PyObject *tmp_unpack_16;
        CHECK_OBJECT( tmp_tuple_unpack_7__source_iter );
        tmp_unpack_16 = tmp_tuple_unpack_7__source_iter;
        tmp_assign_source_86 = UNPACK_NEXT( tmp_unpack_16, 0, 2 );
        if ( tmp_assign_source_86 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 150;
            goto try_except_handler_28;
        }
        assert( tmp_tuple_unpack_7__element_1 == NULL );
        tmp_tuple_unpack_7__element_1 = tmp_assign_source_86;
    }
    {
        PyObject *tmp_assign_source_87;
        PyObject *tmp_unpack_17;
        CHECK_OBJECT( tmp_tuple_unpack_7__source_iter );
        tmp_unpack_17 = tmp_tuple_unpack_7__source_iter;
        tmp_assign_source_87 = UNPACK_NEXT( tmp_unpack_17, 1, 2 );
        if ( tmp_assign_source_87 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 150;
            goto try_except_handler_28;
        }
        assert( tmp_tuple_unpack_7__element_2 == NULL );
        tmp_tuple_unpack_7__element_2 = tmp_assign_source_87;
    }
    {
        PyObject *tmp_iterator_name_7;
        CHECK_OBJECT( tmp_tuple_unpack_7__source_iter );
        tmp_iterator_name_7 = tmp_tuple_unpack_7__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_7 ); assert( HAS_ITERNEXT( tmp_iterator_name_7 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_7 )->tp_iternext)( tmp_iterator_name_7 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    exception_lineno = 150;
                    goto try_except_handler_28;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            exception_lineno = 150;
            goto try_except_handler_28;
        }
    }
    goto try_end_17;
    // Exception handler code:
    try_except_handler_28:;
    exception_keeper_type_25 = exception_type;
    exception_keeper_value_25 = exception_value;
    exception_keeper_tb_25 = exception_tb;
    exception_keeper_lineno_25 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_7__source_iter );
    Py_DECREF( tmp_tuple_unpack_7__source_iter );
    tmp_tuple_unpack_7__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_25;
    exception_value = exception_keeper_value_25;
    exception_tb = exception_keeper_tb_25;
    exception_lineno = exception_keeper_lineno_25;

    goto try_except_handler_27;
    // End of try:
    try_end_17:;
    goto try_end_18;
    // Exception handler code:
    try_except_handler_27:;
    exception_keeper_type_26 = exception_type;
    exception_keeper_value_26 = exception_value;
    exception_keeper_tb_26 = exception_tb;
    exception_keeper_lineno_26 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_7__element_1 );
    tmp_tuple_unpack_7__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_7__element_2 );
    tmp_tuple_unpack_7__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_26;
    exception_value = exception_keeper_value_26;
    exception_tb = exception_keeper_tb_26;
    exception_lineno = exception_keeper_lineno_26;

    goto frame_exception_exit_1;
    // End of try:
    try_end_18:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_7__source_iter );
    Py_DECREF( tmp_tuple_unpack_7__source_iter );
    tmp_tuple_unpack_7__source_iter = NULL;

    {
        PyObject *tmp_assign_source_88;
        CHECK_OBJECT( tmp_tuple_unpack_7__element_1 );
        tmp_assign_source_88 = tmp_tuple_unpack_7__element_1;
        assert( var_fig_width_inch == NULL );
        Py_INCREF( tmp_assign_source_88 );
        var_fig_width_inch = tmp_assign_source_88;
    }
    Py_XDECREF( tmp_tuple_unpack_7__element_1 );
    tmp_tuple_unpack_7__element_1 = NULL;

    {
        PyObject *tmp_assign_source_89;
        CHECK_OBJECT( tmp_tuple_unpack_7__element_2 );
        tmp_assign_source_89 = tmp_tuple_unpack_7__element_2;
        assert( var_fig_height_inch == NULL );
        Py_INCREF( tmp_assign_source_89 );
        var_fig_height_inch = tmp_assign_source_89;
    }
    Py_XDECREF( tmp_tuple_unpack_7__element_2 );
    tmp_tuple_unpack_7__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( var_margin_left );
        tmp_operand_name_1 = var_margin_left;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_assign_source_90;
            PyObject *tmp_called_name_24;
            PyObject *tmp_args_element_name_35;
            PyObject *tmp_left_name_39;
            PyObject *tmp_right_name_40;
            tmp_called_name_24 = LOOKUP_BUILTIN( const_str_plain_max );
            assert( tmp_called_name_24 != NULL );
            // Tried code:
            {
                PyObject *tmp_assign_source_91;
                PyObject *tmp_iter_arg_17;
                PyObject *tmp_subscribed_name_10;
                PyObject *tmp_subscript_name_10;
                PyObject *tmp_start_name_1;
                PyObject *tmp_stop_name_1;
                PyObject *tmp_step_name_1;
                PyObject *tmp_left_name_40;
                PyObject *tmp_right_name_39;
                CHECK_OBJECT( PyCell_GET( var_hspaces ) );
                tmp_subscribed_name_10 = PyCell_GET( var_hspaces );
                tmp_start_name_1 = Py_None;
                tmp_stop_name_1 = Py_None;
                CHECK_OBJECT( PyCell_GET( var_cols ) );
                tmp_left_name_40 = PyCell_GET( var_cols );
                tmp_right_name_39 = const_int_pos_1;
                tmp_step_name_1 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_40, tmp_right_name_39 );
                if ( tmp_step_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 156;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_29;
                }
                tmp_subscript_name_10 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
                Py_DECREF( tmp_step_name_1 );
                assert( !(tmp_subscript_name_10 == NULL) );
                tmp_iter_arg_17 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_10, tmp_subscript_name_10 );
                Py_DECREF( tmp_subscript_name_10 );
                if ( tmp_iter_arg_17 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 156;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_29;
                }
                tmp_assign_source_91 = MAKE_ITERATOR( tmp_iter_arg_17 );
                Py_DECREF( tmp_iter_arg_17 );
                if ( tmp_assign_source_91 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 156;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_29;
                }
                assert( tmp_listcomp_5__$0 == NULL );
                tmp_listcomp_5__$0 = tmp_assign_source_91;
            }
            {
                PyObject *tmp_assign_source_92;
                tmp_assign_source_92 = PyList_New( 0 );
                assert( tmp_listcomp_5__contraction == NULL );
                tmp_listcomp_5__contraction = tmp_assign_source_92;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_b02ee1be6b09833b65bcb24d7211f57b_6, codeobj_b02ee1be6b09833b65bcb24d7211f57b, module_matplotlib$tight_layout, sizeof(void *) );
            frame_b02ee1be6b09833b65bcb24d7211f57b_6 = cache_frame_b02ee1be6b09833b65bcb24d7211f57b_6;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_b02ee1be6b09833b65bcb24d7211f57b_6 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_b02ee1be6b09833b65bcb24d7211f57b_6 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_9:;
            {
                PyObject *tmp_next_source_9;
                PyObject *tmp_assign_source_93;
                CHECK_OBJECT( tmp_listcomp_5__$0 );
                tmp_next_source_9 = tmp_listcomp_5__$0;
                tmp_assign_source_93 = ITERATOR_NEXT( tmp_next_source_9 );
                if ( tmp_assign_source_93 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_9;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "o";
                        exception_lineno = 156;
                        goto try_except_handler_30;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_5__iter_value_0;
                    tmp_listcomp_5__iter_value_0 = tmp_assign_source_93;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_94;
                CHECK_OBJECT( tmp_listcomp_5__iter_value_0 );
                tmp_assign_source_94 = tmp_listcomp_5__iter_value_0;
                {
                    PyObject *old = outline_4_var_s;
                    outline_4_var_s = tmp_assign_source_94;
                    Py_INCREF( outline_4_var_s );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_5;
                PyObject *tmp_append_value_5;
                PyObject *tmp_sum_sequence_1;
                CHECK_OBJECT( tmp_listcomp_5__contraction );
                tmp_append_list_5 = tmp_listcomp_5__contraction;
                CHECK_OBJECT( outline_4_var_s );
                tmp_sum_sequence_1 = outline_4_var_s;
                tmp_append_value_5 = BUILTIN_SUM1( tmp_sum_sequence_1 );
                if ( tmp_append_value_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 156;
                    type_description_2 = "o";
                    goto try_except_handler_30;
                }
                assert( PyList_Check( tmp_append_list_5 ) );
                tmp_res = PyList_Append( tmp_append_list_5, tmp_append_value_5 );
                Py_DECREF( tmp_append_value_5 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 156;
                    type_description_2 = "o";
                    goto try_except_handler_30;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_2 = "o";
                goto try_except_handler_30;
            }
            goto loop_start_9;
            loop_end_9:;
            CHECK_OBJECT( tmp_listcomp_5__contraction );
            tmp_left_name_39 = tmp_listcomp_5__contraction;
            Py_INCREF( tmp_left_name_39 );
            goto try_return_handler_30;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_30:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_5__$0 );
            Py_DECREF( tmp_listcomp_5__$0 );
            tmp_listcomp_5__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_5__contraction );
            Py_DECREF( tmp_listcomp_5__contraction );
            tmp_listcomp_5__contraction = NULL;

            Py_XDECREF( tmp_listcomp_5__iter_value_0 );
            tmp_listcomp_5__iter_value_0 = NULL;

            goto frame_return_exit_6;
            // Exception handler code:
            try_except_handler_30:;
            exception_keeper_type_27 = exception_type;
            exception_keeper_value_27 = exception_value;
            exception_keeper_tb_27 = exception_tb;
            exception_keeper_lineno_27 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_5__$0 );
            Py_DECREF( tmp_listcomp_5__$0 );
            tmp_listcomp_5__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_5__contraction );
            Py_DECREF( tmp_listcomp_5__contraction );
            tmp_listcomp_5__contraction = NULL;

            Py_XDECREF( tmp_listcomp_5__iter_value_0 );
            tmp_listcomp_5__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_27;
            exception_value = exception_keeper_value_27;
            exception_tb = exception_keeper_tb_27;
            exception_lineno = exception_keeper_lineno_27;

            goto frame_exception_exit_6;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_b02ee1be6b09833b65bcb24d7211f57b_6 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_5;

            frame_return_exit_6:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_b02ee1be6b09833b65bcb24d7211f57b_6 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_29;

            frame_exception_exit_6:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_b02ee1be6b09833b65bcb24d7211f57b_6 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_b02ee1be6b09833b65bcb24d7211f57b_6, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_b02ee1be6b09833b65bcb24d7211f57b_6->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_b02ee1be6b09833b65bcb24d7211f57b_6, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_b02ee1be6b09833b65bcb24d7211f57b_6,
                type_description_2,
                outline_4_var_s
            );


            // Release cached frame.
            if ( frame_b02ee1be6b09833b65bcb24d7211f57b_6 == cache_frame_b02ee1be6b09833b65bcb24d7211f57b_6 )
            {
                Py_DECREF( frame_b02ee1be6b09833b65bcb24d7211f57b_6 );
            }
            cache_frame_b02ee1be6b09833b65bcb24d7211f57b_6 = NULL;

            assertFrameObject( frame_b02ee1be6b09833b65bcb24d7211f57b_6 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_5;

            frame_no_exception_5:;
            goto skip_nested_handling_5;
            nested_frame_exit_5:;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_29;
            skip_nested_handling_5:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_29:;
            Py_XDECREF( outline_4_var_s );
            outline_4_var_s = NULL;

            goto outline_result_6;
            // Exception handler code:
            try_except_handler_29:;
            exception_keeper_type_28 = exception_type;
            exception_keeper_value_28 = exception_value;
            exception_keeper_tb_28 = exception_tb;
            exception_keeper_lineno_28 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_4_var_s );
            outline_4_var_s = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_28;
            exception_value = exception_keeper_value_28;
            exception_tb = exception_keeper_tb_28;
            exception_lineno = exception_keeper_lineno_28;

            goto outline_exception_5;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            outline_exception_5:;
            exception_lineno = 156;
            goto frame_exception_exit_1;
            outline_result_6:;
            tmp_right_name_40 = LIST_COPY( const_list_int_0_list );
            tmp_args_element_name_35 = BINARY_OPERATION_ADD_OBJECT_LIST( tmp_left_name_39, tmp_right_name_40 );
            Py_DECREF( tmp_left_name_39 );
            Py_DECREF( tmp_right_name_40 );
            if ( tmp_args_element_name_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 156;
            {
                PyObject *call_args[] = { tmp_args_element_name_35 };
                tmp_assign_source_90 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, call_args );
            }

            Py_DECREF( tmp_args_element_name_35 );
            if ( tmp_assign_source_90 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_margin_left;
                assert( old != NULL );
                var_margin_left = tmp_assign_source_90;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_95;
            PyObject *tmp_left_name_41;
            PyObject *tmp_right_name_41;
            PyObject *tmp_left_name_42;
            PyObject *tmp_right_name_42;
            CHECK_OBJECT( var_margin_left );
            tmp_left_name_41 = var_margin_left;
            CHECK_OBJECT( var_pad_inches );
            tmp_left_name_42 = var_pad_inches;
            CHECK_OBJECT( var_fig_width_inch );
            tmp_right_name_42 = var_fig_width_inch;
            tmp_right_name_41 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_42, tmp_right_name_42 );
            if ( tmp_right_name_41 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_41, tmp_right_name_41 );
            Py_DECREF( tmp_right_name_41 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_95 = tmp_left_name_41;
            var_margin_left = tmp_assign_source_95;

        }
        branch_no_11:;
    }
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_operand_name_2;
        if ( var_margin_right == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_right" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_2 = var_margin_right;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_12;
        }
        else
        {
            goto branch_no_12;
        }
        branch_yes_12:;
        {
            PyObject *tmp_assign_source_96;
            PyObject *tmp_called_name_25;
            PyObject *tmp_args_element_name_36;
            PyObject *tmp_left_name_43;
            PyObject *tmp_right_name_44;
            tmp_called_name_25 = LOOKUP_BUILTIN( const_str_plain_max );
            assert( tmp_called_name_25 != NULL );
            // Tried code:
            {
                PyObject *tmp_assign_source_97;
                PyObject *tmp_iter_arg_18;
                PyObject *tmp_subscribed_name_11;
                PyObject *tmp_subscript_name_11;
                PyObject *tmp_start_name_2;
                PyObject *tmp_stop_name_2;
                PyObject *tmp_step_name_2;
                PyObject *tmp_left_name_44;
                PyObject *tmp_right_name_43;
                CHECK_OBJECT( PyCell_GET( var_hspaces ) );
                tmp_subscribed_name_11 = PyCell_GET( var_hspaces );
                CHECK_OBJECT( PyCell_GET( var_cols ) );
                tmp_start_name_2 = PyCell_GET( var_cols );
                tmp_stop_name_2 = Py_None;
                CHECK_OBJECT( PyCell_GET( var_cols ) );
                tmp_left_name_44 = PyCell_GET( var_cols );
                tmp_right_name_43 = const_int_pos_1;
                tmp_step_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_44, tmp_right_name_43 );
                if ( tmp_step_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 160;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_31;
                }
                tmp_subscript_name_11 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
                Py_DECREF( tmp_step_name_2 );
                assert( !(tmp_subscript_name_11 == NULL) );
                tmp_iter_arg_18 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_11, tmp_subscript_name_11 );
                Py_DECREF( tmp_subscript_name_11 );
                if ( tmp_iter_arg_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 160;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_31;
                }
                tmp_assign_source_97 = MAKE_ITERATOR( tmp_iter_arg_18 );
                Py_DECREF( tmp_iter_arg_18 );
                if ( tmp_assign_source_97 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 160;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_31;
                }
                assert( tmp_listcomp_6__$0 == NULL );
                tmp_listcomp_6__$0 = tmp_assign_source_97;
            }
            {
                PyObject *tmp_assign_source_98;
                tmp_assign_source_98 = PyList_New( 0 );
                assert( tmp_listcomp_6__contraction == NULL );
                tmp_listcomp_6__contraction = tmp_assign_source_98;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_699e36f676ffc125d4d1ecef74569d68_7, codeobj_699e36f676ffc125d4d1ecef74569d68, module_matplotlib$tight_layout, sizeof(void *) );
            frame_699e36f676ffc125d4d1ecef74569d68_7 = cache_frame_699e36f676ffc125d4d1ecef74569d68_7;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_699e36f676ffc125d4d1ecef74569d68_7 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_699e36f676ffc125d4d1ecef74569d68_7 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_10:;
            {
                PyObject *tmp_next_source_10;
                PyObject *tmp_assign_source_99;
                CHECK_OBJECT( tmp_listcomp_6__$0 );
                tmp_next_source_10 = tmp_listcomp_6__$0;
                tmp_assign_source_99 = ITERATOR_NEXT( tmp_next_source_10 );
                if ( tmp_assign_source_99 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_10;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "o";
                        exception_lineno = 160;
                        goto try_except_handler_32;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_6__iter_value_0;
                    tmp_listcomp_6__iter_value_0 = tmp_assign_source_99;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_100;
                CHECK_OBJECT( tmp_listcomp_6__iter_value_0 );
                tmp_assign_source_100 = tmp_listcomp_6__iter_value_0;
                {
                    PyObject *old = outline_5_var_s;
                    outline_5_var_s = tmp_assign_source_100;
                    Py_INCREF( outline_5_var_s );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_6;
                PyObject *tmp_append_value_6;
                PyObject *tmp_sum_sequence_2;
                CHECK_OBJECT( tmp_listcomp_6__contraction );
                tmp_append_list_6 = tmp_listcomp_6__contraction;
                CHECK_OBJECT( outline_5_var_s );
                tmp_sum_sequence_2 = outline_5_var_s;
                tmp_append_value_6 = BUILTIN_SUM1( tmp_sum_sequence_2 );
                if ( tmp_append_value_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 160;
                    type_description_2 = "o";
                    goto try_except_handler_32;
                }
                assert( PyList_Check( tmp_append_list_6 ) );
                tmp_res = PyList_Append( tmp_append_list_6, tmp_append_value_6 );
                Py_DECREF( tmp_append_value_6 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 160;
                    type_description_2 = "o";
                    goto try_except_handler_32;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_2 = "o";
                goto try_except_handler_32;
            }
            goto loop_start_10;
            loop_end_10:;
            CHECK_OBJECT( tmp_listcomp_6__contraction );
            tmp_left_name_43 = tmp_listcomp_6__contraction;
            Py_INCREF( tmp_left_name_43 );
            goto try_return_handler_32;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_32:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_6__$0 );
            Py_DECREF( tmp_listcomp_6__$0 );
            tmp_listcomp_6__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_6__contraction );
            Py_DECREF( tmp_listcomp_6__contraction );
            tmp_listcomp_6__contraction = NULL;

            Py_XDECREF( tmp_listcomp_6__iter_value_0 );
            tmp_listcomp_6__iter_value_0 = NULL;

            goto frame_return_exit_7;
            // Exception handler code:
            try_except_handler_32:;
            exception_keeper_type_29 = exception_type;
            exception_keeper_value_29 = exception_value;
            exception_keeper_tb_29 = exception_tb;
            exception_keeper_lineno_29 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_6__$0 );
            Py_DECREF( tmp_listcomp_6__$0 );
            tmp_listcomp_6__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_6__contraction );
            Py_DECREF( tmp_listcomp_6__contraction );
            tmp_listcomp_6__contraction = NULL;

            Py_XDECREF( tmp_listcomp_6__iter_value_0 );
            tmp_listcomp_6__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_29;
            exception_value = exception_keeper_value_29;
            exception_tb = exception_keeper_tb_29;
            exception_lineno = exception_keeper_lineno_29;

            goto frame_exception_exit_7;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_699e36f676ffc125d4d1ecef74569d68_7 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_6;

            frame_return_exit_7:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_699e36f676ffc125d4d1ecef74569d68_7 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_31;

            frame_exception_exit_7:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_699e36f676ffc125d4d1ecef74569d68_7 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_699e36f676ffc125d4d1ecef74569d68_7, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_699e36f676ffc125d4d1ecef74569d68_7->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_699e36f676ffc125d4d1ecef74569d68_7, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_699e36f676ffc125d4d1ecef74569d68_7,
                type_description_2,
                outline_5_var_s
            );


            // Release cached frame.
            if ( frame_699e36f676ffc125d4d1ecef74569d68_7 == cache_frame_699e36f676ffc125d4d1ecef74569d68_7 )
            {
                Py_DECREF( frame_699e36f676ffc125d4d1ecef74569d68_7 );
            }
            cache_frame_699e36f676ffc125d4d1ecef74569d68_7 = NULL;

            assertFrameObject( frame_699e36f676ffc125d4d1ecef74569d68_7 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_6;

            frame_no_exception_6:;
            goto skip_nested_handling_6;
            nested_frame_exit_6:;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_31;
            skip_nested_handling_6:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_31:;
            Py_XDECREF( outline_5_var_s );
            outline_5_var_s = NULL;

            goto outline_result_7;
            // Exception handler code:
            try_except_handler_31:;
            exception_keeper_type_30 = exception_type;
            exception_keeper_value_30 = exception_value;
            exception_keeper_tb_30 = exception_tb;
            exception_keeper_lineno_30 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_5_var_s );
            outline_5_var_s = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_30;
            exception_value = exception_keeper_value_30;
            exception_tb = exception_keeper_tb_30;
            exception_lineno = exception_keeper_lineno_30;

            goto outline_exception_6;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            outline_exception_6:;
            exception_lineno = 160;
            goto frame_exception_exit_1;
            outline_result_7:;
            tmp_right_name_44 = LIST_COPY( const_list_int_0_list );
            tmp_args_element_name_36 = BINARY_OPERATION_ADD_OBJECT_LIST( tmp_left_name_43, tmp_right_name_44 );
            Py_DECREF( tmp_left_name_43 );
            Py_DECREF( tmp_right_name_44 );
            if ( tmp_args_element_name_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 160;
            {
                PyObject *call_args[] = { tmp_args_element_name_36 };
                tmp_assign_source_96 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, call_args );
            }

            Py_DECREF( tmp_args_element_name_36 );
            if ( tmp_assign_source_96 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_margin_right;
                var_margin_right = tmp_assign_source_96;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_101;
            PyObject *tmp_left_name_45;
            PyObject *tmp_right_name_45;
            PyObject *tmp_left_name_46;
            PyObject *tmp_right_name_46;
            CHECK_OBJECT( var_margin_right );
            tmp_left_name_45 = var_margin_right;
            CHECK_OBJECT( var_pad_inches );
            tmp_left_name_46 = var_pad_inches;
            CHECK_OBJECT( var_fig_width_inch );
            tmp_right_name_46 = var_fig_width_inch;
            tmp_right_name_45 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_46, tmp_right_name_46 );
            if ( tmp_right_name_45 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_45, tmp_right_name_45 );
            Py_DECREF( tmp_right_name_45 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_101 = tmp_left_name_45;
            var_margin_right = tmp_assign_source_101;

        }
        branch_no_12:;
    }
    {
        nuitka_bool tmp_condition_result_13;
        PyObject *tmp_operand_name_3;
        if ( var_margin_top == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_top" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 163;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_3 = var_margin_top;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_13 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        {
            PyObject *tmp_assign_source_102;
            PyObject *tmp_called_name_26;
            PyObject *tmp_args_element_name_37;
            PyObject *tmp_left_name_47;
            PyObject *tmp_right_name_47;
            tmp_called_name_26 = LOOKUP_BUILTIN( const_str_plain_max );
            assert( tmp_called_name_26 != NULL );
            // Tried code:
            {
                PyObject *tmp_assign_source_103;
                PyObject *tmp_iter_arg_19;
                PyObject *tmp_subscribed_name_12;
                PyObject *tmp_subscript_name_12;
                PyObject *tmp_start_name_3;
                PyObject *tmp_stop_name_3;
                PyObject *tmp_step_name_3;
                CHECK_OBJECT( var_vspaces );
                tmp_subscribed_name_12 = var_vspaces;
                tmp_start_name_3 = Py_None;
                CHECK_OBJECT( PyCell_GET( var_cols ) );
                tmp_stop_name_3 = PyCell_GET( var_cols );
                tmp_step_name_3 = Py_None;
                tmp_subscript_name_12 = MAKE_SLICEOBJ3( tmp_start_name_3, tmp_stop_name_3, tmp_step_name_3 );
                assert( !(tmp_subscript_name_12 == NULL) );
                tmp_iter_arg_19 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_12, tmp_subscript_name_12 );
                Py_DECREF( tmp_subscript_name_12 );
                if ( tmp_iter_arg_19 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_33;
                }
                tmp_assign_source_103 = MAKE_ITERATOR( tmp_iter_arg_19 );
                Py_DECREF( tmp_iter_arg_19 );
                if ( tmp_assign_source_103 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_33;
                }
                assert( tmp_listcomp_7__$0 == NULL );
                tmp_listcomp_7__$0 = tmp_assign_source_103;
            }
            {
                PyObject *tmp_assign_source_104;
                tmp_assign_source_104 = PyList_New( 0 );
                assert( tmp_listcomp_7__contraction == NULL );
                tmp_listcomp_7__contraction = tmp_assign_source_104;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_52e777314c8cb5bf5b2e5ade68e5649a_8, codeobj_52e777314c8cb5bf5b2e5ade68e5649a, module_matplotlib$tight_layout, sizeof(void *) );
            frame_52e777314c8cb5bf5b2e5ade68e5649a_8 = cache_frame_52e777314c8cb5bf5b2e5ade68e5649a_8;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_52e777314c8cb5bf5b2e5ade68e5649a_8 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_52e777314c8cb5bf5b2e5ade68e5649a_8 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_11:;
            {
                PyObject *tmp_next_source_11;
                PyObject *tmp_assign_source_105;
                CHECK_OBJECT( tmp_listcomp_7__$0 );
                tmp_next_source_11 = tmp_listcomp_7__$0;
                tmp_assign_source_105 = ITERATOR_NEXT( tmp_next_source_11 );
                if ( tmp_assign_source_105 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_11;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "o";
                        exception_lineno = 164;
                        goto try_except_handler_34;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_7__iter_value_0;
                    tmp_listcomp_7__iter_value_0 = tmp_assign_source_105;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_106;
                CHECK_OBJECT( tmp_listcomp_7__iter_value_0 );
                tmp_assign_source_106 = tmp_listcomp_7__iter_value_0;
                {
                    PyObject *old = outline_6_var_s;
                    outline_6_var_s = tmp_assign_source_106;
                    Py_INCREF( outline_6_var_s );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_7;
                PyObject *tmp_append_value_7;
                PyObject *tmp_sum_sequence_3;
                CHECK_OBJECT( tmp_listcomp_7__contraction );
                tmp_append_list_7 = tmp_listcomp_7__contraction;
                CHECK_OBJECT( outline_6_var_s );
                tmp_sum_sequence_3 = outline_6_var_s;
                tmp_append_value_7 = BUILTIN_SUM1( tmp_sum_sequence_3 );
                if ( tmp_append_value_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;
                    type_description_2 = "o";
                    goto try_except_handler_34;
                }
                assert( PyList_Check( tmp_append_list_7 ) );
                tmp_res = PyList_Append( tmp_append_list_7, tmp_append_value_7 );
                Py_DECREF( tmp_append_value_7 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;
                    type_description_2 = "o";
                    goto try_except_handler_34;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 164;
                type_description_2 = "o";
                goto try_except_handler_34;
            }
            goto loop_start_11;
            loop_end_11:;
            CHECK_OBJECT( tmp_listcomp_7__contraction );
            tmp_left_name_47 = tmp_listcomp_7__contraction;
            Py_INCREF( tmp_left_name_47 );
            goto try_return_handler_34;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_34:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_7__$0 );
            Py_DECREF( tmp_listcomp_7__$0 );
            tmp_listcomp_7__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_7__contraction );
            Py_DECREF( tmp_listcomp_7__contraction );
            tmp_listcomp_7__contraction = NULL;

            Py_XDECREF( tmp_listcomp_7__iter_value_0 );
            tmp_listcomp_7__iter_value_0 = NULL;

            goto frame_return_exit_8;
            // Exception handler code:
            try_except_handler_34:;
            exception_keeper_type_31 = exception_type;
            exception_keeper_value_31 = exception_value;
            exception_keeper_tb_31 = exception_tb;
            exception_keeper_lineno_31 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_7__$0 );
            Py_DECREF( tmp_listcomp_7__$0 );
            tmp_listcomp_7__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_7__contraction );
            Py_DECREF( tmp_listcomp_7__contraction );
            tmp_listcomp_7__contraction = NULL;

            Py_XDECREF( tmp_listcomp_7__iter_value_0 );
            tmp_listcomp_7__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_31;
            exception_value = exception_keeper_value_31;
            exception_tb = exception_keeper_tb_31;
            exception_lineno = exception_keeper_lineno_31;

            goto frame_exception_exit_8;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_52e777314c8cb5bf5b2e5ade68e5649a_8 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_7;

            frame_return_exit_8:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_52e777314c8cb5bf5b2e5ade68e5649a_8 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_33;

            frame_exception_exit_8:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_52e777314c8cb5bf5b2e5ade68e5649a_8 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_52e777314c8cb5bf5b2e5ade68e5649a_8, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_52e777314c8cb5bf5b2e5ade68e5649a_8->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_52e777314c8cb5bf5b2e5ade68e5649a_8, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_52e777314c8cb5bf5b2e5ade68e5649a_8,
                type_description_2,
                outline_6_var_s
            );


            // Release cached frame.
            if ( frame_52e777314c8cb5bf5b2e5ade68e5649a_8 == cache_frame_52e777314c8cb5bf5b2e5ade68e5649a_8 )
            {
                Py_DECREF( frame_52e777314c8cb5bf5b2e5ade68e5649a_8 );
            }
            cache_frame_52e777314c8cb5bf5b2e5ade68e5649a_8 = NULL;

            assertFrameObject( frame_52e777314c8cb5bf5b2e5ade68e5649a_8 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_7;

            frame_no_exception_7:;
            goto skip_nested_handling_7;
            nested_frame_exit_7:;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_33;
            skip_nested_handling_7:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_33:;
            Py_XDECREF( outline_6_var_s );
            outline_6_var_s = NULL;

            goto outline_result_8;
            // Exception handler code:
            try_except_handler_33:;
            exception_keeper_type_32 = exception_type;
            exception_keeper_value_32 = exception_value;
            exception_keeper_tb_32 = exception_tb;
            exception_keeper_lineno_32 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_6_var_s );
            outline_6_var_s = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_32;
            exception_value = exception_keeper_value_32;
            exception_tb = exception_keeper_tb_32;
            exception_lineno = exception_keeper_lineno_32;

            goto outline_exception_7;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            outline_exception_7:;
            exception_lineno = 164;
            goto frame_exception_exit_1;
            outline_result_8:;
            tmp_right_name_47 = LIST_COPY( const_list_int_0_list );
            tmp_args_element_name_37 = BINARY_OPERATION_ADD_OBJECT_LIST( tmp_left_name_47, tmp_right_name_47 );
            Py_DECREF( tmp_left_name_47 );
            Py_DECREF( tmp_right_name_47 );
            if ( tmp_args_element_name_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 164;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 164;
            {
                PyObject *call_args[] = { tmp_args_element_name_37 };
                tmp_assign_source_102 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, call_args );
            }

            Py_DECREF( tmp_args_element_name_37 );
            if ( tmp_assign_source_102 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 164;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_margin_top;
                var_margin_top = tmp_assign_source_102;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_107;
            PyObject *tmp_left_name_48;
            PyObject *tmp_right_name_48;
            PyObject *tmp_left_name_49;
            PyObject *tmp_right_name_49;
            CHECK_OBJECT( var_margin_top );
            tmp_left_name_48 = var_margin_top;
            CHECK_OBJECT( var_pad_inches );
            tmp_left_name_49 = var_pad_inches;
            CHECK_OBJECT( var_fig_height_inch );
            tmp_right_name_49 = var_fig_height_inch;
            tmp_right_name_48 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_49, tmp_right_name_49 );
            if ( tmp_right_name_48 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 165;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_48, tmp_right_name_48 );
            Py_DECREF( tmp_right_name_48 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 165;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_107 = tmp_left_name_48;
            var_margin_top = tmp_assign_source_107;

        }
        branch_no_13:;
    }
    {
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_operand_name_4;
        CHECK_OBJECT( var_margin_bottom );
        tmp_operand_name_4 = var_margin_bottom;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_14 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_assign_source_108;
            PyObject *tmp_called_name_27;
            PyObject *tmp_args_element_name_38;
            PyObject *tmp_left_name_50;
            PyObject *tmp_right_name_50;
            tmp_called_name_27 = LOOKUP_BUILTIN( const_str_plain_max );
            assert( tmp_called_name_27 != NULL );
            // Tried code:
            {
                PyObject *tmp_assign_source_109;
                PyObject *tmp_iter_arg_20;
                PyObject *tmp_subscribed_name_13;
                PyObject *tmp_subscript_name_13;
                PyObject *tmp_start_name_4;
                PyObject *tmp_operand_name_5;
                PyObject *tmp_stop_name_4;
                PyObject *tmp_step_name_4;
                CHECK_OBJECT( var_vspaces );
                tmp_subscribed_name_13 = var_vspaces;
                CHECK_OBJECT( PyCell_GET( var_cols ) );
                tmp_operand_name_5 = PyCell_GET( var_cols );
                tmp_start_name_4 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_5 );
                if ( tmp_start_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 168;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_35;
                }
                tmp_stop_name_4 = Py_None;
                tmp_step_name_4 = Py_None;
                tmp_subscript_name_13 = MAKE_SLICEOBJ3( tmp_start_name_4, tmp_stop_name_4, tmp_step_name_4 );
                Py_DECREF( tmp_start_name_4 );
                assert( !(tmp_subscript_name_13 == NULL) );
                tmp_iter_arg_20 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_13, tmp_subscript_name_13 );
                Py_DECREF( tmp_subscript_name_13 );
                if ( tmp_iter_arg_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 168;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_35;
                }
                tmp_assign_source_109 = MAKE_ITERATOR( tmp_iter_arg_20 );
                Py_DECREF( tmp_iter_arg_20 );
                if ( tmp_assign_source_109 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 168;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto try_except_handler_35;
                }
                assert( tmp_listcomp_8__$0 == NULL );
                tmp_listcomp_8__$0 = tmp_assign_source_109;
            }
            {
                PyObject *tmp_assign_source_110;
                tmp_assign_source_110 = PyList_New( 0 );
                assert( tmp_listcomp_8__contraction == NULL );
                tmp_listcomp_8__contraction = tmp_assign_source_110;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_a945046174c9cda7ebe853bb1339324e_9, codeobj_a945046174c9cda7ebe853bb1339324e, module_matplotlib$tight_layout, sizeof(void *) );
            frame_a945046174c9cda7ebe853bb1339324e_9 = cache_frame_a945046174c9cda7ebe853bb1339324e_9;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_a945046174c9cda7ebe853bb1339324e_9 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_a945046174c9cda7ebe853bb1339324e_9 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_12:;
            {
                PyObject *tmp_next_source_12;
                PyObject *tmp_assign_source_111;
                CHECK_OBJECT( tmp_listcomp_8__$0 );
                tmp_next_source_12 = tmp_listcomp_8__$0;
                tmp_assign_source_111 = ITERATOR_NEXT( tmp_next_source_12 );
                if ( tmp_assign_source_111 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_12;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "o";
                        exception_lineno = 168;
                        goto try_except_handler_36;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_8__iter_value_0;
                    tmp_listcomp_8__iter_value_0 = tmp_assign_source_111;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_112;
                CHECK_OBJECT( tmp_listcomp_8__iter_value_0 );
                tmp_assign_source_112 = tmp_listcomp_8__iter_value_0;
                {
                    PyObject *old = outline_7_var_s;
                    outline_7_var_s = tmp_assign_source_112;
                    Py_INCREF( outline_7_var_s );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_8;
                PyObject *tmp_append_value_8;
                PyObject *tmp_sum_sequence_4;
                CHECK_OBJECT( tmp_listcomp_8__contraction );
                tmp_append_list_8 = tmp_listcomp_8__contraction;
                CHECK_OBJECT( outline_7_var_s );
                tmp_sum_sequence_4 = outline_7_var_s;
                tmp_append_value_8 = BUILTIN_SUM1( tmp_sum_sequence_4 );
                if ( tmp_append_value_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 168;
                    type_description_2 = "o";
                    goto try_except_handler_36;
                }
                assert( PyList_Check( tmp_append_list_8 ) );
                tmp_res = PyList_Append( tmp_append_list_8, tmp_append_value_8 );
                Py_DECREF( tmp_append_value_8 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 168;
                    type_description_2 = "o";
                    goto try_except_handler_36;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_2 = "o";
                goto try_except_handler_36;
            }
            goto loop_start_12;
            loop_end_12:;
            CHECK_OBJECT( tmp_listcomp_8__contraction );
            tmp_left_name_50 = tmp_listcomp_8__contraction;
            Py_INCREF( tmp_left_name_50 );
            goto try_return_handler_36;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_36:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_8__$0 );
            Py_DECREF( tmp_listcomp_8__$0 );
            tmp_listcomp_8__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_8__contraction );
            Py_DECREF( tmp_listcomp_8__contraction );
            tmp_listcomp_8__contraction = NULL;

            Py_XDECREF( tmp_listcomp_8__iter_value_0 );
            tmp_listcomp_8__iter_value_0 = NULL;

            goto frame_return_exit_9;
            // Exception handler code:
            try_except_handler_36:;
            exception_keeper_type_33 = exception_type;
            exception_keeper_value_33 = exception_value;
            exception_keeper_tb_33 = exception_tb;
            exception_keeper_lineno_33 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_8__$0 );
            Py_DECREF( tmp_listcomp_8__$0 );
            tmp_listcomp_8__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_8__contraction );
            Py_DECREF( tmp_listcomp_8__contraction );
            tmp_listcomp_8__contraction = NULL;

            Py_XDECREF( tmp_listcomp_8__iter_value_0 );
            tmp_listcomp_8__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_33;
            exception_value = exception_keeper_value_33;
            exception_tb = exception_keeper_tb_33;
            exception_lineno = exception_keeper_lineno_33;

            goto frame_exception_exit_9;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_a945046174c9cda7ebe853bb1339324e_9 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_8;

            frame_return_exit_9:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_a945046174c9cda7ebe853bb1339324e_9 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_35;

            frame_exception_exit_9:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_a945046174c9cda7ebe853bb1339324e_9 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_a945046174c9cda7ebe853bb1339324e_9, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_a945046174c9cda7ebe853bb1339324e_9->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_a945046174c9cda7ebe853bb1339324e_9, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_a945046174c9cda7ebe853bb1339324e_9,
                type_description_2,
                outline_7_var_s
            );


            // Release cached frame.
            if ( frame_a945046174c9cda7ebe853bb1339324e_9 == cache_frame_a945046174c9cda7ebe853bb1339324e_9 )
            {
                Py_DECREF( frame_a945046174c9cda7ebe853bb1339324e_9 );
            }
            cache_frame_a945046174c9cda7ebe853bb1339324e_9 = NULL;

            assertFrameObject( frame_a945046174c9cda7ebe853bb1339324e_9 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_8;

            frame_no_exception_8:;
            goto skip_nested_handling_8;
            nested_frame_exit_8:;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto try_except_handler_35;
            skip_nested_handling_8:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_35:;
            Py_XDECREF( outline_7_var_s );
            outline_7_var_s = NULL;

            goto outline_result_9;
            // Exception handler code:
            try_except_handler_35:;
            exception_keeper_type_34 = exception_type;
            exception_keeper_value_34 = exception_value;
            exception_keeper_tb_34 = exception_tb;
            exception_keeper_lineno_34 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_7_var_s );
            outline_7_var_s = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_34;
            exception_value = exception_keeper_value_34;
            exception_tb = exception_keeper_tb_34;
            exception_lineno = exception_keeper_lineno_34;

            goto outline_exception_8;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            outline_exception_8:;
            exception_lineno = 168;
            goto frame_exception_exit_1;
            outline_result_9:;
            tmp_right_name_50 = LIST_COPY( const_list_int_0_list );
            tmp_args_element_name_38 = BINARY_OPERATION_ADD_OBJECT_LIST( tmp_left_name_50, tmp_right_name_50 );
            Py_DECREF( tmp_left_name_50 );
            Py_DECREF( tmp_right_name_50 );
            if ( tmp_args_element_name_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 168;
            {
                PyObject *call_args[] = { tmp_args_element_name_38 };
                tmp_assign_source_108 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, call_args );
            }

            Py_DECREF( tmp_args_element_name_38 );
            if ( tmp_assign_source_108 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_margin_bottom;
                assert( old != NULL );
                var_margin_bottom = tmp_assign_source_108;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_113;
            PyObject *tmp_left_name_51;
            PyObject *tmp_right_name_51;
            PyObject *tmp_left_name_52;
            PyObject *tmp_right_name_52;
            CHECK_OBJECT( var_margin_bottom );
            tmp_left_name_51 = var_margin_bottom;
            CHECK_OBJECT( var_pad_inches );
            tmp_left_name_52 = var_pad_inches;
            CHECK_OBJECT( var_fig_height_inch );
            tmp_right_name_52 = var_fig_height_inch;
            tmp_right_name_51 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_52, tmp_right_name_52 );
            if ( tmp_right_name_51 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 169;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_51, tmp_right_name_51 );
            Py_DECREF( tmp_right_name_51 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 169;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_113 = tmp_left_name_51;
            var_margin_bottom = tmp_assign_source_113;

        }
        branch_no_14:;
    }
    {
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_compexpr_left_8;
        PyObject *tmp_compexpr_right_8;
        PyObject *tmp_left_name_53;
        PyObject *tmp_right_name_53;
        if ( var_margin_left == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_left" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_53 = var_margin_left;
        if ( var_margin_right == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_right" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_53 = var_margin_right;
        tmp_compexpr_left_8 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_53, tmp_right_name_53 );
        if ( tmp_compexpr_left_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_8 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
        Py_DECREF( tmp_compexpr_left_8 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_15;
        }
        else
        {
            goto branch_no_15;
        }
        branch_yes_15:;
        {
            PyObject *tmp_called_instance_7;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_call_result_12;
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_cbook );

            if (unlikely( tmp_mvar_value_13 == NULL ))
            {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
            }

            if ( tmp_mvar_value_13 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 172;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_7 = tmp_mvar_value_13;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 172;
            tmp_call_result_12 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain__warn_external, &PyTuple_GET_ITEM( const_tuple_str_digest_f83ea12fefdf969d517c165f4e427181_tuple, 0 ) );

            if ( tmp_call_result_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_12 );
        }
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_15:;
    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_compexpr_left_9;
        PyObject *tmp_compexpr_right_9;
        PyObject *tmp_left_name_54;
        PyObject *tmp_right_name_54;
        if ( var_margin_bottom == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_bottom" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_54 = var_margin_bottom;
        if ( var_margin_top == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_top" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_54 = var_margin_top;
        tmp_compexpr_left_9 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_54, tmp_right_name_54 );
        if ( tmp_compexpr_left_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_9 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
        Py_DECREF( tmp_compexpr_left_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_16;
        }
        else
        {
            goto branch_no_16;
        }
        branch_yes_16:;
        {
            PyObject *tmp_called_instance_8;
            PyObject *tmp_mvar_value_14;
            PyObject *tmp_call_result_13;
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_cbook );

            if (unlikely( tmp_mvar_value_14 == NULL ))
            {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
            }

            if ( tmp_mvar_value_14 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 177;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_8 = tmp_mvar_value_14;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 177;
            tmp_call_result_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain__warn_external, &PyTuple_GET_ITEM( const_tuple_str_digest_8b3984fd8500371f6d99a74526a97354_tuple, 0 ) );

            if ( tmp_call_result_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 177;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_13 );
        }
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_16:;
    }
    {
        PyObject *tmp_assign_source_114;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_left_name_55;
        PyObject *tmp_right_name_55;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_left_name_56;
        PyObject *tmp_right_name_56;
        tmp_dict_key_2 = const_str_plain_left;
        if ( var_margin_left == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_left" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_2 = var_margin_left;
        tmp_assign_source_114 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_assign_source_114, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_right;
        tmp_left_name_55 = const_int_pos_1;
        if ( var_margin_right == NULL )
        {
            Py_DECREF( tmp_assign_source_114 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_right" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_55 = var_margin_right;
        tmp_dict_value_3 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_55, tmp_right_name_55 );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_114 );

            exception_lineno = 183;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_114, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_bottom;
        if ( var_margin_bottom == NULL )
        {
            Py_DECREF( tmp_assign_source_114 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_bottom" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 184;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_4 = var_margin_bottom;
        tmp_res = PyDict_SetItem( tmp_assign_source_114, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_top;
        tmp_left_name_56 = const_int_pos_1;
        if ( var_margin_top == NULL )
        {
            Py_DECREF( tmp_assign_source_114 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_top" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_56 = var_margin_top;
        tmp_dict_value_5 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_56, tmp_right_name_56 );
        if ( tmp_dict_value_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_114 );

            exception_lineno = 185;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_114, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        assert( var_kwargs == NULL );
        var_kwargs = tmp_assign_source_114;
    }
    {
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_compexpr_left_10;
        PyObject *tmp_compexpr_right_10;
        CHECK_OBJECT( PyCell_GET( var_cols ) );
        tmp_compexpr_left_10 = PyCell_GET( var_cols );
        tmp_compexpr_right_10 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_17;
        }
        else
        {
            goto branch_no_17;
        }
        branch_yes_17:;
        {
            PyObject *tmp_assign_source_115;
            PyObject *tmp_left_name_57;
            PyObject *tmp_called_name_28;
            PyObject *tmp_args_element_name_39;
            PyObject *tmp_right_name_57;
            PyObject *tmp_left_name_58;
            PyObject *tmp_right_name_58;
            tmp_called_name_28 = LOOKUP_BUILTIN( const_str_plain_max );
            assert( tmp_called_name_28 != NULL );
            {
                PyObject *tmp_assign_source_116;
                PyObject *tmp_iter_arg_21;
                PyObject *tmp_xrange_low_5;
                CHECK_OBJECT( var_rows );
                tmp_xrange_low_5 = var_rows;
                tmp_iter_arg_21 = BUILTIN_XRANGE1( tmp_xrange_low_5 );
                if ( tmp_iter_arg_21 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_116 = MAKE_ITERATOR( tmp_iter_arg_21 );
                Py_DECREF( tmp_iter_arg_21 );
                if ( tmp_assign_source_116 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 188;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_genexpr_2__$0 == NULL );
                tmp_genexpr_2__$0 = tmp_assign_source_116;
            }
            // Tried code:
            tmp_args_element_name_39 = matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_2_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_39)->m_closure[0] = var_cols;
            Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_39)->m_closure[0] );
            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_39)->m_closure[1] = PyCell_NEW0( tmp_genexpr_2__$0 );
            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_39)->m_closure[2] = var_hspaces;
            Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_39)->m_closure[2] );


            goto try_return_handler_37;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_37:;
            CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
            Py_DECREF( tmp_genexpr_2__$0 );
            tmp_genexpr_2__$0 = NULL;

            goto outline_result_10;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
            Py_DECREF( tmp_genexpr_2__$0 );
            tmp_genexpr_2__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            outline_result_10:;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 188;
            {
                PyObject *call_args[] = { tmp_args_element_name_39 };
                tmp_left_name_57 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
            }

            Py_DECREF( tmp_args_element_name_39 );
            if ( tmp_left_name_57 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 188;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_hpad_inches );
            tmp_left_name_58 = var_hpad_inches;
            CHECK_OBJECT( var_fig_width_inch );
            tmp_right_name_58 = var_fig_width_inch;
            tmp_right_name_57 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_58, tmp_right_name_58 );
            if ( tmp_right_name_57 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_57 );

                exception_lineno = 191;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_115 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_57, tmp_right_name_57 );
            Py_DECREF( tmp_left_name_57 );
            Py_DECREF( tmp_right_name_57 );
            if ( tmp_assign_source_115 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 188;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_hspace == NULL );
            var_hspace = tmp_assign_source_115;
        }
        {
            PyObject *tmp_assign_source_117;
            PyObject *tmp_left_name_59;
            PyObject *tmp_left_name_60;
            PyObject *tmp_left_name_61;
            PyObject *tmp_left_name_62;
            PyObject *tmp_right_name_59;
            PyObject *tmp_right_name_60;
            PyObject *tmp_right_name_61;
            PyObject *tmp_left_name_63;
            PyObject *tmp_right_name_62;
            PyObject *tmp_left_name_64;
            PyObject *tmp_right_name_63;
            PyObject *tmp_right_name_64;
            tmp_left_name_62 = const_int_pos_1;
            if ( var_margin_right == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_right" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 193;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_right_name_59 = var_margin_right;
            tmp_left_name_61 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_62, tmp_right_name_59 );
            if ( tmp_left_name_61 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 193;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            if ( var_margin_left == NULL )
            {
                Py_DECREF( tmp_left_name_61 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_left" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 193;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_right_name_60 = var_margin_left;
            tmp_left_name_60 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_61, tmp_right_name_60 );
            Py_DECREF( tmp_left_name_61 );
            if ( tmp_left_name_60 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 193;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_hspace );
            tmp_left_name_63 = var_hspace;
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_left_name_64 = PyCell_GET( var_cols );
            tmp_right_name_63 = const_int_pos_1;
            tmp_right_name_62 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_64, tmp_right_name_63 );
            if ( tmp_right_name_62 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_60 );

                exception_lineno = 193;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_61 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_63, tmp_right_name_62 );
            Py_DECREF( tmp_right_name_62 );
            if ( tmp_right_name_61 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_60 );

                exception_lineno = 193;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_59 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_60, tmp_right_name_61 );
            Py_DECREF( tmp_left_name_60 );
            Py_DECREF( tmp_right_name_61 );
            if ( tmp_left_name_59 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 193;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( PyCell_GET( var_cols ) );
            tmp_right_name_64 = PyCell_GET( var_cols );
            tmp_assign_source_117 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_59, tmp_right_name_64 );
            Py_DECREF( tmp_left_name_59 );
            if ( tmp_assign_source_117 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 193;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_h_axes == NULL );
            var_h_axes = tmp_assign_source_117;
        }
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_compexpr_left_11;
            PyObject *tmp_compexpr_right_11;
            CHECK_OBJECT( var_h_axes );
            tmp_compexpr_left_11 = var_h_axes;
            tmp_compexpr_right_11 = const_int_0;
            tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 194;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_18;
            }
            else
            {
                goto branch_no_18;
            }
            branch_yes_18:;
            {
                PyObject *tmp_called_instance_9;
                PyObject *tmp_mvar_value_15;
                PyObject *tmp_call_result_14;
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_cbook );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
                }

                if ( tmp_mvar_value_15 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 195;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_9 = tmp_mvar_value_15;
                frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 195;
                tmp_call_result_14 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain__warn_external, &PyTuple_GET_ITEM( const_tuple_str_digest_f46c46c7a707a2eff8629742c0941615_tuple, 0 ) );

                if ( tmp_call_result_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 195;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_14 );
            }
            tmp_return_value = Py_None;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            goto branch_end_18;
            branch_no_18:;
            {
                PyObject *tmp_left_name_65;
                PyObject *tmp_right_name_65;
                CHECK_OBJECT( var_hspace );
                tmp_left_name_65 = var_hspace;
                CHECK_OBJECT( var_h_axes );
                tmp_right_name_65 = var_h_axes;
                tmp_dictset_value = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_65, tmp_right_name_65 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 200;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_kwargs );
                tmp_dictset_dict = var_kwargs;
                tmp_dictset_key = const_str_plain_wspace;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            branch_end_18:;
        }
        branch_no_17:;
    }
    {
        nuitka_bool tmp_condition_result_19;
        PyObject *tmp_compexpr_left_12;
        PyObject *tmp_compexpr_right_12;
        CHECK_OBJECT( var_rows );
        tmp_compexpr_left_12 = var_rows;
        tmp_compexpr_right_12 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_19;
        }
        else
        {
            goto branch_no_19;
        }
        branch_yes_19:;
        {
            PyObject *tmp_assign_source_118;
            PyObject *tmp_left_name_66;
            PyObject *tmp_called_name_29;
            PyObject *tmp_args_element_name_40;
            PyObject *tmp_right_name_66;
            PyObject *tmp_left_name_67;
            PyObject *tmp_right_name_67;
            tmp_called_name_29 = LOOKUP_BUILTIN( const_str_plain_max );
            assert( tmp_called_name_29 != NULL );
            {
                PyObject *tmp_assign_source_119;
                PyObject *tmp_iter_arg_22;
                PyObject *tmp_subscribed_name_14;
                PyObject *tmp_subscript_name_14;
                PyObject *tmp_start_name_5;
                PyObject *tmp_stop_name_5;
                PyObject *tmp_operand_name_6;
                PyObject *tmp_step_name_5;
                CHECK_OBJECT( var_vspaces );
                tmp_subscribed_name_14 = var_vspaces;
                CHECK_OBJECT( PyCell_GET( var_cols ) );
                tmp_start_name_5 = PyCell_GET( var_cols );
                CHECK_OBJECT( PyCell_GET( var_cols ) );
                tmp_operand_name_6 = PyCell_GET( var_cols );
                tmp_stop_name_5 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_6 );
                if ( tmp_stop_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 203;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_step_name_5 = Py_None;
                tmp_subscript_name_14 = MAKE_SLICEOBJ3( tmp_start_name_5, tmp_stop_name_5, tmp_step_name_5 );
                Py_DECREF( tmp_stop_name_5 );
                assert( !(tmp_subscript_name_14 == NULL) );
                tmp_iter_arg_22 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_14, tmp_subscript_name_14 );
                Py_DECREF( tmp_subscript_name_14 );
                if ( tmp_iter_arg_22 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 203;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_119 = MAKE_ITERATOR( tmp_iter_arg_22 );
                Py_DECREF( tmp_iter_arg_22 );
                if ( tmp_assign_source_119 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 203;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_genexpr_3__$0 == NULL );
                tmp_genexpr_3__$0 = tmp_assign_source_119;
            }
            // Tried code:
            tmp_args_element_name_40 = matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_3_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_40)->m_closure[0] = PyCell_NEW0( tmp_genexpr_3__$0 );


            goto try_return_handler_38;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            // Return handler code:
            try_return_handler_38:;
            CHECK_OBJECT( (PyObject *)tmp_genexpr_3__$0 );
            Py_DECREF( tmp_genexpr_3__$0 );
            tmp_genexpr_3__$0 = NULL;

            goto outline_result_11;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_genexpr_3__$0 );
            Py_DECREF( tmp_genexpr_3__$0 );
            tmp_genexpr_3__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
            return NULL;
            outline_result_11:;
            frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 203;
            {
                PyObject *call_args[] = { tmp_args_element_name_40 };
                tmp_left_name_66 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_29, call_args );
            }

            Py_DECREF( tmp_args_element_name_40 );
            if ( tmp_left_name_66 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_vpad_inches );
            tmp_left_name_67 = var_vpad_inches;
            CHECK_OBJECT( var_fig_height_inch );
            tmp_right_name_67 = var_fig_height_inch;
            tmp_right_name_66 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_67, tmp_right_name_67 );
            if ( tmp_right_name_66 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_66 );

                exception_lineno = 204;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_118 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_66, tmp_right_name_66 );
            Py_DECREF( tmp_left_name_66 );
            Py_DECREF( tmp_right_name_66 );
            if ( tmp_assign_source_118 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_vspace == NULL );
            var_vspace = tmp_assign_source_118;
        }
        {
            PyObject *tmp_assign_source_120;
            PyObject *tmp_left_name_68;
            PyObject *tmp_left_name_69;
            PyObject *tmp_left_name_70;
            PyObject *tmp_left_name_71;
            PyObject *tmp_right_name_68;
            PyObject *tmp_right_name_69;
            PyObject *tmp_right_name_70;
            PyObject *tmp_left_name_72;
            PyObject *tmp_right_name_71;
            PyObject *tmp_left_name_73;
            PyObject *tmp_right_name_72;
            PyObject *tmp_right_name_73;
            tmp_left_name_71 = const_int_pos_1;
            if ( var_margin_top == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_top" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 205;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_right_name_68 = var_margin_top;
            tmp_left_name_70 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_71, tmp_right_name_68 );
            if ( tmp_left_name_70 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            if ( var_margin_bottom == NULL )
            {
                Py_DECREF( tmp_left_name_70 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "margin_bottom" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 205;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_right_name_69 = var_margin_bottom;
            tmp_left_name_69 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_70, tmp_right_name_69 );
            Py_DECREF( tmp_left_name_70 );
            if ( tmp_left_name_69 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_vspace );
            tmp_left_name_72 = var_vspace;
            CHECK_OBJECT( var_rows );
            tmp_left_name_73 = var_rows;
            tmp_right_name_72 = const_int_pos_1;
            tmp_right_name_71 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_73, tmp_right_name_72 );
            if ( tmp_right_name_71 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_69 );

                exception_lineno = 205;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_70 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_72, tmp_right_name_71 );
            Py_DECREF( tmp_right_name_71 );
            if ( tmp_right_name_70 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_69 );

                exception_lineno = 205;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_68 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_69, tmp_right_name_70 );
            Py_DECREF( tmp_left_name_69 );
            Py_DECREF( tmp_right_name_70 );
            if ( tmp_left_name_68 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_rows );
            tmp_right_name_73 = var_rows;
            tmp_assign_source_120 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_68, tmp_right_name_73 );
            Py_DECREF( tmp_left_name_68 );
            if ( tmp_assign_source_120 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_v_axes == NULL );
            var_v_axes = tmp_assign_source_120;
        }
        {
            nuitka_bool tmp_condition_result_20;
            PyObject *tmp_compexpr_left_13;
            PyObject *tmp_compexpr_right_13;
            CHECK_OBJECT( var_v_axes );
            tmp_compexpr_left_13 = var_v_axes;
            tmp_compexpr_right_13 = const_int_0;
            tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_13, tmp_compexpr_right_13 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 206;
                type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_20 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            {
                PyObject *tmp_called_instance_10;
                PyObject *tmp_mvar_value_16;
                PyObject *tmp_call_result_15;
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_cbook );

                if (unlikely( tmp_mvar_value_16 == NULL ))
                {
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
                }

                if ( tmp_mvar_value_16 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 207;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_10 = tmp_mvar_value_16;
                frame_dfe501d74e083632d082979fedbf32ac->m_frame.f_lineno = 207;
                tmp_call_result_15 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain__warn_external, &PyTuple_GET_ITEM( const_tuple_str_digest_9b40ba1484f1ad56c07048067bab993a_tuple, 0 ) );

                if ( tmp_call_result_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_15 );
            }
            tmp_return_value = Py_None;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            goto branch_end_20;
            branch_no_20:;
            {
                PyObject *tmp_left_name_74;
                PyObject *tmp_right_name_74;
                CHECK_OBJECT( var_vspace );
                tmp_left_name_74 = var_vspace;
                CHECK_OBJECT( var_v_axes );
                tmp_right_name_74 = var_v_axes;
                tmp_dictset_value = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_74, tmp_right_name_74 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 212;
                    type_description_1 = "ooooooooooocooooooooooocoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_kwargs );
                tmp_dictset_dict = var_kwargs;
                tmp_dictset_key = const_str_plain_hspace;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            branch_end_20:;
        }
        branch_no_19:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dfe501d74e083632d082979fedbf32ac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_9;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dfe501d74e083632d082979fedbf32ac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dfe501d74e083632d082979fedbf32ac );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dfe501d74e083632d082979fedbf32ac, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dfe501d74e083632d082979fedbf32ac->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dfe501d74e083632d082979fedbf32ac, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dfe501d74e083632d082979fedbf32ac,
        type_description_1,
        par_fig,
        par_renderer,
        par_nrows_ncols,
        par_num1num2_list,
        par_subplot_list,
        par_ax_bbox_list,
        par_pad,
        par_h_pad,
        par_w_pad,
        par_rect,
        var_rows,
        var_cols,
        var_font_size_inches,
        var_pad_inches,
        var_vpad_inches,
        var_hpad_inches,
        var_margin_left,
        var_margin_bottom,
        var_margin_right,
        var_margin_top,
        var__right,
        var__top,
        var_vspaces,
        var_hspaces,
        var_union,
        var_subplots,
        var_ax_bbox,
        var_num1,
        var_num2,
        var_tight_bbox_raw,
        var_tight_bbox,
        var_row1,
        var_col1,
        var_row2,
        var_col2,
        var_row_i,
        var_col_i,
        var_fig_width_inch,
        var_fig_height_inch,
        var_kwargs,
        var_hspace,
        var_h_axes,
        var_vspace,
        var_v_axes
    );


    // Release cached frame.
    if ( frame_dfe501d74e083632d082979fedbf32ac == cache_frame_dfe501d74e083632d082979fedbf32ac )
    {
        Py_DECREF( frame_dfe501d74e083632d082979fedbf32ac );
    }
    cache_frame_dfe501d74e083632d082979fedbf32ac = NULL;

    assertFrameObject( frame_dfe501d74e083632d082979fedbf32ac );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_9:;
    CHECK_OBJECT( var_kwargs );
    tmp_return_value = var_kwargs;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    CHECK_OBJECT( (PyObject *)par_nrows_ncols );
    Py_DECREF( par_nrows_ncols );
    par_nrows_ncols = NULL;

    CHECK_OBJECT( (PyObject *)par_num1num2_list );
    Py_DECREF( par_num1num2_list );
    par_num1num2_list = NULL;

    CHECK_OBJECT( (PyObject *)par_subplot_list );
    Py_DECREF( par_subplot_list );
    par_subplot_list = NULL;

    CHECK_OBJECT( (PyObject *)par_ax_bbox_list );
    Py_DECREF( par_ax_bbox_list );
    par_ax_bbox_list = NULL;

    CHECK_OBJECT( (PyObject *)par_pad );
    Py_DECREF( par_pad );
    par_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_h_pad );
    Py_DECREF( par_h_pad );
    par_h_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_w_pad );
    Py_DECREF( par_w_pad );
    par_w_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_rect );
    Py_DECREF( par_rect );
    par_rect = NULL;

    CHECK_OBJECT( (PyObject *)var_rows );
    Py_DECREF( var_rows );
    var_rows = NULL;

    CHECK_OBJECT( (PyObject *)var_cols );
    Py_DECREF( var_cols );
    var_cols = NULL;

    CHECK_OBJECT( (PyObject *)var_font_size_inches );
    Py_DECREF( var_font_size_inches );
    var_font_size_inches = NULL;

    CHECK_OBJECT( (PyObject *)var_pad_inches );
    Py_DECREF( var_pad_inches );
    var_pad_inches = NULL;

    CHECK_OBJECT( (PyObject *)var_vpad_inches );
    Py_DECREF( var_vpad_inches );
    var_vpad_inches = NULL;

    CHECK_OBJECT( (PyObject *)var_hpad_inches );
    Py_DECREF( var_hpad_inches );
    var_hpad_inches = NULL;

    Py_XDECREF( var_margin_left );
    var_margin_left = NULL;

    Py_XDECREF( var_margin_bottom );
    var_margin_bottom = NULL;

    Py_XDECREF( var_margin_right );
    var_margin_right = NULL;

    Py_XDECREF( var_margin_top );
    var_margin_top = NULL;

    Py_XDECREF( var__right );
    var__right = NULL;

    Py_XDECREF( var__top );
    var__top = NULL;

    CHECK_OBJECT( (PyObject *)var_vspaces );
    Py_DECREF( var_vspaces );
    var_vspaces = NULL;

    CHECK_OBJECT( (PyObject *)var_hspaces );
    Py_DECREF( var_hspaces );
    var_hspaces = NULL;

    CHECK_OBJECT( (PyObject *)var_union );
    Py_DECREF( var_union );
    var_union = NULL;

    Py_XDECREF( var_subplots );
    var_subplots = NULL;

    Py_XDECREF( var_ax_bbox );
    var_ax_bbox = NULL;

    Py_XDECREF( var_num1 );
    var_num1 = NULL;

    Py_XDECREF( var_num2 );
    var_num2 = NULL;

    Py_XDECREF( var_tight_bbox_raw );
    var_tight_bbox_raw = NULL;

    Py_XDECREF( var_tight_bbox );
    var_tight_bbox = NULL;

    Py_XDECREF( var_row1 );
    var_row1 = NULL;

    Py_XDECREF( var_col1 );
    var_col1 = NULL;

    Py_XDECREF( var_row2 );
    var_row2 = NULL;

    Py_XDECREF( var_col2 );
    var_col2 = NULL;

    Py_XDECREF( var_row_i );
    var_row_i = NULL;

    Py_XDECREF( var_col_i );
    var_col_i = NULL;

    CHECK_OBJECT( (PyObject *)var_fig_width_inch );
    Py_DECREF( var_fig_width_inch );
    var_fig_width_inch = NULL;

    CHECK_OBJECT( (PyObject *)var_fig_height_inch );
    Py_DECREF( var_fig_height_inch );
    var_fig_height_inch = NULL;

    Py_XDECREF( var_kwargs );
    var_kwargs = NULL;

    Py_XDECREF( var_hspace );
    var_hspace = NULL;

    Py_XDECREF( var_h_axes );
    var_h_axes = NULL;

    Py_XDECREF( var_vspace );
    var_vspace = NULL;

    Py_XDECREF( var_v_axes );
    var_v_axes = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_35 = exception_type;
    exception_keeper_value_35 = exception_value;
    exception_keeper_tb_35 = exception_tb;
    exception_keeper_lineno_35 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    CHECK_OBJECT( (PyObject *)par_nrows_ncols );
    Py_DECREF( par_nrows_ncols );
    par_nrows_ncols = NULL;

    CHECK_OBJECT( (PyObject *)par_num1num2_list );
    Py_DECREF( par_num1num2_list );
    par_num1num2_list = NULL;

    CHECK_OBJECT( (PyObject *)par_subplot_list );
    Py_DECREF( par_subplot_list );
    par_subplot_list = NULL;

    Py_XDECREF( par_ax_bbox_list );
    par_ax_bbox_list = NULL;

    CHECK_OBJECT( (PyObject *)par_pad );
    Py_DECREF( par_pad );
    par_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_h_pad );
    Py_DECREF( par_h_pad );
    par_h_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_w_pad );
    Py_DECREF( par_w_pad );
    par_w_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_rect );
    Py_DECREF( par_rect );
    par_rect = NULL;

    Py_XDECREF( var_rows );
    var_rows = NULL;

    CHECK_OBJECT( (PyObject *)var_cols );
    Py_DECREF( var_cols );
    var_cols = NULL;

    Py_XDECREF( var_font_size_inches );
    var_font_size_inches = NULL;

    Py_XDECREF( var_pad_inches );
    var_pad_inches = NULL;

    Py_XDECREF( var_vpad_inches );
    var_vpad_inches = NULL;

    Py_XDECREF( var_hpad_inches );
    var_hpad_inches = NULL;

    Py_XDECREF( var_margin_left );
    var_margin_left = NULL;

    Py_XDECREF( var_margin_bottom );
    var_margin_bottom = NULL;

    Py_XDECREF( var_margin_right );
    var_margin_right = NULL;

    Py_XDECREF( var_margin_top );
    var_margin_top = NULL;

    Py_XDECREF( var__right );
    var__right = NULL;

    Py_XDECREF( var__top );
    var__top = NULL;

    Py_XDECREF( var_vspaces );
    var_vspaces = NULL;

    CHECK_OBJECT( (PyObject *)var_hspaces );
    Py_DECREF( var_hspaces );
    var_hspaces = NULL;

    Py_XDECREF( var_union );
    var_union = NULL;

    Py_XDECREF( var_subplots );
    var_subplots = NULL;

    Py_XDECREF( var_ax_bbox );
    var_ax_bbox = NULL;

    Py_XDECREF( var_num1 );
    var_num1 = NULL;

    Py_XDECREF( var_num2 );
    var_num2 = NULL;

    Py_XDECREF( var_tight_bbox_raw );
    var_tight_bbox_raw = NULL;

    Py_XDECREF( var_tight_bbox );
    var_tight_bbox = NULL;

    Py_XDECREF( var_row1 );
    var_row1 = NULL;

    Py_XDECREF( var_col1 );
    var_col1 = NULL;

    Py_XDECREF( var_row2 );
    var_row2 = NULL;

    Py_XDECREF( var_col2 );
    var_col2 = NULL;

    Py_XDECREF( var_row_i );
    var_row_i = NULL;

    Py_XDECREF( var_col_i );
    var_col_i = NULL;

    Py_XDECREF( var_fig_width_inch );
    var_fig_width_inch = NULL;

    Py_XDECREF( var_fig_height_inch );
    var_fig_height_inch = NULL;

    Py_XDECREF( var_kwargs );
    var_kwargs = NULL;

    Py_XDECREF( var_hspace );
    var_hspace = NULL;

    Py_XDECREF( var_h_axes );
    var_h_axes = NULL;

    Py_XDECREF( var_vspace );
    var_vspace = NULL;

    Py_XDECREF( var_v_axes );
    var_v_axes = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_35;
    exception_value = exception_keeper_value_35;
    exception_tb = exception_keeper_tb_35;
    exception_lineno = exception_keeper_lineno_35;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_1_genexpr_locals {
    PyObject *var_ax;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_1_genexpr_locals *generator_heap = (struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_ax = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_3af0788c949f194472a6e6612cb18973, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 108;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_ax;
            generator_heap->var_ax = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_ax );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_ax );
        tmp_called_instance_1 = generator_heap->var_ax;
        generator->m_frame->m_frame.f_lineno = 108;
        tmp_operand_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_visible );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 108;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 108;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = ( generator_heap->tmp_res == 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_expression_name_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_operand_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_operand_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 108;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 108;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_ax
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_ax );
    generator_heap->var_ax = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_ax );
    generator_heap->var_ax = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_1_genexpr_context,
        module_matplotlib$tight_layout,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_a2850d87673bb7476bb6ea3595d327b2,
#endif
        codeobj_3af0788c949f194472a6e6612cb18973,
        1,
        sizeof(struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_1_genexpr_locals)
    );
}



struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_2_genexpr_locals {
    PyObject *var_i;
    PyObject *var_s;
    PyObject *tmp_contraction_iter_0;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_iter_value_1;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_2_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_2_genexpr_locals *generator_heap = (struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_2_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_i = NULL;
    generator_heap->var_s = NULL;
    generator_heap->tmp_contraction_iter_0 = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_iter_value_1 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_ca2c371fef01f5b6aa6b49fb0a213168, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[1] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[1] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noocc";
                generator_heap->exception_lineno = 188;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_1;
            generator_heap->tmp_iter_value_1 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_1 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_1;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_left_name_6;
        PyObject *tmp_right_name_4;
        PyObject *tmp_right_name_5;
        PyObject *tmp_left_name_7;
        PyObject *tmp_right_name_6;
        PyObject *tmp_right_name_7;
        PyObject *tmp_step_name_1;
        if ( PyCell_GET( generator->m_closure[2] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "hspaces" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_subscribed_name_1 = PyCell_GET( generator->m_closure[2] );
        CHECK_OBJECT( generator_heap->var_i );
        tmp_left_name_2 = generator_heap->var_i;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "cols" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_left_name_3 = PyCell_GET( generator->m_closure[0] );
        tmp_right_name_2 = const_int_pos_1;
        tmp_right_name_1 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_3, tmp_right_name_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_right_name_3 = const_int_pos_1;
        tmp_start_name_1 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_start_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( generator_heap->var_i );
        tmp_left_name_6 = generator_heap->var_i;
        tmp_right_name_4 = const_int_pos_1;
        tmp_left_name_5 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_6, tmp_right_name_4 );
        if ( tmp_left_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_start_name_1 );

            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_start_name_1 );
            Py_DECREF( tmp_left_name_5 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "cols" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_left_name_7 = PyCell_GET( generator->m_closure[0] );
        tmp_right_name_6 = const_int_pos_1;
        tmp_right_name_5 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_7, tmp_right_name_6 );
        if ( tmp_right_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_start_name_1 );
            Py_DECREF( tmp_left_name_5 );

            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_left_name_4 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_5 );
        Py_DECREF( tmp_right_name_5 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_start_name_1 );

            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_right_name_7 = const_int_pos_1;
        tmp_stop_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_4, tmp_right_name_7 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_stop_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_start_name_1 );

            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        Py_DECREF( tmp_start_name_1 );
        Py_DECREF( tmp_stop_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_iter_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = generator_heap->tmp_contraction_iter_0;
            generator_heap->tmp_contraction_iter_0 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( generator_heap->tmp_contraction_iter_0 );
        tmp_next_source_2 = generator_heap->tmp_contraction_iter_0;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noocc";
                generator_heap->exception_lineno = 188;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_5 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_s;
            generator_heap->var_s = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_s );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_sum_sequence_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_s );
        tmp_sum_sequence_1 = generator_heap->var_s;
        tmp_expression_name_1 = BUILTIN_SUM1( tmp_sum_sequence_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_sum_sequence_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_sum_sequence_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 188;
        generator_heap->type_description_1 = "Noocc";
        goto try_except_handler_2;
    }
    goto loop_start_2;
    loop_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_contraction_iter_0 );
    Py_DECREF( generator_heap->tmp_contraction_iter_0 );
    generator_heap->tmp_contraction_iter_0 = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 188;
        generator_heap->type_description_1 = "Noocc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->tmp_contraction_iter_0 );
    generator_heap->tmp_contraction_iter_0 = NULL;

    Py_XDECREF( generator_heap->tmp_iter_value_1 );
    generator_heap->tmp_iter_value_1 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_i,
            generator_heap->var_s,
            generator->m_closure[2],
            generator->m_closure[0]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    Py_XDECREF( generator_heap->var_s );
    generator_heap->var_s = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->tmp_contraction_iter_0 );
    generator_heap->tmp_contraction_iter_0 = NULL;

    Py_XDECREF( generator_heap->tmp_iter_value_1 );
    generator_heap->tmp_iter_value_1 = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    Py_XDECREF( generator_heap->var_s );
    generator_heap->var_s = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_2_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_2_genexpr_context,
        module_matplotlib$tight_layout,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_a2850d87673bb7476bb6ea3595d327b2,
#endif
        codeobj_ca2c371fef01f5b6aa6b49fb0a213168,
        3,
        sizeof(struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_2_genexpr_locals)
    );
}



struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_3_genexpr_locals {
    PyObject *var_s;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_3_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_3_genexpr_locals *generator_heap = (struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_3_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_s = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_c807c51e7958476d3d90a2b4857dd0b2, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 203;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_s;
            generator_heap->var_s = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_s );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_sum_sequence_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_s );
        tmp_sum_sequence_1 = generator_heap->var_s;
        tmp_expression_name_1 = BUILTIN_SUM1( tmp_sum_sequence_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 203;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_sum_sequence_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_sum_sequence_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 203;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 203;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_s
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_s );
    generator_heap->var_s = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_s );
    generator_heap->var_s = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_3_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_3_genexpr_context,
        module_matplotlib$tight_layout,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_a2850d87673bb7476bb6ea3595d327b2,
#endif
        codeobj_c807c51e7958476d3d90a2b4857dd0b2,
        1,
        sizeof(struct matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars$$$genexpr_3_genexpr_locals)
    );
}


static PyObject *impl_matplotlib$tight_layout$$$function_6_get_renderer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fig = python_pars[ 0 ];
    PyObject *var_renderer = NULL;
    PyObject *var_canvas = NULL;
    PyObject *var_FigureCanvasAgg = NULL;
    struct Nuitka_FrameObject *frame_ed465ddd478fe10c713c4573bf253c8f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_ed465ddd478fe10c713c4573bf253c8f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ed465ddd478fe10c713c4573bf253c8f, codeobj_ed465ddd478fe10c713c4573bf253c8f, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ed465ddd478fe10c713c4573bf253c8f = cache_frame_ed465ddd478fe10c713c4573bf253c8f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ed465ddd478fe10c713c4573bf253c8f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ed465ddd478fe10c713c4573bf253c8f ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_fig );
        tmp_source_name_1 = par_fig;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__cachedRenderer );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 218;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_fig );
            tmp_source_name_2 = par_fig;
            tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__cachedRenderer );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 219;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_renderer == NULL );
            var_renderer = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_fig );
            tmp_source_name_3 = par_fig;
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_canvas );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 221;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_canvas == NULL );
            var_canvas = tmp_assign_source_2;
        }
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            int tmp_truth_name_2;
            PyObject *tmp_source_name_4;
            PyObject *tmp_attribute_name_1;
            CHECK_OBJECT( var_canvas );
            tmp_truth_name_2 = CHECK_IF_TRUE( var_canvas );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( var_canvas );
            tmp_source_name_4 = var_canvas;
            tmp_attribute_name_1 = const_str_plain_get_renderer;
            tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_4, tmp_attribute_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_2 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_called_instance_1;
                CHECK_OBJECT( var_canvas );
                tmp_called_instance_1 = var_canvas;
                frame_ed465ddd478fe10c713c4573bf253c8f->m_frame.f_lineno = 224;
                tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_renderer );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 224;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                assert( var_renderer == NULL );
                var_renderer = tmp_assign_source_3;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_called_instance_2;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_call_result_1;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_cbook );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 227;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_2 = tmp_mvar_value_1;
                frame_ed465ddd478fe10c713c4573bf253c8f->m_frame.f_lineno = 227;
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain__warn_external, &PyTuple_GET_ITEM( const_tuple_str_digest_b1a5174aba8b0a3a776575229ad039ae_tuple, 0 ) );

                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 227;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_import_name_from_1;
                PyObject *tmp_name_name_1;
                PyObject *tmp_globals_name_1;
                PyObject *tmp_locals_name_1;
                PyObject *tmp_fromlist_name_1;
                PyObject *tmp_level_name_1;
                tmp_name_name_1 = const_str_digest_8090d9314ba82f55c86e2a009f824e92;
                tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$tight_layout;
                tmp_locals_name_1 = Py_None;
                tmp_fromlist_name_1 = const_tuple_str_plain_FigureCanvasAgg_tuple;
                tmp_level_name_1 = const_int_0;
                frame_ed465ddd478fe10c713c4573bf253c8f->m_frame.f_lineno = 228;
                tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
                if ( tmp_import_name_from_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_FigureCanvasAgg );
                Py_DECREF( tmp_import_name_from_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                assert( var_FigureCanvasAgg == NULL );
                var_FigureCanvasAgg = tmp_assign_source_4;
            }
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_called_name_1;
                PyObject *tmp_args_element_name_1;
                CHECK_OBJECT( var_FigureCanvasAgg );
                tmp_called_name_1 = var_FigureCanvasAgg;
                CHECK_OBJECT( par_fig );
                tmp_args_element_name_1 = par_fig;
                frame_ed465ddd478fe10c713c4573bf253c8f->m_frame.f_lineno = 229;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1 };
                    tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                if ( tmp_assign_source_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 229;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_canvas;
                    assert( old != NULL );
                    var_canvas = tmp_assign_source_5;
                    Py_DECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_called_instance_3;
                CHECK_OBJECT( var_canvas );
                tmp_called_instance_3 = var_canvas;
                frame_ed465ddd478fe10c713c4573bf253c8f->m_frame.f_lineno = 230;
                tmp_assign_source_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_renderer );
                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 230;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                assert( var_renderer == NULL );
                var_renderer = tmp_assign_source_6;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }
    if ( var_renderer == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "renderer" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 232;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_return_value = var_renderer;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed465ddd478fe10c713c4573bf253c8f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed465ddd478fe10c713c4573bf253c8f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed465ddd478fe10c713c4573bf253c8f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ed465ddd478fe10c713c4573bf253c8f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ed465ddd478fe10c713c4573bf253c8f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ed465ddd478fe10c713c4573bf253c8f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ed465ddd478fe10c713c4573bf253c8f,
        type_description_1,
        par_fig,
        var_renderer,
        var_canvas,
        var_FigureCanvasAgg
    );


    // Release cached frame.
    if ( frame_ed465ddd478fe10c713c4573bf253c8f == cache_frame_ed465ddd478fe10c713c4573bf253c8f )
    {
        Py_DECREF( frame_ed465ddd478fe10c713c4573bf253c8f );
    }
    cache_frame_ed465ddd478fe10c713c4573bf253c8f = NULL;

    assertFrameObject( frame_ed465ddd478fe10c713c4573bf253c8f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_6_get_renderer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    Py_XDECREF( var_renderer );
    var_renderer = NULL;

    Py_XDECREF( var_canvas );
    var_canvas = NULL;

    Py_XDECREF( var_FigureCanvasAgg );
    var_FigureCanvasAgg = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    Py_XDECREF( var_renderer );
    var_renderer = NULL;

    Py_XDECREF( var_canvas );
    var_canvas = NULL;

    Py_XDECREF( var_FigureCanvasAgg );
    var_FigureCanvasAgg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_6_get_renderer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$tight_layout$$$function_7_get_subplotspec_list( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_axes_list = python_pars[ 0 ];
    PyObject *par_grid_spec = python_pars[ 1 ];
    PyObject *var_subplotspec_list = NULL;
    PyObject *var_ax = NULL;
    PyObject *var_axes_or_locator = NULL;
    PyObject *var_subplotspec = NULL;
    PyObject *var_gs = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_e06e28650b8962f2e6e2d032dd2fa616;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_e06e28650b8962f2e6e2d032dd2fa616 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_subplotspec_list == NULL );
        var_subplotspec_list = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e06e28650b8962f2e6e2d032dd2fa616, codeobj_e06e28650b8962f2e6e2d032dd2fa616, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e06e28650b8962f2e6e2d032dd2fa616 = cache_frame_e06e28650b8962f2e6e2d032dd2fa616;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e06e28650b8962f2e6e2d032dd2fa616 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e06e28650b8962f2e6e2d032dd2fa616 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_axes_list );
        tmp_iter_arg_1 = par_axes_list;
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 245;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooo";
                exception_lineno = 245;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_ax;
            var_ax = tmp_assign_source_4;
            Py_INCREF( var_ax );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_ax );
        tmp_called_instance_1 = var_ax;
        frame_e06e28650b8962f2e6e2d032dd2fa616->m_frame.f_lineno = 246;
        tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_axes_locator );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_axes_or_locator;
            var_axes_or_locator = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_axes_or_locator );
        tmp_compexpr_left_1 = var_axes_or_locator;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( var_ax );
            tmp_assign_source_6 = var_ax;
            {
                PyObject *old = var_axes_or_locator;
                assert( old != NULL );
                var_axes_or_locator = tmp_assign_source_6;
                Py_INCREF( var_axes_or_locator );
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_name_1;
        CHECK_OBJECT( var_axes_or_locator );
        tmp_source_name_1 = var_axes_or_locator;
        tmp_attribute_name_1 = const_str_plain_get_subplotspec;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 250;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( var_axes_or_locator );
            tmp_called_instance_2 = var_axes_or_locator;
            frame_e06e28650b8962f2e6e2d032dd2fa616->m_frame.f_lineno = 251;
            tmp_assign_source_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_subplotspec );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 251;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = var_subplotspec;
                var_subplotspec = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_called_instance_3;
            CHECK_OBJECT( var_subplotspec );
            tmp_called_instance_3 = var_subplotspec;
            frame_e06e28650b8962f2e6e2d032dd2fa616->m_frame.f_lineno = 252;
            tmp_assign_source_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_topmost_subplotspec );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 252;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = var_subplotspec;
                assert( old != NULL );
                var_subplotspec = tmp_assign_source_8;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_called_instance_4;
            CHECK_OBJECT( var_subplotspec );
            tmp_called_instance_4 = var_subplotspec;
            frame_e06e28650b8962f2e6e2d032dd2fa616->m_frame.f_lineno = 253;
            tmp_assign_source_9 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_get_gridspec );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 253;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = var_gs;
                var_gs = tmp_assign_source_9;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_grid_spec );
            tmp_compexpr_left_2 = par_grid_spec;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_3 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( var_gs );
                tmp_compexpr_left_3 = var_gs;
                CHECK_OBJECT( par_grid_spec );
                tmp_compexpr_right_3 = par_grid_spec;
                tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 255;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_10;
                    tmp_assign_source_10 = Py_None;
                    {
                        PyObject *old = var_subplotspec;
                        assert( old != NULL );
                        var_subplotspec = tmp_assign_source_10;
                        Py_INCREF( var_subplotspec );
                        Py_DECREF( old );
                    }

                }
                branch_no_4:;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_called_instance_5;
                PyObject *tmp_call_result_1;
                int tmp_truth_name_1;
                CHECK_OBJECT( var_gs );
                tmp_called_instance_5 = var_gs;
                frame_e06e28650b8962f2e6e2d032dd2fa616->m_frame.f_lineno = 257;
                tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_locally_modified_subplot_params );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 257;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }
                tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
                if ( tmp_truth_name_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_call_result_1 );

                    exception_lineno = 257;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_5 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_call_result_1 );
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_assign_source_11;
                    tmp_assign_source_11 = Py_None;
                    {
                        PyObject *old = var_subplotspec;
                        assert( old != NULL );
                        var_subplotspec = tmp_assign_source_11;
                        Py_INCREF( var_subplotspec );
                        Py_DECREF( old );
                    }

                }
                branch_no_5:;
            }
            branch_end_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = Py_None;
            {
                PyObject *old = var_subplotspec;
                var_subplotspec = tmp_assign_source_12;
                Py_INCREF( var_subplotspec );
                Py_XDECREF( old );
            }

        }
        branch_end_2:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_subplotspec_list );
        tmp_source_name_2 = var_subplotspec_list;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        if ( var_subplotspec == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "subplotspec" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 262;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }

        tmp_args_element_name_1 = var_subplotspec;
        frame_e06e28650b8962f2e6e2d032dd2fa616->m_frame.f_lineno = 262;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 245;
        type_description_1 = "ooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e06e28650b8962f2e6e2d032dd2fa616 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e06e28650b8962f2e6e2d032dd2fa616 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e06e28650b8962f2e6e2d032dd2fa616, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e06e28650b8962f2e6e2d032dd2fa616->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e06e28650b8962f2e6e2d032dd2fa616, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e06e28650b8962f2e6e2d032dd2fa616,
        type_description_1,
        par_axes_list,
        par_grid_spec,
        var_subplotspec_list,
        var_ax,
        var_axes_or_locator,
        var_subplotspec,
        var_gs
    );


    // Release cached frame.
    if ( frame_e06e28650b8962f2e6e2d032dd2fa616 == cache_frame_e06e28650b8962f2e6e2d032dd2fa616 )
    {
        Py_DECREF( frame_e06e28650b8962f2e6e2d032dd2fa616 );
    }
    cache_frame_e06e28650b8962f2e6e2d032dd2fa616 = NULL;

    assertFrameObject( frame_e06e28650b8962f2e6e2d032dd2fa616 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_subplotspec_list );
    tmp_return_value = var_subplotspec_list;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_7_get_subplotspec_list );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_axes_list );
    Py_DECREF( par_axes_list );
    par_axes_list = NULL;

    CHECK_OBJECT( (PyObject *)par_grid_spec );
    Py_DECREF( par_grid_spec );
    par_grid_spec = NULL;

    CHECK_OBJECT( (PyObject *)var_subplotspec_list );
    Py_DECREF( var_subplotspec_list );
    var_subplotspec_list = NULL;

    Py_XDECREF( var_ax );
    var_ax = NULL;

    Py_XDECREF( var_axes_or_locator );
    var_axes_or_locator = NULL;

    Py_XDECREF( var_subplotspec );
    var_subplotspec = NULL;

    Py_XDECREF( var_gs );
    var_gs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_axes_list );
    Py_DECREF( par_axes_list );
    par_axes_list = NULL;

    CHECK_OBJECT( (PyObject *)par_grid_spec );
    Py_DECREF( par_grid_spec );
    par_grid_spec = NULL;

    CHECK_OBJECT( (PyObject *)var_subplotspec_list );
    Py_DECREF( var_subplotspec_list );
    var_subplotspec_list = NULL;

    Py_XDECREF( var_ax );
    var_ax = NULL;

    Py_XDECREF( var_axes_or_locator );
    var_axes_or_locator = NULL;

    Py_XDECREF( var_subplotspec );
    var_subplotspec = NULL;

    Py_XDECREF( var_gs );
    var_gs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_7_get_subplotspec_list );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$tight_layout$$$function_8_get_tight_layout_figure( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fig = python_pars[ 0 ];
    PyObject *par_axes_list = python_pars[ 1 ];
    PyObject *par_subplotspec_list = python_pars[ 2 ];
    PyObject *par_renderer = python_pars[ 3 ];
    PyObject *par_pad = python_pars[ 4 ];
    PyObject *par_h_pad = python_pars[ 5 ];
    PyObject *par_w_pad = python_pars[ 6 ];
    PyObject *par_rect = python_pars[ 7 ];
    PyObject *var_subplot_list = NULL;
    PyObject *var_nrows_list = NULL;
    PyObject *var_ncols_list = NULL;
    PyObject *var_ax_bbox_list = NULL;
    PyObject *var_subplot_dict = NULL;
    PyObject *var_subplotspec_list2 = NULL;
    PyObject *var_ax = NULL;
    PyObject *var_subplotspec = NULL;
    PyObject *var_subplots = NULL;
    PyObject *var_myrows = NULL;
    PyObject *var_mycols = NULL;
    PyObject *var__ = NULL;
    PyObject *var_max_nrows = NULL;
    PyObject *var_max_ncols = NULL;
    PyObject *var_num1num2_list = NULL;
    PyObject *var_rows = NULL;
    PyObject *var_cols = NULL;
    PyObject *var_num1 = NULL;
    PyObject *var_num2 = NULL;
    PyObject *var_div_row = NULL;
    PyObject *var_mod_row = NULL;
    PyObject *var_div_col = NULL;
    PyObject *var_mod_col = NULL;
    PyObject *var_rowNum1 = NULL;
    PyObject *var_colNum1 = NULL;
    PyObject *var_rowNum2 = NULL;
    PyObject *var_colNum2 = NULL;
    PyObject *var_kwargs = NULL;
    PyObject *var_left = NULL;
    PyObject *var_bottom = NULL;
    PyObject *var_right = NULL;
    PyObject *var_top = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__element_3 = NULL;
    PyObject *tmp_tuple_unpack_2__element_4 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__element_3 = NULL;
    PyObject *tmp_tuple_unpack_3__element_4 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_tuple_unpack_4__element_1 = NULL;
    PyObject *tmp_tuple_unpack_4__element_2 = NULL;
    PyObject *tmp_tuple_unpack_4__source_iter = NULL;
    PyObject *tmp_tuple_unpack_5__element_1 = NULL;
    PyObject *tmp_tuple_unpack_5__element_2 = NULL;
    PyObject *tmp_tuple_unpack_5__source_iter = NULL;
    PyObject *tmp_tuple_unpack_6__element_1 = NULL;
    PyObject *tmp_tuple_unpack_6__element_2 = NULL;
    PyObject *tmp_tuple_unpack_6__source_iter = NULL;
    PyObject *tmp_tuple_unpack_7__element_1 = NULL;
    PyObject *tmp_tuple_unpack_7__element_2 = NULL;
    PyObject *tmp_tuple_unpack_7__source_iter = NULL;
    PyObject *tmp_tuple_unpack_8__element_1 = NULL;
    PyObject *tmp_tuple_unpack_8__element_2 = NULL;
    PyObject *tmp_tuple_unpack_8__source_iter = NULL;
    PyObject *tmp_tuple_unpack_9__element_1 = NULL;
    PyObject *tmp_tuple_unpack_9__element_2 = NULL;
    PyObject *tmp_tuple_unpack_9__element_3 = NULL;
    PyObject *tmp_tuple_unpack_9__element_4 = NULL;
    PyObject *tmp_tuple_unpack_9__source_iter = NULL;
    struct Nuitka_FrameObject *frame_929741756295e159ad639a088117b506;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_929741756295e159ad639a088117b506 = NULL;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_subplot_list == NULL );
        var_subplot_list = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( var_nrows_list == NULL );
        var_nrows_list = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = PyList_New( 0 );
        assert( var_ncols_list == NULL );
        var_ncols_list = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = PyList_New( 0 );
        assert( var_ax_bbox_list == NULL );
        var_ax_bbox_list = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = PyDict_New();
        assert( var_subplot_dict == NULL );
        var_subplot_dict = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = PyList_New( 0 );
        assert( var_subplotspec_list2 == NULL );
        var_subplotspec_list2 = tmp_assign_source_6;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_929741756295e159ad639a088117b506, codeobj_929741756295e159ad639a088117b506, module_matplotlib$tight_layout, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_929741756295e159ad639a088117b506 = cache_frame_929741756295e159ad639a088117b506;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_929741756295e159ad639a088117b506 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_929741756295e159ad639a088117b506 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_called_name_1 = (PyObject *)&PyZip_Type;
        CHECK_OBJECT( par_axes_list );
        tmp_args_element_name_1 = par_axes_list;
        CHECK_OBJECT( par_subplotspec_list );
        tmp_args_element_name_2 = par_subplotspec_list;
        frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 308;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 308;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 308;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_7;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 308;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_9 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 308;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_10 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_10 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 308;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_11 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_11 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 308;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    exception_lineno = 308;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 308;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_12;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_12 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_ax;
            var_ax = tmp_assign_source_12;
            Py_INCREF( var_ax );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_13 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_subplotspec;
            var_subplotspec = tmp_assign_source_13;
            Py_INCREF( var_subplotspec );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_subplotspec );
        tmp_compexpr_left_1 = var_subplotspec;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_start_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( var_subplot_dict );
        tmp_called_instance_1 = var_subplot_dict;
        CHECK_OBJECT( var_subplotspec );
        tmp_args_element_name_3 = var_subplotspec;
        tmp_args_element_name_4 = PyList_New( 0 );
        frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 312;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_14 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_setdefault, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_subplots;
            var_subplots = tmp_assign_source_14;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( var_subplots );
        tmp_operand_name_1 = var_subplots;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        // Tried code:
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_iter_arg_3;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( var_subplotspec );
            tmp_called_instance_2 = var_subplotspec;
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 315;
            tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_geometry );
            if ( tmp_iter_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 315;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_5;
            }
            tmp_assign_source_15 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
            Py_DECREF( tmp_iter_arg_3 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 315;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__source_iter;
                tmp_tuple_unpack_2__source_iter = tmp_assign_source_15;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_16 = UNPACK_NEXT( tmp_unpack_3, 0, 4 );
            if ( tmp_assign_source_16 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 315;
                goto try_except_handler_6;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_1;
                tmp_tuple_unpack_2__element_1 = tmp_assign_source_16;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_4, 1, 4 );
            if ( tmp_assign_source_17 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 315;
                goto try_except_handler_6;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_2;
                tmp_tuple_unpack_2__element_2 = tmp_assign_source_17;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_unpack_5;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_5 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_5, 2, 4 );
            if ( tmp_assign_source_18 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 315;
                goto try_except_handler_6;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_3;
                tmp_tuple_unpack_2__element_3 = tmp_assign_source_18;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_unpack_6;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_6 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_6, 3, 4 );
            if ( tmp_assign_source_19 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 315;
                goto try_except_handler_6;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_4;
                tmp_tuple_unpack_2__element_4 = tmp_assign_source_19;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_2;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                        exception_lineno = 315;
                        goto try_except_handler_6;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 4)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 315;
                goto try_except_handler_6;
            }
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_5;
        // End of try:
        try_end_3:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_3 );
        tmp_tuple_unpack_2__element_3 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_4 );
        tmp_tuple_unpack_2__element_4 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_2;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        {
            PyObject *tmp_assign_source_20;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
            tmp_assign_source_20 = tmp_tuple_unpack_2__element_1;
            {
                PyObject *old = var_myrows;
                var_myrows = tmp_assign_source_20;
                Py_INCREF( var_myrows );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        {
            PyObject *tmp_assign_source_21;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
            tmp_assign_source_21 = tmp_tuple_unpack_2__element_2;
            {
                PyObject *old = var_mycols;
                var_mycols = tmp_assign_source_21;
                Py_INCREF( var_mycols );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        {
            PyObject *tmp_assign_source_22;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_3 );
            tmp_assign_source_22 = tmp_tuple_unpack_2__element_3;
            {
                PyObject *old = var__;
                var__ = tmp_assign_source_22;
                Py_INCREF( var__ );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_3 );
        tmp_tuple_unpack_2__element_3 = NULL;

        {
            PyObject *tmp_assign_source_23;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_4 );
            tmp_assign_source_23 = tmp_tuple_unpack_2__element_4;
            {
                PyObject *old = var__;
                assert( old != NULL );
                var__ = tmp_assign_source_23;
                Py_INCREF( var__ );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_4 );
        tmp_tuple_unpack_2__element_4 = NULL;

        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( var_nrows_list );
            tmp_called_instance_3 = var_nrows_list;
            CHECK_OBJECT( var_myrows );
            tmp_args_element_name_5 = var_myrows;
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 316;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 316;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_called_instance_4;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( var_ncols_list );
            tmp_called_instance_4 = var_ncols_list;
            CHECK_OBJECT( var_mycols );
            tmp_args_element_name_6 = var_mycols;
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 317;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            PyObject *tmp_called_instance_5;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_7;
            CHECK_OBJECT( var_subplotspec_list2 );
            tmp_called_instance_5 = var_subplotspec_list2;
            CHECK_OBJECT( var_subplotspec );
            tmp_args_element_name_7 = var_subplotspec;
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 318;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 318;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        {
            PyObject *tmp_called_instance_6;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_8;
            CHECK_OBJECT( var_subplot_list );
            tmp_called_instance_6 = var_subplot_list;
            CHECK_OBJECT( var_subplots );
            tmp_args_element_name_8 = var_subplots;
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 319;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 319;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_call_result_5;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_called_instance_7;
            PyObject *tmp_args_element_name_10;
            CHECK_OBJECT( var_ax_bbox_list );
            tmp_source_name_1 = var_ax_bbox_list;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( var_subplotspec );
            tmp_called_instance_7 = var_subplotspec;
            CHECK_OBJECT( par_fig );
            tmp_args_element_name_10 = par_fig;
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 320;
            {
                PyObject *call_args[] = { tmp_args_element_name_10 };
                tmp_args_element_name_9 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_get_position, call_args );
            }

            if ( tmp_args_element_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 320;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_2;
            }
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 320;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_called_instance_8;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_11;
        CHECK_OBJECT( var_subplots );
        tmp_called_instance_8 = var_subplots;
        CHECK_OBJECT( var_ax );
        tmp_args_element_name_11 = var_ax;
        frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 322;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 322;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 308;
        type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        nuitka_bool tmp_condition_result_3;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_len_arg_2;
        CHECK_OBJECT( var_nrows_list );
        tmp_len_arg_1 = var_nrows_list;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        assert( !(tmp_compexpr_left_2 == NULL) );
        tmp_compexpr_right_2 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_or_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( var_ncols_list );
        tmp_len_arg_2 = var_ncols_list;
        tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_2 );
        assert( !(tmp_compexpr_left_3 == NULL) );
        tmp_compexpr_right_3 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        assert( !(tmp_res == -1) );
        tmp_or_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_3 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_3 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        tmp_return_value = PyDict_New();
        goto frame_return_exit_1;
        branch_no_3:;
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_12;
        tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_max );
        assert( tmp_called_name_3 != NULL );
        CHECK_OBJECT( var_nrows_list );
        tmp_args_element_name_12 = var_nrows_list;
        frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 327;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 327;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_max_nrows == NULL );
        var_max_nrows = tmp_assign_source_24;
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_name_4;
        PyObject *tmp_args_element_name_13;
        tmp_called_name_4 = LOOKUP_BUILTIN( const_str_plain_max );
        assert( tmp_called_name_4 != NULL );
        CHECK_OBJECT( var_ncols_list );
        tmp_args_element_name_13 = var_ncols_list;
        frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 328;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_assign_source_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 328;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_max_ncols == NULL );
        var_max_ncols = tmp_assign_source_25;
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = PyList_New( 0 );
        assert( var_num1num2_list == NULL );
        var_num1num2_list = tmp_assign_source_26;
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_iter_arg_4;
        CHECK_OBJECT( var_subplotspec_list2 );
        tmp_iter_arg_4 = var_subplotspec_list2;
        tmp_assign_source_27 = MAKE_ITERATOR( tmp_iter_arg_4 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 331;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_2__for_iterator == NULL );
        tmp_for_loop_2__for_iterator = tmp_assign_source_27;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_28;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_28 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_28 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 331;
                goto try_except_handler_7;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_28;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_29;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_29 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_subplotspec;
            var_subplotspec = tmp_assign_source_29;
            Py_INCREF( var_subplotspec );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_iter_arg_5;
        PyObject *tmp_called_instance_9;
        CHECK_OBJECT( var_subplotspec );
        tmp_called_instance_9 = var_subplotspec;
        frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 332;
        tmp_iter_arg_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_get_geometry );
        if ( tmp_iter_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 332;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_8;
        }
        tmp_assign_source_30 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_5 );
        Py_DECREF( tmp_iter_arg_5 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 332;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_8;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__source_iter;
            tmp_tuple_unpack_3__source_iter = tmp_assign_source_30;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_unpack_7;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_7 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_31 = UNPACK_NEXT( tmp_unpack_7, 0, 4 );
        if ( tmp_assign_source_31 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 332;
            goto try_except_handler_9;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__element_1;
            tmp_tuple_unpack_3__element_1 = tmp_assign_source_31;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_unpack_8;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_8 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_32 = UNPACK_NEXT( tmp_unpack_8, 1, 4 );
        if ( tmp_assign_source_32 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 332;
            goto try_except_handler_9;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__element_2;
            tmp_tuple_unpack_3__element_2 = tmp_assign_source_32;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_unpack_9;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_9 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_33 = UNPACK_NEXT( tmp_unpack_9, 2, 4 );
        if ( tmp_assign_source_33 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 332;
            goto try_except_handler_9;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__element_3;
            tmp_tuple_unpack_3__element_3 = tmp_assign_source_33;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_unpack_10;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_10 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_34 = UNPACK_NEXT( tmp_unpack_10, 3, 4 );
        if ( tmp_assign_source_34 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 332;
            goto try_except_handler_9;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__element_4;
            tmp_tuple_unpack_3__element_4 = tmp_assign_source_34;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_3;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_iterator_name_3 = tmp_tuple_unpack_3__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    exception_lineno = 332;
                    goto try_except_handler_9;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 4)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 332;
            goto try_except_handler_9;
        }
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto try_except_handler_8;
    // End of try:
    try_end_6:;
    goto try_end_7;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_3 );
    tmp_tuple_unpack_3__element_3 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_4 );
    tmp_tuple_unpack_3__element_4 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto try_except_handler_7;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    {
        PyObject *tmp_assign_source_35;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
        tmp_assign_source_35 = tmp_tuple_unpack_3__element_1;
        {
            PyObject *old = var_rows;
            var_rows = tmp_assign_source_35;
            Py_INCREF( var_rows );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    {
        PyObject *tmp_assign_source_36;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
        tmp_assign_source_36 = tmp_tuple_unpack_3__element_2;
        {
            PyObject *old = var_cols;
            var_cols = tmp_assign_source_36;
            Py_INCREF( var_cols );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    {
        PyObject *tmp_assign_source_37;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_3 );
        tmp_assign_source_37 = tmp_tuple_unpack_3__element_3;
        {
            PyObject *old = var_num1;
            var_num1 = tmp_assign_source_37;
            Py_INCREF( var_num1 );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_3 );
    tmp_tuple_unpack_3__element_3 = NULL;

    {
        PyObject *tmp_assign_source_38;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_4 );
        tmp_assign_source_38 = tmp_tuple_unpack_3__element_4;
        {
            PyObject *old = var_num2;
            var_num2 = tmp_assign_source_38;
            Py_INCREF( var_num2 );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_4 );
    tmp_tuple_unpack_3__element_4 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_iter_arg_6;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( var_max_nrows );
        tmp_left_name_1 = var_max_nrows;
        CHECK_OBJECT( var_rows );
        tmp_right_name_1 = var_rows;
        tmp_iter_arg_6 = BUILTIN_DIVMOD( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_iter_arg_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_10;
        }
        tmp_assign_source_39 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_6 );
        Py_DECREF( tmp_iter_arg_6 );
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_10;
        }
        {
            PyObject *old = tmp_tuple_unpack_4__source_iter;
            tmp_tuple_unpack_4__source_iter = tmp_assign_source_39;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_unpack_11;
        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
        tmp_unpack_11 = tmp_tuple_unpack_4__source_iter;
        tmp_assign_source_40 = UNPACK_NEXT( tmp_unpack_11, 0, 2 );
        if ( tmp_assign_source_40 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 333;
            goto try_except_handler_11;
        }
        {
            PyObject *old = tmp_tuple_unpack_4__element_1;
            tmp_tuple_unpack_4__element_1 = tmp_assign_source_40;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_unpack_12;
        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
        tmp_unpack_12 = tmp_tuple_unpack_4__source_iter;
        tmp_assign_source_41 = UNPACK_NEXT( tmp_unpack_12, 1, 2 );
        if ( tmp_assign_source_41 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 333;
            goto try_except_handler_11;
        }
        {
            PyObject *old = tmp_tuple_unpack_4__element_2;
            tmp_tuple_unpack_4__element_2 = tmp_assign_source_41;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_4;
        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
        tmp_iterator_name_4 = tmp_tuple_unpack_4__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_4 ); assert( HAS_ITERNEXT( tmp_iterator_name_4 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_4 )->tp_iternext)( tmp_iterator_name_4 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    exception_lineno = 333;
                    goto try_except_handler_11;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 333;
            goto try_except_handler_11;
        }
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
    Py_DECREF( tmp_tuple_unpack_4__source_iter );
    tmp_tuple_unpack_4__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_10;
    // End of try:
    try_end_8:;
    goto try_end_9;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_4__element_1 );
    tmp_tuple_unpack_4__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_4__element_2 );
    tmp_tuple_unpack_4__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto try_except_handler_7;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
    Py_DECREF( tmp_tuple_unpack_4__source_iter );
    tmp_tuple_unpack_4__source_iter = NULL;

    {
        PyObject *tmp_assign_source_42;
        CHECK_OBJECT( tmp_tuple_unpack_4__element_1 );
        tmp_assign_source_42 = tmp_tuple_unpack_4__element_1;
        {
            PyObject *old = var_div_row;
            var_div_row = tmp_assign_source_42;
            Py_INCREF( var_div_row );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_4__element_1 );
    tmp_tuple_unpack_4__element_1 = NULL;

    {
        PyObject *tmp_assign_source_43;
        CHECK_OBJECT( tmp_tuple_unpack_4__element_2 );
        tmp_assign_source_43 = tmp_tuple_unpack_4__element_2;
        {
            PyObject *old = var_mod_row;
            var_mod_row = tmp_assign_source_43;
            Py_INCREF( var_mod_row );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_4__element_2 );
    tmp_tuple_unpack_4__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_iter_arg_7;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( var_max_ncols );
        tmp_left_name_2 = var_max_ncols;
        CHECK_OBJECT( var_cols );
        tmp_right_name_2 = var_cols;
        tmp_iter_arg_7 = BUILTIN_DIVMOD( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_iter_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_12;
        }
        tmp_assign_source_44 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_7 );
        Py_DECREF( tmp_iter_arg_7 );
        if ( tmp_assign_source_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_12;
        }
        {
            PyObject *old = tmp_tuple_unpack_5__source_iter;
            tmp_tuple_unpack_5__source_iter = tmp_assign_source_44;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_unpack_13;
        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
        tmp_unpack_13 = tmp_tuple_unpack_5__source_iter;
        tmp_assign_source_45 = UNPACK_NEXT( tmp_unpack_13, 0, 2 );
        if ( tmp_assign_source_45 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 334;
            goto try_except_handler_13;
        }
        {
            PyObject *old = tmp_tuple_unpack_5__element_1;
            tmp_tuple_unpack_5__element_1 = tmp_assign_source_45;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_unpack_14;
        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
        tmp_unpack_14 = tmp_tuple_unpack_5__source_iter;
        tmp_assign_source_46 = UNPACK_NEXT( tmp_unpack_14, 1, 2 );
        if ( tmp_assign_source_46 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 334;
            goto try_except_handler_13;
        }
        {
            PyObject *old = tmp_tuple_unpack_5__element_2;
            tmp_tuple_unpack_5__element_2 = tmp_assign_source_46;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_5;
        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
        tmp_iterator_name_5 = tmp_tuple_unpack_5__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_5 ); assert( HAS_ITERNEXT( tmp_iterator_name_5 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_5 )->tp_iternext)( tmp_iterator_name_5 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    exception_lineno = 334;
                    goto try_except_handler_13;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 334;
            goto try_except_handler_13;
        }
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
    Py_DECREF( tmp_tuple_unpack_5__source_iter );
    tmp_tuple_unpack_5__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto try_except_handler_12;
    // End of try:
    try_end_10:;
    goto try_end_11;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_5__element_1 );
    tmp_tuple_unpack_5__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_5__element_2 );
    tmp_tuple_unpack_5__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto try_except_handler_7;
    // End of try:
    try_end_11:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
    Py_DECREF( tmp_tuple_unpack_5__source_iter );
    tmp_tuple_unpack_5__source_iter = NULL;

    {
        PyObject *tmp_assign_source_47;
        CHECK_OBJECT( tmp_tuple_unpack_5__element_1 );
        tmp_assign_source_47 = tmp_tuple_unpack_5__element_1;
        {
            PyObject *old = var_div_col;
            var_div_col = tmp_assign_source_47;
            Py_INCREF( var_div_col );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_5__element_1 );
    tmp_tuple_unpack_5__element_1 = NULL;

    {
        PyObject *tmp_assign_source_48;
        CHECK_OBJECT( tmp_tuple_unpack_5__element_2 );
        tmp_assign_source_48 = tmp_tuple_unpack_5__element_2;
        {
            PyObject *old = var_mod_col;
            var_mod_col = tmp_assign_source_48;
            Py_INCREF( var_mod_col );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_5__element_2 );
    tmp_tuple_unpack_5__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        CHECK_OBJECT( var_mod_row );
        tmp_compexpr_left_4 = var_mod_row;
        tmp_compexpr_right_4 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 335;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_instance_10;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_call_result_7;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_cbook );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 336;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_7;
            }

            tmp_called_instance_10 = tmp_mvar_value_1;
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 336;
            tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain__warn_external, &PyTuple_GET_ITEM( const_tuple_str_digest_779b532b2e2c1b413b3dfeb01f657d05_tuple, 0 ) );

            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 336;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_7;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        tmp_return_value = PyDict_New();
        goto try_return_handler_7;
        branch_no_4:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        CHECK_OBJECT( var_mod_col );
        tmp_compexpr_left_5 = var_mod_col;
        tmp_compexpr_right_5 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 340;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_called_instance_11;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_8;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_cbook );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 341;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_7;
            }

            tmp_called_instance_11 = tmp_mvar_value_2;
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 341;
            tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_11, const_str_plain__warn_external, &PyTuple_GET_ITEM( const_tuple_str_digest_a55724e1f23ea7bee1b2bc604e3da396_tuple, 0 ) );

            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 341;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_7;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        tmp_return_value = PyDict_New();
        goto try_return_handler_7;
        branch_no_5:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_iter_arg_8;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        CHECK_OBJECT( var_num1 );
        tmp_left_name_3 = var_num1;
        CHECK_OBJECT( var_cols );
        tmp_right_name_3 = var_cols;
        tmp_iter_arg_8 = BUILTIN_DIVMOD( tmp_left_name_3, tmp_right_name_3 );
        if ( tmp_iter_arg_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_14;
        }
        tmp_assign_source_49 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_8 );
        Py_DECREF( tmp_iter_arg_8 );
        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_14;
        }
        {
            PyObject *old = tmp_tuple_unpack_6__source_iter;
            tmp_tuple_unpack_6__source_iter = tmp_assign_source_49;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_unpack_15;
        CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
        tmp_unpack_15 = tmp_tuple_unpack_6__source_iter;
        tmp_assign_source_50 = UNPACK_NEXT( tmp_unpack_15, 0, 2 );
        if ( tmp_assign_source_50 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 346;
            goto try_except_handler_15;
        }
        {
            PyObject *old = tmp_tuple_unpack_6__element_1;
            tmp_tuple_unpack_6__element_1 = tmp_assign_source_50;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_unpack_16;
        CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
        tmp_unpack_16 = tmp_tuple_unpack_6__source_iter;
        tmp_assign_source_51 = UNPACK_NEXT( tmp_unpack_16, 1, 2 );
        if ( tmp_assign_source_51 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 346;
            goto try_except_handler_15;
        }
        {
            PyObject *old = tmp_tuple_unpack_6__element_2;
            tmp_tuple_unpack_6__element_2 = tmp_assign_source_51;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_6;
        CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
        tmp_iterator_name_6 = tmp_tuple_unpack_6__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_6 ); assert( HAS_ITERNEXT( tmp_iterator_name_6 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_6 )->tp_iternext)( tmp_iterator_name_6 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    exception_lineno = 346;
                    goto try_except_handler_15;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            exception_lineno = 346;
            goto try_except_handler_15;
        }
    }
    goto try_end_12;
    // Exception handler code:
    try_except_handler_15:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_6__source_iter );
    Py_DECREF( tmp_tuple_unpack_6__source_iter );
    tmp_tuple_unpack_6__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto try_except_handler_14;
    // End of try:
    try_end_12:;
    goto try_end_13;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_6__element_1 );
    tmp_tuple_unpack_6__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_6__element_2 );
    tmp_tuple_unpack_6__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto try_except_handler_7;
    // End of try:
    try_end_13:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_6__source_iter );
    Py_DECREF( tmp_tuple_unpack_6__source_iter );
    tmp_tuple_unpack_6__source_iter = NULL;

    {
        PyObject *tmp_assign_source_52;
        CHECK_OBJECT( tmp_tuple_unpack_6__element_1 );
        tmp_assign_source_52 = tmp_tuple_unpack_6__element_1;
        {
            PyObject *old = var_rowNum1;
            var_rowNum1 = tmp_assign_source_52;
            Py_INCREF( var_rowNum1 );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_6__element_1 );
    tmp_tuple_unpack_6__element_1 = NULL;

    {
        PyObject *tmp_assign_source_53;
        CHECK_OBJECT( tmp_tuple_unpack_6__element_2 );
        tmp_assign_source_53 = tmp_tuple_unpack_6__element_2;
        {
            PyObject *old = var_colNum1;
            var_colNum1 = tmp_assign_source_53;
            Py_INCREF( var_colNum1 );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_6__element_2 );
    tmp_tuple_unpack_6__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        CHECK_OBJECT( var_num2 );
        tmp_compexpr_left_6 = var_num2;
        tmp_compexpr_right_6 = Py_None;
        tmp_condition_result_6 = ( tmp_compexpr_left_6 == tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_54;
            PyObject *tmp_iter_arg_9;
            PyObject *tmp_tuple_element_1;
            CHECK_OBJECT( var_rowNum1 );
            tmp_tuple_element_1 = var_rowNum1;
            tmp_iter_arg_9 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_iter_arg_9, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( var_colNum1 );
            tmp_tuple_element_1 = var_colNum1;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_iter_arg_9, 1, tmp_tuple_element_1 );
            tmp_assign_source_54 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_9 );
            Py_DECREF( tmp_iter_arg_9 );
            assert( !(tmp_assign_source_54 == NULL) );
            {
                PyObject *old = tmp_tuple_unpack_7__source_iter;
                tmp_tuple_unpack_7__source_iter = tmp_assign_source_54;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_55;
            PyObject *tmp_unpack_17;
            CHECK_OBJECT( tmp_tuple_unpack_7__source_iter );
            tmp_unpack_17 = tmp_tuple_unpack_7__source_iter;
            tmp_assign_source_55 = UNPACK_NEXT( tmp_unpack_17, 0, 2 );
            if ( tmp_assign_source_55 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 348;
                goto try_except_handler_17;
            }
            {
                PyObject *old = tmp_tuple_unpack_7__element_1;
                tmp_tuple_unpack_7__element_1 = tmp_assign_source_55;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_56;
            PyObject *tmp_unpack_18;
            CHECK_OBJECT( tmp_tuple_unpack_7__source_iter );
            tmp_unpack_18 = tmp_tuple_unpack_7__source_iter;
            tmp_assign_source_56 = UNPACK_NEXT( tmp_unpack_18, 1, 2 );
            if ( tmp_assign_source_56 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 348;
                goto try_except_handler_17;
            }
            {
                PyObject *old = tmp_tuple_unpack_7__element_2;
                tmp_tuple_unpack_7__element_2 = tmp_assign_source_56;
                Py_XDECREF( old );
            }

        }
        goto try_end_14;
        // Exception handler code:
        try_except_handler_17:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_7__source_iter );
        Py_DECREF( tmp_tuple_unpack_7__source_iter );
        tmp_tuple_unpack_7__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto try_except_handler_16;
        // End of try:
        try_end_14:;
        goto try_end_15;
        // Exception handler code:
        try_except_handler_16:;
        exception_keeper_type_15 = exception_type;
        exception_keeper_value_15 = exception_value;
        exception_keeper_tb_15 = exception_tb;
        exception_keeper_lineno_15 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_7__element_1 );
        tmp_tuple_unpack_7__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_7__element_2 );
        tmp_tuple_unpack_7__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_15;
        exception_value = exception_keeper_value_15;
        exception_tb = exception_keeper_tb_15;
        exception_lineno = exception_keeper_lineno_15;

        goto try_except_handler_7;
        // End of try:
        try_end_15:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_7__source_iter );
        Py_DECREF( tmp_tuple_unpack_7__source_iter );
        tmp_tuple_unpack_7__source_iter = NULL;

        {
            PyObject *tmp_assign_source_57;
            CHECK_OBJECT( tmp_tuple_unpack_7__element_1 );
            tmp_assign_source_57 = tmp_tuple_unpack_7__element_1;
            {
                PyObject *old = var_rowNum2;
                var_rowNum2 = tmp_assign_source_57;
                Py_INCREF( var_rowNum2 );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_7__element_1 );
        tmp_tuple_unpack_7__element_1 = NULL;

        {
            PyObject *tmp_assign_source_58;
            CHECK_OBJECT( tmp_tuple_unpack_7__element_2 );
            tmp_assign_source_58 = tmp_tuple_unpack_7__element_2;
            {
                PyObject *old = var_colNum2;
                var_colNum2 = tmp_assign_source_58;
                Py_INCREF( var_colNum2 );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_7__element_2 );
        tmp_tuple_unpack_7__element_2 = NULL;

        goto branch_end_6;
        branch_no_6:;
        // Tried code:
        {
            PyObject *tmp_assign_source_59;
            PyObject *tmp_iter_arg_10;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            CHECK_OBJECT( var_num2 );
            tmp_left_name_4 = var_num2;
            CHECK_OBJECT( var_cols );
            tmp_right_name_4 = var_cols;
            tmp_iter_arg_10 = BUILTIN_DIVMOD( tmp_left_name_4, tmp_right_name_4 );
            if ( tmp_iter_arg_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 350;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }
            tmp_assign_source_59 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_10 );
            Py_DECREF( tmp_iter_arg_10 );
            if ( tmp_assign_source_59 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 350;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }
            {
                PyObject *old = tmp_tuple_unpack_8__source_iter;
                tmp_tuple_unpack_8__source_iter = tmp_assign_source_59;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_60;
            PyObject *tmp_unpack_19;
            CHECK_OBJECT( tmp_tuple_unpack_8__source_iter );
            tmp_unpack_19 = tmp_tuple_unpack_8__source_iter;
            tmp_assign_source_60 = UNPACK_NEXT( tmp_unpack_19, 0, 2 );
            if ( tmp_assign_source_60 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 350;
                goto try_except_handler_19;
            }
            {
                PyObject *old = tmp_tuple_unpack_8__element_1;
                tmp_tuple_unpack_8__element_1 = tmp_assign_source_60;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_61;
            PyObject *tmp_unpack_20;
            CHECK_OBJECT( tmp_tuple_unpack_8__source_iter );
            tmp_unpack_20 = tmp_tuple_unpack_8__source_iter;
            tmp_assign_source_61 = UNPACK_NEXT( tmp_unpack_20, 1, 2 );
            if ( tmp_assign_source_61 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 350;
                goto try_except_handler_19;
            }
            {
                PyObject *old = tmp_tuple_unpack_8__element_2;
                tmp_tuple_unpack_8__element_2 = tmp_assign_source_61;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_7;
            CHECK_OBJECT( tmp_tuple_unpack_8__source_iter );
            tmp_iterator_name_7 = tmp_tuple_unpack_8__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_7 ); assert( HAS_ITERNEXT( tmp_iterator_name_7 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_7 )->tp_iternext)( tmp_iterator_name_7 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                        exception_lineno = 350;
                        goto try_except_handler_19;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 350;
                goto try_except_handler_19;
            }
        }
        goto try_end_16;
        // Exception handler code:
        try_except_handler_19:;
        exception_keeper_type_16 = exception_type;
        exception_keeper_value_16 = exception_value;
        exception_keeper_tb_16 = exception_tb;
        exception_keeper_lineno_16 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_8__source_iter );
        Py_DECREF( tmp_tuple_unpack_8__source_iter );
        tmp_tuple_unpack_8__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_16;
        exception_value = exception_keeper_value_16;
        exception_tb = exception_keeper_tb_16;
        exception_lineno = exception_keeper_lineno_16;

        goto try_except_handler_18;
        // End of try:
        try_end_16:;
        goto try_end_17;
        // Exception handler code:
        try_except_handler_18:;
        exception_keeper_type_17 = exception_type;
        exception_keeper_value_17 = exception_value;
        exception_keeper_tb_17 = exception_tb;
        exception_keeper_lineno_17 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_8__element_1 );
        tmp_tuple_unpack_8__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_8__element_2 );
        tmp_tuple_unpack_8__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_17;
        exception_value = exception_keeper_value_17;
        exception_tb = exception_keeper_tb_17;
        exception_lineno = exception_keeper_lineno_17;

        goto try_except_handler_7;
        // End of try:
        try_end_17:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_8__source_iter );
        Py_DECREF( tmp_tuple_unpack_8__source_iter );
        tmp_tuple_unpack_8__source_iter = NULL;

        {
            PyObject *tmp_assign_source_62;
            CHECK_OBJECT( tmp_tuple_unpack_8__element_1 );
            tmp_assign_source_62 = tmp_tuple_unpack_8__element_1;
            {
                PyObject *old = var_rowNum2;
                var_rowNum2 = tmp_assign_source_62;
                Py_INCREF( var_rowNum2 );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_8__element_1 );
        tmp_tuple_unpack_8__element_1 = NULL;

        {
            PyObject *tmp_assign_source_63;
            CHECK_OBJECT( tmp_tuple_unpack_8__element_2 );
            tmp_assign_source_63 = tmp_tuple_unpack_8__element_2;
            {
                PyObject *old = var_colNum2;
                var_colNum2 = tmp_assign_source_63;
                Py_INCREF( var_colNum2 );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_8__element_2 );
        tmp_tuple_unpack_8__element_2 = NULL;

        branch_end_6:;
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_9;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_left_name_5;
        PyObject *tmp_left_name_6;
        PyObject *tmp_left_name_7;
        PyObject *tmp_right_name_5;
        PyObject *tmp_right_name_6;
        PyObject *tmp_right_name_7;
        PyObject *tmp_left_name_8;
        PyObject *tmp_right_name_8;
        PyObject *tmp_left_name_9;
        PyObject *tmp_left_name_10;
        PyObject *tmp_left_name_11;
        PyObject *tmp_left_name_12;
        PyObject *tmp_left_name_13;
        PyObject *tmp_left_name_14;
        PyObject *tmp_right_name_9;
        PyObject *tmp_right_name_10;
        PyObject *tmp_right_name_11;
        PyObject *tmp_right_name_12;
        PyObject *tmp_right_name_13;
        PyObject *tmp_left_name_15;
        PyObject *tmp_left_name_16;
        PyObject *tmp_right_name_14;
        PyObject *tmp_right_name_15;
        PyObject *tmp_right_name_16;
        CHECK_OBJECT( var_num1num2_list );
        tmp_source_name_2 = var_num1num2_list;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 352;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        CHECK_OBJECT( var_rowNum1 );
        tmp_left_name_7 = var_rowNum1;
        CHECK_OBJECT( var_div_row );
        tmp_right_name_5 = var_div_row;
        tmp_left_name_6 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_5 );
        if ( tmp_left_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 352;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        CHECK_OBJECT( var_max_ncols );
        tmp_right_name_6 = var_max_ncols;
        tmp_left_name_5 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_6 );
        Py_DECREF( tmp_left_name_6 );
        if ( tmp_left_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 352;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        CHECK_OBJECT( var_colNum1 );
        tmp_left_name_8 = var_colNum1;
        CHECK_OBJECT( var_div_col );
        tmp_right_name_8 = var_div_col;
        tmp_right_name_7 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_8, tmp_right_name_8 );
        if ( tmp_right_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_left_name_5 );

            exception_lineno = 353;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        tmp_tuple_element_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_7 );
        Py_DECREF( tmp_left_name_5 );
        Py_DECREF( tmp_right_name_7 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 352;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        tmp_args_element_name_14 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_args_element_name_14, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( var_rowNum2 );
        tmp_left_name_14 = var_rowNum2;
        tmp_right_name_9 = const_int_pos_1;
        tmp_left_name_13 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_14, tmp_right_name_9 );
        if ( tmp_left_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_14 );

            exception_lineno = 354;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        CHECK_OBJECT( var_div_row );
        tmp_right_name_10 = var_div_row;
        tmp_left_name_12 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_13, tmp_right_name_10 );
        Py_DECREF( tmp_left_name_13 );
        if ( tmp_left_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_14 );

            exception_lineno = 354;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        tmp_right_name_11 = const_int_pos_1;
        tmp_left_name_11 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_12, tmp_right_name_11 );
        Py_DECREF( tmp_left_name_12 );
        if ( tmp_left_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_14 );

            exception_lineno = 354;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        CHECK_OBJECT( var_max_ncols );
        tmp_right_name_12 = var_max_ncols;
        tmp_left_name_10 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_11, tmp_right_name_12 );
        Py_DECREF( tmp_left_name_11 );
        if ( tmp_left_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_14 );

            exception_lineno = 354;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        CHECK_OBJECT( var_colNum2 );
        tmp_left_name_16 = var_colNum2;
        tmp_right_name_14 = const_int_pos_1;
        tmp_left_name_15 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_16, tmp_right_name_14 );
        if ( tmp_left_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_14 );
            Py_DECREF( tmp_left_name_10 );

            exception_lineno = 355;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        CHECK_OBJECT( var_div_col );
        tmp_right_name_15 = var_div_col;
        tmp_right_name_13 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_15, tmp_right_name_15 );
        Py_DECREF( tmp_left_name_15 );
        if ( tmp_right_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_14 );
            Py_DECREF( tmp_left_name_10 );

            exception_lineno = 355;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        tmp_left_name_9 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_10, tmp_right_name_13 );
        Py_DECREF( tmp_left_name_10 );
        Py_DECREF( tmp_right_name_13 );
        if ( tmp_left_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_14 );

            exception_lineno = 354;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        tmp_right_name_16 = const_int_pos_1;
        tmp_tuple_element_2 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_9, tmp_right_name_16 );
        Py_DECREF( tmp_left_name_9 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_14 );

            exception_lineno = 355;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        PyTuple_SET_ITEM( tmp_args_element_name_14, 1, tmp_tuple_element_2 );
        frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 352;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_call_result_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_call_result_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 352;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        Py_DECREF( tmp_call_result_9 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 331;
        type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
        goto try_except_handler_7;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_18;
    // Return handler code:
    try_return_handler_7:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__iter_value );
    Py_DECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_18 = exception_type;
    exception_keeper_value_18 = exception_value;
    exception_keeper_tb_18 = exception_tb;
    exception_keeper_lineno_18 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_18;
    exception_value = exception_keeper_value_18;
    exception_tb = exception_keeper_tb_18;
    exception_lineno = exception_keeper_lineno_18;

    goto frame_exception_exit_1;
    // End of try:
    try_end_18:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_64;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_auto_adjust_subplotpars );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_adjust_subplotpars );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_adjust_subplotpars" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 357;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_3;
        CHECK_OBJECT( par_fig );
        tmp_tuple_element_3 = par_fig;
        tmp_args_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_3 );
        CHECK_OBJECT( par_renderer );
        tmp_tuple_element_3 = par_renderer;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_3 );
        tmp_dict_key_1 = const_str_plain_nrows_ncols;
        CHECK_OBJECT( var_max_nrows );
        tmp_tuple_element_4 = var_max_nrows;
        tmp_dict_value_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_dict_value_1, 0, tmp_tuple_element_4 );
        CHECK_OBJECT( var_max_ncols );
        tmp_tuple_element_4 = var_max_ncols;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_dict_value_1, 1, tmp_tuple_element_4 );
        tmp_kw_name_1 = _PyDict_NewPresized( 7 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_num1num2_list;
        CHECK_OBJECT( var_num1num2_list );
        tmp_dict_value_2 = var_num1num2_list;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_subplot_list;
        CHECK_OBJECT( var_subplot_list );
        tmp_dict_value_3 = var_subplot_list;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_ax_bbox_list;
        CHECK_OBJECT( var_ax_bbox_list );
        tmp_dict_value_4 = var_ax_bbox_list;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_pad;
        CHECK_OBJECT( par_pad );
        tmp_dict_value_5 = par_pad;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_h_pad;
        CHECK_OBJECT( par_h_pad );
        tmp_dict_value_6 = par_h_pad;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_w_pad;
        CHECK_OBJECT( par_w_pad );
        tmp_dict_value_7 = par_w_pad;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 357;
        tmp_assign_source_64 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_kwargs == NULL );
        var_kwargs = tmp_assign_source_64;
    }
    {
        nuitka_bool tmp_condition_result_7;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        PyObject *tmp_compexpr_left_8;
        PyObject *tmp_compexpr_right_8;
        CHECK_OBJECT( par_rect );
        tmp_compexpr_left_7 = par_rect;
        tmp_compexpr_right_7 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_7 != tmp_compexpr_right_7 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_kwargs );
        tmp_compexpr_left_8 = var_kwargs;
        tmp_compexpr_right_8 = Py_None;
        tmp_and_right_value_1 = ( tmp_compexpr_left_8 != tmp_compexpr_right_8 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_7 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_7 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        // Tried code:
        {
            PyObject *tmp_assign_source_65;
            PyObject *tmp_iter_arg_11;
            CHECK_OBJECT( par_rect );
            tmp_iter_arg_11 = par_rect;
            tmp_assign_source_65 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_11 );
            if ( tmp_assign_source_65 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto try_except_handler_20;
            }
            assert( tmp_tuple_unpack_9__source_iter == NULL );
            tmp_tuple_unpack_9__source_iter = tmp_assign_source_65;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_66;
            PyObject *tmp_unpack_21;
            CHECK_OBJECT( tmp_tuple_unpack_9__source_iter );
            tmp_unpack_21 = tmp_tuple_unpack_9__source_iter;
            tmp_assign_source_66 = UNPACK_NEXT( tmp_unpack_21, 0, 4 );
            if ( tmp_assign_source_66 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 374;
                goto try_except_handler_21;
            }
            assert( tmp_tuple_unpack_9__element_1 == NULL );
            tmp_tuple_unpack_9__element_1 = tmp_assign_source_66;
        }
        {
            PyObject *tmp_assign_source_67;
            PyObject *tmp_unpack_22;
            CHECK_OBJECT( tmp_tuple_unpack_9__source_iter );
            tmp_unpack_22 = tmp_tuple_unpack_9__source_iter;
            tmp_assign_source_67 = UNPACK_NEXT( tmp_unpack_22, 1, 4 );
            if ( tmp_assign_source_67 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 374;
                goto try_except_handler_21;
            }
            assert( tmp_tuple_unpack_9__element_2 == NULL );
            tmp_tuple_unpack_9__element_2 = tmp_assign_source_67;
        }
        {
            PyObject *tmp_assign_source_68;
            PyObject *tmp_unpack_23;
            CHECK_OBJECT( tmp_tuple_unpack_9__source_iter );
            tmp_unpack_23 = tmp_tuple_unpack_9__source_iter;
            tmp_assign_source_68 = UNPACK_NEXT( tmp_unpack_23, 2, 4 );
            if ( tmp_assign_source_68 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 374;
                goto try_except_handler_21;
            }
            assert( tmp_tuple_unpack_9__element_3 == NULL );
            tmp_tuple_unpack_9__element_3 = tmp_assign_source_68;
        }
        {
            PyObject *tmp_assign_source_69;
            PyObject *tmp_unpack_24;
            CHECK_OBJECT( tmp_tuple_unpack_9__source_iter );
            tmp_unpack_24 = tmp_tuple_unpack_9__source_iter;
            tmp_assign_source_69 = UNPACK_NEXT( tmp_unpack_24, 3, 4 );
            if ( tmp_assign_source_69 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 374;
                goto try_except_handler_21;
            }
            assert( tmp_tuple_unpack_9__element_4 == NULL );
            tmp_tuple_unpack_9__element_4 = tmp_assign_source_69;
        }
        {
            PyObject *tmp_iterator_name_8;
            CHECK_OBJECT( tmp_tuple_unpack_9__source_iter );
            tmp_iterator_name_8 = tmp_tuple_unpack_9__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_8 ); assert( HAS_ITERNEXT( tmp_iterator_name_8 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_8 )->tp_iternext)( tmp_iterator_name_8 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                        exception_lineno = 374;
                        goto try_except_handler_21;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 4)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                exception_lineno = 374;
                goto try_except_handler_21;
            }
        }
        goto try_end_19;
        // Exception handler code:
        try_except_handler_21:;
        exception_keeper_type_19 = exception_type;
        exception_keeper_value_19 = exception_value;
        exception_keeper_tb_19 = exception_tb;
        exception_keeper_lineno_19 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_9__source_iter );
        Py_DECREF( tmp_tuple_unpack_9__source_iter );
        tmp_tuple_unpack_9__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_19;
        exception_value = exception_keeper_value_19;
        exception_tb = exception_keeper_tb_19;
        exception_lineno = exception_keeper_lineno_19;

        goto try_except_handler_20;
        // End of try:
        try_end_19:;
        goto try_end_20;
        // Exception handler code:
        try_except_handler_20:;
        exception_keeper_type_20 = exception_type;
        exception_keeper_value_20 = exception_value;
        exception_keeper_tb_20 = exception_tb;
        exception_keeper_lineno_20 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_9__element_1 );
        tmp_tuple_unpack_9__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_9__element_2 );
        tmp_tuple_unpack_9__element_2 = NULL;

        Py_XDECREF( tmp_tuple_unpack_9__element_3 );
        tmp_tuple_unpack_9__element_3 = NULL;

        Py_XDECREF( tmp_tuple_unpack_9__element_4 );
        tmp_tuple_unpack_9__element_4 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_20;
        exception_value = exception_keeper_value_20;
        exception_tb = exception_keeper_tb_20;
        exception_lineno = exception_keeper_lineno_20;

        goto frame_exception_exit_1;
        // End of try:
        try_end_20:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_9__source_iter );
        Py_DECREF( tmp_tuple_unpack_9__source_iter );
        tmp_tuple_unpack_9__source_iter = NULL;

        {
            PyObject *tmp_assign_source_70;
            CHECK_OBJECT( tmp_tuple_unpack_9__element_1 );
            tmp_assign_source_70 = tmp_tuple_unpack_9__element_1;
            assert( var_left == NULL );
            Py_INCREF( tmp_assign_source_70 );
            var_left = tmp_assign_source_70;
        }
        Py_XDECREF( tmp_tuple_unpack_9__element_1 );
        tmp_tuple_unpack_9__element_1 = NULL;

        {
            PyObject *tmp_assign_source_71;
            CHECK_OBJECT( tmp_tuple_unpack_9__element_2 );
            tmp_assign_source_71 = tmp_tuple_unpack_9__element_2;
            assert( var_bottom == NULL );
            Py_INCREF( tmp_assign_source_71 );
            var_bottom = tmp_assign_source_71;
        }
        Py_XDECREF( tmp_tuple_unpack_9__element_2 );
        tmp_tuple_unpack_9__element_2 = NULL;

        {
            PyObject *tmp_assign_source_72;
            CHECK_OBJECT( tmp_tuple_unpack_9__element_3 );
            tmp_assign_source_72 = tmp_tuple_unpack_9__element_3;
            assert( var_right == NULL );
            Py_INCREF( tmp_assign_source_72 );
            var_right = tmp_assign_source_72;
        }
        Py_XDECREF( tmp_tuple_unpack_9__element_3 );
        tmp_tuple_unpack_9__element_3 = NULL;

        {
            PyObject *tmp_assign_source_73;
            CHECK_OBJECT( tmp_tuple_unpack_9__element_4 );
            tmp_assign_source_73 = tmp_tuple_unpack_9__element_4;
            assert( var_top == NULL );
            Py_INCREF( tmp_assign_source_73 );
            var_top = tmp_assign_source_73;
        }
        Py_XDECREF( tmp_tuple_unpack_9__element_4 );
        tmp_tuple_unpack_9__element_4 = NULL;

        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_9;
            PyObject *tmp_compexpr_right_9;
            CHECK_OBJECT( var_left );
            tmp_compexpr_left_9 = var_left;
            tmp_compexpr_right_9 = Py_None;
            tmp_condition_result_8 = ( tmp_compexpr_left_9 != tmp_compexpr_right_9 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_assign_source_74;
                PyObject *tmp_left_name_17;
                PyObject *tmp_right_name_17;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( var_left );
                tmp_left_name_17 = var_left;
                CHECK_OBJECT( var_kwargs );
                tmp_subscribed_name_1 = var_kwargs;
                tmp_subscript_name_1 = const_str_plain_left;
                tmp_right_name_17 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                if ( tmp_right_name_17 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 376;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_17, tmp_right_name_17 );
                Py_DECREF( tmp_right_name_17 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 376;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_74 = tmp_left_name_17;
                var_left = tmp_assign_source_74;

            }
            branch_no_8:;
        }
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_10;
            PyObject *tmp_compexpr_right_10;
            CHECK_OBJECT( var_bottom );
            tmp_compexpr_left_10 = var_bottom;
            tmp_compexpr_right_10 = Py_None;
            tmp_condition_result_9 = ( tmp_compexpr_left_10 != tmp_compexpr_right_10 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                PyObject *tmp_assign_source_75;
                PyObject *tmp_left_name_18;
                PyObject *tmp_right_name_18;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                CHECK_OBJECT( var_bottom );
                tmp_left_name_18 = var_bottom;
                CHECK_OBJECT( var_kwargs );
                tmp_subscribed_name_2 = var_kwargs;
                tmp_subscript_name_2 = const_str_plain_bottom;
                tmp_right_name_18 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                if ( tmp_right_name_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 378;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_18, tmp_right_name_18 );
                Py_DECREF( tmp_right_name_18 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 378;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_75 = tmp_left_name_18;
                var_bottom = tmp_assign_source_75;

            }
            branch_no_9:;
        }
        {
            nuitka_bool tmp_condition_result_10;
            PyObject *tmp_compexpr_left_11;
            PyObject *tmp_compexpr_right_11;
            CHECK_OBJECT( var_right );
            tmp_compexpr_left_11 = var_right;
            tmp_compexpr_right_11 = Py_None;
            tmp_condition_result_10 = ( tmp_compexpr_left_11 != tmp_compexpr_right_11 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_10;
            }
            else
            {
                goto branch_no_10;
            }
            branch_yes_10:;
            {
                PyObject *tmp_assign_source_76;
                PyObject *tmp_left_name_19;
                PyObject *tmp_right_name_19;
                PyObject *tmp_left_name_20;
                PyObject *tmp_right_name_20;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_subscript_name_3;
                CHECK_OBJECT( var_right );
                tmp_left_name_19 = var_right;
                tmp_left_name_20 = const_int_pos_1;
                CHECK_OBJECT( var_kwargs );
                tmp_subscribed_name_3 = var_kwargs;
                tmp_subscript_name_3 = const_str_plain_right;
                tmp_right_name_20 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
                if ( tmp_right_name_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 380;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_19 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_20, tmp_right_name_20 );
                Py_DECREF( tmp_right_name_20 );
                if ( tmp_right_name_19 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 380;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceSubtract, &tmp_left_name_19, tmp_right_name_19 );
                Py_DECREF( tmp_right_name_19 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 380;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_76 = tmp_left_name_19;
                var_right = tmp_assign_source_76;

            }
            branch_no_10:;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_compexpr_left_12;
            PyObject *tmp_compexpr_right_12;
            CHECK_OBJECT( var_top );
            tmp_compexpr_left_12 = var_top;
            tmp_compexpr_right_12 = Py_None;
            tmp_condition_result_11 = ( tmp_compexpr_left_12 != tmp_compexpr_right_12 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_assign_source_77;
                PyObject *tmp_left_name_21;
                PyObject *tmp_right_name_21;
                PyObject *tmp_left_name_22;
                PyObject *tmp_right_name_22;
                PyObject *tmp_subscribed_name_4;
                PyObject *tmp_subscript_name_4;
                CHECK_OBJECT( var_top );
                tmp_left_name_21 = var_top;
                tmp_left_name_22 = const_int_pos_1;
                CHECK_OBJECT( var_kwargs );
                tmp_subscribed_name_4 = var_kwargs;
                tmp_subscript_name_4 = const_str_plain_top;
                tmp_right_name_22 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
                if ( tmp_right_name_22 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 382;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_21 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_22, tmp_right_name_22 );
                Py_DECREF( tmp_right_name_22 );
                if ( tmp_right_name_21 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 382;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceSubtract, &tmp_left_name_21, tmp_right_name_21 );
                Py_DECREF( tmp_right_name_21 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 382;
                    type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_77 = tmp_left_name_21;
                var_top = tmp_assign_source_77;

            }
            branch_no_11:;
        }
        {
            PyObject *tmp_assign_source_78;
            PyObject *tmp_called_name_7;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_8;
            PyObject *tmp_dict_value_8;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_dict_key_9;
            PyObject *tmp_dict_value_9;
            PyObject *tmp_dict_key_10;
            PyObject *tmp_dict_value_10;
            PyObject *tmp_dict_key_11;
            PyObject *tmp_dict_value_11;
            PyObject *tmp_dict_key_12;
            PyObject *tmp_dict_value_12;
            PyObject *tmp_dict_key_13;
            PyObject *tmp_dict_value_13;
            PyObject *tmp_dict_key_14;
            PyObject *tmp_dict_value_14;
            PyObject *tmp_dict_key_15;
            PyObject *tmp_dict_value_15;
            PyObject *tmp_tuple_element_7;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_auto_adjust_subplotpars );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_adjust_subplotpars );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_adjust_subplotpars" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 384;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_7 = tmp_mvar_value_4;
            CHECK_OBJECT( par_fig );
            tmp_tuple_element_5 = par_fig;
            tmp_args_name_2 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_5 );
            CHECK_OBJECT( par_renderer );
            tmp_tuple_element_5 = par_renderer;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_5 );
            tmp_dict_key_8 = const_str_plain_nrows_ncols;
            CHECK_OBJECT( var_max_nrows );
            tmp_tuple_element_6 = var_max_nrows;
            tmp_dict_value_8 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_dict_value_8, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( var_max_ncols );
            tmp_tuple_element_6 = var_max_ncols;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_dict_value_8, 1, tmp_tuple_element_6 );
            tmp_kw_name_2 = _PyDict_NewPresized( 8 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_8, tmp_dict_value_8 );
            Py_DECREF( tmp_dict_value_8 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_9 = const_str_plain_num1num2_list;
            CHECK_OBJECT( var_num1num2_list );
            tmp_dict_value_9 = var_num1num2_list;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_9, tmp_dict_value_9 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_10 = const_str_plain_subplot_list;
            CHECK_OBJECT( var_subplot_list );
            tmp_dict_value_10 = var_subplot_list;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_10, tmp_dict_value_10 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_11 = const_str_plain_ax_bbox_list;
            CHECK_OBJECT( var_ax_bbox_list );
            tmp_dict_value_11 = var_ax_bbox_list;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_11, tmp_dict_value_11 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_12 = const_str_plain_pad;
            CHECK_OBJECT( par_pad );
            tmp_dict_value_12 = par_pad;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_12, tmp_dict_value_12 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_13 = const_str_plain_h_pad;
            CHECK_OBJECT( par_h_pad );
            tmp_dict_value_13 = par_h_pad;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_13, tmp_dict_value_13 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_14 = const_str_plain_w_pad;
            CHECK_OBJECT( par_w_pad );
            tmp_dict_value_14 = par_w_pad;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_14, tmp_dict_value_14 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_15 = const_str_plain_rect;
            CHECK_OBJECT( var_left );
            tmp_tuple_element_7 = var_left;
            tmp_dict_value_15 = PyTuple_New( 4 );
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_dict_value_15, 0, tmp_tuple_element_7 );
            CHECK_OBJECT( var_bottom );
            tmp_tuple_element_7 = var_bottom;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_dict_value_15, 1, tmp_tuple_element_7 );
            CHECK_OBJECT( var_right );
            tmp_tuple_element_7 = var_right;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_dict_value_15, 2, tmp_tuple_element_7 );
            CHECK_OBJECT( var_top );
            tmp_tuple_element_7 = var_top;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_dict_value_15, 3, tmp_tuple_element_7 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_15, tmp_dict_value_15 );
            Py_DECREF( tmp_dict_value_15 );
            assert( !(tmp_res != 0) );
            frame_929741756295e159ad639a088117b506->m_frame.f_lineno = 384;
            tmp_assign_source_78 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_assign_source_78 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 384;
                type_description_1 = "oooooooooooooooooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_kwargs;
                assert( old != NULL );
                var_kwargs = tmp_assign_source_78;
                Py_DECREF( old );
            }

        }
        branch_no_7:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_929741756295e159ad639a088117b506 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_929741756295e159ad639a088117b506 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_929741756295e159ad639a088117b506 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_929741756295e159ad639a088117b506, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_929741756295e159ad639a088117b506->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_929741756295e159ad639a088117b506, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_929741756295e159ad639a088117b506,
        type_description_1,
        par_fig,
        par_axes_list,
        par_subplotspec_list,
        par_renderer,
        par_pad,
        par_h_pad,
        par_w_pad,
        par_rect,
        var_subplot_list,
        var_nrows_list,
        var_ncols_list,
        var_ax_bbox_list,
        var_subplot_dict,
        var_subplotspec_list2,
        var_ax,
        var_subplotspec,
        var_subplots,
        var_myrows,
        var_mycols,
        var__,
        var_max_nrows,
        var_max_ncols,
        var_num1num2_list,
        var_rows,
        var_cols,
        var_num1,
        var_num2,
        var_div_row,
        var_mod_row,
        var_div_col,
        var_mod_col,
        var_rowNum1,
        var_colNum1,
        var_rowNum2,
        var_colNum2,
        var_kwargs,
        var_left,
        var_bottom,
        var_right,
        var_top
    );


    // Release cached frame.
    if ( frame_929741756295e159ad639a088117b506 == cache_frame_929741756295e159ad639a088117b506 )
    {
        Py_DECREF( frame_929741756295e159ad639a088117b506 );
    }
    cache_frame_929741756295e159ad639a088117b506 = NULL;

    assertFrameObject( frame_929741756295e159ad639a088117b506 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_kwargs );
    tmp_return_value = var_kwargs;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_8_get_tight_layout_figure );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_list );
    Py_DECREF( par_axes_list );
    par_axes_list = NULL;

    CHECK_OBJECT( (PyObject *)par_subplotspec_list );
    Py_DECREF( par_subplotspec_list );
    par_subplotspec_list = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    CHECK_OBJECT( (PyObject *)par_pad );
    Py_DECREF( par_pad );
    par_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_h_pad );
    Py_DECREF( par_h_pad );
    par_h_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_w_pad );
    Py_DECREF( par_w_pad );
    par_w_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_rect );
    Py_DECREF( par_rect );
    par_rect = NULL;

    CHECK_OBJECT( (PyObject *)var_subplot_list );
    Py_DECREF( var_subplot_list );
    var_subplot_list = NULL;

    CHECK_OBJECT( (PyObject *)var_nrows_list );
    Py_DECREF( var_nrows_list );
    var_nrows_list = NULL;

    CHECK_OBJECT( (PyObject *)var_ncols_list );
    Py_DECREF( var_ncols_list );
    var_ncols_list = NULL;

    CHECK_OBJECT( (PyObject *)var_ax_bbox_list );
    Py_DECREF( var_ax_bbox_list );
    var_ax_bbox_list = NULL;

    CHECK_OBJECT( (PyObject *)var_subplot_dict );
    Py_DECREF( var_subplot_dict );
    var_subplot_dict = NULL;

    CHECK_OBJECT( (PyObject *)var_subplotspec_list2 );
    Py_DECREF( var_subplotspec_list2 );
    var_subplotspec_list2 = NULL;

    Py_XDECREF( var_ax );
    var_ax = NULL;

    Py_XDECREF( var_subplotspec );
    var_subplotspec = NULL;

    Py_XDECREF( var_subplots );
    var_subplots = NULL;

    Py_XDECREF( var_myrows );
    var_myrows = NULL;

    Py_XDECREF( var_mycols );
    var_mycols = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_max_nrows );
    var_max_nrows = NULL;

    Py_XDECREF( var_max_ncols );
    var_max_ncols = NULL;

    Py_XDECREF( var_num1num2_list );
    var_num1num2_list = NULL;

    Py_XDECREF( var_rows );
    var_rows = NULL;

    Py_XDECREF( var_cols );
    var_cols = NULL;

    Py_XDECREF( var_num1 );
    var_num1 = NULL;

    Py_XDECREF( var_num2 );
    var_num2 = NULL;

    Py_XDECREF( var_div_row );
    var_div_row = NULL;

    Py_XDECREF( var_mod_row );
    var_mod_row = NULL;

    Py_XDECREF( var_div_col );
    var_div_col = NULL;

    Py_XDECREF( var_mod_col );
    var_mod_col = NULL;

    Py_XDECREF( var_rowNum1 );
    var_rowNum1 = NULL;

    Py_XDECREF( var_colNum1 );
    var_colNum1 = NULL;

    Py_XDECREF( var_rowNum2 );
    var_rowNum2 = NULL;

    Py_XDECREF( var_colNum2 );
    var_colNum2 = NULL;

    Py_XDECREF( var_kwargs );
    var_kwargs = NULL;

    Py_XDECREF( var_left );
    var_left = NULL;

    Py_XDECREF( var_bottom );
    var_bottom = NULL;

    Py_XDECREF( var_right );
    var_right = NULL;

    Py_XDECREF( var_top );
    var_top = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_21 = exception_type;
    exception_keeper_value_21 = exception_value;
    exception_keeper_tb_21 = exception_tb;
    exception_keeper_lineno_21 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    CHECK_OBJECT( (PyObject *)par_axes_list );
    Py_DECREF( par_axes_list );
    par_axes_list = NULL;

    CHECK_OBJECT( (PyObject *)par_subplotspec_list );
    Py_DECREF( par_subplotspec_list );
    par_subplotspec_list = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    CHECK_OBJECT( (PyObject *)par_pad );
    Py_DECREF( par_pad );
    par_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_h_pad );
    Py_DECREF( par_h_pad );
    par_h_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_w_pad );
    Py_DECREF( par_w_pad );
    par_w_pad = NULL;

    CHECK_OBJECT( (PyObject *)par_rect );
    Py_DECREF( par_rect );
    par_rect = NULL;

    CHECK_OBJECT( (PyObject *)var_subplot_list );
    Py_DECREF( var_subplot_list );
    var_subplot_list = NULL;

    CHECK_OBJECT( (PyObject *)var_nrows_list );
    Py_DECREF( var_nrows_list );
    var_nrows_list = NULL;

    CHECK_OBJECT( (PyObject *)var_ncols_list );
    Py_DECREF( var_ncols_list );
    var_ncols_list = NULL;

    CHECK_OBJECT( (PyObject *)var_ax_bbox_list );
    Py_DECREF( var_ax_bbox_list );
    var_ax_bbox_list = NULL;

    CHECK_OBJECT( (PyObject *)var_subplot_dict );
    Py_DECREF( var_subplot_dict );
    var_subplot_dict = NULL;

    CHECK_OBJECT( (PyObject *)var_subplotspec_list2 );
    Py_DECREF( var_subplotspec_list2 );
    var_subplotspec_list2 = NULL;

    Py_XDECREF( var_ax );
    var_ax = NULL;

    Py_XDECREF( var_subplotspec );
    var_subplotspec = NULL;

    Py_XDECREF( var_subplots );
    var_subplots = NULL;

    Py_XDECREF( var_myrows );
    var_myrows = NULL;

    Py_XDECREF( var_mycols );
    var_mycols = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_max_nrows );
    var_max_nrows = NULL;

    Py_XDECREF( var_max_ncols );
    var_max_ncols = NULL;

    Py_XDECREF( var_num1num2_list );
    var_num1num2_list = NULL;

    Py_XDECREF( var_rows );
    var_rows = NULL;

    Py_XDECREF( var_cols );
    var_cols = NULL;

    Py_XDECREF( var_num1 );
    var_num1 = NULL;

    Py_XDECREF( var_num2 );
    var_num2 = NULL;

    Py_XDECREF( var_div_row );
    var_div_row = NULL;

    Py_XDECREF( var_mod_row );
    var_mod_row = NULL;

    Py_XDECREF( var_div_col );
    var_div_col = NULL;

    Py_XDECREF( var_mod_col );
    var_mod_col = NULL;

    Py_XDECREF( var_rowNum1 );
    var_rowNum1 = NULL;

    Py_XDECREF( var_colNum1 );
    var_colNum1 = NULL;

    Py_XDECREF( var_rowNum2 );
    var_rowNum2 = NULL;

    Py_XDECREF( var_colNum2 );
    var_colNum2 = NULL;

    Py_XDECREF( var_kwargs );
    var_kwargs = NULL;

    Py_XDECREF( var_left );
    var_left = NULL;

    Py_XDECREF( var_bottom );
    var_bottom = NULL;

    Py_XDECREF( var_right );
    var_right = NULL;

    Py_XDECREF( var_top );
    var_top = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_21;
    exception_value = exception_keeper_value_21;
    exception_tb = exception_keeper_tb_21;
    exception_lineno = exception_keeper_lineno_21;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_layout$$$function_8_get_tight_layout_figure );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_1__get_left(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_layout$$$function_1__get_left,
        const_str_plain__get_left,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0e8ec8b23f6915730102842c38ac54c7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_layout,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_2__get_right(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_layout$$$function_2__get_right,
        const_str_plain__get_right,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e4c142720c59242629db15855ec5f99e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_layout,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_3__get_bottom(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_layout$$$function_3__get_bottom,
        const_str_plain__get_bottom,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c6c522dfe327c281626eca0ebefbaa78,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_layout,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_4__get_top(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_layout$$$function_4__get_top,
        const_str_plain__get_top,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_fd638e1a1a1d44e380df4f7b4db53dd5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_layout,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars,
        const_str_plain_auto_adjust_subplotpars,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_dfe501d74e083632d082979fedbf32ac,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_layout,
        const_str_digest_91f231986ceaa1aa2b5332e5f3fa7a03,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_6_get_renderer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_layout$$$function_6_get_renderer,
        const_str_plain_get_renderer,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ed465ddd478fe10c713c4573bf253c8f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_layout,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_7_get_subplotspec_list( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_layout$$$function_7_get_subplotspec_list,
        const_str_plain_get_subplotspec_list,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e06e28650b8962f2e6e2d032dd2fa616,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_layout,
        const_str_digest_ffd2f47d1bf0b28ba0026578ee1ec340,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_layout$$$function_8_get_tight_layout_figure( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_layout$$$function_8_get_tight_layout_figure,
        const_str_plain_get_tight_layout_figure,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_929741756295e159ad639a088117b506,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_layout,
        const_str_digest_81382ed4783c15b20fbc8b2595ebdc58,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_matplotlib$tight_layout =
{
    PyModuleDef_HEAD_INIT,
    "matplotlib.tight_layout",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(matplotlib$tight_layout)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(matplotlib$tight_layout)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_matplotlib$tight_layout );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("matplotlib.tight_layout: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.tight_layout: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.tight_layout: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initmatplotlib$tight_layout" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_matplotlib$tight_layout = Py_InitModule4(
        "matplotlib.tight_layout",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_matplotlib$tight_layout = PyModule_Create( &mdef_matplotlib$tight_layout );
#endif

    moduledict_matplotlib$tight_layout = MODULE_DICT( module_matplotlib$tight_layout );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_matplotlib$tight_layout,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_matplotlib$tight_layout,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$tight_layout,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$tight_layout,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_matplotlib$tight_layout );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_e5414949eeddd63c81115af32fad7475, module_matplotlib$tight_layout );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_c351c7510022255ee6c7be0c2ccc9d1d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_e11a877378ac93aa8261e847c3379ba0;
        UPDATE_STRING_DICT0( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_c351c7510022255ee6c7be0c2ccc9d1d = MAKE_MODULE_FRAME( codeobj_c351c7510022255ee6c7be0c2ccc9d1d, module_matplotlib$tight_layout );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_c351c7510022255ee6c7be0c2ccc9d1d );
    assert( Py_REFCNT( frame_c351c7510022255ee6c7be0c2ccc9d1d ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_matplotlib;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$tight_layout;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_cbook_str_plain_rcParams_tuple;
        tmp_level_name_1 = const_int_0;
        frame_c351c7510022255ee6c7be0c2ccc9d1d->m_frame.f_lineno = 12;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_cbook );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_cbook, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_rcParams );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_rcParams, tmp_assign_source_6 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_f27e18bd3454dcd2358f62563ae885e3;
        tmp_globals_name_2 = (PyObject *)moduledict_matplotlib$tight_layout;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_FontProperties_tuple;
        tmp_level_name_2 = const_int_0;
        frame_c351c7510022255ee6c7be0c2ccc9d1d->m_frame.f_lineno = 13;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_FontProperties );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_FontProperties, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_d28745151750b44e549d12f294293c52;
        tmp_globals_name_3 = (PyObject *)moduledict_matplotlib$tight_layout;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_TransformedBbox_str_plain_Bbox_tuple;
        tmp_level_name_3 = const_int_0;
        frame_c351c7510022255ee6c7be0c2ccc9d1d->m_frame.f_lineno = 14;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_8;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_4 = tmp_import_from_2__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_TransformedBbox );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_TransformedBbox, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_Bbox );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_Bbox, tmp_assign_source_10 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c351c7510022255ee6c7be0c2ccc9d1d );
#endif
    popFrameStack();

    assertFrameObject( frame_c351c7510022255ee6c7be0c2ccc9d1d );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c351c7510022255ee6c7be0c2ccc9d1d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c351c7510022255ee6c7be0c2ccc9d1d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c351c7510022255ee6c7be0c2ccc9d1d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c351c7510022255ee6c7be0c2ccc9d1d, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_matplotlib$tight_layout$$$function_1__get_left(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_left, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_matplotlib$tight_layout$$$function_2__get_right(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_right, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_matplotlib$tight_layout$$$function_3__get_bottom(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_bottom, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_matplotlib$tight_layout$$$function_4__get_top(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain__get_top, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_float_1_08_none_none_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_15 = MAKE_FUNCTION_matplotlib$tight_layout$$$function_5_auto_adjust_subplotpars( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_auto_adjust_subplotpars, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_matplotlib$tight_layout$$$function_6_get_renderer(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_get_renderer, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_17 = MAKE_FUNCTION_matplotlib$tight_layout$$$function_7_get_subplotspec_list( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_get_subplotspec_list, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_float_1_08_none_none_none_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_18 = MAKE_FUNCTION_matplotlib$tight_layout$$$function_8_get_tight_layout_figure( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_layout, (Nuitka_StringObject *)const_str_plain_get_tight_layout_figure, tmp_assign_source_18 );
    }

    return MOD_RETURN_VALUE( module_matplotlib$tight_layout );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
