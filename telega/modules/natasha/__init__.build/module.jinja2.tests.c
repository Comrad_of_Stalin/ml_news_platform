/* Generated code for Python module 'jinja2.tests'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jinja2$tests" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jinja2$tests;
PyDictObject *moduledict_jinja2$tests;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_seq;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_chr_60;
extern PyObject *const_str_plain_string;
static PyObject *const_str_plain_regex_type;
extern PyObject *const_str_plain_iterable;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_test_lower;
static PyObject *const_str_plain_lessthan;
static PyObject *const_str_plain_number_re;
extern PyObject *const_tuple_str_plain_value_tuple;
extern PyObject *const_str_digest_a53e6044afa65158b90d5bc4937bebce;
static PyObject *const_str_digest_2d0d1093dabfa768a56953977d776cba;
extern PyObject *const_str_plain_num;
extern PyObject *const_tuple_str_plain_Undefined_tuple;
extern PyObject *const_str_digest_13243295198ba360687160a687a3cc51;
extern PyObject *const_str_plain_callable;
extern PyObject *const_str_plain_decimal;
static PyObject *const_str_plain_test_none;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain_lower;
static PyObject *const_str_digest_451262ace7f4f5ad73996a2a002a76b3;
static PyObject *const_tuple_9e82f206d5d4b06c2316e5f3514399fa_tuple;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_plain_test_defined;
extern PyObject *const_str_chr_62;
static PyObject *const_str_digest_86cd05eac338e7b35eca1bc28d5e03b4;
static PyObject *const_str_digest_c74fe08cd05df6b8ccceb5a7d43e0ae0;
static PyObject *const_str_digest_072e974f4b4ffee1e06cb9da72c89c56;
static PyObject *const_str_plain_test_iterable;
extern PyObject *const_str_plain_sequence;
extern PyObject *const_str_digest_c566e8f3fba64199b66b7b7424440c9c;
extern PyObject *const_str_plain_gt;
static PyObject *const_str_digest_c9ec9f0d093a887cabbac1aac6538a28;
static PyObject *const_str_digest_8241b8201687b1fc04987284ac752246;
static PyObject *const_str_digest_3ac1c6b319dd49e13153a576f1d30e8f;
extern PyObject *const_str_plain_none;
static PyObject *const_tuple_str_digest_707d5ca85d98e8b10fedfcc92bdc7034_tuple;
static PyObject *const_str_digest_c4ed5be0cad2bc797ba5192bafc0c15e;
extern PyObject *const_str_plain_string_types;
extern PyObject *const_str_digest_38d72d37090218643439302eee0c8cc7;
static PyObject *const_str_plain_test_sequence;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_in;
static PyObject *const_str_plain_greaterthan;
extern PyObject *const_str_plain_float;
static PyObject *const_str_plain_equalto;
static PyObject *const_str_digest_8ca79c63339af468c29aefe4249e2307;
static PyObject *const_tuple_str_plain_value_str_plain_num_tuple;
extern PyObject *const_str_plain_collections;
extern PyObject *const_str_plain_Decimal;
extern PyObject *const_str_plain_even;
static PyObject *const_str_digest_707d5ca85d98e8b10fedfcc92bdc7034;
static PyObject *const_str_digest_5a648dd33a06978b9424d00906545927;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_integer_types;
extern PyObject *const_str_plain_ge;
static PyObject *const_str_plain_test_upper;
static PyObject *const_str_plain_escaped;
static PyObject *const_str_plain_test_divisibleby;
static PyObject *const_str_digest_575a17f3af0b3c0a4486f8916a419968;
static PyObject *const_str_plain_test_sameas;
static PyObject *const_tuple_str_plain_value_str_plain_seq_tuple;
extern PyObject *const_str_plain_compile;
static PyObject *const_str_plain_sameas;
static PyObject *const_str_plain_test_escaped;
static PyObject *const_str_digest_078811cd0365d495d2ab03aea59b89e0;
extern PyObject *const_str_plain_eq;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_plain_test_even;
static PyObject *const_tuple_str_plain_value_str_plain_other_tuple;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_upper;
static PyObject *const_str_digest_53094b51223df367150c354ba7a8652f;
extern PyObject *const_str_plain_complex;
extern PyObject *const_str_plain_TESTS;
extern PyObject *const_str_plain___html__;
static PyObject *const_str_plain_test_string;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_test_odd;
extern PyObject *const_str_plain_undefined;
extern PyObject *const_str_plain_mapping;
static PyObject *const_str_plain_test_in;
extern PyObject *const_str_plain_odd;
static PyObject *const_str_plain_test_undefined;
static PyObject *const_str_plain_test_number;
static PyObject *const_str_plain_test_callable;
extern PyObject *const_str_digest_5f408ea264aad5c192d303d32799c57f;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_islower;
extern PyObject *const_str_digest_06f0dc014cf3cf0959686df5c4beb2e6;
extern PyObject *const_str_plain_text_type;
extern PyObject *const_str_plain_Undefined;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_825c1a98e14fc7118798d7057dcb6f53;
extern PyObject *const_str_plain_other;
extern PyObject *const_str_plain_Mapping;
extern PyObject *const_tuple_str_plain_Mapping_tuple;
static PyObject *const_str_digest_51469a7370f355943cb51c71759d8b1e;
static PyObject *const_str_plain_defined;
extern PyObject *const_str_plain_operator;
static PyObject *const_str_plain_test_mapping;
static PyObject *const_str_digest_22596797f0eb52619eeb05ac378414d8;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_isupper;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_lt;
extern PyObject *const_str_plain_ne;
static PyObject *const_str_digest_d9d7ca212d4c5c70420a2a1370389dac;
static PyObject *const_str_plain_divisibleby;
extern PyObject *const_str_plain_le;
static PyObject *const_str_digest_0b981f06297a7f16a44328433ed19d5a;
extern PyObject *const_str_plain_number;
extern PyObject *const_str_digest_124f1473eafa684c185fd606074efc0f;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_regex_type = UNSTREAM_STRING_ASCII( &constant_bin[ 1138745 ], 10, 1 );
    const_str_plain_test_lower = UNSTREAM_STRING_ASCII( &constant_bin[ 1138755 ], 10, 1 );
    const_str_plain_lessthan = UNSTREAM_STRING_ASCII( &constant_bin[ 1098156 ], 8, 1 );
    const_str_plain_number_re = UNSTREAM_STRING_ASCII( &constant_bin[ 1138765 ], 9, 1 );
    const_str_digest_2d0d1093dabfa768a56953977d776cba = UNSTREAM_STRING_ASCII( &constant_bin[ 1138774 ], 36, 0 );
    const_str_plain_test_none = UNSTREAM_STRING_ASCII( &constant_bin[ 1138810 ], 9, 1 );
    const_str_digest_451262ace7f4f5ad73996a2a002a76b3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1138819 ], 36, 0 );
    const_tuple_9e82f206d5d4b06c2316e5f3514399fa_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_9e82f206d5d4b06c2316e5f3514399fa_tuple, 0, const_str_plain_text_type ); Py_INCREF( const_str_plain_text_type );
    PyTuple_SET_ITEM( const_tuple_9e82f206d5d4b06c2316e5f3514399fa_tuple, 1, const_str_plain_string_types ); Py_INCREF( const_str_plain_string_types );
    PyTuple_SET_ITEM( const_tuple_9e82f206d5d4b06c2316e5f3514399fa_tuple, 2, const_str_plain_integer_types ); Py_INCREF( const_str_plain_integer_types );
    const_str_plain_test_defined = UNSTREAM_STRING_ASCII( &constant_bin[ 1138855 ], 12, 1 );
    const_str_digest_86cd05eac338e7b35eca1bc28d5e03b4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1138867 ], 35, 0 );
    const_str_digest_c74fe08cd05df6b8ccceb5a7d43e0ae0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1138902 ], 83, 0 );
    const_str_digest_072e974f4b4ffee1e06cb9da72c89c56 = UNSTREAM_STRING_ASCII( &constant_bin[ 1138985 ], 15, 0 );
    const_str_plain_test_iterable = UNSTREAM_STRING_ASCII( &constant_bin[ 1139000 ], 13, 1 );
    const_str_digest_c9ec9f0d093a887cabbac1aac6538a28 = UNSTREAM_STRING_ASCII( &constant_bin[ 1139013 ], 58, 0 );
    const_str_digest_8241b8201687b1fc04987284ac752246 = UNSTREAM_STRING_ASCII( &constant_bin[ 1139071 ], 45, 0 );
    const_str_digest_3ac1c6b319dd49e13153a576f1d30e8f = UNSTREAM_STRING_ASCII( &constant_bin[ 1139116 ], 40, 0 );
    const_tuple_str_digest_707d5ca85d98e8b10fedfcc92bdc7034_tuple = PyTuple_New( 1 );
    const_str_digest_707d5ca85d98e8b10fedfcc92bdc7034 = UNSTREAM_STRING_ASCII( &constant_bin[ 1139156 ], 15, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_707d5ca85d98e8b10fedfcc92bdc7034_tuple, 0, const_str_digest_707d5ca85d98e8b10fedfcc92bdc7034 ); Py_INCREF( const_str_digest_707d5ca85d98e8b10fedfcc92bdc7034 );
    const_str_digest_c4ed5be0cad2bc797ba5192bafc0c15e = UNSTREAM_STRING_ASCII( &constant_bin[ 1139171 ], 185, 0 );
    const_str_plain_test_sequence = UNSTREAM_STRING_ASCII( &constant_bin[ 1139356 ], 13, 1 );
    const_str_plain_greaterthan = UNSTREAM_STRING_ASCII( &constant_bin[ 1139369 ], 11, 1 );
    const_str_plain_equalto = UNSTREAM_STRING_ASCII( &constant_bin[ 1098201 ], 7, 1 );
    const_str_digest_8ca79c63339af468c29aefe4249e2307 = UNSTREAM_STRING_ASCII( &constant_bin[ 1139380 ], 21, 0 );
    const_tuple_str_plain_value_str_plain_num_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_num_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_num_tuple, 1, const_str_plain_num ); Py_INCREF( const_str_plain_num );
    const_str_digest_5a648dd33a06978b9424d00906545927 = UNSTREAM_STRING_ASCII( &constant_bin[ 1139401 ], 42, 0 );
    const_str_plain_test_upper = UNSTREAM_STRING_ASCII( &constant_bin[ 1139443 ], 10, 1 );
    const_str_plain_escaped = UNSTREAM_STRING_ASCII( &constant_bin[ 205157 ], 7, 1 );
    const_str_plain_test_divisibleby = UNSTREAM_STRING_ASCII( &constant_bin[ 1139453 ], 16, 1 );
    const_str_digest_575a17f3af0b3c0a4486f8916a419968 = UNSTREAM_STRING_ASCII( &constant_bin[ 1139469 ], 315, 0 );
    const_str_plain_test_sameas = UNSTREAM_STRING_ASCII( &constant_bin[ 1139784 ], 11, 1 );
    const_tuple_str_plain_value_str_plain_seq_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_seq_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_seq_tuple, 1, const_str_plain_seq ); Py_INCREF( const_str_plain_seq );
    const_str_plain_sameas = UNSTREAM_STRING_ASCII( &constant_bin[ 1139789 ], 6, 1 );
    const_str_plain_test_escaped = UNSTREAM_STRING_ASCII( &constant_bin[ 1139795 ], 12, 1 );
    const_str_digest_078811cd0365d495d2ab03aea59b89e0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1139807 ], 94, 0 );
    const_str_plain_test_even = UNSTREAM_STRING_ASCII( &constant_bin[ 1139901 ], 9, 1 );
    const_tuple_str_plain_value_str_plain_other_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_other_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_other_tuple, 1, const_str_plain_other ); Py_INCREF( const_str_plain_other );
    const_str_digest_53094b51223df367150c354ba7a8652f = UNSTREAM_STRING_ASCII( &constant_bin[ 1139910 ], 239, 0 );
    const_str_plain_test_string = UNSTREAM_STRING_ASCII( &constant_bin[ 1140149 ], 11, 1 );
    const_str_plain_test_odd = UNSTREAM_STRING_ASCII( &constant_bin[ 1140160 ], 8, 1 );
    const_str_plain_test_in = UNSTREAM_STRING_ASCII( &constant_bin[ 1140168 ], 7, 1 );
    const_str_plain_test_undefined = UNSTREAM_STRING_ASCII( &constant_bin[ 1140175 ], 14, 1 );
    const_str_plain_test_number = UNSTREAM_STRING_ASCII( &constant_bin[ 1140189 ], 11, 1 );
    const_str_plain_test_callable = UNSTREAM_STRING_ASCII( &constant_bin[ 1140200 ], 13, 1 );
    const_str_digest_825c1a98e14fc7118798d7057dcb6f53 = UNSTREAM_STRING_ASCII( &constant_bin[ 1140213 ], 30, 0 );
    const_str_digest_51469a7370f355943cb51c71759d8b1e = UNSTREAM_STRING_ASCII( &constant_bin[ 1140243 ], 42, 0 );
    const_str_plain_defined = UNSTREAM_STRING_ASCII( &constant_bin[ 100511 ], 7, 1 );
    const_str_plain_test_mapping = UNSTREAM_STRING_ASCII( &constant_bin[ 1140285 ], 12, 1 );
    const_str_digest_22596797f0eb52619eeb05ac378414d8 = UNSTREAM_STRING_ASCII( &constant_bin[ 1140297 ], 45, 0 );
    const_str_digest_d9d7ca212d4c5c70420a2a1370389dac = UNSTREAM_STRING_ASCII( &constant_bin[ 1140342 ], 49, 0 );
    const_str_plain_divisibleby = UNSTREAM_STRING_ASCII( &constant_bin[ 1098109 ], 11, 1 );
    const_str_digest_0b981f06297a7f16a44328433ed19d5a = UNSTREAM_STRING_ASCII( &constant_bin[ 1140391 ], 38, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jinja2$tests( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_eaa60a7b55e03d94cc42a76fc3910735;
static PyCodeObject *codeobj_94cf9a5d9e22bf65c43b43510e715518;
static PyCodeObject *codeobj_7699459fe4de7630e2129d6c81ec6616;
static PyCodeObject *codeobj_0929f99b9009e5b3f94d58d68dc1a408;
static PyCodeObject *codeobj_d97f4a0663b7267b19c999c597196dbf;
static PyCodeObject *codeobj_db862756221a026249e6967ad8b0144f;
static PyCodeObject *codeobj_1f3b2369047ab496043deecb594cdf9d;
static PyCodeObject *codeobj_9579a76c267243601e2d93407ec59cdc;
static PyCodeObject *codeobj_bc6f30448799f7fd3520c2e7af124e06;
static PyCodeObject *codeobj_5d6adbba2907730ae76b4369beef440c;
static PyCodeObject *codeobj_28f2d59139cdaf8337b1c95310a55eb3;
static PyCodeObject *codeobj_8edf9d76b98cb63eae2841a55017b871;
static PyCodeObject *codeobj_671a7953c8902adf1db8830c42b1cf04;
static PyCodeObject *codeobj_19e44cf27c749bdce1f55555128786e9;
static PyCodeObject *codeobj_52f43674fd5e00b44ba6c301b5bafa5b;
static PyCodeObject *codeobj_84ef43c933402285a848aed734bf69c1;
static PyCodeObject *codeobj_a094b69ea2157c626260a602b0f9b221;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_072e974f4b4ffee1e06cb9da72c89c56 );
    codeobj_eaa60a7b55e03d94cc42a76fc3910735 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_8ca79c63339af468c29aefe4249e2307, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_94cf9a5d9e22bf65c43b43510e715518 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_defined, 40, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7699459fe4de7630e2129d6c81ec6616 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_divisibleby, 35, const_tuple_str_plain_value_str_plain_num_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0929f99b9009e5b3f94d58d68dc1a408 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_escaped, 129, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d97f4a0663b7267b19c999c597196dbf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_even, 30, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_db862756221a026249e6967ad8b0144f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_in, 134, const_tuple_str_plain_value_str_plain_seq_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1f3b2369047ab496043deecb594cdf9d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_iterable, 120, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9579a76c267243601e2d93407ec59cdc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_lower, 67, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bc6f30448799f7fd3520c2e7af124e06 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_mapping, 82, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5d6adbba2907730ae76b4369beef440c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_none, 62, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_28f2d59139cdaf8337b1c95310a55eb3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_number, 90, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8edf9d76b98cb63eae2841a55017b871 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_odd, 25, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_671a7953c8902adf1db8830c42b1cf04 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_sameas, 107, const_tuple_str_plain_value_str_plain_other_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_19e44cf27c749bdce1f55555128786e9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_sequence, 95, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_52f43674fd5e00b44ba6c301b5bafa5b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_string, 77, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_84ef43c933402285a848aed734bf69c1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_undefined, 57, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a094b69ea2157c626260a602b0f9b221 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_test_upper, 72, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_10_test_mapping(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_11_test_number(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_12_test_sequence(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_13_test_sameas(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_14_test_iterable(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_15_test_escaped(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_16_test_in(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_1_test_odd(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_2_test_even(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_3_test_divisibleby(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_4_test_defined(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_5_test_undefined(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_6_test_none(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_7_test_lower(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_8_test_upper(  );


static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_9_test_string(  );


// The module function definitions.
static PyObject *impl_jinja2$tests$$$function_1_test_odd( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_8edf9d76b98cb63eae2841a55017b871;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8edf9d76b98cb63eae2841a55017b871 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8edf9d76b98cb63eae2841a55017b871, codeobj_8edf9d76b98cb63eae2841a55017b871, module_jinja2$tests, sizeof(void *) );
    frame_8edf9d76b98cb63eae2841a55017b871 = cache_frame_8edf9d76b98cb63eae2841a55017b871;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8edf9d76b98cb63eae2841a55017b871 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8edf9d76b98cb63eae2841a55017b871 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_value );
        tmp_left_name_1 = par_value;
        tmp_right_name_1 = const_int_pos_2;
        tmp_compexpr_left_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_1;
        tmp_return_value = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8edf9d76b98cb63eae2841a55017b871 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8edf9d76b98cb63eae2841a55017b871 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8edf9d76b98cb63eae2841a55017b871 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8edf9d76b98cb63eae2841a55017b871, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8edf9d76b98cb63eae2841a55017b871->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8edf9d76b98cb63eae2841a55017b871, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8edf9d76b98cb63eae2841a55017b871,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_8edf9d76b98cb63eae2841a55017b871 == cache_frame_8edf9d76b98cb63eae2841a55017b871 )
    {
        Py_DECREF( frame_8edf9d76b98cb63eae2841a55017b871 );
    }
    cache_frame_8edf9d76b98cb63eae2841a55017b871 = NULL;

    assertFrameObject( frame_8edf9d76b98cb63eae2841a55017b871 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_1_test_odd );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_1_test_odd );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_2_test_even( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d97f4a0663b7267b19c999c597196dbf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d97f4a0663b7267b19c999c597196dbf = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d97f4a0663b7267b19c999c597196dbf, codeobj_d97f4a0663b7267b19c999c597196dbf, module_jinja2$tests, sizeof(void *) );
    frame_d97f4a0663b7267b19c999c597196dbf = cache_frame_d97f4a0663b7267b19c999c597196dbf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d97f4a0663b7267b19c999c597196dbf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d97f4a0663b7267b19c999c597196dbf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_value );
        tmp_left_name_1 = par_value;
        tmp_right_name_1 = const_int_pos_2;
        tmp_compexpr_left_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_0;
        tmp_return_value = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d97f4a0663b7267b19c999c597196dbf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d97f4a0663b7267b19c999c597196dbf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d97f4a0663b7267b19c999c597196dbf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d97f4a0663b7267b19c999c597196dbf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d97f4a0663b7267b19c999c597196dbf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d97f4a0663b7267b19c999c597196dbf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d97f4a0663b7267b19c999c597196dbf,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_d97f4a0663b7267b19c999c597196dbf == cache_frame_d97f4a0663b7267b19c999c597196dbf )
    {
        Py_DECREF( frame_d97f4a0663b7267b19c999c597196dbf );
    }
    cache_frame_d97f4a0663b7267b19c999c597196dbf = NULL;

    assertFrameObject( frame_d97f4a0663b7267b19c999c597196dbf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_2_test_even );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_2_test_even );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_3_test_divisibleby( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *par_num = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_7699459fe4de7630e2129d6c81ec6616;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7699459fe4de7630e2129d6c81ec6616 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7699459fe4de7630e2129d6c81ec6616, codeobj_7699459fe4de7630e2129d6c81ec6616, module_jinja2$tests, sizeof(void *)+sizeof(void *) );
    frame_7699459fe4de7630e2129d6c81ec6616 = cache_frame_7699459fe4de7630e2129d6c81ec6616;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7699459fe4de7630e2129d6c81ec6616 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7699459fe4de7630e2129d6c81ec6616 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_value );
        tmp_left_name_1 = par_value;
        CHECK_OBJECT( par_num );
        tmp_right_name_1 = par_num;
        tmp_compexpr_left_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_0;
        tmp_return_value = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7699459fe4de7630e2129d6c81ec6616 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7699459fe4de7630e2129d6c81ec6616 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7699459fe4de7630e2129d6c81ec6616 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7699459fe4de7630e2129d6c81ec6616, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7699459fe4de7630e2129d6c81ec6616->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7699459fe4de7630e2129d6c81ec6616, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7699459fe4de7630e2129d6c81ec6616,
        type_description_1,
        par_value,
        par_num
    );


    // Release cached frame.
    if ( frame_7699459fe4de7630e2129d6c81ec6616 == cache_frame_7699459fe4de7630e2129d6c81ec6616 )
    {
        Py_DECREF( frame_7699459fe4de7630e2129d6c81ec6616 );
    }
    cache_frame_7699459fe4de7630e2129d6c81ec6616 = NULL;

    assertFrameObject( frame_7699459fe4de7630e2129d6c81ec6616 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_3_test_divisibleby );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_3_test_divisibleby );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_4_test_defined( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_94cf9a5d9e22bf65c43b43510e715518;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_94cf9a5d9e22bf65c43b43510e715518 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_94cf9a5d9e22bf65c43b43510e715518, codeobj_94cf9a5d9e22bf65c43b43510e715518, module_jinja2$tests, sizeof(void *) );
    frame_94cf9a5d9e22bf65c43b43510e715518 = cache_frame_94cf9a5d9e22bf65c43b43510e715518;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_94cf9a5d9e22bf65c43b43510e715518 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_94cf9a5d9e22bf65c43b43510e715518 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_isinstance_inst_1 = par_value;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_Undefined );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Undefined );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Undefined" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res == 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_94cf9a5d9e22bf65c43b43510e715518 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_94cf9a5d9e22bf65c43b43510e715518 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_94cf9a5d9e22bf65c43b43510e715518 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_94cf9a5d9e22bf65c43b43510e715518, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_94cf9a5d9e22bf65c43b43510e715518->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_94cf9a5d9e22bf65c43b43510e715518, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_94cf9a5d9e22bf65c43b43510e715518,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_94cf9a5d9e22bf65c43b43510e715518 == cache_frame_94cf9a5d9e22bf65c43b43510e715518 )
    {
        Py_DECREF( frame_94cf9a5d9e22bf65c43b43510e715518 );
    }
    cache_frame_94cf9a5d9e22bf65c43b43510e715518 = NULL;

    assertFrameObject( frame_94cf9a5d9e22bf65c43b43510e715518 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_4_test_defined );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_4_test_defined );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_5_test_undefined( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_84ef43c933402285a848aed734bf69c1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_84ef43c933402285a848aed734bf69c1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_84ef43c933402285a848aed734bf69c1, codeobj_84ef43c933402285a848aed734bf69c1, module_jinja2$tests, sizeof(void *) );
    frame_84ef43c933402285a848aed734bf69c1 = cache_frame_84ef43c933402285a848aed734bf69c1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_84ef43c933402285a848aed734bf69c1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_84ef43c933402285a848aed734bf69c1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_isinstance_inst_1 = par_value;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_Undefined );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Undefined );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Undefined" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 59;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_84ef43c933402285a848aed734bf69c1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_84ef43c933402285a848aed734bf69c1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_84ef43c933402285a848aed734bf69c1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_84ef43c933402285a848aed734bf69c1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_84ef43c933402285a848aed734bf69c1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_84ef43c933402285a848aed734bf69c1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_84ef43c933402285a848aed734bf69c1,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_84ef43c933402285a848aed734bf69c1 == cache_frame_84ef43c933402285a848aed734bf69c1 )
    {
        Py_DECREF( frame_84ef43c933402285a848aed734bf69c1 );
    }
    cache_frame_84ef43c933402285a848aed734bf69c1 = NULL;

    assertFrameObject( frame_84ef43c933402285a848aed734bf69c1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_5_test_undefined );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_5_test_undefined );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_6_test_none( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_value );
        tmp_compexpr_left_1 = par_value;
        tmp_compexpr_right_1 = Py_None;
        tmp_return_value = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_6_test_none );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_6_test_none );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_7_test_lower( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9579a76c267243601e2d93407ec59cdc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9579a76c267243601e2d93407ec59cdc = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9579a76c267243601e2d93407ec59cdc, codeobj_9579a76c267243601e2d93407ec59cdc, module_jinja2$tests, sizeof(void *) );
    frame_9579a76c267243601e2d93407ec59cdc = cache_frame_9579a76c267243601e2d93407ec59cdc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9579a76c267243601e2d93407ec59cdc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9579a76c267243601e2d93407ec59cdc ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_text_type );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_text_type );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "text_type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_1 = par_value;
        frame_9579a76c267243601e2d93407ec59cdc->m_frame.f_lineno = 69;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_instance_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_9579a76c267243601e2d93407ec59cdc->m_frame.f_lineno = 69;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_islower );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9579a76c267243601e2d93407ec59cdc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9579a76c267243601e2d93407ec59cdc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9579a76c267243601e2d93407ec59cdc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9579a76c267243601e2d93407ec59cdc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9579a76c267243601e2d93407ec59cdc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9579a76c267243601e2d93407ec59cdc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9579a76c267243601e2d93407ec59cdc,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_9579a76c267243601e2d93407ec59cdc == cache_frame_9579a76c267243601e2d93407ec59cdc )
    {
        Py_DECREF( frame_9579a76c267243601e2d93407ec59cdc );
    }
    cache_frame_9579a76c267243601e2d93407ec59cdc = NULL;

    assertFrameObject( frame_9579a76c267243601e2d93407ec59cdc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_7_test_lower );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_7_test_lower );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_8_test_upper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a094b69ea2157c626260a602b0f9b221;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a094b69ea2157c626260a602b0f9b221 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a094b69ea2157c626260a602b0f9b221, codeobj_a094b69ea2157c626260a602b0f9b221, module_jinja2$tests, sizeof(void *) );
    frame_a094b69ea2157c626260a602b0f9b221 = cache_frame_a094b69ea2157c626260a602b0f9b221;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a094b69ea2157c626260a602b0f9b221 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a094b69ea2157c626260a602b0f9b221 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_text_type );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_text_type );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "text_type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_1 = par_value;
        frame_a094b69ea2157c626260a602b0f9b221->m_frame.f_lineno = 74;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_instance_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_a094b69ea2157c626260a602b0f9b221->m_frame.f_lineno = 74;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_isupper );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a094b69ea2157c626260a602b0f9b221 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a094b69ea2157c626260a602b0f9b221 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a094b69ea2157c626260a602b0f9b221 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a094b69ea2157c626260a602b0f9b221, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a094b69ea2157c626260a602b0f9b221->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a094b69ea2157c626260a602b0f9b221, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a094b69ea2157c626260a602b0f9b221,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_a094b69ea2157c626260a602b0f9b221 == cache_frame_a094b69ea2157c626260a602b0f9b221 )
    {
        Py_DECREF( frame_a094b69ea2157c626260a602b0f9b221 );
    }
    cache_frame_a094b69ea2157c626260a602b0f9b221 = NULL;

    assertFrameObject( frame_a094b69ea2157c626260a602b0f9b221 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_8_test_upper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_8_test_upper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_9_test_string( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_52f43674fd5e00b44ba6c301b5bafa5b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_52f43674fd5e00b44ba6c301b5bafa5b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_52f43674fd5e00b44ba6c301b5bafa5b, codeobj_52f43674fd5e00b44ba6c301b5bafa5b, module_jinja2$tests, sizeof(void *) );
    frame_52f43674fd5e00b44ba6c301b5bafa5b = cache_frame_52f43674fd5e00b44ba6c301b5bafa5b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_52f43674fd5e00b44ba6c301b5bafa5b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_52f43674fd5e00b44ba6c301b5bafa5b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_isinstance_inst_1 = par_value;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_string_types );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_string_types );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "string_types" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_52f43674fd5e00b44ba6c301b5bafa5b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_52f43674fd5e00b44ba6c301b5bafa5b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_52f43674fd5e00b44ba6c301b5bafa5b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_52f43674fd5e00b44ba6c301b5bafa5b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_52f43674fd5e00b44ba6c301b5bafa5b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_52f43674fd5e00b44ba6c301b5bafa5b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_52f43674fd5e00b44ba6c301b5bafa5b,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_52f43674fd5e00b44ba6c301b5bafa5b == cache_frame_52f43674fd5e00b44ba6c301b5bafa5b )
    {
        Py_DECREF( frame_52f43674fd5e00b44ba6c301b5bafa5b );
    }
    cache_frame_52f43674fd5e00b44ba6c301b5bafa5b = NULL;

    assertFrameObject( frame_52f43674fd5e00b44ba6c301b5bafa5b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_9_test_string );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_9_test_string );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_10_test_mapping( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_bc6f30448799f7fd3520c2e7af124e06;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_bc6f30448799f7fd3520c2e7af124e06 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bc6f30448799f7fd3520c2e7af124e06, codeobj_bc6f30448799f7fd3520c2e7af124e06, module_jinja2$tests, sizeof(void *) );
    frame_bc6f30448799f7fd3520c2e7af124e06 = cache_frame_bc6f30448799f7fd3520c2e7af124e06;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bc6f30448799f7fd3520c2e7af124e06 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bc6f30448799f7fd3520c2e7af124e06 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_isinstance_inst_1 = par_value;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_Mapping );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Mapping );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Mapping" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 87;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc6f30448799f7fd3520c2e7af124e06 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc6f30448799f7fd3520c2e7af124e06 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc6f30448799f7fd3520c2e7af124e06 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bc6f30448799f7fd3520c2e7af124e06, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bc6f30448799f7fd3520c2e7af124e06->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bc6f30448799f7fd3520c2e7af124e06, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bc6f30448799f7fd3520c2e7af124e06,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_bc6f30448799f7fd3520c2e7af124e06 == cache_frame_bc6f30448799f7fd3520c2e7af124e06 )
    {
        Py_DECREF( frame_bc6f30448799f7fd3520c2e7af124e06 );
    }
    cache_frame_bc6f30448799f7fd3520c2e7af124e06 = NULL;

    assertFrameObject( frame_bc6f30448799f7fd3520c2e7af124e06 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_10_test_mapping );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_10_test_mapping );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_11_test_number( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_28f2d59139cdaf8337b1c95310a55eb3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_28f2d59139cdaf8337b1c95310a55eb3 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_28f2d59139cdaf8337b1c95310a55eb3, codeobj_28f2d59139cdaf8337b1c95310a55eb3, module_jinja2$tests, sizeof(void *) );
    frame_28f2d59139cdaf8337b1c95310a55eb3 = cache_frame_28f2d59139cdaf8337b1c95310a55eb3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_28f2d59139cdaf8337b1c95310a55eb3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_28f2d59139cdaf8337b1c95310a55eb3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_value );
        tmp_isinstance_inst_1 = par_value;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_integer_types );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_integer_types );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "integer_types" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_left_name_1 = tmp_mvar_value_1;
        tmp_tuple_element_1 = (PyObject *)&PyFloat_Type;
        tmp_right_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = (PyObject *)&PyComplex_Type;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_decimal );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_decimal );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_right_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "decimal" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_2;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Decimal );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 92;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
        tmp_isinstance_cls_1 = BINARY_OPERATION_ADD_OBJECT_TUPLE( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_isinstance_cls_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_28f2d59139cdaf8337b1c95310a55eb3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_28f2d59139cdaf8337b1c95310a55eb3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_28f2d59139cdaf8337b1c95310a55eb3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_28f2d59139cdaf8337b1c95310a55eb3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_28f2d59139cdaf8337b1c95310a55eb3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_28f2d59139cdaf8337b1c95310a55eb3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_28f2d59139cdaf8337b1c95310a55eb3,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_28f2d59139cdaf8337b1c95310a55eb3 == cache_frame_28f2d59139cdaf8337b1c95310a55eb3 )
    {
        Py_DECREF( frame_28f2d59139cdaf8337b1c95310a55eb3 );
    }
    cache_frame_28f2d59139cdaf8337b1c95310a55eb3 = NULL;

    assertFrameObject( frame_28f2d59139cdaf8337b1c95310a55eb3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_11_test_number );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_11_test_number );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_12_test_sequence( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_19e44cf27c749bdce1f55555128786e9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_19e44cf27c749bdce1f55555128786e9 = NULL;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_19e44cf27c749bdce1f55555128786e9, codeobj_19e44cf27c749bdce1f55555128786e9, module_jinja2$tests, sizeof(void *) );
    frame_19e44cf27c749bdce1f55555128786e9 = cache_frame_19e44cf27c749bdce1f55555128786e9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_19e44cf27c749bdce1f55555128786e9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_19e44cf27c749bdce1f55555128786e9 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_len_arg_1;
        PyObject *tmp_capi_result_1;
        CHECK_OBJECT( par_value );
        tmp_len_arg_1 = par_value;
        tmp_capi_result_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_capi_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_capi_result_1 );
    }
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        CHECK_OBJECT( par_value );
        tmp_source_name_1 = par_value;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___getitem__ );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_attribute_value_1 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_19e44cf27c749bdce1f55555128786e9, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_19e44cf27c749bdce1f55555128786e9, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    tmp_return_value = Py_False;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_12_test_sequence );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // End of try:
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_12_test_sequence );
    return NULL;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_19e44cf27c749bdce1f55555128786e9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_19e44cf27c749bdce1f55555128786e9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_True;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_12_test_sequence );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_12_test_sequence );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_13_test_sameas( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *par_other = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_value );
        tmp_compexpr_left_1 = par_value;
        CHECK_OBJECT( par_other );
        tmp_compexpr_right_1 = par_other;
        tmp_return_value = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_13_test_sameas );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_other );
    Py_DECREF( par_other );
    par_other = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_13_test_sameas );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_14_test_iterable( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_1f3b2369047ab496043deecb594cdf9d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_1f3b2369047ab496043deecb594cdf9d = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1f3b2369047ab496043deecb594cdf9d, codeobj_1f3b2369047ab496043deecb594cdf9d, module_jinja2$tests, sizeof(void *) );
    frame_1f3b2369047ab496043deecb594cdf9d = cache_frame_1f3b2369047ab496043deecb594cdf9d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1f3b2369047ab496043deecb594cdf9d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1f3b2369047ab496043deecb594cdf9d ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_capi_result_1;
        CHECK_OBJECT( par_value );
        tmp_iter_arg_1 = par_value;
        tmp_capi_result_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_capi_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_capi_result_1 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_1f3b2369047ab496043deecb594cdf9d, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_1f3b2369047ab496043deecb594cdf9d, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_TypeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "o";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_False;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_3;
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 122;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_1f3b2369047ab496043deecb594cdf9d->m_frame) frame_1f3b2369047ab496043deecb594cdf9d->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "o";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_14_test_iterable );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1f3b2369047ab496043deecb594cdf9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1f3b2369047ab496043deecb594cdf9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1f3b2369047ab496043deecb594cdf9d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1f3b2369047ab496043deecb594cdf9d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1f3b2369047ab496043deecb594cdf9d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1f3b2369047ab496043deecb594cdf9d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1f3b2369047ab496043deecb594cdf9d,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_1f3b2369047ab496043deecb594cdf9d == cache_frame_1f3b2369047ab496043deecb594cdf9d )
    {
        Py_DECREF( frame_1f3b2369047ab496043deecb594cdf9d );
    }
    cache_frame_1f3b2369047ab496043deecb594cdf9d = NULL;

    assertFrameObject( frame_1f3b2369047ab496043deecb594cdf9d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_True;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_14_test_iterable );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_14_test_iterable );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_15_test_escaped( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0929f99b9009e5b3f94d58d68dc1a408;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0929f99b9009e5b3f94d58d68dc1a408 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0929f99b9009e5b3f94d58d68dc1a408, codeobj_0929f99b9009e5b3f94d58d68dc1a408, module_jinja2$tests, sizeof(void *) );
    frame_0929f99b9009e5b3f94d58d68dc1a408 = cache_frame_0929f99b9009e5b3f94d58d68dc1a408;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0929f99b9009e5b3f94d58d68dc1a408 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0929f99b9009e5b3f94d58d68dc1a408 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_name_1;
        CHECK_OBJECT( par_value );
        tmp_source_name_1 = par_value;
        tmp_attribute_name_1 = const_str_plain___html__;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0929f99b9009e5b3f94d58d68dc1a408 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0929f99b9009e5b3f94d58d68dc1a408 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0929f99b9009e5b3f94d58d68dc1a408 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0929f99b9009e5b3f94d58d68dc1a408, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0929f99b9009e5b3f94d58d68dc1a408->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0929f99b9009e5b3f94d58d68dc1a408, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0929f99b9009e5b3f94d58d68dc1a408,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_0929f99b9009e5b3f94d58d68dc1a408 == cache_frame_0929f99b9009e5b3f94d58d68dc1a408 )
    {
        Py_DECREF( frame_0929f99b9009e5b3f94d58d68dc1a408 );
    }
    cache_frame_0929f99b9009e5b3f94d58d68dc1a408 = NULL;

    assertFrameObject( frame_0929f99b9009e5b3f94d58d68dc1a408 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_15_test_escaped );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_15_test_escaped );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$tests$$$function_16_test_in( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *par_seq = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_db862756221a026249e6967ad8b0144f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_db862756221a026249e6967ad8b0144f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_db862756221a026249e6967ad8b0144f, codeobj_db862756221a026249e6967ad8b0144f, module_jinja2$tests, sizeof(void *)+sizeof(void *) );
    frame_db862756221a026249e6967ad8b0144f = cache_frame_db862756221a026249e6967ad8b0144f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_db862756221a026249e6967ad8b0144f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_db862756221a026249e6967ad8b0144f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_value );
        tmp_compexpr_left_1 = par_value;
        CHECK_OBJECT( par_seq );
        tmp_compexpr_right_1 = par_seq;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_db862756221a026249e6967ad8b0144f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_db862756221a026249e6967ad8b0144f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_db862756221a026249e6967ad8b0144f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_db862756221a026249e6967ad8b0144f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_db862756221a026249e6967ad8b0144f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_db862756221a026249e6967ad8b0144f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_db862756221a026249e6967ad8b0144f,
        type_description_1,
        par_value,
        par_seq
    );


    // Release cached frame.
    if ( frame_db862756221a026249e6967ad8b0144f == cache_frame_db862756221a026249e6967ad8b0144f )
    {
        Py_DECREF( frame_db862756221a026249e6967ad8b0144f );
    }
    cache_frame_db862756221a026249e6967ad8b0144f = NULL;

    assertFrameObject( frame_db862756221a026249e6967ad8b0144f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_16_test_in );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_seq );
    Py_DECREF( par_seq );
    par_seq = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_seq );
    Py_DECREF( par_seq );
    par_seq = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$tests$$$function_16_test_in );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_10_test_mapping(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_10_test_mapping,
        const_str_plain_test_mapping,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bc6f30448799f7fd3520c2e7af124e06,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_c74fe08cd05df6b8ccceb5a7d43e0ae0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_11_test_number(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_11_test_number,
        const_str_plain_test_number,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_28f2d59139cdaf8337b1c95310a55eb3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_3ac1c6b319dd49e13153a576f1d30e8f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_12_test_sequence(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_12_test_sequence,
        const_str_plain_test_sequence,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_19e44cf27c749bdce1f55555128786e9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_078811cd0365d495d2ab03aea59b89e0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_13_test_sameas(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_13_test_sameas,
        const_str_plain_test_sameas,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_671a7953c8902adf1db8830c42b1cf04,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_53094b51223df367150c354ba7a8652f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_14_test_iterable(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_14_test_iterable,
        const_str_plain_test_iterable,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1f3b2369047ab496043deecb594cdf9d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_d9d7ca212d4c5c70420a2a1370389dac,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_15_test_escaped(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_15_test_escaped,
        const_str_plain_test_escaped,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0929f99b9009e5b3f94d58d68dc1a408,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_825c1a98e14fc7118798d7057dcb6f53,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_16_test_in(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_16_test_in,
        const_str_plain_test_in,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_db862756221a026249e6967ad8b0144f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_c9ec9f0d093a887cabbac1aac6538a28,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_1_test_odd(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_1_test_odd,
        const_str_plain_test_odd,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8edf9d76b98cb63eae2841a55017b871,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_86cd05eac338e7b35eca1bc28d5e03b4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_2_test_even(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_2_test_even,
        const_str_plain_test_even,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d97f4a0663b7267b19c999c597196dbf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_2d0d1093dabfa768a56953977d776cba,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_3_test_divisibleby(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_3_test_divisibleby,
        const_str_plain_test_divisibleby,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7699459fe4de7630e2129d6c81ec6616,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_8241b8201687b1fc04987284ac752246,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_4_test_defined(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_4_test_defined,
        const_str_plain_test_defined,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_94cf9a5d9e22bf65c43b43510e715518,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_575a17f3af0b3c0a4486f8916a419968,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_5_test_undefined(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_5_test_undefined,
        const_str_plain_test_undefined,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_84ef43c933402285a848aed734bf69c1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_22596797f0eb52619eeb05ac378414d8,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_6_test_none(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_6_test_none,
        const_str_plain_test_none,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5d6adbba2907730ae76b4369beef440c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_451262ace7f4f5ad73996a2a002a76b3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_7_test_lower(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_7_test_lower,
        const_str_plain_test_lower,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9579a76c267243601e2d93407ec59cdc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_51469a7370f355943cb51c71759d8b1e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_8_test_upper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_8_test_upper,
        const_str_plain_test_upper,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a094b69ea2157c626260a602b0f9b221,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_5a648dd33a06978b9424d00906545927,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$tests$$$function_9_test_string(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$tests$$$function_9_test_string,
        const_str_plain_test_string,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_52f43674fd5e00b44ba6c301b5bafa5b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$tests,
        const_str_digest_0b981f06297a7f16a44328433ed19d5a,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jinja2$tests =
{
    PyModuleDef_HEAD_INIT,
    "jinja2.tests",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jinja2$tests)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jinja2$tests)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jinja2$tests );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jinja2.tests: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jinja2.tests: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jinja2.tests: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjinja2$tests" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jinja2$tests = Py_InitModule4(
        "jinja2.tests",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jinja2$tests = PyModule_Create( &mdef_jinja2$tests );
#endif

    moduledict_jinja2$tests = MODULE_DICT( module_jinja2$tests );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jinja2$tests,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jinja2$tests,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jinja2$tests,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jinja2$tests,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jinja2$tests );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_06f0dc014cf3cf0959686df5c4beb2e6, module_jinja2$tests );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_eaa60a7b55e03d94cc42a76fc3910735;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_c4ed5be0cad2bc797ba5192bafc0c15e;
        UPDATE_STRING_DICT0( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_eaa60a7b55e03d94cc42a76fc3910735 = MAKE_MODULE_FRAME( codeobj_eaa60a7b55e03d94cc42a76fc3910735, module_jinja2$tests );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_eaa60a7b55e03d94cc42a76fc3910735 );
    assert( Py_REFCNT( frame_eaa60a7b55e03d94cc42a76fc3910735 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_operator;
        tmp_globals_name_1 = (PyObject *)moduledict_jinja2$tests;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_eaa60a7b55e03d94cc42a76fc3910735->m_frame.f_lineno = 11;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_re;
        tmp_globals_name_2 = (PyObject *)moduledict_jinja2$tests;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_eaa60a7b55e03d94cc42a76fc3910735->m_frame.f_lineno = 12;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_collections;
        tmp_globals_name_3 = (PyObject *)moduledict_jinja2$tests;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_Mapping_tuple;
        tmp_level_name_3 = const_int_0;
        frame_eaa60a7b55e03d94cc42a76fc3910735->m_frame.f_lineno = 13;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Mapping );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_Mapping, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_38d72d37090218643439302eee0c8cc7;
        tmp_globals_name_4 = (PyObject *)moduledict_jinja2$tests;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_Undefined_tuple;
        tmp_level_name_4 = const_int_0;
        frame_eaa60a7b55e03d94cc42a76fc3910735->m_frame.f_lineno = 14;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Undefined );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_Undefined, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_124f1473eafa684c185fd606074efc0f;
        tmp_globals_name_5 = (PyObject *)moduledict_jinja2$tests;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_9e82f206d5d4b06c2316e5f3514399fa_tuple;
        tmp_level_name_5 = const_int_0;
        frame_eaa60a7b55e03d94cc42a76fc3910735->m_frame.f_lineno = 15;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_8;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_text_type );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_text_type, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_string_types );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_string_types, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_integer_types );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_integer_types, tmp_assign_source_11 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_decimal;
        tmp_globals_name_6 = (PyObject *)moduledict_jinja2$tests;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_eaa60a7b55e03d94cc42a76fc3910735->m_frame.f_lineno = 16;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_decimal, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 18;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        frame_eaa60a7b55e03d94cc42a76fc3910735->m_frame.f_lineno = 18;
        tmp_assign_source_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_707d5ca85d98e8b10fedfcc92bdc7034_tuple, 0 ) );

        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_number_re, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_number_re );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_number_re );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_type_arg_1 = tmp_mvar_value_4;
        tmp_assign_source_14 = BUILTIN_TYPE1( tmp_type_arg_1 );
        assert( !(tmp_assign_source_14 == NULL) );
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_regex_type, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = LOOKUP_BUILTIN( const_str_plain_callable );
        assert( tmp_assign_source_15 != NULL );
        UPDATE_STRING_DICT0( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_callable, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_jinja2$tests$$$function_1_test_odd(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_odd, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_jinja2$tests$$$function_2_test_even(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_even, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_jinja2$tests$$$function_3_test_divisibleby(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_divisibleby, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_jinja2$tests$$$function_4_test_defined(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_defined, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_jinja2$tests$$$function_5_test_undefined(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_undefined, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_jinja2$tests$$$function_6_test_none(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_none, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = MAKE_FUNCTION_jinja2$tests$$$function_7_test_lower(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_lower, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = MAKE_FUNCTION_jinja2$tests$$$function_8_test_upper(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_upper, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_jinja2$tests$$$function_9_test_string(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_string, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = MAKE_FUNCTION_jinja2$tests$$$function_10_test_mapping(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_mapping, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = MAKE_FUNCTION_jinja2$tests$$$function_11_test_number(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_number, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = MAKE_FUNCTION_jinja2$tests$$$function_12_test_sequence(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_sequence, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = MAKE_FUNCTION_jinja2$tests$$$function_13_test_sameas(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_sameas, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = MAKE_FUNCTION_jinja2$tests$$$function_14_test_iterable(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_iterable, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = MAKE_FUNCTION_jinja2$tests$$$function_15_test_escaped(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_escaped, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = MAKE_FUNCTION_jinja2$tests$$$function_16_test_in(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_in, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_dict_key_22;
        PyObject *tmp_dict_value_22;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_dict_key_28;
        PyObject *tmp_dict_value_28;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_dict_key_29;
        PyObject *tmp_dict_value_29;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_dict_key_30;
        PyObject *tmp_dict_value_30;
        PyObject *tmp_source_name_13;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_dict_key_31;
        PyObject *tmp_dict_value_31;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_dict_key_32;
        PyObject *tmp_dict_value_32;
        PyObject *tmp_source_name_15;
        PyObject *tmp_mvar_value_36;
        tmp_dict_key_1 = const_str_plain_odd;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_odd );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_odd );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_dict_value_1 = tmp_mvar_value_5;
        tmp_assign_source_32 = _PyDict_NewPresized( 32 );
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_even;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_even );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_even );
        }

        CHECK_OBJECT( tmp_mvar_value_6 );
        tmp_dict_value_2 = tmp_mvar_value_6;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_divisibleby;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_divisibleby );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_divisibleby );
        }

        CHECK_OBJECT( tmp_mvar_value_7 );
        tmp_dict_value_3 = tmp_mvar_value_7;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_defined;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_defined );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_defined );
        }

        CHECK_OBJECT( tmp_mvar_value_8 );
        tmp_dict_value_4 = tmp_mvar_value_8;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_undefined;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_undefined );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_undefined );
        }

        CHECK_OBJECT( tmp_mvar_value_9 );
        tmp_dict_value_5 = tmp_mvar_value_9;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_none;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_none );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_none );
        }

        CHECK_OBJECT( tmp_mvar_value_10 );
        tmp_dict_value_6 = tmp_mvar_value_10;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_lower;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_lower );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_lower );
        }

        CHECK_OBJECT( tmp_mvar_value_11 );
        tmp_dict_value_7 = tmp_mvar_value_11;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_upper;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_upper );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_upper );
        }

        CHECK_OBJECT( tmp_mvar_value_12 );
        tmp_dict_value_8 = tmp_mvar_value_12;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_plain_string;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_string );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_string );
        }

        CHECK_OBJECT( tmp_mvar_value_13 );
        tmp_dict_value_9 = tmp_mvar_value_13;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_mapping;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_mapping );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_mapping );
        }

        CHECK_OBJECT( tmp_mvar_value_14 );
        tmp_dict_value_10 = tmp_mvar_value_14;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_11 = const_str_plain_number;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_number );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_number );
        }

        CHECK_OBJECT( tmp_mvar_value_15 );
        tmp_dict_value_11 = tmp_mvar_value_15;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_12 = const_str_plain_sequence;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_sequence );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_sequence );
        }

        CHECK_OBJECT( tmp_mvar_value_16 );
        tmp_dict_value_12 = tmp_mvar_value_16;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_12, tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_13 = const_str_plain_iterable;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_iterable );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_iterable );
        }

        CHECK_OBJECT( tmp_mvar_value_17 );
        tmp_dict_value_13 = tmp_mvar_value_17;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_13, tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_14 = const_str_plain_callable;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_callable );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_callable );
        }

        CHECK_OBJECT( tmp_mvar_value_18 );
        tmp_dict_value_14 = tmp_mvar_value_18;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_14, tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_15 = const_str_plain_sameas;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_sameas );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_sameas );
        }

        CHECK_OBJECT( tmp_mvar_value_19 );
        tmp_dict_value_15 = tmp_mvar_value_19;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_15, tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_16 = const_str_plain_escaped;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_escaped );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_escaped );
        }

        CHECK_OBJECT( tmp_mvar_value_20 );
        tmp_dict_value_16 = tmp_mvar_value_20;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_16, tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_17 = const_str_plain_in;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_test_in );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_test_in );
        }

        CHECK_OBJECT( tmp_mvar_value_21 );
        tmp_dict_value_17 = tmp_mvar_value_21;
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_17, tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_18 = const_str_digest_13243295198ba360687160a687a3cc51;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_22;
        tmp_dict_value_18 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_eq );
        if ( tmp_dict_value_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_18, tmp_dict_value_18 );
        Py_DECREF( tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_19 = const_str_plain_eq;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 161;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_23;
        tmp_dict_value_19 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_eq );
        if ( tmp_dict_value_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 161;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_19, tmp_dict_value_19 );
        Py_DECREF( tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_20 = const_str_plain_equalto;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_24;
        tmp_dict_value_20 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_eq );
        if ( tmp_dict_value_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 162;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_20, tmp_dict_value_20 );
        Py_DECREF( tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_21 = const_str_digest_5f408ea264aad5c192d303d32799c57f;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_25 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_25;
        tmp_dict_value_21 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_ne );
        if ( tmp_dict_value_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_21, tmp_dict_value_21 );
        Py_DECREF( tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_22 = const_str_plain_ne;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_26;
        tmp_dict_value_22 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_ne );
        if ( tmp_dict_value_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_22, tmp_dict_value_22 );
        Py_DECREF( tmp_dict_value_22 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_23 = const_str_chr_62;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_27;
        tmp_dict_value_23 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_gt );
        if ( tmp_dict_value_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_23, tmp_dict_value_23 );
        Py_DECREF( tmp_dict_value_23 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_24 = const_str_plain_gt;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_28;
        tmp_dict_value_24 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_gt );
        if ( tmp_dict_value_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_24, tmp_dict_value_24 );
        Py_DECREF( tmp_dict_value_24 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_25 = const_str_plain_greaterthan;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_29;
        tmp_dict_value_25 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_gt );
        if ( tmp_dict_value_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_25, tmp_dict_value_25 );
        Py_DECREF( tmp_dict_value_25 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_26 = const_str_plain_ge;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 168;

            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_30;
        tmp_dict_value_26 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_ge );
        if ( tmp_dict_value_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 168;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_26, tmp_dict_value_26 );
        Py_DECREF( tmp_dict_value_26 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_27 = const_str_digest_a53e6044afa65158b90d5bc4937bebce;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_31 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_31;
        tmp_dict_value_27 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_ge );
        if ( tmp_dict_value_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_27, tmp_dict_value_27 );
        Py_DECREF( tmp_dict_value_27 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_28 = const_str_chr_60;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 170;

            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_32;
        tmp_dict_value_28 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_lt );
        if ( tmp_dict_value_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 170;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_28, tmp_dict_value_28 );
        Py_DECREF( tmp_dict_value_28 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_29 = const_str_plain_lt;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_33;
        tmp_dict_value_29 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_lt );
        if ( tmp_dict_value_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_29, tmp_dict_value_29 );
        Py_DECREF( tmp_dict_value_29 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_30 = const_str_plain_lessthan;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }

        tmp_source_name_13 = tmp_mvar_value_34;
        tmp_dict_value_30 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_lt );
        if ( tmp_dict_value_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_30, tmp_dict_value_30 );
        Py_DECREF( tmp_dict_value_30 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_31 = const_str_digest_c566e8f3fba64199b66b7b7424440c9c;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_35 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_35;
        tmp_dict_value_31 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_le );
        if ( tmp_dict_value_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_31, tmp_dict_value_31 );
        Py_DECREF( tmp_dict_value_31 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_32 = const_str_plain_le;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_36 == NULL )
        {
            Py_DECREF( tmp_assign_source_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }

        tmp_source_name_15 = tmp_mvar_value_36;
        tmp_dict_value_32 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_le );
        if ( tmp_dict_value_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_32 );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_32, tmp_dict_key_32, tmp_dict_value_32 );
        Py_DECREF( tmp_dict_value_32 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_jinja2$tests, (Nuitka_StringObject *)const_str_plain_TESTS, tmp_assign_source_32 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_eaa60a7b55e03d94cc42a76fc3910735 );
#endif
    popFrameStack();

    assertFrameObject( frame_eaa60a7b55e03d94cc42a76fc3910735 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_eaa60a7b55e03d94cc42a76fc3910735 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eaa60a7b55e03d94cc42a76fc3910735, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eaa60a7b55e03d94cc42a76fc3910735->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eaa60a7b55e03d94cc42a76fc3910735, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_jinja2$tests );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
