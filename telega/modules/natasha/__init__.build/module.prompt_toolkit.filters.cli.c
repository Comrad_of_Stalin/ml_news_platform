/* Generated code for Python module 'prompt_toolkit.filters.cli'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$filters$cli" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$filters$cli;
PyDictObject *moduledict_prompt_toolkit$filters$cli;

/* The declarations of module constants used, if any. */
static PyObject *const_str_plain_EmacsInsertMode;
extern PyObject *const_str_plain_vi_selection_mode;
static PyObject *const_str_plain_RendererHeightIsKnown;
static PyObject *const_str_plain_IsSearching;
extern PyObject *const_str_plain_is_done;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_unicode_literals;
static PyObject *const_str_plain_HasValidationError;
static PyObject *const_str_plain_ViReplaceMode;
extern PyObject *const_str_plain_in_paste_mode;
extern PyObject *const_str_plain_vi_digraph_mode;
extern PyObject *const_str_plain_is_multiline;
extern PyObject *const_str_plain___all__;
extern PyObject *const_str_plain_has_focus;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain_HasFocus;
static PyObject *const_str_digest_2935874cc0f5c88427b0dadf723b966e;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_vi_waiting_for_text_object_mode;
static PyObject *const_str_plain_EmacsSelectionMode;
static PyObject *const_str_plain_HasSearch;
static PyObject *const_str_plain_InEditingMode;
extern PyObject *const_str_plain_vi_insert_multiple_mode;
extern PyObject *const_str_plain_has_validation_error;
extern PyObject *const_str_plain_is_searching;
extern PyObject *const_str_plain_has_selection;
extern PyObject *const_tuple_str_chr_42_tuple;
extern PyObject *const_str_plain_vi_navigation_mode;
static PyObject *const_str_plain_ViNavigationMode;
static PyObject *const_str_plain_InPasteMode;
extern PyObject *const_str_plain_in_editing_mode;
static PyObject *const_str_plain_IsDone;
static PyObject *const_str_plain_HasArg;
static PyObject *const_list_f75ba3bb8e804abbf278622059d4d8a5_list;
static PyObject *const_str_plain_ViWaitingForTextObjectMode;
extern PyObject *const_str_plain_has_arg;
extern PyObject *const_str_plain_vi_mode;
static PyObject *const_str_plain_ControlIsSearchable;
extern PyObject *const_str_plain_renderer_height_is_known;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_EmacsMode;
static PyObject *const_str_plain_ViInsertMode;
extern PyObject *const_str_chr_42;
extern PyObject *const_str_plain_control_is_searchable;
static PyObject *const_str_plain_IsMultiline;
static PyObject *const_str_plain_ViSelectionMode;
extern PyObject *const_str_plain_vi_insert_mode;
static PyObject *const_str_plain_ViMode;
static PyObject *const_str_plain_ViDigraphMode;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_ViInsertMultipleMode;
extern PyObject *const_str_plain_emacs_mode;
extern PyObject *const_str_plain_has_completions;
extern PyObject *const_str_plain_is_read_only;
extern PyObject *const_str_plain_emacs_insert_mode;
extern PyObject *const_str_angle_lambda;
static PyObject *const_str_plain_HasSelection;
extern PyObject *const_str_plain_emacs_selection_mode;
extern PyObject *const_str_plain_app;
extern PyObject *const_str_plain_vi_replace_mode;
static PyObject *const_str_digest_8a0caa3e7185baf8afb32f7c6f33e0fe;
static PyObject *const_str_digest_000de22bb6068486f77d2f4464847814;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain_IsReadOnly;
static PyObject *const_str_plain_HasCompletions;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_EmacsInsertMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653059 ], 15, 1 );
    const_str_plain_RendererHeightIsKnown = UNSTREAM_STRING_ASCII( &constant_bin[ 4653074 ], 21, 1 );
    const_str_plain_IsSearching = UNSTREAM_STRING_ASCII( &constant_bin[ 4653095 ], 11, 1 );
    const_str_plain_HasValidationError = UNSTREAM_STRING_ASCII( &constant_bin[ 4653106 ], 18, 1 );
    const_str_plain_ViReplaceMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653124 ], 13, 1 );
    const_str_plain_HasFocus = UNSTREAM_STRING_ASCII( &constant_bin[ 4653137 ], 8, 1 );
    const_str_digest_2935874cc0f5c88427b0dadf723b966e = UNSTREAM_STRING_ASCII( &constant_bin[ 4653145 ], 35, 0 );
    const_str_plain_EmacsSelectionMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653180 ], 18, 1 );
    const_str_plain_HasSearch = UNSTREAM_STRING_ASCII( &constant_bin[ 4647709 ], 9, 1 );
    const_str_plain_InEditingMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653198 ], 13, 1 );
    const_str_plain_ViNavigationMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653211 ], 16, 1 );
    const_str_plain_InPasteMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653227 ], 11, 1 );
    const_str_plain_IsDone = UNSTREAM_STRING_ASCII( &constant_bin[ 4653238 ], 6, 1 );
    const_str_plain_HasArg = UNSTREAM_STRING_ASCII( &constant_bin[ 4653244 ], 6, 1 );
    const_list_f75ba3bb8e804abbf278622059d4d8a5_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 4653250 ], 387 );
    const_str_plain_ViWaitingForTextObjectMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653503 ], 26, 1 );
    const_str_plain_ControlIsSearchable = UNSTREAM_STRING_ASCII( &constant_bin[ 4653618 ], 19, 1 );
    const_str_plain_EmacsMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653546 ], 9, 1 );
    const_str_plain_ViInsertMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653435 ], 12, 1 );
    const_str_plain_IsMultiline = UNSTREAM_STRING_ASCII( &constant_bin[ 4653345 ], 11, 1 );
    const_str_plain_ViSelectionMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653486 ], 15, 1 );
    const_str_plain_ViMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653409 ], 6, 1 );
    const_str_plain_ViDigraphMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653531 ], 13, 1 );
    const_str_plain_ViInsertMultipleMode = UNSTREAM_STRING_ASCII( &constant_bin[ 4653449 ], 20, 1 );
    const_str_plain_HasSelection = UNSTREAM_STRING_ASCII( &constant_bin[ 4653291 ], 12, 1 );
    const_str_digest_8a0caa3e7185baf8afb32f7c6f33e0fe = UNSTREAM_STRING_ASCII( &constant_bin[ 4653153 ], 26, 0 );
    const_str_digest_000de22bb6068486f77d2f4464847814 = UNSTREAM_STRING_ASCII( &constant_bin[ 4653637 ], 29, 0 );
    const_str_plain_IsReadOnly = UNSTREAM_STRING_ASCII( &constant_bin[ 4653333 ], 10, 1 );
    const_str_plain_HasCompletions = UNSTREAM_STRING_ASCII( &constant_bin[ 4653265 ], 14, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$filters$cli( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_290bc47af887053c204821758857f685;
static PyCodeObject *codeobj_cbc88715f10d16dd9d969e8cd928b59b;
static PyCodeObject *codeobj_dab2f80ad874b81530872225ad446f98;
static PyCodeObject *codeobj_f7e08ba1fab71ffee1cc02a891e6fbd1;
static PyCodeObject *codeobj_bb9b482e7ccf1a728adfa65dbdc1f21c;
static PyCodeObject *codeobj_02904fefb5c952061b1a53f8ed8a8600;
static PyCodeObject *codeobj_e31e13f7906f6854a46a1a8788117157;
static PyCodeObject *codeobj_c785c11a2831d8e6146d4ac7faaf6ee9;
static PyCodeObject *codeobj_51ec85bb7fa31f576b7249223f8aa90d;
static PyCodeObject *codeobj_b2ac080496a9e4ddc32785ee8f46dc3d;
static PyCodeObject *codeobj_dc54d457d0ebeb58df91267aa76ef63c;
static PyCodeObject *codeobj_072e4def420ed650df1b37036de59aa2;
static PyCodeObject *codeobj_a211800129fc53aeb78cba6f82fb8ff0;
static PyCodeObject *codeobj_6c010521069a9b2e64b677869a1c0eae;
static PyCodeObject *codeobj_a182aa200d38edea1443661d32edae87;
static PyCodeObject *codeobj_fdbf988ea4be76652aa1d022c3835728;
static PyCodeObject *codeobj_5423d50312a3e130e5a8fb0ad7ca9486;
static PyCodeObject *codeobj_faa99b3e22bbd023be141dd1f2e13058;
static PyCodeObject *codeobj_e3508652a80c1e8b696a44ef7dc0c251;
static PyCodeObject *codeobj_96129e0ffded0b2773aa202e4a2c00b3;
static PyCodeObject *codeobj_1dabcf5c8c47879561da184f1a4e649a;
static PyCodeObject *codeobj_9e2db0ea764f50d59c67b0875ab78c45;
static PyCodeObject *codeobj_f11379480be3570771d48f03238d6d58;
static PyCodeObject *codeobj_70fb417f52187b6e019fd89a32ae3549;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_000de22bb6068486f77d2f4464847814 );
    codeobj_290bc47af887053c204821758857f685 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 40, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_cbc88715f10d16dd9d969e8cd928b59b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 41, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_dab2f80ad874b81530872225ad446f98 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 42, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f7e08ba1fab71ffee1cc02a891e6fbd1 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 43, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_bb9b482e7ccf1a728adfa65dbdc1f21c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 44, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_02904fefb5c952061b1a53f8ed8a8600 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 45, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e31e13f7906f6854a46a1a8788117157 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 46, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c785c11a2831d8e6146d4ac7faaf6ee9 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 47, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_51ec85bb7fa31f576b7249223f8aa90d = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 48, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b2ac080496a9e4ddc32785ee8f46dc3d = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 49, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_dc54d457d0ebeb58df91267aa76ef63c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 50, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_072e4def420ed650df1b37036de59aa2 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 51, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a211800129fc53aeb78cba6f82fb8ff0 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 52, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6c010521069a9b2e64b677869a1c0eae = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 53, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a182aa200d38edea1443661d32edae87 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 54, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_fdbf988ea4be76652aa1d022c3835728 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 55, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5423d50312a3e130e5a8fb0ad7ca9486 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 56, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_faa99b3e22bbd023be141dd1f2e13058 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 57, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e3508652a80c1e8b696a44ef7dc0c251 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 58, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_96129e0ffded0b2773aa202e4a2c00b3 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 59, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1dabcf5c8c47879561da184f1a4e649a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 60, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9e2db0ea764f50d59c67b0875ab78c45 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 61, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f11379480be3570771d48f03238d6d58 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 62, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_70fb417f52187b6e019fd89a32ae3549 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_2935874cc0f5c88427b0dadf723b966e, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_10_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_11_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_12_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_13_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_14_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_15_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_16_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_17_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_18_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_19_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_20_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_21_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_22_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_23_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_2_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_3_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_4_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_5_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_6_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_7_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_8_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_9_lambda(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$filters$cli$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_290bc47af887053c204821758857f685;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_290bc47af887053c204821758857f685 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_290bc47af887053c204821758857f685, codeobj_290bc47af887053c204821758857f685, module_prompt_toolkit$filters$cli, 0 );
    frame_290bc47af887053c204821758857f685 = cache_frame_290bc47af887053c204821758857f685;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_290bc47af887053c204821758857f685 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_290bc47af887053c204821758857f685 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_has_validation_error );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_validation_error );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_validation_error" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_290bc47af887053c204821758857f685 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_290bc47af887053c204821758857f685 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_290bc47af887053c204821758857f685 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_290bc47af887053c204821758857f685, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_290bc47af887053c204821758857f685->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_290bc47af887053c204821758857f685, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_290bc47af887053c204821758857f685,
        type_description_1
    );


    // Release cached frame.
    if ( frame_290bc47af887053c204821758857f685 == cache_frame_290bc47af887053c204821758857f685 )
    {
        Py_DECREF( frame_290bc47af887053c204821758857f685 );
    }
    cache_frame_290bc47af887053c204821758857f685 = NULL;

    assertFrameObject( frame_290bc47af887053c204821758857f685 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_2_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_cbc88715f10d16dd9d969e8cd928b59b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_cbc88715f10d16dd9d969e8cd928b59b = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_cbc88715f10d16dd9d969e8cd928b59b, codeobj_cbc88715f10d16dd9d969e8cd928b59b, module_prompt_toolkit$filters$cli, 0 );
    frame_cbc88715f10d16dd9d969e8cd928b59b = cache_frame_cbc88715f10d16dd9d969e8cd928b59b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cbc88715f10d16dd9d969e8cd928b59b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cbc88715f10d16dd9d969e8cd928b59b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_has_arg );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_arg );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_arg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cbc88715f10d16dd9d969e8cd928b59b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cbc88715f10d16dd9d969e8cd928b59b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cbc88715f10d16dd9d969e8cd928b59b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cbc88715f10d16dd9d969e8cd928b59b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cbc88715f10d16dd9d969e8cd928b59b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cbc88715f10d16dd9d969e8cd928b59b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cbc88715f10d16dd9d969e8cd928b59b,
        type_description_1
    );


    // Release cached frame.
    if ( frame_cbc88715f10d16dd9d969e8cd928b59b == cache_frame_cbc88715f10d16dd9d969e8cd928b59b )
    {
        Py_DECREF( frame_cbc88715f10d16dd9d969e8cd928b59b );
    }
    cache_frame_cbc88715f10d16dd9d969e8cd928b59b = NULL;

    assertFrameObject( frame_cbc88715f10d16dd9d969e8cd928b59b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_2_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_3_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_dab2f80ad874b81530872225ad446f98;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_dab2f80ad874b81530872225ad446f98 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_dab2f80ad874b81530872225ad446f98, codeobj_dab2f80ad874b81530872225ad446f98, module_prompt_toolkit$filters$cli, 0 );
    frame_dab2f80ad874b81530872225ad446f98 = cache_frame_dab2f80ad874b81530872225ad446f98;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dab2f80ad874b81530872225ad446f98 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dab2f80ad874b81530872225ad446f98 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_is_done );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_done );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_done" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dab2f80ad874b81530872225ad446f98 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dab2f80ad874b81530872225ad446f98 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dab2f80ad874b81530872225ad446f98 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dab2f80ad874b81530872225ad446f98, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dab2f80ad874b81530872225ad446f98->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dab2f80ad874b81530872225ad446f98, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dab2f80ad874b81530872225ad446f98,
        type_description_1
    );


    // Release cached frame.
    if ( frame_dab2f80ad874b81530872225ad446f98 == cache_frame_dab2f80ad874b81530872225ad446f98 )
    {
        Py_DECREF( frame_dab2f80ad874b81530872225ad446f98 );
    }
    cache_frame_dab2f80ad874b81530872225ad446f98 = NULL;

    assertFrameObject( frame_dab2f80ad874b81530872225ad446f98 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_3_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_4_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_f7e08ba1fab71ffee1cc02a891e6fbd1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f7e08ba1fab71ffee1cc02a891e6fbd1 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_f7e08ba1fab71ffee1cc02a891e6fbd1, codeobj_f7e08ba1fab71ffee1cc02a891e6fbd1, module_prompt_toolkit$filters$cli, 0 );
    frame_f7e08ba1fab71ffee1cc02a891e6fbd1 = cache_frame_f7e08ba1fab71ffee1cc02a891e6fbd1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f7e08ba1fab71ffee1cc02a891e6fbd1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f7e08ba1fab71ffee1cc02a891e6fbd1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_renderer_height_is_known );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_renderer_height_is_known );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "renderer_height_is_known" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f7e08ba1fab71ffee1cc02a891e6fbd1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f7e08ba1fab71ffee1cc02a891e6fbd1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f7e08ba1fab71ffee1cc02a891e6fbd1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f7e08ba1fab71ffee1cc02a891e6fbd1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f7e08ba1fab71ffee1cc02a891e6fbd1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f7e08ba1fab71ffee1cc02a891e6fbd1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f7e08ba1fab71ffee1cc02a891e6fbd1,
        type_description_1
    );


    // Release cached frame.
    if ( frame_f7e08ba1fab71ffee1cc02a891e6fbd1 == cache_frame_f7e08ba1fab71ffee1cc02a891e6fbd1 )
    {
        Py_DECREF( frame_f7e08ba1fab71ffee1cc02a891e6fbd1 );
    }
    cache_frame_f7e08ba1fab71ffee1cc02a891e6fbd1 = NULL;

    assertFrameObject( frame_f7e08ba1fab71ffee1cc02a891e6fbd1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_4_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_5_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_bb9b482e7ccf1a728adfa65dbdc1f21c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_bb9b482e7ccf1a728adfa65dbdc1f21c = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_bb9b482e7ccf1a728adfa65dbdc1f21c, codeobj_bb9b482e7ccf1a728adfa65dbdc1f21c, module_prompt_toolkit$filters$cli, 0 );
    frame_bb9b482e7ccf1a728adfa65dbdc1f21c = cache_frame_bb9b482e7ccf1a728adfa65dbdc1f21c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bb9b482e7ccf1a728adfa65dbdc1f21c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bb9b482e7ccf1a728adfa65dbdc1f21c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_vi_navigation_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_navigation_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_navigation_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bb9b482e7ccf1a728adfa65dbdc1f21c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bb9b482e7ccf1a728adfa65dbdc1f21c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bb9b482e7ccf1a728adfa65dbdc1f21c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bb9b482e7ccf1a728adfa65dbdc1f21c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bb9b482e7ccf1a728adfa65dbdc1f21c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bb9b482e7ccf1a728adfa65dbdc1f21c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bb9b482e7ccf1a728adfa65dbdc1f21c,
        type_description_1
    );


    // Release cached frame.
    if ( frame_bb9b482e7ccf1a728adfa65dbdc1f21c == cache_frame_bb9b482e7ccf1a728adfa65dbdc1f21c )
    {
        Py_DECREF( frame_bb9b482e7ccf1a728adfa65dbdc1f21c );
    }
    cache_frame_bb9b482e7ccf1a728adfa65dbdc1f21c = NULL;

    assertFrameObject( frame_bb9b482e7ccf1a728adfa65dbdc1f21c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_5_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_6_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_02904fefb5c952061b1a53f8ed8a8600;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_02904fefb5c952061b1a53f8ed8a8600 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_02904fefb5c952061b1a53f8ed8a8600, codeobj_02904fefb5c952061b1a53f8ed8a8600, module_prompt_toolkit$filters$cli, 0 );
    frame_02904fefb5c952061b1a53f8ed8a8600 = cache_frame_02904fefb5c952061b1a53f8ed8a8600;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_02904fefb5c952061b1a53f8ed8a8600 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_02904fefb5c952061b1a53f8ed8a8600 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_in_paste_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_paste_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_paste_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02904fefb5c952061b1a53f8ed8a8600 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_02904fefb5c952061b1a53f8ed8a8600 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02904fefb5c952061b1a53f8ed8a8600 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_02904fefb5c952061b1a53f8ed8a8600, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_02904fefb5c952061b1a53f8ed8a8600->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_02904fefb5c952061b1a53f8ed8a8600, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_02904fefb5c952061b1a53f8ed8a8600,
        type_description_1
    );


    // Release cached frame.
    if ( frame_02904fefb5c952061b1a53f8ed8a8600 == cache_frame_02904fefb5c952061b1a53f8ed8a8600 )
    {
        Py_DECREF( frame_02904fefb5c952061b1a53f8ed8a8600 );
    }
    cache_frame_02904fefb5c952061b1a53f8ed8a8600 = NULL;

    assertFrameObject( frame_02904fefb5c952061b1a53f8ed8a8600 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_6_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_7_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_e31e13f7906f6854a46a1a8788117157;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e31e13f7906f6854a46a1a8788117157 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_e31e13f7906f6854a46a1a8788117157, codeobj_e31e13f7906f6854a46a1a8788117157, module_prompt_toolkit$filters$cli, 0 );
    frame_e31e13f7906f6854a46a1a8788117157 = cache_frame_e31e13f7906f6854a46a1a8788117157;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e31e13f7906f6854a46a1a8788117157 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e31e13f7906f6854a46a1a8788117157 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_emacs_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_emacs_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "emacs_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e31e13f7906f6854a46a1a8788117157 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e31e13f7906f6854a46a1a8788117157 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e31e13f7906f6854a46a1a8788117157 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e31e13f7906f6854a46a1a8788117157, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e31e13f7906f6854a46a1a8788117157->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e31e13f7906f6854a46a1a8788117157, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e31e13f7906f6854a46a1a8788117157,
        type_description_1
    );


    // Release cached frame.
    if ( frame_e31e13f7906f6854a46a1a8788117157 == cache_frame_e31e13f7906f6854a46a1a8788117157 )
    {
        Py_DECREF( frame_e31e13f7906f6854a46a1a8788117157 );
    }
    cache_frame_e31e13f7906f6854a46a1a8788117157 = NULL;

    assertFrameObject( frame_e31e13f7906f6854a46a1a8788117157 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_7_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_8_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_c785c11a2831d8e6146d4ac7faaf6ee9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c785c11a2831d8e6146d4ac7faaf6ee9 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_c785c11a2831d8e6146d4ac7faaf6ee9, codeobj_c785c11a2831d8e6146d4ac7faaf6ee9, module_prompt_toolkit$filters$cli, 0 );
    frame_c785c11a2831d8e6146d4ac7faaf6ee9 = cache_frame_c785c11a2831d8e6146d4ac7faaf6ee9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c785c11a2831d8e6146d4ac7faaf6ee9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c785c11a2831d8e6146d4ac7faaf6ee9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_emacs_insert_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_emacs_insert_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "emacs_insert_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c785c11a2831d8e6146d4ac7faaf6ee9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c785c11a2831d8e6146d4ac7faaf6ee9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c785c11a2831d8e6146d4ac7faaf6ee9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c785c11a2831d8e6146d4ac7faaf6ee9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c785c11a2831d8e6146d4ac7faaf6ee9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c785c11a2831d8e6146d4ac7faaf6ee9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c785c11a2831d8e6146d4ac7faaf6ee9,
        type_description_1
    );


    // Release cached frame.
    if ( frame_c785c11a2831d8e6146d4ac7faaf6ee9 == cache_frame_c785c11a2831d8e6146d4ac7faaf6ee9 )
    {
        Py_DECREF( frame_c785c11a2831d8e6146d4ac7faaf6ee9 );
    }
    cache_frame_c785c11a2831d8e6146d4ac7faaf6ee9 = NULL;

    assertFrameObject( frame_c785c11a2831d8e6146d4ac7faaf6ee9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_8_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_9_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_51ec85bb7fa31f576b7249223f8aa90d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_51ec85bb7fa31f576b7249223f8aa90d = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_51ec85bb7fa31f576b7249223f8aa90d, codeobj_51ec85bb7fa31f576b7249223f8aa90d, module_prompt_toolkit$filters$cli, 0 );
    frame_51ec85bb7fa31f576b7249223f8aa90d = cache_frame_51ec85bb7fa31f576b7249223f8aa90d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_51ec85bb7fa31f576b7249223f8aa90d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_51ec85bb7fa31f576b7249223f8aa90d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_vi_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_51ec85bb7fa31f576b7249223f8aa90d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_51ec85bb7fa31f576b7249223f8aa90d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_51ec85bb7fa31f576b7249223f8aa90d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_51ec85bb7fa31f576b7249223f8aa90d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_51ec85bb7fa31f576b7249223f8aa90d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_51ec85bb7fa31f576b7249223f8aa90d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_51ec85bb7fa31f576b7249223f8aa90d,
        type_description_1
    );


    // Release cached frame.
    if ( frame_51ec85bb7fa31f576b7249223f8aa90d == cache_frame_51ec85bb7fa31f576b7249223f8aa90d )
    {
        Py_DECREF( frame_51ec85bb7fa31f576b7249223f8aa90d );
    }
    cache_frame_51ec85bb7fa31f576b7249223f8aa90d = NULL;

    assertFrameObject( frame_51ec85bb7fa31f576b7249223f8aa90d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_9_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_10_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_b2ac080496a9e4ddc32785ee8f46dc3d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b2ac080496a9e4ddc32785ee8f46dc3d = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_b2ac080496a9e4ddc32785ee8f46dc3d, codeobj_b2ac080496a9e4ddc32785ee8f46dc3d, module_prompt_toolkit$filters$cli, 0 );
    frame_b2ac080496a9e4ddc32785ee8f46dc3d = cache_frame_b2ac080496a9e4ddc32785ee8f46dc3d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b2ac080496a9e4ddc32785ee8f46dc3d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b2ac080496a9e4ddc32785ee8f46dc3d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_is_searching );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_searching );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_searching" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 49;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2ac080496a9e4ddc32785ee8f46dc3d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2ac080496a9e4ddc32785ee8f46dc3d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2ac080496a9e4ddc32785ee8f46dc3d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b2ac080496a9e4ddc32785ee8f46dc3d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b2ac080496a9e4ddc32785ee8f46dc3d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b2ac080496a9e4ddc32785ee8f46dc3d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b2ac080496a9e4ddc32785ee8f46dc3d,
        type_description_1
    );


    // Release cached frame.
    if ( frame_b2ac080496a9e4ddc32785ee8f46dc3d == cache_frame_b2ac080496a9e4ddc32785ee8f46dc3d )
    {
        Py_DECREF( frame_b2ac080496a9e4ddc32785ee8f46dc3d );
    }
    cache_frame_b2ac080496a9e4ddc32785ee8f46dc3d = NULL;

    assertFrameObject( frame_b2ac080496a9e4ddc32785ee8f46dc3d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_10_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_11_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_dc54d457d0ebeb58df91267aa76ef63c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_dc54d457d0ebeb58df91267aa76ef63c = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_dc54d457d0ebeb58df91267aa76ef63c, codeobj_dc54d457d0ebeb58df91267aa76ef63c, module_prompt_toolkit$filters$cli, 0 );
    frame_dc54d457d0ebeb58df91267aa76ef63c = cache_frame_dc54d457d0ebeb58df91267aa76ef63c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dc54d457d0ebeb58df91267aa76ef63c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dc54d457d0ebeb58df91267aa76ef63c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_is_searching );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_searching );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_searching" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 50;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc54d457d0ebeb58df91267aa76ef63c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc54d457d0ebeb58df91267aa76ef63c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc54d457d0ebeb58df91267aa76ef63c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dc54d457d0ebeb58df91267aa76ef63c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dc54d457d0ebeb58df91267aa76ef63c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dc54d457d0ebeb58df91267aa76ef63c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dc54d457d0ebeb58df91267aa76ef63c,
        type_description_1
    );


    // Release cached frame.
    if ( frame_dc54d457d0ebeb58df91267aa76ef63c == cache_frame_dc54d457d0ebeb58df91267aa76ef63c )
    {
        Py_DECREF( frame_dc54d457d0ebeb58df91267aa76ef63c );
    }
    cache_frame_dc54d457d0ebeb58df91267aa76ef63c = NULL;

    assertFrameObject( frame_dc54d457d0ebeb58df91267aa76ef63c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_11_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_12_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_072e4def420ed650df1b37036de59aa2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_072e4def420ed650df1b37036de59aa2 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_072e4def420ed650df1b37036de59aa2, codeobj_072e4def420ed650df1b37036de59aa2, module_prompt_toolkit$filters$cli, 0 );
    frame_072e4def420ed650df1b37036de59aa2 = cache_frame_072e4def420ed650df1b37036de59aa2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_072e4def420ed650df1b37036de59aa2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_072e4def420ed650df1b37036de59aa2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_control_is_searchable );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_control_is_searchable );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "control_is_searchable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_072e4def420ed650df1b37036de59aa2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_072e4def420ed650df1b37036de59aa2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_072e4def420ed650df1b37036de59aa2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_072e4def420ed650df1b37036de59aa2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_072e4def420ed650df1b37036de59aa2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_072e4def420ed650df1b37036de59aa2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_072e4def420ed650df1b37036de59aa2,
        type_description_1
    );


    // Release cached frame.
    if ( frame_072e4def420ed650df1b37036de59aa2 == cache_frame_072e4def420ed650df1b37036de59aa2 )
    {
        Py_DECREF( frame_072e4def420ed650df1b37036de59aa2 );
    }
    cache_frame_072e4def420ed650df1b37036de59aa2 = NULL;

    assertFrameObject( frame_072e4def420ed650df1b37036de59aa2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_12_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_13_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_a211800129fc53aeb78cba6f82fb8ff0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a211800129fc53aeb78cba6f82fb8ff0 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_a211800129fc53aeb78cba6f82fb8ff0, codeobj_a211800129fc53aeb78cba6f82fb8ff0, module_prompt_toolkit$filters$cli, 0 );
    frame_a211800129fc53aeb78cba6f82fb8ff0 = cache_frame_a211800129fc53aeb78cba6f82fb8ff0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a211800129fc53aeb78cba6f82fb8ff0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a211800129fc53aeb78cba6f82fb8ff0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_emacs_selection_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_emacs_selection_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "emacs_selection_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a211800129fc53aeb78cba6f82fb8ff0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a211800129fc53aeb78cba6f82fb8ff0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a211800129fc53aeb78cba6f82fb8ff0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a211800129fc53aeb78cba6f82fb8ff0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a211800129fc53aeb78cba6f82fb8ff0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a211800129fc53aeb78cba6f82fb8ff0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a211800129fc53aeb78cba6f82fb8ff0,
        type_description_1
    );


    // Release cached frame.
    if ( frame_a211800129fc53aeb78cba6f82fb8ff0 == cache_frame_a211800129fc53aeb78cba6f82fb8ff0 )
    {
        Py_DECREF( frame_a211800129fc53aeb78cba6f82fb8ff0 );
    }
    cache_frame_a211800129fc53aeb78cba6f82fb8ff0 = NULL;

    assertFrameObject( frame_a211800129fc53aeb78cba6f82fb8ff0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_13_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_14_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_6c010521069a9b2e64b677869a1c0eae;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6c010521069a9b2e64b677869a1c0eae = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_6c010521069a9b2e64b677869a1c0eae, codeobj_6c010521069a9b2e64b677869a1c0eae, module_prompt_toolkit$filters$cli, 0 );
    frame_6c010521069a9b2e64b677869a1c0eae = cache_frame_6c010521069a9b2e64b677869a1c0eae;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6c010521069a9b2e64b677869a1c0eae );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6c010521069a9b2e64b677869a1c0eae ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_vi_digraph_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_digraph_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_digraph_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6c010521069a9b2e64b677869a1c0eae );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6c010521069a9b2e64b677869a1c0eae );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6c010521069a9b2e64b677869a1c0eae );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6c010521069a9b2e64b677869a1c0eae, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6c010521069a9b2e64b677869a1c0eae->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6c010521069a9b2e64b677869a1c0eae, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6c010521069a9b2e64b677869a1c0eae,
        type_description_1
    );


    // Release cached frame.
    if ( frame_6c010521069a9b2e64b677869a1c0eae == cache_frame_6c010521069a9b2e64b677869a1c0eae )
    {
        Py_DECREF( frame_6c010521069a9b2e64b677869a1c0eae );
    }
    cache_frame_6c010521069a9b2e64b677869a1c0eae = NULL;

    assertFrameObject( frame_6c010521069a9b2e64b677869a1c0eae );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_14_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_15_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_a182aa200d38edea1443661d32edae87;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a182aa200d38edea1443661d32edae87 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_a182aa200d38edea1443661d32edae87, codeobj_a182aa200d38edea1443661d32edae87, module_prompt_toolkit$filters$cli, 0 );
    frame_a182aa200d38edea1443661d32edae87 = cache_frame_a182aa200d38edea1443661d32edae87;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a182aa200d38edea1443661d32edae87 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a182aa200d38edea1443661d32edae87 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_vi_waiting_for_text_object_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_waiting_for_text_object_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_waiting_for_text_object_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a182aa200d38edea1443661d32edae87 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a182aa200d38edea1443661d32edae87 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a182aa200d38edea1443661d32edae87 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a182aa200d38edea1443661d32edae87, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a182aa200d38edea1443661d32edae87->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a182aa200d38edea1443661d32edae87, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a182aa200d38edea1443661d32edae87,
        type_description_1
    );


    // Release cached frame.
    if ( frame_a182aa200d38edea1443661d32edae87 == cache_frame_a182aa200d38edea1443661d32edae87 )
    {
        Py_DECREF( frame_a182aa200d38edea1443661d32edae87 );
    }
    cache_frame_a182aa200d38edea1443661d32edae87 = NULL;

    assertFrameObject( frame_a182aa200d38edea1443661d32edae87 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_15_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_16_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_fdbf988ea4be76652aa1d022c3835728;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_fdbf988ea4be76652aa1d022c3835728 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_fdbf988ea4be76652aa1d022c3835728, codeobj_fdbf988ea4be76652aa1d022c3835728, module_prompt_toolkit$filters$cli, 0 );
    frame_fdbf988ea4be76652aa1d022c3835728 = cache_frame_fdbf988ea4be76652aa1d022c3835728;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_fdbf988ea4be76652aa1d022c3835728 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_fdbf988ea4be76652aa1d022c3835728 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_vi_selection_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_selection_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_selection_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fdbf988ea4be76652aa1d022c3835728 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_fdbf988ea4be76652aa1d022c3835728 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fdbf988ea4be76652aa1d022c3835728 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_fdbf988ea4be76652aa1d022c3835728, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_fdbf988ea4be76652aa1d022c3835728->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_fdbf988ea4be76652aa1d022c3835728, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_fdbf988ea4be76652aa1d022c3835728,
        type_description_1
    );


    // Release cached frame.
    if ( frame_fdbf988ea4be76652aa1d022c3835728 == cache_frame_fdbf988ea4be76652aa1d022c3835728 )
    {
        Py_DECREF( frame_fdbf988ea4be76652aa1d022c3835728 );
    }
    cache_frame_fdbf988ea4be76652aa1d022c3835728 = NULL;

    assertFrameObject( frame_fdbf988ea4be76652aa1d022c3835728 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_16_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_17_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_5423d50312a3e130e5a8fb0ad7ca9486;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5423d50312a3e130e5a8fb0ad7ca9486 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_5423d50312a3e130e5a8fb0ad7ca9486, codeobj_5423d50312a3e130e5a8fb0ad7ca9486, module_prompt_toolkit$filters$cli, 0 );
    frame_5423d50312a3e130e5a8fb0ad7ca9486 = cache_frame_5423d50312a3e130e5a8fb0ad7ca9486;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5423d50312a3e130e5a8fb0ad7ca9486 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5423d50312a3e130e5a8fb0ad7ca9486 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_vi_replace_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_replace_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_replace_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 56;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5423d50312a3e130e5a8fb0ad7ca9486 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5423d50312a3e130e5a8fb0ad7ca9486 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5423d50312a3e130e5a8fb0ad7ca9486 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5423d50312a3e130e5a8fb0ad7ca9486, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5423d50312a3e130e5a8fb0ad7ca9486->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5423d50312a3e130e5a8fb0ad7ca9486, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5423d50312a3e130e5a8fb0ad7ca9486,
        type_description_1
    );


    // Release cached frame.
    if ( frame_5423d50312a3e130e5a8fb0ad7ca9486 == cache_frame_5423d50312a3e130e5a8fb0ad7ca9486 )
    {
        Py_DECREF( frame_5423d50312a3e130e5a8fb0ad7ca9486 );
    }
    cache_frame_5423d50312a3e130e5a8fb0ad7ca9486 = NULL;

    assertFrameObject( frame_5423d50312a3e130e5a8fb0ad7ca9486 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_17_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_18_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_faa99b3e22bbd023be141dd1f2e13058;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_faa99b3e22bbd023be141dd1f2e13058 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_faa99b3e22bbd023be141dd1f2e13058, codeobj_faa99b3e22bbd023be141dd1f2e13058, module_prompt_toolkit$filters$cli, 0 );
    frame_faa99b3e22bbd023be141dd1f2e13058 = cache_frame_faa99b3e22bbd023be141dd1f2e13058;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_faa99b3e22bbd023be141dd1f2e13058 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_faa99b3e22bbd023be141dd1f2e13058 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_vi_insert_multiple_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_insert_multiple_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_insert_multiple_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_faa99b3e22bbd023be141dd1f2e13058 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_faa99b3e22bbd023be141dd1f2e13058 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_faa99b3e22bbd023be141dd1f2e13058 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_faa99b3e22bbd023be141dd1f2e13058, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_faa99b3e22bbd023be141dd1f2e13058->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_faa99b3e22bbd023be141dd1f2e13058, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_faa99b3e22bbd023be141dd1f2e13058,
        type_description_1
    );


    // Release cached frame.
    if ( frame_faa99b3e22bbd023be141dd1f2e13058 == cache_frame_faa99b3e22bbd023be141dd1f2e13058 )
    {
        Py_DECREF( frame_faa99b3e22bbd023be141dd1f2e13058 );
    }
    cache_frame_faa99b3e22bbd023be141dd1f2e13058 = NULL;

    assertFrameObject( frame_faa99b3e22bbd023be141dd1f2e13058 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_18_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_19_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_e3508652a80c1e8b696a44ef7dc0c251;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e3508652a80c1e8b696a44ef7dc0c251 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_e3508652a80c1e8b696a44ef7dc0c251, codeobj_e3508652a80c1e8b696a44ef7dc0c251, module_prompt_toolkit$filters$cli, 0 );
    frame_e3508652a80c1e8b696a44ef7dc0c251 = cache_frame_e3508652a80c1e8b696a44ef7dc0c251;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e3508652a80c1e8b696a44ef7dc0c251 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e3508652a80c1e8b696a44ef7dc0c251 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_vi_insert_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_insert_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_insert_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e3508652a80c1e8b696a44ef7dc0c251 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e3508652a80c1e8b696a44ef7dc0c251 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e3508652a80c1e8b696a44ef7dc0c251 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e3508652a80c1e8b696a44ef7dc0c251, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e3508652a80c1e8b696a44ef7dc0c251->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e3508652a80c1e8b696a44ef7dc0c251, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e3508652a80c1e8b696a44ef7dc0c251,
        type_description_1
    );


    // Release cached frame.
    if ( frame_e3508652a80c1e8b696a44ef7dc0c251 == cache_frame_e3508652a80c1e8b696a44ef7dc0c251 )
    {
        Py_DECREF( frame_e3508652a80c1e8b696a44ef7dc0c251 );
    }
    cache_frame_e3508652a80c1e8b696a44ef7dc0c251 = NULL;

    assertFrameObject( frame_e3508652a80c1e8b696a44ef7dc0c251 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_19_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_20_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_96129e0ffded0b2773aa202e4a2c00b3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_96129e0ffded0b2773aa202e4a2c00b3 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_96129e0ffded0b2773aa202e4a2c00b3, codeobj_96129e0ffded0b2773aa202e4a2c00b3, module_prompt_toolkit$filters$cli, 0 );
    frame_96129e0ffded0b2773aa202e4a2c00b3 = cache_frame_96129e0ffded0b2773aa202e4a2c00b3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_96129e0ffded0b2773aa202e4a2c00b3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_96129e0ffded0b2773aa202e4a2c00b3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 59;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96129e0ffded0b2773aa202e4a2c00b3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_96129e0ffded0b2773aa202e4a2c00b3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96129e0ffded0b2773aa202e4a2c00b3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_96129e0ffded0b2773aa202e4a2c00b3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_96129e0ffded0b2773aa202e4a2c00b3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_96129e0ffded0b2773aa202e4a2c00b3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_96129e0ffded0b2773aa202e4a2c00b3,
        type_description_1
    );


    // Release cached frame.
    if ( frame_96129e0ffded0b2773aa202e4a2c00b3 == cache_frame_96129e0ffded0b2773aa202e4a2c00b3 )
    {
        Py_DECREF( frame_96129e0ffded0b2773aa202e4a2c00b3 );
    }
    cache_frame_96129e0ffded0b2773aa202e4a2c00b3 = NULL;

    assertFrameObject( frame_96129e0ffded0b2773aa202e4a2c00b3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_20_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_21_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_1dabcf5c8c47879561da184f1a4e649a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1dabcf5c8c47879561da184f1a4e649a = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_1dabcf5c8c47879561da184f1a4e649a, codeobj_1dabcf5c8c47879561da184f1a4e649a, module_prompt_toolkit$filters$cli, 0 );
    frame_1dabcf5c8c47879561da184f1a4e649a = cache_frame_1dabcf5c8c47879561da184f1a4e649a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1dabcf5c8c47879561da184f1a4e649a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1dabcf5c8c47879561da184f1a4e649a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_has_completions );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_completions );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_completions" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1dabcf5c8c47879561da184f1a4e649a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1dabcf5c8c47879561da184f1a4e649a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1dabcf5c8c47879561da184f1a4e649a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1dabcf5c8c47879561da184f1a4e649a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1dabcf5c8c47879561da184f1a4e649a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1dabcf5c8c47879561da184f1a4e649a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1dabcf5c8c47879561da184f1a4e649a,
        type_description_1
    );


    // Release cached frame.
    if ( frame_1dabcf5c8c47879561da184f1a4e649a == cache_frame_1dabcf5c8c47879561da184f1a4e649a )
    {
        Py_DECREF( frame_1dabcf5c8c47879561da184f1a4e649a );
    }
    cache_frame_1dabcf5c8c47879561da184f1a4e649a = NULL;

    assertFrameObject( frame_1dabcf5c8c47879561da184f1a4e649a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_21_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_22_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_9e2db0ea764f50d59c67b0875ab78c45;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9e2db0ea764f50d59c67b0875ab78c45 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_9e2db0ea764f50d59c67b0875ab78c45, codeobj_9e2db0ea764f50d59c67b0875ab78c45, module_prompt_toolkit$filters$cli, 0 );
    frame_9e2db0ea764f50d59c67b0875ab78c45 = cache_frame_9e2db0ea764f50d59c67b0875ab78c45;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9e2db0ea764f50d59c67b0875ab78c45 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9e2db0ea764f50d59c67b0875ab78c45 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_is_read_only );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_read_only );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_read_only" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9e2db0ea764f50d59c67b0875ab78c45 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9e2db0ea764f50d59c67b0875ab78c45 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9e2db0ea764f50d59c67b0875ab78c45 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9e2db0ea764f50d59c67b0875ab78c45, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9e2db0ea764f50d59c67b0875ab78c45->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9e2db0ea764f50d59c67b0875ab78c45, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9e2db0ea764f50d59c67b0875ab78c45,
        type_description_1
    );


    // Release cached frame.
    if ( frame_9e2db0ea764f50d59c67b0875ab78c45 == cache_frame_9e2db0ea764f50d59c67b0875ab78c45 )
    {
        Py_DECREF( frame_9e2db0ea764f50d59c67b0875ab78c45 );
    }
    cache_frame_9e2db0ea764f50d59c67b0875ab78c45 = NULL;

    assertFrameObject( frame_9e2db0ea764f50d59c67b0875ab78c45 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_22_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$filters$cli$$$function_23_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_f11379480be3570771d48f03238d6d58;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f11379480be3570771d48f03238d6d58 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_f11379480be3570771d48f03238d6d58, codeobj_f11379480be3570771d48f03238d6d58, module_prompt_toolkit$filters$cli, 0 );
    frame_f11379480be3570771d48f03238d6d58 = cache_frame_f11379480be3570771d48f03238d6d58;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f11379480be3570771d48f03238d6d58 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f11379480be3570771d48f03238d6d58 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_is_multiline );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_multiline );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_multiline" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f11379480be3570771d48f03238d6d58 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f11379480be3570771d48f03238d6d58 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f11379480be3570771d48f03238d6d58 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f11379480be3570771d48f03238d6d58, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f11379480be3570771d48f03238d6d58->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f11379480be3570771d48f03238d6d58, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f11379480be3570771d48f03238d6d58,
        type_description_1
    );


    // Release cached frame.
    if ( frame_f11379480be3570771d48f03238d6d58 == cache_frame_f11379480be3570771d48f03238d6d58 )
    {
        Py_DECREF( frame_f11379480be3570771d48f03238d6d58 );
    }
    cache_frame_f11379480be3570771d48f03238d6d58 = NULL;

    assertFrameObject( frame_f11379480be3570771d48f03238d6d58 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$filters$cli$$$function_23_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_10_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_10_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b2ac080496a9e4ddc32785ee8f46dc3d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_11_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_11_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_dc54d457d0ebeb58df91267aa76ef63c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_12_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_12_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_072e4def420ed650df1b37036de59aa2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_13_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_13_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a211800129fc53aeb78cba6f82fb8ff0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_14_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_14_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6c010521069a9b2e64b677869a1c0eae,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_15_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_15_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a182aa200d38edea1443661d32edae87,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_16_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_16_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_fdbf988ea4be76652aa1d022c3835728,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_17_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_17_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5423d50312a3e130e5a8fb0ad7ca9486,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_18_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_18_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_faa99b3e22bbd023be141dd1f2e13058,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_19_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_19_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e3508652a80c1e8b696a44ef7dc0c251,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_290bc47af887053c204821758857f685,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_20_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_20_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_96129e0ffded0b2773aa202e4a2c00b3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_21_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_21_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1dabcf5c8c47879561da184f1a4e649a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_22_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_22_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9e2db0ea764f50d59c67b0875ab78c45,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_23_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_23_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f11379480be3570771d48f03238d6d58,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_2_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_2_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cbc88715f10d16dd9d969e8cd928b59b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_3_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_3_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_dab2f80ad874b81530872225ad446f98,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_4_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_4_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f7e08ba1fab71ffee1cc02a891e6fbd1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_5_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_5_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bb9b482e7ccf1a728adfa65dbdc1f21c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_6_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_6_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_02904fefb5c952061b1a53f8ed8a8600,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_7_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_7_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e31e13f7906f6854a46a1a8788117157,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_8_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_8_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c785c11a2831d8e6146d4ac7faaf6ee9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_9_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$filters$cli$$$function_9_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_51ec85bb7fa31f576b7249223f8aa90d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$filters$cli,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$filters$cli =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.filters.cli",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$filters$cli)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$filters$cli)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$filters$cli );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.filters.cli: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.filters.cli: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.filters.cli: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$filters$cli" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$filters$cli = Py_InitModule4(
        "prompt_toolkit.filters.cli",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$filters$cli = PyModule_Create( &mdef_prompt_toolkit$filters$cli );
#endif

    moduledict_prompt_toolkit$filters$cli = MODULE_DICT( module_prompt_toolkit$filters$cli );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$filters$cli,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$filters$cli,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$filters$cli,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$filters$cli,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$filters$cli );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_8a0caa3e7185baf8afb32f7c6f33e0fe, module_prompt_toolkit$filters$cli );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_70fb417f52187b6e019fd89a32ae3549;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_70fb417f52187b6e019fd89a32ae3549 = MAKE_MODULE_FRAME( codeobj_70fb417f52187b6e019fd89a32ae3549, module_prompt_toolkit$filters$cli );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_70fb417f52187b6e019fd89a32ae3549 );
    assert( Py_REFCNT( frame_70fb417f52187b6e019fd89a32ae3549 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_70fb417f52187b6e019fd89a32ae3549->m_frame.f_lineno = 1;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_star_imported_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_app;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$filters$cli;
        tmp_locals_name_1 = (PyObject *)moduledict_prompt_toolkit$filters$cli;
        tmp_fromlist_name_1 = const_tuple_str_chr_42_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_70fb417f52187b6e019fd89a32ae3549->m_frame.f_lineno = 5;
        tmp_star_imported_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_star_imported_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        tmp_result = IMPORT_MODULE_STAR( module_prompt_toolkit$filters$cli, true, tmp_star_imported_1 );
        Py_DECREF( tmp_star_imported_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = LIST_COPY( const_list_f75ba3bb8e804abbf278622059d4d8a5_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_1_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_HasValidationError, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_2_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_HasArg, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_3_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_IsDone, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_4_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_RendererHeightIsKnown, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_5_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_ViNavigationMode, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_6_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_InPasteMode, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_7_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_EmacsMode, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_8_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_EmacsInsertMode, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_9_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_ViMode, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_10_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_IsSearching, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_11_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_HasSearch, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_12_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_ControlIsSearchable, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_13_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_EmacsSelectionMode, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_14_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_ViDigraphMode, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_15_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_ViWaitingForTextObjectMode, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_16_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_ViSelectionMode, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_17_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_ViReplaceMode, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_18_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_ViInsertMultipleMode, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_19_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_ViInsertMode, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_20_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_HasSelection, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_21_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_HasCompletions, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_22_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_IsReadOnly, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = MAKE_FUNCTION_prompt_toolkit$filters$cli$$$function_23_lambda(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_IsMultiline, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_has_focus );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_focus );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_focus" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_29 = tmp_mvar_value_3;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_HasFocus, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_in_editing_mode );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_editing_mode );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_editing_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 65;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_30 = tmp_mvar_value_4;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$filters$cli, (Nuitka_StringObject *)const_str_plain_InEditingMode, tmp_assign_source_30 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_70fb417f52187b6e019fd89a32ae3549 );
#endif
    popFrameStack();

    assertFrameObject( frame_70fb417f52187b6e019fd89a32ae3549 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_70fb417f52187b6e019fd89a32ae3549 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_70fb417f52187b6e019fd89a32ae3549, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_70fb417f52187b6e019fd89a32ae3549->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_70fb417f52187b6e019fd89a32ae3549, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_prompt_toolkit$filters$cli );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
