/* Generated code for Python module 'matplotlib.backends.backend_webagg'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_matplotlib$backends$backend_webagg" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_matplotlib$backends$backend_webagg;
PyDictObject *moduledict_matplotlib$backends$backend_webagg;

/* The declarations of module constants used, if any. */
static PyObject *const_tuple_0b290ac3226aa5d811f7bfe0b354c55a_tuple;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain_emf;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_ps;
extern PyObject *const_str_plain_IOLoop;
extern PyObject *const_str_plain_sorted;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_encode;
extern PyObject *const_str_plain_Gcf;
static PyObject *const_str_digest_c8055de4fcc7313dbf78247962e0824e;
extern PyObject *const_str_plain_None;
static PyObject *const_str_plain_old_handler;
static PyObject *const_tuple_30953d211847cb7e4eb831df36500ce8_tuple;
extern PyObject *const_int_pos_5;
static PyObject *const_tuple_50b9d97eb7a12ae78ce70b8ce8f4f2ec_tuple;
extern PyObject *const_str_plain_stdout;
extern PyObject *const_str_digest_53cde7bdda744b7198c29b41fd8f35c8;
extern PyObject *const_str_plain_svg;
extern PyObject *const_str_plain_start;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_str_plain_eps;
extern PyObject *const_str_plain_contextlib;
extern PyObject *const_str_plain_savefig;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_digest_e5180111a112c9c8343168ac5095042b;
extern PyObject *const_str_plain_errno;
extern PyObject *const_str_plain_matplotlib;
static PyObject *const_str_plain_MplJs;
static PyObject *const_str_digest_d089ded72d92e062b8046828d1680e22;
static PyObject *const_tuple_str_plain_old_handler_tuple;
extern PyObject *const_str_digest_b06ed3dd4281a933420dcb4d45b5b3e6;
extern PyObject *const_str_plain_generate;
extern PyObject *const_int_neg_2;
extern PyObject *const_str_digest_a1105e36a1e6c2a432fdc8c72e22be55;
extern PyObject *const_str_plain_min;
static PyObject *const_str_digest_bc4bb1da5dbfb724e7f6bf7cec6bce9e;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_plain_BytesIO;
extern PyObject *const_str_plain_get_fig_manager;
extern PyObject *const_str_plain_export;
extern PyObject *const_tuple_str_plain_rcParams_tuple;
static PyObject *const_tuple_str_plain_ioloop_str_plain_cls_tuple;
extern PyObject *const_str_plain_base64;
static PyObject *const_str_digest_a8219884483fbf4a1c80a3e53d26236e;
extern PyObject *const_str_plain_FigureCanvas;
extern PyObject *const_str_plain_rcParams;
static PyObject *const_tuple_str_plain_self_str_plain_url_prefix_str_plain___class___tuple;
static PyObject *const_str_plain_image_path;
extern PyObject *const_str_plain_SIGINT;
extern PyObject *const_str_digest_6d6a615162e89eb148ba9bf8dbfc06d3;
extern PyObject *const_str_plain_contextmanager;
static PyObject *const_tuple_str_plain_self_str_plain_js_content_tuple;
extern PyObject *const_str_plain_on_close;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
extern PyObject *const_str_plain_tornado;
extern PyObject *const_str_plain_RequestHandler;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_getvalue;
extern PyObject *const_tuple_str_newline_str_empty_tuple;
extern PyObject *const_str_digest_ba38e0a1f7c48a254518d6d13e2cde5e;
static PyObject *const_tuple_799964753fb254af1caffea4583447e9_tuple;
static PyObject *const_str_digest_3f9c4d1d16726006443c19835dc0b4cf;
extern PyObject *const_tuple_str_plain_manager_tuple;
static PyObject *const_str_digest_70e95e273cd6102139446e892067474b;
extern PyObject *const_str_plain_buff;
extern PyObject *const_str_plain_get_javascript;
static PyObject *const_str_digest_656c72a5258e3f4a4478e916d5cfd0c1;
extern PyObject *const_str_plain_type;
static PyObject *const_str_plain_random_ports;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_2d86bc60c406ff06b8e8e7c50993855e;
static PyObject *const_tuple_442b8eef8c52cbabac4bc765f0a76ebc_tuple;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_replace;
extern PyObject *const_str_plain_port;
extern PyObject *const_str_digest_82da718018898901a2bd1c8219eb7ef2;
extern PyObject *const_str_plain_open;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_url;
extern PyObject *const_str_plain_pathlib;
extern PyObject *const_str_plain_write;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_get_static_file_path;
extern PyObject *const_str_plain_format;
static PyObject *const_str_digest_00d47290c7127c3984f25c5335fc3bc0;
static PyObject *const_str_digest_58b59e05f5b467ff947a344bcf92c599;
extern PyObject *const_str_plain_threading;
static PyObject *const_tuple_str_digest_82a10b5e093fc185ab39410caa4ed093_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_ioloop;
static PyObject *const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
extern PyObject *const_str_plain_fmt;
extern PyObject *const_str_plain_trigger_manager_draw;
static PyObject *const_str_digest_bbbe012e9b37e4f5ca2bd4138d7e5177;
extern PyObject *const_str_digest_61dec4a44b387cbf49fbbb6055ee75ea;
extern PyObject *const_str_plain_websocket;
extern PyObject *const_str_plain_send_binary;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_digest_b8901647f1104a11a5798377646ea8e6;
static PyObject *const_str_plain_WebAggApplication;
extern PyObject *const_str_plain_ipython_inline_display;
extern PyObject *const_str_plain_template;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_on_message;
static PyObject *const_str_digest_19787f06335e55a827636e7543295c3a;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_value;
static PyObject *const_str_plain_ServerThread;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_message;
static PyObject *const_str_digest_8268cbbdc5de4f0dbacb3d0583d75fff;
static PyObject *const_str_digest_82a10b5e093fc185ab39410caa4ed093;
extern PyObject *const_str_plain_error;
static PyObject *const_str_digest_701cb490bd26c3b3c6f69fb944609012;
static PyObject *const_str_digest_b0b81ebbdbf86428cb5f6c718abbd9fc;
extern PyObject *const_str_plain_decode;
extern PyObject *const_tuple_str_empty_tuple;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_digest_94ef863da98ac059e0cc890a2de10347;
extern PyObject *const_str_plain_figs;
extern PyObject *const_str_plain_webbrowser;
extern PyObject *const_tuple_str_plain_Gcf_tuple;
extern PyObject *const_str_plain_EADDRINUSE;
extern PyObject *const_str_plain_tpl;
extern PyObject *const_str_plain_TimerTornado;
extern PyObject *const_str_plain_dumps;
static PyObject *const_str_plain_figures;
static PyObject *const_str_digest_7c9ae6dc7c0dd7733d03c0e195021bf7;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_plain_js_content;
extern PyObject *const_str_plain_Thread;
extern PyObject *const_str_plain_NavigationToolbar2WebAgg;
extern PyObject *const_str_plain_req;
extern PyObject *const_str_plain_send_json;
extern PyObject *const_str_plain__Backend;
extern PyObject *const_str_chr_47;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_io;
extern PyObject *const_str_plain_fignum;
extern PyObject *const_str_plain_initialized;
extern PyObject *const_str_plain_socket;
static PyObject *const_str_plain_FavIcon;
extern PyObject *const_str_plain_stop;
extern PyObject *const_str_plain___init__;
static PyObject *const_str_digest_7cfaee3a0ec3cd71db582f039eb9e3ec;
extern PyObject *const_str_plain_loads;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_figure;
extern PyObject *const_str_plain_listen;
extern PyObject *const_str_empty;
extern PyObject *const_tuple_str_plain__Backend_tuple;
extern PyObject *const_str_digest_c10948e26b8029e04e0a0726e5ba7b39;
extern PyObject *const_str_plain_url_prefix;
static PyObject *const_str_digest_2e8d6fe611be80191eea7726c648e557;
extern PyObject *const_str_plain_jpeg;
extern PyObject *const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple;
static PyObject *const_str_digest_c7f7e94eaf1363c475e2a641103897dd;
extern PyObject *const_str_plain_request;
static PyObject *const_str_digest_f235eb3b97dc845b01d2e43ab3230a53;
extern PyObject *const_str_digest_94d6a065657110387ae0f04e74160a45;
extern PyObject *const_str_plain_mimetypes;
extern PyObject *const_str_plain_initialize;
extern PyObject *const_str_plain_Path;
static PyObject *const_str_digest_88e8b939d1eb863a6008c9132450a421;
static PyObject *const_tuple_str_plain_self_str_plain_message_str_plain_manager_tuple;
extern PyObject *const_str_plain_draw_idle;
extern PyObject *const_str_plain_prefix;
extern PyObject *const_str_plain_json;
static PyObject *const_dict_8643c5c1af414a0f8c299f5ceeb1d538;
extern PyObject *const_str_plain_template_path;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_digest_66ef772b0f62991fd20497da33b24b9b;
static PyObject *const_str_digest_056fedea4fee9cdcd1748a56dc3715ea;
static PyObject *const_tuple_str_plain_self_str_plain_blob_str_plain_data_uri_tuple;
extern PyObject *const_str_plain_classmethod;
extern PyObject *const_str_plain_add_web_socket;
extern PyObject *const_str_plain_png;
extern PyObject *const_str_plain_flush;
extern PyObject *const_str_plain_canvas;
extern PyObject *const_str_plain_datapath;
static PyObject *const_str_plain_catch_sigint;
extern PyObject *const_str_plain_write_message;
extern PyObject *const_tuple_true_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_digest_35fa36169a0e18ddad7803910d4c3238;
static PyObject *const_tuple_str_plain_self_str_plain_fignum_tuple;
extern PyObject *const_str_digest_6d0365632ca69247476514d3663a86aa;
static PyObject *const_str_digest_38188f2430656c7112bd3e342db7fed3;
static PyObject *const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple;
static PyObject *const_tuple_str_digest_e41aedc5c700d9694e57af141610fd0e_tuple;
static PyObject *const_str_digest_1f46cbc1f0f599cad3baec9d5fe3b6f9;
static PyObject *const_dict_59adcb2296c213fb0eb9f1796274d900;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_tuple_str_plain_backend_webagg_core_tuple;
extern PyObject *const_str_plain_Template;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_SingleFigurePage;
extern PyObject *const_str_plain_toolitems;
static PyObject *const_str_digest_0afdc4d6eef3f00b0658c948a3cbf4ef;
static PyObject *const_str_digest_a7d70f15c8248fb5c4d5e0f25faea644;
extern PyObject *const_tuple_str_plain_contextmanager_tuple;
static PyObject *const_str_plain_AllFiguresPage;
extern PyObject *const_tuple_str_plain_BytesIO_tuple;
static PyObject *const_str_digest_394f75c46e7f761b798516dac533aae6;
extern PyObject *const_str_plain_read_text;
extern PyObject *const_tuple_str_empty_none_none_tuple;
extern PyObject *const_tuple_str_plain_self_str_plain_content_tuple;
extern PyObject *const_str_plain_tif;
static PyObject *const_str_digest_e83c35b37b34ee65973562144b1428a0;
extern PyObject *const_str_plain_set_nodelay;
extern PyObject *const_str_plain_content;
static PyObject *const_str_plain_webagg_server_thread;
extern PyObject *const_str_plain_set_header;
extern PyObject *const_str_plain_sig;
static PyObject *const_str_plain_ws_uri;
static PyObject *const_str_digest_7dd06b8ccaa246845f93fc1486ea3de5;
static PyObject *const_tuple_str_plain_self_str_plain_ws_uri_tuple;
static PyObject *const_str_digest_22d6d4c089761fdd74b115af7e210732;
extern PyObject *const_str_plain_is_alive;
extern PyObject *const_str_plain_app;
extern PyObject *const_str_plain_randint;
static PyObject *const_str_plain_WebSocket;
extern PyObject *const_str_plain___class__;
static PyObject *const_tuple_05dadc01141b31df536583e6e2798580_tuple;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_digest_2dfccf0bcd7bc134e2e81f7b5144e3e0;
static PyObject *const_str_digest_1775d0c14bb215a45c234d4f99ab9c5b;
extern PyObject *const_str_plain_manager;
extern PyObject *const_str_plain_web;
extern PyObject *const_str_plain_print;
extern PyObject *const_str_plain_address;
static PyObject *const_str_digest_dcebcd7cd27a3f2bf5f57a79b3cb4a41;
extern PyObject *const_str_plain_binary;
extern PyObject *const_tuple_str_plain_Path_tuple;
extern PyObject *const_str_plain_FigureManagerWebAgg;
extern PyObject *const_str_plain_get;
static PyObject *const_str_digest_b3afeac9755413daa29c5ba369677d8a;
extern PyObject *const_str_digest_d5c187b86dc6b4ec38f0ddb9c3eb9233;
extern PyObject *const_str_plain_started;
extern PyObject *const_str_plain_new_timer;
extern PyObject *const_str_plain_shutdown;
extern PyObject *const_str_plain_read_bytes;
extern PyObject *const_str_plain_number;
extern PyObject *const_str_plain_run;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_digest_99105b9e29e2649d8dbc9a686f274db6;
static PyObject *const_str_digest_84d8f162a9332acb860ab19d798a389d;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain___exit__;
extern PyObject *const_tuple_str_plain_self_str_plain_args_str_plain_kwargs_tuple;
static PyObject *const_tuple_str_plain_port_str_plain_n_str_plain_i_tuple;
extern PyObject *const_str_plain_items;
static PyObject *const_str_digest_1977aefd41afa78359014f2ead2320b0;
extern PyObject *const_str_digest_06bf4bb6663dc5cf008410bbd11c4301;
extern PyObject *const_str_plain_instance;
extern PyObject *const_str_plain_signal;
static PyObject *const_tuple_str_plain_base64_tuple;
static PyObject *const_str_plain_backend_webagg_core;
extern PyObject *const_str_plain_staticmethod;
static PyObject *const_str_plain_data_uri;
extern PyObject *const_str_plain_remove_web_socket;
extern PyObject *const_str_plain___enter__;
extern PyObject *const_str_plain_Application;
extern PyObject *const_str_plain_cls;
extern PyObject *const_str_plain_images;
extern PyObject *const_str_plain_FigureManager;
extern PyObject *const_str_digest_62fc664646c5b4f462aaa7001dcc03a6;
static PyObject *const_str_plain_blob;
static PyObject *const_str_digest_2ea824438b778763e56ab3fed3870421;
extern PyObject *const_str_plain_WebSocketHandler;
extern PyObject *const_str_plain_frame;
extern PyObject *const_str_plain_render;
extern PyObject *const_str_plain_t;
static PyObject *const_str_digest_99c8daaa545708fe4682d180a23b59e4;
static PyObject *const_str_plain_fig_id;
extern PyObject *const_str_digest_e58eec2a5ea585c2c4db40f39721d451;
extern PyObject *const_str_digest_79d9bffedced0e4cee681f99603a2fbc;
extern PyObject *const_str_plain_pdf;
extern PyObject *const_str_plain_random;
static PyObject *const_tuple_str_digest_056fedea4fee9cdcd1748a56dc3715ea_tuple;
static PyObject *const_str_digest_3d541f49a8fda2dfe9a9a0f962378f08;
extern PyObject *const_str_plain_add_callback_from_signal;
extern PyObject *const_str_plain_application;
static PyObject *const_tuple_str_digest_c8055de4fcc7313dbf78247962e0824e_tuple;
static PyObject *const_str_digest_97cf460c4b1e10b498964a539e0a4591;
static PyObject *const_str_digest_671311a951c594c3cb20f9707bda3cd3;
static PyObject *const_str_plain__BackendWebAgg;
extern PyObject *const_str_plain_show;
static PyObject *const_tuple_str_plain_self_str_plain_image_path_tuple;
static PyObject *const_dict_623a57f4e4ea26ffffdc7682acb5f6ab;
extern PyObject *const_str_plain_FigureCanvasWebAggCore;
static PyObject *const_tuple_str_plain_url_str_plain_webbrowser_tuple;
extern PyObject *const_str_plain_core;
extern PyObject *const_str_plain_StaticFileHandler;
static PyObject *const_str_digest_27f3593b1aff11723abf4de571d5884c;
extern PyObject *const_str_angle_lambda;
static PyObject *const_str_digest_05b9dc565b469bc9595924b7779b08e5;
static PyObject *const_tuple_40307db08d5aebdc7fcfdcb3df2165ca_tuple;
static PyObject *const_str_plain_FigureCanvasWebAgg;
static PyObject *const_str_digest_e41aedc5c700d9694e57af141610fd0e;
static PyObject *const_str_digest_7409f88d8b0b7b58054ceec78ccf1548;
static PyObject *const_str_digest_76dc643249992a4e2a9e86cad0e27075;
extern PyObject *const_str_newline;
extern PyObject *const_str_plain_handle_json;
static PyObject *const_str_digest_83d81d1e8b1b0b95b8b9444315d7e384;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_supports_binary;
static PyObject *const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple;
static PyObject *const_str_digest_9510e51027f71cceb45091b2fa751e76;
static PyObject *const_tuple_str_digest_2ea824438b778763e56ab3fed3870421_tuple;
extern PyObject *const_str_plain_Download;
extern PyObject *const_str_digest_961ec7b6f052f313b621a72bbdfaf6df;
extern PyObject *const_int_pos_2;
static PyObject *const_str_digest_88c7db975dc5d7198afe33fc086f0ebc;
static PyObject *const_str_digest_8474c3bcc1faa7791e88cb2c17dd8f40;
static PyObject *const_tuple_str_plain_TimerTornado_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_0b290ac3226aa5d811f7bfe0b354c55a_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_0b290ac3226aa5d811f7bfe0b354c55a_tuple, 0, const_str_plain_figure ); Py_INCREF( const_str_plain_figure );
    PyTuple_SET_ITEM( const_tuple_0b290ac3226aa5d811f7bfe0b354c55a_tuple, 1, const_str_plain_tornado ); Py_INCREF( const_str_plain_tornado );
    PyTuple_SET_ITEM( const_tuple_0b290ac3226aa5d811f7bfe0b354c55a_tuple, 2, const_str_plain_fignum ); Py_INCREF( const_str_plain_fignum );
    PyTuple_SET_ITEM( const_tuple_0b290ac3226aa5d811f7bfe0b354c55a_tuple, 3, const_str_plain_tpl ); Py_INCREF( const_str_plain_tpl );
    PyTuple_SET_ITEM( const_tuple_0b290ac3226aa5d811f7bfe0b354c55a_tuple, 4, const_str_plain_t ); Py_INCREF( const_str_plain_t );
    const_str_plain_emf = UNSTREAM_STRING_ASCII( &constant_bin[ 204673 ], 3, 1 );
    const_str_digest_c8055de4fcc7313dbf78247962e0824e = UNSTREAM_STRING_ASCII( &constant_bin[ 1826056 ], 16, 0 );
    const_str_plain_old_handler = UNSTREAM_STRING_ASCII( &constant_bin[ 1826072 ], 11, 1 );
    const_tuple_30953d211847cb7e4eb831df36500ce8_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_30953d211847cb7e4eb831df36500ce8_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_30953d211847cb7e4eb831df36500ce8_tuple, 1, const_str_plain_ioloop ); Py_INCREF( const_str_plain_ioloop );
    PyTuple_SET_ITEM( const_tuple_30953d211847cb7e4eb831df36500ce8_tuple, 2, const_str_plain_shutdown ); Py_INCREF( const_str_plain_shutdown );
    const_str_plain_catch_sigint = UNSTREAM_STRING_ASCII( &constant_bin[ 1826083 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_30953d211847cb7e4eb831df36500ce8_tuple, 3, const_str_plain_catch_sigint ); Py_INCREF( const_str_plain_catch_sigint );
    const_tuple_50b9d97eb7a12ae78ce70b8ce8f4f2ec_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_50b9d97eb7a12ae78ce70b8ce8f4f2ec_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_50b9d97eb7a12ae78ce70b8ce8f4f2ec_tuple, 1, const_str_plain_fignum ); Py_INCREF( const_str_plain_fignum );
    PyTuple_SET_ITEM( const_tuple_50b9d97eb7a12ae78ce70b8ce8f4f2ec_tuple, 2, const_str_plain_fmt ); Py_INCREF( const_str_plain_fmt );
    PyTuple_SET_ITEM( const_tuple_50b9d97eb7a12ae78ce70b8ce8f4f2ec_tuple, 3, const_str_plain_manager ); Py_INCREF( const_str_plain_manager );
    PyTuple_SET_ITEM( const_tuple_50b9d97eb7a12ae78ce70b8ce8f4f2ec_tuple, 4, const_str_plain_mimetypes ); Py_INCREF( const_str_plain_mimetypes );
    PyTuple_SET_ITEM( const_tuple_50b9d97eb7a12ae78ce70b8ce8f4f2ec_tuple, 5, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    const_str_digest_e5180111a112c9c8343168ac5095042b = UNSTREAM_STRING_ASCII( &constant_bin[ 1826095 ], 10, 0 );
    const_str_plain_MplJs = UNSTREAM_STRING_ASCII( &constant_bin[ 1826105 ], 5, 1 );
    const_str_digest_d089ded72d92e062b8046828d1680e22 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826110 ], 24, 0 );
    const_tuple_str_plain_old_handler_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_old_handler_tuple, 0, const_str_plain_old_handler ); Py_INCREF( const_str_plain_old_handler );
    const_str_digest_bc4bb1da5dbfb724e7f6bf7cec6bce9e = UNSTREAM_STRING_ASCII( &constant_bin[ 1826134 ], 23, 0 );
    const_tuple_str_plain_ioloop_str_plain_cls_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ioloop_str_plain_cls_tuple, 0, const_str_plain_ioloop ); Py_INCREF( const_str_plain_ioloop );
    PyTuple_SET_ITEM( const_tuple_str_plain_ioloop_str_plain_cls_tuple, 1, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    const_str_digest_a8219884483fbf4a1c80a3e53d26236e = UNSTREAM_STRING_ASCII( &constant_bin[ 1826157 ], 16, 0 );
    const_tuple_str_plain_self_str_plain_url_prefix_str_plain___class___tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_url_prefix_str_plain___class___tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_url_prefix_str_plain___class___tuple, 1, const_str_plain_url_prefix ); Py_INCREF( const_str_plain_url_prefix );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_url_prefix_str_plain___class___tuple, 2, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_plain_image_path = UNSTREAM_STRING_ASCII( &constant_bin[ 1826173 ], 10, 1 );
    const_tuple_str_plain_self_str_plain_js_content_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_js_content_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_js_content = UNSTREAM_STRING_ASCII( &constant_bin[ 1826183 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_js_content_tuple, 1, const_str_plain_js_content ); Py_INCREF( const_str_plain_js_content );
    const_tuple_799964753fb254af1caffea4583447e9_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_799964753fb254af1caffea4583447e9_tuple, 0, const_str_digest_6d6a615162e89eb148ba9bf8dbfc06d3 ); Py_INCREF( const_str_digest_6d6a615162e89eb148ba9bf8dbfc06d3 );
    PyTuple_SET_ITEM( const_tuple_799964753fb254af1caffea4583447e9_tuple, 1, const_str_digest_b06ed3dd4281a933420dcb4d45b5b3e6 ); Py_INCREF( const_str_digest_b06ed3dd4281a933420dcb4d45b5b3e6 );
    const_str_digest_3f9c4d1d16726006443c19835dc0b4cf = UNSTREAM_STRING_ASCII( &constant_bin[ 1826193 ], 26, 0 );
    const_str_digest_70e95e273cd6102139446e892067474b = UNSTREAM_STRING_ASCII( &constant_bin[ 1826219 ], 22, 0 );
    const_str_digest_656c72a5258e3f4a4478e916d5cfd0c1 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826241 ], 38, 0 );
    const_str_plain_random_ports = UNSTREAM_STRING_ASCII( &constant_bin[ 1195612 ], 12, 1 );
    const_str_digest_2d86bc60c406ff06b8e8e7c50993855e = UNSTREAM_STRING_ASCII( &constant_bin[ 1826279 ], 32, 0 );
    const_tuple_442b8eef8c52cbabac4bc765f0a76ebc_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_442b8eef8c52cbabac4bc765f0a76ebc_tuple, 0, const_str_digest_6d6a615162e89eb148ba9bf8dbfc06d3 ); Py_INCREF( const_str_digest_6d6a615162e89eb148ba9bf8dbfc06d3 );
    PyTuple_SET_ITEM( const_tuple_442b8eef8c52cbabac4bc765f0a76ebc_tuple, 1, const_str_digest_62fc664646c5b4f462aaa7001dcc03a6 ); Py_INCREF( const_str_digest_62fc664646c5b4f462aaa7001dcc03a6 );
    const_str_digest_00d47290c7127c3984f25c5335fc3bc0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826311 ], 39, 0 );
    const_str_digest_58b59e05f5b467ff947a344bcf92c599 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826350 ], 14, 0 );
    const_tuple_str_digest_82a10b5e093fc185ab39410caa4ed093_tuple = PyTuple_New( 1 );
    const_str_digest_82a10b5e093fc185ab39410caa4ed093 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826364 ], 18, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_82a10b5e093fc185ab39410caa4ed093_tuple, 0, const_str_digest_82a10b5e093fc185ab39410caa4ed093 ); Py_INCREF( const_str_digest_82a10b5e093fc185ab39410caa4ed093 );
    const_str_digest_526e1fe88ddc5c56efb2459c1633b489 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826382 ], 34, 0 );
    const_str_digest_bbbe012e9b37e4f5ca2bd4138d7e5177 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826416 ], 28, 0 );
    const_str_digest_b8901647f1104a11a5798377646ea8e6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826444 ], 31, 0 );
    const_str_plain_WebAggApplication = UNSTREAM_STRING_ASCII( &constant_bin[ 1826134 ], 17, 1 );
    const_str_digest_19787f06335e55a827636e7543295c3a = UNSTREAM_STRING_ASCII( &constant_bin[ 1826475 ], 36, 0 );
    const_str_plain_ServerThread = UNSTREAM_STRING_ASCII( &constant_bin[ 1826157 ], 12, 1 );
    const_str_digest_8268cbbdc5de4f0dbacb3d0583d75fff = UNSTREAM_STRING_ASCII( &constant_bin[ 1826511 ], 32, 0 );
    const_str_digest_701cb490bd26c3b3c6f69fb944609012 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826543 ], 36, 0 );
    const_str_digest_b0b81ebbdbf86428cb5f6c718abbd9fc = UNSTREAM_STRING_ASCII( &constant_bin[ 1826579 ], 35, 0 );
    const_str_digest_94ef863da98ac059e0cc890a2de10347 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826614 ], 30, 0 );
    const_str_plain_figures = UNSTREAM_STRING_ASCII( &constant_bin[ 12829 ], 7, 1 );
    const_str_digest_7c9ae6dc7c0dd7733d03c0e195021bf7 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826644 ], 19, 0 );
    const_str_plain_FavIcon = UNSTREAM_STRING_ASCII( &constant_bin[ 1826663 ], 7, 1 );
    const_str_digest_7cfaee3a0ec3cd71db582f039eb9e3ec = UNSTREAM_STRING_ASCII( &constant_bin[ 1826670 ], 27, 0 );
    const_str_digest_2e8d6fe611be80191eea7726c648e557 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826697 ], 37, 0 );
    const_str_digest_c7f7e94eaf1363c475e2a641103897dd = UNSTREAM_STRING_ASCII( &constant_bin[ 1826734 ], 29, 0 );
    const_str_digest_f235eb3b97dc845b01d2e43ab3230a53 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826444 ], 9, 0 );
    const_str_digest_88e8b939d1eb863a6008c9132450a421 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826763 ], 226, 0 );
    const_tuple_str_plain_self_str_plain_message_str_plain_manager_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_message_str_plain_manager_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_message_str_plain_manager_tuple, 1, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_message_str_plain_manager_tuple, 2, const_str_plain_manager ); Py_INCREF( const_str_plain_manager );
    const_dict_8643c5c1af414a0f8c299f5ceeb1d538 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_8643c5c1af414a0f8c299f5ceeb1d538, const_str_plain_binary, Py_True );
    assert( PyDict_Size( const_dict_8643c5c1af414a0f8c299f5ceeb1d538 ) == 1 );
    const_str_digest_056fedea4fee9cdcd1748a56dc3715ea = UNSTREAM_STRING_ASCII( &constant_bin[ 1826989 ], 17, 0 );
    const_tuple_str_plain_self_str_plain_blob_str_plain_data_uri_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_blob_str_plain_data_uri_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_blob = UNSTREAM_STRING_ASCII( &constant_bin[ 998603 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_blob_str_plain_data_uri_tuple, 1, const_str_plain_blob ); Py_INCREF( const_str_plain_blob );
    const_str_plain_data_uri = UNSTREAM_STRING_ASCII( &constant_bin[ 1827006 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_blob_str_plain_data_uri_tuple, 2, const_str_plain_data_uri ); Py_INCREF( const_str_plain_data_uri );
    const_tuple_str_plain_self_str_plain_fignum_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_fignum_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_fignum_tuple, 1, const_str_plain_fignum ); Py_INCREF( const_str_plain_fignum );
    const_str_digest_38188f2430656c7112bd3e342db7fed3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827014 ], 41, 0 );
    const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple, 1, const_str_plain_application ); Py_INCREF( const_str_plain_application );
    PyTuple_SET_ITEM( const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple, 2, const_str_plain_request ); Py_INCREF( const_str_plain_request );
    PyTuple_SET_ITEM( const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple, 3, const_str_plain_url_prefix ); Py_INCREF( const_str_plain_url_prefix );
    PyTuple_SET_ITEM( const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple, 4, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple, 5, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_tuple_str_digest_e41aedc5c700d9694e57af141610fd0e_tuple = PyTuple_New( 1 );
    const_str_digest_e41aedc5c700d9694e57af141610fd0e = UNSTREAM_STRING_ASCII( &constant_bin[ 1827055 ], 34, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_e41aedc5c700d9694e57af141610fd0e_tuple, 0, const_str_digest_e41aedc5c700d9694e57af141610fd0e ); Py_INCREF( const_str_digest_e41aedc5c700d9694e57af141610fd0e );
    const_str_digest_1f46cbc1f0f599cad3baec9d5fe3b6f9 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827089 ], 50, 0 );
    const_dict_59adcb2296c213fb0eb9f1796274d900 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_59adcb2296c213fb0eb9f1796274d900, const_str_plain_url_prefix, const_str_empty );
    assert( PyDict_Size( const_dict_59adcb2296c213fb0eb9f1796274d900 ) == 1 );
    const_tuple_str_plain_backend_webagg_core_tuple = PyTuple_New( 1 );
    const_str_plain_backend_webagg_core = UNSTREAM_STRING_ASCII( &constant_bin[ 1827139 ], 19, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_backend_webagg_core_tuple, 0, const_str_plain_backend_webagg_core ); Py_INCREF( const_str_plain_backend_webagg_core );
    const_str_plain_SingleFigurePage = UNSTREAM_STRING_ASCII( &constant_bin[ 1826259 ], 16, 1 );
    const_str_digest_0afdc4d6eef3f00b0658c948a3cbf4ef = UNSTREAM_STRING_ASCII( &constant_bin[ 1827158 ], 43, 0 );
    const_str_digest_a7d70f15c8248fb5c4d5e0f25faea644 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827201 ], 12, 0 );
    const_str_plain_AllFiguresPage = UNSTREAM_STRING_ASCII( &constant_bin[ 1826297 ], 14, 1 );
    const_str_digest_394f75c46e7f761b798516dac533aae6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827213 ], 41, 0 );
    const_str_digest_e83c35b37b34ee65973562144b1428a0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827254 ], 31, 0 );
    const_str_plain_webagg_server_thread = UNSTREAM_STRING_ASCII( &constant_bin[ 1827285 ], 20, 1 );
    const_str_plain_ws_uri = UNSTREAM_STRING_ASCII( &constant_bin[ 1827305 ], 6, 1 );
    const_str_digest_7dd06b8ccaa246845f93fc1486ea3de5 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827311 ], 12, 0 );
    const_tuple_str_plain_self_str_plain_ws_uri_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_ws_uri_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_ws_uri_tuple, 1, const_str_plain_ws_uri ); Py_INCREF( const_str_plain_ws_uri );
    const_str_digest_22d6d4c089761fdd74b115af7e210732 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827323 ], 83, 0 );
    const_str_plain_WebSocket = UNSTREAM_STRING_ASCII( &constant_bin[ 1826329 ], 9, 1 );
    const_tuple_05dadc01141b31df536583e6e2798580_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_05dadc01141b31df536583e6e2798580_tuple, 0, const_str_plain_sig ); Py_INCREF( const_str_plain_sig );
    PyTuple_SET_ITEM( const_tuple_05dadc01141b31df536583e6e2798580_tuple, 1, const_str_plain_frame ); Py_INCREF( const_str_plain_frame );
    PyTuple_SET_ITEM( const_tuple_05dadc01141b31df536583e6e2798580_tuple, 2, const_str_plain_ioloop ); Py_INCREF( const_str_plain_ioloop );
    PyTuple_SET_ITEM( const_tuple_05dadc01141b31df536583e6e2798580_tuple, 3, const_str_plain_shutdown ); Py_INCREF( const_str_plain_shutdown );
    const_str_digest_2dfccf0bcd7bc134e2e81f7b5144e3e0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827089 ], 28, 0 );
    const_str_digest_1775d0c14bb215a45c234d4f99ab9c5b = UNSTREAM_STRING_ASCII( &constant_bin[ 1827406 ], 45, 0 );
    const_str_digest_dcebcd7cd27a3f2bf5f57a79b3cb4a41 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827451 ], 38, 0 );
    const_str_digest_b3afeac9755413daa29c5ba369677d8a = UNSTREAM_STRING_ASCII( &constant_bin[ 1827489 ], 25, 0 );
    const_str_digest_99105b9e29e2649d8dbc9a686f274db6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826311 ], 27, 0 );
    const_str_digest_84d8f162a9332acb860ab19d798a389d = UNSTREAM_STRING_ASCII( &constant_bin[ 1826241 ], 34, 0 );
    const_tuple_str_plain_port_str_plain_n_str_plain_i_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_port_str_plain_n_str_plain_i_tuple, 0, const_str_plain_port ); Py_INCREF( const_str_plain_port );
    PyTuple_SET_ITEM( const_tuple_str_plain_port_str_plain_n_str_plain_i_tuple, 1, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_str_plain_port_str_plain_n_str_plain_i_tuple, 2, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    const_str_digest_1977aefd41afa78359014f2ead2320b0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827514 ], 25, 0 );
    const_tuple_str_plain_base64_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_base64_tuple, 0, const_str_plain_base64 ); Py_INCREF( const_str_plain_base64 );
    const_str_digest_2ea824438b778763e56ab3fed3870421 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827539 ], 54, 0 );
    const_str_digest_99c8daaa545708fe4682d180a23b59e4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1826734 ], 25, 0 );
    const_str_plain_fig_id = UNSTREAM_STRING_ASCII( &constant_bin[ 1827593 ], 6, 1 );
    const_tuple_str_digest_056fedea4fee9cdcd1748a56dc3715ea_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_056fedea4fee9cdcd1748a56dc3715ea_tuple, 0, const_str_digest_056fedea4fee9cdcd1748a56dc3715ea ); Py_INCREF( const_str_digest_056fedea4fee9cdcd1748a56dc3715ea );
    const_str_digest_3d541f49a8fda2dfe9a9a0f962378f08 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827599 ], 37, 0 );
    const_tuple_str_digest_c8055de4fcc7313dbf78247962e0824e_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_c8055de4fcc7313dbf78247962e0824e_tuple, 0, const_str_digest_c8055de4fcc7313dbf78247962e0824e ); Py_INCREF( const_str_digest_c8055de4fcc7313dbf78247962e0824e );
    const_str_digest_97cf460c4b1e10b498964a539e0a4591 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827636 ], 2, 0 );
    const_str_digest_671311a951c594c3cb20f9707bda3cd3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827638 ], 23, 0 );
    const_str_plain__BackendWebAgg = UNSTREAM_STRING_ASCII( &constant_bin[ 1826579 ], 14, 1 );
    const_tuple_str_plain_self_str_plain_image_path_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_image_path_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_image_path_tuple, 1, const_str_plain_image_path ); Py_INCREF( const_str_plain_image_path );
    const_dict_623a57f4e4ea26ffffdc7682acb5f6ab = _PyDict_NewPresized( 8 );
    PyDict_SetItem( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab, const_str_plain_ps, const_str_digest_70e95e273cd6102139446e892067474b );
    PyDict_SetItem( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab, const_str_plain_eps, const_str_digest_70e95e273cd6102139446e892067474b );
    PyDict_SetItem( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab, const_str_plain_pdf, const_str_digest_d5c187b86dc6b4ec38f0ddb9c3eb9233 );
    PyDict_SetItem( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab, const_str_plain_svg, const_str_digest_82da718018898901a2bd1c8219eb7ef2 );
    PyDict_SetItem( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab, const_str_plain_png, const_str_digest_62fc664646c5b4f462aaa7001dcc03a6 );
    PyDict_SetItem( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab, const_str_plain_jpeg, const_str_digest_e58eec2a5ea585c2c4db40f39721d451 );
    PyDict_SetItem( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab, const_str_plain_tif, const_str_digest_a1105e36a1e6c2a432fdc8c72e22be55 );
    const_str_digest_76dc643249992a4e2a9e86cad0e27075 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827661 ], 15, 0 );
    PyDict_SetItem( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab, const_str_plain_emf, const_str_digest_76dc643249992a4e2a9e86cad0e27075 );
    assert( PyDict_Size( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab ) == 8 );
    const_tuple_str_plain_url_str_plain_webbrowser_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_url_str_plain_webbrowser_tuple, 0, const_str_plain_url ); Py_INCREF( const_str_plain_url );
    PyTuple_SET_ITEM( const_tuple_str_plain_url_str_plain_webbrowser_tuple, 1, const_str_plain_webbrowser ); Py_INCREF( const_str_plain_webbrowser );
    const_str_digest_27f3593b1aff11723abf4de571d5884c = UNSTREAM_STRING_ASCII( &constant_bin[ 1827676 ], 36, 0 );
    const_str_digest_05b9dc565b469bc9595924b7779b08e5 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827712 ], 26, 0 );
    const_tuple_40307db08d5aebdc7fcfdcb3df2165ca_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_40307db08d5aebdc7fcfdcb3df2165ca_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_40307db08d5aebdc7fcfdcb3df2165ca_tuple, 1, const_str_plain_fignum ); Py_INCREF( const_str_plain_fignum );
    PyTuple_SET_ITEM( const_tuple_40307db08d5aebdc7fcfdcb3df2165ca_tuple, 2, const_str_plain_manager ); Py_INCREF( const_str_plain_manager );
    PyTuple_SET_ITEM( const_tuple_40307db08d5aebdc7fcfdcb3df2165ca_tuple, 3, const_str_plain_ws_uri ); Py_INCREF( const_str_plain_ws_uri );
    const_str_plain_FigureCanvasWebAgg = UNSTREAM_STRING_ASCII( &constant_bin[ 1826416 ], 18, 1 );
    const_str_digest_7409f88d8b0b7b58054ceec78ccf1548 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827014 ], 23, 0 );
    const_str_digest_83d81d1e8b1b0b95b8b9444315d7e384 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827738 ], 63, 0 );
    const_str_plain_supports_binary = UNSTREAM_STRING_ASCII( &constant_bin[ 1827801 ], 15, 1 );
    const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple, 1, const_str_plain_url_prefix ); Py_INCREF( const_str_plain_url_prefix );
    PyTuple_SET_ITEM( const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple, 2, const_str_plain_port ); Py_INCREF( const_str_plain_port );
    PyTuple_SET_ITEM( const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple, 3, const_str_plain_address ); Py_INCREF( const_str_plain_address );
    PyTuple_SET_ITEM( const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple, 4, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    PyTuple_SET_ITEM( const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple, 5, const_str_plain_random_ports ); Py_INCREF( const_str_plain_random_ports );
    PyTuple_SET_ITEM( const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple, 6, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_str_digest_9510e51027f71cceb45091b2fa751e76 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827816 ], 43, 0 );
    const_tuple_str_digest_2ea824438b778763e56ab3fed3870421_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_2ea824438b778763e56ab3fed3870421_tuple, 0, const_str_digest_2ea824438b778763e56ab3fed3870421 ); Py_INCREF( const_str_digest_2ea824438b778763e56ab3fed3870421 );
    const_str_digest_88c7db975dc5d7198afe33fc086f0ebc = UNSTREAM_STRING_ASCII( &constant_bin[ 1827859 ], 26, 0 );
    const_str_digest_8474c3bcc1faa7791e88cb2c17dd8f40 = UNSTREAM_STRING_ASCII( &constant_bin[ 1827885 ], 13, 0 );
    const_tuple_str_plain_TimerTornado_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_TimerTornado_tuple, 0, const_str_plain_TimerTornado ); Py_INCREF( const_str_plain_TimerTornado );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_matplotlib$backends$backend_webagg( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_91c5fd307105d8c5eb47c9f6ab80c7a2;
static PyCodeObject *codeobj_a6bc9fc9cbcc7aad54ab5534e23a973a;
static PyCodeObject *codeobj_2ff8ab1c8bc2b4722af7cb6304c8f9cb;
static PyCodeObject *codeobj_56eaf9c20edf0f83e8ca1007194d7244;
static PyCodeObject *codeobj_d5530390e95e557195a38c84aae20396;
static PyCodeObject *codeobj_255a10bf58c0f52531761a99c6df03f3;
static PyCodeObject *codeobj_8c2d2a58a411a4c99aa649f8241de86d;
static PyCodeObject *codeobj_86295468592d459d67ea0acb84bbdbc3;
static PyCodeObject *codeobj_ac4a2b74daab0cd32f52a8f231717a01;
static PyCodeObject *codeobj_94d52aad8429f3f66731c14b3a262317;
static PyCodeObject *codeobj_e4252a982fc0f588ca0f884fdde91a3c;
static PyCodeObject *codeobj_7511e4691622db519fa9a0190d02ddea;
static PyCodeObject *codeobj_d2ba4fddab043b1038a29d853da561cb;
static PyCodeObject *codeobj_10590e77b6e6b31bea078cf8e3648e6c;
static PyCodeObject *codeobj_4056424134cd88e89ad9c087662b9c69;
static PyCodeObject *codeobj_05856c71000b555c771d767b07290f08;
static PyCodeObject *codeobj_28daa94b5b13f15a8e34ee41354f6c3c;
static PyCodeObject *codeobj_579d25a8138587c2d8a273b6b61298cc;
static PyCodeObject *codeobj_c309583248caa2d6e005fb1b6559bdd5;
static PyCodeObject *codeobj_a05d20662ec2597c5918a7f05956ca17;
static PyCodeObject *codeobj_bcd86a8402f4325e8d71659cde92e3cb;
static PyCodeObject *codeobj_d24eb11dbf63ec52ed1531708d5e939b;
static PyCodeObject *codeobj_59a631561ae8e27b85ee9e9c4e28e2c0;
static PyCodeObject *codeobj_e871247c6ee89deb80b38ca037bb1b5b;
static PyCodeObject *codeobj_4f019f07580fdf9db6f5f7b6a8d472c9;
static PyCodeObject *codeobj_29c71d80aa90df85f84b4375176659e8;
static PyCodeObject *codeobj_26a157c8ad8249ac8335c0675bf63d8c;
static PyCodeObject *codeobj_bf502c75be877a7f1761059066e378b6;
static PyCodeObject *codeobj_5f3c69bab71b5855d3cca521f46b6240;
static PyCodeObject *codeobj_17236095b803dfaa4508a944fabd1501;
static PyCodeObject *codeobj_d5246a112a4d127550d1b2f5eecf38f2;
static PyCodeObject *codeobj_799bcbcc714b317d6ab66a84c99bff0a;
static PyCodeObject *codeobj_52a67043ad8c473fec5f005a16fc7484;
static PyCodeObject *codeobj_11daac69127bc4b1332f013f3788ccde;
static PyCodeObject *codeobj_26ad7b1131685d69733c17efa46cfc56;
static PyCodeObject *codeobj_adddb674aca57e56cb311d54effb4486;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_3d541f49a8fda2dfe9a9a0f962378f08 );
    codeobj_91c5fd307105d8c5eb47c9f6ab80c7a2 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 278, const_tuple_05dadc01141b31df536583e6e2798580_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_a6bc9fc9cbcc7aad54ab5534e23a973a = MAKE_CODEOBJ( module_filename_obj, const_str_digest_9510e51027f71cceb45091b2fa751e76, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_2ff8ab1c8bc2b4722af7cb6304c8f9cb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AllFiguresPage, 89, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_56eaf9c20edf0f83e8ca1007194d7244 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Download, 112, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_d5530390e95e557195a38c84aae20396 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_FavIcon, 64, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_255a10bf58c0f52531761a99c6df03f3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_FigureCanvasWebAgg, 49, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_8c2d2a58a411a4c99aa649f8241de86d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MplJs, 104, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_86295468592d459d67ea0acb84bbdbc3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ServerThread, 41, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_ac4a2b74daab0cd32f52a8f231717a01 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_SingleFigurePage, 70, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_94d52aad8429f3f66731c14b3a262317 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_WebAggApplication, 60, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_e4252a982fc0f588ca0f884fdde91a3c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_WebSocket, 135, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_7511e4691622db519fa9a0190d02ddea = MAKE_CODEOBJ( module_filename_obj, const_str_plain__BackendWebAgg, 313, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_d2ba4fddab043b1038a29d853da561cb = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 71, const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple, 3, 1, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS );
    codeobj_10590e77b6e6b31bea078cf8e3648e6c = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 90, const_tuple_ef4aecb07a1e46020c0f0abeb71697bf_tuple, 3, 1, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS );
    codeobj_4056424134cd88e89ad9c087662b9c69 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 174, const_tuple_str_plain_self_str_plain_url_prefix_str_plain___class___tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_05856c71000b555c771d767b07290f08 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_catch_sigint, 274, const_tuple_str_plain_old_handler_tuple, 0, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_28daa94b5b13f15a8e34ee41354f6c3c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get, 113, const_tuple_50b9d97eb7a12ae78ce70b8ce8f4f2ec_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_579d25a8138587c2d8a273b6b61298cc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get, 75, const_tuple_40307db08d5aebdc7fcfdcb3df2165ca_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c309583248caa2d6e005fb1b6559bdd5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get, 65, const_tuple_str_plain_self_str_plain_image_path_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a05d20662ec2597c5918a7f05956ca17 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get, 105, const_tuple_str_plain_self_str_plain_js_content_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bcd86a8402f4325e8d71659cde92e3cb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get, 94, const_tuple_str_plain_self_str_plain_ws_uri_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d24eb11dbf63ec52ed1531708d5e939b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_initialize, 209, const_tuple_661d43aec940d5080e7d4f356e477dfc_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_59a631561ae8e27b85ee9e9c4e28e2c0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ipython_inline_display, 293, const_tuple_0b290ac3226aa5d811f7bfe0b354c55a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e871247c6ee89deb80b38ca037bb1b5b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_new_timer, 55, const_tuple_str_plain_self_str_plain_args_str_plain_kwargs_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_4f019f07580fdf9db6f5f7b6a8d472c9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_on_close, 145, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_29c71d80aa90df85f84b4375176659e8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_on_message, 148, const_tuple_str_plain_self_str_plain_message_str_plain_manager_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_26a157c8ad8249ac8335c0675bf63d8c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_open, 138, const_tuple_str_plain_self_str_plain_fignum_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bf502c75be877a7f1761059066e378b6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_random_ports, 221, const_tuple_str_plain_port_str_plain_n_str_plain_i_tuple, 2, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5f3c69bab71b5855d3cca521f46b6240 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_run, 42, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_17236095b803dfaa4508a944fabd1501 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_send_binary, 166, const_tuple_str_plain_self_str_plain_blob_str_plain_data_uri_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d5246a112a4d127550d1b2f5eecf38f2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_send_json, 163, const_tuple_str_plain_self_str_plain_content_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_799bcbcc714b317d6ab66a84c99bff0a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show, 50, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_52a67043ad8c473fec5f005a16fc7484 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show, 321, const_tuple_str_plain_url_str_plain_webbrowser_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_11daac69127bc4b1332f013f3788ccde = MAKE_CODEOBJ( module_filename_obj, const_str_plain_shutdown, 268, const_tuple_str_plain_ioloop_str_plain_cls_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_26ad7b1131685d69733c17efa46cfc56 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_start, 254, const_tuple_30953d211847cb7e4eb831df36500ce8_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_adddb674aca57e56cb311d54effb4486 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_trigger_manager_draw, 317, const_tuple_str_plain_manager_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports$$$genobj_1_random_ports_maker( void );


static PyObject *matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_10_get(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_11_open(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_12_on_close(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_13_on_message(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_14_send_json(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_15_send_binary(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_16___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_17_initialize( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start$$$function_1_shutdown(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_19_ipython_inline_display(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_1_run(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_20_trigger_manager_draw(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_21_show(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_2_show(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_3_new_timer(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_4_get(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_5___init__( PyObject *kw_defaults );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_6_get(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_7___init__( PyObject *kw_defaults );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_8_get(  );


static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_9_get(  );


// The module function definitions.
static PyObject *impl_matplotlib$backends$backend_webagg$$$function_1_run( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5f3c69bab71b5855d3cca521f46b6240;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5f3c69bab71b5855d3cca521f46b6240 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5f3c69bab71b5855d3cca521f46b6240, codeobj_5f3c69bab71b5855d3cca521f46b6240, module_matplotlib$backends$backend_webagg, sizeof(void *) );
    frame_5f3c69bab71b5855d3cca521f46b6240 = cache_frame_5f3c69bab71b5855d3cca521f46b6240;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5f3c69bab71b5855d3cca521f46b6240 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5f3c69bab71b5855d3cca521f46b6240 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_ioloop );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_IOLoop );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_5f3c69bab71b5855d3cca521f46b6240->m_frame.f_lineno = 43;
        tmp_called_instance_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_instance );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_5f3c69bab71b5855d3cca521f46b6240->m_frame.f_lineno = 43;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_start );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f3c69bab71b5855d3cca521f46b6240 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f3c69bab71b5855d3cca521f46b6240 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5f3c69bab71b5855d3cca521f46b6240, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5f3c69bab71b5855d3cca521f46b6240->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5f3c69bab71b5855d3cca521f46b6240, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5f3c69bab71b5855d3cca521f46b6240,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_5f3c69bab71b5855d3cca521f46b6240 == cache_frame_5f3c69bab71b5855d3cca521f46b6240 )
    {
        Py_DECREF( frame_5f3c69bab71b5855d3cca521f46b6240 );
    }
    cache_frame_5f3c69bab71b5855d3cca521f46b6240 = NULL;

    assertFrameObject( frame_5f3c69bab71b5855d3cca521f46b6240 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_1_run );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_1_run );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_2_show( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_799bcbcc714b317d6ab66a84c99bff0a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_799bcbcc714b317d6ab66a84c99bff0a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_799bcbcc714b317d6ab66a84c99bff0a, codeobj_799bcbcc714b317d6ab66a84c99bff0a, module_matplotlib$backends$backend_webagg, sizeof(void *) );
    frame_799bcbcc714b317d6ab66a84c99bff0a = cache_frame_799bcbcc714b317d6ab66a84c99bff0a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_799bcbcc714b317d6ab66a84c99bff0a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_799bcbcc714b317d6ab66a84c99bff0a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_799bcbcc714b317d6ab66a84c99bff0a->m_frame.f_lineno = 53;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_799bcbcc714b317d6ab66a84c99bff0a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_799bcbcc714b317d6ab66a84c99bff0a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_799bcbcc714b317d6ab66a84c99bff0a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_799bcbcc714b317d6ab66a84c99bff0a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_799bcbcc714b317d6ab66a84c99bff0a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_799bcbcc714b317d6ab66a84c99bff0a,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_799bcbcc714b317d6ab66a84c99bff0a == cache_frame_799bcbcc714b317d6ab66a84c99bff0a )
    {
        Py_DECREF( frame_799bcbcc714b317d6ab66a84c99bff0a );
    }
    cache_frame_799bcbcc714b317d6ab66a84c99bff0a = NULL;

    assertFrameObject( frame_799bcbcc714b317d6ab66a84c99bff0a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_2_show );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_2_show );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_3_new_timer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_e871247c6ee89deb80b38ca037bb1b5b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e871247c6ee89deb80b38ca037bb1b5b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e871247c6ee89deb80b38ca037bb1b5b, codeobj_e871247c6ee89deb80b38ca037bb1b5b, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e871247c6ee89deb80b38ca037bb1b5b = cache_frame_e871247c6ee89deb80b38ca037bb1b5b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e871247c6ee89deb80b38ca037bb1b5b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e871247c6ee89deb80b38ca037bb1b5b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_TimerTornado );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TimerTornado );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TimerTornado" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e871247c6ee89deb80b38ca037bb1b5b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e871247c6ee89deb80b38ca037bb1b5b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e871247c6ee89deb80b38ca037bb1b5b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e871247c6ee89deb80b38ca037bb1b5b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e871247c6ee89deb80b38ca037bb1b5b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e871247c6ee89deb80b38ca037bb1b5b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e871247c6ee89deb80b38ca037bb1b5b,
        type_description_1,
        par_self,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_e871247c6ee89deb80b38ca037bb1b5b == cache_frame_e871247c6ee89deb80b38ca037bb1b5b )
    {
        Py_DECREF( frame_e871247c6ee89deb80b38ca037bb1b5b );
    }
    cache_frame_e871247c6ee89deb80b38ca037bb1b5b = NULL;

    assertFrameObject( frame_e871247c6ee89deb80b38ca037bb1b5b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_3_new_timer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_3_new_timer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_4_get( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_image_path = NULL;
    struct Nuitka_FrameObject *frame_c309583248caa2d6e005fb1b6559bdd5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c309583248caa2d6e005fb1b6559bdd5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c309583248caa2d6e005fb1b6559bdd5, codeobj_c309583248caa2d6e005fb1b6559bdd5, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *) );
    frame_c309583248caa2d6e005fb1b6559bdd5 = cache_frame_c309583248caa2d6e005fb1b6559bdd5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c309583248caa2d6e005fb1b6559bdd5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c309583248caa2d6e005fb1b6559bdd5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_c309583248caa2d6e005fb1b6559bdd5->m_frame.f_lineno = 66;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_set_header, &PyTuple_GET_ITEM( const_tuple_442b8eef8c52cbabac4bc765f0a76ebc_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_Path );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Path );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Path" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_2;
        tmp_subscript_name_1 = const_str_plain_datapath;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = const_str_plain_images;
        tmp_args_element_name_3 = const_str_digest_58b59e05f5b467ff947a344bcf92c599;
        frame_c309583248caa2d6e005fb1b6559bdd5->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_image_path == NULL );
        var_image_path = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_write );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_image_path );
        tmp_called_instance_2 = var_image_path;
        frame_c309583248caa2d6e005fb1b6559bdd5->m_frame.f_lineno = 68;
        tmp_args_element_name_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_read_bytes );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 68;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_c309583248caa2d6e005fb1b6559bdd5->m_frame.f_lineno = 68;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c309583248caa2d6e005fb1b6559bdd5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c309583248caa2d6e005fb1b6559bdd5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c309583248caa2d6e005fb1b6559bdd5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c309583248caa2d6e005fb1b6559bdd5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c309583248caa2d6e005fb1b6559bdd5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c309583248caa2d6e005fb1b6559bdd5,
        type_description_1,
        par_self,
        var_image_path
    );


    // Release cached frame.
    if ( frame_c309583248caa2d6e005fb1b6559bdd5 == cache_frame_c309583248caa2d6e005fb1b6559bdd5 )
    {
        Py_DECREF( frame_c309583248caa2d6e005fb1b6559bdd5 );
    }
    cache_frame_c309583248caa2d6e005fb1b6559bdd5 = NULL;

    assertFrameObject( frame_c309583248caa2d6e005fb1b6559bdd5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_4_get );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_image_path );
    Py_DECREF( var_image_path );
    var_image_path = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_image_path );
    var_image_path = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_4_get );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_5___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_application = python_pars[ 1 ];
    PyObject *par_request = python_pars[ 2 ];
    PyObject *par_url_prefix = python_pars[ 3 ];
    PyObject *par_kwargs = python_pars[ 4 ];
    struct Nuitka_FrameObject *frame_d2ba4fddab043b1038a29d853da561cb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d2ba4fddab043b1038a29d853da561cb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d2ba4fddab043b1038a29d853da561cb, codeobj_d2ba4fddab043b1038a29d853da561cb, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d2ba4fddab043b1038a29d853da561cb = cache_frame_d2ba4fddab043b1038a29d853da561cb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d2ba4fddab043b1038a29d853da561cb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d2ba4fddab043b1038a29d853da561cb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_url_prefix );
        tmp_assattr_name_1 = par_url_prefix;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_url_prefix, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_call_result_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_application );
        tmp_tuple_element_1 = par_application;
        tmp_dircall_arg2_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_request );
        tmp_tuple_element_1 = par_request;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_call_result_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d2ba4fddab043b1038a29d853da561cb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d2ba4fddab043b1038a29d853da561cb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d2ba4fddab043b1038a29d853da561cb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d2ba4fddab043b1038a29d853da561cb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d2ba4fddab043b1038a29d853da561cb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d2ba4fddab043b1038a29d853da561cb,
        type_description_1,
        par_self,
        par_application,
        par_request,
        par_url_prefix,
        par_kwargs,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_d2ba4fddab043b1038a29d853da561cb == cache_frame_d2ba4fddab043b1038a29d853da561cb )
    {
        Py_DECREF( frame_d2ba4fddab043b1038a29d853da561cb );
    }
    cache_frame_d2ba4fddab043b1038a29d853da561cb = NULL;

    assertFrameObject( frame_d2ba4fddab043b1038a29d853da561cb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_5___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_application );
    Py_DECREF( par_application );
    par_application = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_url_prefix );
    Py_DECREF( par_url_prefix );
    par_url_prefix = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_application );
    Py_DECREF( par_application );
    par_application = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_url_prefix );
    Py_DECREF( par_url_prefix );
    par_url_prefix = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_5___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_6_get( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_fignum = python_pars[ 1 ];
    PyObject *var_manager = NULL;
    PyObject *var_ws_uri = NULL;
    struct Nuitka_FrameObject *frame_579d25a8138587c2d8a273b6b61298cc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_579d25a8138587c2d8a273b6b61298cc = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_579d25a8138587c2d8a273b6b61298cc, codeobj_579d25a8138587c2d8a273b6b61298cc, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_579d25a8138587c2d8a273b6b61298cc = cache_frame_579d25a8138587c2d8a273b6b61298cc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_579d25a8138587c2d8a273b6b61298cc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_579d25a8138587c2d8a273b6b61298cc ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_int_arg_1;
        CHECK_OBJECT( par_fignum );
        tmp_int_arg_1 = par_fignum;
        tmp_assign_source_1 = PyNumber_Int( tmp_int_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_fignum;
            assert( old != NULL );
            par_fignum = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_Gcf );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_fignum );
        tmp_args_element_name_1 = par_fignum;
        frame_579d25a8138587c2d8a273b6b61298cc->m_frame.f_lineno = 77;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get_fig_manager, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_manager == NULL );
        var_manager = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_3;
        tmp_source_name_1 = const_str_digest_d089ded72d92e062b8046828d1680e22;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
        assert( !(tmp_called_name_1 == NULL) );
        tmp_dict_key_1 = const_str_plain_req;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_request );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 79;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_prefix;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_url_prefix );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 80;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_579d25a8138587c2d8a273b6b61298cc->m_frame.f_lineno = 79;
        tmp_assign_source_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_ws_uri == NULL );
        var_ws_uri = tmp_assign_source_3;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_source_name_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_source_name_8;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_render );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = const_tuple_str_digest_82a10b5e093fc185ab39410caa4ed093_tuple;
        tmp_dict_key_3 = const_str_plain_prefix;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_url_prefix );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 83;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_2 = _PyDict_NewPresized( 5 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_ws_uri;
        CHECK_OBJECT( var_ws_uri );
        tmp_dict_value_4 = var_ws_uri;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_fig_id;
        CHECK_OBJECT( par_fignum );
        tmp_dict_value_5 = par_fignum;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_toolitems;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_core );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "core" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_2;
        tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_NavigationToolbar2WebAgg );
        if ( tmp_source_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_toolitems );
        Py_DECREF( tmp_source_name_6 );
        if ( tmp_dict_value_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_canvas;
        CHECK_OBJECT( var_manager );
        tmp_source_name_8 = var_manager;
        tmp_dict_value_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_canvas );
        if ( tmp_dict_value_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 87;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        frame_579d25a8138587c2d8a273b6b61298cc->m_frame.f_lineno = 81;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_2 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_579d25a8138587c2d8a273b6b61298cc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_579d25a8138587c2d8a273b6b61298cc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_579d25a8138587c2d8a273b6b61298cc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_579d25a8138587c2d8a273b6b61298cc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_579d25a8138587c2d8a273b6b61298cc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_579d25a8138587c2d8a273b6b61298cc,
        type_description_1,
        par_self,
        par_fignum,
        var_manager,
        var_ws_uri
    );


    // Release cached frame.
    if ( frame_579d25a8138587c2d8a273b6b61298cc == cache_frame_579d25a8138587c2d8a273b6b61298cc )
    {
        Py_DECREF( frame_579d25a8138587c2d8a273b6b61298cc );
    }
    cache_frame_579d25a8138587c2d8a273b6b61298cc = NULL;

    assertFrameObject( frame_579d25a8138587c2d8a273b6b61298cc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_6_get );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fignum );
    Py_DECREF( par_fignum );
    par_fignum = NULL;

    CHECK_OBJECT( (PyObject *)var_manager );
    Py_DECREF( var_manager );
    var_manager = NULL;

    CHECK_OBJECT( (PyObject *)var_ws_uri );
    Py_DECREF( var_ws_uri );
    var_ws_uri = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fignum );
    Py_DECREF( par_fignum );
    par_fignum = NULL;

    Py_XDECREF( var_manager );
    var_manager = NULL;

    Py_XDECREF( var_ws_uri );
    var_ws_uri = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_6_get );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_7___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_application = python_pars[ 1 ];
    PyObject *par_request = python_pars[ 2 ];
    PyObject *par_url_prefix = python_pars[ 3 ];
    PyObject *par_kwargs = python_pars[ 4 ];
    struct Nuitka_FrameObject *frame_10590e77b6e6b31bea078cf8e3648e6c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_10590e77b6e6b31bea078cf8e3648e6c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_10590e77b6e6b31bea078cf8e3648e6c, codeobj_10590e77b6e6b31bea078cf8e3648e6c, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_10590e77b6e6b31bea078cf8e3648e6c = cache_frame_10590e77b6e6b31bea078cf8e3648e6c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_10590e77b6e6b31bea078cf8e3648e6c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_10590e77b6e6b31bea078cf8e3648e6c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_url_prefix );
        tmp_assattr_name_1 = par_url_prefix;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_url_prefix, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_call_result_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_application );
        tmp_tuple_element_1 = par_application;
        tmp_dircall_arg2_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_request );
        tmp_tuple_element_1 = par_request;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_call_result_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_10590e77b6e6b31bea078cf8e3648e6c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_10590e77b6e6b31bea078cf8e3648e6c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_10590e77b6e6b31bea078cf8e3648e6c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_10590e77b6e6b31bea078cf8e3648e6c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_10590e77b6e6b31bea078cf8e3648e6c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_10590e77b6e6b31bea078cf8e3648e6c,
        type_description_1,
        par_self,
        par_application,
        par_request,
        par_url_prefix,
        par_kwargs,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_10590e77b6e6b31bea078cf8e3648e6c == cache_frame_10590e77b6e6b31bea078cf8e3648e6c )
    {
        Py_DECREF( frame_10590e77b6e6b31bea078cf8e3648e6c );
    }
    cache_frame_10590e77b6e6b31bea078cf8e3648e6c = NULL;

    assertFrameObject( frame_10590e77b6e6b31bea078cf8e3648e6c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_7___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_application );
    Py_DECREF( par_application );
    par_application = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_url_prefix );
    Py_DECREF( par_url_prefix );
    par_url_prefix = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_application );
    Py_DECREF( par_application );
    par_application = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_url_prefix );
    Py_DECREF( par_url_prefix );
    par_url_prefix = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_7___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_8_get( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_ws_uri = NULL;
    struct Nuitka_FrameObject *frame_bcd86a8402f4325e8d71659cde92e3cb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_bcd86a8402f4325e8d71659cde92e3cb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bcd86a8402f4325e8d71659cde92e3cb, codeobj_bcd86a8402f4325e8d71659cde92e3cb, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *) );
    frame_bcd86a8402f4325e8d71659cde92e3cb = cache_frame_bcd86a8402f4325e8d71659cde92e3cb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bcd86a8402f4325e8d71659cde92e3cb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bcd86a8402f4325e8d71659cde92e3cb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_3;
        tmp_source_name_1 = const_str_digest_d089ded72d92e062b8046828d1680e22;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
        assert( !(tmp_called_name_1 == NULL) );
        tmp_dict_key_1 = const_str_plain_req;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_request );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 95;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_prefix;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_url_prefix );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 96;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_bcd86a8402f4325e8d71659cde92e3cb->m_frame.f_lineno = 95;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_ws_uri == NULL );
        var_ws_uri = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_render );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = const_tuple_str_digest_c8055de4fcc7313dbf78247962e0824e_tuple;
        tmp_dict_key_3 = const_str_plain_prefix;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_url_prefix );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 99;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_2 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_ws_uri;
        CHECK_OBJECT( var_ws_uri );
        tmp_dict_value_4 = var_ws_uri;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_figures;
        tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_sorted );
        assert( tmp_called_name_3 != NULL );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_Gcf );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_1;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_figs );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 101;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_bcd86a8402f4325e8d71659cde92e3cb->m_frame.f_lineno = 101;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 101;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_bcd86a8402f4325e8d71659cde92e3cb->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_dict_value_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_dict_value_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 101;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_toolitems;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_core );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "core" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_2;
        tmp_source_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_NavigationToolbar2WebAgg );
        if ( tmp_source_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_toolitems );
        Py_DECREF( tmp_source_name_7 );
        if ( tmp_dict_value_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        frame_bcd86a8402f4325e8d71659cde92e3cb->m_frame.f_lineno = 97;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_2 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bcd86a8402f4325e8d71659cde92e3cb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bcd86a8402f4325e8d71659cde92e3cb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bcd86a8402f4325e8d71659cde92e3cb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bcd86a8402f4325e8d71659cde92e3cb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bcd86a8402f4325e8d71659cde92e3cb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bcd86a8402f4325e8d71659cde92e3cb,
        type_description_1,
        par_self,
        var_ws_uri
    );


    // Release cached frame.
    if ( frame_bcd86a8402f4325e8d71659cde92e3cb == cache_frame_bcd86a8402f4325e8d71659cde92e3cb )
    {
        Py_DECREF( frame_bcd86a8402f4325e8d71659cde92e3cb );
    }
    cache_frame_bcd86a8402f4325e8d71659cde92e3cb = NULL;

    assertFrameObject( frame_bcd86a8402f4325e8d71659cde92e3cb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_8_get );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_ws_uri );
    Py_DECREF( var_ws_uri );
    var_ws_uri = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_ws_uri );
    var_ws_uri = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_8_get );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_9_get( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_js_content = NULL;
    struct Nuitka_FrameObject *frame_a05d20662ec2597c5918a7f05956ca17;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a05d20662ec2597c5918a7f05956ca17 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a05d20662ec2597c5918a7f05956ca17, codeobj_a05d20662ec2597c5918a7f05956ca17, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *) );
    frame_a05d20662ec2597c5918a7f05956ca17 = cache_frame_a05d20662ec2597c5918a7f05956ca17;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a05d20662ec2597c5918a7f05956ca17 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a05d20662ec2597c5918a7f05956ca17 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_a05d20662ec2597c5918a7f05956ca17->m_frame.f_lineno = 106;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_set_header, &PyTuple_GET_ITEM( const_tuple_799964753fb254af1caffea4583447e9_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_core );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "core" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_FigureManagerWebAgg );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_a05d20662ec2597c5918a7f05956ca17->m_frame.f_lineno = 108;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_javascript );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_js_content == NULL );
        var_js_content = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_3 = par_self;
        CHECK_OBJECT( var_js_content );
        tmp_args_element_name_1 = var_js_content;
        frame_a05d20662ec2597c5918a7f05956ca17->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_write, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a05d20662ec2597c5918a7f05956ca17 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a05d20662ec2597c5918a7f05956ca17 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a05d20662ec2597c5918a7f05956ca17, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a05d20662ec2597c5918a7f05956ca17->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a05d20662ec2597c5918a7f05956ca17, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a05d20662ec2597c5918a7f05956ca17,
        type_description_1,
        par_self,
        var_js_content
    );


    // Release cached frame.
    if ( frame_a05d20662ec2597c5918a7f05956ca17 == cache_frame_a05d20662ec2597c5918a7f05956ca17 )
    {
        Py_DECREF( frame_a05d20662ec2597c5918a7f05956ca17 );
    }
    cache_frame_a05d20662ec2597c5918a7f05956ca17 = NULL;

    assertFrameObject( frame_a05d20662ec2597c5918a7f05956ca17 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_9_get );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_js_content );
    Py_DECREF( var_js_content );
    var_js_content = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_js_content );
    var_js_content = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_9_get );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_10_get( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_fignum = python_pars[ 1 ];
    PyObject *par_fmt = python_pars[ 2 ];
    PyObject *var_manager = NULL;
    PyObject *var_mimetypes = NULL;
    PyObject *var_buff = NULL;
    struct Nuitka_FrameObject *frame_28daa94b5b13f15a8e34ee41354f6c3c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_28daa94b5b13f15a8e34ee41354f6c3c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_28daa94b5b13f15a8e34ee41354f6c3c, codeobj_28daa94b5b13f15a8e34ee41354f6c3c, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_28daa94b5b13f15a8e34ee41354f6c3c = cache_frame_28daa94b5b13f15a8e34ee41354f6c3c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_28daa94b5b13f15a8e34ee41354f6c3c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_28daa94b5b13f15a8e34ee41354f6c3c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_int_arg_1;
        CHECK_OBJECT( par_fignum );
        tmp_int_arg_1 = par_fignum;
        tmp_assign_source_1 = PyNumber_Int( tmp_int_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_fignum;
            assert( old != NULL );
            par_fignum = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_Gcf );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_fignum );
        tmp_args_element_name_1 = par_fignum;
        frame_28daa94b5b13f15a8e34ee41354f6c3c->m_frame.f_lineno = 115;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get_fig_manager, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_manager == NULL );
        var_manager = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = PyDict_Copy( const_dict_623a57f4e4ea26ffffdc7682acb5f6ab );
        assert( var_mimetypes == NULL );
        var_mimetypes = tmp_assign_source_3;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_set_header );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = const_str_digest_6d6a615162e89eb148ba9bf8dbfc06d3;
        CHECK_OBJECT( var_mimetypes );
        tmp_called_instance_2 = var_mimetypes;
        CHECK_OBJECT( par_fmt );
        tmp_args_element_name_4 = par_fmt;
        tmp_args_element_name_5 = const_str_plain_binary;
        frame_28daa94b5b13f15a8e34ee41354f6c3c->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_args_element_name_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_get, call_args );
        }

        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 129;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_28daa94b5b13f15a8e34ee41354f6c3c->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_BytesIO );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BytesIO );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BytesIO" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        frame_28daa94b5b13f15a8e34ee41354f6c3c->m_frame.f_lineno = 131;
        tmp_assign_source_4 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        CHECK_OBJECT( var_manager );
        tmp_source_name_4 = var_manager;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_canvas );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_figure );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_savefig );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_buff );
        tmp_tuple_element_1 = var_buff;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_format;
        CHECK_OBJECT( par_fmt );
        tmp_dict_value_1 = par_fmt;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_28daa94b5b13f15a8e34ee41354f6c3c->m_frame.f_lineno = 132;
        tmp_call_result_2 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_write );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_buff );
        tmp_called_instance_3 = var_buff;
        frame_28daa94b5b13f15a8e34ee41354f6c3c->m_frame.f_lineno = 133;
        tmp_args_element_name_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_getvalue );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 133;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_28daa94b5b13f15a8e34ee41354f6c3c->m_frame.f_lineno = 133;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_28daa94b5b13f15a8e34ee41354f6c3c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_28daa94b5b13f15a8e34ee41354f6c3c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_28daa94b5b13f15a8e34ee41354f6c3c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_28daa94b5b13f15a8e34ee41354f6c3c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_28daa94b5b13f15a8e34ee41354f6c3c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_28daa94b5b13f15a8e34ee41354f6c3c,
        type_description_1,
        par_self,
        par_fignum,
        par_fmt,
        var_manager,
        var_mimetypes,
        var_buff
    );


    // Release cached frame.
    if ( frame_28daa94b5b13f15a8e34ee41354f6c3c == cache_frame_28daa94b5b13f15a8e34ee41354f6c3c )
    {
        Py_DECREF( frame_28daa94b5b13f15a8e34ee41354f6c3c );
    }
    cache_frame_28daa94b5b13f15a8e34ee41354f6c3c = NULL;

    assertFrameObject( frame_28daa94b5b13f15a8e34ee41354f6c3c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_10_get );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fignum );
    Py_DECREF( par_fignum );
    par_fignum = NULL;

    CHECK_OBJECT( (PyObject *)par_fmt );
    Py_DECREF( par_fmt );
    par_fmt = NULL;

    CHECK_OBJECT( (PyObject *)var_manager );
    Py_DECREF( var_manager );
    var_manager = NULL;

    CHECK_OBJECT( (PyObject *)var_mimetypes );
    Py_DECREF( var_mimetypes );
    var_mimetypes = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fignum );
    Py_DECREF( par_fignum );
    par_fignum = NULL;

    CHECK_OBJECT( (PyObject *)par_fmt );
    Py_DECREF( par_fmt );
    par_fmt = NULL;

    Py_XDECREF( var_manager );
    var_manager = NULL;

    Py_XDECREF( var_mimetypes );
    var_mimetypes = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_10_get );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_11_open( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_fignum = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_26a157c8ad8249ac8335c0675bf63d8c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_26a157c8ad8249ac8335c0675bf63d8c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_26a157c8ad8249ac8335c0675bf63d8c, codeobj_26a157c8ad8249ac8335c0675bf63d8c, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *) );
    frame_26a157c8ad8249ac8335c0675bf63d8c = cache_frame_26a157c8ad8249ac8335c0675bf63d8c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_26a157c8ad8249ac8335c0675bf63d8c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_26a157c8ad8249ac8335c0675bf63d8c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_int_arg_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_fignum );
        tmp_int_arg_1 = par_fignum;
        tmp_assattr_name_1 = PyNumber_Int( tmp_int_arg_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_fignum, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_Gcf );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_fig_manager );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_fignum );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_26a157c8ad8249ac8335c0675bf63d8c->m_frame.f_lineno = 140;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_manager, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_manager );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_args_element_name_2 = par_self;
        frame_26a157c8ad8249ac8335c0675bf63d8c->m_frame.f_lineno = 141;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_add_web_socket, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_attribute_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_attribute_name_1 = const_str_plain_set_nodelay;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_4, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( par_self );
            tmp_called_instance_2 = par_self;
            frame_26a157c8ad8249ac8335c0675bf63d8c->m_frame.f_lineno = 143;
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_nodelay, &PyTuple_GET_ITEM( const_tuple_true_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_26a157c8ad8249ac8335c0675bf63d8c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_26a157c8ad8249ac8335c0675bf63d8c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_26a157c8ad8249ac8335c0675bf63d8c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_26a157c8ad8249ac8335c0675bf63d8c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_26a157c8ad8249ac8335c0675bf63d8c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_26a157c8ad8249ac8335c0675bf63d8c,
        type_description_1,
        par_self,
        par_fignum
    );


    // Release cached frame.
    if ( frame_26a157c8ad8249ac8335c0675bf63d8c == cache_frame_26a157c8ad8249ac8335c0675bf63d8c )
    {
        Py_DECREF( frame_26a157c8ad8249ac8335c0675bf63d8c );
    }
    cache_frame_26a157c8ad8249ac8335c0675bf63d8c = NULL;

    assertFrameObject( frame_26a157c8ad8249ac8335c0675bf63d8c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_11_open );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fignum );
    Py_DECREF( par_fignum );
    par_fignum = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fignum );
    Py_DECREF( par_fignum );
    par_fignum = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_11_open );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_12_on_close( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_4f019f07580fdf9db6f5f7b6a8d472c9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_4f019f07580fdf9db6f5f7b6a8d472c9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4f019f07580fdf9db6f5f7b6a8d472c9, codeobj_4f019f07580fdf9db6f5f7b6a8d472c9, module_matplotlib$backends$backend_webagg, sizeof(void *) );
    frame_4f019f07580fdf9db6f5f7b6a8d472c9 = cache_frame_4f019f07580fdf9db6f5f7b6a8d472c9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4f019f07580fdf9db6f5f7b6a8d472c9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4f019f07580fdf9db6f5f7b6a8d472c9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_manager );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        frame_4f019f07580fdf9db6f5f7b6a8d472c9->m_frame.f_lineno = 146;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_remove_web_socket, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4f019f07580fdf9db6f5f7b6a8d472c9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4f019f07580fdf9db6f5f7b6a8d472c9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4f019f07580fdf9db6f5f7b6a8d472c9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4f019f07580fdf9db6f5f7b6a8d472c9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4f019f07580fdf9db6f5f7b6a8d472c9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4f019f07580fdf9db6f5f7b6a8d472c9,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_4f019f07580fdf9db6f5f7b6a8d472c9 == cache_frame_4f019f07580fdf9db6f5f7b6a8d472c9 )
    {
        Py_DECREF( frame_4f019f07580fdf9db6f5f7b6a8d472c9 );
    }
    cache_frame_4f019f07580fdf9db6f5f7b6a8d472c9 = NULL;

    assertFrameObject( frame_4f019f07580fdf9db6f5f7b6a8d472c9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_12_on_close );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_12_on_close );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_13_on_message( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_message = python_pars[ 1 ];
    PyObject *var_manager = NULL;
    struct Nuitka_FrameObject *frame_29c71d80aa90df85f84b4375176659e8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_29c71d80aa90df85f84b4375176659e8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_29c71d80aa90df85f84b4375176659e8, codeobj_29c71d80aa90df85f84b4375176659e8, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_29c71d80aa90df85f84b4375176659e8 = cache_frame_29c71d80aa90df85f84b4375176659e8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_29c71d80aa90df85f84b4375176659e8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_29c71d80aa90df85f84b4375176659e8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_json );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_message );
        tmp_args_element_name_1 = par_message;
        frame_29c71d80aa90df85f84b4375176659e8->m_frame.f_lineno = 149;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_loads, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_message;
            assert( old != NULL );
            par_message = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_message );
        tmp_subscribed_name_1 = par_message;
        tmp_subscript_name_1 = const_str_plain_type;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_supports_binary;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( par_message );
            tmp_subscribed_name_2 = par_message;
            tmp_subscript_name_2 = const_str_plain_value;
            tmp_assattr_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 154;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_assattr_target_1 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_supports_binary, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 154;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_Gcf );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 156;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_2;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_fig_manager );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_fignum );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 156;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_29c71d80aa90df85f84b4375176659e8->m_frame.f_lineno = 156;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( var_manager == NULL );
            var_manager = tmp_assign_source_2;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( var_manager );
            tmp_compexpr_left_2 = var_manager;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_instance_2;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_3;
                CHECK_OBJECT( var_manager );
                tmp_called_instance_2 = var_manager;
                CHECK_OBJECT( par_message );
                tmp_args_element_name_3 = par_message;
                frame_29c71d80aa90df85f84b4375176659e8->m_frame.f_lineno = 161;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3 };
                    tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_handle_json, call_args );
                }

                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 161;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            branch_no_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_29c71d80aa90df85f84b4375176659e8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_29c71d80aa90df85f84b4375176659e8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_29c71d80aa90df85f84b4375176659e8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_29c71d80aa90df85f84b4375176659e8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_29c71d80aa90df85f84b4375176659e8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_29c71d80aa90df85f84b4375176659e8,
        type_description_1,
        par_self,
        par_message,
        var_manager
    );


    // Release cached frame.
    if ( frame_29c71d80aa90df85f84b4375176659e8 == cache_frame_29c71d80aa90df85f84b4375176659e8 )
    {
        Py_DECREF( frame_29c71d80aa90df85f84b4375176659e8 );
    }
    cache_frame_29c71d80aa90df85f84b4375176659e8 = NULL;

    assertFrameObject( frame_29c71d80aa90df85f84b4375176659e8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_13_on_message );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    Py_XDECREF( var_manager );
    var_manager = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    Py_XDECREF( var_manager );
    var_manager = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_13_on_message );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_14_send_json( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_content = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_d5246a112a4d127550d1b2f5eecf38f2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d5246a112a4d127550d1b2f5eecf38f2 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d5246a112a4d127550d1b2f5eecf38f2, codeobj_d5246a112a4d127550d1b2f5eecf38f2, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *) );
    frame_d5246a112a4d127550d1b2f5eecf38f2 = cache_frame_d5246a112a4d127550d1b2f5eecf38f2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d5246a112a4d127550d1b2f5eecf38f2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d5246a112a4d127550d1b2f5eecf38f2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_write_message );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_json );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_content );
        tmp_args_element_name_2 = par_content;
        frame_d5246a112a4d127550d1b2f5eecf38f2->m_frame.f_lineno = 164;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_dumps, call_args );
        }

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 164;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_d5246a112a4d127550d1b2f5eecf38f2->m_frame.f_lineno = 164;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5246a112a4d127550d1b2f5eecf38f2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5246a112a4d127550d1b2f5eecf38f2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d5246a112a4d127550d1b2f5eecf38f2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d5246a112a4d127550d1b2f5eecf38f2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d5246a112a4d127550d1b2f5eecf38f2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d5246a112a4d127550d1b2f5eecf38f2,
        type_description_1,
        par_self,
        par_content
    );


    // Release cached frame.
    if ( frame_d5246a112a4d127550d1b2f5eecf38f2 == cache_frame_d5246a112a4d127550d1b2f5eecf38f2 )
    {
        Py_DECREF( frame_d5246a112a4d127550d1b2f5eecf38f2 );
    }
    cache_frame_d5246a112a4d127550d1b2f5eecf38f2 = NULL;

    assertFrameObject( frame_d5246a112a4d127550d1b2f5eecf38f2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_14_send_json );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_content );
    Py_DECREF( par_content );
    par_content = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_content );
    Py_DECREF( par_content );
    par_content = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_14_send_json );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_15_send_binary( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_blob = python_pars[ 1 ];
    PyObject *var_data_uri = NULL;
    struct Nuitka_FrameObject *frame_17236095b803dfaa4508a944fabd1501;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_17236095b803dfaa4508a944fabd1501 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_17236095b803dfaa4508a944fabd1501, codeobj_17236095b803dfaa4508a944fabd1501, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_17236095b803dfaa4508a944fabd1501 = cache_frame_17236095b803dfaa4508a944fabd1501;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_17236095b803dfaa4508a944fabd1501 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_17236095b803dfaa4508a944fabd1501 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_supports_binary );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 167;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_write_message );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_blob );
            tmp_tuple_element_1 = par_blob;
            tmp_args_name_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_kw_name_1 = PyDict_Copy( const_dict_8643c5c1af414a0f8c299f5ceeb1d538 );
            frame_17236095b803dfaa4508a944fabd1501->m_frame.f_lineno = 168;
            tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_called_instance_2;
            tmp_source_name_3 = const_str_digest_b3afeac9755413daa29c5ba369677d8a;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_format );
            assert( !(tmp_called_name_2 == NULL) );
            CHECK_OBJECT( par_blob );
            tmp_called_instance_2 = par_blob;
            frame_17236095b803dfaa4508a944fabd1501->m_frame.f_lineno = 171;
            tmp_called_instance_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_plain_base64_tuple, 0 ) );

            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 171;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_17236095b803dfaa4508a944fabd1501->m_frame.f_lineno = 171;
            tmp_args_element_name_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_newline_str_empty_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 171;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_17236095b803dfaa4508a944fabd1501->m_frame.f_lineno = 170;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 170;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( var_data_uri == NULL );
            var_data_uri = tmp_assign_source_1;
        }
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_self );
            tmp_called_instance_3 = par_self;
            CHECK_OBJECT( var_data_uri );
            tmp_args_element_name_2 = var_data_uri;
            frame_17236095b803dfaa4508a944fabd1501->m_frame.f_lineno = 172;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_write_message, call_args );
            }

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_17236095b803dfaa4508a944fabd1501 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_17236095b803dfaa4508a944fabd1501 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_17236095b803dfaa4508a944fabd1501, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_17236095b803dfaa4508a944fabd1501->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_17236095b803dfaa4508a944fabd1501, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_17236095b803dfaa4508a944fabd1501,
        type_description_1,
        par_self,
        par_blob,
        var_data_uri
    );


    // Release cached frame.
    if ( frame_17236095b803dfaa4508a944fabd1501 == cache_frame_17236095b803dfaa4508a944fabd1501 )
    {
        Py_DECREF( frame_17236095b803dfaa4508a944fabd1501 );
    }
    cache_frame_17236095b803dfaa4508a944fabd1501 = NULL;

    assertFrameObject( frame_17236095b803dfaa4508a944fabd1501 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_15_send_binary );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_blob );
    Py_DECREF( par_blob );
    par_blob = NULL;

    Py_XDECREF( var_data_uri );
    var_data_uri = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_blob );
    Py_DECREF( par_blob );
    par_blob = NULL;

    Py_XDECREF( var_data_uri );
    var_data_uri = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_15_send_binary );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_16___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_url_prefix = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_4056424134cd88e89ad9c087662b9c69;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_4056424134cd88e89ad9c087662b9c69 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4056424134cd88e89ad9c087662b9c69, codeobj_4056424134cd88e89ad9c087662b9c69, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4056424134cd88e89ad9c087662b9c69 = cache_frame_4056424134cd88e89ad9c087662b9c69;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4056424134cd88e89ad9c087662b9c69 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4056424134cd88e89ad9c087662b9c69 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_url_prefix );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_url_prefix );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            int tmp_and_left_truth_1;
            PyObject *tmp_and_left_value_1;
            PyObject *tmp_and_right_value_1;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            CHECK_OBJECT( par_url_prefix );
            tmp_subscribed_name_1 = par_url_prefix;
            tmp_subscript_name_1 = const_int_0;
            tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_compexpr_left_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooc";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_1 = const_str_chr_47;
            tmp_and_left_value_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            Py_DECREF( tmp_compexpr_left_1 );
            if ( tmp_and_left_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooc";
                goto frame_exception_exit_1;
            }
            tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
            if ( tmp_and_left_truth_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_and_left_value_1 );

                exception_lineno = 176;
                type_description_1 = "ooc";
                goto frame_exception_exit_1;
            }
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            Py_DECREF( tmp_and_left_value_1 );
            CHECK_OBJECT( par_url_prefix );
            tmp_subscribed_name_2 = par_url_prefix;
            tmp_subscript_name_2 = const_int_neg_1;
            tmp_compexpr_left_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, -1 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooc";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_str_chr_47;
            tmp_and_right_value_1 = RICH_COMPARE_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_and_right_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooc";
                goto frame_exception_exit_1;
            }
            tmp_operand_name_1 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_operand_name_1 = tmp_and_left_value_1;
            and_end_1:;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooc";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                tmp_raise_type_1 = PyExc_AssertionError;
                tmp_raise_value_1 = const_tuple_str_digest_2ea824438b778763e56ab3fed3870421_tuple;
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                Py_INCREF( tmp_raise_value_1 );
                exception_lineno = 176;
                RAISE_EXCEPTION_WITH_VALUE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooc";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_list_element_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_source_name_5;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_4;
        PyObject *tmp_source_name_7;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_tuple_element_6;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_5;
        PyObject *tmp_source_name_8;
        PyObject *tmp_tuple_element_7;
        PyObject *tmp_left_name_6;
        PyObject *tmp_right_name_6;
        PyObject *tmp_source_name_9;
        PyObject *tmp_tuple_element_8;
        PyObject *tmp_left_name_7;
        PyObject *tmp_right_name_7;
        PyObject *tmp_source_name_10;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_3;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 179;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_url_prefix );
        tmp_left_name_1 = par_url_prefix;
        tmp_right_name_1 = const_str_digest_8474c3bcc1faa7791e88cb2c17dd8f40;
        tmp_tuple_element_2 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 182;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_list_element_1 = PyTuple_New( 3 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_2 );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_web );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 183;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_StaticFileHandler );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 183;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_2 );
        tmp_dict_key_1 = const_str_plain_path;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_core );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "core" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 184;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_2;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_FigureManagerWebAgg );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 184;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        frame_4056424134cd88e89ad9c087662b9c69->m_frame.f_lineno = 184;
        tmp_dict_value_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_static_file_path );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 184;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_tuple_element_2, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        PyTuple_SET_ITEM( tmp_list_element_1, 2, tmp_tuple_element_2 );
        tmp_tuple_element_1 = PyList_New( 7 );
        PyList_SET_ITEM( tmp_tuple_element_1, 0, tmp_list_element_1 );
        CHECK_OBJECT( par_url_prefix );
        tmp_left_name_2 = par_url_prefix;
        tmp_right_name_2 = const_str_digest_a7d70f15c8248fb5c4d5e0f25faea644;
        tmp_tuple_element_3 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_tuple_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );

            exception_lineno = 187;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_list_element_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_3 );
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_FavIcon );
        if ( tmp_tuple_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 187;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_3 );
        PyList_SET_ITEM( tmp_tuple_element_1, 1, tmp_list_element_1 );
        CHECK_OBJECT( par_url_prefix );
        tmp_left_name_3 = par_url_prefix;
        tmp_right_name_3 = const_str_digest_f235eb3b97dc845b01d2e43ab3230a53;
        tmp_tuple_element_4 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_3, tmp_right_name_3 );
        if ( tmp_tuple_element_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );

            exception_lineno = 190;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_list_element_1 = PyTuple_New( 3 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_4 );
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_tuple_element_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_SingleFigurePage );
        if ( tmp_tuple_element_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 190;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_4 );
        tmp_dict_key_2 = const_str_plain_url_prefix;
        CHECK_OBJECT( par_url_prefix );
        tmp_dict_value_2 = par_url_prefix;
        tmp_tuple_element_4 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_tuple_element_4, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        PyTuple_SET_ITEM( tmp_list_element_1, 2, tmp_tuple_element_4 );
        PyList_SET_ITEM( tmp_tuple_element_1, 2, tmp_list_element_1 );
        CHECK_OBJECT( par_url_prefix );
        tmp_left_name_4 = par_url_prefix;
        tmp_right_name_4 = const_str_digest_97cf460c4b1e10b498964a539e0a4591;
        tmp_tuple_element_5 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_4, tmp_right_name_4 );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );

            exception_lineno = 194;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_list_element_1 = PyTuple_New( 3 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_5 );
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_AllFiguresPage );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 194;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_5 );
        tmp_dict_key_3 = const_str_plain_url_prefix;
        CHECK_OBJECT( par_url_prefix );
        tmp_dict_value_3 = par_url_prefix;
        tmp_tuple_element_5 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_tuple_element_5, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        PyTuple_SET_ITEM( tmp_list_element_1, 2, tmp_tuple_element_5 );
        PyList_SET_ITEM( tmp_tuple_element_1, 3, tmp_list_element_1 );
        CHECK_OBJECT( par_url_prefix );
        tmp_left_name_5 = par_url_prefix;
        tmp_right_name_5 = const_str_digest_e5180111a112c9c8343168ac5095042b;
        tmp_tuple_element_6 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_5, tmp_right_name_5 );
        if ( tmp_tuple_element_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );

            exception_lineno = 197;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_list_element_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_6 );
        CHECK_OBJECT( par_self );
        tmp_source_name_8 = par_self;
        tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_MplJs );
        if ( tmp_tuple_element_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 197;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_6 );
        PyList_SET_ITEM( tmp_tuple_element_1, 4, tmp_list_element_1 );
        CHECK_OBJECT( par_url_prefix );
        tmp_left_name_6 = par_url_prefix;
        tmp_right_name_6 = const_str_digest_7dd06b8ccaa246845f93fc1486ea3de5;
        tmp_tuple_element_7 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_6, tmp_right_name_6 );
        if ( tmp_tuple_element_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );

            exception_lineno = 201;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_list_element_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_7 );
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_WebSocket );
        if ( tmp_tuple_element_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 201;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_7 );
        PyList_SET_ITEM( tmp_tuple_element_1, 5, tmp_list_element_1 );
        CHECK_OBJECT( par_url_prefix );
        tmp_left_name_7 = par_url_prefix;
        tmp_right_name_7 = const_str_digest_b8901647f1104a11a5798377646ea8e6;
        tmp_tuple_element_8 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_7, tmp_right_name_7 );
        if ( tmp_tuple_element_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );

            exception_lineno = 204;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_list_element_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_8 );
        CHECK_OBJECT( par_self );
        tmp_source_name_10 = par_self;
        tmp_tuple_element_8 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_Download );
        if ( tmp_tuple_element_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_tuple_element_1 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 205;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_8 );
        PyList_SET_ITEM( tmp_tuple_element_1, 6, tmp_list_element_1 );
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_4 = const_str_plain_template_path;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_core );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "core" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 207;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_3;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_FigureManagerWebAgg );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 207;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        frame_4056424134cd88e89ad9c087662b9c69->m_frame.f_lineno = 207;
        tmp_dict_value_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_static_file_path );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 207;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_4056424134cd88e89ad9c087662b9c69->m_frame.f_lineno = 179;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4056424134cd88e89ad9c087662b9c69 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4056424134cd88e89ad9c087662b9c69 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4056424134cd88e89ad9c087662b9c69, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4056424134cd88e89ad9c087662b9c69->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4056424134cd88e89ad9c087662b9c69, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4056424134cd88e89ad9c087662b9c69,
        type_description_1,
        par_self,
        par_url_prefix,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_4056424134cd88e89ad9c087662b9c69 == cache_frame_4056424134cd88e89ad9c087662b9c69 )
    {
        Py_DECREF( frame_4056424134cd88e89ad9c087662b9c69 );
    }
    cache_frame_4056424134cd88e89ad9c087662b9c69 = NULL;

    assertFrameObject( frame_4056424134cd88e89ad9c087662b9c69 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_16___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_url_prefix );
    Py_DECREF( par_url_prefix );
    par_url_prefix = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_url_prefix );
    Py_DECREF( par_url_prefix );
    par_url_prefix = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_16___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_17_initialize( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_url_prefix = python_pars[ 1 ];
    PyObject *par_port = python_pars[ 2 ];
    PyObject *par_address = python_pars[ 3 ];
    PyObject *var_app = NULL;
    PyObject *var_random_ports = NULL;
    PyObject *var_e = NULL;
    nuitka_bool tmp_for_loop_1__break_indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_try_except_1__unhandled_indicator = NULL;
    struct Nuitka_FrameObject *frame_d24eb11dbf63ec52ed1531708d5e939b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_d24eb11dbf63ec52ed1531708d5e939b = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d24eb11dbf63ec52ed1531708d5e939b, codeobj_d24eb11dbf63ec52ed1531708d5e939b, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d24eb11dbf63ec52ed1531708d5e939b = cache_frame_d24eb11dbf63ec52ed1531708d5e939b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d24eb11dbf63ec52ed1531708d5e939b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d24eb11dbf63ec52ed1531708d5e939b ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_cls );
        tmp_source_name_1 = par_cls;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_initialized );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 211;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        CHECK_OBJECT( par_cls );
        tmp_called_name_1 = par_cls;
        tmp_dict_key_1 = const_str_plain_url_prefix;
        CHECK_OBJECT( par_url_prefix );
        tmp_dict_value_1 = par_url_prefix;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_d24eb11dbf63ec52ed1531708d5e939b->m_frame.f_lineno = 215;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 215;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_app == NULL );
        var_app = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_url_prefix );
        tmp_assattr_name_1 = par_url_prefix;
        CHECK_OBJECT( par_cls );
        tmp_assattr_target_1 = par_cls;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_url_prefix, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 217;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports(  );



        assert( var_random_ports == NULL );
        var_random_ports = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_address );
        tmp_compexpr_left_1 = par_address;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_assattr_target_2;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_rcParams );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 234;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_1 = tmp_mvar_value_1;
            tmp_subscript_name_1 = const_str_digest_06bf4bb6663dc5cf008410bbd11c4301;
            tmp_assattr_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_assattr_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_cls );
            tmp_assattr_target_2 = par_cls;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_address, tmp_assattr_name_2 );
            Py_DECREF( tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assattr_name_3;
            PyObject *tmp_assattr_target_3;
            CHECK_OBJECT( par_address );
            tmp_assattr_name_3 = par_address;
            CHECK_OBJECT( par_cls );
            tmp_assattr_target_3 = par_cls;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_address, tmp_assattr_name_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 236;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_assattr_target_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 237;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_2 = tmp_mvar_value_2;
        tmp_subscript_name_2 = const_str_digest_61dec4a44b387cbf49fbbb6055ee75ea;
        tmp_assattr_name_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_assattr_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 237;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_cls );
        tmp_assattr_target_4 = par_cls;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_port, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 237;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_assign_source_3;
        tmp_assign_source_3 = NUITKA_BOOL_FALSE;
        tmp_for_loop_1__break_indicator = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_subscript_name_3;
        CHECK_OBJECT( var_random_ports );
        tmp_called_name_2 = var_random_ports;
        CHECK_OBJECT( par_cls );
        tmp_source_name_2 = par_cls;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_port );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_args_element_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 238;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }

        tmp_subscribed_name_3 = tmp_mvar_value_3;
        tmp_subscript_name_3 = const_str_digest_c10948e26b8029e04e0a0726e5ba7b39;
        tmp_args_element_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 238;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        frame_d24eb11dbf63ec52ed1531708d5e939b->m_frame.f_lineno = 238;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_1:;
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_value_name_1;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_value_name_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_value_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 238;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = exception_keeper_type_1;
        tmp_compexpr_right_2 = PyExc_StopIteration;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            Py_DECREF( exception_keeper_type_1 );
            Py_XDECREF( exception_keeper_value_1 );
            Py_XDECREF( exception_keeper_tb_1 );

            exception_lineno = 238;
            type_description_1 = "ooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            nuitka_bool tmp_assign_source_6;
            tmp_assign_source_6 = NUITKA_BOOL_TRUE;
            tmp_for_loop_1__break_indicator = tmp_assign_source_6;
        }
        Py_DECREF( exception_keeper_type_1 );
        Py_XDECREF( exception_keeper_value_1 );
        Py_XDECREF( exception_keeper_tb_1 );
        goto loop_end_1;
        goto branch_end_3;
        branch_no_3:;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_3;
        branch_end_3:;
    }
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_7 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = par_port;
            assert( old != NULL );
            par_port = tmp_assign_source_7;
            Py_INCREF( par_port );
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = Py_True;
        {
            PyObject *old = tmp_try_except_1__unhandled_indicator;
            tmp_try_except_1__unhandled_indicator = tmp_assign_source_8;
            Py_INCREF( tmp_try_except_1__unhandled_indicator );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_app );
        tmp_source_name_3 = var_app;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_listen );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "ooooooo";
            goto try_except_handler_6;
        }
        CHECK_OBJECT( par_port );
        tmp_args_element_name_3 = par_port;
        CHECK_OBJECT( par_cls );
        tmp_source_name_4 = par_cls;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_address );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 240;
            type_description_1 = "ooooooo";
            goto try_except_handler_6;
        }
        frame_d24eb11dbf63ec52ed1531708d5e939b->m_frame.f_lineno = 240;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "ooooooo";
            goto try_except_handler_6;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = Py_False;
        {
            PyObject *old = tmp_try_except_1__unhandled_indicator;
            assert( old != NULL );
            tmp_try_except_1__unhandled_indicator = tmp_assign_source_9;
            Py_INCREF( tmp_try_except_1__unhandled_indicator );
            Py_DECREF( old );
        }

    }
    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_d24eb11dbf63ec52ed1531708d5e939b, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_d24eb11dbf63ec52ed1531708d5e939b, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_4;
        tmp_compexpr_left_3 = EXC_TYPE(PyThreadState_GET());
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_socket );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 241;
            type_description_1 = "ooooooo";
            goto try_except_handler_7;
        }

        tmp_source_name_5 = tmp_mvar_value_4;
        tmp_compexpr_right_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_error );
        if ( tmp_compexpr_right_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 241;
            type_description_1 = "ooooooo";
            goto try_except_handler_7;
        }
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 241;
            type_description_1 = "ooooooo";
            goto try_except_handler_7;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_10;
            tmp_assign_source_10 = EXC_VALUE(PyThreadState_GET());
            {
                PyObject *old = var_e;
                var_e = tmp_assign_source_10;
                Py_INCREF( var_e );
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_source_name_6;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_5;
            CHECK_OBJECT( var_e );
            tmp_source_name_6 = var_e;
            tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_errno );
            if ( tmp_compexpr_left_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 242;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_errno );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_errno );
            }

            if ( tmp_mvar_value_5 == NULL )
            {
                Py_DECREF( tmp_compexpr_left_4 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "errno" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 242;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }

            tmp_source_name_7 = tmp_mvar_value_5;
            tmp_compexpr_right_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_EADDRINUSE );
            if ( tmp_compexpr_right_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_4 );

                exception_lineno = 242;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            Py_DECREF( tmp_compexpr_left_4 );
            Py_DECREF( tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 242;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 243;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_d24eb11dbf63ec52ed1531708d5e939b->m_frame) frame_d24eb11dbf63ec52ed1531708d5e939b->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooo";
            goto try_except_handler_8;
            branch_no_5:;
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_e );
        var_e = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_7;
        // End of try:
        try_end_3:;
        Py_XDECREF( var_e );
        var_e = NULL;

        goto branch_end_4;
        branch_no_4:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 239;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_d24eb11dbf63ec52ed1531708d5e939b->m_frame) frame_d24eb11dbf63ec52ed1531708d5e939b->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooo";
        goto try_except_handler_7;
        branch_end_4:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_5;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_17_initialize );
    return NULL;
    // End of try:
    try_end_2:;
    {
        nuitka_bool tmp_condition_result_6;
        nuitka_bool tmp_compexpr_left_5;
        nuitka_bool tmp_compexpr_right_5;
        int tmp_truth_name_2;
        CHECK_OBJECT( tmp_try_except_1__unhandled_indicator );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_try_except_1__unhandled_indicator );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        tmp_compexpr_left_5 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_compexpr_right_5 = NUITKA_BOOL_TRUE;
        tmp_condition_result_6 = ( tmp_compexpr_left_5 == tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assattr_name_5;
            PyObject *tmp_assattr_target_5;
            CHECK_OBJECT( par_port );
            tmp_assattr_name_5 = par_port;
            CHECK_OBJECT( par_cls );
            tmp_assattr_target_5 = par_cls;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_port, tmp_assattr_name_5 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 245;
                type_description_1 = "ooooooo";
                goto try_except_handler_5;
            }
        }
        goto try_break_handler_5;
        branch_no_6:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_try_except_1__unhandled_indicator );
    tmp_try_except_1__unhandled_indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_3;
    // try break handler code:
    try_break_handler_5:;
    CHECK_OBJECT( (PyObject *)tmp_try_except_1__unhandled_indicator );
    Py_DECREF( tmp_try_except_1__unhandled_indicator );
    tmp_try_except_1__unhandled_indicator = NULL;

    goto loop_end_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_try_except_1__unhandled_indicator );
    Py_DECREF( tmp_try_except_1__unhandled_indicator );
    tmp_try_except_1__unhandled_indicator = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 238;
        type_description_1 = "ooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto try_except_handler_2;
    // End of try:
    try_end_6:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        nuitka_bool tmp_condition_result_7;
        nuitka_bool tmp_compexpr_left_6;
        nuitka_bool tmp_compexpr_right_6;
        assert( tmp_for_loop_1__break_indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_6 = tmp_for_loop_1__break_indicator;
        tmp_compexpr_right_6 = NUITKA_BOOL_TRUE;
        tmp_condition_result_7 = ( tmp_compexpr_left_6 == tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_22d6d4c089761fdd74b115af7e210732;
            frame_d24eb11dbf63ec52ed1531708d5e939b->m_frame.f_lineno = 248;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_SystemExit, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 248;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        branch_no_7:;
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_assattr_target_6;
        tmp_assattr_name_6 = Py_True;
        CHECK_OBJECT( par_cls );
        tmp_assattr_target_6 = par_cls;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_initialized, tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 252;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d24eb11dbf63ec52ed1531708d5e939b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d24eb11dbf63ec52ed1531708d5e939b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d24eb11dbf63ec52ed1531708d5e939b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d24eb11dbf63ec52ed1531708d5e939b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d24eb11dbf63ec52ed1531708d5e939b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d24eb11dbf63ec52ed1531708d5e939b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d24eb11dbf63ec52ed1531708d5e939b,
        type_description_1,
        par_cls,
        par_url_prefix,
        par_port,
        par_address,
        var_app,
        var_random_ports,
        var_e
    );


    // Release cached frame.
    if ( frame_d24eb11dbf63ec52ed1531708d5e939b == cache_frame_d24eb11dbf63ec52ed1531708d5e939b )
    {
        Py_DECREF( frame_d24eb11dbf63ec52ed1531708d5e939b );
    }
    cache_frame_d24eb11dbf63ec52ed1531708d5e939b = NULL;

    assertFrameObject( frame_d24eb11dbf63ec52ed1531708d5e939b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_17_initialize );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_url_prefix );
    Py_DECREF( par_url_prefix );
    par_url_prefix = NULL;

    Py_XDECREF( par_port );
    par_port = NULL;

    CHECK_OBJECT( (PyObject *)par_address );
    Py_DECREF( par_address );
    par_address = NULL;

    Py_XDECREF( var_app );
    var_app = NULL;

    Py_XDECREF( var_random_ports );
    var_random_ports = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_url_prefix );
    Py_DECREF( par_url_prefix );
    par_url_prefix = NULL;

    Py_XDECREF( par_port );
    par_port = NULL;

    CHECK_OBJECT( (PyObject *)par_address );
    Py_DECREF( par_address );
    par_address = NULL;

    Py_XDECREF( var_app );
    var_app = NULL;

    Py_XDECREF( var_random_ports );
    var_random_ports = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_17_initialize );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_port = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_n = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports$$$genobj_1_random_ports_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_n;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = par_port;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_port );
    Py_DECREF( par_port );
    par_port = NULL;

    CHECK_OBJECT( (PyObject *)par_n );
    Py_DECREF( par_n );
    par_n = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_port );
    Py_DECREF( par_port );
    par_port = NULL;

    CHECK_OBJECT( (PyObject *)par_n );
    Py_DECREF( par_n );
    par_n = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports$$$genobj_1_random_ports_locals {
    PyObject *var_i;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_for_loop_2__for_iterator;
    PyObject *tmp_for_loop_2__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports$$$genobj_1_random_ports_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports$$$genobj_1_random_ports_locals *generator_heap = (struct matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports$$$genobj_1_random_ports_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_i = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->tmp_for_loop_2__for_iterator = NULL;
    generator_heap->tmp_for_loop_2__iter_value = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_bf502c75be877a7f1761059066e378b6, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_xrange_low_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_min );
        assert( tmp_called_name_1 != NULL );
        tmp_args_element_name_1 = const_int_pos_5;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "n" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 228;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( generator->m_closure[0] );
        generator->m_frame->m_frame.f_lineno = 228;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_xrange_low_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_xrange_low_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 228;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        tmp_iter_arg_1 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
        Py_DECREF( tmp_xrange_low_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 228;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 228;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "cco";
                generator_heap->exception_lineno = 228;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = generator_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_3;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "port" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 229;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_2;
        }

        tmp_left_name_1 = PyCell_GET( generator->m_closure[1] );
        CHECK_OBJECT( generator_heap->var_i );
        tmp_right_name_1 = generator_heap->var_i;
        tmp_expression_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 229;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_left_name_1, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_left_name_1, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 229;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 228;
        generator_heap->type_description_1 = "cco";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_xrange_low_2;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "n" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 230;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_left_name_2 = PyCell_GET( generator->m_closure[0] );
        tmp_right_name_2 = const_int_pos_5;
        tmp_xrange_low_2 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_xrange_low_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 230;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        tmp_iter_arg_2 = BUILTIN_XRANGE1( tmp_xrange_low_2 );
        Py_DECREF( tmp_xrange_low_2 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 230;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 230;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_2__for_iterator == NULL );
        generator_heap->tmp_for_loop_2__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = generator_heap->tmp_for_loop_2__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "cco";
                generator_heap->exception_lineno = 230;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_2__iter_value;
            generator_heap->tmp_for_loop_2__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_for_loop_2__iter_value );
        tmp_assign_source_6 = generator_heap->tmp_for_loop_2__iter_value;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_4;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_5;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_2;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "port" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }

        tmp_left_name_3 = PyCell_GET( generator->m_closure[1] );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_random );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_random );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "random" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_randint );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }
        tmp_left_name_4 = const_int_neg_2;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "n" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }

        tmp_right_name_4 = PyCell_GET( generator->m_closure[0] );
        tmp_args_element_name_3 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_4, tmp_right_name_4 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }
        tmp_left_name_5 = const_int_pos_2;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "n" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }

        tmp_right_name_5 = PyCell_GET( generator->m_closure[0] );
        tmp_args_element_name_4 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_5, tmp_right_name_5 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }
        generator->m_frame->m_frame.f_lineno = 231;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_right_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }
        tmp_expression_name_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_left_name_3, sizeof(PyObject *), &tmp_right_name_3, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_left_name_4, sizeof(PyObject *), &tmp_right_name_4, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_left_name_5, sizeof(PyObject *), &tmp_right_name_5, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 2;
        return tmp_expression_name_2;
        yield_return_2:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_left_name_3, sizeof(PyObject *), &tmp_right_name_3, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_left_name_4, sizeof(PyObject *), &tmp_right_name_4, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_left_name_5, sizeof(PyObject *), &tmp_right_name_5, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 231;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }
        tmp_yield_result_2 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 230;
        generator_heap->type_description_1 = "cco";
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_2__iter_value );
    generator_heap->tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_2__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_2__for_iterator );
    generator_heap->tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[1],
            generator->m_closure[0],
            generator_heap->var_i
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:
    try_end_3:;
    Py_XDECREF( generator_heap->tmp_for_loop_2__iter_value );
    generator_heap->tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_2__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_2__for_iterator );
    generator_heap->tmp_for_loop_2__for_iterator = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports$$$genobj_1_random_ports_maker( void )
{
    return Nuitka_Generator_New(
        matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports$$$genobj_1_random_ports_context,
        module_matplotlib$backends$backend_webagg,
        const_str_plain_random_ports,
#if PYTHON_VERSION >= 350
        const_str_digest_1f46cbc1f0f599cad3baec9d5fe3b6f9,
#endif
        codeobj_bf502c75be877a7f1761059066e378b6,
        2,
        sizeof(struct matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports$$$genobj_1_random_ports_locals)
    );
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_18_start( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_cls = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *var_ioloop = PyCell_EMPTY();
    struct Nuitka_CellObject *var_shutdown = PyCell_EMPTY();
    PyObject *var_catch_sigint = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_26ad7b1131685d69733c17efa46cfc56;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_26ad7b1131685d69733c17efa46cfc56 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_26ad7b1131685d69733c17efa46cfc56, codeobj_26ad7b1131685d69733c17efa46cfc56, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_26ad7b1131685d69733c17efa46cfc56 = cache_frame_26ad7b1131685d69733c17efa46cfc56;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_26ad7b1131685d69733c17efa46cfc56 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_26ad7b1131685d69733c17efa46cfc56 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( PyCell_GET( par_cls ) );
        tmp_source_name_1 = PyCell_GET( par_cls );
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_started );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 256;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 266;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_ioloop );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_IOLoop );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 266;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_instance );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_ioloop ) == NULL );
        PyCell_SET( var_ioloop, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start$$$function_1_shutdown(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = par_cls;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] = var_ioloop;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] );


        assert( PyCell_GET( var_shutdown ) == NULL );
        PyCell_SET( var_shutdown, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_contextmanager );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_contextmanager );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "contextmanager" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 274;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        tmp_args_element_name_1 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] = var_ioloop;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[1] = var_shutdown;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[1] );


        frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 274;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 274;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        assert( var_catch_sigint == NULL );
        var_catch_sigint = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_True;
        CHECK_OBJECT( PyCell_GET( par_cls ) );
        tmp_assattr_target_1 = PyCell_GET( par_cls );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_started, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 285;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_call_result_1;
        tmp_called_name_2 = LOOKUP_BUILTIN( const_str_plain_print );
        assert( tmp_called_name_2 != NULL );
        frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 287;
        tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_digest_e41aedc5c700d9694e57af141610fd0e_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 287;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 288;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_3;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_stdout );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 288;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 288;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_flush );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 288;
            type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_3;
        CHECK_OBJECT( var_catch_sigint );
        tmp_called_name_3 = var_catch_sigint;
        frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 289;
        tmp_assign_source_4 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 289;
            type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_5 = tmp_with_1__source;
        tmp_called_name_4 = LOOKUP_SPECIAL( tmp_source_name_5, const_str_plain___enter__ );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 289;
            type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 289;
        tmp_assign_source_5 = CALL_FUNCTION_NO_ARGS( tmp_called_name_4 );
        Py_DECREF( tmp_called_name_4 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 289;
            type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_6 = tmp_with_1__source;
        tmp_assign_source_6 = LOOKUP_SPECIAL( tmp_source_name_6, const_str_plain___exit__ );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 289;
            type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_6;
    }
    {
        nuitka_bool tmp_assign_source_7;
        tmp_assign_source_7 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_7;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_3;
        CHECK_OBJECT( PyCell_GET( var_ioloop ) );
        tmp_called_instance_3 = PyCell_GET( var_ioloop );
        frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 290;
        tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_start );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 290;
            type_description_1 = "ccco";
            goto try_except_handler_4;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_26ad7b1131685d69733c17efa46cfc56, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_26ad7b1131685d69733c17efa46cfc56, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 289;
            type_description_1 = "ccco";
            goto try_except_handler_5;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_assign_source_8;
            tmp_assign_source_8 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_8;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_5 = tmp_with_1__exit;
            tmp_args_element_name_2 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_3 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_4 = EXC_TRACEBACK(PyThreadState_GET());
            frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 290;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 290;
                type_description_1 = "ccco";
                goto try_except_handler_5;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 290;
                type_description_1 = "ccco";
                goto try_except_handler_5;
            }
            tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 290;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_26ad7b1131685d69733c17efa46cfc56->m_frame) frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ccco";
            goto try_except_handler_5;
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 289;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_26ad7b1131685d69733c17efa46cfc56->m_frame) frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ccco";
        goto try_except_handler_5;
        branch_end_2:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_18_start );
    return NULL;
    // End of try:
    try_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_with_1__indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_call_result_4;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_6 = tmp_with_1__exit;
            frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 290;
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_3 );
                Py_XDECREF( exception_keeper_value_3 );
                Py_XDECREF( exception_keeper_tb_3 );

                exception_lineno = 290;
                type_description_1 = "ccco";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_4:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    {
        nuitka_bool tmp_condition_result_5;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_5 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_called_name_7;
            PyObject *tmp_call_result_5;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_7 = tmp_with_1__exit;
            frame_26ad7b1131685d69733c17efa46cfc56->m_frame.f_lineno = 290;
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 290;
                type_description_1 = "ccco";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_5:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_26ad7b1131685d69733c17efa46cfc56 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_26ad7b1131685d69733c17efa46cfc56 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_26ad7b1131685d69733c17efa46cfc56 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_26ad7b1131685d69733c17efa46cfc56, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_26ad7b1131685d69733c17efa46cfc56->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_26ad7b1131685d69733c17efa46cfc56, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_26ad7b1131685d69733c17efa46cfc56,
        type_description_1,
        par_cls,
        var_ioloop,
        var_shutdown,
        var_catch_sigint
    );


    // Release cached frame.
    if ( frame_26ad7b1131685d69733c17efa46cfc56 == cache_frame_26ad7b1131685d69733c17efa46cfc56 )
    {
        Py_DECREF( frame_26ad7b1131685d69733c17efa46cfc56 );
    }
    cache_frame_26ad7b1131685d69733c17efa46cfc56 = NULL;

    assertFrameObject( frame_26ad7b1131685d69733c17efa46cfc56 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_18_start );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_ioloop );
    Py_DECREF( var_ioloop );
    var_ioloop = NULL;

    CHECK_OBJECT( (PyObject *)var_shutdown );
    Py_DECREF( var_shutdown );
    var_shutdown = NULL;

    Py_XDECREF( var_catch_sigint );
    var_catch_sigint = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_ioloop );
    Py_DECREF( var_ioloop );
    var_ioloop = NULL;

    CHECK_OBJECT( (PyObject *)var_shutdown );
    Py_DECREF( var_shutdown );
    var_shutdown = NULL;

    Py_XDECREF( var_catch_sigint );
    var_catch_sigint = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_18_start );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_18_start$$$function_1_shutdown( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_11daac69127bc4b1332f013f3788ccde;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_11daac69127bc4b1332f013f3788ccde = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_11daac69127bc4b1332f013f3788ccde, codeobj_11daac69127bc4b1332f013f3788ccde, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *) );
    frame_11daac69127bc4b1332f013f3788ccde = cache_frame_11daac69127bc4b1332f013f3788ccde;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_11daac69127bc4b1332f013f3788ccde );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_11daac69127bc4b1332f013f3788ccde ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "ioloop" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 269;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( self->m_closure[1] );
        frame_11daac69127bc4b1332f013f3788ccde->m_frame.f_lineno = 269;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_stop );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 269;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_2;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_print );
        assert( tmp_called_name_1 != NULL );
        frame_11daac69127bc4b1332f013f3788ccde->m_frame.f_lineno = 270;
        tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_str_digest_056fedea4fee9cdcd1748a56dc3715ea_tuple, 0 ) );

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 270;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 271;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_stdout );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        frame_11daac69127bc4b1332f013f3788ccde->m_frame.f_lineno = 271;
        tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_flush );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_False;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "cls" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 272;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_1 = PyCell_GET( self->m_closure[0] );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_started, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_11daac69127bc4b1332f013f3788ccde );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_11daac69127bc4b1332f013f3788ccde );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_11daac69127bc4b1332f013f3788ccde, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_11daac69127bc4b1332f013f3788ccde->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_11daac69127bc4b1332f013f3788ccde, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_11daac69127bc4b1332f013f3788ccde,
        type_description_1,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_11daac69127bc4b1332f013f3788ccde == cache_frame_11daac69127bc4b1332f013f3788ccde )
    {
        Py_DECREF( frame_11daac69127bc4b1332f013f3788ccde );
    }
    cache_frame_11daac69127bc4b1332f013f3788ccde = NULL;

    assertFrameObject( frame_11daac69127bc4b1332f013f3788ccde );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_18_start$$$function_1_shutdown );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = self->m_closure[0];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = self->m_closure[1];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );


    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint_locals {
    PyObject *var_old_handler;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint_locals *generator_heap = (struct matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_old_handler = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_05856c71000b555c771d767b07290f08, module_matplotlib$backends$backend_webagg, sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_signal );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_signal );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "signal" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 276;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_signal );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 276;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_signal );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_signal );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "signal" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 277;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_SIGINT );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            generator_heap->exception_lineno = 277;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint$$$function_1_lambda(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] = generator->m_closure[0];
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] = generator->m_closure[1];
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] );


        generator->m_frame->m_frame.f_lineno = 276;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 276;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->var_old_handler == NULL );
        generator_heap->var_old_handler = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_expression_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_expression_name_1 = Py_None;
        Py_INCREF( tmp_expression_name_1 );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 280;
            generator_heap->type_description_1 = "o";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Preserve existing published exception.
    generator_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_type_1 );
    generator_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_value_1 );
    generator_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_tb_1 );

    if ( generator_heap->exception_keeper_tb_1 == NULL )
    {
        generator_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }
    else if ( generator_heap->exception_keeper_lineno_1 != 0 )
    {
        generator_heap->exception_keeper_tb_1 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_1, generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    PyException_SetTraceback( generator_heap->exception_keeper_value_1, (PyObject *)generator_heap->exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    // Tried code:
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_signal );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_signal );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "signal" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto try_except_handler_3;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_signal );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto try_except_handler_3;
        }
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_signal );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_signal );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "signal" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto try_except_handler_3;
        }

        tmp_source_name_4 = tmp_mvar_value_4;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_SIGINT );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto try_except_handler_3;
        }
        CHECK_OBJECT( generator_heap->var_old_handler );
        tmp_args_element_name_4 = generator_heap->var_old_handler;
        generator->m_frame->m_frame.f_lineno = 282;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
    if (unlikely( generator_heap->tmp_result == false ))
    {
        generator_heap->exception_lineno = 279;
    }

    if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
    generator_heap->type_description_1 = "o";
    goto try_except_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_signal );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_signal );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "signal" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_5;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_signal );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_signal );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_signal );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "signal" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_6;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_SIGINT );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_3 );

            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( generator_heap->var_old_handler );
        tmp_args_element_name_6 = generator_heap->var_old_handler;
        generator->m_frame->m_frame.f_lineno = 282;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator_heap->var_old_handler
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_old_handler );
    generator_heap->var_old_handler = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->var_old_handler );
    Py_DECREF( generator_heap->var_old_handler );
    generator_heap->var_old_handler = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint_maker( void )
{
    return Nuitka_Generator_New(
        matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint_context,
        module_matplotlib$backends$backend_webagg,
        const_str_plain_catch_sigint,
#if PYTHON_VERSION >= 350
        const_str_digest_1775d0c14bb215a45c234d4f99ab9c5b,
#endif
        codeobj_05856c71000b555c771d767b07290f08,
        2,
        sizeof(struct matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint_locals)
    );
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_sig = python_pars[ 0 ];
    PyObject *par_frame = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_91c5fd307105d8c5eb47c9f6ab80c7a2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_91c5fd307105d8c5eb47c9f6ab80c7a2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_91c5fd307105d8c5eb47c9f6ab80c7a2, codeobj_91c5fd307105d8c5eb47c9f6ab80c7a2, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_91c5fd307105d8c5eb47c9f6ab80c7a2 = cache_frame_91c5fd307105d8c5eb47c9f6ab80c7a2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_91c5fd307105d8c5eb47c9f6ab80c7a2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_91c5fd307105d8c5eb47c9f6ab80c7a2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "ioloop" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 278;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[0] );
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_add_callback_from_signal );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 278;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "shutdown" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 278;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[1] );
        frame_91c5fd307105d8c5eb47c9f6ab80c7a2->m_frame.f_lineno = 278;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 278;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_91c5fd307105d8c5eb47c9f6ab80c7a2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_91c5fd307105d8c5eb47c9f6ab80c7a2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_91c5fd307105d8c5eb47c9f6ab80c7a2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_91c5fd307105d8c5eb47c9f6ab80c7a2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_91c5fd307105d8c5eb47c9f6ab80c7a2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_91c5fd307105d8c5eb47c9f6ab80c7a2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_91c5fd307105d8c5eb47c9f6ab80c7a2,
        type_description_1,
        par_sig,
        par_frame,
        self->m_closure[0],
        self->m_closure[1]
    );


    // Release cached frame.
    if ( frame_91c5fd307105d8c5eb47c9f6ab80c7a2 == cache_frame_91c5fd307105d8c5eb47c9f6ab80c7a2 )
    {
        Py_DECREF( frame_91c5fd307105d8c5eb47c9f6ab80c7a2 );
    }
    cache_frame_91c5fd307105d8c5eb47c9f6ab80c7a2 = NULL;

    assertFrameObject( frame_91c5fd307105d8c5eb47c9f6ab80c7a2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_sig );
    Py_DECREF( par_sig );
    par_sig = NULL;

    CHECK_OBJECT( (PyObject *)par_frame );
    Py_DECREF( par_frame );
    par_frame = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_sig );
    Py_DECREF( par_sig );
    par_sig = NULL;

    CHECK_OBJECT( (PyObject *)par_frame );
    Py_DECREF( par_frame );
    par_frame = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_19_ipython_inline_display( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_figure = python_pars[ 0 ];
    PyObject *var_tornado = NULL;
    PyObject *var_fignum = NULL;
    PyObject *var_tpl = NULL;
    PyObject *var_t = NULL;
    struct Nuitka_FrameObject *frame_59a631561ae8e27b85ee9e9c4e28e2c0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_59a631561ae8e27b85ee9e9c4e28e2c0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_59a631561ae8e27b85ee9e9c4e28e2c0, codeobj_59a631561ae8e27b85ee9e9c4e28e2c0, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_59a631561ae8e27b85ee9e9c4e28e2c0 = cache_frame_59a631561ae8e27b85ee9e9c4e28e2c0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_59a631561ae8e27b85ee9e9c4e28e2c0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_59a631561ae8e27b85ee9e9c4e28e2c0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_6d0365632ca69247476514d3663a86aa;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 294;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_tornado == NULL );
        var_tornado = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_WebAggApplication );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WebAggApplication );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WebAggApplication" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 296;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 296;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_initialize );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 296;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_webagg_server_thread );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_webagg_server_thread );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "webagg_server_thread" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 297;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 297;
        tmp_operand_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_is_alive );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 297;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 297;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_call_result_2;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_webagg_server_thread );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_webagg_server_thread );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "webagg_server_thread" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 298;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_3 = tmp_mvar_value_3;
            frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 298;
            tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_start );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 298;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_figure );
        tmp_source_name_1 = par_figure;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_number );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 300;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_fignum == NULL );
        var_fignum = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_Path );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Path );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Path" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 301;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_4;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_core );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "core" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 301;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_5;
        tmp_called_instance_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_FigureManagerWebAgg );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 301;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 301;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_get_static_file_path );
        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 301;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = const_str_digest_88c7db975dc5d7198afe33fc086f0ebc;
        frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 301;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_called_instance_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 301;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 301;
        tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_read_text );
        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 301;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_tpl == NULL );
        var_tpl = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( var_tornado );
        tmp_source_name_3 = var_tornado;
        tmp_called_instance_6 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_template );
        if ( tmp_called_instance_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 303;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_tpl );
        tmp_args_element_name_3 = var_tpl;
        frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 303;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_Template, call_args );
        }

        Py_DECREF( tmp_called_instance_6 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 303;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_t == NULL );
        var_t = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_instance_7;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_source_name_8;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_8;
        CHECK_OBJECT( var_t );
        tmp_source_name_4 = var_t;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_generate );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_prefix;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_WebAggApplication );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WebAggApplication );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WebAggApplication" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 305;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_6;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_url_prefix );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 305;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 5 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_fig_id;
        CHECK_OBJECT( var_fignum );
        tmp_dict_value_2 = var_fignum;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_toolitems;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_core );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "core" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 307;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_7;
        tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_NavigationToolbar2WebAgg );
        if ( tmp_source_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 307;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_toolitems );
        Py_DECREF( tmp_source_name_6 );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 307;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_canvas;
        CHECK_OBJECT( par_figure );
        tmp_source_name_8 = par_figure;
        tmp_dict_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_canvas );
        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 308;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_port;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_WebAggApplication );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WebAggApplication );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WebAggApplication" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 309;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_8;
        tmp_dict_value_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_port );
        if ( tmp_dict_value_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 309;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 304;
        tmp_called_instance_7 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_instance_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame.f_lineno = 304;
        tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_decode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_7 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_59a631561ae8e27b85ee9e9c4e28e2c0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_59a631561ae8e27b85ee9e9c4e28e2c0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_59a631561ae8e27b85ee9e9c4e28e2c0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_59a631561ae8e27b85ee9e9c4e28e2c0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_59a631561ae8e27b85ee9e9c4e28e2c0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_59a631561ae8e27b85ee9e9c4e28e2c0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_59a631561ae8e27b85ee9e9c4e28e2c0,
        type_description_1,
        par_figure,
        var_tornado,
        var_fignum,
        var_tpl,
        var_t
    );


    // Release cached frame.
    if ( frame_59a631561ae8e27b85ee9e9c4e28e2c0 == cache_frame_59a631561ae8e27b85ee9e9c4e28e2c0 )
    {
        Py_DECREF( frame_59a631561ae8e27b85ee9e9c4e28e2c0 );
    }
    cache_frame_59a631561ae8e27b85ee9e9c4e28e2c0 = NULL;

    assertFrameObject( frame_59a631561ae8e27b85ee9e9c4e28e2c0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_19_ipython_inline_display );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_figure );
    Py_DECREF( par_figure );
    par_figure = NULL;

    CHECK_OBJECT( (PyObject *)var_tornado );
    Py_DECREF( var_tornado );
    var_tornado = NULL;

    CHECK_OBJECT( (PyObject *)var_fignum );
    Py_DECREF( var_fignum );
    var_fignum = NULL;

    CHECK_OBJECT( (PyObject *)var_tpl );
    Py_DECREF( var_tpl );
    var_tpl = NULL;

    CHECK_OBJECT( (PyObject *)var_t );
    Py_DECREF( var_t );
    var_t = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_figure );
    Py_DECREF( par_figure );
    par_figure = NULL;

    Py_XDECREF( var_tornado );
    var_tornado = NULL;

    Py_XDECREF( var_fignum );
    var_fignum = NULL;

    Py_XDECREF( var_tpl );
    var_tpl = NULL;

    Py_XDECREF( var_t );
    var_t = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_19_ipython_inline_display );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_20_trigger_manager_draw( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_manager = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_adddb674aca57e56cb311d54effb4486;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_adddb674aca57e56cb311d54effb4486 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_adddb674aca57e56cb311d54effb4486, codeobj_adddb674aca57e56cb311d54effb4486, module_matplotlib$backends$backend_webagg, sizeof(void *) );
    frame_adddb674aca57e56cb311d54effb4486 = cache_frame_adddb674aca57e56cb311d54effb4486;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_adddb674aca57e56cb311d54effb4486 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_adddb674aca57e56cb311d54effb4486 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_manager );
        tmp_source_name_1 = par_manager;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_canvas );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_adddb674aca57e56cb311d54effb4486->m_frame.f_lineno = 319;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_draw_idle );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_adddb674aca57e56cb311d54effb4486 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_adddb674aca57e56cb311d54effb4486 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_adddb674aca57e56cb311d54effb4486, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_adddb674aca57e56cb311d54effb4486->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_adddb674aca57e56cb311d54effb4486, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_adddb674aca57e56cb311d54effb4486,
        type_description_1,
        par_manager
    );


    // Release cached frame.
    if ( frame_adddb674aca57e56cb311d54effb4486 == cache_frame_adddb674aca57e56cb311d54effb4486 )
    {
        Py_DECREF( frame_adddb674aca57e56cb311d54effb4486 );
    }
    cache_frame_adddb674aca57e56cb311d54effb4486 = NULL;

    assertFrameObject( frame_adddb674aca57e56cb311d54effb4486 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_20_trigger_manager_draw );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_manager );
    Py_DECREF( par_manager );
    par_manager = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_manager );
    Py_DECREF( par_manager );
    par_manager = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_20_trigger_manager_draw );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$backends$backend_webagg$$$function_21_show( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_url = NULL;
    PyObject *var_webbrowser = NULL;
    struct Nuitka_FrameObject *frame_52a67043ad8c473fec5f005a16fc7484;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_52a67043ad8c473fec5f005a16fc7484 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_52a67043ad8c473fec5f005a16fc7484, codeobj_52a67043ad8c473fec5f005a16fc7484, module_matplotlib$backends$backend_webagg, sizeof(void *)+sizeof(void *) );
    frame_52a67043ad8c473fec5f005a16fc7484 = cache_frame_52a67043ad8c473fec5f005a16fc7484;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_52a67043ad8c473fec5f005a16fc7484 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_52a67043ad8c473fec5f005a16fc7484 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_WebAggApplication );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WebAggApplication );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WebAggApplication" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 323;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_52a67043ad8c473fec5f005a16fc7484->m_frame.f_lineno = 323;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_initialize );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 323;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_4;
        tmp_source_name_1 = const_str_digest_e83c35b37b34ee65973562144b1428a0;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
        assert( !(tmp_called_name_1 == NULL) );
        tmp_dict_key_1 = const_str_plain_address;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_WebAggApplication );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WebAggApplication );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WebAggApplication" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 326;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_address );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 326;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_port;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_WebAggApplication );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WebAggApplication );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WebAggApplication" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 327;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_port );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 327;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_prefix;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_WebAggApplication );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WebAggApplication );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WebAggApplication" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 328;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_4;
        tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_url_prefix );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 328;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        frame_52a67043ad8c473fec5f005a16fc7484->m_frame.f_lineno = 325;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 325;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_url == NULL );
        var_url = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_subscript_result_1;
        int tmp_truth_name_1;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 330;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_5;
        tmp_subscript_name_1 = const_str_digest_961ec7b6f052f313b621a72bbdfaf6df;
        tmp_subscript_result_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_subscript_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 330;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_subscript_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscript_result_1 );

            exception_lineno = 330;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_subscript_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_name_name_1;
            PyObject *tmp_globals_name_1;
            PyObject *tmp_locals_name_1;
            PyObject *tmp_fromlist_name_1;
            PyObject *tmp_level_name_1;
            tmp_name_name_1 = const_str_plain_webbrowser;
            tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
            tmp_locals_name_1 = Py_None;
            tmp_fromlist_name_1 = Py_None;
            tmp_level_name_1 = const_int_0;
            frame_52a67043ad8c473fec5f005a16fc7484->m_frame.f_lineno = 331;
            tmp_assign_source_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 331;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( var_webbrowser == NULL );
            var_webbrowser = tmp_assign_source_2;
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var_webbrowser );
            tmp_called_instance_2 = var_webbrowser;
            CHECK_OBJECT( var_url );
            tmp_args_element_name_1 = var_url;
            frame_52a67043ad8c473fec5f005a16fc7484->m_frame.f_lineno = 332;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_open, call_args );
            }

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 332;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_args_element_name_3;
            tmp_called_name_2 = LOOKUP_BUILTIN( const_str_plain_print );
            assert( tmp_called_name_2 != NULL );
            tmp_called_instance_3 = const_str_digest_1977aefd41afa78359014f2ead2320b0;
            CHECK_OBJECT( var_url );
            tmp_args_element_name_3 = var_url;
            frame_52a67043ad8c473fec5f005a16fc7484->m_frame.f_lineno = 334;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_args_element_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_format, call_args );
            }

            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 334;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_52a67043ad8c473fec5f005a16fc7484->m_frame.f_lineno = 334;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 334;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_call_result_4;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_WebAggApplication );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WebAggApplication );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WebAggApplication" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 336;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_6;
        frame_52a67043ad8c473fec5f005a16fc7484->m_frame.f_lineno = 336;
        tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_start );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 336;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_52a67043ad8c473fec5f005a16fc7484 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_52a67043ad8c473fec5f005a16fc7484 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_52a67043ad8c473fec5f005a16fc7484, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_52a67043ad8c473fec5f005a16fc7484->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_52a67043ad8c473fec5f005a16fc7484, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_52a67043ad8c473fec5f005a16fc7484,
        type_description_1,
        var_url,
        var_webbrowser
    );


    // Release cached frame.
    if ( frame_52a67043ad8c473fec5f005a16fc7484 == cache_frame_52a67043ad8c473fec5f005a16fc7484 )
    {
        Py_DECREF( frame_52a67043ad8c473fec5f005a16fc7484 );
    }
    cache_frame_52a67043ad8c473fec5f005a16fc7484 = NULL;

    assertFrameObject( frame_52a67043ad8c473fec5f005a16fc7484 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_21_show );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_url );
    Py_DECREF( var_url );
    var_url = NULL;

    Py_XDECREF( var_webbrowser );
    var_webbrowser = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_url );
    var_url = NULL;

    Py_XDECREF( var_webbrowser );
    var_webbrowser = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg$$$function_21_show );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_10_get(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_10_get,
        const_str_plain_get,
#if PYTHON_VERSION >= 300
        const_str_digest_94ef863da98ac059e0cc890a2de10347,
#endif
        codeobj_28daa94b5b13f15a8e34ee41354f6c3c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_11_open(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_11_open,
        const_str_plain_open,
#if PYTHON_VERSION >= 300
        const_str_digest_8268cbbdc5de4f0dbacb3d0583d75fff,
#endif
        codeobj_26a157c8ad8249ac8335c0675bf63d8c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_12_on_close(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_12_on_close,
        const_str_plain_on_close,
#if PYTHON_VERSION >= 300
        const_str_digest_19787f06335e55a827636e7543295c3a,
#endif
        codeobj_4f019f07580fdf9db6f5f7b6a8d472c9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_13_on_message(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_13_on_message,
        const_str_plain_on_message,
#if PYTHON_VERSION >= 300
        const_str_digest_dcebcd7cd27a3f2bf5f57a79b3cb4a41,
#endif
        codeobj_29c71d80aa90df85f84b4375176659e8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_14_send_json(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_14_send_json,
        const_str_plain_send_json,
#if PYTHON_VERSION >= 300
        const_str_digest_2e8d6fe611be80191eea7726c648e557,
#endif
        codeobj_d5246a112a4d127550d1b2f5eecf38f2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_15_send_binary(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_15_send_binary,
        const_str_plain_send_binary,
#if PYTHON_VERSION >= 300
        const_str_digest_00d47290c7127c3984f25c5335fc3bc0,
#endif
        codeobj_17236095b803dfaa4508a944fabd1501,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_16___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_16___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_05b9dc565b469bc9595924b7779b08e5,
#endif
        codeobj_4056424134cd88e89ad9c087662b9c69,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_17_initialize( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_17_initialize,
        const_str_plain_initialize,
#if PYTHON_VERSION >= 300
        const_str_digest_2dfccf0bcd7bc134e2e81f7b5144e3e0,
#endif
        codeobj_d24eb11dbf63ec52ed1531708d5e939b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_17_initialize$$$function_1_random_ports,
        const_str_plain_random_ports,
#if PYTHON_VERSION >= 300
        const_str_digest_1f46cbc1f0f599cad3baec9d5fe3b6f9,
#endif
        codeobj_bf502c75be877a7f1761059066e378b6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        const_str_digest_88e8b939d1eb863a6008c9132450a421,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_18_start,
        const_str_plain_start,
#if PYTHON_VERSION >= 300
        const_str_digest_7409f88d8b0b7b58054ceec78ccf1548,
#endif
        codeobj_26ad7b1131685d69733c17efa46cfc56,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start$$$function_1_shutdown(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_18_start$$$function_1_shutdown,
        const_str_plain_shutdown,
#if PYTHON_VERSION >= 300
        const_str_digest_38188f2430656c7112bd3e342db7fed3,
#endif
        codeobj_11daac69127bc4b1332f013f3788ccde,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint,
        const_str_plain_catch_sigint,
#if PYTHON_VERSION >= 300
        const_str_digest_1775d0c14bb215a45c234d4f99ab9c5b,
#endif
        codeobj_05856c71000b555c771d767b07290f08,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_18_start$$$function_2_catch_sigint$$$genobj_1_catch_sigint$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_83d81d1e8b1b0b95b8b9444315d7e384,
#endif
        codeobj_91c5fd307105d8c5eb47c9f6ab80c7a2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_19_ipython_inline_display(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_19_ipython_inline_display,
        const_str_plain_ipython_inline_display,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_59a631561ae8e27b85ee9e9c4e28e2c0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_1_run(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_1_run,
        const_str_plain_run,
#if PYTHON_VERSION >= 300
        const_str_digest_a8219884483fbf4a1c80a3e53d26236e,
#endif
        codeobj_5f3c69bab71b5855d3cca521f46b6240,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_20_trigger_manager_draw(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_20_trigger_manager_draw,
        const_str_plain_trigger_manager_draw,
#if PYTHON_VERSION >= 300
        const_str_digest_b0b81ebbdbf86428cb5f6c718abbd9fc,
#endif
        codeobj_adddb674aca57e56cb311d54effb4486,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_21_show(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_21_show,
        const_str_plain_show,
#if PYTHON_VERSION >= 300
        const_str_digest_7c9ae6dc7c0dd7733d03c0e195021bf7,
#endif
        codeobj_52a67043ad8c473fec5f005a16fc7484,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_2_show(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_2_show,
        const_str_plain_show,
#if PYTHON_VERSION >= 300
        const_str_digest_671311a951c594c3cb20f9707bda3cd3,
#endif
        codeobj_799bcbcc714b317d6ab66a84c99bff0a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_3_new_timer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_3_new_timer,
        const_str_plain_new_timer,
#if PYTHON_VERSION >= 300
        const_str_digest_bbbe012e9b37e4f5ca2bd4138d7e5177,
#endif
        codeobj_e871247c6ee89deb80b38ca037bb1b5b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_4_get(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_4_get,
        const_str_plain_get,
#if PYTHON_VERSION >= 300
        const_str_digest_c7f7e94eaf1363c475e2a641103897dd,
#endif
        codeobj_c309583248caa2d6e005fb1b6559bdd5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_5___init__( PyObject *kw_defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_5___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_0afdc4d6eef3f00b0658c948a3cbf4ef,
#endif
        codeobj_d2ba4fddab043b1038a29d853da561cb,
        NULL,
#if PYTHON_VERSION >= 300
        kw_defaults,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_6_get(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_6_get,
        const_str_plain_get,
#if PYTHON_VERSION >= 300
        const_str_digest_656c72a5258e3f4a4478e916d5cfd0c1,
#endif
        codeobj_579d25a8138587c2d8a273b6b61298cc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_7___init__( PyObject *kw_defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_7___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_394f75c46e7f761b798516dac533aae6,
#endif
        codeobj_10590e77b6e6b31bea078cf8e3648e6c,
        NULL,
#if PYTHON_VERSION >= 300
        kw_defaults,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_8_get(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_8_get,
        const_str_plain_get,
#if PYTHON_VERSION >= 300
        const_str_digest_701cb490bd26c3b3c6f69fb944609012,
#endif
        codeobj_bcd86a8402f4325e8d71659cde92e3cb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_9_get(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$backends$backend_webagg$$$function_9_get,
        const_str_plain_get,
#if PYTHON_VERSION >= 300
        const_str_digest_7cfaee3a0ec3cd71db582f039eb9e3ec,
#endif
        codeobj_a05d20662ec2597c5918a7f05956ca17,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$backends$backend_webagg,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_matplotlib$backends$backend_webagg =
{
    PyModuleDef_HEAD_INIT,
    "matplotlib.backends.backend_webagg",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(matplotlib$backends$backend_webagg)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(matplotlib$backends$backend_webagg)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_matplotlib$backends$backend_webagg );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("matplotlib.backends.backend_webagg: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.backends.backend_webagg: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.backends.backend_webagg: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initmatplotlib$backends$backend_webagg" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_matplotlib$backends$backend_webagg = Py_InitModule4(
        "matplotlib.backends.backend_webagg",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_matplotlib$backends$backend_webagg = PyModule_Create( &mdef_matplotlib$backends$backend_webagg );
#endif

    moduledict_matplotlib$backends$backend_webagg = MODULE_DICT( module_matplotlib$backends$backend_webagg );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_matplotlib$backends$backend_webagg,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_matplotlib$backends$backend_webagg,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$backends$backend_webagg,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$backends$backend_webagg,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_matplotlib$backends$backend_webagg );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_526e1fe88ddc5c56efb2459c1633b489, module_matplotlib$backends$backend_webagg );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    struct Nuitka_CellObject *outline_2_var___class__ = PyCell_EMPTY();
    PyObject *outline_3_var___class__ = NULL;
    struct Nuitka_CellObject *outline_4_var___class__ = PyCell_EMPTY();
    struct Nuitka_CellObject *outline_5_var___class__ = PyCell_EMPTY();
    PyObject *outline_6_var___class__ = NULL;
    PyObject *outline_7_var___class__ = NULL;
    PyObject *outline_8_var___class__ = NULL;
    PyObject *outline_9_var___class__ = NULL;
    PyObject *tmp_WebAggApplication$class_creation_1__bases = NULL;
    PyObject *tmp_WebAggApplication$class_creation_1__bases_orig = NULL;
    PyObject *tmp_WebAggApplication$class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_WebAggApplication$class_creation_1__metaclass = NULL;
    PyObject *tmp_WebAggApplication$class_creation_1__prepared = NULL;
    PyObject *tmp_WebAggApplication$class_creation_2__bases = NULL;
    PyObject *tmp_WebAggApplication$class_creation_2__bases_orig = NULL;
    PyObject *tmp_WebAggApplication$class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_WebAggApplication$class_creation_2__metaclass = NULL;
    PyObject *tmp_WebAggApplication$class_creation_2__prepared = NULL;
    PyObject *tmp_WebAggApplication$class_creation_3__bases = NULL;
    PyObject *tmp_WebAggApplication$class_creation_3__bases_orig = NULL;
    PyObject *tmp_WebAggApplication$class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_WebAggApplication$class_creation_3__metaclass = NULL;
    PyObject *tmp_WebAggApplication$class_creation_3__prepared = NULL;
    PyObject *tmp_WebAggApplication$class_creation_4__bases = NULL;
    PyObject *tmp_WebAggApplication$class_creation_4__bases_orig = NULL;
    PyObject *tmp_WebAggApplication$class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_WebAggApplication$class_creation_4__metaclass = NULL;
    PyObject *tmp_WebAggApplication$class_creation_4__prepared = NULL;
    PyObject *tmp_WebAggApplication$class_creation_5__bases = NULL;
    PyObject *tmp_WebAggApplication$class_creation_5__bases_orig = NULL;
    PyObject *tmp_WebAggApplication$class_creation_5__class_decl_dict = NULL;
    PyObject *tmp_WebAggApplication$class_creation_5__metaclass = NULL;
    PyObject *tmp_WebAggApplication$class_creation_5__prepared = NULL;
    PyObject *tmp_WebAggApplication$class_creation_6__bases = NULL;
    PyObject *tmp_WebAggApplication$class_creation_6__bases_orig = NULL;
    PyObject *tmp_WebAggApplication$class_creation_6__class_decl_dict = NULL;
    PyObject *tmp_WebAggApplication$class_creation_6__metaclass = NULL;
    PyObject *tmp_WebAggApplication$class_creation_6__prepared = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_class_creation_4__bases = NULL;
    PyObject *tmp_class_creation_4__bases_orig = NULL;
    PyObject *tmp_class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_class_creation_4__metaclass = NULL;
    PyObject *tmp_class_creation_4__prepared = NULL;
    struct Nuitka_FrameObject *frame_a6bc9fc9cbcc7aad54ab5534e23a973a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_matplotlib$backends$backend_webagg_41 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_86295468592d459d67ea0acb84bbdbc3_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_86295468592d459d67ea0acb84bbdbc3_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *locals_matplotlib$backends$backend_webagg_49 = NULL;
    struct Nuitka_FrameObject *frame_255a10bf58c0f52531761a99c6df03f3_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_255a10bf58c0f52531761a99c6df03f3_3 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *locals_matplotlib$backends$backend_webagg_60 = NULL;
    struct Nuitka_FrameObject *frame_94d52aad8429f3f66731c14b3a262317_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    PyObject *locals_matplotlib$backends$backend_webagg_64 = NULL;
    struct Nuitka_FrameObject *frame_d5530390e95e557195a38c84aae20396_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    static struct Nuitka_FrameObject *cache_frame_d5530390e95e557195a38c84aae20396_5 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *locals_matplotlib$backends$backend_webagg_70 = NULL;
    struct Nuitka_FrameObject *frame_ac4a2b74daab0cd32f52a8f231717a01_6;
    NUITKA_MAY_BE_UNUSED char const *type_description_6 = NULL;
    static struct Nuitka_FrameObject *cache_frame_ac4a2b74daab0cd32f52a8f231717a01_6 = NULL;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *locals_matplotlib$backends$backend_webagg_89 = NULL;
    struct Nuitka_FrameObject *frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7;
    NUITKA_MAY_BE_UNUSED char const *type_description_7 = NULL;
    static struct Nuitka_FrameObject *cache_frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 = NULL;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *locals_matplotlib$backends$backend_webagg_104 = NULL;
    struct Nuitka_FrameObject *frame_8c2d2a58a411a4c99aa649f8241de86d_8;
    NUITKA_MAY_BE_UNUSED char const *type_description_8 = NULL;
    static struct Nuitka_FrameObject *cache_frame_8c2d2a58a411a4c99aa649f8241de86d_8 = NULL;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *locals_matplotlib$backends$backend_webagg_112 = NULL;
    struct Nuitka_FrameObject *frame_56eaf9c20edf0f83e8ca1007194d7244_9;
    NUITKA_MAY_BE_UNUSED char const *type_description_9 = NULL;
    static struct Nuitka_FrameObject *cache_frame_56eaf9c20edf0f83e8ca1007194d7244_9 = NULL;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *exception_keeper_type_22;
    PyObject *exception_keeper_value_22;
    PyTracebackObject *exception_keeper_tb_22;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_22;
    PyObject *exception_keeper_type_23;
    PyObject *exception_keeper_value_23;
    PyTracebackObject *exception_keeper_tb_23;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_23;
    PyObject *locals_matplotlib$backends$backend_webagg_135 = NULL;
    struct Nuitka_FrameObject *frame_e4252a982fc0f588ca0f884fdde91a3c_10;
    NUITKA_MAY_BE_UNUSED char const *type_description_10 = NULL;
    static struct Nuitka_FrameObject *cache_frame_e4252a982fc0f588ca0f884fdde91a3c_10 = NULL;
    PyObject *exception_keeper_type_24;
    PyObject *exception_keeper_value_24;
    PyTracebackObject *exception_keeper_tb_24;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_24;
    PyObject *exception_keeper_type_25;
    PyObject *exception_keeper_value_25;
    PyTracebackObject *exception_keeper_tb_25;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_25;
    PyObject *exception_keeper_type_26;
    PyObject *exception_keeper_value_26;
    PyTracebackObject *exception_keeper_tb_26;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_26;
    static struct Nuitka_FrameObject *cache_frame_94d52aad8429f3f66731c14b3a262317_4 = NULL;
    PyObject *exception_keeper_type_27;
    PyObject *exception_keeper_value_27;
    PyTracebackObject *exception_keeper_tb_27;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_27;
    PyObject *exception_keeper_type_28;
    PyObject *exception_keeper_value_28;
    PyTracebackObject *exception_keeper_tb_28;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_28;
    PyObject *exception_keeper_type_29;
    PyObject *exception_keeper_value_29;
    PyTracebackObject *exception_keeper_tb_29;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_29;
    PyObject *locals_matplotlib$backends$backend_webagg_313 = NULL;
    struct Nuitka_FrameObject *frame_7511e4691622db519fa9a0190d02ddea_11;
    NUITKA_MAY_BE_UNUSED char const *type_description_11 = NULL;
    static struct Nuitka_FrameObject *cache_frame_7511e4691622db519fa9a0190d02ddea_11 = NULL;
    PyObject *exception_keeper_type_30;
    PyObject *exception_keeper_value_30;
    PyTracebackObject *exception_keeper_tb_30;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_30;
    PyObject *exception_keeper_type_31;
    PyObject *exception_keeper_value_31;
    PyTracebackObject *exception_keeper_tb_31;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_31;
    PyObject *exception_keeper_type_32;
    PyObject *exception_keeper_value_32;
    PyTracebackObject *exception_keeper_tb_32;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_32;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_ba38e0a1f7c48a254518d6d13e2cde5e;
        UPDATE_STRING_DICT0( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_a6bc9fc9cbcc7aad54ab5534e23a973a = MAKE_MODULE_FRAME( codeobj_a6bc9fc9cbcc7aad54ab5534e23a973a, module_matplotlib$backends$backend_webagg );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_a6bc9fc9cbcc7aad54ab5534e23a973a );
    assert( Py_REFCNT( frame_a6bc9fc9cbcc7aad54ab5534e23a973a ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_contextlib;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_contextmanager_tuple;
        tmp_level_name_1 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 14;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_contextmanager );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_contextmanager, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_errno;
        tmp_globals_name_2 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 15;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        assert( !(tmp_assign_source_5 == NULL) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_errno, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_io;
        tmp_globals_name_3 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_BytesIO_tuple;
        tmp_level_name_3 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 16;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_BytesIO );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_BytesIO, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_json;
        tmp_globals_name_4 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 17;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_json, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_pathlib;
        tmp_globals_name_5 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_Path_tuple;
        tmp_level_name_5 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 18;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_Path );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_Path, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_random;
        tmp_globals_name_6 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 19;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_random, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_sys;
        tmp_globals_name_7 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = Py_None;
        tmp_level_name_7 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 20;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        assert( !(tmp_assign_source_10 == NULL) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_plain_signal;
        tmp_globals_name_8 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = Py_None;
        tmp_level_name_8 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 21;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_signal, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_plain_socket;
        tmp_globals_name_9 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = Py_None;
        tmp_level_name_9 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 22;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_socket, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_plain_threading;
        tmp_globals_name_10 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = Py_None;
        tmp_level_name_10 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 23;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_threading, tmp_assign_source_13 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_plain_tornado;
        tmp_globals_name_11 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = Py_None;
        tmp_level_name_11 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 26;
        tmp_assign_source_14 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado, tmp_assign_source_14 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_a6bc9fc9cbcc7aad54ab5534e23a973a, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_a6bc9fc9cbcc7aad54ab5534e23a973a, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_27f3593b1aff11723abf4de571d5884c;
            frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 28;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 28;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );

            goto try_except_handler_2;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 25;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame) frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_2;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
    return MOD_RETURN_VALUE( NULL );
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_digest_53cde7bdda744b7198c29b41fd8f35c8;
        tmp_globals_name_12 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = Py_None;
        tmp_level_name_12 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 30;
        tmp_assign_source_15 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_name_name_13;
        PyObject *tmp_globals_name_13;
        PyObject *tmp_locals_name_13;
        PyObject *tmp_fromlist_name_13;
        PyObject *tmp_level_name_13;
        tmp_name_name_13 = const_str_digest_66ef772b0f62991fd20497da33b24b9b;
        tmp_globals_name_13 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_13 = Py_None;
        tmp_fromlist_name_13 = Py_None;
        tmp_level_name_13 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 31;
        tmp_assign_source_16 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_name_name_14;
        PyObject *tmp_globals_name_14;
        PyObject *tmp_locals_name_14;
        PyObject *tmp_fromlist_name_14;
        PyObject *tmp_level_name_14;
        tmp_name_name_14 = const_str_digest_94d6a065657110387ae0f04e74160a45;
        tmp_globals_name_14 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_14 = Py_None;
        tmp_fromlist_name_14 = Py_None;
        tmp_level_name_14 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 32;
        tmp_assign_source_17 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_15;
        PyObject *tmp_globals_name_15;
        PyObject *tmp_locals_name_15;
        PyObject *tmp_fromlist_name_15;
        PyObject *tmp_level_name_15;
        tmp_name_name_15 = const_str_plain_matplotlib;
        tmp_globals_name_15 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_15 = Py_None;
        tmp_fromlist_name_15 = const_tuple_str_plain_rcParams_tuple;
        tmp_level_name_15 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 34;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_15, tmp_globals_name_15, tmp_locals_name_15, tmp_fromlist_name_15, tmp_level_name_15 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_rcParams );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_rcParams, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_16;
        PyObject *tmp_globals_name_16;
        PyObject *tmp_locals_name_16;
        PyObject *tmp_fromlist_name_16;
        PyObject *tmp_level_name_16;
        tmp_name_name_16 = const_str_digest_35fa36169a0e18ddad7803910d4c3238;
        tmp_globals_name_16 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_16 = Py_None;
        tmp_fromlist_name_16 = const_tuple_str_plain__Backend_tuple;
        tmp_level_name_16 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 35;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_16, tmp_globals_name_16, tmp_locals_name_16, tmp_fromlist_name_16, tmp_level_name_16 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain__Backend );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain__Backend, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_17;
        PyObject *tmp_globals_name_17;
        PyObject *tmp_locals_name_17;
        PyObject *tmp_fromlist_name_17;
        PyObject *tmp_level_name_17;
        tmp_name_name_17 = const_str_digest_79d9bffedced0e4cee681f99603a2fbc;
        tmp_globals_name_17 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_17 = Py_None;
        tmp_fromlist_name_17 = const_tuple_str_plain_Gcf_tuple;
        tmp_level_name_17 = const_int_0;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 36;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_17, tmp_globals_name_17, tmp_locals_name_17, tmp_fromlist_name_17, tmp_level_name_17 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_Gcf );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_Gcf, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_18;
        PyObject *tmp_globals_name_18;
        PyObject *tmp_locals_name_18;
        PyObject *tmp_fromlist_name_18;
        PyObject *tmp_level_name_18;
        tmp_name_name_18 = const_str_empty;
        tmp_globals_name_18 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_18 = Py_None;
        tmp_fromlist_name_18 = const_tuple_str_plain_backend_webagg_core_tuple;
        tmp_level_name_18 = const_int_pos_1;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 37;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_18, tmp_globals_name_18, tmp_locals_name_18, tmp_fromlist_name_18, tmp_level_name_18 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_7 ) )
        {
           tmp_assign_source_21 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_7,
                (PyObject *)moduledict_matplotlib$backends$backend_webagg,
                const_str_plain_backend_webagg_core,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_backend_webagg_core );
        }

        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_19;
        PyObject *tmp_globals_name_19;
        PyObject *tmp_locals_name_19;
        PyObject *tmp_fromlist_name_19;
        PyObject *tmp_level_name_19;
        tmp_name_name_19 = const_str_plain_backend_webagg_core;
        tmp_globals_name_19 = (PyObject *)moduledict_matplotlib$backends$backend_webagg;
        tmp_locals_name_19 = Py_None;
        tmp_fromlist_name_19 = const_tuple_str_plain_TimerTornado_tuple;
        tmp_level_name_19 = const_int_pos_1;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 38;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_19, tmp_globals_name_19, tmp_locals_name_19, tmp_fromlist_name_19, tmp_level_name_19 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_22 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_matplotlib$backends$backend_webagg,
                const_str_plain_TimerTornado,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_TimerTornado );
        }

        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_TimerTornado, tmp_assign_source_22 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_threading );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_threading );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "threading" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;

            goto try_except_handler_3;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Thread );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        tmp_assign_source_23 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_23, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_23;
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_24 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_24;
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_25;
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_26 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_26;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_3;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_2 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_2, const_str_plain___prepare__ );
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_3 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;

                goto try_except_handler_3;
            }
            tmp_tuple_element_2 = const_str_plain_ServerThread;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 41;
            tmp_assign_source_27 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;

                goto try_except_handler_3;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_27;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_4 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_4, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;

                goto try_except_handler_3;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_5;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 41;

                    goto try_except_handler_3;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_5 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_5 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 41;

                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 41;

                    goto try_except_handler_3;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 41;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_3;
            }
            branch_no_4:;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_28;
            tmp_assign_source_28 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_28;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_29;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_matplotlib$backends$backend_webagg_41 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_41, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_plain_ServerThread;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_41, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto try_except_handler_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_86295468592d459d67ea0acb84bbdbc3_2, codeobj_86295468592d459d67ea0acb84bbdbc3, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_86295468592d459d67ea0acb84bbdbc3_2 = cache_frame_86295468592d459d67ea0acb84bbdbc3_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_86295468592d459d67ea0acb84bbdbc3_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_86295468592d459d67ea0acb84bbdbc3_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_1_run(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_41, const_str_plain_run, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_86295468592d459d67ea0acb84bbdbc3_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_86295468592d459d67ea0acb84bbdbc3_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_86295468592d459d67ea0acb84bbdbc3_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_86295468592d459d67ea0acb84bbdbc3_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_86295468592d459d67ea0acb84bbdbc3_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_86295468592d459d67ea0acb84bbdbc3_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_86295468592d459d67ea0acb84bbdbc3_2 == cache_frame_86295468592d459d67ea0acb84bbdbc3_2 )
        {
            Py_DECREF( frame_86295468592d459d67ea0acb84bbdbc3_2 );
        }
        cache_frame_86295468592d459d67ea0acb84bbdbc3_2 = NULL;

        assertFrameObject( frame_86295468592d459d67ea0acb84bbdbc3_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_5;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_2 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;

                goto try_except_handler_5;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_41, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;

                goto try_except_handler_5;
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_ServerThread;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_matplotlib$backends$backend_webagg_41;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 41;
            tmp_assign_source_30 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;

                goto try_except_handler_5;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_30;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_29 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_29 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_41 );
        locals_matplotlib$backends$backend_webagg_41 = NULL;
        goto try_return_handler_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_41 );
        locals_matplotlib$backends$backend_webagg_41 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 41;
        goto try_except_handler_3;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_ServerThread, tmp_assign_source_29 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_ServerThread );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ServerThread );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ServerThread" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;

            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 46;
        tmp_assign_source_31 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_webagg_server_thread, tmp_assign_source_31 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_core );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "core" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 49;

            goto try_except_handler_6;
        }

        tmp_source_name_6 = tmp_mvar_value_5;
        tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_FigureCanvasWebAggCore );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        tmp_assign_source_32 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_32, 0, tmp_tuple_element_5 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_33 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_33;
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_34;
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        tmp_condition_result_9 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_35 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_35;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_6;
        }
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_7 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___prepare__ );
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_8;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_8 = tmp_class_creation_2__metaclass;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___prepare__ );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_6;
            }
            tmp_tuple_element_6 = const_str_plain_FigureCanvasWebAgg;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 49;
            tmp_assign_source_36 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_6;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_36;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_9;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_9 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_6;
            }
            tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_10;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 49;

                    goto try_except_handler_6;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_10 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_10 == NULL) );
                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_10 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 49;

                    goto try_except_handler_6;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_7 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 49;

                    goto try_except_handler_6;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 49;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_6;
            }
            branch_no_8:;
        }
        goto branch_end_7;
        branch_no_7:;
        {
            PyObject *tmp_assign_source_37;
            tmp_assign_source_37 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_37;
        }
        branch_end_7:;
    }
    {
        PyObject *tmp_assign_source_38;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_matplotlib$backends$backend_webagg_49 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_49, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_plain_FigureCanvasWebAgg;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_49, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_8;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_255a10bf58c0f52531761a99c6df03f3_3, codeobj_255a10bf58c0f52531761a99c6df03f3, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_255a10bf58c0f52531761a99c6df03f3_3 = cache_frame_255a10bf58c0f52531761a99c6df03f3_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_255a10bf58c0f52531761a99c6df03f3_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_255a10bf58c0f52531761a99c6df03f3_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_2_show(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_49, const_str_plain_show, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_3_new_timer(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_49, const_str_plain_new_timer, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_255a10bf58c0f52531761a99c6df03f3_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_255a10bf58c0f52531761a99c6df03f3_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_255a10bf58c0f52531761a99c6df03f3_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_255a10bf58c0f52531761a99c6df03f3_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_255a10bf58c0f52531761a99c6df03f3_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_255a10bf58c0f52531761a99c6df03f3_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_255a10bf58c0f52531761a99c6df03f3_3 == cache_frame_255a10bf58c0f52531761a99c6df03f3_3 )
        {
            Py_DECREF( frame_255a10bf58c0f52531761a99c6df03f3_3 );
        }
        cache_frame_255a10bf58c0f52531761a99c6df03f3_3 = NULL;

        assertFrameObject( frame_255a10bf58c0f52531761a99c6df03f3_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_8;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_3 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_8;
            }
            tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_49, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_8;
            }
            branch_no_9:;
        }
        {
            PyObject *tmp_assign_source_39;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_5 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_8 = const_str_plain_FigureCanvasWebAgg;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_8 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
            tmp_tuple_element_8 = locals_matplotlib$backends$backend_webagg_49;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 49;
            tmp_assign_source_39 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_8;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_39;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_38 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_38 );
        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_49 );
        locals_matplotlib$backends$backend_webagg_49 = NULL;
        goto try_return_handler_7;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_49 );
        locals_matplotlib$backends$backend_webagg_49 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 49;
        goto try_except_handler_6;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_FigureCanvasWebAgg, tmp_assign_source_38 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_tuple_element_9;
        PyObject *tmp_source_name_11;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;

            goto try_except_handler_9;
        }

        tmp_source_name_12 = tmp_mvar_value_6;
        tmp_source_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_web );
        if ( tmp_source_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        tmp_tuple_element_9 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_Application );
        Py_DECREF( tmp_source_name_11 );
        if ( tmp_tuple_element_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        tmp_assign_source_40 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_40, 0, tmp_tuple_element_9 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_40;
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_dircall_arg1_3;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_41 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_41;
    }
    {
        PyObject *tmp_assign_source_42;
        tmp_assign_source_42 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_42;
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_15;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        tmp_condition_result_15 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_3 = tmp_class_creation_3__bases;
        tmp_subscript_name_3 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_6:;
        condexpr_end_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_43 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_43;
    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_9;
        }
        branch_no_10:;
    }
    {
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_source_name_13;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_13 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_13, const_str_plain___prepare__ );
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_called_name_6;
            PyObject *tmp_source_name_14;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_14 = tmp_class_creation_3__metaclass;
            tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain___prepare__ );
            if ( tmp_called_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_9;
            }
            tmp_tuple_element_10 = const_str_plain_WebAggApplication;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_10 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 60;
            tmp_assign_source_44 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_9;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_44;
        }
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_15;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_15 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_15, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_9;
            }
            tmp_condition_result_18 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            {
                PyObject *tmp_raise_type_4;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_11;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_16;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_4 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_11 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 60;

                    goto try_except_handler_9;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_11 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_16 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_16 == NULL) );
                tmp_tuple_element_11 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_16 );
                if ( tmp_tuple_element_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 60;

                    goto try_except_handler_9;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_11 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 60;

                    goto try_except_handler_9;
                }
                exception_type = tmp_raise_type_4;
                Py_INCREF( tmp_raise_type_4 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 60;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_9;
            }
            branch_no_12:;
        }
        goto branch_end_11;
        branch_no_11:;
        {
            PyObject *tmp_assign_source_45;
            tmp_assign_source_45 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_45;
        }
        branch_end_11:;
    }
    {
        PyObject *tmp_assign_source_46;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_matplotlib$backends$backend_webagg_60 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_11;
        }
        tmp_dictset_value = const_str_plain_WebAggApplication;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_11;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_94d52aad8429f3f66731c14b3a262317_4, codeobj_94d52aad8429f3f66731c14b3a262317, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_94d52aad8429f3f66731c14b3a262317_4 = cache_frame_94d52aad8429f3f66731c14b3a262317_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_94d52aad8429f3f66731c14b3a262317_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_94d52aad8429f3f66731c14b3a262317_4 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = Py_False;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_initialized, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_2 = "c";
            goto frame_exception_exit_4;
        }
        tmp_dictset_value = Py_False;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_started, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_2 = "c";
            goto frame_exception_exit_4;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_47;
            PyObject *tmp_tuple_element_12;
            PyObject *tmp_source_name_17;
            PyObject *tmp_source_name_18;
            PyObject *tmp_mvar_value_7;
            tmp_source_name_18 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_tornado );

            if ( tmp_source_name_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 64;
                    type_description_2 = "c";
                    goto try_except_handler_12;
                }

                tmp_source_name_18 = tmp_mvar_value_7;
                Py_INCREF( tmp_source_name_18 );
                }
            }

            tmp_source_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_web );
            Py_DECREF( tmp_source_name_18 );
            if ( tmp_source_name_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            tmp_tuple_element_12 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_RequestHandler );
            Py_DECREF( tmp_source_name_17 );
            if ( tmp_tuple_element_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            tmp_assign_source_47 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_assign_source_47, 0, tmp_tuple_element_12 );
            assert( tmp_WebAggApplication$class_creation_1__bases_orig == NULL );
            tmp_WebAggApplication$class_creation_1__bases_orig = tmp_assign_source_47;
        }
        {
            PyObject *tmp_assign_source_48;
            PyObject *tmp_dircall_arg1_4;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__bases_orig );
            tmp_dircall_arg1_4 = tmp_WebAggApplication$class_creation_1__bases_orig;
            Py_INCREF( tmp_dircall_arg1_4 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_4};
                tmp_assign_source_48 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_48 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            assert( tmp_WebAggApplication$class_creation_1__bases == NULL );
            tmp_WebAggApplication$class_creation_1__bases = tmp_assign_source_48;
        }
        {
            PyObject *tmp_assign_source_49;
            tmp_assign_source_49 = PyDict_New();
            assert( tmp_WebAggApplication$class_creation_1__class_decl_dict == NULL );
            tmp_WebAggApplication$class_creation_1__class_decl_dict = tmp_assign_source_49;
        }
        {
            PyObject *tmp_assign_source_50;
            PyObject *tmp_metaclass_name_4;
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_key_name_10;
            PyObject *tmp_dict_name_10;
            PyObject *tmp_dict_name_11;
            PyObject *tmp_key_name_11;
            nuitka_bool tmp_condition_result_20;
            int tmp_truth_name_4;
            PyObject *tmp_type_arg_7;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_subscript_name_4;
            PyObject *tmp_bases_name_4;
            tmp_key_name_10 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__class_decl_dict );
            tmp_dict_name_10 = tmp_WebAggApplication$class_creation_1__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_10, tmp_key_name_10 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_7;
            }
            else
            {
                goto condexpr_false_7;
            }
            condexpr_true_7:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__class_decl_dict );
            tmp_dict_name_11 = tmp_WebAggApplication$class_creation_1__class_decl_dict;
            tmp_key_name_11 = const_str_plain_metaclass;
            tmp_metaclass_name_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
            if ( tmp_metaclass_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            goto condexpr_end_7;
            condexpr_false_7:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__bases );
            tmp_truth_name_4 = CHECK_IF_TRUE( tmp_WebAggApplication$class_creation_1__bases );
            if ( tmp_truth_name_4 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            tmp_condition_result_20 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_8;
            }
            else
            {
                goto condexpr_false_8;
            }
            condexpr_true_8:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__bases );
            tmp_subscribed_name_4 = tmp_WebAggApplication$class_creation_1__bases;
            tmp_subscript_name_4 = const_int_0;
            tmp_type_arg_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
            if ( tmp_type_arg_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            tmp_metaclass_name_4 = BUILTIN_TYPE1( tmp_type_arg_7 );
            Py_DECREF( tmp_type_arg_7 );
            if ( tmp_metaclass_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            goto condexpr_end_8;
            condexpr_false_8:;
            tmp_metaclass_name_4 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_4 );
            condexpr_end_8:;
            condexpr_end_7:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__bases );
            tmp_bases_name_4 = tmp_WebAggApplication$class_creation_1__bases;
            tmp_assign_source_50 = SELECT_METACLASS( tmp_metaclass_name_4, tmp_bases_name_4 );
            Py_DECREF( tmp_metaclass_name_4 );
            if ( tmp_assign_source_50 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            assert( tmp_WebAggApplication$class_creation_1__metaclass == NULL );
            tmp_WebAggApplication$class_creation_1__metaclass = tmp_assign_source_50;
        }
        {
            nuitka_bool tmp_condition_result_21;
            PyObject *tmp_key_name_12;
            PyObject *tmp_dict_name_12;
            tmp_key_name_12 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__class_decl_dict );
            tmp_dict_name_12 = tmp_WebAggApplication$class_creation_1__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_12, tmp_key_name_12 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_13;
            }
            else
            {
                goto branch_no_13;
            }
            branch_yes_13:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__class_decl_dict );
            tmp_dictdel_dict = tmp_WebAggApplication$class_creation_1__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_12;
            }
            branch_no_13:;
        }
        {
            nuitka_bool tmp_condition_result_22;
            PyObject *tmp_source_name_19;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__metaclass );
            tmp_source_name_19 = tmp_WebAggApplication$class_creation_1__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_19, const_str_plain___prepare__ );
            tmp_condition_result_22 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_14;
            }
            else
            {
                goto branch_no_14;
            }
            branch_yes_14:;
            {
                PyObject *tmp_assign_source_51;
                PyObject *tmp_called_name_7;
                PyObject *tmp_source_name_20;
                PyObject *tmp_args_name_6;
                PyObject *tmp_tuple_element_13;
                PyObject *tmp_kw_name_6;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__metaclass );
                tmp_source_name_20 = tmp_WebAggApplication$class_creation_1__metaclass;
                tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain___prepare__ );
                if ( tmp_called_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 64;
                    type_description_2 = "c";
                    goto try_except_handler_12;
                }
                tmp_tuple_element_13 = const_str_plain_FavIcon;
                tmp_args_name_6 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_13 );
                PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_13 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__bases );
                tmp_tuple_element_13 = tmp_WebAggApplication$class_creation_1__bases;
                Py_INCREF( tmp_tuple_element_13 );
                PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_13 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__class_decl_dict );
                tmp_kw_name_6 = tmp_WebAggApplication$class_creation_1__class_decl_dict;
                frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 64;
                tmp_assign_source_51 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_6, tmp_kw_name_6 );
                Py_DECREF( tmp_called_name_7 );
                Py_DECREF( tmp_args_name_6 );
                if ( tmp_assign_source_51 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 64;
                    type_description_2 = "c";
                    goto try_except_handler_12;
                }
                assert( tmp_WebAggApplication$class_creation_1__prepared == NULL );
                tmp_WebAggApplication$class_creation_1__prepared = tmp_assign_source_51;
            }
            {
                nuitka_bool tmp_condition_result_23;
                PyObject *tmp_operand_name_4;
                PyObject *tmp_source_name_21;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__prepared );
                tmp_source_name_21 = tmp_WebAggApplication$class_creation_1__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_21, const_str_plain___getitem__ );
                tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 64;
                    type_description_2 = "c";
                    goto try_except_handler_12;
                }
                tmp_condition_result_23 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_15;
                }
                else
                {
                    goto branch_no_15;
                }
                branch_yes_15:;
                {
                    PyObject *tmp_raise_type_5;
                    PyObject *tmp_raise_value_4;
                    PyObject *tmp_left_name_4;
                    PyObject *tmp_right_name_4;
                    PyObject *tmp_tuple_element_14;
                    PyObject *tmp_getattr_target_4;
                    PyObject *tmp_getattr_attr_4;
                    PyObject *tmp_getattr_default_4;
                    PyObject *tmp_source_name_22;
                    PyObject *tmp_type_arg_8;
                    tmp_raise_type_5 = PyExc_TypeError;
                    tmp_left_name_4 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__metaclass );
                    tmp_getattr_target_4 = tmp_WebAggApplication$class_creation_1__metaclass;
                    tmp_getattr_attr_4 = const_str_plain___name__;
                    tmp_getattr_default_4 = const_str_angle_metaclass;
                    tmp_tuple_element_14 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, tmp_getattr_default_4 );
                    if ( tmp_tuple_element_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 64;
                        type_description_2 = "c";
                        goto try_except_handler_12;
                    }
                    tmp_right_name_4 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_14 );
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__prepared );
                    tmp_type_arg_8 = tmp_WebAggApplication$class_creation_1__prepared;
                    tmp_source_name_22 = BUILTIN_TYPE1( tmp_type_arg_8 );
                    assert( !(tmp_source_name_22 == NULL) );
                    tmp_tuple_element_14 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_22 );
                    if ( tmp_tuple_element_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_4 );

                        exception_lineno = 64;
                        type_description_2 = "c";
                        goto try_except_handler_12;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_14 );
                    tmp_raise_value_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                    Py_DECREF( tmp_right_name_4 );
                    if ( tmp_raise_value_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 64;
                        type_description_2 = "c";
                        goto try_except_handler_12;
                    }
                    exception_type = tmp_raise_type_5;
                    Py_INCREF( tmp_raise_type_5 );
                    exception_value = tmp_raise_value_4;
                    exception_lineno = 64;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "c";
                    goto try_except_handler_12;
                }
                branch_no_15:;
            }
            goto branch_end_14;
            branch_no_14:;
            {
                PyObject *tmp_assign_source_52;
                tmp_assign_source_52 = PyDict_New();
                assert( tmp_WebAggApplication$class_creation_1__prepared == NULL );
                tmp_WebAggApplication$class_creation_1__prepared = tmp_assign_source_52;
            }
            branch_end_14:;
        }
        {
            PyObject *tmp_set_locals_4;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__prepared );
            tmp_set_locals_4 = tmp_WebAggApplication$class_creation_1__prepared;
            locals_matplotlib$backends$backend_webagg_64 = tmp_set_locals_4;
            Py_INCREF( tmp_set_locals_4 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_64, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_2 = "c";
            goto try_except_handler_14;
        }
        tmp_dictset_value = const_str_digest_99c8daaa545708fe4682d180a23b59e4;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_64, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_2 = "c";
            goto try_except_handler_14;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_d5530390e95e557195a38c84aae20396_5, codeobj_d5530390e95e557195a38c84aae20396, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_d5530390e95e557195a38c84aae20396_5 = cache_frame_d5530390e95e557195a38c84aae20396_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_d5530390e95e557195a38c84aae20396_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_d5530390e95e557195a38c84aae20396_5 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_4_get(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_64, const_str_plain_get, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_3 = "o";
            goto frame_exception_exit_5;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d5530390e95e557195a38c84aae20396_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d5530390e95e557195a38c84aae20396_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_d5530390e95e557195a38c84aae20396_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_d5530390e95e557195a38c84aae20396_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_d5530390e95e557195a38c84aae20396_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_d5530390e95e557195a38c84aae20396_5,
            type_description_3,
            outline_3_var___class__
        );


        // Release cached frame.
        if ( frame_d5530390e95e557195a38c84aae20396_5 == cache_frame_d5530390e95e557195a38c84aae20396_5 )
        {
            Py_DECREF( frame_d5530390e95e557195a38c84aae20396_5 );
        }
        cache_frame_d5530390e95e557195a38c84aae20396_5 = NULL;

        assertFrameObject( frame_d5530390e95e557195a38c84aae20396_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_4:;
        type_description_2 = "c";
        goto try_except_handler_14;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_24;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__bases );
            tmp_compexpr_left_4 = tmp_WebAggApplication$class_creation_1__bases;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__bases_orig );
            tmp_compexpr_right_4 = tmp_WebAggApplication$class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_14;
            }
            tmp_condition_result_24 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_16;
            }
            else
            {
                goto branch_no_16;
            }
            branch_yes_16:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__bases_orig );
            tmp_dictset_value = tmp_WebAggApplication$class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_64, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_14;
            }
            branch_no_16:;
        }
        {
            PyObject *tmp_assign_source_53;
            PyObject *tmp_called_name_8;
            PyObject *tmp_args_name_7;
            PyObject *tmp_tuple_element_15;
            PyObject *tmp_kw_name_7;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__metaclass );
            tmp_called_name_8 = tmp_WebAggApplication$class_creation_1__metaclass;
            tmp_tuple_element_15 = const_str_plain_FavIcon;
            tmp_args_name_7 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_15 );
            PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_15 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__bases );
            tmp_tuple_element_15 = tmp_WebAggApplication$class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_15 );
            PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_15 );
            tmp_tuple_element_15 = locals_matplotlib$backends$backend_webagg_64;
            Py_INCREF( tmp_tuple_element_15 );
            PyTuple_SET_ITEM( tmp_args_name_7, 2, tmp_tuple_element_15 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_1__class_decl_dict );
            tmp_kw_name_7 = tmp_WebAggApplication$class_creation_1__class_decl_dict;
            frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 64;
            tmp_assign_source_53 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_7, tmp_kw_name_7 );
            Py_DECREF( tmp_args_name_7 );
            if ( tmp_assign_source_53 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "c";
                goto try_except_handler_14;
            }
            assert( outline_3_var___class__ == NULL );
            outline_3_var___class__ = tmp_assign_source_53;
        }
        CHECK_OBJECT( outline_3_var___class__ );
        tmp_dictset_value = outline_3_var___class__;
        Py_INCREF( tmp_dictset_value );
        goto try_return_handler_14;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_14:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_64 );
        locals_matplotlib$backends$backend_webagg_64 = NULL;
        goto try_return_handler_13;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_64 );
        locals_matplotlib$backends$backend_webagg_64 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_13;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_13:;
        CHECK_OBJECT( (PyObject *)outline_3_var___class__ );
        Py_DECREF( outline_3_var___class__ );
        outline_3_var___class__ = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_4:;
        exception_lineno = 64;
        goto try_except_handler_12;
        outline_result_4:;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_FavIcon, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_2 = "c";
            goto try_except_handler_12;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_WebAggApplication$class_creation_1__bases_orig );
        tmp_WebAggApplication$class_creation_1__bases_orig = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_1__bases );
        tmp_WebAggApplication$class_creation_1__bases = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_1__class_decl_dict );
        tmp_WebAggApplication$class_creation_1__class_decl_dict = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_1__metaclass );
        tmp_WebAggApplication$class_creation_1__metaclass = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_1__prepared );
        tmp_WebAggApplication$class_creation_1__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;
        exception_lineno = exception_keeper_lineno_11;

        goto frame_exception_exit_4;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_1__bases_orig );
        Py_DECREF( tmp_WebAggApplication$class_creation_1__bases_orig );
        tmp_WebAggApplication$class_creation_1__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_1__bases );
        Py_DECREF( tmp_WebAggApplication$class_creation_1__bases );
        tmp_WebAggApplication$class_creation_1__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_1__class_decl_dict );
        Py_DECREF( tmp_WebAggApplication$class_creation_1__class_decl_dict );
        tmp_WebAggApplication$class_creation_1__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_1__metaclass );
        Py_DECREF( tmp_WebAggApplication$class_creation_1__metaclass );
        tmp_WebAggApplication$class_creation_1__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_1__prepared );
        Py_DECREF( tmp_WebAggApplication$class_creation_1__prepared );
        tmp_WebAggApplication$class_creation_1__prepared = NULL;

        // Tried code:
        {
            PyObject *tmp_assign_source_54;
            PyObject *tmp_tuple_element_16;
            PyObject *tmp_source_name_23;
            PyObject *tmp_source_name_24;
            PyObject *tmp_mvar_value_8;
            tmp_source_name_24 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_tornado );

            if ( tmp_source_name_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 70;
                    type_description_2 = "c";
                    goto try_except_handler_15;
                }

                tmp_source_name_24 = tmp_mvar_value_8;
                Py_INCREF( tmp_source_name_24 );
                }
            }

            tmp_source_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_web );
            Py_DECREF( tmp_source_name_24 );
            if ( tmp_source_name_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            tmp_tuple_element_16 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_RequestHandler );
            Py_DECREF( tmp_source_name_23 );
            if ( tmp_tuple_element_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            tmp_assign_source_54 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_assign_source_54, 0, tmp_tuple_element_16 );
            assert( tmp_WebAggApplication$class_creation_2__bases_orig == NULL );
            tmp_WebAggApplication$class_creation_2__bases_orig = tmp_assign_source_54;
        }
        {
            PyObject *tmp_assign_source_55;
            PyObject *tmp_dircall_arg1_5;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__bases_orig );
            tmp_dircall_arg1_5 = tmp_WebAggApplication$class_creation_2__bases_orig;
            Py_INCREF( tmp_dircall_arg1_5 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_5};
                tmp_assign_source_55 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_55 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            assert( tmp_WebAggApplication$class_creation_2__bases == NULL );
            tmp_WebAggApplication$class_creation_2__bases = tmp_assign_source_55;
        }
        {
            PyObject *tmp_assign_source_56;
            tmp_assign_source_56 = PyDict_New();
            assert( tmp_WebAggApplication$class_creation_2__class_decl_dict == NULL );
            tmp_WebAggApplication$class_creation_2__class_decl_dict = tmp_assign_source_56;
        }
        {
            PyObject *tmp_assign_source_57;
            PyObject *tmp_metaclass_name_5;
            nuitka_bool tmp_condition_result_25;
            PyObject *tmp_key_name_13;
            PyObject *tmp_dict_name_13;
            PyObject *tmp_dict_name_14;
            PyObject *tmp_key_name_14;
            nuitka_bool tmp_condition_result_26;
            int tmp_truth_name_5;
            PyObject *tmp_type_arg_9;
            PyObject *tmp_subscribed_name_5;
            PyObject *tmp_subscript_name_5;
            PyObject *tmp_bases_name_5;
            tmp_key_name_13 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__class_decl_dict );
            tmp_dict_name_13 = tmp_WebAggApplication$class_creation_2__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_13, tmp_key_name_13 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_9;
            }
            else
            {
                goto condexpr_false_9;
            }
            condexpr_true_9:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__class_decl_dict );
            tmp_dict_name_14 = tmp_WebAggApplication$class_creation_2__class_decl_dict;
            tmp_key_name_14 = const_str_plain_metaclass;
            tmp_metaclass_name_5 = DICT_GET_ITEM( tmp_dict_name_14, tmp_key_name_14 );
            if ( tmp_metaclass_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            goto condexpr_end_9;
            condexpr_false_9:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__bases );
            tmp_truth_name_5 = CHECK_IF_TRUE( tmp_WebAggApplication$class_creation_2__bases );
            if ( tmp_truth_name_5 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            tmp_condition_result_26 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_10;
            }
            else
            {
                goto condexpr_false_10;
            }
            condexpr_true_10:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__bases );
            tmp_subscribed_name_5 = tmp_WebAggApplication$class_creation_2__bases;
            tmp_subscript_name_5 = const_int_0;
            tmp_type_arg_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 0 );
            if ( tmp_type_arg_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            tmp_metaclass_name_5 = BUILTIN_TYPE1( tmp_type_arg_9 );
            Py_DECREF( tmp_type_arg_9 );
            if ( tmp_metaclass_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            goto condexpr_end_10;
            condexpr_false_10:;
            tmp_metaclass_name_5 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_5 );
            condexpr_end_10:;
            condexpr_end_9:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__bases );
            tmp_bases_name_5 = tmp_WebAggApplication$class_creation_2__bases;
            tmp_assign_source_57 = SELECT_METACLASS( tmp_metaclass_name_5, tmp_bases_name_5 );
            Py_DECREF( tmp_metaclass_name_5 );
            if ( tmp_assign_source_57 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            assert( tmp_WebAggApplication$class_creation_2__metaclass == NULL );
            tmp_WebAggApplication$class_creation_2__metaclass = tmp_assign_source_57;
        }
        {
            nuitka_bool tmp_condition_result_27;
            PyObject *tmp_key_name_15;
            PyObject *tmp_dict_name_15;
            tmp_key_name_15 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__class_decl_dict );
            tmp_dict_name_15 = tmp_WebAggApplication$class_creation_2__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_15, tmp_key_name_15 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            tmp_condition_result_27 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_17;
            }
            else
            {
                goto branch_no_17;
            }
            branch_yes_17:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__class_decl_dict );
            tmp_dictdel_dict = tmp_WebAggApplication$class_creation_2__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_15;
            }
            branch_no_17:;
        }
        {
            nuitka_bool tmp_condition_result_28;
            PyObject *tmp_source_name_25;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__metaclass );
            tmp_source_name_25 = tmp_WebAggApplication$class_creation_2__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_25, const_str_plain___prepare__ );
            tmp_condition_result_28 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_28 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_18;
            }
            else
            {
                goto branch_no_18;
            }
            branch_yes_18:;
            {
                PyObject *tmp_assign_source_58;
                PyObject *tmp_called_name_9;
                PyObject *tmp_source_name_26;
                PyObject *tmp_args_name_8;
                PyObject *tmp_tuple_element_17;
                PyObject *tmp_kw_name_8;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__metaclass );
                tmp_source_name_26 = tmp_WebAggApplication$class_creation_2__metaclass;
                tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain___prepare__ );
                if ( tmp_called_name_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 70;
                    type_description_2 = "c";
                    goto try_except_handler_15;
                }
                tmp_tuple_element_17 = const_str_plain_SingleFigurePage;
                tmp_args_name_8 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_17 );
                PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_17 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__bases );
                tmp_tuple_element_17 = tmp_WebAggApplication$class_creation_2__bases;
                Py_INCREF( tmp_tuple_element_17 );
                PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_17 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__class_decl_dict );
                tmp_kw_name_8 = tmp_WebAggApplication$class_creation_2__class_decl_dict;
                frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 70;
                tmp_assign_source_58 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_8, tmp_kw_name_8 );
                Py_DECREF( tmp_called_name_9 );
                Py_DECREF( tmp_args_name_8 );
                if ( tmp_assign_source_58 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 70;
                    type_description_2 = "c";
                    goto try_except_handler_15;
                }
                assert( tmp_WebAggApplication$class_creation_2__prepared == NULL );
                tmp_WebAggApplication$class_creation_2__prepared = tmp_assign_source_58;
            }
            {
                nuitka_bool tmp_condition_result_29;
                PyObject *tmp_operand_name_5;
                PyObject *tmp_source_name_27;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__prepared );
                tmp_source_name_27 = tmp_WebAggApplication$class_creation_2__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_27, const_str_plain___getitem__ );
                tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 70;
                    type_description_2 = "c";
                    goto try_except_handler_15;
                }
                tmp_condition_result_29 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_29 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_19;
                }
                else
                {
                    goto branch_no_19;
                }
                branch_yes_19:;
                {
                    PyObject *tmp_raise_type_6;
                    PyObject *tmp_raise_value_5;
                    PyObject *tmp_left_name_5;
                    PyObject *tmp_right_name_5;
                    PyObject *tmp_tuple_element_18;
                    PyObject *tmp_getattr_target_5;
                    PyObject *tmp_getattr_attr_5;
                    PyObject *tmp_getattr_default_5;
                    PyObject *tmp_source_name_28;
                    PyObject *tmp_type_arg_10;
                    tmp_raise_type_6 = PyExc_TypeError;
                    tmp_left_name_5 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__metaclass );
                    tmp_getattr_target_5 = tmp_WebAggApplication$class_creation_2__metaclass;
                    tmp_getattr_attr_5 = const_str_plain___name__;
                    tmp_getattr_default_5 = const_str_angle_metaclass;
                    tmp_tuple_element_18 = BUILTIN_GETATTR( tmp_getattr_target_5, tmp_getattr_attr_5, tmp_getattr_default_5 );
                    if ( tmp_tuple_element_18 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 70;
                        type_description_2 = "c";
                        goto try_except_handler_15;
                    }
                    tmp_right_name_5 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_5, 0, tmp_tuple_element_18 );
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__prepared );
                    tmp_type_arg_10 = tmp_WebAggApplication$class_creation_2__prepared;
                    tmp_source_name_28 = BUILTIN_TYPE1( tmp_type_arg_10 );
                    assert( !(tmp_source_name_28 == NULL) );
                    tmp_tuple_element_18 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_28 );
                    if ( tmp_tuple_element_18 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_5 );

                        exception_lineno = 70;
                        type_description_2 = "c";
                        goto try_except_handler_15;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_5, 1, tmp_tuple_element_18 );
                    tmp_raise_value_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                    Py_DECREF( tmp_right_name_5 );
                    if ( tmp_raise_value_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 70;
                        type_description_2 = "c";
                        goto try_except_handler_15;
                    }
                    exception_type = tmp_raise_type_6;
                    Py_INCREF( tmp_raise_type_6 );
                    exception_value = tmp_raise_value_5;
                    exception_lineno = 70;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "c";
                    goto try_except_handler_15;
                }
                branch_no_19:;
            }
            goto branch_end_18;
            branch_no_18:;
            {
                PyObject *tmp_assign_source_59;
                tmp_assign_source_59 = PyDict_New();
                assert( tmp_WebAggApplication$class_creation_2__prepared == NULL );
                tmp_WebAggApplication$class_creation_2__prepared = tmp_assign_source_59;
            }
            branch_end_18:;
        }
        {
            PyObject *tmp_set_locals_5;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__prepared );
            tmp_set_locals_5 = tmp_WebAggApplication$class_creation_2__prepared;
            locals_matplotlib$backends$backend_webagg_70 = tmp_set_locals_5;
            Py_INCREF( tmp_set_locals_5 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_70, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_2 = "c";
            goto try_except_handler_17;
        }
        tmp_dictset_value = const_str_digest_84d8f162a9332acb860ab19d798a389d;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_70, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_2 = "c";
            goto try_except_handler_17;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_ac4a2b74daab0cd32f52a8f231717a01_6, codeobj_ac4a2b74daab0cd32f52a8f231717a01, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_ac4a2b74daab0cd32f52a8f231717a01_6 = cache_frame_ac4a2b74daab0cd32f52a8f231717a01_6;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_ac4a2b74daab0cd32f52a8f231717a01_6 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_ac4a2b74daab0cd32f52a8f231717a01_6 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_kw_defaults_1;
            tmp_kw_defaults_1 = PyDict_Copy( const_dict_59adcb2296c213fb0eb9f1796274d900 );
            tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_5___init__( tmp_kw_defaults_1 );

            ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = outline_4_var___class__;
            Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );


            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_70, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_3 = "c";
                goto frame_exception_exit_6;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_6_get(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_70, const_str_plain_get, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_3 = "c";
            goto frame_exception_exit_6;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ac4a2b74daab0cd32f52a8f231717a01_6 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_exception_exit_6:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ac4a2b74daab0cd32f52a8f231717a01_6 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_ac4a2b74daab0cd32f52a8f231717a01_6, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_ac4a2b74daab0cd32f52a8f231717a01_6->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_ac4a2b74daab0cd32f52a8f231717a01_6, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_ac4a2b74daab0cd32f52a8f231717a01_6,
            type_description_3,
            outline_4_var___class__
        );


        // Release cached frame.
        if ( frame_ac4a2b74daab0cd32f52a8f231717a01_6 == cache_frame_ac4a2b74daab0cd32f52a8f231717a01_6 )
        {
            Py_DECREF( frame_ac4a2b74daab0cd32f52a8f231717a01_6 );
        }
        cache_frame_ac4a2b74daab0cd32f52a8f231717a01_6 = NULL;

        assertFrameObject( frame_ac4a2b74daab0cd32f52a8f231717a01_6 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_5;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_5:;
        type_description_2 = "c";
        goto try_except_handler_17;
        skip_nested_handling_4:;
        {
            nuitka_bool tmp_condition_result_30;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__bases );
            tmp_compexpr_left_5 = tmp_WebAggApplication$class_creation_2__bases;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__bases_orig );
            tmp_compexpr_right_5 = tmp_WebAggApplication$class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_17;
            }
            tmp_condition_result_30 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_30 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__bases_orig );
            tmp_dictset_value = tmp_WebAggApplication$class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_70, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_17;
            }
            branch_no_20:;
        }
        {
            PyObject *tmp_assign_source_60;
            PyObject *tmp_called_name_10;
            PyObject *tmp_args_name_9;
            PyObject *tmp_tuple_element_19;
            PyObject *tmp_kw_name_9;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__metaclass );
            tmp_called_name_10 = tmp_WebAggApplication$class_creation_2__metaclass;
            tmp_tuple_element_19 = const_str_plain_SingleFigurePage;
            tmp_args_name_9 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_19 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__bases );
            tmp_tuple_element_19 = tmp_WebAggApplication$class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_args_name_9, 1, tmp_tuple_element_19 );
            tmp_tuple_element_19 = locals_matplotlib$backends$backend_webagg_70;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_args_name_9, 2, tmp_tuple_element_19 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_2__class_decl_dict );
            tmp_kw_name_9 = tmp_WebAggApplication$class_creation_2__class_decl_dict;
            frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 70;
            tmp_assign_source_60 = CALL_FUNCTION( tmp_called_name_10, tmp_args_name_9, tmp_kw_name_9 );
            Py_DECREF( tmp_args_name_9 );
            if ( tmp_assign_source_60 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "c";
                goto try_except_handler_17;
            }
            {
                PyObject *old = PyCell_GET( outline_4_var___class__ );
                PyCell_SET( outline_4_var___class__, tmp_assign_source_60 );
                Py_XDECREF( old );
            }

        }
        CHECK_OBJECT( PyCell_GET( outline_4_var___class__ ) );
        tmp_dictset_value = PyCell_GET( outline_4_var___class__ );
        Py_INCREF( tmp_dictset_value );
        goto try_return_handler_17;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_17:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_70 );
        locals_matplotlib$backends$backend_webagg_70 = NULL;
        goto try_return_handler_16;
        // Exception handler code:
        try_except_handler_17:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_70 );
        locals_matplotlib$backends$backend_webagg_70 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto try_except_handler_16;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_16:;
        CHECK_OBJECT( (PyObject *)outline_4_var___class__ );
        Py_DECREF( outline_4_var___class__ );
        outline_4_var___class__ = NULL;

        goto outline_result_5;
        // Exception handler code:
        try_except_handler_16:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)outline_4_var___class__ );
        Py_DECREF( outline_4_var___class__ );
        outline_4_var___class__ = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto outline_exception_5;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_5:;
        exception_lineno = 70;
        goto try_except_handler_15;
        outline_result_5:;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_SingleFigurePage, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_2 = "c";
            goto try_except_handler_15;
        }
        goto try_end_5;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_WebAggApplication$class_creation_2__bases_orig );
        tmp_WebAggApplication$class_creation_2__bases_orig = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_2__bases );
        tmp_WebAggApplication$class_creation_2__bases = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_2__class_decl_dict );
        tmp_WebAggApplication$class_creation_2__class_decl_dict = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_2__metaclass );
        tmp_WebAggApplication$class_creation_2__metaclass = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_2__prepared );
        tmp_WebAggApplication$class_creation_2__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto frame_exception_exit_4;
        // End of try:
        try_end_5:;
        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_2__bases_orig );
        Py_DECREF( tmp_WebAggApplication$class_creation_2__bases_orig );
        tmp_WebAggApplication$class_creation_2__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_2__bases );
        Py_DECREF( tmp_WebAggApplication$class_creation_2__bases );
        tmp_WebAggApplication$class_creation_2__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_2__class_decl_dict );
        Py_DECREF( tmp_WebAggApplication$class_creation_2__class_decl_dict );
        tmp_WebAggApplication$class_creation_2__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_2__metaclass );
        Py_DECREF( tmp_WebAggApplication$class_creation_2__metaclass );
        tmp_WebAggApplication$class_creation_2__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_2__prepared );
        Py_DECREF( tmp_WebAggApplication$class_creation_2__prepared );
        tmp_WebAggApplication$class_creation_2__prepared = NULL;

        // Tried code:
        {
            PyObject *tmp_assign_source_61;
            PyObject *tmp_tuple_element_20;
            PyObject *tmp_source_name_29;
            PyObject *tmp_source_name_30;
            PyObject *tmp_mvar_value_9;
            tmp_source_name_30 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_tornado );

            if ( tmp_source_name_30 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
                }

                if ( tmp_mvar_value_9 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 89;
                    type_description_2 = "c";
                    goto try_except_handler_18;
                }

                tmp_source_name_30 = tmp_mvar_value_9;
                Py_INCREF( tmp_source_name_30 );
                }
            }

            tmp_source_name_29 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_web );
            Py_DECREF( tmp_source_name_30 );
            if ( tmp_source_name_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            tmp_tuple_element_20 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_RequestHandler );
            Py_DECREF( tmp_source_name_29 );
            if ( tmp_tuple_element_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            tmp_assign_source_61 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_assign_source_61, 0, tmp_tuple_element_20 );
            assert( tmp_WebAggApplication$class_creation_3__bases_orig == NULL );
            tmp_WebAggApplication$class_creation_3__bases_orig = tmp_assign_source_61;
        }
        {
            PyObject *tmp_assign_source_62;
            PyObject *tmp_dircall_arg1_6;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__bases_orig );
            tmp_dircall_arg1_6 = tmp_WebAggApplication$class_creation_3__bases_orig;
            Py_INCREF( tmp_dircall_arg1_6 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_6};
                tmp_assign_source_62 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_62 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            assert( tmp_WebAggApplication$class_creation_3__bases == NULL );
            tmp_WebAggApplication$class_creation_3__bases = tmp_assign_source_62;
        }
        {
            PyObject *tmp_assign_source_63;
            tmp_assign_source_63 = PyDict_New();
            assert( tmp_WebAggApplication$class_creation_3__class_decl_dict == NULL );
            tmp_WebAggApplication$class_creation_3__class_decl_dict = tmp_assign_source_63;
        }
        {
            PyObject *tmp_assign_source_64;
            PyObject *tmp_metaclass_name_6;
            nuitka_bool tmp_condition_result_31;
            PyObject *tmp_key_name_16;
            PyObject *tmp_dict_name_16;
            PyObject *tmp_dict_name_17;
            PyObject *tmp_key_name_17;
            nuitka_bool tmp_condition_result_32;
            int tmp_truth_name_6;
            PyObject *tmp_type_arg_11;
            PyObject *tmp_subscribed_name_6;
            PyObject *tmp_subscript_name_6;
            PyObject *tmp_bases_name_6;
            tmp_key_name_16 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__class_decl_dict );
            tmp_dict_name_16 = tmp_WebAggApplication$class_creation_3__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_16, tmp_key_name_16 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            tmp_condition_result_31 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_31 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_11;
            }
            else
            {
                goto condexpr_false_11;
            }
            condexpr_true_11:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__class_decl_dict );
            tmp_dict_name_17 = tmp_WebAggApplication$class_creation_3__class_decl_dict;
            tmp_key_name_17 = const_str_plain_metaclass;
            tmp_metaclass_name_6 = DICT_GET_ITEM( tmp_dict_name_17, tmp_key_name_17 );
            if ( tmp_metaclass_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            goto condexpr_end_11;
            condexpr_false_11:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__bases );
            tmp_truth_name_6 = CHECK_IF_TRUE( tmp_WebAggApplication$class_creation_3__bases );
            if ( tmp_truth_name_6 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            tmp_condition_result_32 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_32 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_12;
            }
            else
            {
                goto condexpr_false_12;
            }
            condexpr_true_12:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__bases );
            tmp_subscribed_name_6 = tmp_WebAggApplication$class_creation_3__bases;
            tmp_subscript_name_6 = const_int_0;
            tmp_type_arg_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
            if ( tmp_type_arg_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            tmp_metaclass_name_6 = BUILTIN_TYPE1( tmp_type_arg_11 );
            Py_DECREF( tmp_type_arg_11 );
            if ( tmp_metaclass_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            goto condexpr_end_12;
            condexpr_false_12:;
            tmp_metaclass_name_6 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_6 );
            condexpr_end_12:;
            condexpr_end_11:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__bases );
            tmp_bases_name_6 = tmp_WebAggApplication$class_creation_3__bases;
            tmp_assign_source_64 = SELECT_METACLASS( tmp_metaclass_name_6, tmp_bases_name_6 );
            Py_DECREF( tmp_metaclass_name_6 );
            if ( tmp_assign_source_64 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            assert( tmp_WebAggApplication$class_creation_3__metaclass == NULL );
            tmp_WebAggApplication$class_creation_3__metaclass = tmp_assign_source_64;
        }
        {
            nuitka_bool tmp_condition_result_33;
            PyObject *tmp_key_name_18;
            PyObject *tmp_dict_name_18;
            tmp_key_name_18 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__class_decl_dict );
            tmp_dict_name_18 = tmp_WebAggApplication$class_creation_3__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_18, tmp_key_name_18 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            tmp_condition_result_33 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_33 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_21;
            }
            else
            {
                goto branch_no_21;
            }
            branch_yes_21:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__class_decl_dict );
            tmp_dictdel_dict = tmp_WebAggApplication$class_creation_3__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_18;
            }
            branch_no_21:;
        }
        {
            nuitka_bool tmp_condition_result_34;
            PyObject *tmp_source_name_31;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__metaclass );
            tmp_source_name_31 = tmp_WebAggApplication$class_creation_3__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_31, const_str_plain___prepare__ );
            tmp_condition_result_34 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_34 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_22;
            }
            else
            {
                goto branch_no_22;
            }
            branch_yes_22:;
            {
                PyObject *tmp_assign_source_65;
                PyObject *tmp_called_name_11;
                PyObject *tmp_source_name_32;
                PyObject *tmp_args_name_10;
                PyObject *tmp_tuple_element_21;
                PyObject *tmp_kw_name_10;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__metaclass );
                tmp_source_name_32 = tmp_WebAggApplication$class_creation_3__metaclass;
                tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain___prepare__ );
                if ( tmp_called_name_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 89;
                    type_description_2 = "c";
                    goto try_except_handler_18;
                }
                tmp_tuple_element_21 = const_str_plain_AllFiguresPage;
                tmp_args_name_10 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_21 );
                PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_21 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__bases );
                tmp_tuple_element_21 = tmp_WebAggApplication$class_creation_3__bases;
                Py_INCREF( tmp_tuple_element_21 );
                PyTuple_SET_ITEM( tmp_args_name_10, 1, tmp_tuple_element_21 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__class_decl_dict );
                tmp_kw_name_10 = tmp_WebAggApplication$class_creation_3__class_decl_dict;
                frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 89;
                tmp_assign_source_65 = CALL_FUNCTION( tmp_called_name_11, tmp_args_name_10, tmp_kw_name_10 );
                Py_DECREF( tmp_called_name_11 );
                Py_DECREF( tmp_args_name_10 );
                if ( tmp_assign_source_65 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 89;
                    type_description_2 = "c";
                    goto try_except_handler_18;
                }
                assert( tmp_WebAggApplication$class_creation_3__prepared == NULL );
                tmp_WebAggApplication$class_creation_3__prepared = tmp_assign_source_65;
            }
            {
                nuitka_bool tmp_condition_result_35;
                PyObject *tmp_operand_name_6;
                PyObject *tmp_source_name_33;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__prepared );
                tmp_source_name_33 = tmp_WebAggApplication$class_creation_3__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_33, const_str_plain___getitem__ );
                tmp_operand_name_6 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_6 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 89;
                    type_description_2 = "c";
                    goto try_except_handler_18;
                }
                tmp_condition_result_35 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_35 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_23;
                }
                else
                {
                    goto branch_no_23;
                }
                branch_yes_23:;
                {
                    PyObject *tmp_raise_type_7;
                    PyObject *tmp_raise_value_6;
                    PyObject *tmp_left_name_6;
                    PyObject *tmp_right_name_6;
                    PyObject *tmp_tuple_element_22;
                    PyObject *tmp_getattr_target_6;
                    PyObject *tmp_getattr_attr_6;
                    PyObject *tmp_getattr_default_6;
                    PyObject *tmp_source_name_34;
                    PyObject *tmp_type_arg_12;
                    tmp_raise_type_7 = PyExc_TypeError;
                    tmp_left_name_6 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__metaclass );
                    tmp_getattr_target_6 = tmp_WebAggApplication$class_creation_3__metaclass;
                    tmp_getattr_attr_6 = const_str_plain___name__;
                    tmp_getattr_default_6 = const_str_angle_metaclass;
                    tmp_tuple_element_22 = BUILTIN_GETATTR( tmp_getattr_target_6, tmp_getattr_attr_6, tmp_getattr_default_6 );
                    if ( tmp_tuple_element_22 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 89;
                        type_description_2 = "c";
                        goto try_except_handler_18;
                    }
                    tmp_right_name_6 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_6, 0, tmp_tuple_element_22 );
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__prepared );
                    tmp_type_arg_12 = tmp_WebAggApplication$class_creation_3__prepared;
                    tmp_source_name_34 = BUILTIN_TYPE1( tmp_type_arg_12 );
                    assert( !(tmp_source_name_34 == NULL) );
                    tmp_tuple_element_22 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_34 );
                    if ( tmp_tuple_element_22 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_6 );

                        exception_lineno = 89;
                        type_description_2 = "c";
                        goto try_except_handler_18;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_6, 1, tmp_tuple_element_22 );
                    tmp_raise_value_6 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
                    Py_DECREF( tmp_right_name_6 );
                    if ( tmp_raise_value_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 89;
                        type_description_2 = "c";
                        goto try_except_handler_18;
                    }
                    exception_type = tmp_raise_type_7;
                    Py_INCREF( tmp_raise_type_7 );
                    exception_value = tmp_raise_value_6;
                    exception_lineno = 89;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "c";
                    goto try_except_handler_18;
                }
                branch_no_23:;
            }
            goto branch_end_22;
            branch_no_22:;
            {
                PyObject *tmp_assign_source_66;
                tmp_assign_source_66 = PyDict_New();
                assert( tmp_WebAggApplication$class_creation_3__prepared == NULL );
                tmp_WebAggApplication$class_creation_3__prepared = tmp_assign_source_66;
            }
            branch_end_22:;
        }
        {
            PyObject *tmp_set_locals_6;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__prepared );
            tmp_set_locals_6 = tmp_WebAggApplication$class_creation_3__prepared;
            locals_matplotlib$backends$backend_webagg_89 = tmp_set_locals_6;
            Py_INCREF( tmp_set_locals_6 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_89, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_2 = "c";
            goto try_except_handler_20;
        }
        tmp_dictset_value = const_str_digest_2d86bc60c406ff06b8e8e7c50993855e;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_89, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_2 = "c";
            goto try_except_handler_20;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7, codeobj_2ff8ab1c8bc2b4722af7cb6304c8f9cb, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 = cache_frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_kw_defaults_2;
            tmp_kw_defaults_2 = PyDict_Copy( const_dict_59adcb2296c213fb0eb9f1796274d900 );
            tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_7___init__( tmp_kw_defaults_2 );

            ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = outline_5_var___class__;
            Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );


            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_89, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_3 = "c";
                goto frame_exception_exit_7;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_8_get(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_89, const_str_plain_get, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_3 = "c";
            goto frame_exception_exit_7;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_5;

        frame_exception_exit_7:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7,
            type_description_3,
            outline_5_var___class__
        );


        // Release cached frame.
        if ( frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 == cache_frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 )
        {
            Py_DECREF( frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 );
        }
        cache_frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 = NULL;

        assertFrameObject( frame_2ff8ab1c8bc2b4722af7cb6304c8f9cb_7 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_6;

        frame_no_exception_5:;
        goto skip_nested_handling_5;
        nested_frame_exit_6:;
        type_description_2 = "c";
        goto try_except_handler_20;
        skip_nested_handling_5:;
        {
            nuitka_bool tmp_condition_result_36;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__bases );
            tmp_compexpr_left_6 = tmp_WebAggApplication$class_creation_3__bases;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__bases_orig );
            tmp_compexpr_right_6 = tmp_WebAggApplication$class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_20;
            }
            tmp_condition_result_36 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_36 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_24;
            }
            else
            {
                goto branch_no_24;
            }
            branch_yes_24:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__bases_orig );
            tmp_dictset_value = tmp_WebAggApplication$class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_89, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_20;
            }
            branch_no_24:;
        }
        {
            PyObject *tmp_assign_source_67;
            PyObject *tmp_called_name_12;
            PyObject *tmp_args_name_11;
            PyObject *tmp_tuple_element_23;
            PyObject *tmp_kw_name_11;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__metaclass );
            tmp_called_name_12 = tmp_WebAggApplication$class_creation_3__metaclass;
            tmp_tuple_element_23 = const_str_plain_AllFiguresPage;
            tmp_args_name_11 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_11, 0, tmp_tuple_element_23 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__bases );
            tmp_tuple_element_23 = tmp_WebAggApplication$class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_11, 1, tmp_tuple_element_23 );
            tmp_tuple_element_23 = locals_matplotlib$backends$backend_webagg_89;
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_11, 2, tmp_tuple_element_23 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_3__class_decl_dict );
            tmp_kw_name_11 = tmp_WebAggApplication$class_creation_3__class_decl_dict;
            frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 89;
            tmp_assign_source_67 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_11, tmp_kw_name_11 );
            Py_DECREF( tmp_args_name_11 );
            if ( tmp_assign_source_67 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "c";
                goto try_except_handler_20;
            }
            {
                PyObject *old = PyCell_GET( outline_5_var___class__ );
                PyCell_SET( outline_5_var___class__, tmp_assign_source_67 );
                Py_XDECREF( old );
            }

        }
        CHECK_OBJECT( PyCell_GET( outline_5_var___class__ ) );
        tmp_dictset_value = PyCell_GET( outline_5_var___class__ );
        Py_INCREF( tmp_dictset_value );
        goto try_return_handler_20;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_20:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_89 );
        locals_matplotlib$backends$backend_webagg_89 = NULL;
        goto try_return_handler_19;
        // Exception handler code:
        try_except_handler_20:;
        exception_keeper_type_15 = exception_type;
        exception_keeper_value_15 = exception_value;
        exception_keeper_tb_15 = exception_tb;
        exception_keeper_lineno_15 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_89 );
        locals_matplotlib$backends$backend_webagg_89 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_15;
        exception_value = exception_keeper_value_15;
        exception_tb = exception_keeper_tb_15;
        exception_lineno = exception_keeper_lineno_15;

        goto try_except_handler_19;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_19:;
        CHECK_OBJECT( (PyObject *)outline_5_var___class__ );
        Py_DECREF( outline_5_var___class__ );
        outline_5_var___class__ = NULL;

        goto outline_result_6;
        // Exception handler code:
        try_except_handler_19:;
        exception_keeper_type_16 = exception_type;
        exception_keeper_value_16 = exception_value;
        exception_keeper_tb_16 = exception_tb;
        exception_keeper_lineno_16 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)outline_5_var___class__ );
        Py_DECREF( outline_5_var___class__ );
        outline_5_var___class__ = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_16;
        exception_value = exception_keeper_value_16;
        exception_tb = exception_keeper_tb_16;
        exception_lineno = exception_keeper_lineno_16;

        goto outline_exception_6;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_6:;
        exception_lineno = 89;
        goto try_except_handler_18;
        outline_result_6:;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_AllFiguresPage, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_2 = "c";
            goto try_except_handler_18;
        }
        goto try_end_6;
        // Exception handler code:
        try_except_handler_18:;
        exception_keeper_type_17 = exception_type;
        exception_keeper_value_17 = exception_value;
        exception_keeper_tb_17 = exception_tb;
        exception_keeper_lineno_17 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_WebAggApplication$class_creation_3__bases_orig );
        tmp_WebAggApplication$class_creation_3__bases_orig = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_3__bases );
        tmp_WebAggApplication$class_creation_3__bases = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_3__class_decl_dict );
        tmp_WebAggApplication$class_creation_3__class_decl_dict = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_3__metaclass );
        tmp_WebAggApplication$class_creation_3__metaclass = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_3__prepared );
        tmp_WebAggApplication$class_creation_3__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_17;
        exception_value = exception_keeper_value_17;
        exception_tb = exception_keeper_tb_17;
        exception_lineno = exception_keeper_lineno_17;

        goto frame_exception_exit_4;
        // End of try:
        try_end_6:;
        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_3__bases_orig );
        Py_DECREF( tmp_WebAggApplication$class_creation_3__bases_orig );
        tmp_WebAggApplication$class_creation_3__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_3__bases );
        Py_DECREF( tmp_WebAggApplication$class_creation_3__bases );
        tmp_WebAggApplication$class_creation_3__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_3__class_decl_dict );
        Py_DECREF( tmp_WebAggApplication$class_creation_3__class_decl_dict );
        tmp_WebAggApplication$class_creation_3__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_3__metaclass );
        Py_DECREF( tmp_WebAggApplication$class_creation_3__metaclass );
        tmp_WebAggApplication$class_creation_3__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_3__prepared );
        Py_DECREF( tmp_WebAggApplication$class_creation_3__prepared );
        tmp_WebAggApplication$class_creation_3__prepared = NULL;

        // Tried code:
        {
            PyObject *tmp_assign_source_68;
            PyObject *tmp_tuple_element_24;
            PyObject *tmp_source_name_35;
            PyObject *tmp_source_name_36;
            PyObject *tmp_mvar_value_10;
            tmp_source_name_36 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_tornado );

            if ( tmp_source_name_36 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
                }

                if ( tmp_mvar_value_10 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 104;
                    type_description_2 = "c";
                    goto try_except_handler_21;
                }

                tmp_source_name_36 = tmp_mvar_value_10;
                Py_INCREF( tmp_source_name_36 );
                }
            }

            tmp_source_name_35 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain_web );
            Py_DECREF( tmp_source_name_36 );
            if ( tmp_source_name_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            tmp_tuple_element_24 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain_RequestHandler );
            Py_DECREF( tmp_source_name_35 );
            if ( tmp_tuple_element_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            tmp_assign_source_68 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_assign_source_68, 0, tmp_tuple_element_24 );
            assert( tmp_WebAggApplication$class_creation_4__bases_orig == NULL );
            tmp_WebAggApplication$class_creation_4__bases_orig = tmp_assign_source_68;
        }
        {
            PyObject *tmp_assign_source_69;
            PyObject *tmp_dircall_arg1_7;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__bases_orig );
            tmp_dircall_arg1_7 = tmp_WebAggApplication$class_creation_4__bases_orig;
            Py_INCREF( tmp_dircall_arg1_7 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_7};
                tmp_assign_source_69 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_69 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            assert( tmp_WebAggApplication$class_creation_4__bases == NULL );
            tmp_WebAggApplication$class_creation_4__bases = tmp_assign_source_69;
        }
        {
            PyObject *tmp_assign_source_70;
            tmp_assign_source_70 = PyDict_New();
            assert( tmp_WebAggApplication$class_creation_4__class_decl_dict == NULL );
            tmp_WebAggApplication$class_creation_4__class_decl_dict = tmp_assign_source_70;
        }
        {
            PyObject *tmp_assign_source_71;
            PyObject *tmp_metaclass_name_7;
            nuitka_bool tmp_condition_result_37;
            PyObject *tmp_key_name_19;
            PyObject *tmp_dict_name_19;
            PyObject *tmp_dict_name_20;
            PyObject *tmp_key_name_20;
            nuitka_bool tmp_condition_result_38;
            int tmp_truth_name_7;
            PyObject *tmp_type_arg_13;
            PyObject *tmp_subscribed_name_7;
            PyObject *tmp_subscript_name_7;
            PyObject *tmp_bases_name_7;
            tmp_key_name_19 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__class_decl_dict );
            tmp_dict_name_19 = tmp_WebAggApplication$class_creation_4__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_19, tmp_key_name_19 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            tmp_condition_result_37 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_37 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_13;
            }
            else
            {
                goto condexpr_false_13;
            }
            condexpr_true_13:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__class_decl_dict );
            tmp_dict_name_20 = tmp_WebAggApplication$class_creation_4__class_decl_dict;
            tmp_key_name_20 = const_str_plain_metaclass;
            tmp_metaclass_name_7 = DICT_GET_ITEM( tmp_dict_name_20, tmp_key_name_20 );
            if ( tmp_metaclass_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            goto condexpr_end_13;
            condexpr_false_13:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__bases );
            tmp_truth_name_7 = CHECK_IF_TRUE( tmp_WebAggApplication$class_creation_4__bases );
            if ( tmp_truth_name_7 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            tmp_condition_result_38 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_38 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_14;
            }
            else
            {
                goto condexpr_false_14;
            }
            condexpr_true_14:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__bases );
            tmp_subscribed_name_7 = tmp_WebAggApplication$class_creation_4__bases;
            tmp_subscript_name_7 = const_int_0;
            tmp_type_arg_13 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_7, tmp_subscript_name_7, 0 );
            if ( tmp_type_arg_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            tmp_metaclass_name_7 = BUILTIN_TYPE1( tmp_type_arg_13 );
            Py_DECREF( tmp_type_arg_13 );
            if ( tmp_metaclass_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            goto condexpr_end_14;
            condexpr_false_14:;
            tmp_metaclass_name_7 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_7 );
            condexpr_end_14:;
            condexpr_end_13:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__bases );
            tmp_bases_name_7 = tmp_WebAggApplication$class_creation_4__bases;
            tmp_assign_source_71 = SELECT_METACLASS( tmp_metaclass_name_7, tmp_bases_name_7 );
            Py_DECREF( tmp_metaclass_name_7 );
            if ( tmp_assign_source_71 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            assert( tmp_WebAggApplication$class_creation_4__metaclass == NULL );
            tmp_WebAggApplication$class_creation_4__metaclass = tmp_assign_source_71;
        }
        {
            nuitka_bool tmp_condition_result_39;
            PyObject *tmp_key_name_21;
            PyObject *tmp_dict_name_21;
            tmp_key_name_21 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__class_decl_dict );
            tmp_dict_name_21 = tmp_WebAggApplication$class_creation_4__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_21, tmp_key_name_21 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            tmp_condition_result_39 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_39 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_25;
            }
            else
            {
                goto branch_no_25;
            }
            branch_yes_25:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__class_decl_dict );
            tmp_dictdel_dict = tmp_WebAggApplication$class_creation_4__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_21;
            }
            branch_no_25:;
        }
        {
            nuitka_bool tmp_condition_result_40;
            PyObject *tmp_source_name_37;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__metaclass );
            tmp_source_name_37 = tmp_WebAggApplication$class_creation_4__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_37, const_str_plain___prepare__ );
            tmp_condition_result_40 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_40 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_26;
            }
            else
            {
                goto branch_no_26;
            }
            branch_yes_26:;
            {
                PyObject *tmp_assign_source_72;
                PyObject *tmp_called_name_13;
                PyObject *tmp_source_name_38;
                PyObject *tmp_args_name_12;
                PyObject *tmp_tuple_element_25;
                PyObject *tmp_kw_name_12;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__metaclass );
                tmp_source_name_38 = tmp_WebAggApplication$class_creation_4__metaclass;
                tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain___prepare__ );
                if ( tmp_called_name_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 104;
                    type_description_2 = "c";
                    goto try_except_handler_21;
                }
                tmp_tuple_element_25 = const_str_plain_MplJs;
                tmp_args_name_12 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_25 );
                PyTuple_SET_ITEM( tmp_args_name_12, 0, tmp_tuple_element_25 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__bases );
                tmp_tuple_element_25 = tmp_WebAggApplication$class_creation_4__bases;
                Py_INCREF( tmp_tuple_element_25 );
                PyTuple_SET_ITEM( tmp_args_name_12, 1, tmp_tuple_element_25 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__class_decl_dict );
                tmp_kw_name_12 = tmp_WebAggApplication$class_creation_4__class_decl_dict;
                frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 104;
                tmp_assign_source_72 = CALL_FUNCTION( tmp_called_name_13, tmp_args_name_12, tmp_kw_name_12 );
                Py_DECREF( tmp_called_name_13 );
                Py_DECREF( tmp_args_name_12 );
                if ( tmp_assign_source_72 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 104;
                    type_description_2 = "c";
                    goto try_except_handler_21;
                }
                assert( tmp_WebAggApplication$class_creation_4__prepared == NULL );
                tmp_WebAggApplication$class_creation_4__prepared = tmp_assign_source_72;
            }
            {
                nuitka_bool tmp_condition_result_41;
                PyObject *tmp_operand_name_7;
                PyObject *tmp_source_name_39;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__prepared );
                tmp_source_name_39 = tmp_WebAggApplication$class_creation_4__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_39, const_str_plain___getitem__ );
                tmp_operand_name_7 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_7 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 104;
                    type_description_2 = "c";
                    goto try_except_handler_21;
                }
                tmp_condition_result_41 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_41 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_27;
                }
                else
                {
                    goto branch_no_27;
                }
                branch_yes_27:;
                {
                    PyObject *tmp_raise_type_8;
                    PyObject *tmp_raise_value_7;
                    PyObject *tmp_left_name_7;
                    PyObject *tmp_right_name_7;
                    PyObject *tmp_tuple_element_26;
                    PyObject *tmp_getattr_target_7;
                    PyObject *tmp_getattr_attr_7;
                    PyObject *tmp_getattr_default_7;
                    PyObject *tmp_source_name_40;
                    PyObject *tmp_type_arg_14;
                    tmp_raise_type_8 = PyExc_TypeError;
                    tmp_left_name_7 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__metaclass );
                    tmp_getattr_target_7 = tmp_WebAggApplication$class_creation_4__metaclass;
                    tmp_getattr_attr_7 = const_str_plain___name__;
                    tmp_getattr_default_7 = const_str_angle_metaclass;
                    tmp_tuple_element_26 = BUILTIN_GETATTR( tmp_getattr_target_7, tmp_getattr_attr_7, tmp_getattr_default_7 );
                    if ( tmp_tuple_element_26 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 104;
                        type_description_2 = "c";
                        goto try_except_handler_21;
                    }
                    tmp_right_name_7 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_7, 0, tmp_tuple_element_26 );
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__prepared );
                    tmp_type_arg_14 = tmp_WebAggApplication$class_creation_4__prepared;
                    tmp_source_name_40 = BUILTIN_TYPE1( tmp_type_arg_14 );
                    assert( !(tmp_source_name_40 == NULL) );
                    tmp_tuple_element_26 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_40 );
                    if ( tmp_tuple_element_26 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_7 );

                        exception_lineno = 104;
                        type_description_2 = "c";
                        goto try_except_handler_21;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_7, 1, tmp_tuple_element_26 );
                    tmp_raise_value_7 = BINARY_OPERATION_REMAINDER( tmp_left_name_7, tmp_right_name_7 );
                    Py_DECREF( tmp_right_name_7 );
                    if ( tmp_raise_value_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 104;
                        type_description_2 = "c";
                        goto try_except_handler_21;
                    }
                    exception_type = tmp_raise_type_8;
                    Py_INCREF( tmp_raise_type_8 );
                    exception_value = tmp_raise_value_7;
                    exception_lineno = 104;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "c";
                    goto try_except_handler_21;
                }
                branch_no_27:;
            }
            goto branch_end_26;
            branch_no_26:;
            {
                PyObject *tmp_assign_source_73;
                tmp_assign_source_73 = PyDict_New();
                assert( tmp_WebAggApplication$class_creation_4__prepared == NULL );
                tmp_WebAggApplication$class_creation_4__prepared = tmp_assign_source_73;
            }
            branch_end_26:;
        }
        {
            PyObject *tmp_set_locals_7;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__prepared );
            tmp_set_locals_7 = tmp_WebAggApplication$class_creation_4__prepared;
            locals_matplotlib$backends$backend_webagg_104 = tmp_set_locals_7;
            Py_INCREF( tmp_set_locals_7 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_104, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_2 = "c";
            goto try_except_handler_23;
        }
        tmp_dictset_value = const_str_digest_bc4bb1da5dbfb724e7f6bf7cec6bce9e;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_104, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_2 = "c";
            goto try_except_handler_23;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_8c2d2a58a411a4c99aa649f8241de86d_8, codeobj_8c2d2a58a411a4c99aa649f8241de86d, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_8c2d2a58a411a4c99aa649f8241de86d_8 = cache_frame_8c2d2a58a411a4c99aa649f8241de86d_8;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_8c2d2a58a411a4c99aa649f8241de86d_8 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_8c2d2a58a411a4c99aa649f8241de86d_8 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_9_get(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_104, const_str_plain_get, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_3 = "o";
            goto frame_exception_exit_8;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8c2d2a58a411a4c99aa649f8241de86d_8 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_6;

        frame_exception_exit_8:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8c2d2a58a411a4c99aa649f8241de86d_8 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_8c2d2a58a411a4c99aa649f8241de86d_8, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_8c2d2a58a411a4c99aa649f8241de86d_8->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_8c2d2a58a411a4c99aa649f8241de86d_8, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_8c2d2a58a411a4c99aa649f8241de86d_8,
            type_description_3,
            outline_6_var___class__
        );


        // Release cached frame.
        if ( frame_8c2d2a58a411a4c99aa649f8241de86d_8 == cache_frame_8c2d2a58a411a4c99aa649f8241de86d_8 )
        {
            Py_DECREF( frame_8c2d2a58a411a4c99aa649f8241de86d_8 );
        }
        cache_frame_8c2d2a58a411a4c99aa649f8241de86d_8 = NULL;

        assertFrameObject( frame_8c2d2a58a411a4c99aa649f8241de86d_8 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_7;

        frame_no_exception_6:;
        goto skip_nested_handling_6;
        nested_frame_exit_7:;
        type_description_2 = "c";
        goto try_except_handler_23;
        skip_nested_handling_6:;
        {
            nuitka_bool tmp_condition_result_42;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__bases );
            tmp_compexpr_left_7 = tmp_WebAggApplication$class_creation_4__bases;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__bases_orig );
            tmp_compexpr_right_7 = tmp_WebAggApplication$class_creation_4__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_23;
            }
            tmp_condition_result_42 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_42 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_28;
            }
            else
            {
                goto branch_no_28;
            }
            branch_yes_28:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__bases_orig );
            tmp_dictset_value = tmp_WebAggApplication$class_creation_4__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_104, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_23;
            }
            branch_no_28:;
        }
        {
            PyObject *tmp_assign_source_74;
            PyObject *tmp_called_name_14;
            PyObject *tmp_args_name_13;
            PyObject *tmp_tuple_element_27;
            PyObject *tmp_kw_name_13;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__metaclass );
            tmp_called_name_14 = tmp_WebAggApplication$class_creation_4__metaclass;
            tmp_tuple_element_27 = const_str_plain_MplJs;
            tmp_args_name_13 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_27 );
            PyTuple_SET_ITEM( tmp_args_name_13, 0, tmp_tuple_element_27 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__bases );
            tmp_tuple_element_27 = tmp_WebAggApplication$class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_27 );
            PyTuple_SET_ITEM( tmp_args_name_13, 1, tmp_tuple_element_27 );
            tmp_tuple_element_27 = locals_matplotlib$backends$backend_webagg_104;
            Py_INCREF( tmp_tuple_element_27 );
            PyTuple_SET_ITEM( tmp_args_name_13, 2, tmp_tuple_element_27 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_4__class_decl_dict );
            tmp_kw_name_13 = tmp_WebAggApplication$class_creation_4__class_decl_dict;
            frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 104;
            tmp_assign_source_74 = CALL_FUNCTION( tmp_called_name_14, tmp_args_name_13, tmp_kw_name_13 );
            Py_DECREF( tmp_args_name_13 );
            if ( tmp_assign_source_74 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_2 = "c";
                goto try_except_handler_23;
            }
            assert( outline_6_var___class__ == NULL );
            outline_6_var___class__ = tmp_assign_source_74;
        }
        CHECK_OBJECT( outline_6_var___class__ );
        tmp_dictset_value = outline_6_var___class__;
        Py_INCREF( tmp_dictset_value );
        goto try_return_handler_23;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_23:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_104 );
        locals_matplotlib$backends$backend_webagg_104 = NULL;
        goto try_return_handler_22;
        // Exception handler code:
        try_except_handler_23:;
        exception_keeper_type_18 = exception_type;
        exception_keeper_value_18 = exception_value;
        exception_keeper_tb_18 = exception_tb;
        exception_keeper_lineno_18 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_104 );
        locals_matplotlib$backends$backend_webagg_104 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_18;
        exception_value = exception_keeper_value_18;
        exception_tb = exception_keeper_tb_18;
        exception_lineno = exception_keeper_lineno_18;

        goto try_except_handler_22;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_22:;
        CHECK_OBJECT( (PyObject *)outline_6_var___class__ );
        Py_DECREF( outline_6_var___class__ );
        outline_6_var___class__ = NULL;

        goto outline_result_7;
        // Exception handler code:
        try_except_handler_22:;
        exception_keeper_type_19 = exception_type;
        exception_keeper_value_19 = exception_value;
        exception_keeper_tb_19 = exception_tb;
        exception_keeper_lineno_19 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_19;
        exception_value = exception_keeper_value_19;
        exception_tb = exception_keeper_tb_19;
        exception_lineno = exception_keeper_lineno_19;

        goto outline_exception_7;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_7:;
        exception_lineno = 104;
        goto try_except_handler_21;
        outline_result_7:;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_MplJs, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_2 = "c";
            goto try_except_handler_21;
        }
        goto try_end_7;
        // Exception handler code:
        try_except_handler_21:;
        exception_keeper_type_20 = exception_type;
        exception_keeper_value_20 = exception_value;
        exception_keeper_tb_20 = exception_tb;
        exception_keeper_lineno_20 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_WebAggApplication$class_creation_4__bases_orig );
        tmp_WebAggApplication$class_creation_4__bases_orig = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_4__bases );
        tmp_WebAggApplication$class_creation_4__bases = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_4__class_decl_dict );
        tmp_WebAggApplication$class_creation_4__class_decl_dict = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_4__metaclass );
        tmp_WebAggApplication$class_creation_4__metaclass = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_4__prepared );
        tmp_WebAggApplication$class_creation_4__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_20;
        exception_value = exception_keeper_value_20;
        exception_tb = exception_keeper_tb_20;
        exception_lineno = exception_keeper_lineno_20;

        goto frame_exception_exit_4;
        // End of try:
        try_end_7:;
        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_4__bases_orig );
        Py_DECREF( tmp_WebAggApplication$class_creation_4__bases_orig );
        tmp_WebAggApplication$class_creation_4__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_4__bases );
        Py_DECREF( tmp_WebAggApplication$class_creation_4__bases );
        tmp_WebAggApplication$class_creation_4__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_4__class_decl_dict );
        Py_DECREF( tmp_WebAggApplication$class_creation_4__class_decl_dict );
        tmp_WebAggApplication$class_creation_4__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_4__metaclass );
        Py_DECREF( tmp_WebAggApplication$class_creation_4__metaclass );
        tmp_WebAggApplication$class_creation_4__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_4__prepared );
        Py_DECREF( tmp_WebAggApplication$class_creation_4__prepared );
        tmp_WebAggApplication$class_creation_4__prepared = NULL;

        // Tried code:
        {
            PyObject *tmp_assign_source_75;
            PyObject *tmp_tuple_element_28;
            PyObject *tmp_source_name_41;
            PyObject *tmp_source_name_42;
            PyObject *tmp_mvar_value_11;
            tmp_source_name_42 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_tornado );

            if ( tmp_source_name_42 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 112;
                    type_description_2 = "c";
                    goto try_except_handler_24;
                }

                tmp_source_name_42 = tmp_mvar_value_11;
                Py_INCREF( tmp_source_name_42 );
                }
            }

            tmp_source_name_41 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain_web );
            Py_DECREF( tmp_source_name_42 );
            if ( tmp_source_name_41 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            tmp_tuple_element_28 = LOOKUP_ATTRIBUTE( tmp_source_name_41, const_str_plain_RequestHandler );
            Py_DECREF( tmp_source_name_41 );
            if ( tmp_tuple_element_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            tmp_assign_source_75 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_assign_source_75, 0, tmp_tuple_element_28 );
            assert( tmp_WebAggApplication$class_creation_5__bases_orig == NULL );
            tmp_WebAggApplication$class_creation_5__bases_orig = tmp_assign_source_75;
        }
        {
            PyObject *tmp_assign_source_76;
            PyObject *tmp_dircall_arg1_8;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__bases_orig );
            tmp_dircall_arg1_8 = tmp_WebAggApplication$class_creation_5__bases_orig;
            Py_INCREF( tmp_dircall_arg1_8 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_8};
                tmp_assign_source_76 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_76 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            assert( tmp_WebAggApplication$class_creation_5__bases == NULL );
            tmp_WebAggApplication$class_creation_5__bases = tmp_assign_source_76;
        }
        {
            PyObject *tmp_assign_source_77;
            tmp_assign_source_77 = PyDict_New();
            assert( tmp_WebAggApplication$class_creation_5__class_decl_dict == NULL );
            tmp_WebAggApplication$class_creation_5__class_decl_dict = tmp_assign_source_77;
        }
        {
            PyObject *tmp_assign_source_78;
            PyObject *tmp_metaclass_name_8;
            nuitka_bool tmp_condition_result_43;
            PyObject *tmp_key_name_22;
            PyObject *tmp_dict_name_22;
            PyObject *tmp_dict_name_23;
            PyObject *tmp_key_name_23;
            nuitka_bool tmp_condition_result_44;
            int tmp_truth_name_8;
            PyObject *tmp_type_arg_15;
            PyObject *tmp_subscribed_name_8;
            PyObject *tmp_subscript_name_8;
            PyObject *tmp_bases_name_8;
            tmp_key_name_22 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__class_decl_dict );
            tmp_dict_name_22 = tmp_WebAggApplication$class_creation_5__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_22, tmp_key_name_22 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            tmp_condition_result_43 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_43 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_15;
            }
            else
            {
                goto condexpr_false_15;
            }
            condexpr_true_15:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__class_decl_dict );
            tmp_dict_name_23 = tmp_WebAggApplication$class_creation_5__class_decl_dict;
            tmp_key_name_23 = const_str_plain_metaclass;
            tmp_metaclass_name_8 = DICT_GET_ITEM( tmp_dict_name_23, tmp_key_name_23 );
            if ( tmp_metaclass_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            goto condexpr_end_15;
            condexpr_false_15:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__bases );
            tmp_truth_name_8 = CHECK_IF_TRUE( tmp_WebAggApplication$class_creation_5__bases );
            if ( tmp_truth_name_8 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            tmp_condition_result_44 = tmp_truth_name_8 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_44 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_16;
            }
            else
            {
                goto condexpr_false_16;
            }
            condexpr_true_16:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__bases );
            tmp_subscribed_name_8 = tmp_WebAggApplication$class_creation_5__bases;
            tmp_subscript_name_8 = const_int_0;
            tmp_type_arg_15 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_8, tmp_subscript_name_8, 0 );
            if ( tmp_type_arg_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            tmp_metaclass_name_8 = BUILTIN_TYPE1( tmp_type_arg_15 );
            Py_DECREF( tmp_type_arg_15 );
            if ( tmp_metaclass_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            goto condexpr_end_16;
            condexpr_false_16:;
            tmp_metaclass_name_8 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_8 );
            condexpr_end_16:;
            condexpr_end_15:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__bases );
            tmp_bases_name_8 = tmp_WebAggApplication$class_creation_5__bases;
            tmp_assign_source_78 = SELECT_METACLASS( tmp_metaclass_name_8, tmp_bases_name_8 );
            Py_DECREF( tmp_metaclass_name_8 );
            if ( tmp_assign_source_78 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            assert( tmp_WebAggApplication$class_creation_5__metaclass == NULL );
            tmp_WebAggApplication$class_creation_5__metaclass = tmp_assign_source_78;
        }
        {
            nuitka_bool tmp_condition_result_45;
            PyObject *tmp_key_name_24;
            PyObject *tmp_dict_name_24;
            tmp_key_name_24 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__class_decl_dict );
            tmp_dict_name_24 = tmp_WebAggApplication$class_creation_5__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_24, tmp_key_name_24 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            tmp_condition_result_45 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_45 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_29;
            }
            else
            {
                goto branch_no_29;
            }
            branch_yes_29:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__class_decl_dict );
            tmp_dictdel_dict = tmp_WebAggApplication$class_creation_5__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_24;
            }
            branch_no_29:;
        }
        {
            nuitka_bool tmp_condition_result_46;
            PyObject *tmp_source_name_43;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__metaclass );
            tmp_source_name_43 = tmp_WebAggApplication$class_creation_5__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_43, const_str_plain___prepare__ );
            tmp_condition_result_46 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_46 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_30;
            }
            else
            {
                goto branch_no_30;
            }
            branch_yes_30:;
            {
                PyObject *tmp_assign_source_79;
                PyObject *tmp_called_name_15;
                PyObject *tmp_source_name_44;
                PyObject *tmp_args_name_14;
                PyObject *tmp_tuple_element_29;
                PyObject *tmp_kw_name_14;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__metaclass );
                tmp_source_name_44 = tmp_WebAggApplication$class_creation_5__metaclass;
                tmp_called_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_44, const_str_plain___prepare__ );
                if ( tmp_called_name_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 112;
                    type_description_2 = "c";
                    goto try_except_handler_24;
                }
                tmp_tuple_element_29 = const_str_plain_Download;
                tmp_args_name_14 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_29 );
                PyTuple_SET_ITEM( tmp_args_name_14, 0, tmp_tuple_element_29 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__bases );
                tmp_tuple_element_29 = tmp_WebAggApplication$class_creation_5__bases;
                Py_INCREF( tmp_tuple_element_29 );
                PyTuple_SET_ITEM( tmp_args_name_14, 1, tmp_tuple_element_29 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__class_decl_dict );
                tmp_kw_name_14 = tmp_WebAggApplication$class_creation_5__class_decl_dict;
                frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 112;
                tmp_assign_source_79 = CALL_FUNCTION( tmp_called_name_15, tmp_args_name_14, tmp_kw_name_14 );
                Py_DECREF( tmp_called_name_15 );
                Py_DECREF( tmp_args_name_14 );
                if ( tmp_assign_source_79 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 112;
                    type_description_2 = "c";
                    goto try_except_handler_24;
                }
                assert( tmp_WebAggApplication$class_creation_5__prepared == NULL );
                tmp_WebAggApplication$class_creation_5__prepared = tmp_assign_source_79;
            }
            {
                nuitka_bool tmp_condition_result_47;
                PyObject *tmp_operand_name_8;
                PyObject *tmp_source_name_45;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__prepared );
                tmp_source_name_45 = tmp_WebAggApplication$class_creation_5__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_45, const_str_plain___getitem__ );
                tmp_operand_name_8 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_8 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 112;
                    type_description_2 = "c";
                    goto try_except_handler_24;
                }
                tmp_condition_result_47 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_47 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_31;
                }
                else
                {
                    goto branch_no_31;
                }
                branch_yes_31:;
                {
                    PyObject *tmp_raise_type_9;
                    PyObject *tmp_raise_value_8;
                    PyObject *tmp_left_name_8;
                    PyObject *tmp_right_name_8;
                    PyObject *tmp_tuple_element_30;
                    PyObject *tmp_getattr_target_8;
                    PyObject *tmp_getattr_attr_8;
                    PyObject *tmp_getattr_default_8;
                    PyObject *tmp_source_name_46;
                    PyObject *tmp_type_arg_16;
                    tmp_raise_type_9 = PyExc_TypeError;
                    tmp_left_name_8 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__metaclass );
                    tmp_getattr_target_8 = tmp_WebAggApplication$class_creation_5__metaclass;
                    tmp_getattr_attr_8 = const_str_plain___name__;
                    tmp_getattr_default_8 = const_str_angle_metaclass;
                    tmp_tuple_element_30 = BUILTIN_GETATTR( tmp_getattr_target_8, tmp_getattr_attr_8, tmp_getattr_default_8 );
                    if ( tmp_tuple_element_30 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 112;
                        type_description_2 = "c";
                        goto try_except_handler_24;
                    }
                    tmp_right_name_8 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_8, 0, tmp_tuple_element_30 );
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__prepared );
                    tmp_type_arg_16 = tmp_WebAggApplication$class_creation_5__prepared;
                    tmp_source_name_46 = BUILTIN_TYPE1( tmp_type_arg_16 );
                    assert( !(tmp_source_name_46 == NULL) );
                    tmp_tuple_element_30 = LOOKUP_ATTRIBUTE( tmp_source_name_46, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_46 );
                    if ( tmp_tuple_element_30 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_8 );

                        exception_lineno = 112;
                        type_description_2 = "c";
                        goto try_except_handler_24;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_8, 1, tmp_tuple_element_30 );
                    tmp_raise_value_8 = BINARY_OPERATION_REMAINDER( tmp_left_name_8, tmp_right_name_8 );
                    Py_DECREF( tmp_right_name_8 );
                    if ( tmp_raise_value_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 112;
                        type_description_2 = "c";
                        goto try_except_handler_24;
                    }
                    exception_type = tmp_raise_type_9;
                    Py_INCREF( tmp_raise_type_9 );
                    exception_value = tmp_raise_value_8;
                    exception_lineno = 112;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "c";
                    goto try_except_handler_24;
                }
                branch_no_31:;
            }
            goto branch_end_30;
            branch_no_30:;
            {
                PyObject *tmp_assign_source_80;
                tmp_assign_source_80 = PyDict_New();
                assert( tmp_WebAggApplication$class_creation_5__prepared == NULL );
                tmp_WebAggApplication$class_creation_5__prepared = tmp_assign_source_80;
            }
            branch_end_30:;
        }
        {
            PyObject *tmp_set_locals_8;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__prepared );
            tmp_set_locals_8 = tmp_WebAggApplication$class_creation_5__prepared;
            locals_matplotlib$backends$backend_webagg_112 = tmp_set_locals_8;
            Py_INCREF( tmp_set_locals_8 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_112, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_2 = "c";
            goto try_except_handler_26;
        }
        tmp_dictset_value = const_str_digest_3f9c4d1d16726006443c19835dc0b4cf;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_112, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_2 = "c";
            goto try_except_handler_26;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_56eaf9c20edf0f83e8ca1007194d7244_9, codeobj_56eaf9c20edf0f83e8ca1007194d7244, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_56eaf9c20edf0f83e8ca1007194d7244_9 = cache_frame_56eaf9c20edf0f83e8ca1007194d7244_9;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_56eaf9c20edf0f83e8ca1007194d7244_9 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_56eaf9c20edf0f83e8ca1007194d7244_9 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_10_get(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_112, const_str_plain_get, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_3 = "o";
            goto frame_exception_exit_9;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_56eaf9c20edf0f83e8ca1007194d7244_9 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_7;

        frame_exception_exit_9:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_56eaf9c20edf0f83e8ca1007194d7244_9 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_56eaf9c20edf0f83e8ca1007194d7244_9, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_56eaf9c20edf0f83e8ca1007194d7244_9->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_56eaf9c20edf0f83e8ca1007194d7244_9, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_56eaf9c20edf0f83e8ca1007194d7244_9,
            type_description_3,
            outline_7_var___class__
        );


        // Release cached frame.
        if ( frame_56eaf9c20edf0f83e8ca1007194d7244_9 == cache_frame_56eaf9c20edf0f83e8ca1007194d7244_9 )
        {
            Py_DECREF( frame_56eaf9c20edf0f83e8ca1007194d7244_9 );
        }
        cache_frame_56eaf9c20edf0f83e8ca1007194d7244_9 = NULL;

        assertFrameObject( frame_56eaf9c20edf0f83e8ca1007194d7244_9 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_8;

        frame_no_exception_7:;
        goto skip_nested_handling_7;
        nested_frame_exit_8:;
        type_description_2 = "c";
        goto try_except_handler_26;
        skip_nested_handling_7:;
        {
            nuitka_bool tmp_condition_result_48;
            PyObject *tmp_compexpr_left_8;
            PyObject *tmp_compexpr_right_8;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__bases );
            tmp_compexpr_left_8 = tmp_WebAggApplication$class_creation_5__bases;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__bases_orig );
            tmp_compexpr_right_8 = tmp_WebAggApplication$class_creation_5__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_26;
            }
            tmp_condition_result_48 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_48 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_32;
            }
            else
            {
                goto branch_no_32;
            }
            branch_yes_32:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__bases_orig );
            tmp_dictset_value = tmp_WebAggApplication$class_creation_5__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_112, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_26;
            }
            branch_no_32:;
        }
        {
            PyObject *tmp_assign_source_81;
            PyObject *tmp_called_name_16;
            PyObject *tmp_args_name_15;
            PyObject *tmp_tuple_element_31;
            PyObject *tmp_kw_name_15;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__metaclass );
            tmp_called_name_16 = tmp_WebAggApplication$class_creation_5__metaclass;
            tmp_tuple_element_31 = const_str_plain_Download;
            tmp_args_name_15 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_31 );
            PyTuple_SET_ITEM( tmp_args_name_15, 0, tmp_tuple_element_31 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__bases );
            tmp_tuple_element_31 = tmp_WebAggApplication$class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_31 );
            PyTuple_SET_ITEM( tmp_args_name_15, 1, tmp_tuple_element_31 );
            tmp_tuple_element_31 = locals_matplotlib$backends$backend_webagg_112;
            Py_INCREF( tmp_tuple_element_31 );
            PyTuple_SET_ITEM( tmp_args_name_15, 2, tmp_tuple_element_31 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_5__class_decl_dict );
            tmp_kw_name_15 = tmp_WebAggApplication$class_creation_5__class_decl_dict;
            frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 112;
            tmp_assign_source_81 = CALL_FUNCTION( tmp_called_name_16, tmp_args_name_15, tmp_kw_name_15 );
            Py_DECREF( tmp_args_name_15 );
            if ( tmp_assign_source_81 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "c";
                goto try_except_handler_26;
            }
            assert( outline_7_var___class__ == NULL );
            outline_7_var___class__ = tmp_assign_source_81;
        }
        CHECK_OBJECT( outline_7_var___class__ );
        tmp_dictset_value = outline_7_var___class__;
        Py_INCREF( tmp_dictset_value );
        goto try_return_handler_26;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_26:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_112 );
        locals_matplotlib$backends$backend_webagg_112 = NULL;
        goto try_return_handler_25;
        // Exception handler code:
        try_except_handler_26:;
        exception_keeper_type_21 = exception_type;
        exception_keeper_value_21 = exception_value;
        exception_keeper_tb_21 = exception_tb;
        exception_keeper_lineno_21 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_112 );
        locals_matplotlib$backends$backend_webagg_112 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_21;
        exception_value = exception_keeper_value_21;
        exception_tb = exception_keeper_tb_21;
        exception_lineno = exception_keeper_lineno_21;

        goto try_except_handler_25;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_25:;
        CHECK_OBJECT( (PyObject *)outline_7_var___class__ );
        Py_DECREF( outline_7_var___class__ );
        outline_7_var___class__ = NULL;

        goto outline_result_8;
        // Exception handler code:
        try_except_handler_25:;
        exception_keeper_type_22 = exception_type;
        exception_keeper_value_22 = exception_value;
        exception_keeper_tb_22 = exception_tb;
        exception_keeper_lineno_22 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_22;
        exception_value = exception_keeper_value_22;
        exception_tb = exception_keeper_tb_22;
        exception_lineno = exception_keeper_lineno_22;

        goto outline_exception_8;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_8:;
        exception_lineno = 112;
        goto try_except_handler_24;
        outline_result_8:;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_Download, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_2 = "c";
            goto try_except_handler_24;
        }
        goto try_end_8;
        // Exception handler code:
        try_except_handler_24:;
        exception_keeper_type_23 = exception_type;
        exception_keeper_value_23 = exception_value;
        exception_keeper_tb_23 = exception_tb;
        exception_keeper_lineno_23 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_WebAggApplication$class_creation_5__bases_orig );
        tmp_WebAggApplication$class_creation_5__bases_orig = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_5__bases );
        tmp_WebAggApplication$class_creation_5__bases = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_5__class_decl_dict );
        tmp_WebAggApplication$class_creation_5__class_decl_dict = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_5__metaclass );
        tmp_WebAggApplication$class_creation_5__metaclass = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_5__prepared );
        tmp_WebAggApplication$class_creation_5__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_23;
        exception_value = exception_keeper_value_23;
        exception_tb = exception_keeper_tb_23;
        exception_lineno = exception_keeper_lineno_23;

        goto frame_exception_exit_4;
        // End of try:
        try_end_8:;
        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_5__bases_orig );
        Py_DECREF( tmp_WebAggApplication$class_creation_5__bases_orig );
        tmp_WebAggApplication$class_creation_5__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_5__bases );
        Py_DECREF( tmp_WebAggApplication$class_creation_5__bases );
        tmp_WebAggApplication$class_creation_5__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_5__class_decl_dict );
        Py_DECREF( tmp_WebAggApplication$class_creation_5__class_decl_dict );
        tmp_WebAggApplication$class_creation_5__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_5__metaclass );
        Py_DECREF( tmp_WebAggApplication$class_creation_5__metaclass );
        tmp_WebAggApplication$class_creation_5__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_5__prepared );
        Py_DECREF( tmp_WebAggApplication$class_creation_5__prepared );
        tmp_WebAggApplication$class_creation_5__prepared = NULL;

        // Tried code:
        {
            PyObject *tmp_assign_source_82;
            PyObject *tmp_tuple_element_32;
            PyObject *tmp_source_name_47;
            PyObject *tmp_source_name_48;
            PyObject *tmp_mvar_value_12;
            tmp_source_name_48 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_tornado );

            if ( tmp_source_name_48 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_tornado );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tornado );
                }

                if ( tmp_mvar_value_12 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tornado" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 135;
                    type_description_2 = "c";
                    goto try_except_handler_27;
                }

                tmp_source_name_48 = tmp_mvar_value_12;
                Py_INCREF( tmp_source_name_48 );
                }
            }

            tmp_source_name_47 = LOOKUP_ATTRIBUTE( tmp_source_name_48, const_str_plain_websocket );
            Py_DECREF( tmp_source_name_48 );
            if ( tmp_source_name_47 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            tmp_tuple_element_32 = LOOKUP_ATTRIBUTE( tmp_source_name_47, const_str_plain_WebSocketHandler );
            Py_DECREF( tmp_source_name_47 );
            if ( tmp_tuple_element_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            tmp_assign_source_82 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_assign_source_82, 0, tmp_tuple_element_32 );
            assert( tmp_WebAggApplication$class_creation_6__bases_orig == NULL );
            tmp_WebAggApplication$class_creation_6__bases_orig = tmp_assign_source_82;
        }
        {
            PyObject *tmp_assign_source_83;
            PyObject *tmp_dircall_arg1_9;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__bases_orig );
            tmp_dircall_arg1_9 = tmp_WebAggApplication$class_creation_6__bases_orig;
            Py_INCREF( tmp_dircall_arg1_9 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_9};
                tmp_assign_source_83 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_83 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            assert( tmp_WebAggApplication$class_creation_6__bases == NULL );
            tmp_WebAggApplication$class_creation_6__bases = tmp_assign_source_83;
        }
        {
            PyObject *tmp_assign_source_84;
            tmp_assign_source_84 = PyDict_New();
            assert( tmp_WebAggApplication$class_creation_6__class_decl_dict == NULL );
            tmp_WebAggApplication$class_creation_6__class_decl_dict = tmp_assign_source_84;
        }
        {
            PyObject *tmp_assign_source_85;
            PyObject *tmp_metaclass_name_9;
            nuitka_bool tmp_condition_result_49;
            PyObject *tmp_key_name_25;
            PyObject *tmp_dict_name_25;
            PyObject *tmp_dict_name_26;
            PyObject *tmp_key_name_26;
            nuitka_bool tmp_condition_result_50;
            int tmp_truth_name_9;
            PyObject *tmp_type_arg_17;
            PyObject *tmp_subscribed_name_9;
            PyObject *tmp_subscript_name_9;
            PyObject *tmp_bases_name_9;
            tmp_key_name_25 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__class_decl_dict );
            tmp_dict_name_25 = tmp_WebAggApplication$class_creation_6__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_25, tmp_key_name_25 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            tmp_condition_result_49 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_49 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_17;
            }
            else
            {
                goto condexpr_false_17;
            }
            condexpr_true_17:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__class_decl_dict );
            tmp_dict_name_26 = tmp_WebAggApplication$class_creation_6__class_decl_dict;
            tmp_key_name_26 = const_str_plain_metaclass;
            tmp_metaclass_name_9 = DICT_GET_ITEM( tmp_dict_name_26, tmp_key_name_26 );
            if ( tmp_metaclass_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            goto condexpr_end_17;
            condexpr_false_17:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__bases );
            tmp_truth_name_9 = CHECK_IF_TRUE( tmp_WebAggApplication$class_creation_6__bases );
            if ( tmp_truth_name_9 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            tmp_condition_result_50 = tmp_truth_name_9 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_50 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_18;
            }
            else
            {
                goto condexpr_false_18;
            }
            condexpr_true_18:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__bases );
            tmp_subscribed_name_9 = tmp_WebAggApplication$class_creation_6__bases;
            tmp_subscript_name_9 = const_int_0;
            tmp_type_arg_17 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_9, tmp_subscript_name_9, 0 );
            if ( tmp_type_arg_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            tmp_metaclass_name_9 = BUILTIN_TYPE1( tmp_type_arg_17 );
            Py_DECREF( tmp_type_arg_17 );
            if ( tmp_metaclass_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            goto condexpr_end_18;
            condexpr_false_18:;
            tmp_metaclass_name_9 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_9 );
            condexpr_end_18:;
            condexpr_end_17:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__bases );
            tmp_bases_name_9 = tmp_WebAggApplication$class_creation_6__bases;
            tmp_assign_source_85 = SELECT_METACLASS( tmp_metaclass_name_9, tmp_bases_name_9 );
            Py_DECREF( tmp_metaclass_name_9 );
            if ( tmp_assign_source_85 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            assert( tmp_WebAggApplication$class_creation_6__metaclass == NULL );
            tmp_WebAggApplication$class_creation_6__metaclass = tmp_assign_source_85;
        }
        {
            nuitka_bool tmp_condition_result_51;
            PyObject *tmp_key_name_27;
            PyObject *tmp_dict_name_27;
            tmp_key_name_27 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__class_decl_dict );
            tmp_dict_name_27 = tmp_WebAggApplication$class_creation_6__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_27, tmp_key_name_27 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            tmp_condition_result_51 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_51 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_33;
            }
            else
            {
                goto branch_no_33;
            }
            branch_yes_33:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__class_decl_dict );
            tmp_dictdel_dict = tmp_WebAggApplication$class_creation_6__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_27;
            }
            branch_no_33:;
        }
        {
            nuitka_bool tmp_condition_result_52;
            PyObject *tmp_source_name_49;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__metaclass );
            tmp_source_name_49 = tmp_WebAggApplication$class_creation_6__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_49, const_str_plain___prepare__ );
            tmp_condition_result_52 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_52 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_34;
            }
            else
            {
                goto branch_no_34;
            }
            branch_yes_34:;
            {
                PyObject *tmp_assign_source_86;
                PyObject *tmp_called_name_17;
                PyObject *tmp_source_name_50;
                PyObject *tmp_args_name_16;
                PyObject *tmp_tuple_element_33;
                PyObject *tmp_kw_name_16;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__metaclass );
                tmp_source_name_50 = tmp_WebAggApplication$class_creation_6__metaclass;
                tmp_called_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_50, const_str_plain___prepare__ );
                if ( tmp_called_name_17 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 135;
                    type_description_2 = "c";
                    goto try_except_handler_27;
                }
                tmp_tuple_element_33 = const_str_plain_WebSocket;
                tmp_args_name_16 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_33 );
                PyTuple_SET_ITEM( tmp_args_name_16, 0, tmp_tuple_element_33 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__bases );
                tmp_tuple_element_33 = tmp_WebAggApplication$class_creation_6__bases;
                Py_INCREF( tmp_tuple_element_33 );
                PyTuple_SET_ITEM( tmp_args_name_16, 1, tmp_tuple_element_33 );
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__class_decl_dict );
                tmp_kw_name_16 = tmp_WebAggApplication$class_creation_6__class_decl_dict;
                frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 135;
                tmp_assign_source_86 = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_16, tmp_kw_name_16 );
                Py_DECREF( tmp_called_name_17 );
                Py_DECREF( tmp_args_name_16 );
                if ( tmp_assign_source_86 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 135;
                    type_description_2 = "c";
                    goto try_except_handler_27;
                }
                assert( tmp_WebAggApplication$class_creation_6__prepared == NULL );
                tmp_WebAggApplication$class_creation_6__prepared = tmp_assign_source_86;
            }
            {
                nuitka_bool tmp_condition_result_53;
                PyObject *tmp_operand_name_9;
                PyObject *tmp_source_name_51;
                CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__prepared );
                tmp_source_name_51 = tmp_WebAggApplication$class_creation_6__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_51, const_str_plain___getitem__ );
                tmp_operand_name_9 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_9 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 135;
                    type_description_2 = "c";
                    goto try_except_handler_27;
                }
                tmp_condition_result_53 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_53 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_35;
                }
                else
                {
                    goto branch_no_35;
                }
                branch_yes_35:;
                {
                    PyObject *tmp_raise_type_10;
                    PyObject *tmp_raise_value_9;
                    PyObject *tmp_left_name_9;
                    PyObject *tmp_right_name_9;
                    PyObject *tmp_tuple_element_34;
                    PyObject *tmp_getattr_target_9;
                    PyObject *tmp_getattr_attr_9;
                    PyObject *tmp_getattr_default_9;
                    PyObject *tmp_source_name_52;
                    PyObject *tmp_type_arg_18;
                    tmp_raise_type_10 = PyExc_TypeError;
                    tmp_left_name_9 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__metaclass );
                    tmp_getattr_target_9 = tmp_WebAggApplication$class_creation_6__metaclass;
                    tmp_getattr_attr_9 = const_str_plain___name__;
                    tmp_getattr_default_9 = const_str_angle_metaclass;
                    tmp_tuple_element_34 = BUILTIN_GETATTR( tmp_getattr_target_9, tmp_getattr_attr_9, tmp_getattr_default_9 );
                    if ( tmp_tuple_element_34 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 135;
                        type_description_2 = "c";
                        goto try_except_handler_27;
                    }
                    tmp_right_name_9 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_9, 0, tmp_tuple_element_34 );
                    CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__prepared );
                    tmp_type_arg_18 = tmp_WebAggApplication$class_creation_6__prepared;
                    tmp_source_name_52 = BUILTIN_TYPE1( tmp_type_arg_18 );
                    assert( !(tmp_source_name_52 == NULL) );
                    tmp_tuple_element_34 = LOOKUP_ATTRIBUTE( tmp_source_name_52, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_52 );
                    if ( tmp_tuple_element_34 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_9 );

                        exception_lineno = 135;
                        type_description_2 = "c";
                        goto try_except_handler_27;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_9, 1, tmp_tuple_element_34 );
                    tmp_raise_value_9 = BINARY_OPERATION_REMAINDER( tmp_left_name_9, tmp_right_name_9 );
                    Py_DECREF( tmp_right_name_9 );
                    if ( tmp_raise_value_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 135;
                        type_description_2 = "c";
                        goto try_except_handler_27;
                    }
                    exception_type = tmp_raise_type_10;
                    Py_INCREF( tmp_raise_type_10 );
                    exception_value = tmp_raise_value_9;
                    exception_lineno = 135;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "c";
                    goto try_except_handler_27;
                }
                branch_no_35:;
            }
            goto branch_end_34;
            branch_no_34:;
            {
                PyObject *tmp_assign_source_87;
                tmp_assign_source_87 = PyDict_New();
                assert( tmp_WebAggApplication$class_creation_6__prepared == NULL );
                tmp_WebAggApplication$class_creation_6__prepared = tmp_assign_source_87;
            }
            branch_end_34:;
        }
        {
            PyObject *tmp_set_locals_9;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__prepared );
            tmp_set_locals_9 = tmp_WebAggApplication$class_creation_6__prepared;
            locals_matplotlib$backends$backend_webagg_135 = tmp_set_locals_9;
            Py_INCREF( tmp_set_locals_9 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_135, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_2 = "c";
            goto try_except_handler_29;
        }
        tmp_dictset_value = const_str_digest_99105b9e29e2649d8dbc9a686f274db6;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_135, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_2 = "c";
            goto try_except_handler_29;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_e4252a982fc0f588ca0f884fdde91a3c_10, codeobj_e4252a982fc0f588ca0f884fdde91a3c, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_e4252a982fc0f588ca0f884fdde91a3c_10 = cache_frame_e4252a982fc0f588ca0f884fdde91a3c_10;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_e4252a982fc0f588ca0f884fdde91a3c_10 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_e4252a982fc0f588ca0f884fdde91a3c_10 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = Py_True;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_135, const_str_plain_supports_binary, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_3 = "o";
            goto frame_exception_exit_10;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_11_open(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_135, const_str_plain_open, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_3 = "o";
            goto frame_exception_exit_10;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_12_on_close(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_135, const_str_plain_on_close, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_3 = "o";
            goto frame_exception_exit_10;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_13_on_message(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_135, const_str_plain_on_message, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_3 = "o";
            goto frame_exception_exit_10;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_14_send_json(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_135, const_str_plain_send_json, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_3 = "o";
            goto frame_exception_exit_10;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_15_send_binary(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_135, const_str_plain_send_binary, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_3 = "o";
            goto frame_exception_exit_10;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e4252a982fc0f588ca0f884fdde91a3c_10 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_8;

        frame_exception_exit_10:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e4252a982fc0f588ca0f884fdde91a3c_10 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_e4252a982fc0f588ca0f884fdde91a3c_10, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_e4252a982fc0f588ca0f884fdde91a3c_10->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_e4252a982fc0f588ca0f884fdde91a3c_10, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_e4252a982fc0f588ca0f884fdde91a3c_10,
            type_description_3,
            outline_8_var___class__
        );


        // Release cached frame.
        if ( frame_e4252a982fc0f588ca0f884fdde91a3c_10 == cache_frame_e4252a982fc0f588ca0f884fdde91a3c_10 )
        {
            Py_DECREF( frame_e4252a982fc0f588ca0f884fdde91a3c_10 );
        }
        cache_frame_e4252a982fc0f588ca0f884fdde91a3c_10 = NULL;

        assertFrameObject( frame_e4252a982fc0f588ca0f884fdde91a3c_10 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_9;

        frame_no_exception_8:;
        goto skip_nested_handling_8;
        nested_frame_exit_9:;
        type_description_2 = "c";
        goto try_except_handler_29;
        skip_nested_handling_8:;
        {
            nuitka_bool tmp_condition_result_54;
            PyObject *tmp_compexpr_left_9;
            PyObject *tmp_compexpr_right_9;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__bases );
            tmp_compexpr_left_9 = tmp_WebAggApplication$class_creation_6__bases;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__bases_orig );
            tmp_compexpr_right_9 = tmp_WebAggApplication$class_creation_6__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_29;
            }
            tmp_condition_result_54 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_54 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_36;
            }
            else
            {
                goto branch_no_36;
            }
            branch_yes_36:;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__bases_orig );
            tmp_dictset_value = tmp_WebAggApplication$class_creation_6__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_135, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_29;
            }
            branch_no_36:;
        }
        {
            PyObject *tmp_assign_source_88;
            PyObject *tmp_called_name_18;
            PyObject *tmp_args_name_17;
            PyObject *tmp_tuple_element_35;
            PyObject *tmp_kw_name_17;
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__metaclass );
            tmp_called_name_18 = tmp_WebAggApplication$class_creation_6__metaclass;
            tmp_tuple_element_35 = const_str_plain_WebSocket;
            tmp_args_name_17 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_35 );
            PyTuple_SET_ITEM( tmp_args_name_17, 0, tmp_tuple_element_35 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__bases );
            tmp_tuple_element_35 = tmp_WebAggApplication$class_creation_6__bases;
            Py_INCREF( tmp_tuple_element_35 );
            PyTuple_SET_ITEM( tmp_args_name_17, 1, tmp_tuple_element_35 );
            tmp_tuple_element_35 = locals_matplotlib$backends$backend_webagg_135;
            Py_INCREF( tmp_tuple_element_35 );
            PyTuple_SET_ITEM( tmp_args_name_17, 2, tmp_tuple_element_35 );
            CHECK_OBJECT( tmp_WebAggApplication$class_creation_6__class_decl_dict );
            tmp_kw_name_17 = tmp_WebAggApplication$class_creation_6__class_decl_dict;
            frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 135;
            tmp_assign_source_88 = CALL_FUNCTION( tmp_called_name_18, tmp_args_name_17, tmp_kw_name_17 );
            Py_DECREF( tmp_args_name_17 );
            if ( tmp_assign_source_88 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_2 = "c";
                goto try_except_handler_29;
            }
            assert( outline_8_var___class__ == NULL );
            outline_8_var___class__ = tmp_assign_source_88;
        }
        CHECK_OBJECT( outline_8_var___class__ );
        tmp_dictset_value = outline_8_var___class__;
        Py_INCREF( tmp_dictset_value );
        goto try_return_handler_29;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_29:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_135 );
        locals_matplotlib$backends$backend_webagg_135 = NULL;
        goto try_return_handler_28;
        // Exception handler code:
        try_except_handler_29:;
        exception_keeper_type_24 = exception_type;
        exception_keeper_value_24 = exception_value;
        exception_keeper_tb_24 = exception_tb;
        exception_keeper_lineno_24 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_135 );
        locals_matplotlib$backends$backend_webagg_135 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_24;
        exception_value = exception_keeper_value_24;
        exception_tb = exception_keeper_tb_24;
        exception_lineno = exception_keeper_lineno_24;

        goto try_except_handler_28;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_28:;
        CHECK_OBJECT( (PyObject *)outline_8_var___class__ );
        Py_DECREF( outline_8_var___class__ );
        outline_8_var___class__ = NULL;

        goto outline_result_9;
        // Exception handler code:
        try_except_handler_28:;
        exception_keeper_type_25 = exception_type;
        exception_keeper_value_25 = exception_value;
        exception_keeper_tb_25 = exception_tb;
        exception_keeper_lineno_25 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_25;
        exception_value = exception_keeper_value_25;
        exception_tb = exception_keeper_tb_25;
        exception_lineno = exception_keeper_lineno_25;

        goto outline_exception_9;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_9:;
        exception_lineno = 135;
        goto try_except_handler_27;
        outline_result_9:;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_WebSocket, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_2 = "c";
            goto try_except_handler_27;
        }
        goto try_end_9;
        // Exception handler code:
        try_except_handler_27:;
        exception_keeper_type_26 = exception_type;
        exception_keeper_value_26 = exception_value;
        exception_keeper_tb_26 = exception_tb;
        exception_keeper_lineno_26 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_WebAggApplication$class_creation_6__bases_orig );
        tmp_WebAggApplication$class_creation_6__bases_orig = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_6__bases );
        tmp_WebAggApplication$class_creation_6__bases = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_6__class_decl_dict );
        tmp_WebAggApplication$class_creation_6__class_decl_dict = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_6__metaclass );
        tmp_WebAggApplication$class_creation_6__metaclass = NULL;

        Py_XDECREF( tmp_WebAggApplication$class_creation_6__prepared );
        tmp_WebAggApplication$class_creation_6__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_26;
        exception_value = exception_keeper_value_26;
        exception_tb = exception_keeper_tb_26;
        exception_lineno = exception_keeper_lineno_26;

        goto frame_exception_exit_4;
        // End of try:
        try_end_9:;
        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_6__bases_orig );
        Py_DECREF( tmp_WebAggApplication$class_creation_6__bases_orig );
        tmp_WebAggApplication$class_creation_6__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_6__bases );
        Py_DECREF( tmp_WebAggApplication$class_creation_6__bases );
        tmp_WebAggApplication$class_creation_6__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_6__class_decl_dict );
        Py_DECREF( tmp_WebAggApplication$class_creation_6__class_decl_dict );
        tmp_WebAggApplication$class_creation_6__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_6__metaclass );
        Py_DECREF( tmp_WebAggApplication$class_creation_6__metaclass );
        tmp_WebAggApplication$class_creation_6__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_WebAggApplication$class_creation_6__prepared );
        Py_DECREF( tmp_WebAggApplication$class_creation_6__prepared );
        tmp_WebAggApplication$class_creation_6__prepared = NULL;

        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_str_empty_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_16___init__( tmp_defaults_1 );

            ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = outline_2_var___class__;
            Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );


            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_55;
            PyObject *tmp_called_name_19;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_defaults_2;
            PyObject *tmp_classmethod_arg_1;
            PyObject *tmp_defaults_3;
            tmp_res = MAPPING_HAS_ITEM( locals_matplotlib$backends$backend_webagg_60, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 209;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_55 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_55 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_19;
            }
            else
            {
                goto condexpr_false_19;
            }
            condexpr_true_19:;
            tmp_called_name_19 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_classmethod );

            if ( tmp_called_name_19 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 209;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 209;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
            tmp_defaults_2 = const_tuple_str_empty_none_none_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_args_element_name_1 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_17_initialize( tmp_defaults_2 );



            frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 209;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
            }

            Py_DECREF( tmp_called_name_19 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 209;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_19;
            condexpr_false_19:;
            tmp_defaults_3 = const_tuple_str_empty_none_none_tuple;
            Py_INCREF( tmp_defaults_3 );
            tmp_classmethod_arg_1 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_17_initialize( tmp_defaults_3 );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_1 );
            Py_DECREF( tmp_classmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 209;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
            condexpr_end_19:;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_initialize, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 209;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_56;
            PyObject *tmp_called_name_20;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_classmethod_arg_2;
            tmp_res = MAPPING_HAS_ITEM( locals_matplotlib$backends$backend_webagg_60, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 254;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_56 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_56 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_20;
            }
            else
            {
                goto condexpr_false_20;
            }
            condexpr_true_20:;
            tmp_called_name_20 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_classmethod );

            if ( tmp_called_name_20 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 254;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 254;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_2 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start(  );



            frame_94d52aad8429f3f66731c14b3a262317_4->m_frame.f_lineno = 254;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
            }

            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 254;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_20;
            condexpr_false_20:;
            tmp_classmethod_arg_2 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_18_start(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_2 );
            Py_DECREF( tmp_classmethod_arg_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 254;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
            condexpr_end_20:;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain_start, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 254;
                type_description_2 = "c";
                goto frame_exception_exit_4;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_94d52aad8429f3f66731c14b3a262317_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_9;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_94d52aad8429f3f66731c14b3a262317_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_94d52aad8429f3f66731c14b3a262317_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_94d52aad8429f3f66731c14b3a262317_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_94d52aad8429f3f66731c14b3a262317_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_94d52aad8429f3f66731c14b3a262317_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_94d52aad8429f3f66731c14b3a262317_4 == cache_frame_94d52aad8429f3f66731c14b3a262317_4 )
        {
            Py_DECREF( frame_94d52aad8429f3f66731c14b3a262317_4 );
        }
        cache_frame_94d52aad8429f3f66731c14b3a262317_4 = NULL;

        assertFrameObject( frame_94d52aad8429f3f66731c14b3a262317_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_9:;
        goto skip_nested_handling_9;
        nested_frame_exit_3:;

        goto try_except_handler_11;
        skip_nested_handling_9:;
        {
            nuitka_bool tmp_condition_result_57;
            PyObject *tmp_compexpr_left_10;
            PyObject *tmp_compexpr_right_10;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_10 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_10 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_11;
            }
            tmp_condition_result_57 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_57 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_37;
            }
            else
            {
                goto branch_no_37;
            }
            branch_yes_37:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_60, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_11;
            }
            branch_no_37:;
        }
        {
            PyObject *tmp_assign_source_89;
            PyObject *tmp_called_name_21;
            PyObject *tmp_args_name_18;
            PyObject *tmp_tuple_element_36;
            PyObject *tmp_kw_name_18;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_21 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_36 = const_str_plain_WebAggApplication;
            tmp_args_name_18 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_args_name_18, 0, tmp_tuple_element_36 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_36 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_args_name_18, 1, tmp_tuple_element_36 );
            tmp_tuple_element_36 = locals_matplotlib$backends$backend_webagg_60;
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_args_name_18, 2, tmp_tuple_element_36 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_18 = tmp_class_creation_3__class_decl_dict;
            frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 60;
            tmp_assign_source_89 = CALL_FUNCTION( tmp_called_name_21, tmp_args_name_18, tmp_kw_name_18 );
            Py_DECREF( tmp_args_name_18 );
            if ( tmp_assign_source_89 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_11;
            }
            {
                PyObject *old = PyCell_GET( outline_2_var___class__ );
                PyCell_SET( outline_2_var___class__, tmp_assign_source_89 );
                Py_XDECREF( old );
            }

        }
        CHECK_OBJECT( PyCell_GET( outline_2_var___class__ ) );
        tmp_assign_source_46 = PyCell_GET( outline_2_var___class__ );
        Py_INCREF( tmp_assign_source_46 );
        goto try_return_handler_11;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_11:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_60 );
        locals_matplotlib$backends$backend_webagg_60 = NULL;
        goto try_return_handler_10;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_27 = exception_type;
        exception_keeper_value_27 = exception_value;
        exception_keeper_tb_27 = exception_tb;
        exception_keeper_lineno_27 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_60 );
        locals_matplotlib$backends$backend_webagg_60 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_27;
        exception_value = exception_keeper_value_27;
        exception_tb = exception_keeper_tb_27;
        exception_lineno = exception_keeper_lineno_27;

        goto try_except_handler_10;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_28 = exception_type;
        exception_keeper_value_28 = exception_value;
        exception_keeper_tb_28 = exception_tb;
        exception_keeper_lineno_28 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_28;
        exception_value = exception_keeper_value_28;
        exception_tb = exception_keeper_tb_28;
        exception_lineno = exception_keeper_lineno_28;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 60;
        goto try_except_handler_9;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_WebAggApplication, tmp_assign_source_46 );
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_29 = exception_type;
    exception_keeper_value_29 = exception_value;
    exception_keeper_tb_29 = exception_tb;
    exception_keeper_lineno_29 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_29;
    exception_value = exception_keeper_value_29;
    exception_tb = exception_keeper_tb_29;
    exception_lineno = exception_keeper_lineno_29;

    goto frame_exception_exit_1;
    // End of try:
    try_end_10:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    {
        PyObject *tmp_assign_source_90;
        tmp_assign_source_90 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_19_ipython_inline_display(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_ipython_inline_display, tmp_assign_source_90 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_91;
        PyObject *tmp_tuple_element_37;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain__Backend );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Backend );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Backend" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 313;

            goto try_except_handler_30;
        }

        tmp_tuple_element_37 = tmp_mvar_value_13;
        tmp_assign_source_91 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_37 );
        PyTuple_SET_ITEM( tmp_assign_source_91, 0, tmp_tuple_element_37 );
        assert( tmp_class_creation_4__bases_orig == NULL );
        tmp_class_creation_4__bases_orig = tmp_assign_source_91;
    }
    {
        PyObject *tmp_assign_source_92;
        PyObject *tmp_dircall_arg1_10;
        CHECK_OBJECT( tmp_class_creation_4__bases_orig );
        tmp_dircall_arg1_10 = tmp_class_creation_4__bases_orig;
        Py_INCREF( tmp_dircall_arg1_10 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_10};
            tmp_assign_source_92 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_92 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_30;
        }
        assert( tmp_class_creation_4__bases == NULL );
        tmp_class_creation_4__bases = tmp_assign_source_92;
    }
    {
        PyObject *tmp_assign_source_93;
        tmp_assign_source_93 = PyDict_New();
        assert( tmp_class_creation_4__class_decl_dict == NULL );
        tmp_class_creation_4__class_decl_dict = tmp_assign_source_93;
    }
    {
        PyObject *tmp_assign_source_94;
        PyObject *tmp_metaclass_name_10;
        nuitka_bool tmp_condition_result_58;
        PyObject *tmp_key_name_28;
        PyObject *tmp_dict_name_28;
        PyObject *tmp_dict_name_29;
        PyObject *tmp_key_name_29;
        nuitka_bool tmp_condition_result_59;
        int tmp_truth_name_10;
        PyObject *tmp_type_arg_19;
        PyObject *tmp_subscribed_name_10;
        PyObject *tmp_subscript_name_10;
        PyObject *tmp_bases_name_10;
        tmp_key_name_28 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_28 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_28, tmp_key_name_28 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_30;
        }
        tmp_condition_result_58 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_58 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_21;
        }
        else
        {
            goto condexpr_false_21;
        }
        condexpr_true_21:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_29 = tmp_class_creation_4__class_decl_dict;
        tmp_key_name_29 = const_str_plain_metaclass;
        tmp_metaclass_name_10 = DICT_GET_ITEM( tmp_dict_name_29, tmp_key_name_29 );
        if ( tmp_metaclass_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_30;
        }
        goto condexpr_end_21;
        condexpr_false_21:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_truth_name_10 = CHECK_IF_TRUE( tmp_class_creation_4__bases );
        if ( tmp_truth_name_10 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_30;
        }
        tmp_condition_result_59 = tmp_truth_name_10 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_59 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_22;
        }
        else
        {
            goto condexpr_false_22;
        }
        condexpr_true_22:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_subscribed_name_10 = tmp_class_creation_4__bases;
        tmp_subscript_name_10 = const_int_0;
        tmp_type_arg_19 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_10, tmp_subscript_name_10, 0 );
        if ( tmp_type_arg_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_30;
        }
        tmp_metaclass_name_10 = BUILTIN_TYPE1( tmp_type_arg_19 );
        Py_DECREF( tmp_type_arg_19 );
        if ( tmp_metaclass_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_30;
        }
        goto condexpr_end_22;
        condexpr_false_22:;
        tmp_metaclass_name_10 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_10 );
        condexpr_end_22:;
        condexpr_end_21:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_bases_name_10 = tmp_class_creation_4__bases;
        tmp_assign_source_94 = SELECT_METACLASS( tmp_metaclass_name_10, tmp_bases_name_10 );
        Py_DECREF( tmp_metaclass_name_10 );
        if ( tmp_assign_source_94 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_30;
        }
        assert( tmp_class_creation_4__metaclass == NULL );
        tmp_class_creation_4__metaclass = tmp_assign_source_94;
    }
    {
        nuitka_bool tmp_condition_result_60;
        PyObject *tmp_key_name_30;
        PyObject *tmp_dict_name_30;
        tmp_key_name_30 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_30 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_30, tmp_key_name_30 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_30;
        }
        tmp_condition_result_60 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_60 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_38;
        }
        else
        {
            goto branch_no_38;
        }
        branch_yes_38:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_4__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_30;
        }
        branch_no_38:;
    }
    {
        nuitka_bool tmp_condition_result_61;
        PyObject *tmp_source_name_53;
        CHECK_OBJECT( tmp_class_creation_4__metaclass );
        tmp_source_name_53 = tmp_class_creation_4__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_53, const_str_plain___prepare__ );
        tmp_condition_result_61 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_61 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_39;
        }
        else
        {
            goto branch_no_39;
        }
        branch_yes_39:;
        {
            PyObject *tmp_assign_source_95;
            PyObject *tmp_called_name_22;
            PyObject *tmp_source_name_54;
            PyObject *tmp_args_name_19;
            PyObject *tmp_tuple_element_38;
            PyObject *tmp_kw_name_19;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_source_name_54 = tmp_class_creation_4__metaclass;
            tmp_called_name_22 = LOOKUP_ATTRIBUTE( tmp_source_name_54, const_str_plain___prepare__ );
            if ( tmp_called_name_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;

                goto try_except_handler_30;
            }
            tmp_tuple_element_38 = const_str_plain__BackendWebAgg;
            tmp_args_name_19 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_38 );
            PyTuple_SET_ITEM( tmp_args_name_19, 0, tmp_tuple_element_38 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_38 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_38 );
            PyTuple_SET_ITEM( tmp_args_name_19, 1, tmp_tuple_element_38 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_19 = tmp_class_creation_4__class_decl_dict;
            frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 313;
            tmp_assign_source_95 = CALL_FUNCTION( tmp_called_name_22, tmp_args_name_19, tmp_kw_name_19 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_name_19 );
            if ( tmp_assign_source_95 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;

                goto try_except_handler_30;
            }
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_95;
        }
        {
            nuitka_bool tmp_condition_result_62;
            PyObject *tmp_operand_name_10;
            PyObject *tmp_source_name_55;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_source_name_55 = tmp_class_creation_4__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_55, const_str_plain___getitem__ );
            tmp_operand_name_10 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_10 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;

                goto try_except_handler_30;
            }
            tmp_condition_result_62 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_62 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_40;
            }
            else
            {
                goto branch_no_40;
            }
            branch_yes_40:;
            {
                PyObject *tmp_raise_type_11;
                PyObject *tmp_raise_value_10;
                PyObject *tmp_left_name_10;
                PyObject *tmp_right_name_10;
                PyObject *tmp_tuple_element_39;
                PyObject *tmp_getattr_target_10;
                PyObject *tmp_getattr_attr_10;
                PyObject *tmp_getattr_default_10;
                PyObject *tmp_source_name_56;
                PyObject *tmp_type_arg_20;
                tmp_raise_type_11 = PyExc_TypeError;
                tmp_left_name_10 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_4__metaclass );
                tmp_getattr_target_10 = tmp_class_creation_4__metaclass;
                tmp_getattr_attr_10 = const_str_plain___name__;
                tmp_getattr_default_10 = const_str_angle_metaclass;
                tmp_tuple_element_39 = BUILTIN_GETATTR( tmp_getattr_target_10, tmp_getattr_attr_10, tmp_getattr_default_10 );
                if ( tmp_tuple_element_39 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 313;

                    goto try_except_handler_30;
                }
                tmp_right_name_10 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_10, 0, tmp_tuple_element_39 );
                CHECK_OBJECT( tmp_class_creation_4__prepared );
                tmp_type_arg_20 = tmp_class_creation_4__prepared;
                tmp_source_name_56 = BUILTIN_TYPE1( tmp_type_arg_20 );
                assert( !(tmp_source_name_56 == NULL) );
                tmp_tuple_element_39 = LOOKUP_ATTRIBUTE( tmp_source_name_56, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_56 );
                if ( tmp_tuple_element_39 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_10 );

                    exception_lineno = 313;

                    goto try_except_handler_30;
                }
                PyTuple_SET_ITEM( tmp_right_name_10, 1, tmp_tuple_element_39 );
                tmp_raise_value_10 = BINARY_OPERATION_REMAINDER( tmp_left_name_10, tmp_right_name_10 );
                Py_DECREF( tmp_right_name_10 );
                if ( tmp_raise_value_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 313;

                    goto try_except_handler_30;
                }
                exception_type = tmp_raise_type_11;
                Py_INCREF( tmp_raise_type_11 );
                exception_value = tmp_raise_value_10;
                exception_lineno = 313;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_30;
            }
            branch_no_40:;
        }
        goto branch_end_39;
        branch_no_39:;
        {
            PyObject *tmp_assign_source_96;
            tmp_assign_source_96 = PyDict_New();
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_96;
        }
        branch_end_39:;
    }
    {
        PyObject *tmp_assign_source_97;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain__Backend );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Backend );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Backend" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 312;

            goto try_except_handler_30;
        }

        tmp_called_instance_1 = tmp_mvar_value_14;
        {
            PyObject *tmp_set_locals_10;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_set_locals_10 = tmp_class_creation_4__prepared;
            locals_matplotlib$backends$backend_webagg_313 = tmp_set_locals_10;
            Py_INCREF( tmp_set_locals_10 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_526e1fe88ddc5c56efb2459c1633b489;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_32;
        }
        tmp_dictset_value = const_str_plain__BackendWebAgg;
        tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;

            goto try_except_handler_32;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_7511e4691622db519fa9a0190d02ddea_11, codeobj_7511e4691622db519fa9a0190d02ddea, module_matplotlib$backends$backend_webagg, sizeof(void *) );
        frame_7511e4691622db519fa9a0190d02ddea_11 = cache_frame_7511e4691622db519fa9a0190d02ddea_11;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_7511e4691622db519fa9a0190d02ddea_11 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_7511e4691622db519fa9a0190d02ddea_11 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_mvar_value_15;
            tmp_dictset_value = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain_FigureCanvasWebAgg );

            if ( tmp_dictset_value == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_FigureCanvasWebAgg );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FigureCanvasWebAgg );
                }

                if ( tmp_mvar_value_15 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FigureCanvasWebAgg" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 314;
                    type_description_2 = "o";
                    goto frame_exception_exit_11;
                }

                tmp_dictset_value = tmp_mvar_value_15;
                Py_INCREF( tmp_dictset_value );
                }
            }

            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain_FigureCanvas, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 314;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
        }
        {
            PyObject *tmp_source_name_57;
            PyObject *tmp_mvar_value_16;
            tmp_source_name_57 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain_core );

            if ( tmp_source_name_57 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain_core );

                if (unlikely( tmp_mvar_value_16 == NULL ))
                {
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_core );
                }

                if ( tmp_mvar_value_16 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "core" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 315;
                    type_description_2 = "o";
                    goto frame_exception_exit_11;
                }

                tmp_source_name_57 = tmp_mvar_value_16;
                Py_INCREF( tmp_source_name_57 );
                }
            }

            tmp_dictset_value = LOOKUP_ATTRIBUTE( tmp_source_name_57, const_str_plain_FigureManagerWebAgg );
            Py_DECREF( tmp_source_name_57 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 315;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain_FigureManager, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 315;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
        }
        {
            nuitka_bool tmp_condition_result_63;
            PyObject *tmp_called_name_23;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_staticmethod_arg_1;
            tmp_res = MAPPING_HAS_ITEM( locals_matplotlib$backends$backend_webagg_313, const_str_plain_staticmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            tmp_condition_result_63 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_63 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_23;
            }
            else
            {
                goto condexpr_false_23;
            }
            condexpr_true_23:;
            tmp_called_name_23 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain_staticmethod );

            if ( tmp_called_name_23 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "staticmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 317;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }

            if ( tmp_called_name_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            tmp_args_element_name_4 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_20_trigger_manager_draw(  );



            frame_7511e4691622db519fa9a0190d02ddea_11->m_frame.f_lineno = 317;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
            }

            Py_DECREF( tmp_called_name_23 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            goto condexpr_end_23;
            condexpr_false_23:;
            tmp_staticmethod_arg_1 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_20_trigger_manager_draw(  );



            tmp_dictset_value = BUILTIN_STATICMETHOD( tmp_staticmethod_arg_1 );
            Py_DECREF( tmp_staticmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            condexpr_end_23:;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain_trigger_manager_draw, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
        }
        {
            nuitka_bool tmp_condition_result_64;
            PyObject *tmp_called_name_24;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_staticmethod_arg_2;
            tmp_res = MAPPING_HAS_ITEM( locals_matplotlib$backends$backend_webagg_313, const_str_plain_staticmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            tmp_condition_result_64 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_64 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_24;
            }
            else
            {
                goto condexpr_false_24;
            }
            condexpr_true_24:;
            tmp_called_name_24 = PyObject_GetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain_staticmethod );

            if ( tmp_called_name_24 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "staticmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 321;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }

            if ( tmp_called_name_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            tmp_args_element_name_5 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_21_show(  );



            frame_7511e4691622db519fa9a0190d02ddea_11->m_frame.f_lineno = 321;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, call_args );
            }

            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            goto condexpr_end_24;
            condexpr_false_24:;
            tmp_staticmethod_arg_2 = MAKE_FUNCTION_matplotlib$backends$backend_webagg$$$function_21_show(  );



            tmp_dictset_value = BUILTIN_STATICMETHOD( tmp_staticmethod_arg_2 );
            Py_DECREF( tmp_staticmethod_arg_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            condexpr_end_24:;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain_show, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_7511e4691622db519fa9a0190d02ddea_11 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_10;

        frame_exception_exit_11:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_7511e4691622db519fa9a0190d02ddea_11 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_7511e4691622db519fa9a0190d02ddea_11, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_7511e4691622db519fa9a0190d02ddea_11->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_7511e4691622db519fa9a0190d02ddea_11, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_7511e4691622db519fa9a0190d02ddea_11,
            type_description_2,
            outline_9_var___class__
        );


        // Release cached frame.
        if ( frame_7511e4691622db519fa9a0190d02ddea_11 == cache_frame_7511e4691622db519fa9a0190d02ddea_11 )
        {
            Py_DECREF( frame_7511e4691622db519fa9a0190d02ddea_11 );
        }
        cache_frame_7511e4691622db519fa9a0190d02ddea_11 = NULL;

        assertFrameObject( frame_7511e4691622db519fa9a0190d02ddea_11 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_10;

        frame_no_exception_10:;
        goto skip_nested_handling_10;
        nested_frame_exit_10:;

        goto try_except_handler_32;
        skip_nested_handling_10:;
        {
            nuitka_bool tmp_condition_result_65;
            PyObject *tmp_compexpr_left_11;
            PyObject *tmp_compexpr_right_11;
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_compexpr_left_11 = tmp_class_creation_4__bases;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_compexpr_right_11 = tmp_class_creation_4__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;

                goto try_except_handler_32;
            }
            tmp_condition_result_65 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_65 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_41;
            }
            else
            {
                goto branch_no_41;
            }
            branch_yes_41:;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_dictset_value = tmp_class_creation_4__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$backends$backend_webagg_313, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;

                goto try_except_handler_32;
            }
            branch_no_41:;
        }
        {
            PyObject *tmp_assign_source_98;
            PyObject *tmp_called_name_25;
            PyObject *tmp_args_name_20;
            PyObject *tmp_tuple_element_40;
            PyObject *tmp_kw_name_20;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_called_name_25 = tmp_class_creation_4__metaclass;
            tmp_tuple_element_40 = const_str_plain__BackendWebAgg;
            tmp_args_name_20 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_40 );
            PyTuple_SET_ITEM( tmp_args_name_20, 0, tmp_tuple_element_40 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_40 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_40 );
            PyTuple_SET_ITEM( tmp_args_name_20, 1, tmp_tuple_element_40 );
            tmp_tuple_element_40 = locals_matplotlib$backends$backend_webagg_313;
            Py_INCREF( tmp_tuple_element_40 );
            PyTuple_SET_ITEM( tmp_args_name_20, 2, tmp_tuple_element_40 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_20 = tmp_class_creation_4__class_decl_dict;
            frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 313;
            tmp_assign_source_98 = CALL_FUNCTION( tmp_called_name_25, tmp_args_name_20, tmp_kw_name_20 );
            Py_DECREF( tmp_args_name_20 );
            if ( tmp_assign_source_98 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;

                goto try_except_handler_32;
            }
            assert( outline_9_var___class__ == NULL );
            outline_9_var___class__ = tmp_assign_source_98;
        }
        CHECK_OBJECT( outline_9_var___class__ );
        tmp_args_element_name_3 = outline_9_var___class__;
        Py_INCREF( tmp_args_element_name_3 );
        goto try_return_handler_32;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_32:;
        Py_DECREF( locals_matplotlib$backends$backend_webagg_313 );
        locals_matplotlib$backends$backend_webagg_313 = NULL;
        goto try_return_handler_31;
        // Exception handler code:
        try_except_handler_32:;
        exception_keeper_type_30 = exception_type;
        exception_keeper_value_30 = exception_value;
        exception_keeper_tb_30 = exception_tb;
        exception_keeper_lineno_30 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$backends$backend_webagg_313 );
        locals_matplotlib$backends$backend_webagg_313 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_30;
        exception_value = exception_keeper_value_30;
        exception_tb = exception_keeper_tb_30;
        exception_lineno = exception_keeper_lineno_30;

        goto try_except_handler_31;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_31:;
        CHECK_OBJECT( (PyObject *)outline_9_var___class__ );
        Py_DECREF( outline_9_var___class__ );
        outline_9_var___class__ = NULL;

        goto outline_result_10;
        // Exception handler code:
        try_except_handler_31:;
        exception_keeper_type_31 = exception_type;
        exception_keeper_value_31 = exception_value;
        exception_keeper_tb_31 = exception_tb;
        exception_keeper_lineno_31 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_31;
        exception_value = exception_keeper_value_31;
        exception_tb = exception_keeper_tb_31;
        exception_lineno = exception_keeper_lineno_31;

        goto outline_exception_10;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$backends$backend_webagg );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_10:;
        exception_lineno = 313;
        goto try_except_handler_30;
        outline_result_10:;
        frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame.f_lineno = 312;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_97 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_export, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_97 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;

            goto try_except_handler_30;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$backends$backend_webagg, (Nuitka_StringObject *)const_str_plain__BackendWebAgg, tmp_assign_source_97 );
    }
    goto try_end_11;
    // Exception handler code:
    try_except_handler_30:;
    exception_keeper_type_32 = exception_type;
    exception_keeper_value_32 = exception_value;
    exception_keeper_tb_32 = exception_tb;
    exception_keeper_lineno_32 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    Py_XDECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_32;
    exception_value = exception_keeper_value_32;
    exception_tb = exception_keeper_tb_32;
    exception_lineno = exception_keeper_lineno_32;

    goto frame_exception_exit_1;
    // End of try:
    try_end_11:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a6bc9fc9cbcc7aad54ab5534e23a973a );
#endif
    popFrameStack();

    assertFrameObject( frame_a6bc9fc9cbcc7aad54ab5534e23a973a );

    goto frame_no_exception_11;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a6bc9fc9cbcc7aad54ab5534e23a973a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a6bc9fc9cbcc7aad54ab5534e23a973a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a6bc9fc9cbcc7aad54ab5534e23a973a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a6bc9fc9cbcc7aad54ab5534e23a973a, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_11:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases_orig );
    Py_DECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases );
    Py_DECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__class_decl_dict );
    Py_DECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__metaclass );
    Py_DECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__prepared );
    Py_DECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;


    return MOD_RETURN_VALUE( module_matplotlib$backends$backend_webagg );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
