/* Generated code for Python module 'cffi.cffi_opcode'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_cffi$cffi_opcode" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_cffi$cffi_opcode;
PyDictObject *moduledict_cffi$cffi_opcode;

/* The declarations of module constants used, if any. */
static PyObject *const_str_plain_ptrdiff_t;
extern PyObject *const_str_plain_op;
extern PyObject *const_int_pos_12;
static PyObject *const_str_plain_F_UNION;
extern PyObject *const_int_pos_20;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain_PRIM_INT_LEAST64;
extern PyObject *const_str_plain_OP_FUNCTION;
extern PyObject *const_int_pos_24;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_plain_PRIM_FLOATCOMPLEX;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_OP_OPEN_ARRAY;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_OP_STRUCT_UNION;
extern PyObject *const_int_pos_22;
static PyObject *const_str_plain_PRIM_INT_LEAST8;
static PyObject *const_str_plain_PRIM_INT_LEAST16;
extern PyObject *const_int_pos_9;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_PRIM_UINT;
extern PyObject *const_str_plain_arg;
extern PyObject *const_int_pos_42;
static PyObject *const_str_plain_PRIM_INT_FAST64;
static PyObject *const_str_plain__UNKNOWN_LONG_DOUBLE;
static PyObject *const_str_digest_ea9b4f812bfcd8150ff586ab51ffa83a;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain__name;
extern PyObject *const_int_pos_11;
static PyObject *const_str_plain_OP_TYPENAME;
static PyObject *const_str_plain_PRIM_DOUBLECOMPLEX;
static PyObject *const_str_plain_PRIM_UINT_LEAST32;
extern PyObject *const_tuple_str_plain_num_tuple;
extern PyObject *const_str_plain_double;
extern PyObject *const_str_plain_OP_GLOBAL_VAR;
static PyObject *const_str_plain_OP_;
static PyObject *const_str_plain_PRIM_USHORT;
extern PyObject *const_int_pos_39;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_items;
extern PyObject *const_str_plain_VerificationError;
extern PyObject *const_int_pos_19;
static PyObject *const_str_plain_PRIM_FLOAT;
extern PyObject *const_str_plain_num;
extern PyObject *const_int_pos_23;
extern PyObject *const_int_pos_31;
static PyObject *const_str_plain_PRIM_LONG;
extern PyObject *const_int_pos_17;
extern PyObject *const_str_plain_size_t;
extern PyObject *const_int_pos_6;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_OP_POINTER;
extern PyObject *const_str_plain__Bool;
extern PyObject *const_int_pos_25;
static PyObject *const_str_plain_F_OPAQUE;
extern PyObject *const_int_pos_27;
extern PyObject *const_int_pos_52;
static PyObject *const_str_digest_7cae98d7c18be7e7451e4f9c1636e913;
static PyObject *const_str_plain_PRIM_UINT_FAST32;
extern PyObject *const_slice_int_pos_3_none_none;
extern PyObject *const_int_pos_5;
static PyObject *const_str_digest_a199b01cf85ae649d8d5249bb63e5635;
extern PyObject *const_str_plain_uintptr_t;
extern PyObject *const_int_pos_41;
static PyObject *const_str_plain_PRIM_UCHAR;
extern PyObject *const_str_plain_OP_NOOP;
extern PyObject *const_str_plain_char16_t;
static PyObject *const_str_plain_int32_t;
static PyObject *const_str_plain_int64_t;
static PyObject *const_str_plain_int16_t;
static PyObject *const_str_plain__UNKNOWN_FLOAT_PRIM;
extern PyObject *const_int_pos_16;
extern PyObject *const_int_pos_33;
extern PyObject *const_str_plain_PRIM_UINT32;
extern PyObject *const_int_pos_30;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_plain_PRIM_UINT_LEAST16;
extern PyObject *const_str_plain__value;
static PyObject *const_str_digest_52c6c5cc5fa5960f8d48b84cb181cfbe;
extern PyObject *const_int_pos_32;
static PyObject *const_str_digest_531f769303ec4566f610e39d187e791f;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_plain_int8_t;
extern PyObject *const_str_plain_as_python_bytes;
static PyObject *const_str_plain_PRIM_ULONG;
extern PyObject *const_str_plain_str;
static PyObject *const_str_plain_PRIM_INT;
extern PyObject *const_str_plain_as_c_expr;
static PyObject *const_str_plain_PRIM_INTMAX;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_format_four_bytes;
static PyObject *const_str_plain_PRIM_BOOL;
extern PyObject *const_int_neg_2;
extern PyObject *const_str_plain_long;
extern PyObject *const_str_plain_isdigit;
static PyObject *const_str_digest_40527726347e207cd2c2efc6de6163be;
extern PyObject *const_int_pos_38;
static PyObject *const_str_plain_PRIM_UINT_FAST16;
extern PyObject *const_str_plain_int;
static PyObject *const_str_plain__UNKNOWN_PRIM;
extern PyObject *const_str_plain_OP_DLOPEN_FUNC;
static PyObject *const_str_plain_int_least64_t;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_float;
static PyObject *const_str_plain_uint64_t;
static PyObject *const_str_digest_6bf3055530e3f17a8db26b23f339f170;
extern PyObject *const_int_pos_8;
static PyObject *const_str_plain_uint_least8_t;
static PyObject *const_str_plain_PRIM_CHAR32;
extern PyObject *const_str_digest_b985875eb32c9998d65889b2c366bd29;
extern PyObject *const_str_plain_OP_EXTERN_PYTHON;
extern PyObject *const_tuple_empty;
extern PyObject *const_tuple_str_plain_self_str_plain_value_tuple;
extern PyObject *const_int_pos_34;
extern PyObject *const_str_plain_CffiOp;
extern PyObject *const_int_pos_48;
static PyObject *const_str_plain_PRIM_DOUBLE;
static PyObject *const_str_plain_PRIM_UINT_LEAST8;
extern PyObject *const_str_plain_PRIM_UINT64;
extern PyObject *const_int_pos_10;
static PyObject *const_str_plain_intptr_t;
static PyObject *const_str_plain_PRIM_INT_LEAST32;
static PyObject *const_str_plain_PRIM_CHAR;
static PyObject *const_str_plain_int_fast64_t;
extern PyObject *const_str_plain_PRIM_UINT8;
extern PyObject *const_str_plain_error;
static PyObject *const_str_plain_PRIM_UINTMAX;
static PyObject *const_str_plain_uint_fast64_t;
static PyObject *const_str_plain_uint16_t;
extern PyObject *const_str_plain_PRIM_UINT16;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_char;
extern PyObject *const_int_pos_46;
extern PyObject *const_str_plain_PRIM_INT8;
extern PyObject *const_int_pos_26;
static PyObject *const_str_plain_PRIM_UINT_FAST8;
static PyObject *const_str_plain_PRIM_WCHAR;
extern PyObject *const_str_digest_902703cdcced1b2f97d32b33c70b1358;
extern PyObject *const_str_plain_classname;
extern PyObject *const_int_0;
static PyObject *const_str_digest_c9646a8f11d26365a78cd7f9af36786d;
extern PyObject *const_str_plain_OP_PRIMITIVE;
static PyObject *const_str_plain_PRIM_ULONGLONG;
static PyObject *const_str_plain__NUM_PRIM;
extern PyObject *const_str_digest_054eabad1e66af9ef84d8e439327b578;
extern PyObject *const_int_pos_15;
static PyObject *const_str_plain_uint_fast32_t;
extern PyObject *const_int_pos_21;
extern PyObject *const_int_pos_18;
static PyObject *const_tuple_str_plain__key_tuple;
static PyObject *const_int_pos_51;
extern PyObject *const_str_plain__key;
extern PyObject *const_tuple_str_plain_VerificationError_tuple;
extern PyObject *const_int_pos_37;
static PyObject *const_str_plain_int_fast16_t;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_PRIM_SCHAR;
extern PyObject *const_str_plain_OP_ARRAY;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_plain_PRIM_INTPTR;
static PyObject *const_str_plain__IO_FILE_STRUCT;
extern PyObject *const_str_digest_92dfa96e420126c66052ca0989a42b38;
static PyObject *const_str_plain_uint_least16_t;
static PyObject *const_str_plain_intmax_t;
static PyObject *const_str_plain_PRIM_SSIZE;
static PyObject *const_str_plain_PRIM_INT_FAST16;
extern PyObject *const_str_angle_listcomp;
static PyObject *const_str_plain__CFFI_;
static PyObject *const_str_plain_ssize_t;
static PyObject *const_str_plain_PRIM_PTRDIFF;
extern PyObject *const_int_pos_36;
extern PyObject *const_int_pos_4;
static PyObject *const_str_plain_PRIM_CHAR16;
extern PyObject *const_str_plain_G_FLAGS;
extern PyObject *const_int_pos_50;
extern PyObject *const_int_neg_3;
static PyObject *const_str_digest_29ebfd2e1c80292c48e8d899d54008d4;
static PyObject *const_str_plain_PRIM_INT_FAST8;
extern PyObject *const_int_pos_45;
static PyObject *const_tuple_846c2542d58a56b722dde5dd5723159c_tuple;
extern PyObject *const_str_plain_type;
extern PyObject *const_int_pos_13;
static PyObject *const_str_plain_PRIM_INT_FAST32;
extern PyObject *const_int_pos_47;
static PyObject *const_str_digest_1467190cb80aea848f68f8b7aafba358;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_40a9bc3d25971efe24fea0d3f90d4bde;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_plain_int_least32_t;
static PyObject *const_str_plain_PRIM_LONGDOUBLE;
static PyObject *const_str_plain_F_PACKED;
static PyObject *const_str_plain_uint_fast16_t;
static PyObject *const_str_plain_uintmax_t;
extern PyObject *const_str_plain_PRIM_VOID;
extern PyObject *const_str_plain_PRIM_INT32;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain___str__;
extern PyObject *const_int_pos_44;
static PyObject *const_str_plain_int_fast8_t;
extern PyObject *const_str_plain_PRIM_INT64;
static PyObject *const_str_plain_uint_fast8_t;
static PyObject *const_str_plain_PRIM_SIZE;
static PyObject *const_tuple_str_plain_self_str_plain_op_str_plain_arg_tuple;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain_int_least8_t;
extern PyObject *const_str_plain_OP_CPYTHON_BLTN_O;
static PyObject *const_str_digest_34d4327b4ade916eb42f8474ad8232e7;
static PyObject *const_str_digest_0f0c93e8d064b7bf26500d833b5d60de;
extern PyObject *const_str_plain_PRIM_INT16;
extern PyObject *const_str_plain_OP_CONSTANT;
static PyObject *const_str_plain_PRIM_LONGLONG;
static PyObject *const_str_plain_uint_least32_t;
static PyObject *const_str_digest_526ea772325382dcbe2f1265eae53384;
extern PyObject *const_str_plain_short;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_OP_CONSTANT_INT;
extern PyObject *const_int_pos_2147483648;
extern PyObject *const_str_plain_OP_CPYTHON_BLTN_N;
static PyObject *const_str_plain_PRIM_UINT_FAST64;
extern PyObject *const_str_plain_OP_GLOBAL_VAR_F;
extern PyObject *const_int_pos_255;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_PRIM_UINTPTR;
static PyObject *const_str_plain_PRIM_SHORT;
extern PyObject *const_str_digest_61eb47d02a6bf21baa1afce40f67ac8b;
extern PyObject *const_str_plain_PRIMITIVE_TO_INDEX;
static PyObject *const_tuple_str_plain_OP__tuple;
extern PyObject *const_str_plain_wchar_t;
extern PyObject *const_int_pos_40;
static PyObject *const_str_plain_F_CHECK_FIELDS;
static PyObject *const_str_digest_defbc8e84ca3d92d4d0d24789267d167;
extern PyObject *const_str_plain_OP_ENUM;
extern PyObject *const_int_pos_7;
extern PyObject *const_int_pos_28;
static PyObject *const_str_plain_uint_least64_t;
static PyObject *const_str_plain_uint32_t;
extern PyObject *const_str_plain_get;
static PyObject *const_str_plain_CLASS_NAME;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_35;
static PyObject *const_str_digest_761aa052032797f72215e71185b344fb;
extern PyObject *const_int_pos_2;
static PyObject *const_str_plain_PRIM_UINT_LEAST64;
static PyObject *const_str_plain_uint8_t;
extern PyObject *const_int_pos_29;
static PyObject *const_tuple_str_plain_self_str_plain_classname_tuple;
extern PyObject *const_str_plain_OP_FUNCTION_END;
extern PyObject *const_str_plain_OP_BITFIELD;
extern PyObject *const_str_plain_char32_t;
static PyObject *const_str_plain_F_EXTERNAL;
extern PyObject *const_int_pos_49;
extern PyObject *const_str_plain_startswith;
extern PyObject *const_str_plain_OP_CPYTHON_BLTN_V;
static PyObject *const_str_plain_int_fast32_t;
extern PyObject *const_str_plain_OP_DLOPEN_CONST;
extern PyObject *const_int_pos_43;
extern PyObject *const_int_pos_14;
static PyObject *const_str_digest_713f1d26e22b4e7d29609c4f71e00deb;
static PyObject *const_str_digest_ec33135e1884a3a1c75406a04061265c;
static PyObject *const_str_plain_int_least16_t;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_ptrdiff_t = UNSTREAM_STRING_ASCII( &constant_bin[ 194573 ], 9, 1 );
    const_str_plain_F_UNION = UNSTREAM_STRING_ASCII( &constant_bin[ 194582 ], 7, 1 );
    const_str_plain_PRIM_INT_LEAST64 = UNSTREAM_STRING_ASCII( &constant_bin[ 194589 ], 16, 1 );
    const_str_plain_PRIM_FLOATCOMPLEX = UNSTREAM_STRING_ASCII( &constant_bin[ 194605 ], 17, 1 );
    const_str_plain_PRIM_INT_LEAST8 = UNSTREAM_STRING_ASCII( &constant_bin[ 194622 ], 15, 1 );
    const_str_plain_PRIM_INT_LEAST16 = UNSTREAM_STRING_ASCII( &constant_bin[ 194637 ], 16, 1 );
    const_str_plain_PRIM_UINT = UNSTREAM_STRING_ASCII( &constant_bin[ 194653 ], 9, 1 );
    const_str_plain_PRIM_INT_FAST64 = UNSTREAM_STRING_ASCII( &constant_bin[ 194662 ], 15, 1 );
    const_str_plain__UNKNOWN_LONG_DOUBLE = UNSTREAM_STRING_ASCII( &constant_bin[ 194677 ], 20, 1 );
    const_str_digest_ea9b4f812bfcd8150ff586ab51ffa83a = UNSTREAM_STRING_ASCII( &constant_bin[ 194697 ], 15, 0 );
    const_str_plain_OP_TYPENAME = UNSTREAM_STRING_ASCII( &constant_bin[ 194712 ], 11, 1 );
    const_str_plain_PRIM_DOUBLECOMPLEX = UNSTREAM_STRING_ASCII( &constant_bin[ 194723 ], 18, 1 );
    const_str_plain_PRIM_UINT_LEAST32 = UNSTREAM_STRING_ASCII( &constant_bin[ 194741 ], 17, 1 );
    const_str_plain_OP_ = UNSTREAM_STRING_ASCII( &constant_bin[ 19953 ], 3, 1 );
    const_str_plain_PRIM_USHORT = UNSTREAM_STRING_ASCII( &constant_bin[ 194758 ], 11, 1 );
    const_str_plain_PRIM_FLOAT = UNSTREAM_STRING_ASCII( &constant_bin[ 194605 ], 10, 1 );
    const_str_plain_PRIM_LONG = UNSTREAM_STRING_ASCII( &constant_bin[ 194769 ], 9, 1 );
    const_str_plain_F_OPAQUE = UNSTREAM_STRING_ASCII( &constant_bin[ 194778 ], 8, 1 );
    const_str_digest_7cae98d7c18be7e7451e4f9c1636e913 = UNSTREAM_STRING_ASCII( &constant_bin[ 194786 ], 15, 0 );
    const_str_plain_PRIM_UINT_FAST32 = UNSTREAM_STRING_ASCII( &constant_bin[ 194801 ], 16, 1 );
    const_str_digest_a199b01cf85ae649d8d5249bb63e5635 = UNSTREAM_STRING_ASCII( &constant_bin[ 194817 ], 14, 0 );
    const_str_plain_PRIM_UCHAR = UNSTREAM_STRING_ASCII( &constant_bin[ 194831 ], 10, 1 );
    const_str_plain_int32_t = UNSTREAM_STRING_ASCII( &constant_bin[ 194841 ], 7, 1 );
    const_str_plain_int64_t = UNSTREAM_STRING_ASCII( &constant_bin[ 194848 ], 7, 1 );
    const_str_plain_int16_t = UNSTREAM_STRING_ASCII( &constant_bin[ 194855 ], 7, 1 );
    const_str_plain__UNKNOWN_FLOAT_PRIM = UNSTREAM_STRING_ASCII( &constant_bin[ 194862 ], 19, 1 );
    const_str_plain_PRIM_UINT_LEAST16 = UNSTREAM_STRING_ASCII( &constant_bin[ 194881 ], 17, 1 );
    const_str_digest_52c6c5cc5fa5960f8d48b84cb181cfbe = UNSTREAM_STRING_ASCII( &constant_bin[ 194898 ], 19, 0 );
    const_str_digest_531f769303ec4566f610e39d187e791f = UNSTREAM_STRING_ASCII( &constant_bin[ 194917 ], 20, 0 );
    const_str_plain_int8_t = UNSTREAM_STRING_ASCII( &constant_bin[ 194937 ], 6, 1 );
    const_str_plain_PRIM_ULONG = UNSTREAM_STRING_ASCII( &constant_bin[ 194943 ], 10, 1 );
    const_str_plain_PRIM_INT = UNSTREAM_STRING_ASCII( &constant_bin[ 194589 ], 8, 1 );
    const_str_plain_PRIM_INTMAX = UNSTREAM_STRING_ASCII( &constant_bin[ 194953 ], 11, 1 );
    const_str_plain_PRIM_BOOL = UNSTREAM_STRING_ASCII( &constant_bin[ 194964 ], 9, 1 );
    const_str_digest_40527726347e207cd2c2efc6de6163be = UNSTREAM_STRING_ASCII( &constant_bin[ 194973 ], 16, 0 );
    const_str_plain_PRIM_UINT_FAST16 = UNSTREAM_STRING_ASCII( &constant_bin[ 194989 ], 16, 1 );
    const_str_plain__UNKNOWN_PRIM = UNSTREAM_STRING_ASCII( &constant_bin[ 195005 ], 13, 1 );
    const_str_plain_int_least64_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195018 ], 13, 1 );
    const_str_plain_uint64_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195031 ], 8, 1 );
    const_str_digest_6bf3055530e3f17a8db26b23f339f170 = UNSTREAM_STRING_ASCII( &constant_bin[ 195039 ], 18, 0 );
    const_str_plain_uint_least8_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195057 ], 13, 1 );
    const_str_plain_PRIM_CHAR32 = UNSTREAM_STRING_ASCII( &constant_bin[ 195070 ], 11, 1 );
    const_str_plain_PRIM_DOUBLE = UNSTREAM_STRING_ASCII( &constant_bin[ 194723 ], 11, 1 );
    const_str_plain_PRIM_UINT_LEAST8 = UNSTREAM_STRING_ASCII( &constant_bin[ 195081 ], 16, 1 );
    const_str_plain_intptr_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195097 ], 8, 1 );
    const_str_plain_PRIM_INT_LEAST32 = UNSTREAM_STRING_ASCII( &constant_bin[ 195105 ], 16, 1 );
    const_str_plain_PRIM_CHAR = UNSTREAM_STRING_ASCII( &constant_bin[ 195070 ], 9, 1 );
    const_str_plain_int_fast64_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195121 ], 12, 1 );
    const_str_plain_PRIM_UINTMAX = UNSTREAM_STRING_ASCII( &constant_bin[ 195133 ], 12, 1 );
    const_str_plain_uint_fast64_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195145 ], 13, 1 );
    const_str_plain_uint16_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195158 ], 8, 1 );
    const_str_plain_PRIM_UINT_FAST8 = UNSTREAM_STRING_ASCII( &constant_bin[ 195166 ], 15, 1 );
    const_str_plain_PRIM_WCHAR = UNSTREAM_STRING_ASCII( &constant_bin[ 195181 ], 10, 1 );
    const_str_digest_c9646a8f11d26365a78cd7f9af36786d = UNSTREAM_STRING_ASCII( &constant_bin[ 195191 ], 25, 0 );
    const_str_plain_PRIM_ULONGLONG = UNSTREAM_STRING_ASCII( &constant_bin[ 195216 ], 14, 1 );
    const_str_plain__NUM_PRIM = UNSTREAM_STRING_ASCII( &constant_bin[ 195230 ], 9, 1 );
    const_str_plain_uint_fast32_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195239 ], 13, 1 );
    const_tuple_str_plain__key_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__key_tuple, 0, const_str_plain__key ); Py_INCREF( const_str_plain__key );
    const_int_pos_51 = PyLong_FromUnsignedLong( 51ul );
    const_str_plain_int_fast16_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195252 ], 12, 1 );
    const_str_plain_PRIM_SCHAR = UNSTREAM_STRING_ASCII( &constant_bin[ 195264 ], 10, 1 );
    const_str_plain_PRIM_INTPTR = UNSTREAM_STRING_ASCII( &constant_bin[ 195274 ], 11, 1 );
    const_str_plain__IO_FILE_STRUCT = UNSTREAM_STRING_ASCII( &constant_bin[ 195285 ], 15, 1 );
    const_str_plain_uint_least16_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195300 ], 14, 1 );
    const_str_plain_intmax_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195314 ], 8, 1 );
    const_str_plain_PRIM_SSIZE = UNSTREAM_STRING_ASCII( &constant_bin[ 195322 ], 10, 1 );
    const_str_plain_PRIM_INT_FAST16 = UNSTREAM_STRING_ASCII( &constant_bin[ 195332 ], 15, 1 );
    const_str_plain__CFFI_ = UNSTREAM_STRING_ASCII( &constant_bin[ 36154 ], 6, 1 );
    const_str_plain_ssize_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195347 ], 7, 1 );
    const_str_plain_PRIM_PTRDIFF = UNSTREAM_STRING_ASCII( &constant_bin[ 195354 ], 12, 1 );
    const_str_plain_PRIM_CHAR16 = UNSTREAM_STRING_ASCII( &constant_bin[ 195366 ], 11, 1 );
    const_str_digest_29ebfd2e1c80292c48e8d899d54008d4 = UNSTREAM_STRING_ASCII( &constant_bin[ 195377 ], 22, 0 );
    const_str_plain_PRIM_INT_FAST8 = UNSTREAM_STRING_ASCII( &constant_bin[ 195399 ], 14, 1 );
    const_tuple_846c2542d58a56b722dde5dd5723159c_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_846c2542d58a56b722dde5dd5723159c_tuple, 0, const_str_plain_F_UNION ); Py_INCREF( const_str_plain_F_UNION );
    const_str_plain_F_CHECK_FIELDS = UNSTREAM_STRING_ASCII( &constant_bin[ 195413 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_846c2542d58a56b722dde5dd5723159c_tuple, 1, const_str_plain_F_CHECK_FIELDS ); Py_INCREF( const_str_plain_F_CHECK_FIELDS );
    const_str_plain_F_PACKED = UNSTREAM_STRING_ASCII( &constant_bin[ 195427 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_846c2542d58a56b722dde5dd5723159c_tuple, 2, const_str_plain_F_PACKED ); Py_INCREF( const_str_plain_F_PACKED );
    const_str_plain_F_EXTERNAL = UNSTREAM_STRING_ASCII( &constant_bin[ 195435 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_846c2542d58a56b722dde5dd5723159c_tuple, 3, const_str_plain_F_EXTERNAL ); Py_INCREF( const_str_plain_F_EXTERNAL );
    PyTuple_SET_ITEM( const_tuple_846c2542d58a56b722dde5dd5723159c_tuple, 4, const_str_plain_F_OPAQUE ); Py_INCREF( const_str_plain_F_OPAQUE );
    const_str_plain_PRIM_INT_FAST32 = UNSTREAM_STRING_ASCII( &constant_bin[ 195445 ], 15, 1 );
    const_str_digest_1467190cb80aea848f68f8b7aafba358 = UNSTREAM_STRING_ASCII( &constant_bin[ 195460 ], 24, 0 );
    const_str_digest_40a9bc3d25971efe24fea0d3f90d4bde = UNSTREAM_STRING_ASCII( &constant_bin[ 195484 ], 25, 0 );
    const_str_plain_int_least32_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195509 ], 13, 1 );
    const_str_plain_PRIM_LONGDOUBLE = UNSTREAM_STRING_ASCII( &constant_bin[ 195522 ], 15, 1 );
    const_str_plain_uint_fast16_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195537 ], 13, 1 );
    const_str_plain_uintmax_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195550 ], 9, 1 );
    const_str_plain_int_fast8_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195559 ], 11, 1 );
    const_str_plain_uint_fast8_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195570 ], 12, 1 );
    const_str_plain_PRIM_SIZE = UNSTREAM_STRING_ASCII( &constant_bin[ 195582 ], 9, 1 );
    const_tuple_str_plain_self_str_plain_op_str_plain_arg_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_op_str_plain_arg_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_op_str_plain_arg_tuple, 1, const_str_plain_op ); Py_INCREF( const_str_plain_op );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_op_str_plain_arg_tuple, 2, const_str_plain_arg ); Py_INCREF( const_str_plain_arg );
    const_str_plain_int_least8_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195058 ], 12, 1 );
    const_str_digest_34d4327b4ade916eb42f8474ad8232e7 = UNSTREAM_STRING_ASCII( &constant_bin[ 195591 ], 25, 0 );
    const_str_digest_0f0c93e8d064b7bf26500d833b5d60de = UNSTREAM_STRING_ASCII( &constant_bin[ 195616 ], 16, 0 );
    const_str_plain_PRIM_LONGLONG = UNSTREAM_STRING_ASCII( &constant_bin[ 195632 ], 13, 1 );
    const_str_plain_uint_least32_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195645 ], 14, 1 );
    const_str_digest_526ea772325382dcbe2f1265eae53384 = UNSTREAM_STRING_ASCII( &constant_bin[ 195659 ], 14, 0 );
    const_str_plain_PRIM_UINT_FAST64 = UNSTREAM_STRING_ASCII( &constant_bin[ 195673 ], 16, 1 );
    const_str_plain_PRIM_UINTPTR = UNSTREAM_STRING_ASCII( &constant_bin[ 195689 ], 12, 1 );
    const_str_plain_PRIM_SHORT = UNSTREAM_STRING_ASCII( &constant_bin[ 195701 ], 10, 1 );
    const_tuple_str_plain_OP__tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_OP__tuple, 0, const_str_plain_OP_ ); Py_INCREF( const_str_plain_OP_ );
    const_str_digest_defbc8e84ca3d92d4d0d24789267d167 = UNSTREAM_STRING_ASCII( &constant_bin[ 195711 ], 7, 0 );
    const_str_plain_uint_least64_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195718 ], 14, 1 );
    const_str_plain_uint32_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195732 ], 8, 1 );
    const_str_plain_CLASS_NAME = UNSTREAM_STRING_ASCII( &constant_bin[ 195740 ], 10, 1 );
    const_str_digest_761aa052032797f72215e71185b344fb = UNSTREAM_STRING_ASCII( &constant_bin[ 94393 ], 13, 0 );
    const_str_plain_PRIM_UINT_LEAST64 = UNSTREAM_STRING_ASCII( &constant_bin[ 195750 ], 17, 1 );
    const_str_plain_uint8_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195767 ], 7, 1 );
    const_tuple_str_plain_self_str_plain_classname_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_classname_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_classname_tuple, 1, const_str_plain_classname ); Py_INCREF( const_str_plain_classname );
    const_str_plain_int_fast32_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195240 ], 12, 1 );
    const_str_digest_713f1d26e22b4e7d29609c4f71e00deb = UNSTREAM_STRING_ASCII( &constant_bin[ 195774 ], 34, 0 );
    const_str_digest_ec33135e1884a3a1c75406a04061265c = UNSTREAM_STRING_ASCII( &constant_bin[ 94395 ], 11, 0 );
    const_str_plain_int_least16_t = UNSTREAM_STRING_ASCII( &constant_bin[ 195301 ], 13, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_cffi$cffi_opcode( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_6ae2693b9011f33a25ed33fe4ce144af;
static PyCodeObject *codeobj_8e6544fd904bbdfa0fac46cff1e20875;
static PyCodeObject *codeobj_6e8a0fa37b12b695e6135d72c3ad7434;
static PyCodeObject *codeobj_1053cd220d0c98af130991ad9c64320c;
static PyCodeObject *codeobj_a4ac9c4945f933e6ece40acd037f2e3a;
static PyCodeObject *codeobj_9b97fe705c82b24bdfb7132e2faadc65;
static PyCodeObject *codeobj_7c54501dce6428f498802e57c848c26e;
static PyCodeObject *codeobj_5f3c4c462ead5034c080b04630d2d81a;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_52c6c5cc5fa5960f8d48b84cb181cfbe );
    codeobj_6ae2693b9011f33a25ed33fe4ce144af = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 180, const_tuple_str_plain__key_tuple, 1, 0, CO_NEWLOCALS | CO_NOFREE );
    codeobj_8e6544fd904bbdfa0fac46cff1e20875 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_34d4327b4ade916eb42f8474ad8232e7, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_6e8a0fa37b12b695e6135d72c3ad7434 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_CffiOp, 3, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_1053cd220d0c98af130991ad9c64320c = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 4, const_tuple_str_plain_self_str_plain_op_str_plain_arg_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a4ac9c4945f933e6ece40acd037f2e3a = MAKE_CODEOBJ( module_filename_obj, const_str_plain___str__, 26, const_tuple_str_plain_self_str_plain_classname_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9b97fe705c82b24bdfb7132e2faadc65 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_as_c_expr, 8, const_tuple_str_plain_self_str_plain_classname_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7c54501dce6428f498802e57c848c26e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_as_python_bytes, 15, const_tuple_str_plain_self_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5f3c4c462ead5034c080b04630d2d81a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_format_four_bytes, 30, const_tuple_str_plain_num_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_1___init__(  );


static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_2_as_c_expr(  );


static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_3_as_python_bytes(  );


static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_4___str__(  );


static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_5_format_four_bytes(  );


// The module function definitions.
static PyObject *impl_cffi$cffi_opcode$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_op = python_pars[ 1 ];
    PyObject *par_arg = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_1053cd220d0c98af130991ad9c64320c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1053cd220d0c98af130991ad9c64320c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1053cd220d0c98af130991ad9c64320c, codeobj_1053cd220d0c98af130991ad9c64320c, module_cffi$cffi_opcode, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1053cd220d0c98af130991ad9c64320c = cache_frame_1053cd220d0c98af130991ad9c64320c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1053cd220d0c98af130991ad9c64320c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1053cd220d0c98af130991ad9c64320c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_op );
        tmp_assattr_name_1 = par_op;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_op, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_arg );
        tmp_assattr_name_2 = par_arg;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_arg, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1053cd220d0c98af130991ad9c64320c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1053cd220d0c98af130991ad9c64320c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1053cd220d0c98af130991ad9c64320c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1053cd220d0c98af130991ad9c64320c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1053cd220d0c98af130991ad9c64320c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1053cd220d0c98af130991ad9c64320c,
        type_description_1,
        par_self,
        par_op,
        par_arg
    );


    // Release cached frame.
    if ( frame_1053cd220d0c98af130991ad9c64320c == cache_frame_1053cd220d0c98af130991ad9c64320c )
    {
        Py_DECREF( frame_1053cd220d0c98af130991ad9c64320c );
    }
    cache_frame_1053cd220d0c98af130991ad9c64320c = NULL;

    assertFrameObject( frame_1053cd220d0c98af130991ad9c64320c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_op );
    Py_DECREF( par_op );
    par_op = NULL;

    CHECK_OBJECT( (PyObject *)par_arg );
    Py_DECREF( par_arg );
    par_arg = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_op );
    Py_DECREF( par_op );
    par_op = NULL;

    CHECK_OBJECT( (PyObject *)par_arg );
    Py_DECREF( par_arg );
    par_arg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$cffi_opcode$$$function_2_as_c_expr( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_classname = NULL;
    struct Nuitka_FrameObject *frame_9b97fe705c82b24bdfb7132e2faadc65;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_9b97fe705c82b24bdfb7132e2faadc65 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9b97fe705c82b24bdfb7132e2faadc65, codeobj_9b97fe705c82b24bdfb7132e2faadc65, module_cffi$cffi_opcode, sizeof(void *)+sizeof(void *) );
    frame_9b97fe705c82b24bdfb7132e2faadc65 = cache_frame_9b97fe705c82b24bdfb7132e2faadc65;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9b97fe705c82b24bdfb7132e2faadc65 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9b97fe705c82b24bdfb7132e2faadc65 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_op );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_isinstance_inst_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg );
            if ( tmp_isinstance_inst_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 10;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_isinstance_cls_1 = (PyObject *)&PyUnicode_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            Py_DECREF( tmp_isinstance_inst_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 10;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 10;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                tmp_raise_type_1 = PyExc_AssertionError;
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_lineno = 10;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        {
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_source_name_3;
            tmp_left_name_1 = const_str_digest_531f769303ec4566f610e39d187e791f;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 11;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
            tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 11;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_source_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_CLASS_NAME );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CLASS_NAME );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CLASS_NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 12;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_subscript_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_op );
        if ( tmp_subscript_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_classname == NULL );
        var_classname = tmp_assign_source_1;
    }
    {
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_source_name_5;
        tmp_left_name_2 = const_str_digest_40a9bc3d25971efe24fea0d3f90d4bde;
        CHECK_OBJECT( var_classname );
        tmp_tuple_element_2 = var_classname;
        tmp_right_name_2 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_2 );

            exception_lineno = 13;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_2 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b97fe705c82b24bdfb7132e2faadc65 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b97fe705c82b24bdfb7132e2faadc65 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b97fe705c82b24bdfb7132e2faadc65 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9b97fe705c82b24bdfb7132e2faadc65, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9b97fe705c82b24bdfb7132e2faadc65->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9b97fe705c82b24bdfb7132e2faadc65, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9b97fe705c82b24bdfb7132e2faadc65,
        type_description_1,
        par_self,
        var_classname
    );


    // Release cached frame.
    if ( frame_9b97fe705c82b24bdfb7132e2faadc65 == cache_frame_9b97fe705c82b24bdfb7132e2faadc65 )
    {
        Py_DECREF( frame_9b97fe705c82b24bdfb7132e2faadc65 );
    }
    cache_frame_9b97fe705c82b24bdfb7132e2faadc65 = NULL;

    assertFrameObject( frame_9b97fe705c82b24bdfb7132e2faadc65 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_2_as_c_expr );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_classname );
    var_classname = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_classname );
    var_classname = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_2_as_c_expr );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$cffi_opcode$$$function_3_as_python_bytes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_value = NULL;
    struct Nuitka_FrameObject *frame_7c54501dce6428f498802e57c848c26e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_7c54501dce6428f498802e57c848c26e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7c54501dce6428f498802e57c848c26e, codeobj_7c54501dce6428f498802e57c848c26e, module_cffi$cffi_opcode, sizeof(void *)+sizeof(void *) );
    frame_7c54501dce6428f498802e57c848c26e = cache_frame_7c54501dce6428f498802e57c848c26e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7c54501dce6428f498802e57c848c26e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7c54501dce6428f498802e57c848c26e ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_op );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_7c54501dce6428f498802e57c848c26e->m_frame.f_lineno = 16;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_isdigit );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 16;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_int_arg_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_int_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
            if ( tmp_int_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = PyNumber_Int( tmp_int_arg_1 );
            Py_DECREF( tmp_int_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( var_value == NULL );
            var_value = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( var_value );
            tmp_compexpr_left_2 = var_value;
            tmp_compexpr_right_2 = const_int_pos_2147483648;
            tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_make_exception_arg_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_source_name_4;
                tmp_left_name_1 = const_str_digest_713f1d26e22b4e7d29609c4f71e00deb;
                CHECK_OBJECT( par_self );
                tmp_source_name_4 = par_self;
                tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_arg );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 20;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_1 = PyTuple_New( 1 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
                tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_make_exception_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 19;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                frame_7c54501dce6428f498802e57c848c26e->m_frame.f_lineno = 19;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_1 };
                    tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_OverflowError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_1 );
                assert( !(tmp_raise_type_1 == NULL) );
                exception_type = tmp_raise_type_1;
                exception_lineno = 19;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_format_four_bytes );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_format_four_bytes );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "format_four_bytes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 21;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( var_value );
            tmp_args_element_name_1 = var_value;
            frame_7c54501dce6428f498802e57c848c26e->m_frame.f_lineno = 21;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_isinstance_inst_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_isinstance_inst_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_isinstance_cls_1 = (PyObject *)&PyUnicode_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_inst_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_source_name_6;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_VerificationError );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VerificationError );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "VerificationError" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 23;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_2;
            tmp_left_name_2 = const_str_digest_c9646a8f11d26365a78cd7f9af36786d;
            CHECK_OBJECT( par_self );
            tmp_source_name_6 = par_self;
            tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_arg );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_2 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_2 );
            tmp_args_element_name_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_7c54501dce6428f498802e57c848c26e->m_frame.f_lineno = 23;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_raise_type_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_2;
            exception_lineno = 23;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_source_name_7;
        PyObject *tmp_right_name_3;
        PyObject *tmp_right_name_4;
        PyObject *tmp_source_name_8;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_format_four_bytes );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_format_four_bytes );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "format_four_bytes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 24;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_left_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_arg );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_int_pos_8;
        tmp_left_name_3 = BINARY_OPERATION( PyNumber_Lshift, tmp_left_name_4, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_8 = par_self;
        tmp_right_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_op );
        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_3 );

            exception_lineno = 24;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_3, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_3 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_7c54501dce6428f498802e57c848c26e->m_frame.f_lineno = 24;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7c54501dce6428f498802e57c848c26e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7c54501dce6428f498802e57c848c26e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7c54501dce6428f498802e57c848c26e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7c54501dce6428f498802e57c848c26e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7c54501dce6428f498802e57c848c26e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7c54501dce6428f498802e57c848c26e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7c54501dce6428f498802e57c848c26e,
        type_description_1,
        par_self,
        var_value
    );


    // Release cached frame.
    if ( frame_7c54501dce6428f498802e57c848c26e == cache_frame_7c54501dce6428f498802e57c848c26e )
    {
        Py_DECREF( frame_7c54501dce6428f498802e57c848c26e );
    }
    cache_frame_7c54501dce6428f498802e57c848c26e = NULL;

    assertFrameObject( frame_7c54501dce6428f498802e57c848c26e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_3_as_python_bytes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_3_as_python_bytes );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$cffi_opcode$$$function_4___str__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_classname = NULL;
    struct Nuitka_FrameObject *frame_a4ac9c4945f933e6ece40acd037f2e3a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_a4ac9c4945f933e6ece40acd037f2e3a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a4ac9c4945f933e6ece40acd037f2e3a, codeobj_a4ac9c4945f933e6ece40acd037f2e3a, module_cffi$cffi_opcode, sizeof(void *)+sizeof(void *) );
    frame_a4ac9c4945f933e6ece40acd037f2e3a = cache_frame_a4ac9c4945f933e6ece40acd037f2e3a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a4ac9c4945f933e6ece40acd037f2e3a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a4ac9c4945f933e6ece40acd037f2e3a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_CLASS_NAME );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CLASS_NAME );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CLASS_NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_op );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_op );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_a4ac9c4945f933e6ece40acd037f2e3a->m_frame.f_lineno = 27;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_classname == NULL );
        var_classname = tmp_assign_source_1;
    }
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_4;
        tmp_left_name_1 = const_str_digest_defbc8e84ca3d92d4d0d24789267d167;
        CHECK_OBJECT( var_classname );
        tmp_tuple_element_1 = var_classname;
        tmp_right_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_arg );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 28;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a4ac9c4945f933e6ece40acd037f2e3a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a4ac9c4945f933e6ece40acd037f2e3a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a4ac9c4945f933e6ece40acd037f2e3a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a4ac9c4945f933e6ece40acd037f2e3a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a4ac9c4945f933e6ece40acd037f2e3a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a4ac9c4945f933e6ece40acd037f2e3a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a4ac9c4945f933e6ece40acd037f2e3a,
        type_description_1,
        par_self,
        var_classname
    );


    // Release cached frame.
    if ( frame_a4ac9c4945f933e6ece40acd037f2e3a == cache_frame_a4ac9c4945f933e6ece40acd037f2e3a )
    {
        Py_DECREF( frame_a4ac9c4945f933e6ece40acd037f2e3a );
    }
    cache_frame_a4ac9c4945f933e6ece40acd037f2e3a = NULL;

    assertFrameObject( frame_a4ac9c4945f933e6ece40acd037f2e3a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_4___str__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_classname );
    Py_DECREF( var_classname );
    var_classname = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_classname );
    var_classname = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_4___str__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$cffi_opcode$$$function_5_format_four_bytes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_num = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5f3c4c462ead5034c080b04630d2d81a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5f3c4c462ead5034c080b04630d2d81a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5f3c4c462ead5034c080b04630d2d81a, codeobj_5f3c4c462ead5034c080b04630d2d81a, module_cffi$cffi_opcode, sizeof(void *) );
    frame_5f3c4c462ead5034c080b04630d2d81a = cache_frame_5f3c4c462ead5034c080b04630d2d81a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5f3c4c462ead5034c080b04630d2d81a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5f3c4c462ead5034c080b04630d2d81a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_4;
        PyObject *tmp_right_name_5;
        PyObject *tmp_left_name_6;
        PyObject *tmp_left_name_7;
        PyObject *tmp_right_name_6;
        PyObject *tmp_right_name_7;
        PyObject *tmp_left_name_8;
        PyObject *tmp_right_name_8;
        tmp_left_name_1 = const_str_digest_1467190cb80aea848f68f8b7aafba358;
        CHECK_OBJECT( par_num );
        tmp_left_name_3 = par_num;
        tmp_right_name_2 = const_int_pos_24;
        tmp_left_name_2 = BINARY_OPERATION( PyNumber_Rshift, tmp_left_name_3, tmp_right_name_2 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_int_pos_255;
        tmp_tuple_element_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 4 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_num );
        tmp_left_name_5 = par_num;
        tmp_right_name_4 = const_int_pos_16;
        tmp_left_name_4 = BINARY_OPERATION( PyNumber_Rshift, tmp_left_name_5, tmp_right_name_4 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 33;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_5 = const_int_pos_255;
        tmp_tuple_element_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_4, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 33;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_num );
        tmp_left_name_7 = par_num;
        tmp_right_name_6 = const_int_pos_8;
        tmp_left_name_6 = BINARY_OPERATION( PyNumber_Rshift, tmp_left_name_7, tmp_right_name_6 );
        if ( tmp_left_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 34;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_7 = const_int_pos_255;
        tmp_tuple_element_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_6, tmp_right_name_7 );
        Py_DECREF( tmp_left_name_6 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 34;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_num );
        tmp_left_name_8 = par_num;
        tmp_right_name_8 = const_int_pos_255;
        tmp_tuple_element_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_8, tmp_right_name_8 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 35;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 3, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f3c4c462ead5034c080b04630d2d81a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f3c4c462ead5034c080b04630d2d81a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f3c4c462ead5034c080b04630d2d81a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5f3c4c462ead5034c080b04630d2d81a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5f3c4c462ead5034c080b04630d2d81a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5f3c4c462ead5034c080b04630d2d81a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5f3c4c462ead5034c080b04630d2d81a,
        type_description_1,
        par_num
    );


    // Release cached frame.
    if ( frame_5f3c4c462ead5034c080b04630d2d81a == cache_frame_5f3c4c462ead5034c080b04630d2d81a )
    {
        Py_DECREF( frame_5f3c4c462ead5034c080b04630d2d81a );
    }
    cache_frame_5f3c4c462ead5034c080b04630d2d81a = NULL;

    assertFrameObject( frame_5f3c4c462ead5034c080b04630d2d81a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_5_format_four_bytes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode$$$function_5_format_four_bytes );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_1___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$cffi_opcode$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_7cae98d7c18be7e7451e4f9c1636e913,
#endif
        codeobj_1053cd220d0c98af130991ad9c64320c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$cffi_opcode,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_2_as_c_expr(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$cffi_opcode$$$function_2_as_c_expr,
        const_str_plain_as_c_expr,
#if PYTHON_VERSION >= 300
        const_str_digest_0f0c93e8d064b7bf26500d833b5d60de,
#endif
        codeobj_9b97fe705c82b24bdfb7132e2faadc65,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$cffi_opcode,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_3_as_python_bytes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$cffi_opcode$$$function_3_as_python_bytes,
        const_str_plain_as_python_bytes,
#if PYTHON_VERSION >= 300
        const_str_digest_29ebfd2e1c80292c48e8d899d54008d4,
#endif
        codeobj_7c54501dce6428f498802e57c848c26e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$cffi_opcode,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_4___str__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$cffi_opcode$$$function_4___str__,
        const_str_plain___str__,
#if PYTHON_VERSION >= 300
        const_str_digest_526ea772325382dcbe2f1265eae53384,
#endif
        codeobj_a4ac9c4945f933e6ece40acd037f2e3a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$cffi_opcode,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$cffi_opcode$$$function_5_format_four_bytes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$cffi_opcode$$$function_5_format_four_bytes,
        const_str_plain_format_four_bytes,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5f3c4c462ead5034c080b04630d2d81a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$cffi_opcode,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_cffi$cffi_opcode =
{
    PyModuleDef_HEAD_INIT,
    "cffi.cffi_opcode",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(cffi$cffi_opcode)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(cffi$cffi_opcode)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_cffi$cffi_opcode );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("cffi.cffi_opcode: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("cffi.cffi_opcode: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("cffi.cffi_opcode: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initcffi$cffi_opcode" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_cffi$cffi_opcode = Py_InitModule4(
        "cffi.cffi_opcode",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_cffi$cffi_opcode = PyModule_Create( &mdef_cffi$cffi_opcode );
#endif

    moduledict_cffi$cffi_opcode = MODULE_DICT( module_cffi$cffi_opcode );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_cffi$cffi_opcode,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_cffi$cffi_opcode,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_cffi$cffi_opcode,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_cffi$cffi_opcode,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_cffi$cffi_opcode );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_40527726347e207cd2c2efc6de6163be, module_cffi$cffi_opcode );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var__key = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_8e6544fd904bbdfa0fac46cff1e20875;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_cffi$cffi_opcode_3 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_6e8a0fa37b12b695e6135d72c3ad7434_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_6e8a0fa37b12b695e6135d72c3ad7434_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    struct Nuitka_FrameObject *frame_6ae2693b9011f33a25ed33fe4ce144af_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_6ae2693b9011f33a25ed33fe4ce144af_3 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_8e6544fd904bbdfa0fac46cff1e20875 = MAKE_MODULE_FRAME( codeobj_8e6544fd904bbdfa0fac46cff1e20875, module_cffi$cffi_opcode );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_8e6544fd904bbdfa0fac46cff1e20875 );
    assert( Py_REFCNT( frame_8e6544fd904bbdfa0fac46cff1e20875 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_error;
        tmp_globals_name_1 = (PyObject *)moduledict_cffi$cffi_opcode;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_VerificationError_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_8e6544fd904bbdfa0fac46cff1e20875->m_frame.f_lineno = 1;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_4 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_cffi$cffi_opcode,
                const_str_plain_VerificationError,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_VerificationError );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_VerificationError, tmp_assign_source_4 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_5 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_7 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_7;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 3;

                goto try_except_handler_1;
            }
            tmp_tuple_element_1 = const_str_plain_CffiOp;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_8e6544fd904bbdfa0fac46cff1e20875->m_frame.f_lineno = 3;
            tmp_assign_source_8 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 3;

                goto try_except_handler_1;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_8;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 3;

                goto try_except_handler_1;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 3;

                    goto try_except_handler_1;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 3;

                    goto try_except_handler_1;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 3;

                    goto try_except_handler_1;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 3;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_1;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_9;
            tmp_assign_source_9 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_9;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_10;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_cffi$cffi_opcode_3 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_40527726347e207cd2c2efc6de6163be;
        tmp_res = PyObject_SetItem( locals_cffi$cffi_opcode_3, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_3;
        }
        tmp_dictset_value = const_str_plain_CffiOp;
        tmp_res = PyObject_SetItem( locals_cffi$cffi_opcode_3, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_6e8a0fa37b12b695e6135d72c3ad7434_2, codeobj_6e8a0fa37b12b695e6135d72c3ad7434, module_cffi$cffi_opcode, sizeof(void *) );
        frame_6e8a0fa37b12b695e6135d72c3ad7434_2 = cache_frame_6e8a0fa37b12b695e6135d72c3ad7434_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_6e8a0fa37b12b695e6135d72c3ad7434_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_6e8a0fa37b12b695e6135d72c3ad7434_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_cffi$cffi_opcode$$$function_1___init__(  );



        tmp_res = PyObject_SetItem( locals_cffi$cffi_opcode_3, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_cffi$cffi_opcode$$$function_2_as_c_expr(  );



        tmp_res = PyObject_SetItem( locals_cffi$cffi_opcode_3, const_str_plain_as_c_expr, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_cffi$cffi_opcode$$$function_3_as_python_bytes(  );



        tmp_res = PyObject_SetItem( locals_cffi$cffi_opcode_3, const_str_plain_as_python_bytes, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_cffi$cffi_opcode$$$function_4___str__(  );



        tmp_res = PyObject_SetItem( locals_cffi$cffi_opcode_3, const_str_plain___str__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6e8a0fa37b12b695e6135d72c3ad7434_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6e8a0fa37b12b695e6135d72c3ad7434_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_6e8a0fa37b12b695e6135d72c3ad7434_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_6e8a0fa37b12b695e6135d72c3ad7434_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_6e8a0fa37b12b695e6135d72c3ad7434_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_6e8a0fa37b12b695e6135d72c3ad7434_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_6e8a0fa37b12b695e6135d72c3ad7434_2 == cache_frame_6e8a0fa37b12b695e6135d72c3ad7434_2 )
        {
            Py_DECREF( frame_6e8a0fa37b12b695e6135d72c3ad7434_2 );
        }
        cache_frame_6e8a0fa37b12b695e6135d72c3ad7434_2 = NULL;

        assertFrameObject( frame_6e8a0fa37b12b695e6135d72c3ad7434_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_3;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 3;

                goto try_except_handler_3;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_cffi$cffi_opcode_3, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 3;

                goto try_except_handler_3;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_CffiOp;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_cffi$cffi_opcode_3;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_8e6544fd904bbdfa0fac46cff1e20875->m_frame.f_lineno = 3;
            tmp_assign_source_11 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 3;

                goto try_except_handler_3;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_11;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_10 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_10 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        Py_DECREF( locals_cffi$cffi_opcode_3 );
        locals_cffi$cffi_opcode_3 = NULL;
        goto try_return_handler_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_cffi$cffi_opcode_3 );
        locals_cffi$cffi_opcode_3 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 3;
        goto try_except_handler_1;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_CffiOp, tmp_assign_source_10 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_cffi$cffi_opcode$$$function_5_format_four_bytes(  );



        UPDATE_STRING_DICT1( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_format_four_bytes, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = const_int_pos_1;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_PRIMITIVE, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = const_int_pos_3;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_POINTER, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = const_int_pos_5;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_ARRAY, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = const_int_pos_7;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_OPEN_ARRAY, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = const_int_pos_9;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_STRUCT_UNION, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = const_int_pos_11;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_ENUM, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = const_int_pos_13;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_FUNCTION, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = const_int_pos_15;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_FUNCTION_END, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = const_int_pos_17;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_NOOP, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = const_int_pos_19;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_BITFIELD, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = const_int_pos_21;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_TYPENAME, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = const_int_pos_23;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_CPYTHON_BLTN_V, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = const_int_pos_25;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_CPYTHON_BLTN_N, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = const_int_pos_27;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_CPYTHON_BLTN_O, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = const_int_pos_29;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_CONSTANT, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = const_int_pos_31;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_CONSTANT_INT, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = const_int_pos_33;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_GLOBAL_VAR, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = const_int_pos_35;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_DLOPEN_FUNC, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = const_int_pos_37;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_DLOPEN_CONST, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        tmp_assign_source_32 = const_int_pos_39;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_GLOBAL_VAR_F, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        tmp_assign_source_33 = const_int_pos_41;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_OP_EXTERN_PYTHON, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = const_int_0;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_VOID, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        tmp_assign_source_35 = const_int_pos_1;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_BOOL, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        tmp_assign_source_36 = const_int_pos_2;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_CHAR, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        tmp_assign_source_37 = const_int_pos_3;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_SCHAR, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        tmp_assign_source_38 = const_int_pos_4;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UCHAR, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        tmp_assign_source_39 = const_int_pos_5;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_SHORT, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        tmp_assign_source_40 = const_int_pos_6;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_USHORT, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        tmp_assign_source_41 = const_int_pos_7;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        tmp_assign_source_42 = const_int_pos_8;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        tmp_assign_source_43 = const_int_pos_9;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_LONG, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        tmp_assign_source_44 = const_int_pos_10;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_ULONG, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        tmp_assign_source_45 = const_int_pos_11;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_LONGLONG, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        tmp_assign_source_46 = const_int_pos_12;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_ULONGLONG, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        tmp_assign_source_47 = const_int_pos_13;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_FLOAT, tmp_assign_source_47 );
    }
    {
        PyObject *tmp_assign_source_48;
        tmp_assign_source_48 = const_int_pos_14;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_DOUBLE, tmp_assign_source_48 );
    }
    {
        PyObject *tmp_assign_source_49;
        tmp_assign_source_49 = const_int_pos_15;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_LONGDOUBLE, tmp_assign_source_49 );
    }
    {
        PyObject *tmp_assign_source_50;
        tmp_assign_source_50 = const_int_pos_16;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_WCHAR, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        tmp_assign_source_51 = const_int_pos_17;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT8, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        tmp_assign_source_52 = const_int_pos_18;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT8, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        tmp_assign_source_53 = const_int_pos_19;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT16, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        tmp_assign_source_54 = const_int_pos_20;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT16, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        tmp_assign_source_55 = const_int_pos_21;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT32, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        tmp_assign_source_56 = const_int_pos_22;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT32, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        tmp_assign_source_57 = const_int_pos_23;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT64, tmp_assign_source_57 );
    }
    {
        PyObject *tmp_assign_source_58;
        tmp_assign_source_58 = const_int_pos_24;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT64, tmp_assign_source_58 );
    }
    {
        PyObject *tmp_assign_source_59;
        tmp_assign_source_59 = const_int_pos_25;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INTPTR, tmp_assign_source_59 );
    }
    {
        PyObject *tmp_assign_source_60;
        tmp_assign_source_60 = const_int_pos_26;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINTPTR, tmp_assign_source_60 );
    }
    {
        PyObject *tmp_assign_source_61;
        tmp_assign_source_61 = const_int_pos_27;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_PTRDIFF, tmp_assign_source_61 );
    }
    {
        PyObject *tmp_assign_source_62;
        tmp_assign_source_62 = const_int_pos_28;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_SIZE, tmp_assign_source_62 );
    }
    {
        PyObject *tmp_assign_source_63;
        tmp_assign_source_63 = const_int_pos_29;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_SSIZE, tmp_assign_source_63 );
    }
    {
        PyObject *tmp_assign_source_64;
        tmp_assign_source_64 = const_int_pos_30;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST8, tmp_assign_source_64 );
    }
    {
        PyObject *tmp_assign_source_65;
        tmp_assign_source_65 = const_int_pos_31;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST8, tmp_assign_source_65 );
    }
    {
        PyObject *tmp_assign_source_66;
        tmp_assign_source_66 = const_int_pos_32;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST16, tmp_assign_source_66 );
    }
    {
        PyObject *tmp_assign_source_67;
        tmp_assign_source_67 = const_int_pos_33;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST16, tmp_assign_source_67 );
    }
    {
        PyObject *tmp_assign_source_68;
        tmp_assign_source_68 = const_int_pos_34;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST32, tmp_assign_source_68 );
    }
    {
        PyObject *tmp_assign_source_69;
        tmp_assign_source_69 = const_int_pos_35;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST32, tmp_assign_source_69 );
    }
    {
        PyObject *tmp_assign_source_70;
        tmp_assign_source_70 = const_int_pos_36;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST64, tmp_assign_source_70 );
    }
    {
        PyObject *tmp_assign_source_71;
        tmp_assign_source_71 = const_int_pos_37;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST64, tmp_assign_source_71 );
    }
    {
        PyObject *tmp_assign_source_72;
        tmp_assign_source_72 = const_int_pos_38;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST8, tmp_assign_source_72 );
    }
    {
        PyObject *tmp_assign_source_73;
        tmp_assign_source_73 = const_int_pos_39;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST8, tmp_assign_source_73 );
    }
    {
        PyObject *tmp_assign_source_74;
        tmp_assign_source_74 = const_int_pos_40;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST16, tmp_assign_source_74 );
    }
    {
        PyObject *tmp_assign_source_75;
        tmp_assign_source_75 = const_int_pos_41;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST16, tmp_assign_source_75 );
    }
    {
        PyObject *tmp_assign_source_76;
        tmp_assign_source_76 = const_int_pos_42;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST32, tmp_assign_source_76 );
    }
    {
        PyObject *tmp_assign_source_77;
        tmp_assign_source_77 = const_int_pos_43;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST32, tmp_assign_source_77 );
    }
    {
        PyObject *tmp_assign_source_78;
        tmp_assign_source_78 = const_int_pos_44;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST64, tmp_assign_source_78 );
    }
    {
        PyObject *tmp_assign_source_79;
        tmp_assign_source_79 = const_int_pos_45;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST64, tmp_assign_source_79 );
    }
    {
        PyObject *tmp_assign_source_80;
        tmp_assign_source_80 = const_int_pos_46;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INTMAX, tmp_assign_source_80 );
    }
    {
        PyObject *tmp_assign_source_81;
        tmp_assign_source_81 = const_int_pos_47;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINTMAX, tmp_assign_source_81 );
    }
    {
        PyObject *tmp_assign_source_82;
        tmp_assign_source_82 = const_int_pos_48;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_FLOATCOMPLEX, tmp_assign_source_82 );
    }
    {
        PyObject *tmp_assign_source_83;
        tmp_assign_source_83 = const_int_pos_49;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_DOUBLECOMPLEX, tmp_assign_source_83 );
    }
    {
        PyObject *tmp_assign_source_84;
        tmp_assign_source_84 = const_int_pos_50;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_CHAR16, tmp_assign_source_84 );
    }
    {
        PyObject *tmp_assign_source_85;
        tmp_assign_source_85 = const_int_pos_51;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_CHAR32, tmp_assign_source_85 );
    }
    {
        PyObject *tmp_assign_source_86;
        tmp_assign_source_86 = const_int_pos_52;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__NUM_PRIM, tmp_assign_source_86 );
    }
    {
        PyObject *tmp_assign_source_87;
        tmp_assign_source_87 = const_int_neg_1;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__UNKNOWN_PRIM, tmp_assign_source_87 );
    }
    {
        PyObject *tmp_assign_source_88;
        tmp_assign_source_88 = const_int_neg_2;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__UNKNOWN_FLOAT_PRIM, tmp_assign_source_88 );
    }
    {
        PyObject *tmp_assign_source_89;
        tmp_assign_source_89 = const_int_neg_3;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__UNKNOWN_LONG_DOUBLE, tmp_assign_source_89 );
    }
    {
        PyObject *tmp_assign_source_90;
        tmp_assign_source_90 = const_int_neg_1;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__IO_FILE_STRUCT, tmp_assign_source_90 );
    }
    {
        PyObject *tmp_assign_source_91;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_dict_key_22;
        PyObject *tmp_dict_value_22;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_dict_key_28;
        PyObject *tmp_dict_value_28;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_dict_key_29;
        PyObject *tmp_dict_value_29;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_dict_key_30;
        PyObject *tmp_dict_value_30;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_dict_key_31;
        PyObject *tmp_dict_value_31;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_dict_key_32;
        PyObject *tmp_dict_value_32;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_dict_key_33;
        PyObject *tmp_dict_value_33;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_dict_key_34;
        PyObject *tmp_dict_value_34;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_dict_key_35;
        PyObject *tmp_dict_value_35;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_dict_key_36;
        PyObject *tmp_dict_value_36;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_dict_key_37;
        PyObject *tmp_dict_value_37;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_dict_key_38;
        PyObject *tmp_dict_value_38;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_dict_key_39;
        PyObject *tmp_dict_value_39;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_dict_key_40;
        PyObject *tmp_dict_value_40;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_dict_key_41;
        PyObject *tmp_dict_value_41;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_dict_key_42;
        PyObject *tmp_dict_value_42;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_dict_key_43;
        PyObject *tmp_dict_value_43;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_dict_key_44;
        PyObject *tmp_dict_value_44;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_dict_key_45;
        PyObject *tmp_dict_value_45;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_dict_key_46;
        PyObject *tmp_dict_value_46;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_dict_key_47;
        PyObject *tmp_dict_value_47;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_dict_key_48;
        PyObject *tmp_dict_value_48;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_dict_key_49;
        PyObject *tmp_dict_value_49;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_dict_key_50;
        PyObject *tmp_dict_value_50;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_dict_key_51;
        PyObject *tmp_dict_value_51;
        PyObject *tmp_mvar_value_53;
        tmp_dict_key_1 = const_str_plain_char;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_CHAR );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_CHAR );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_dict_value_1 = tmp_mvar_value_3;
        tmp_assign_source_91 = _PyDict_NewPresized( 51 );
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_short;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_SHORT );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_dict_value_2 = tmp_mvar_value_4;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_int;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_dict_value_3 = tmp_mvar_value_5;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_long;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_LONG );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_6 );
        tmp_dict_value_4 = tmp_mvar_value_6;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_digest_b985875eb32c9998d65889b2c366bd29;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_LONGLONG );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_LONGLONG );
        }

        CHECK_OBJECT( tmp_mvar_value_7 );
        tmp_dict_value_5 = tmp_mvar_value_7;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_digest_ec33135e1884a3a1c75406a04061265c;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_SCHAR );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_SCHAR );
        }

        CHECK_OBJECT( tmp_mvar_value_8 );
        tmp_dict_value_6 = tmp_mvar_value_8;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_digest_761aa052032797f72215e71185b344fb;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UCHAR );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UCHAR );
        }

        CHECK_OBJECT( tmp_mvar_value_9 );
        tmp_dict_value_7 = tmp_mvar_value_9;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_digest_92dfa96e420126c66052ca0989a42b38;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_USHORT );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_USHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_10 );
        tmp_dict_value_8 = tmp_mvar_value_10;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_digest_054eabad1e66af9ef84d8e439327b578;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT );
        }

        CHECK_OBJECT( tmp_mvar_value_11 );
        tmp_dict_value_9 = tmp_mvar_value_11;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_digest_902703cdcced1b2f97d32b33c70b1358;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_ULONG );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_ULONG );
        }

        CHECK_OBJECT( tmp_mvar_value_12 );
        tmp_dict_value_10 = tmp_mvar_value_12;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_11 = const_str_digest_6bf3055530e3f17a8db26b23f339f170;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_ULONGLONG );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_ULONGLONG );
        }

        CHECK_OBJECT( tmp_mvar_value_13 );
        tmp_dict_value_11 = tmp_mvar_value_13;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_12 = const_str_plain_float;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_FLOAT );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_FLOAT );
        }

        CHECK_OBJECT( tmp_mvar_value_14 );
        tmp_dict_value_12 = tmp_mvar_value_14;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_12, tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_13 = const_str_plain_double;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_DOUBLE );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_DOUBLE );
        }

        CHECK_OBJECT( tmp_mvar_value_15 );
        tmp_dict_value_13 = tmp_mvar_value_15;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_13, tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_14 = const_str_digest_61eb47d02a6bf21baa1afce40f67ac8b;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_LONGDOUBLE );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_LONGDOUBLE );
        }

        CHECK_OBJECT( tmp_mvar_value_16 );
        tmp_dict_value_14 = tmp_mvar_value_16;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_14, tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_15 = const_str_digest_a199b01cf85ae649d8d5249bb63e5635;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_FLOATCOMPLEX );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_FLOATCOMPLEX );
        }

        CHECK_OBJECT( tmp_mvar_value_17 );
        tmp_dict_value_15 = tmp_mvar_value_17;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_15, tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_16 = const_str_digest_ea9b4f812bfcd8150ff586ab51ffa83a;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_DOUBLECOMPLEX );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_DOUBLECOMPLEX );
        }

        CHECK_OBJECT( tmp_mvar_value_18 );
        tmp_dict_value_16 = tmp_mvar_value_18;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_16, tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_17 = const_str_plain__Bool;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_BOOL );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_BOOL );
        }

        CHECK_OBJECT( tmp_mvar_value_19 );
        tmp_dict_value_17 = tmp_mvar_value_19;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_17, tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_18 = const_str_plain_wchar_t;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_WCHAR );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_WCHAR );
        }

        CHECK_OBJECT( tmp_mvar_value_20 );
        tmp_dict_value_18 = tmp_mvar_value_20;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_18, tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_19 = const_str_plain_char16_t;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_CHAR16 );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_CHAR16 );
        }

        CHECK_OBJECT( tmp_mvar_value_21 );
        tmp_dict_value_19 = tmp_mvar_value_21;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_19, tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_20 = const_str_plain_char32_t;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_CHAR32 );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_CHAR32 );
        }

        CHECK_OBJECT( tmp_mvar_value_22 );
        tmp_dict_value_20 = tmp_mvar_value_22;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_20, tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_21 = const_str_plain_int8_t;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT8 );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT8 );
        }

        CHECK_OBJECT( tmp_mvar_value_23 );
        tmp_dict_value_21 = tmp_mvar_value_23;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_21, tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_22 = const_str_plain_uint8_t;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT8 );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT8 );
        }

        CHECK_OBJECT( tmp_mvar_value_24 );
        tmp_dict_value_22 = tmp_mvar_value_24;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_22, tmp_dict_value_22 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_23 = const_str_plain_int16_t;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT16 );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT16 );
        }

        CHECK_OBJECT( tmp_mvar_value_25 );
        tmp_dict_value_23 = tmp_mvar_value_25;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_23, tmp_dict_value_23 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_24 = const_str_plain_uint16_t;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT16 );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT16 );
        }

        CHECK_OBJECT( tmp_mvar_value_26 );
        tmp_dict_value_24 = tmp_mvar_value_26;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_24, tmp_dict_value_24 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_25 = const_str_plain_int32_t;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT32 );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT32 );
        }

        CHECK_OBJECT( tmp_mvar_value_27 );
        tmp_dict_value_25 = tmp_mvar_value_27;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_25, tmp_dict_value_25 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_26 = const_str_plain_uint32_t;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT32 );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT32 );
        }

        CHECK_OBJECT( tmp_mvar_value_28 );
        tmp_dict_value_26 = tmp_mvar_value_28;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_26, tmp_dict_value_26 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_27 = const_str_plain_int64_t;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT64 );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT64 );
        }

        CHECK_OBJECT( tmp_mvar_value_29 );
        tmp_dict_value_27 = tmp_mvar_value_29;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_27, tmp_dict_value_27 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_28 = const_str_plain_uint64_t;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT64 );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT64 );
        }

        CHECK_OBJECT( tmp_mvar_value_30 );
        tmp_dict_value_28 = tmp_mvar_value_30;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_28, tmp_dict_value_28 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_29 = const_str_plain_intptr_t;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INTPTR );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INTPTR );
        }

        CHECK_OBJECT( tmp_mvar_value_31 );
        tmp_dict_value_29 = tmp_mvar_value_31;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_29, tmp_dict_value_29 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_30 = const_str_plain_uintptr_t;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINTPTR );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINTPTR );
        }

        CHECK_OBJECT( tmp_mvar_value_32 );
        tmp_dict_value_30 = tmp_mvar_value_32;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_30, tmp_dict_value_30 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_31 = const_str_plain_ptrdiff_t;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_PTRDIFF );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_PTRDIFF );
        }

        CHECK_OBJECT( tmp_mvar_value_33 );
        tmp_dict_value_31 = tmp_mvar_value_33;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_31, tmp_dict_value_31 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_32 = const_str_plain_size_t;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_SIZE );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_SIZE );
        }

        CHECK_OBJECT( tmp_mvar_value_34 );
        tmp_dict_value_32 = tmp_mvar_value_34;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_32, tmp_dict_value_32 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_33 = const_str_plain_ssize_t;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_SSIZE );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_SSIZE );
        }

        CHECK_OBJECT( tmp_mvar_value_35 );
        tmp_dict_value_33 = tmp_mvar_value_35;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_33, tmp_dict_value_33 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_34 = const_str_plain_int_least8_t;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST8 );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST8 );
        }

        CHECK_OBJECT( tmp_mvar_value_36 );
        tmp_dict_value_34 = tmp_mvar_value_36;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_34, tmp_dict_value_34 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_35 = const_str_plain_uint_least8_t;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST8 );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST8 );
        }

        CHECK_OBJECT( tmp_mvar_value_37 );
        tmp_dict_value_35 = tmp_mvar_value_37;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_35, tmp_dict_value_35 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_36 = const_str_plain_int_least16_t;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST16 );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST16 );
        }

        CHECK_OBJECT( tmp_mvar_value_38 );
        tmp_dict_value_36 = tmp_mvar_value_38;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_36, tmp_dict_value_36 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_37 = const_str_plain_uint_least16_t;
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST16 );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST16 );
        }

        CHECK_OBJECT( tmp_mvar_value_39 );
        tmp_dict_value_37 = tmp_mvar_value_39;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_37, tmp_dict_value_37 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_38 = const_str_plain_int_least32_t;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST32 );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST32 );
        }

        CHECK_OBJECT( tmp_mvar_value_40 );
        tmp_dict_value_38 = tmp_mvar_value_40;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_38, tmp_dict_value_38 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_39 = const_str_plain_uint_least32_t;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST32 );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST32 );
        }

        CHECK_OBJECT( tmp_mvar_value_41 );
        tmp_dict_value_39 = tmp_mvar_value_41;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_39, tmp_dict_value_39 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_40 = const_str_plain_int_least64_t;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST64 );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT_LEAST64 );
        }

        CHECK_OBJECT( tmp_mvar_value_42 );
        tmp_dict_value_40 = tmp_mvar_value_42;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_40, tmp_dict_value_40 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_41 = const_str_plain_uint_least64_t;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST64 );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_LEAST64 );
        }

        CHECK_OBJECT( tmp_mvar_value_43 );
        tmp_dict_value_41 = tmp_mvar_value_43;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_41, tmp_dict_value_41 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_42 = const_str_plain_int_fast8_t;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST8 );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST8 );
        }

        CHECK_OBJECT( tmp_mvar_value_44 );
        tmp_dict_value_42 = tmp_mvar_value_44;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_42, tmp_dict_value_42 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_43 = const_str_plain_uint_fast8_t;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST8 );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST8 );
        }

        CHECK_OBJECT( tmp_mvar_value_45 );
        tmp_dict_value_43 = tmp_mvar_value_45;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_43, tmp_dict_value_43 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_44 = const_str_plain_int_fast16_t;
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST16 );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST16 );
        }

        CHECK_OBJECT( tmp_mvar_value_46 );
        tmp_dict_value_44 = tmp_mvar_value_46;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_44, tmp_dict_value_44 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_45 = const_str_plain_uint_fast16_t;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST16 );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST16 );
        }

        CHECK_OBJECT( tmp_mvar_value_47 );
        tmp_dict_value_45 = tmp_mvar_value_47;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_45, tmp_dict_value_45 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_46 = const_str_plain_int_fast32_t;
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST32 );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST32 );
        }

        CHECK_OBJECT( tmp_mvar_value_48 );
        tmp_dict_value_46 = tmp_mvar_value_48;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_46, tmp_dict_value_46 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_47 = const_str_plain_uint_fast32_t;
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST32 );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST32 );
        }

        CHECK_OBJECT( tmp_mvar_value_49 );
        tmp_dict_value_47 = tmp_mvar_value_49;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_47, tmp_dict_value_47 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_48 = const_str_plain_int_fast64_t;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST64 );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INT_FAST64 );
        }

        CHECK_OBJECT( tmp_mvar_value_50 );
        tmp_dict_value_48 = tmp_mvar_value_50;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_48, tmp_dict_value_48 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_49 = const_str_plain_uint_fast64_t;
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST64 );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINT_FAST64 );
        }

        CHECK_OBJECT( tmp_mvar_value_51 );
        tmp_dict_value_49 = tmp_mvar_value_51;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_49, tmp_dict_value_49 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_50 = const_str_plain_intmax_t;
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_INTMAX );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_INTMAX );
        }

        CHECK_OBJECT( tmp_mvar_value_52 );
        tmp_dict_value_50 = tmp_mvar_value_52;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_50, tmp_dict_value_50 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_51 = const_str_plain_uintmax_t;
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIM_UINTMAX );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRIM_UINTMAX );
        }

        CHECK_OBJECT( tmp_mvar_value_53 );
        tmp_dict_value_51 = tmp_mvar_value_53;
        tmp_res = PyDict_SetItem( tmp_assign_source_91, tmp_dict_key_51, tmp_dict_value_51 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_PRIMITIVE_TO_INDEX, tmp_assign_source_91 );
    }
    {
        PyObject *tmp_assign_source_92;
        tmp_assign_source_92 = const_int_pos_1;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_F_UNION, tmp_assign_source_92 );
    }
    {
        PyObject *tmp_assign_source_93;
        tmp_assign_source_93 = const_int_pos_2;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_F_CHECK_FIELDS, tmp_assign_source_93 );
    }
    {
        PyObject *tmp_assign_source_94;
        tmp_assign_source_94 = const_int_pos_4;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_F_PACKED, tmp_assign_source_94 );
    }
    {
        PyObject *tmp_assign_source_95;
        tmp_assign_source_95 = const_int_pos_8;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_F_EXTERNAL, tmp_assign_source_95 );
    }
    {
        PyObject *tmp_assign_source_96;
        tmp_assign_source_96 = const_int_pos_16;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_F_OPAQUE, tmp_assign_source_96 );
    }
    {
        PyObject *tmp_assign_source_97;
        PyObject *tmp_dict_seq_1;
        {
            PyObject *tmp_assign_source_98;
            PyObject *tmp_iter_arg_1;
            tmp_iter_arg_1 = const_tuple_846c2542d58a56b722dde5dd5723159c_tuple;
            tmp_assign_source_98 = MAKE_ITERATOR( tmp_iter_arg_1 );
            assert( !(tmp_assign_source_98 == NULL) );
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_98;
        }
        {
            PyObject *tmp_assign_source_99;
            tmp_assign_source_99 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_99;
        }
        // Tried code:
        MAKE_OR_REUSE_FRAME( cache_frame_6ae2693b9011f33a25ed33fe4ce144af_3, codeobj_6ae2693b9011f33a25ed33fe4ce144af, module_cffi$cffi_opcode, sizeof(void *) );
        frame_6ae2693b9011f33a25ed33fe4ce144af_3 = cache_frame_6ae2693b9011f33a25ed33fe4ce144af_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_6ae2693b9011f33a25ed33fe4ce144af_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_6ae2693b9011f33a25ed33fe4ce144af_3 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_100;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_100 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_100 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 180;
                    goto try_except_handler_5;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_100;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_101;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_101 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_1_var__key;
                outline_1_var__key = tmp_assign_source_101;
                Py_INCREF( outline_1_var__key );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_left_name_2 = const_str_plain__CFFI_;
            CHECK_OBJECT( outline_1_var__key );
            tmp_right_name_2 = outline_1_var__key;
            tmp_tuple_element_4 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_2, tmp_right_name_2 );
            if ( tmp_tuple_element_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;
                type_description_2 = "o";
                goto try_except_handler_5;
            }
            tmp_append_value_1 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_append_value_1, 0, tmp_tuple_element_4 );
            tmp_subscribed_name_2 = (PyObject *)moduledict_cffi$cffi_opcode;
            CHECK_OBJECT( outline_1_var__key );
            tmp_subscript_name_2 = outline_1_var__key;
            tmp_tuple_element_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_tuple_element_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_append_value_1 );

                exception_lineno = 180;
                type_description_2 = "o";
                goto try_except_handler_5;
            }
            PyTuple_SET_ITEM( tmp_append_value_1, 1, tmp_tuple_element_4 );
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;
                type_description_2 = "o";
                goto try_except_handler_5;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_2 = "o";
            goto try_except_handler_5;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_dict_seq_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_dict_seq_1 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_3;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6ae2693b9011f33a25ed33fe4ce144af_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_return_exit_1:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_6ae2693b9011f33a25ed33fe4ce144af_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_4;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6ae2693b9011f33a25ed33fe4ce144af_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_6ae2693b9011f33a25ed33fe4ce144af_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_6ae2693b9011f33a25ed33fe4ce144af_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_6ae2693b9011f33a25ed33fe4ce144af_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_6ae2693b9011f33a25ed33fe4ce144af_3,
            type_description_2,
            outline_1_var__key
        );


        // Release cached frame.
        if ( frame_6ae2693b9011f33a25ed33fe4ce144af_3 == cache_frame_6ae2693b9011f33a25ed33fe4ce144af_3 )
        {
            Py_DECREF( frame_6ae2693b9011f33a25ed33fe4ce144af_3 );
        }
        cache_frame_6ae2693b9011f33a25ed33fe4ce144af_3 = NULL;

        assertFrameObject( frame_6ae2693b9011f33a25ed33fe4ce144af_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_4;
        skip_nested_handling_2:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        Py_XDECREF( outline_1_var__key );
        outline_1_var__key = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_1_var__key );
        outline_1_var__key = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( cffi$cffi_opcode );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 180;
        goto frame_exception_exit_1;
        outline_result_2:;
        tmp_assign_source_97 = TO_DICT( tmp_dict_seq_1, NULL );
        Py_DECREF( tmp_dict_seq_1 );
        if ( tmp_assign_source_97 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_G_FLAGS, tmp_assign_source_97 );
    }
    {
        PyObject *tmp_assign_source_102;
        tmp_assign_source_102 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_CLASS_NAME, tmp_assign_source_102 );
    }
    {
        PyObject *tmp_assign_source_103;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_1;
        tmp_called_instance_1 = (PyObject *)moduledict_cffi$cffi_opcode;
        frame_8e6544fd904bbdfa0fac46cff1e20875->m_frame.f_lineno = 185;
        tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;

            goto frame_exception_exit_1;
        }
        tmp_iter_arg_2 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_103 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_103 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;

            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_103;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_104;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_2 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_104 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_104 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                exception_lineno = 185;
                goto try_except_handler_6;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_104;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_105;
        PyObject *tmp_iter_arg_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_3 = tmp_for_loop_1__iter_value;
        tmp_assign_source_105 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
        if ( tmp_assign_source_105 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;

            goto try_except_handler_7;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_105;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_106;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_106 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_106 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 185;
            goto try_except_handler_8;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_106;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_107;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_107 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_107 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 185;
            goto try_except_handler_8;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_107;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 185;
                    goto try_except_handler_8;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            goto try_except_handler_8;
        }
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto try_except_handler_7;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto try_except_handler_6;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_108;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_108 = tmp_tuple_unpack_1__element_1;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__name, tmp_assign_source_108 );
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_109;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_109 = tmp_tuple_unpack_1__element_2;
        UPDATE_STRING_DICT0( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__value, tmp_assign_source_109 );
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_7;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_2;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_55;
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__name );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__name );
        }

        if ( tmp_mvar_value_54 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 186;

            goto try_except_handler_6;
        }

        tmp_called_instance_2 = tmp_mvar_value_54;
        frame_8e6544fd904bbdfa0fac46cff1e20875->m_frame.f_lineno = 186;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_OP__tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;

            goto try_except_handler_6;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 186;

            goto try_except_handler_6;
        }
        tmp_and_left_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__value );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__value );
        }

        if ( tmp_mvar_value_55 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_value" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 186;

            goto try_except_handler_6;
        }

        tmp_isinstance_inst_1 = tmp_mvar_value_55;
        tmp_isinstance_cls_1 = (PyObject *)&PyLong_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;

            goto try_except_handler_6;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_7 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_7 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_mvar_value_56;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_mvar_value_57;
            PyObject *tmp_ass_subscript_1;
            PyObject *tmp_mvar_value_58;
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__name );

            if (unlikely( tmp_mvar_value_56 == NULL ))
            {
                tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__name );
            }

            if ( tmp_mvar_value_56 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_name" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 187;

                goto try_except_handler_6;
            }

            tmp_subscribed_name_3 = tmp_mvar_value_56;
            tmp_subscript_name_3 = const_slice_int_pos_3_none_none;
            tmp_ass_subvalue_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            if ( tmp_ass_subvalue_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 187;

                goto try_except_handler_6;
            }
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain_CLASS_NAME );

            if (unlikely( tmp_mvar_value_57 == NULL ))
            {
                tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CLASS_NAME );
            }

            if ( tmp_mvar_value_57 == NULL )
            {
                Py_DECREF( tmp_ass_subvalue_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CLASS_NAME" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 187;

                goto try_except_handler_6;
            }

            tmp_ass_subscribed_1 = tmp_mvar_value_57;
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_cffi$cffi_opcode, (Nuitka_StringObject *)const_str_plain__value );

            if (unlikely( tmp_mvar_value_58 == NULL ))
            {
                tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__value );
            }

            if ( tmp_mvar_value_58 == NULL )
            {
                Py_DECREF( tmp_ass_subvalue_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_value" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 187;

                goto try_except_handler_6;
            }

            tmp_ass_subscript_1 = tmp_mvar_value_58;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 187;

                goto try_except_handler_6;
            }
        }
        branch_no_5:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 185;

        goto try_except_handler_6;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e6544fd904bbdfa0fac46cff1e20875 );
#endif
    popFrameStack();

    assertFrameObject( frame_8e6544fd904bbdfa0fac46cff1e20875 );

    goto frame_no_exception_3;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e6544fd904bbdfa0fac46cff1e20875 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8e6544fd904bbdfa0fac46cff1e20875, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8e6544fd904bbdfa0fac46cff1e20875->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8e6544fd904bbdfa0fac46cff1e20875, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;


    return MOD_RETURN_VALUE( module_cffi$cffi_opcode );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
