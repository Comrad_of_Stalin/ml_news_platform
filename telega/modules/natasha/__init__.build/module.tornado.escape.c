/* Generated code for Python module 'tornado.escape'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_tornado$escape" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_tornado$escape;
PyDictObject *moduledict_tornado$escape;

/* The declarations of module constants used, if any. */
extern PyObject *const_tuple_str_plain_i_tuple;
extern PyObject *const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple;
extern PyObject *const_str_chr_38;
static PyObject *const_str_digest_7e12d8e7d8726364c4d8827b8ca157eb;
static PyObject *const_list_type_str_list;
extern PyObject *const_str_plain_result;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain__URL_RE;
extern PyObject *const_str_plain_dict;
static PyObject *const_str_plain_proto_len;
extern PyObject *const_str_chr_43;
extern PyObject *const_str_chr_60;
static PyObject *const_str_digest_b4cc80237db0fadc7e9411b7e60e8a1d;
extern PyObject *const_str_plain_i;
extern PyObject *const_slice_int_pos_1_none_none;
static PyObject *const_str_digest_fed3a5498cbc1161ae9286cb8a8ce2c4;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_cd3210c70bf2f05624551f1b595530e4;
extern PyObject *const_str_plain__unicode;
static PyObject *const_str_plain_extra_params;
extern PyObject *const_str_plain_group;
static PyObject *const_str_digest_923e74d046a976c7e2f576a3b2a89286;
extern PyObject *const_str_plain_latin1;
extern PyObject *const_tuple_str_plain_value_tuple;
extern PyObject *const_str_plain_bytes;
extern PyObject *const_str_plain_bool;
extern PyObject *const_str_plain_encode;
static PyObject *const_str_digest_a13637e8cd7cb8c057e4d409319b229a;
static PyObject *const_str_plain_recursive_unicode;
extern PyObject *const_float_45_0;
extern PyObject *const_str_plain_json;
extern PyObject *const_tuple_false_false_tuple;
extern PyObject *const_str_plain_to_unicode;
static PyObject *const_tuple_str_plain_value_str_plain_plus_str_plain_quote_tuple;
extern PyObject *const_str_plain_items;
extern PyObject *const_str_plain_rfind;
extern PyObject *const_str_plain_Callable;
static PyObject *const_str_plain_make_link;
static PyObject *const_dict_8a2a7d46105ecf8375ebf4493d3a5935;
static PyObject *const_dict_7b0c1fe6e32c976caf2154377bedae8e;
static PyObject *const_str_digest_da233aeba81e00ca61b42b1678245594;
extern PyObject *const_str_plain_m;
extern PyObject *const_tuple_type_str_type_bytes_tuple;
static PyObject *const_tuple_825449a2a10def72445ca4a66750ebbb_tuple;
extern PyObject *const_str_digest_e077944e15accbec54ecb40fd81dafde;
extern PyObject *const_str_plain_sub;
static PyObject *const_str_digest_3fdba945c17629f91d696fe3fa5b508a;
extern PyObject *const_str_plain_Optional;
extern PyObject *const_str_plain_return;
extern PyObject *const_str_plain_Dict;
static PyObject *const_dict_0755fb5af920d0ea293e582e7b7e51f2;
extern PyObject *const_int_pos_25;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_strip;
extern PyObject *const_str_plain_List;
extern PyObject *const_str_plain_callable;
static PyObject *const_str_plain_before_clip;
static PyObject *const_str_digest_54e10fe59f47c2b318a42cad76bfb3bf;
static PyObject *const_str_plain__XHTML_ESCAPE_DICT;
extern PyObject *const_str_digest_84a3f2fedb1ec7918aa30203da68a052;
static PyObject *const_tuple_94ab84002ba56c0f19e558b6ab3642d9_tuple;
static PyObject *const_str_plain_shorten;
extern PyObject *const_str_plain_json_encode;
extern PyObject *const_str_chr_63;
extern PyObject *const_tuple_true_tuple;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain_typing;
extern PyObject *const_int_pos_16;
extern PyObject *const_str_plain_xhtml_escape;
extern PyObject *const_int_pos_30;
extern PyObject *const_str_plain_re;
static PyObject *const_str_digest_51fd4efe30577acc0163b2fc96ab3c75;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_digest_04991ea695faff4a76e4efb6a8a8593f;
extern PyObject *const_str_plain_lower;
extern PyObject *const_str_digest_7c06a402579f6f9d9db7f3e04da983fc;
extern PyObject *const_str_plain_Match;
static PyObject *const_str_plain_strict_parsing;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_i_tuple;
static PyObject *const_tuple_str_plain_unicode_map_str_plain_name_str_plain_value_tuple;
static PyObject *const_tuple_type_bytes_anon_NoneType_tuple;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_dot;
extern PyObject *const_str_chr_62;
static PyObject *const_str_digest_4a3231db8f125f2cc6fa458948ea5c7b;
extern PyObject *const_str_plain_quote;
extern PyObject *const_str_digest_a3d81feff0be3a3ff360ca33ad836962;
extern PyObject *const_str_plain_urllib;
static PyObject *const_str_digest_b8e8d133fb615b98bbf54fba1416cc4d;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_digest_0a0424cff781b765f01c51300bb05782;
static PyObject *const_tuple_list_type_str_list_type_str_tuple;
extern PyObject *const_str_plain_https;
static PyObject *const_str_plain_unicode_map;
extern PyObject *const_str_chr_39;
static PyObject *const_tuple_str_digest_54e10fe59f47c2b318a42cad76bfb3bf_tuple;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_href;
static PyObject *const_tuple_str_chr_43_str_space_tuple;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_chr_35;
extern PyObject *const_str_plain_overload;
extern PyObject *const_tuple_str_plain_latin1_tuple;
extern PyObject *const_str_digest_f5862b11e62dc1382b5d1226dd5ff379;
static PyObject *const_str_digest_e3cd4610161aba9eaa28da8430bf368e;
extern PyObject *const_str_plain_linkify;
extern PyObject *const_tuple_none_type_str_type_bytes_tuple;
extern PyObject *const_int_pos_8;
static PyObject *const_str_digest_c6564c886ff8a30837617a6ed3437441;
extern PyObject *const_str_plain_parts;
extern PyObject *const_tuple_str_dot_tuple;
extern PyObject *const_str_digest_3501979af1b70861f5e9d6a0f04129bf;
extern PyObject *const_str_plain_url_unescape;
static PyObject *const_str_digest_5f510ad45c7784edef65d44eb17e31f8;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_digest_945228044f484d0c86a3cdf3a6e4cbbd;
extern PyObject *const_str_space;
extern PyObject *const_str_digest_9e22f855d491619d56677fec5533ea95;
extern PyObject *const_str_plain_name;
static PyObject *const_str_plain__HTML_UNICODE_MAP;
extern PyObject *const_str_plain_errors;
static PyObject *const_str_digest_44838993fea9df39d3b7bc798cbee383;
static PyObject *const_str_plain_require_protocol;
extern PyObject *const_str_plain_Any;
extern PyObject *const_tuple_str_chr_38_tuple;
static PyObject *const_str_digest_3d735498de5062618ac9d67223655804;
static PyObject *const_str_plain__XHTML_ESCAPE_RE;
extern PyObject *const_str_plain_compile;
static PyObject *const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple;
extern PyObject *const_str_plain_squeeze;
static PyObject *const_str_digest_f41e016f8aa911562fdcf3a6a6a5e612;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_proto;
extern PyObject *const_str_plain_match;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_digest_f8a05ce6419eb433bc14bb5bfb3e62be;
extern PyObject *const_str_plain_decode;
extern PyObject *const_str_plain_False;
extern PyObject *const_tuple_str_plain_unicode_type_tuple;
extern PyObject *const_str_plain_k;
extern PyObject *const_tuple_int_pos_3_tuple;
extern PyObject *const_slice_none_int_pos_1_none;
static PyObject *const_tuple_str_plain_value_str_plain_encoding_str_plain_plus_tuple;
static PyObject *const_str_plain_xhtml_unescape;
extern PyObject *const_tuple_str_chr_63_tuple;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
extern PyObject *const_str_plain_strict;
extern PyObject *const_str_plain_list;
static PyObject *const_str_plain_permitted_protocols;
extern PyObject *const_str_plain_encoding;
static PyObject *const_str_plain__build_unicode_map;
extern PyObject *const_str_plain_unquote_plus;
extern PyObject *const_int_0;
extern PyObject *const_slice_none_int_pos_8_none;
static PyObject *const_dict_30bf287ffc06b1900ccad90e04ffd31f;
static PyObject *const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple;
extern PyObject *const_str_plain_keep_blank_values;
extern PyObject *const_tuple_str_plain_m_tuple;
extern PyObject *const_str_plain_dumps;
extern PyObject *const_tuple_int_pos_2_tuple;
extern PyObject *const_str_plain_text;
static PyObject *const_str_digest_2b81e333423fe686031ad0e58e5b971f;
static PyObject *const_str_digest_4d6910c1b1b9b2832ed31e1aa91dd2b0;
extern PyObject *const_str_plain_origin;
extern PyObject *const_list_str_plain_http_str_plain_https_list;
static PyObject *const_str_digest_0a965353587989cc5fc3cd0b4a3488a4;
static PyObject *const_str_digest_19e50c71cf2d846e79def17f96a4688e;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_plain_html;
static PyObject *const_str_plain_name2codepoint;
static PyObject *const_str_plain_json_decode;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_digest_25fd5ead3f086addba74ff5e559ee564;
static PyObject *const_str_plain_max_len;
static PyObject *const_str_digest_70c7667bf24de28cd03ec3fd3eaa87e1;
extern PyObject *const_tuple_b0c00933e83a0151953f3a00c2178204_tuple;
extern PyObject *const_str_angle_lambda;
static PyObject *const_str_plain_entities;
extern PyObject *const_str_plain_unquote;
extern PyObject *const_str_plain_unicode_type;
extern PyObject *const_tuple_type_str_type_str_tuple;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_tuple_str_plain_match_tuple;
extern PyObject *const_str_plain_v;
extern PyObject *const_str_plain_encoded;
static PyObject *const_slice_none_int_pos_30_none;
extern PyObject *const_str_plain_Union;
extern PyObject *const_str_plain_plus;
extern PyObject *const_str_chr_47;
extern PyObject *const_str_plain_utf8;
static PyObject *const_tuple_str_digest_c6564c886ff8a30837617a6ed3437441_tuple;
extern PyObject *const_str_plain_unquote_to_bytes;
extern PyObject *const_str_plain_url_escape;
extern PyObject *const_str_plain_params;
static PyObject *const_str_digest_93388c71b47089bccb0fc69365887500;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_replace;
extern PyObject *const_str_plain_parse_qs_bytes;
extern PyObject *const_str_plain_parse_qs;
extern PyObject *const_str_plain_qs;
static PyObject *const_tuple_56b5d8cec332f08075953109a25f8ca5_tuple;
extern PyObject *const_str_plain_tuple;
static PyObject *const_str_plain__TO_UNICODE_TYPES;
extern PyObject *const_tuple_str_plain_obj_tuple;
static PyObject *const_str_digest_2551c1e4e59cf8c15b7d64978f8ef587;
extern PyObject *const_tuple_str_chr_47_tuple;
static PyObject *const_dict_5adb0e5ad5df1b9a044047b0bcbbda85;
extern PyObject *const_str_chr_34;
extern PyObject *const_int_pos_3;
static PyObject *const_str_plain_amp;
static PyObject *const_str_plain_to_basestring;
extern PyObject *const_str_plain_parse;
extern PyObject *const_str_plain_url;
static PyObject *const_str_digest_499ae5223bd071deb12fed5e6db084f2;
static PyObject *const_str_digest_83c3338071788a985dd4e30e51ae98c4;
extern PyObject *const_str_plain_loads;
static PyObject *const_dict_231f0a9ff30c1cff2216ed4aa4c3c88e;
extern PyObject *const_str_plain_has_location;
static PyObject *const_dict_0d51aaa043ae9db1f2dbb75fac333587;
static PyObject *const_tuple_97607d55253c54dd862fed7d2a27401d_tuple;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_http;
static PyObject *const_str_plain__convert_entity;
static PyObject *const_str_digest_3cc5261cf52c88f0c57b29fa3bd967e3;
extern PyObject *const_str_plain_obj;
static PyObject *const_str_plain__UTF8_TYPES;
extern PyObject *const_str_empty;
static PyObject *const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_true_tuple;
extern PyObject *const_str_plain_native_str;
extern PyObject *const_str_plain_quote_plus;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_7e12d8e7d8726364c4d8827b8ca157eb = UNSTREAM_STRING_ASCII( &constant_bin[ 5325605 ], 36, 0 );
    const_list_type_str_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_type_str_list, 0, (PyObject *)&PyUnicode_Type ); Py_INCREF( (PyObject *)&PyUnicode_Type );
    const_str_plain__URL_RE = UNSTREAM_STRING_ASCII( &constant_bin[ 5325641 ], 7, 1 );
    const_str_plain_proto_len = UNSTREAM_STRING_ASCII( &constant_bin[ 5325648 ], 9, 1 );
    const_str_digest_b4cc80237db0fadc7e9411b7e60e8a1d = UNSTREAM_STRING_ASCII( &constant_bin[ 5325657 ], 23, 0 );
    const_str_digest_fed3a5498cbc1161ae9286cb8a8ce2c4 = UNSTREAM_STRING_ASCII( &constant_bin[ 5325680 ], 287, 0 );
    const_str_digest_cd3210c70bf2f05624551f1b595530e4 = UNSTREAM_STRING_ASCII( &constant_bin[ 5325967 ], 37, 0 );
    const_str_plain_extra_params = UNSTREAM_STRING_ASCII( &constant_bin[ 5326004 ], 12, 1 );
    const_str_digest_923e74d046a976c7e2f576a3b2a89286 = UNSTREAM_STRING_ASCII( &constant_bin[ 5326016 ], 30, 0 );
    const_str_digest_a13637e8cd7cb8c057e4d409319b229a = UNSTREAM_STRING_ASCII( &constant_bin[ 5326046 ], 62, 0 );
    const_str_plain_recursive_unicode = UNSTREAM_STRING_ASCII( &constant_bin[ 5325605 ], 17, 1 );
    const_tuple_str_plain_value_str_plain_plus_str_plain_quote_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_plus_str_plain_quote_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_plus_str_plain_quote_tuple, 1, const_str_plain_plus ); Py_INCREF( const_str_plain_plus );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_plus_str_plain_quote_tuple, 2, const_str_plain_quote ); Py_INCREF( const_str_plain_quote );
    const_str_plain_make_link = UNSTREAM_STRING_ASCII( &constant_bin[ 5326108 ], 9, 1 );
    const_dict_8a2a7d46105ecf8375ebf4493d3a5935 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_8a2a7d46105ecf8375ebf4493d3a5935, const_str_plain_value, (PyObject *)&PyUnicode_Type );
    PyDict_SetItem( const_dict_8a2a7d46105ecf8375ebf4493d3a5935, const_str_plain_return, (PyObject *)&PyUnicode_Type );
    assert( PyDict_Size( const_dict_8a2a7d46105ecf8375ebf4493d3a5935 ) == 2 );
    const_dict_7b0c1fe6e32c976caf2154377bedae8e = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_7b0c1fe6e32c976caf2154377bedae8e, const_str_plain_encoding, const_str_plain_latin1 );
    PyDict_SetItem( const_dict_7b0c1fe6e32c976caf2154377bedae8e, const_str_plain_errors, const_str_plain_strict );
    assert( PyDict_Size( const_dict_7b0c1fe6e32c976caf2154377bedae8e ) == 2 );
    const_str_digest_da233aeba81e00ca61b42b1678245594 = UNSTREAM_STRING_ASCII( &constant_bin[ 5326117 ], 12, 0 );
    const_tuple_825449a2a10def72445ca4a66750ebbb_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_825449a2a10def72445ca4a66750ebbb_tuple, 0, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    const_str_plain_shorten = UNSTREAM_STRING_ASCII( &constant_bin[ 1092018 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_825449a2a10def72445ca4a66750ebbb_tuple, 1, const_str_plain_shorten ); Py_INCREF( const_str_plain_shorten );
    PyTuple_SET_ITEM( const_tuple_825449a2a10def72445ca4a66750ebbb_tuple, 2, const_str_plain_extra_params ); Py_INCREF( const_str_plain_extra_params );
    const_str_plain_require_protocol = UNSTREAM_STRING_ASCII( &constant_bin[ 5326129 ], 16, 1 );
    PyTuple_SET_ITEM( const_tuple_825449a2a10def72445ca4a66750ebbb_tuple, 3, const_str_plain_require_protocol ); Py_INCREF( const_str_plain_require_protocol );
    const_str_plain_permitted_protocols = UNSTREAM_STRING_ASCII( &constant_bin[ 5326145 ], 19, 1 );
    PyTuple_SET_ITEM( const_tuple_825449a2a10def72445ca4a66750ebbb_tuple, 4, const_str_plain_permitted_protocols ); Py_INCREF( const_str_plain_permitted_protocols );
    PyTuple_SET_ITEM( const_tuple_825449a2a10def72445ca4a66750ebbb_tuple, 5, const_str_plain_make_link ); Py_INCREF( const_str_plain_make_link );
    const_str_digest_3fdba945c17629f91d696fe3fa5b508a = UNSTREAM_STRING_ASCII( &constant_bin[ 5326164 ], 196, 0 );
    const_dict_0755fb5af920d0ea293e582e7b7e51f2 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_0755fb5af920d0ea293e582e7b7e51f2, const_str_plain_value, Py_None );
    PyDict_SetItem( const_dict_0755fb5af920d0ea293e582e7b7e51f2, const_str_plain_return, Py_None );
    assert( PyDict_Size( const_dict_0755fb5af920d0ea293e582e7b7e51f2 ) == 2 );
    const_str_plain_before_clip = UNSTREAM_STRING_ASCII( &constant_bin[ 5326360 ], 11, 1 );
    const_str_digest_54e10fe59f47c2b318a42cad76bfb3bf = UNSTREAM_STRING_ASCII( &constant_bin[ 5326371 ], 7, 0 );
    const_str_plain__XHTML_ESCAPE_DICT = UNSTREAM_STRING_ASCII( &constant_bin[ 5326378 ], 18, 1 );
    const_tuple_94ab84002ba56c0f19e558b6ab3642d9_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_94ab84002ba56c0f19e558b6ab3642d9_tuple, 0, const_str_digest_9e22f855d491619d56677fec5533ea95 ); Py_INCREF( const_str_digest_9e22f855d491619d56677fec5533ea95 );
    const_str_digest_2b81e333423fe686031ad0e58e5b971f = UNSTREAM_STRING_ASCII( &constant_bin[ 2714137 ], 3, 0 );
    PyTuple_SET_ITEM( const_tuple_94ab84002ba56c0f19e558b6ab3642d9_tuple, 1, const_str_digest_2b81e333423fe686031ad0e58e5b971f ); Py_INCREF( const_str_digest_2b81e333423fe686031ad0e58e5b971f );
    const_str_digest_51fd4efe30577acc0163b2fc96ab3c75 = UNSTREAM_STRING_ASCII( &constant_bin[ 5326396 ], 304, 0 );
    const_str_plain_strict_parsing = UNSTREAM_STRING_ASCII( &constant_bin[ 5326700 ], 14, 1 );
    const_tuple_str_plain_unicode_map_str_plain_name_str_plain_value_tuple = PyTuple_New( 3 );
    const_str_plain_unicode_map = UNSTREAM_STRING_ASCII( &constant_bin[ 5326714 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_unicode_map_str_plain_name_str_plain_value_tuple, 0, const_str_plain_unicode_map ); Py_INCREF( const_str_plain_unicode_map );
    PyTuple_SET_ITEM( const_tuple_str_plain_unicode_map_str_plain_name_str_plain_value_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_str_plain_unicode_map_str_plain_name_str_plain_value_tuple, 2, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_tuple_type_bytes_anon_NoneType_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_type_bytes_anon_NoneType_tuple, 0, (PyObject *)&PyBytes_Type ); Py_INCREF( (PyObject *)&PyBytes_Type );
    PyTuple_SET_ITEM( const_tuple_type_bytes_anon_NoneType_tuple, 1, (PyObject *)Py_TYPE( Py_None ) ); Py_INCREF( (PyObject *)Py_TYPE( Py_None ) );
    const_str_digest_4a3231db8f125f2cc6fa458948ea5c7b = UNSTREAM_STRING_ASCII( &constant_bin[ 5326725 ], 21, 0 );
    const_str_digest_b8e8d133fb615b98bbf54fba1416cc4d = UNSTREAM_STRING_ASCII( &constant_bin[ 5326746 ], 17, 0 );
    const_tuple_list_type_str_list_type_str_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_list_type_str_list_type_str_tuple, 0, const_list_type_str_list ); Py_INCREF( const_list_type_str_list );
    PyTuple_SET_ITEM( const_tuple_list_type_str_list_type_str_tuple, 1, (PyObject *)&PyUnicode_Type ); Py_INCREF( (PyObject *)&PyUnicode_Type );
    const_tuple_str_digest_54e10fe59f47c2b318a42cad76bfb3bf_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_54e10fe59f47c2b318a42cad76bfb3bf_tuple, 0, const_str_digest_54e10fe59f47c2b318a42cad76bfb3bf ); Py_INCREF( const_str_digest_54e10fe59f47c2b318a42cad76bfb3bf );
    const_tuple_str_chr_43_str_space_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_chr_43_str_space_tuple, 0, const_str_chr_43 ); Py_INCREF( const_str_chr_43 );
    PyTuple_SET_ITEM( const_tuple_str_chr_43_str_space_tuple, 1, const_str_space ); Py_INCREF( const_str_space );
    const_str_digest_e3cd4610161aba9eaa28da8430bf368e = UNSTREAM_STRING_ASCII( &constant_bin[ 2715980 ], 11, 0 );
    const_str_digest_c6564c886ff8a30837617a6ed3437441 = UNSTREAM_STRING_ASCII( &constant_bin[ 5326763 ], 141, 0 );
    const_str_digest_5f510ad45c7784edef65d44eb17e31f8 = UNSTREAM_STRING_ASCII( &constant_bin[ 5326904 ], 40, 0 );
    const_str_plain__HTML_UNICODE_MAP = UNSTREAM_STRING_ASCII( &constant_bin[ 5326944 ], 17, 1 );
    const_str_digest_44838993fea9df39d3b7bc798cbee383 = UNSTREAM_STRING_ASCII( &constant_bin[ 5326961 ], 99, 0 );
    const_str_digest_3d735498de5062618ac9d67223655804 = UNSTREAM_STRING_ASCII( &constant_bin[ 5327060 ], 26, 0 );
    const_str_plain__XHTML_ESCAPE_RE = UNSTREAM_STRING_ASCII( &constant_bin[ 5327086 ], 16, 1 );
    const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple, 0, const_str_plain_qs ); Py_INCREF( const_str_plain_qs );
    PyTuple_SET_ITEM( const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple, 1, const_str_plain_keep_blank_values ); Py_INCREF( const_str_plain_keep_blank_values );
    PyTuple_SET_ITEM( const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple, 2, const_str_plain_strict_parsing ); Py_INCREF( const_str_plain_strict_parsing );
    PyTuple_SET_ITEM( const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple, 3, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    PyTuple_SET_ITEM( const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple, 4, const_str_plain_encoded ); Py_INCREF( const_str_plain_encoded );
    PyTuple_SET_ITEM( const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple, 5, const_str_plain_k ); Py_INCREF( const_str_plain_k );
    PyTuple_SET_ITEM( const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple, 6, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    const_str_digest_f41e016f8aa911562fdcf3a6a6a5e612 = UNSTREAM_STRING_ASCII( &constant_bin[ 5327102 ], 200, 0 );
    const_tuple_str_plain_value_str_plain_encoding_str_plain_plus_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_encoding_str_plain_plus_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_encoding_str_plain_plus_tuple, 1, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_encoding_str_plain_plus_tuple, 2, const_str_plain_plus ); Py_INCREF( const_str_plain_plus );
    const_str_plain_xhtml_unescape = UNSTREAM_STRING_ASCII( &constant_bin[ 5327302 ], 14, 1 );
    const_str_plain__build_unicode_map = UNSTREAM_STRING_ASCII( &constant_bin[ 5327316 ], 18, 1 );
    const_dict_30bf287ffc06b1900ccad90e04ffd31f = _PyDict_NewPresized( 5 );
    PyDict_SetItem( const_dict_30bf287ffc06b1900ccad90e04ffd31f, const_str_chr_38, const_str_digest_945228044f484d0c86a3cdf3a6e4cbbd );
    PyDict_SetItem( const_dict_30bf287ffc06b1900ccad90e04ffd31f, const_str_chr_60, const_str_digest_25fd5ead3f086addba74ff5e559ee564 );
    PyDict_SetItem( const_dict_30bf287ffc06b1900ccad90e04ffd31f, const_str_chr_62, const_str_digest_f8a05ce6419eb433bc14bb5bfb3e62be );
    PyDict_SetItem( const_dict_30bf287ffc06b1900ccad90e04ffd31f, const_str_chr_34, const_str_digest_0a0424cff781b765f01c51300bb05782 );
    PyDict_SetItem( const_dict_30bf287ffc06b1900ccad90e04ffd31f, const_str_chr_39, const_str_digest_a3d81feff0be3a3ff360ca33ad836962 );
    assert( PyDict_Size( const_dict_30bf287ffc06b1900ccad90e04ffd31f ) == 5 );
    const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple = PyTuple_New( 14 );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 0, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 1, const_str_plain_url ); Py_INCREF( const_str_plain_url );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 2, const_str_plain_proto ); Py_INCREF( const_str_plain_proto );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 3, const_str_plain_href ); Py_INCREF( const_str_plain_href );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 4, const_str_plain_params ); Py_INCREF( const_str_plain_params );
    const_str_plain_max_len = UNSTREAM_STRING_ASCII( &constant_bin[ 5327334 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 5, const_str_plain_max_len ); Py_INCREF( const_str_plain_max_len );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 6, const_str_plain_before_clip ); Py_INCREF( const_str_plain_before_clip );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 7, const_str_plain_proto_len ); Py_INCREF( const_str_plain_proto_len );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 8, const_str_plain_parts ); Py_INCREF( const_str_plain_parts );
    const_str_plain_amp = UNSTREAM_STRING_ASCII( &constant_bin[ 6841 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 9, const_str_plain_amp ); Py_INCREF( const_str_plain_amp );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 10, const_str_plain_require_protocol ); Py_INCREF( const_str_plain_require_protocol );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 11, const_str_plain_permitted_protocols ); Py_INCREF( const_str_plain_permitted_protocols );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 12, const_str_plain_extra_params ); Py_INCREF( const_str_plain_extra_params );
    PyTuple_SET_ITEM( const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 13, const_str_plain_shorten ); Py_INCREF( const_str_plain_shorten );
    const_str_digest_4d6910c1b1b9b2832ed31e1aa91dd2b0 = UNSTREAM_STRING_ASCII( &constant_bin[ 5327341 ], 1163, 0 );
    const_str_digest_0a965353587989cc5fc3cd0b4a3488a4 = UNSTREAM_STRING_ASCII( &constant_bin[ 5328504 ], 4, 0 );
    const_str_digest_19e50c71cf2d846e79def17f96a4688e = UNSTREAM_STRING_ASCII( &constant_bin[ 5328508 ], 361, 0 );
    const_str_plain_name2codepoint = UNSTREAM_STRING_ASCII( &constant_bin[ 5328869 ], 14, 1 );
    const_str_plain_json_decode = UNSTREAM_STRING_ASCII( &constant_bin[ 2113155 ], 11, 1 );
    const_str_digest_70c7667bf24de28cd03ec3fd3eaa87e1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5328883 ], 118, 0 );
    const_str_plain_entities = UNSTREAM_STRING_ASCII( &constant_bin[ 581717 ], 8, 1 );
    const_slice_none_int_pos_30_none = PySlice_New( Py_None, const_int_pos_30, Py_None );
    const_tuple_str_digest_c6564c886ff8a30837617a6ed3437441_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_c6564c886ff8a30837617a6ed3437441_tuple, 0, const_str_digest_c6564c886ff8a30837617a6ed3437441 ); Py_INCREF( const_str_digest_c6564c886ff8a30837617a6ed3437441 );
    const_str_digest_93388c71b47089bccb0fc69365887500 = UNSTREAM_STRING_ASCII( &constant_bin[ 5329001 ], 5, 0 );
    const_tuple_56b5d8cec332f08075953109a25f8ca5_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_56b5d8cec332f08075953109a25f8ca5_tuple, 0, const_str_plain_Union ); Py_INCREF( const_str_plain_Union );
    PyTuple_SET_ITEM( const_tuple_56b5d8cec332f08075953109a25f8ca5_tuple, 1, const_str_plain_Any ); Py_INCREF( const_str_plain_Any );
    PyTuple_SET_ITEM( const_tuple_56b5d8cec332f08075953109a25f8ca5_tuple, 2, const_str_plain_Optional ); Py_INCREF( const_str_plain_Optional );
    PyTuple_SET_ITEM( const_tuple_56b5d8cec332f08075953109a25f8ca5_tuple, 3, const_str_plain_Dict ); Py_INCREF( const_str_plain_Dict );
    PyTuple_SET_ITEM( const_tuple_56b5d8cec332f08075953109a25f8ca5_tuple, 4, const_str_plain_List ); Py_INCREF( const_str_plain_List );
    PyTuple_SET_ITEM( const_tuple_56b5d8cec332f08075953109a25f8ca5_tuple, 5, const_str_plain_Callable ); Py_INCREF( const_str_plain_Callable );
    const_str_plain__TO_UNICODE_TYPES = UNSTREAM_STRING_ASCII( &constant_bin[ 5329006 ], 17, 1 );
    const_str_digest_2551c1e4e59cf8c15b7d64978f8ef587 = UNSTREAM_STRING_ASCII( &constant_bin[ 5329023 ], 606, 0 );
    const_dict_5adb0e5ad5df1b9a044047b0bcbbda85 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_5adb0e5ad5df1b9a044047b0bcbbda85, const_str_plain_value, (PyObject *)&PyUnicode_Type );
    PyDict_SetItem( const_dict_5adb0e5ad5df1b9a044047b0bcbbda85, const_str_plain_return, (PyObject *)&PyBytes_Type );
    assert( PyDict_Size( const_dict_5adb0e5ad5df1b9a044047b0bcbbda85 ) == 2 );
    const_str_plain_to_basestring = UNSTREAM_STRING_ASCII( &constant_bin[ 5329629 ], 13, 1 );
    const_str_digest_499ae5223bd071deb12fed5e6db084f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5329642 ], 12, 0 );
    const_str_digest_83c3338071788a985dd4e30e51ae98c4 = UNSTREAM_STRING_ASCII( &constant_bin[ 5329654 ], 163, 0 );
    const_dict_231f0a9ff30c1cff2216ed4aa4c3c88e = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_231f0a9ff30c1cff2216ed4aa4c3c88e, const_str_plain_value, (PyObject *)&PyBytes_Type );
    PyDict_SetItem( const_dict_231f0a9ff30c1cff2216ed4aa4c3c88e, const_str_plain_return, (PyObject *)&PyUnicode_Type );
    assert( PyDict_Size( const_dict_231f0a9ff30c1cff2216ed4aa4c3c88e ) == 2 );
    const_dict_0d51aaa043ae9db1f2dbb75fac333587 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_0d51aaa043ae9db1f2dbb75fac333587, const_str_plain_value, (PyObject *)&PyBytes_Type );
    PyDict_SetItem( const_dict_0d51aaa043ae9db1f2dbb75fac333587, const_str_plain_return, (PyObject *)&PyBytes_Type );
    assert( PyDict_Size( const_dict_0d51aaa043ae9db1f2dbb75fac333587 ) == 2 );
    const_tuple_97607d55253c54dd862fed7d2a27401d_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_97607d55253c54dd862fed7d2a27401d_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_97607d55253c54dd862fed7d2a27401d_tuple, 1, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    PyTuple_SET_ITEM( const_tuple_97607d55253c54dd862fed7d2a27401d_tuple, 2, const_str_plain_plus ); Py_INCREF( const_str_plain_plus );
    PyTuple_SET_ITEM( const_tuple_97607d55253c54dd862fed7d2a27401d_tuple, 3, const_str_plain_unquote ); Py_INCREF( const_str_plain_unquote );
    const_str_plain__convert_entity = UNSTREAM_STRING_ASCII( &constant_bin[ 5329817 ], 15, 1 );
    const_str_digest_3cc5261cf52c88f0c57b29fa3bd967e3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5329832 ], 33, 0 );
    const_str_plain__UTF8_TYPES = UNSTREAM_STRING_ASCII( &constant_bin[ 5329865 ], 11, 1 );
    const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_true_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_true_tuple, 0, const_str_digest_c075052d723d6707083e869a0e3659bb ); Py_INCREF( const_str_digest_c075052d723d6707083e869a0e3659bb );
    PyTuple_SET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_true_tuple, 1, Py_True ); Py_INCREF( Py_True );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_tornado$escape( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_6e047f9504cc26b3d1d05a1c4c8e67ad;
static PyCodeObject *codeobj_98627c45100bf0003c00ce067707cdf5;
static PyCodeObject *codeobj_8c45b806923f1e580927d3a29be76c7f;
static PyCodeObject *codeobj_a27444012d0c3a96ef98053d148cd811;
static PyCodeObject *codeobj_9178d1450e157b672bf8147b6b4641b5;
static PyCodeObject *codeobj_a74e88def304d1fbe08fb4688630b4c4;
static PyCodeObject *codeobj_f8e2b05f388388dd3c768271cd977b2f;
static PyCodeObject *codeobj_c5446992fcad4079a33861b5cefae205;
static PyCodeObject *codeobj_576ea3dc5ed3c857bd8c79ec4dcf0577;
static PyCodeObject *codeobj_31922533793f55aa78505742334ebb48;
static PyCodeObject *codeobj_53cf9b7ffbe8ea9a7750e9bad0c565d9;
static PyCodeObject *codeobj_bccf81b17a0b884150a8eafb0fd57813;
static PyCodeObject *codeobj_c9bbec91076591a2b9c659a133d35e3a;
static PyCodeObject *codeobj_7383220ca4a74759df7b67f7b0e48cb7;
static PyCodeObject *codeobj_8ebed34ed6c5d940d9936d13171977c1;
static PyCodeObject *codeobj_da713263cce893ee9f551664e2ffb3f4;
static PyCodeObject *codeobj_54dc8c426c974d5a18757df31e39e82e;
static PyCodeObject *codeobj_783c509865856b70579d4dcb9007eaab;
static PyCodeObject *codeobj_be813ac06ef379d66594c8be487f9abc;
static PyCodeObject *codeobj_85917ccd43032c186ec182f453f4f39f;
static PyCodeObject *codeobj_e69d731bae6cdc1640635b65955e830f;
static PyCodeObject *codeobj_77d18306c7988cfa785ee2b705ef4f5f;
static PyCodeObject *codeobj_e80c64282c52818be9dfd1256a205f0f;
static PyCodeObject *codeobj_35ae2d10928327a4980ded94705f9a6d;
static PyCodeObject *codeobj_f7b4c7467b5095ddd58b86565ca9250a;
static PyCodeObject *codeobj_a70f8a699631d4222d3597a3ef735d6f;
static PyCodeObject *codeobj_d58793142ff8d50205aeba9863150c26;
static PyCodeObject *codeobj_0f6e65a8c04585fbfadac2f6fca1e53b;
static PyCodeObject *codeobj_7569a0352786d2405fe05a51c3671a5c;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_b8e8d133fb615b98bbf54fba1416cc4d );
    codeobj_6e047f9504cc26b3d1d05a1c4c8e67ad = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 250, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_i_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_98627c45100bf0003c00ce067707cdf5 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 252, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_i_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8c45b806923f1e580927d3a29be76c7f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 247, const_tuple_b0c00933e83a0151953f3a00c2178204_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a27444012d0c3a96ef98053d148cd811 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 55, const_tuple_str_plain_match_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9178d1450e157b672bf8147b6b4641b5 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 164, const_tuple_str_plain_i_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a74e88def304d1fbe08fb4688630b4c4 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_b4cc80237db0fadc7e9411b7e60e8a1d, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_f8e2b05f388388dd3c768271cd977b2f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__build_unicode_map, 393, const_tuple_str_plain_unicode_map_str_plain_name_str_plain_value_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c5446992fcad4079a33861b5cefae205 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__convert_entity, 378, const_tuple_str_plain_m_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_576ea3dc5ed3c857bd8c79ec4dcf0577 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_json_decode, 78, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_31922533793f55aa78505742334ebb48 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_json_encode, 67, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_53cf9b7ffbe8ea9a7750e9bad0c565d9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_linkify, 273, const_tuple_825449a2a10def72445ca4a66750ebbb_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bccf81b17a0b884150a8eafb0fd57813 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_link, 312, const_tuple_c9f13adba42cd27802d2776655a7e17c_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_c9bbec91076591a2b9c659a133d35e3a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_parse_qs_bytes, 147, const_tuple_871e2d0e0c27848619d4b9f01924cd73_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7383220ca4a74759df7b67f7b0e48cb7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_recursive_unicode, 240, const_tuple_str_plain_obj_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8ebed34ed6c5d940d9936d13171977c1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_squeeze, 86, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_da713263cce893ee9f551664e2ffb3f4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_to_unicode, 202, const_tuple_str_plain_value_tuple, 1, 0, CO_NOFREE );
    codeobj_54dc8c426c974d5a18757df31e39e82e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_to_unicode, 207, const_tuple_str_plain_value_tuple, 1, 0, CO_NOFREE );
    codeobj_783c509865856b70579d4dcb9007eaab = MAKE_CODEOBJ( module_filename_obj, const_str_plain_to_unicode, 212, const_tuple_str_plain_value_tuple, 1, 0, CO_NOFREE );
    codeobj_be813ac06ef379d66594c8be487f9abc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_to_unicode, 217, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_85917ccd43032c186ec182f453f4f39f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_url_escape, 91, const_tuple_str_plain_value_str_plain_plus_str_plain_quote_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e69d731bae6cdc1640635b65955e830f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_url_unescape, 106, const_tuple_str_plain_value_str_plain_encoding_str_plain_plus_tuple, 3, 0, CO_NOFREE );
    codeobj_77d18306c7988cfa785ee2b705ef4f5f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_url_unescape, 111, const_tuple_str_plain_value_str_plain_encoding_str_plain_plus_tuple, 3, 0, CO_NOFREE );
    codeobj_e80c64282c52818be9dfd1256a205f0f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_url_unescape, 118, const_tuple_97607d55253c54dd862fed7d2a27401d_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_35ae2d10928327a4980ded94705f9a6d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_utf8, 171, const_tuple_str_plain_value_tuple, 1, 0, CO_NOFREE );
    codeobj_f7b4c7467b5095ddd58b86565ca9250a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_utf8, 176, const_tuple_str_plain_value_tuple, 1, 0, CO_NOFREE );
    codeobj_a70f8a699631d4222d3597a3ef735d6f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_utf8, 181, const_tuple_str_plain_value_tuple, 1, 0, CO_NOFREE );
    codeobj_d58793142ff8d50205aeba9863150c26 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_utf8, 186, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0f6e65a8c04585fbfadac2f6fca1e53b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_xhtml_escape, 43, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7569a0352786d2405fe05a51c3671a5c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_xhtml_unescape, 59, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *tornado$escape$$$function_19_recursive_unicode$$$genexpr_1_genexpr_maker( void );


static PyObject *tornado$escape$$$function_19_recursive_unicode$$$genexpr_2_genexpr_maker( void );


static PyObject *tornado$escape$$$function_19_recursive_unicode$$$genexpr_3_genexpr_maker( void );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_10_parse_qs_bytes( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_11_utf8( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_12_utf8( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_13_utf8( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_14_utf8( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_15_to_unicode( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_16_to_unicode( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_17_to_unicode( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_18_to_unicode( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_19_recursive_unicode( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_1_xhtml_escape( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_1_xhtml_escape$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_20_linkify( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_20_linkify$$$function_1_make_link( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_21__convert_entity( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_22__build_unicode_map( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_2_xhtml_unescape( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_3_json_encode( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_4_json_decode( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_5_squeeze( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_6_url_escape( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_7_url_unescape( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_8_url_unescape( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$escape$$$function_9_url_unescape( PyObject *defaults, PyObject *annotations );


// The module function definitions.
static PyObject *impl_tornado$escape$$$function_1_xhtml_escape( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0f6e65a8c04585fbfadac2f6fca1e53b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0f6e65a8c04585fbfadac2f6fca1e53b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0f6e65a8c04585fbfadac2f6fca1e53b, codeobj_0f6e65a8c04585fbfadac2f6fca1e53b, module_tornado$escape, sizeof(void *) );
    frame_0f6e65a8c04585fbfadac2f6fca1e53b = cache_frame_0f6e65a8c04585fbfadac2f6fca1e53b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0f6e65a8c04585fbfadac2f6fca1e53b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0f6e65a8c04585fbfadac2f6fca1e53b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__XHTML_ESCAPE_RE );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__XHTML_ESCAPE_RE );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_XHTML_ESCAPE_RE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sub );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = MAKE_FUNCTION_tornado$escape$$$function_1_xhtml_escape$$$function_1_lambda(  );



        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_basestring );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_to_basestring );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "to_basestring" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_3 = par_value;
        frame_0f6e65a8c04585fbfadac2f6fca1e53b->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_0f6e65a8c04585fbfadac2f6fca1e53b->m_frame.f_lineno = 54;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0f6e65a8c04585fbfadac2f6fca1e53b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0f6e65a8c04585fbfadac2f6fca1e53b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0f6e65a8c04585fbfadac2f6fca1e53b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0f6e65a8c04585fbfadac2f6fca1e53b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0f6e65a8c04585fbfadac2f6fca1e53b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0f6e65a8c04585fbfadac2f6fca1e53b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0f6e65a8c04585fbfadac2f6fca1e53b,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_0f6e65a8c04585fbfadac2f6fca1e53b == cache_frame_0f6e65a8c04585fbfadac2f6fca1e53b )
    {
        Py_DECREF( frame_0f6e65a8c04585fbfadac2f6fca1e53b );
    }
    cache_frame_0f6e65a8c04585fbfadac2f6fca1e53b = NULL;

    assertFrameObject( frame_0f6e65a8c04585fbfadac2f6fca1e53b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_1_xhtml_escape );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_1_xhtml_escape );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_1_xhtml_escape$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_match = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a27444012d0c3a96ef98053d148cd811;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a27444012d0c3a96ef98053d148cd811 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a27444012d0c3a96ef98053d148cd811, codeobj_a27444012d0c3a96ef98053d148cd811, module_tornado$escape, sizeof(void *) );
    frame_a27444012d0c3a96ef98053d148cd811 = cache_frame_a27444012d0c3a96ef98053d148cd811;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a27444012d0c3a96ef98053d148cd811 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a27444012d0c3a96ef98053d148cd811 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_called_instance_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__XHTML_ESCAPE_DICT );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__XHTML_ESCAPE_DICT );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_XHTML_ESCAPE_DICT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_match );
        tmp_called_instance_1 = par_match;
        frame_a27444012d0c3a96ef98053d148cd811->m_frame.f_lineno = 55;
        tmp_subscript_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_subscript_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a27444012d0c3a96ef98053d148cd811 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a27444012d0c3a96ef98053d148cd811 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a27444012d0c3a96ef98053d148cd811 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a27444012d0c3a96ef98053d148cd811, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a27444012d0c3a96ef98053d148cd811->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a27444012d0c3a96ef98053d148cd811, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a27444012d0c3a96ef98053d148cd811,
        type_description_1,
        par_match
    );


    // Release cached frame.
    if ( frame_a27444012d0c3a96ef98053d148cd811 == cache_frame_a27444012d0c3a96ef98053d148cd811 )
    {
        Py_DECREF( frame_a27444012d0c3a96ef98053d148cd811 );
    }
    cache_frame_a27444012d0c3a96ef98053d148cd811 = NULL;

    assertFrameObject( frame_a27444012d0c3a96ef98053d148cd811 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_1_xhtml_escape$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_match );
    Py_DECREF( par_match );
    par_match = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_match );
    Py_DECREF( par_match );
    par_match = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_1_xhtml_escape$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_2_xhtml_unescape( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7569a0352786d2405fe05a51c3671a5c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7569a0352786d2405fe05a51c3671a5c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7569a0352786d2405fe05a51c3671a5c, codeobj_7569a0352786d2405fe05a51c3671a5c, module_tornado$escape, sizeof(void *) );
    frame_7569a0352786d2405fe05a51c3671a5c = cache_frame_7569a0352786d2405fe05a51c3671a5c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7569a0352786d2405fe05a51c3671a5c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7569a0352786d2405fe05a51c3671a5c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sub );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = const_str_digest_499ae5223bd071deb12fed5e6db084f2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__convert_entity );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__convert_entity );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_convert_entity" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__unicode );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__unicode );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_unicode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_4 = par_value;
        frame_7569a0352786d2405fe05a51c3671a5c->m_frame.f_lineno = 61;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_7569a0352786d2405fe05a51c3671a5c->m_frame.f_lineno = 61;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7569a0352786d2405fe05a51c3671a5c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7569a0352786d2405fe05a51c3671a5c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7569a0352786d2405fe05a51c3671a5c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7569a0352786d2405fe05a51c3671a5c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7569a0352786d2405fe05a51c3671a5c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7569a0352786d2405fe05a51c3671a5c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7569a0352786d2405fe05a51c3671a5c,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_7569a0352786d2405fe05a51c3671a5c == cache_frame_7569a0352786d2405fe05a51c3671a5c )
    {
        Py_DECREF( frame_7569a0352786d2405fe05a51c3671a5c );
    }
    cache_frame_7569a0352786d2405fe05a51c3671a5c = NULL;

    assertFrameObject( frame_7569a0352786d2405fe05a51c3671a5c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_2_xhtml_unescape );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_2_xhtml_unescape );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_3_json_encode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_31922533793f55aa78505742334ebb48;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_31922533793f55aa78505742334ebb48 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_31922533793f55aa78505742334ebb48, codeobj_31922533793f55aa78505742334ebb48, module_tornado$escape, sizeof(void *) );
    frame_31922533793f55aa78505742334ebb48 = cache_frame_31922533793f55aa78505742334ebb48;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_31922533793f55aa78505742334ebb48 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_31922533793f55aa78505742334ebb48 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_json );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 75;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_1 = par_value;
        frame_31922533793f55aa78505742334ebb48->m_frame.f_lineno = 75;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_instance_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_dumps, call_args );
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_31922533793f55aa78505742334ebb48->m_frame.f_lineno = 75;
        tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_94ab84002ba56c0f19e558b6ab3642d9_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_31922533793f55aa78505742334ebb48 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_31922533793f55aa78505742334ebb48 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_31922533793f55aa78505742334ebb48 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_31922533793f55aa78505742334ebb48, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_31922533793f55aa78505742334ebb48->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_31922533793f55aa78505742334ebb48, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_31922533793f55aa78505742334ebb48,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_31922533793f55aa78505742334ebb48 == cache_frame_31922533793f55aa78505742334ebb48 )
    {
        Py_DECREF( frame_31922533793f55aa78505742334ebb48 );
    }
    cache_frame_31922533793f55aa78505742334ebb48 = NULL;

    assertFrameObject( frame_31922533793f55aa78505742334ebb48 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_3_json_encode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_3_json_encode );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_4_json_decode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_576ea3dc5ed3c857bd8c79ec4dcf0577;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_576ea3dc5ed3c857bd8c79ec4dcf0577 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_576ea3dc5ed3c857bd8c79ec4dcf0577, codeobj_576ea3dc5ed3c857bd8c79ec4dcf0577, module_tornado$escape, sizeof(void *) );
    frame_576ea3dc5ed3c857bd8c79ec4dcf0577 = cache_frame_576ea3dc5ed3c857bd8c79ec4dcf0577;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_576ea3dc5ed3c857bd8c79ec4dcf0577 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_576ea3dc5ed3c857bd8c79ec4dcf0577 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_json );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_loads );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_basestring );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_to_basestring );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "to_basestring" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_2 = par_value;
        frame_576ea3dc5ed3c857bd8c79ec4dcf0577->m_frame.f_lineno = 83;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 83;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_576ea3dc5ed3c857bd8c79ec4dcf0577->m_frame.f_lineno = 83;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_576ea3dc5ed3c857bd8c79ec4dcf0577 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_576ea3dc5ed3c857bd8c79ec4dcf0577 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_576ea3dc5ed3c857bd8c79ec4dcf0577 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_576ea3dc5ed3c857bd8c79ec4dcf0577, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_576ea3dc5ed3c857bd8c79ec4dcf0577->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_576ea3dc5ed3c857bd8c79ec4dcf0577, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_576ea3dc5ed3c857bd8c79ec4dcf0577,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_576ea3dc5ed3c857bd8c79ec4dcf0577 == cache_frame_576ea3dc5ed3c857bd8c79ec4dcf0577 )
    {
        Py_DECREF( frame_576ea3dc5ed3c857bd8c79ec4dcf0577 );
    }
    cache_frame_576ea3dc5ed3c857bd8c79ec4dcf0577 = NULL;

    assertFrameObject( frame_576ea3dc5ed3c857bd8c79ec4dcf0577 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_4_json_decode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_4_json_decode );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_5_squeeze( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_8ebed34ed6c5d940d9936d13171977c1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8ebed34ed6c5d940d9936d13171977c1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8ebed34ed6c5d940d9936d13171977c1, codeobj_8ebed34ed6c5d940d9936d13171977c1, module_tornado$escape, sizeof(void *) );
    frame_8ebed34ed6c5d940d9936d13171977c1 = cache_frame_8ebed34ed6c5d940d9936d13171977c1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8ebed34ed6c5d940d9936d13171977c1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8ebed34ed6c5d940d9936d13171977c1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 88;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_1;
        tmp_args_element_name_1 = const_str_digest_da233aeba81e00ca61b42b1678245594;
        tmp_args_element_name_2 = const_str_space;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_3 = par_value;
        frame_8ebed34ed6c5d940d9936d13171977c1->m_frame.f_lineno = 88;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_called_instance_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_2, const_str_plain_sub, call_args );
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_8ebed34ed6c5d940d9936d13171977c1->m_frame.f_lineno = 88;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_strip );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ebed34ed6c5d940d9936d13171977c1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ebed34ed6c5d940d9936d13171977c1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ebed34ed6c5d940d9936d13171977c1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8ebed34ed6c5d940d9936d13171977c1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8ebed34ed6c5d940d9936d13171977c1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8ebed34ed6c5d940d9936d13171977c1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8ebed34ed6c5d940d9936d13171977c1,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_8ebed34ed6c5d940d9936d13171977c1 == cache_frame_8ebed34ed6c5d940d9936d13171977c1 )
    {
        Py_DECREF( frame_8ebed34ed6c5d940d9936d13171977c1 );
    }
    cache_frame_8ebed34ed6c5d940d9936d13171977c1 = NULL;

    assertFrameObject( frame_8ebed34ed6c5d940d9936d13171977c1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_5_squeeze );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_5_squeeze );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_6_url_escape( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *par_plus = python_pars[ 1 ];
    PyObject *var_quote = NULL;
    struct Nuitka_FrameObject *frame_85917ccd43032c186ec182f453f4f39f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_85917ccd43032c186ec182f453f4f39f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_85917ccd43032c186ec182f453f4f39f, codeobj_85917ccd43032c186ec182f453f4f39f, module_tornado$escape, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_85917ccd43032c186ec182f453f4f39f = cache_frame_85917ccd43032c186ec182f453f4f39f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_85917ccd43032c186ec182f453f4f39f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_85917ccd43032c186ec182f453f4f39f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_plus );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_plus );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_urllib );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_urllib );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "urllib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_parse );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_quote_plus );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_urllib );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_urllib );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "urllib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_2;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_parse );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_quote );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        condexpr_end_1:;
        assert( var_quote == NULL );
        var_quote = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_quote );
        tmp_called_name_1 = var_quote;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_utf8 );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_utf8 );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "utf8" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 103;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_2 = par_value;
        frame_85917ccd43032c186ec182f453f4f39f->m_frame.f_lineno = 103;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_85917ccd43032c186ec182f453f4f39f->m_frame.f_lineno = 103;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_85917ccd43032c186ec182f453f4f39f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_85917ccd43032c186ec182f453f4f39f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_85917ccd43032c186ec182f453f4f39f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_85917ccd43032c186ec182f453f4f39f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_85917ccd43032c186ec182f453f4f39f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_85917ccd43032c186ec182f453f4f39f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_85917ccd43032c186ec182f453f4f39f,
        type_description_1,
        par_value,
        par_plus,
        var_quote
    );


    // Release cached frame.
    if ( frame_85917ccd43032c186ec182f453f4f39f == cache_frame_85917ccd43032c186ec182f453f4f39f )
    {
        Py_DECREF( frame_85917ccd43032c186ec182f453f4f39f );
    }
    cache_frame_85917ccd43032c186ec182f453f4f39f = NULL;

    assertFrameObject( frame_85917ccd43032c186ec182f453f4f39f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_6_url_escape );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_plus );
    Py_DECREF( par_plus );
    par_plus = NULL;

    CHECK_OBJECT( (PyObject *)var_quote );
    Py_DECREF( var_quote );
    var_quote = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_plus );
    Py_DECREF( par_plus );
    par_plus = NULL;

    Py_XDECREF( var_quote );
    var_quote = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_6_url_escape );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_7_url_unescape( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *par_encoding = python_pars[ 1 ];
    PyObject *par_plus = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_7_url_unescape );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_plus );
    Py_DECREF( par_plus );
    par_plus = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_plus );
    Py_DECREF( par_plus );
    par_plus = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_7_url_unescape );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_8_url_unescape( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *par_encoding = python_pars[ 1 ];
    PyObject *par_plus = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_8_url_unescape );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_plus );
    Py_DECREF( par_plus );
    par_plus = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_plus );
    Py_DECREF( par_plus );
    par_plus = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_8_url_unescape );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_9_url_unescape( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *par_encoding = python_pars[ 1 ];
    PyObject *par_plus = python_pars[ 2 ];
    PyObject *var_unquote = NULL;
    struct Nuitka_FrameObject *frame_e80c64282c52818be9dfd1256a205f0f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_e80c64282c52818be9dfd1256a205f0f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e80c64282c52818be9dfd1256a205f0f, codeobj_e80c64282c52818be9dfd1256a205f0f, module_tornado$escape, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e80c64282c52818be9dfd1256a205f0f = cache_frame_e80c64282c52818be9dfd1256a205f0f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e80c64282c52818be9dfd1256a205f0f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e80c64282c52818be9dfd1256a205f0f ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_encoding );
        tmp_compexpr_left_1 = par_encoding;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_truth_name_1;
            CHECK_OBJECT( par_plus );
            tmp_truth_name_1 = CHECK_IF_TRUE( par_plus );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_1;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_args_element_name_1;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_basestring );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_to_basestring );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "to_basestring" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 140;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_1 = tmp_mvar_value_1;
                CHECK_OBJECT( par_value );
                tmp_args_element_name_1 = par_value;
                frame_e80c64282c52818be9dfd1256a205f0f->m_frame.f_lineno = 140;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1 };
                    tmp_called_instance_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                frame_e80c64282c52818be9dfd1256a205f0f->m_frame.f_lineno = 140;
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_43_str_space_tuple, 0 ) );

                Py_DECREF( tmp_called_instance_1 );
                if ( tmp_assign_source_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_value;
                    assert( old != NULL );
                    par_value = tmp_assign_source_1;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_urllib );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_urllib );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "urllib" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 141;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_2;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_parse );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_value );
            tmp_args_element_name_2 = par_value;
            frame_e80c64282c52818be9dfd1256a205f0f->m_frame.f_lineno = 141;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_unquote_to_bytes, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            nuitka_bool tmp_condition_result_3;
            int tmp_truth_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_source_name_5;
            PyObject *tmp_mvar_value_4;
            CHECK_OBJECT( par_plus );
            tmp_truth_name_2 = CHECK_IF_TRUE( par_plus );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_1;
            }
            else
            {
                goto condexpr_false_1;
            }
            condexpr_true_1:;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_urllib );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_urllib );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "urllib" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 143;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = tmp_mvar_value_3;
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_parse );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_unquote_plus );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto condexpr_end_1;
            condexpr_false_1:;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_urllib );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_urllib );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "urllib" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 143;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_5 = tmp_mvar_value_4;
            tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_parse );
            if ( tmp_source_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_unquote );
            Py_DECREF( tmp_source_name_4 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            condexpr_end_1:;
            assert( var_unquote == NULL );
            var_unquote = tmp_assign_source_2;
        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            CHECK_OBJECT( var_unquote );
            tmp_called_name_2 = var_unquote;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_basestring );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_to_basestring );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "to_basestring" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 144;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_3 = tmp_mvar_value_5;
            CHECK_OBJECT( par_value );
            tmp_args_element_name_3 = par_value;
            frame_e80c64282c52818be9dfd1256a205f0f->m_frame.f_lineno = 144;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_args_name_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_dict_key_1 = const_str_plain_encoding;
            CHECK_OBJECT( par_encoding );
            tmp_dict_value_1 = par_encoding;
            tmp_kw_name_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_e80c64282c52818be9dfd1256a205f0f->m_frame.f_lineno = 144;
            tmp_return_value = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e80c64282c52818be9dfd1256a205f0f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e80c64282c52818be9dfd1256a205f0f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e80c64282c52818be9dfd1256a205f0f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e80c64282c52818be9dfd1256a205f0f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e80c64282c52818be9dfd1256a205f0f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e80c64282c52818be9dfd1256a205f0f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e80c64282c52818be9dfd1256a205f0f,
        type_description_1,
        par_value,
        par_encoding,
        par_plus,
        var_unquote
    );


    // Release cached frame.
    if ( frame_e80c64282c52818be9dfd1256a205f0f == cache_frame_e80c64282c52818be9dfd1256a205f0f )
    {
        Py_DECREF( frame_e80c64282c52818be9dfd1256a205f0f );
    }
    cache_frame_e80c64282c52818be9dfd1256a205f0f = NULL;

    assertFrameObject( frame_e80c64282c52818be9dfd1256a205f0f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_9_url_unescape );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_plus );
    Py_DECREF( par_plus );
    par_plus = NULL;

    Py_XDECREF( var_unquote );
    var_unquote = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_plus );
    Py_DECREF( par_plus );
    par_plus = NULL;

    Py_XDECREF( var_unquote );
    var_unquote = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_9_url_unescape );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_10_parse_qs_bytes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_qs = python_pars[ 0 ];
    PyObject *par_keep_blank_values = python_pars[ 1 ];
    PyObject *par_strict_parsing = python_pars[ 2 ];
    PyObject *var_result = NULL;
    PyObject *var_encoded = NULL;
    PyObject *var_k = NULL;
    PyObject *var_v = NULL;
    PyObject *outline_0_var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_c9bbec91076591a2b9c659a133d35e3a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_9178d1450e157b672bf8147b6b4641b5_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_9178d1450e157b672bf8147b6b4641b5_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_c9bbec91076591a2b9c659a133d35e3a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c9bbec91076591a2b9c659a133d35e3a, codeobj_c9bbec91076591a2b9c659a133d35e3a, module_tornado$escape, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c9bbec91076591a2b9c659a133d35e3a = cache_frame_c9bbec91076591a2b9c659a133d35e3a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c9bbec91076591a2b9c659a133d35e3a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c9bbec91076591a2b9c659a133d35e3a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_urllib );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_urllib );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "urllib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_parse );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_parse_qs );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_qs );
        tmp_tuple_element_1 = par_qs;
        tmp_args_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_keep_blank_values );
        tmp_tuple_element_1 = par_keep_blank_values;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_strict_parsing );
        tmp_tuple_element_1 = par_strict_parsing;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_7b0c1fe6e32c976caf2154377bedae8e );
        frame_c9bbec91076591a2b9c659a133d35e3a->m_frame.f_lineno = 159;
        tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_result == NULL );
        var_result = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyDict_New();
        assert( var_encoded == NULL );
        var_encoded = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_result );
        tmp_called_instance_1 = var_result;
        frame_c9bbec91076591a2b9c659a133d35e3a->m_frame.f_lineno = 163;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooo";
                exception_lineno = 163;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_5 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "ooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 163;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 163;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooo";
                    exception_lineno = 163;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooo";
            exception_lineno = 163;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_k;
            var_k = tmp_assign_source_8;
            Py_INCREF( var_k );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_v;
            var_v = tmp_assign_source_9;
            Py_INCREF( var_v );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_iter_arg_3;
        CHECK_OBJECT( var_v );
        tmp_iter_arg_3 = var_v;
        tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_3 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_listcomp_1__$0;
            tmp_listcomp_1__$0 = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = PyList_New( 0 );
        {
            PyObject *old = tmp_listcomp_1__contraction;
            tmp_listcomp_1__contraction = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    MAKE_OR_REUSE_FRAME( cache_frame_9178d1450e157b672bf8147b6b4641b5_2, codeobj_9178d1450e157b672bf8147b6b4641b5, module_tornado$escape, sizeof(void *) );
    frame_9178d1450e157b672bf8147b6b4641b5_2 = cache_frame_9178d1450e157b672bf8147b6b4641b5_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9178d1450e157b672bf8147b6b4641b5_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9178d1450e157b672bf8147b6b4641b5_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_12;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_2 = tmp_listcomp_1__$0;
        tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_12 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "o";
                exception_lineno = 164;
                goto try_except_handler_6;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_12;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_13 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var_i;
            outline_0_var_i = tmp_assign_source_13;
            Py_INCREF( outline_0_var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_append_list_1;
        PyObject *tmp_append_value_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_append_list_1 = tmp_listcomp_1__contraction;
        CHECK_OBJECT( outline_0_var_i );
        tmp_called_instance_2 = outline_0_var_i;
        frame_9178d1450e157b672bf8147b6b4641b5_2->m_frame.f_lineno = 164;
        tmp_append_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_plain_latin1_tuple, 0 ) );

        if ( tmp_append_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_2 = "o";
            goto try_except_handler_6;
        }
        assert( PyList_Check( tmp_append_list_1 ) );
        tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
        Py_DECREF( tmp_append_value_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_2 = "o";
            goto try_except_handler_6;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 164;
        type_description_2 = "o";
        goto try_except_handler_6;
    }
    goto loop_start_2;
    loop_end_2:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_dictset_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_dictset_value );
    goto try_return_handler_6;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_10_parse_qs_bytes );
    return NULL;
    // Return handler code:
    try_return_handler_6:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9178d1450e157b672bf8147b6b4641b5_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9178d1450e157b672bf8147b6b4641b5_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_5;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9178d1450e157b672bf8147b6b4641b5_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9178d1450e157b672bf8147b6b4641b5_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9178d1450e157b672bf8147b6b4641b5_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9178d1450e157b672bf8147b6b4641b5_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9178d1450e157b672bf8147b6b4641b5_2,
        type_description_2,
        outline_0_var_i
    );


    // Release cached frame.
    if ( frame_9178d1450e157b672bf8147b6b4641b5_2 == cache_frame_9178d1450e157b672bf8147b6b4641b5_2 )
    {
        Py_DECREF( frame_9178d1450e157b672bf8147b6b4641b5_2 );
    }
    cache_frame_9178d1450e157b672bf8147b6b4641b5_2 = NULL;

    assertFrameObject( frame_9178d1450e157b672bf8147b6b4641b5_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "ooooooo";
    goto try_except_handler_5;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_10_parse_qs_bytes );
    return NULL;
    // Return handler code:
    try_return_handler_5:;
    Py_XDECREF( outline_0_var_i );
    outline_0_var_i = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var_i );
    outline_0_var_i = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_10_parse_qs_bytes );
    return NULL;
    outline_exception_1:;
    exception_lineno = 164;
    goto try_except_handler_2;
    outline_result_1:;
    CHECK_OBJECT( var_encoded );
    tmp_dictset_dict = var_encoded;
    CHECK_OBJECT( var_k );
    tmp_dictset_key = var_k;
    tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
    Py_DECREF( tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 164;
        type_description_1 = "ooooooo";
        goto try_except_handler_2;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 163;
        type_description_1 = "ooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c9bbec91076591a2b9c659a133d35e3a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c9bbec91076591a2b9c659a133d35e3a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c9bbec91076591a2b9c659a133d35e3a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c9bbec91076591a2b9c659a133d35e3a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c9bbec91076591a2b9c659a133d35e3a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c9bbec91076591a2b9c659a133d35e3a,
        type_description_1,
        par_qs,
        par_keep_blank_values,
        par_strict_parsing,
        var_result,
        var_encoded,
        var_k,
        var_v
    );


    // Release cached frame.
    if ( frame_c9bbec91076591a2b9c659a133d35e3a == cache_frame_c9bbec91076591a2b9c659a133d35e3a )
    {
        Py_DECREF( frame_c9bbec91076591a2b9c659a133d35e3a );
    }
    cache_frame_c9bbec91076591a2b9c659a133d35e3a = NULL;

    assertFrameObject( frame_c9bbec91076591a2b9c659a133d35e3a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_encoded );
    tmp_return_value = var_encoded;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_10_parse_qs_bytes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_qs );
    Py_DECREF( par_qs );
    par_qs = NULL;

    CHECK_OBJECT( (PyObject *)par_keep_blank_values );
    Py_DECREF( par_keep_blank_values );
    par_keep_blank_values = NULL;

    CHECK_OBJECT( (PyObject *)par_strict_parsing );
    Py_DECREF( par_strict_parsing );
    par_strict_parsing = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    CHECK_OBJECT( (PyObject *)var_encoded );
    Py_DECREF( var_encoded );
    var_encoded = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_qs );
    Py_DECREF( par_qs );
    par_qs = NULL;

    CHECK_OBJECT( (PyObject *)par_keep_blank_values );
    Py_DECREF( par_keep_blank_values );
    par_keep_blank_values = NULL;

    CHECK_OBJECT( (PyObject *)par_strict_parsing );
    Py_DECREF( par_strict_parsing );
    par_strict_parsing = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_encoded );
    var_encoded = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_10_parse_qs_bytes );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_11_utf8( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_11_utf8 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_11_utf8 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_12_utf8( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_12_utf8 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_12_utf8 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_13_utf8( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_13_utf8 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_13_utf8 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_14_utf8( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d58793142ff8d50205aeba9863150c26;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_d58793142ff8d50205aeba9863150c26 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d58793142ff8d50205aeba9863150c26, codeobj_d58793142ff8d50205aeba9863150c26, module_tornado$escape, sizeof(void *) );
    frame_d58793142ff8d50205aeba9863150c26 = cache_frame_d58793142ff8d50205aeba9863150c26;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d58793142ff8d50205aeba9863150c26 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d58793142ff8d50205aeba9863150c26 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_isinstance_inst_1 = par_value;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__UTF8_TYPES );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__UTF8_TYPES );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_UTF8_TYPES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 192;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_value );
        tmp_return_value = par_value;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_2;
        PyObject *tmp_isinstance_cls_2;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_value );
        tmp_isinstance_inst_2 = par_value;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_unicode_type );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode_type );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode_type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 194;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_2 = tmp_mvar_value_2;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_type_arg_1;
            tmp_left_name_1 = const_str_digest_5f510ad45c7784edef65d44eb17e31f8;
            CHECK_OBJECT( par_value );
            tmp_type_arg_1 = par_value;
            tmp_right_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
            assert( !(tmp_right_name_1 == NULL) );
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 195;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_d58793142ff8d50205aeba9863150c26->m_frame.f_lineno = 195;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 195;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_value );
        tmp_called_instance_1 = par_value;
        frame_d58793142ff8d50205aeba9863150c26->m_frame.f_lineno = 196;
        tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d58793142ff8d50205aeba9863150c26 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d58793142ff8d50205aeba9863150c26 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d58793142ff8d50205aeba9863150c26 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d58793142ff8d50205aeba9863150c26, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d58793142ff8d50205aeba9863150c26->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d58793142ff8d50205aeba9863150c26, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d58793142ff8d50205aeba9863150c26,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_d58793142ff8d50205aeba9863150c26 == cache_frame_d58793142ff8d50205aeba9863150c26 )
    {
        Py_DECREF( frame_d58793142ff8d50205aeba9863150c26 );
    }
    cache_frame_d58793142ff8d50205aeba9863150c26 = NULL;

    assertFrameObject( frame_d58793142ff8d50205aeba9863150c26 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_14_utf8 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_14_utf8 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_15_to_unicode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_15_to_unicode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_15_to_unicode );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_16_to_unicode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_16_to_unicode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_16_to_unicode );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_17_to_unicode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_17_to_unicode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_17_to_unicode );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_18_to_unicode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_be813ac06ef379d66594c8be487f9abc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_be813ac06ef379d66594c8be487f9abc = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_be813ac06ef379d66594c8be487f9abc, codeobj_be813ac06ef379d66594c8be487f9abc, module_tornado$escape, sizeof(void *) );
    frame_be813ac06ef379d66594c8be487f9abc = cache_frame_be813ac06ef379d66594c8be487f9abc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_be813ac06ef379d66594c8be487f9abc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_be813ac06ef379d66594c8be487f9abc ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_isinstance_inst_1 = par_value;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__TO_UNICODE_TYPES );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__TO_UNICODE_TYPES );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_TO_UNICODE_TYPES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 223;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_value );
        tmp_return_value = par_value;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_2;
        PyObject *tmp_isinstance_cls_2;
        CHECK_OBJECT( par_value );
        tmp_isinstance_inst_2 = par_value;
        tmp_isinstance_cls_2 = (PyObject *)&PyBytes_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_type_arg_1;
            tmp_left_name_1 = const_str_digest_5f510ad45c7784edef65d44eb17e31f8;
            CHECK_OBJECT( par_value );
            tmp_type_arg_1 = par_value;
            tmp_right_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
            assert( !(tmp_right_name_1 == NULL) );
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 226;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_be813ac06ef379d66594c8be487f9abc->m_frame.f_lineno = 226;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 226;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_value );
        tmp_called_instance_1 = par_value;
        frame_be813ac06ef379d66594c8be487f9abc->m_frame.f_lineno = 227;
        tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_decode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be813ac06ef379d66594c8be487f9abc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_be813ac06ef379d66594c8be487f9abc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be813ac06ef379d66594c8be487f9abc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_be813ac06ef379d66594c8be487f9abc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_be813ac06ef379d66594c8be487f9abc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_be813ac06ef379d66594c8be487f9abc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_be813ac06ef379d66594c8be487f9abc,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_be813ac06ef379d66594c8be487f9abc == cache_frame_be813ac06ef379d66594c8be487f9abc )
    {
        Py_DECREF( frame_be813ac06ef379d66594c8be487f9abc );
    }
    cache_frame_be813ac06ef379d66594c8be487f9abc = NULL;

    assertFrameObject( frame_be813ac06ef379d66594c8be487f9abc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_18_to_unicode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_18_to_unicode );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_19_recursive_unicode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_obj = python_pars[ 0 ];
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_genexpr_2__$0 = NULL;
    PyObject *tmp_genexpr_3__$0 = NULL;
    struct Nuitka_FrameObject *frame_7383220ca4a74759df7b67f7b0e48cb7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_7383220ca4a74759df7b67f7b0e48cb7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7383220ca4a74759df7b67f7b0e48cb7, codeobj_7383220ca4a74759df7b67f7b0e48cb7, module_tornado$escape, sizeof(void *) );
    frame_7383220ca4a74759df7b67f7b0e48cb7 = cache_frame_7383220ca4a74759df7b67f7b0e48cb7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7383220ca4a74759df7b67f7b0e48cb7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7383220ca4a74759df7b67f7b0e48cb7 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_obj );
        tmp_isinstance_inst_1 = par_obj;
        tmp_isinstance_cls_1 = (PyObject *)&PyDict_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 245;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_dict_seq_1;
            {
                PyObject *tmp_assign_source_1;
                PyObject *tmp_iter_arg_1;
                PyObject *tmp_called_instance_1;
                CHECK_OBJECT( par_obj );
                tmp_called_instance_1 = par_obj;
                frame_7383220ca4a74759df7b67f7b0e48cb7->m_frame.f_lineno = 247;
                tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
                if ( tmp_iter_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 247;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
                Py_DECREF( tmp_iter_arg_1 );
                if ( tmp_assign_source_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 247;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                assert( tmp_genexpr_1__$0 == NULL );
                tmp_genexpr_1__$0 = tmp_assign_source_1;
            }
            // Tried code:
            tmp_dict_seq_1 = tornado$escape$$$function_19_recursive_unicode$$$genexpr_1_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_dict_seq_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


            goto try_return_handler_2;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_19_recursive_unicode );
            return NULL;
            // Return handler code:
            try_return_handler_2:;
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            goto outline_result_1;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_19_recursive_unicode );
            return NULL;
            outline_result_1:;
            tmp_return_value = TO_DICT( tmp_dict_seq_1, NULL );
            Py_DECREF( tmp_dict_seq_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 246;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            CHECK_OBJECT( par_obj );
            tmp_isinstance_inst_2 = par_obj;
            tmp_isinstance_cls_2 = (PyObject *)&PyList_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 249;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_list_arg_1;
                {
                    PyObject *tmp_assign_source_2;
                    PyObject *tmp_iter_arg_2;
                    CHECK_OBJECT( par_obj );
                    tmp_iter_arg_2 = par_obj;
                    tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_2 );
                    if ( tmp_assign_source_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 250;
                        type_description_1 = "o";
                        goto frame_exception_exit_1;
                    }
                    assert( tmp_genexpr_2__$0 == NULL );
                    tmp_genexpr_2__$0 = tmp_assign_source_2;
                }
                // Tried code:
                tmp_list_arg_1 = tornado$escape$$$function_19_recursive_unicode$$$genexpr_2_genexpr_maker();

                ((struct Nuitka_GeneratorObject *)tmp_list_arg_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_2__$0 );


                goto try_return_handler_3;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_19_recursive_unicode );
                return NULL;
                // Return handler code:
                try_return_handler_3:;
                CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
                Py_DECREF( tmp_genexpr_2__$0 );
                tmp_genexpr_2__$0 = NULL;

                goto outline_result_2;
                // End of try:
                CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
                Py_DECREF( tmp_genexpr_2__$0 );
                tmp_genexpr_2__$0 = NULL;

                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_19_recursive_unicode );
                return NULL;
                outline_result_2:;
                tmp_return_value = PySequence_List( tmp_list_arg_1 );
                Py_DECREF( tmp_list_arg_1 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 250;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_isinstance_inst_3;
                PyObject *tmp_isinstance_cls_3;
                CHECK_OBJECT( par_obj );
                tmp_isinstance_inst_3 = par_obj;
                tmp_isinstance_cls_3 = (PyObject *)&PyTuple_Type;
                tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_3, tmp_isinstance_cls_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 251;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_tuple_arg_1;
                    {
                        PyObject *tmp_assign_source_3;
                        PyObject *tmp_iter_arg_3;
                        CHECK_OBJECT( par_obj );
                        tmp_iter_arg_3 = par_obj;
                        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_3 );
                        if ( tmp_assign_source_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 252;
                            type_description_1 = "o";
                            goto frame_exception_exit_1;
                        }
                        assert( tmp_genexpr_3__$0 == NULL );
                        tmp_genexpr_3__$0 = tmp_assign_source_3;
                    }
                    // Tried code:
                    tmp_tuple_arg_1 = tornado$escape$$$function_19_recursive_unicode$$$genexpr_3_genexpr_maker();

                    ((struct Nuitka_GeneratorObject *)tmp_tuple_arg_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_3__$0 );


                    goto try_return_handler_4;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_19_recursive_unicode );
                    return NULL;
                    // Return handler code:
                    try_return_handler_4:;
                    CHECK_OBJECT( (PyObject *)tmp_genexpr_3__$0 );
                    Py_DECREF( tmp_genexpr_3__$0 );
                    tmp_genexpr_3__$0 = NULL;

                    goto outline_result_3;
                    // End of try:
                    CHECK_OBJECT( (PyObject *)tmp_genexpr_3__$0 );
                    Py_DECREF( tmp_genexpr_3__$0 );
                    tmp_genexpr_3__$0 = NULL;

                    // Return statement must have exited already.
                    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_19_recursive_unicode );
                    return NULL;
                    outline_result_3:;
                    tmp_return_value = PySequence_Tuple( tmp_tuple_arg_1 );
                    Py_DECREF( tmp_tuple_arg_1 );
                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 252;
                        type_description_1 = "o";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                goto branch_end_3;
                branch_no_3:;
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_isinstance_inst_4;
                    PyObject *tmp_isinstance_cls_4;
                    CHECK_OBJECT( par_obj );
                    tmp_isinstance_inst_4 = par_obj;
                    tmp_isinstance_cls_4 = (PyObject *)&PyBytes_Type;
                    tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_4, tmp_isinstance_cls_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 253;
                        type_description_1 = "o";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    {
                        PyObject *tmp_called_name_1;
                        PyObject *tmp_mvar_value_1;
                        PyObject *tmp_args_element_name_1;
                        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_unicode );

                        if (unlikely( tmp_mvar_value_1 == NULL ))
                        {
                            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_to_unicode );
                        }

                        if ( tmp_mvar_value_1 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "to_unicode" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 254;
                            type_description_1 = "o";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_name_1 = tmp_mvar_value_1;
                        CHECK_OBJECT( par_obj );
                        tmp_args_element_name_1 = par_obj;
                        frame_7383220ca4a74759df7b67f7b0e48cb7->m_frame.f_lineno = 254;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_1 };
                            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                        }

                        if ( tmp_return_value == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 254;
                            type_description_1 = "o";
                            goto frame_exception_exit_1;
                        }
                        goto frame_return_exit_1;
                    }
                    goto branch_end_4;
                    branch_no_4:;
                    CHECK_OBJECT( par_obj );
                    tmp_return_value = par_obj;
                    Py_INCREF( tmp_return_value );
                    goto frame_return_exit_1;
                    branch_end_4:;
                }
                branch_end_3:;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7383220ca4a74759df7b67f7b0e48cb7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7383220ca4a74759df7b67f7b0e48cb7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7383220ca4a74759df7b67f7b0e48cb7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7383220ca4a74759df7b67f7b0e48cb7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7383220ca4a74759df7b67f7b0e48cb7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7383220ca4a74759df7b67f7b0e48cb7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7383220ca4a74759df7b67f7b0e48cb7,
        type_description_1,
        par_obj
    );


    // Release cached frame.
    if ( frame_7383220ca4a74759df7b67f7b0e48cb7 == cache_frame_7383220ca4a74759df7b67f7b0e48cb7 )
    {
        Py_DECREF( frame_7383220ca4a74759df7b67f7b0e48cb7 );
    }
    cache_frame_7383220ca4a74759df7b67f7b0e48cb7 = NULL;

    assertFrameObject( frame_7383220ca4a74759df7b67f7b0e48cb7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_19_recursive_unicode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_19_recursive_unicode );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_1_genexpr_locals {
    PyObject *var_k;
    PyObject *var_v;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *tornado$escape$$$function_19_recursive_unicode$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_1_genexpr_locals *generator_heap = (struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_k = NULL;
    generator_heap->var_v = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_8c45b806923f1e580927d3a29be76c7f, module_tornado$escape, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noo";
                generator_heap->exception_lineno = 247;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_iter_arg_1 = generator_heap->tmp_iter_value_0;
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 247;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 247;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 247;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "Noo";
                    generator_heap->exception_lineno = 247;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 247;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_k;
            generator_heap->var_k = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_k );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_v;
            generator_heap->var_v = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_v );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_recursive_unicode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_recursive_unicode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "recursive_unicode" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 247;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_k );
        tmp_args_element_name_1 = generator_heap->var_k;
        generator->m_frame->m_frame.f_lineno = 247;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 247;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_recursive_unicode );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_recursive_unicode );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "recursive_unicode" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 247;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( generator_heap->var_v );
        tmp_args_element_name_2 = generator_heap->var_v;
        generator->m_frame->m_frame.f_lineno = 247;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );

            generator_heap->exception_lineno = 247;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 247;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 247;
        generator_heap->type_description_1 = "Noo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_k,
            generator_heap->var_v
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *tornado$escape$$$function_19_recursive_unicode$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        tornado$escape$$$function_19_recursive_unicode$$$genexpr_1_genexpr_context,
        module_tornado$escape,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_7e12d8e7d8726364c4d8827b8ca157eb,
#endif
        codeobj_8c45b806923f1e580927d3a29be76c7f,
        1,
        sizeof(struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_1_genexpr_locals)
    );
}



struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_2_genexpr_locals {
    PyObject *var_i;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *tornado$escape$$$function_19_recursive_unicode$$$genexpr_2_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_2_genexpr_locals *generator_heap = (struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_2_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_i = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_6e047f9504cc26b3d1d05a1c4c8e67ad, module_tornado$escape, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 250;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_recursive_unicode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_recursive_unicode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "recursive_unicode" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 250;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_i );
        tmp_args_element_name_1 = generator_heap->var_i;
        generator->m_frame->m_frame.f_lineno = 250;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_expression_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 250;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 250;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 250;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_i
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *tornado$escape$$$function_19_recursive_unicode$$$genexpr_2_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        tornado$escape$$$function_19_recursive_unicode$$$genexpr_2_genexpr_context,
        module_tornado$escape,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_7e12d8e7d8726364c4d8827b8ca157eb,
#endif
        codeobj_6e047f9504cc26b3d1d05a1c4c8e67ad,
        1,
        sizeof(struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_2_genexpr_locals)
    );
}



struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_3_genexpr_locals {
    PyObject *var_i;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *tornado$escape$$$function_19_recursive_unicode$$$genexpr_3_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_3_genexpr_locals *generator_heap = (struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_3_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_i = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_98627c45100bf0003c00ce067707cdf5, module_tornado$escape, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 252;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_recursive_unicode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_recursive_unicode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "recursive_unicode" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 252;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_i );
        tmp_args_element_name_1 = generator_heap->var_i;
        generator->m_frame->m_frame.f_lineno = 252;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_expression_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 252;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 252;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 252;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_i
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *tornado$escape$$$function_19_recursive_unicode$$$genexpr_3_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        tornado$escape$$$function_19_recursive_unicode$$$genexpr_3_genexpr_context,
        module_tornado$escape,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_7e12d8e7d8726364c4d8827b8ca157eb,
#endif
        codeobj_98627c45100bf0003c00ce067707cdf5,
        1,
        sizeof(struct tornado$escape$$$function_19_recursive_unicode$$$genexpr_3_genexpr_locals)
    );
}


static PyObject *impl_tornado$escape$$$function_20_linkify( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    struct Nuitka_CellObject *par_shorten = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_extra_params = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_require_protocol = PyCell_NEW1( python_pars[ 3 ] );
    struct Nuitka_CellObject *par_permitted_protocols = PyCell_NEW1( python_pars[ 4 ] );
    PyObject *var_make_link = NULL;
    struct Nuitka_FrameObject *frame_53cf9b7ffbe8ea9a7750e9bad0c565d9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_53cf9b7ffbe8ea9a7750e9bad0c565d9, codeobj_53cf9b7ffbe8ea9a7750e9bad0c565d9, module_tornado$escape, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 = cache_frame_53cf9b7ffbe8ea9a7750e9bad0c565d9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( PyCell_GET( par_extra_params ) );
        tmp_truth_name_1 = CHECK_IF_TRUE( PyCell_GET( par_extra_params ) );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 309;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_callable );
        assert( tmp_called_name_1 != NULL );
        CHECK_OBJECT( PyCell_GET( par_extra_params ) );
        tmp_args_element_name_1 = PyCell_GET( par_extra_params );
        frame_53cf9b7ffbe8ea9a7750e9bad0c565d9->m_frame.f_lineno = 309;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 309;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 309;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_called_instance_1;
            tmp_left_name_1 = const_str_space;
            CHECK_OBJECT( PyCell_GET( par_extra_params ) );
            tmp_called_instance_1 = PyCell_GET( par_extra_params );
            frame_53cf9b7ffbe8ea9a7750e9bad0c565d9->m_frame.f_lineno = 310;
            tmp_right_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_strip );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 310;
                type_description_1 = "occcco";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 310;
                type_description_1 = "occcco";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = PyCell_GET( par_extra_params );
                PyCell_SET( par_extra_params, tmp_assign_source_1 );
                Py_XDECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_annotations_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_dict_key_1 = const_str_plain_m;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 312;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Match );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }
        tmp_annotations_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_return;
        tmp_dict_value_2 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_2 = MAKE_FUNCTION_tornado$escape$$$function_20_linkify$$$function_1_make_link( tmp_annotations_1 );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = par_extra_params;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] = par_permitted_protocols;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[2] = par_require_protocol;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[3] = par_shorten;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[3] );


        assert( var_make_link == NULL );
        var_make_link = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__unicode );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__unicode );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_unicode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 374;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_xhtml_escape );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_xhtml_escape );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "xhtml_escape" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 374;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_3 = par_text;
        frame_53cf9b7ffbe8ea9a7750e9bad0c565d9->m_frame.f_lineno = 374;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 374;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }
        frame_53cf9b7ffbe8ea9a7750e9bad0c565d9->m_frame.f_lineno = 374;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 374;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_text;
            assert( old != NULL );
            par_text = tmp_assign_source_3;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__URL_RE );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__URL_RE );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_URL_RE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 375;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_4;
        CHECK_OBJECT( var_make_link );
        tmp_args_element_name_4 = var_make_link;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_5 = par_text;
        frame_53cf9b7ffbe8ea9a7750e9bad0c565d9->m_frame.f_lineno = 375;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_sub, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 375;
            type_description_1 = "occcco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_53cf9b7ffbe8ea9a7750e9bad0c565d9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_53cf9b7ffbe8ea9a7750e9bad0c565d9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_53cf9b7ffbe8ea9a7750e9bad0c565d9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_53cf9b7ffbe8ea9a7750e9bad0c565d9,
        type_description_1,
        par_text,
        par_shorten,
        par_extra_params,
        par_require_protocol,
        par_permitted_protocols,
        var_make_link
    );


    // Release cached frame.
    if ( frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 == cache_frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 )
    {
        Py_DECREF( frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 );
    }
    cache_frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 = NULL;

    assertFrameObject( frame_53cf9b7ffbe8ea9a7750e9bad0c565d9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_20_linkify );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_shorten );
    Py_DECREF( par_shorten );
    par_shorten = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_params );
    Py_DECREF( par_extra_params );
    par_extra_params = NULL;

    CHECK_OBJECT( (PyObject *)par_require_protocol );
    Py_DECREF( par_require_protocol );
    par_require_protocol = NULL;

    CHECK_OBJECT( (PyObject *)par_permitted_protocols );
    Py_DECREF( par_permitted_protocols );
    par_permitted_protocols = NULL;

    CHECK_OBJECT( (PyObject *)var_make_link );
    Py_DECREF( var_make_link );
    var_make_link = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_shorten );
    Py_DECREF( par_shorten );
    par_shorten = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_params );
    Py_DECREF( par_extra_params );
    par_extra_params = NULL;

    CHECK_OBJECT( (PyObject *)par_require_protocol );
    Py_DECREF( par_require_protocol );
    par_require_protocol = NULL;

    CHECK_OBJECT( (PyObject *)par_permitted_protocols );
    Py_DECREF( par_permitted_protocols );
    par_permitted_protocols = NULL;

    Py_XDECREF( var_make_link );
    var_make_link = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_20_linkify );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_20_linkify$$$function_1_make_link( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_m = python_pars[ 0 ];
    PyObject *var_url = NULL;
    PyObject *var_proto = NULL;
    PyObject *var_href = NULL;
    PyObject *var_params = NULL;
    PyObject *var_before_clip = NULL;
    PyObject *var_proto_len = NULL;
    PyObject *var_parts = NULL;
    PyObject *var_amp = NULL;
    struct Nuitka_FrameObject *frame_bccf81b17a0b884150a8eafb0fd57813;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_bccf81b17a0b884150a8eafb0fd57813 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bccf81b17a0b884150a8eafb0fd57813, codeobj_bccf81b17a0b884150a8eafb0fd57813, module_tornado$escape, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bccf81b17a0b884150a8eafb0fd57813 = cache_frame_bccf81b17a0b884150a8eafb0fd57813;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bccf81b17a0b884150a8eafb0fd57813 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bccf81b17a0b884150a8eafb0fd57813 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_m );
        tmp_called_instance_1 = par_m;
        frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 313;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        assert( var_url == NULL );
        var_url = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_m );
        tmp_called_instance_2 = par_m;
        frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 314;
        tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        assert( var_proto == NULL );
        var_proto = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_operand_name_1;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "require_protocol" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 315;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( PyCell_GET( self->m_closure[2] ) );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 315;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_proto );
        tmp_operand_name_1 = var_proto;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 315;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( var_url );
        tmp_return_value = var_url;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_2;
        nuitka_bool tmp_and_left_value_2;
        nuitka_bool tmp_and_right_value_2;
        int tmp_truth_name_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_proto );
        tmp_truth_name_2 = CHECK_IF_TRUE( var_proto );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 318;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        CHECK_OBJECT( var_proto );
        tmp_compexpr_left_1 = var_proto;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "permitted_protocols" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 318;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = PyCell_GET( self->m_closure[1] );
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 318;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_2 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_condition_result_2 = tmp_and_left_value_2;
        and_end_2:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( var_url );
        tmp_return_value = var_url;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( par_m );
        tmp_called_instance_3 = par_m;
        frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 321;
        tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 321;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        assert( var_href == NULL );
        var_href = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_operand_name_2;
        CHECK_OBJECT( var_proto );
        tmp_operand_name_2 = var_proto;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 322;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_left_name_1 = const_str_digest_7c06a402579f6f9d9db7f3e04da983fc;
            CHECK_OBJECT( var_href );
            tmp_right_name_1 = var_href;
            tmp_assign_source_4 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 323;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_href;
                assert( old != NULL );
                var_href = tmp_assign_source_4;
                Py_DECREF( old );
            }

        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        int tmp_truth_name_3;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_callable );
        assert( tmp_called_name_1 != NULL );
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "extra_params" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 325;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[0] );
        frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 325;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 325;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 325;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_2;
            tmp_left_name_2 = const_str_space;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "extra_params" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 326;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = PyCell_GET( self->m_closure[0] );
            CHECK_OBJECT( var_href );
            tmp_args_element_name_2 = var_href;
            frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 326;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_called_instance_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_called_instance_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 326;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 326;
            tmp_right_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_strip );
            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 326;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_5 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 326;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            assert( var_params == NULL );
            var_params = tmp_assign_source_5;
        }
        goto branch_end_4;
        branch_no_4:;
        {
            PyObject *tmp_assign_source_6;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "extra_params" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 328;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }

            tmp_assign_source_6 = PyCell_GET( self->m_closure[0] );
            assert( var_params == NULL );
            Py_INCREF( tmp_assign_source_6 );
            var_params = tmp_assign_source_6;
        }
        branch_end_4:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        int tmp_and_left_truth_3;
        nuitka_bool tmp_and_left_value_3;
        nuitka_bool tmp_and_right_value_3;
        int tmp_truth_name_4;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        if ( PyCell_GET( self->m_closure[3] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "shorten" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 332;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_4 = CHECK_IF_TRUE( PyCell_GET( self->m_closure[3] ) );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 332;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_3 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_3 = tmp_and_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_3 == 1 )
        {
            goto and_right_3;
        }
        else
        {
            goto and_left_3;
        }
        and_right_3:;
        CHECK_OBJECT( var_url );
        tmp_len_arg_1 = var_url;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 332;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_pos_30;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_and_right_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_5 = tmp_and_right_value_3;
        goto and_end_3;
        and_left_3:;
        tmp_condition_result_5 = tmp_and_left_value_3;
        and_end_3:;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( var_url );
            tmp_assign_source_7 = var_url;
            assert( var_before_clip == NULL );
            Py_INCREF( tmp_assign_source_7 );
            var_before_clip = tmp_assign_source_7;
        }
        {
            nuitka_bool tmp_condition_result_6;
            int tmp_truth_name_5;
            CHECK_OBJECT( var_proto );
            tmp_truth_name_5 = CHECK_IF_TRUE( var_proto );
            if ( tmp_truth_name_5 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 334;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_6 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_assign_source_8;
                PyObject *tmp_left_name_3;
                PyObject *tmp_left_name_4;
                PyObject *tmp_len_arg_2;
                PyObject *tmp_right_name_3;
                PyObject *tmp_right_name_4;
                PyObject *tmp_len_arg_3;
                int tmp_or_left_truth_1;
                PyObject *tmp_or_left_value_1;
                PyObject *tmp_or_right_value_1;
                PyObject *tmp_called_instance_5;
                CHECK_OBJECT( var_proto );
                tmp_len_arg_2 = var_proto;
                tmp_left_name_4 = BUILTIN_LEN( tmp_len_arg_2 );
                if ( tmp_left_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 335;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_3 = const_int_pos_1;
                tmp_left_name_3 = BINARY_OPERATION_ADD_LONG_LONG( tmp_left_name_4, tmp_right_name_3 );
                Py_DECREF( tmp_left_name_4 );
                assert( !(tmp_left_name_3 == NULL) );
                CHECK_OBJECT( par_m );
                tmp_called_instance_5 = par_m;
                frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 335;
                tmp_or_left_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_3_tuple, 0 ) );

                if ( tmp_or_left_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_3 );

                    exception_lineno = 335;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
                if ( tmp_or_left_truth_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_3 );
                    Py_DECREF( tmp_or_left_value_1 );

                    exception_lineno = 335;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                if ( tmp_or_left_truth_1 == 1 )
                {
                    goto or_left_1;
                }
                else
                {
                    goto or_right_1;
                }
                or_right_1:;
                Py_DECREF( tmp_or_left_value_1 );
                tmp_or_right_value_1 = const_str_empty;
                Py_INCREF( tmp_or_right_value_1 );
                tmp_len_arg_3 = tmp_or_right_value_1;
                goto or_end_1;
                or_left_1:;
                tmp_len_arg_3 = tmp_or_left_value_1;
                or_end_1:;
                tmp_right_name_4 = BUILTIN_LEN( tmp_len_arg_3 );
                Py_DECREF( tmp_len_arg_3 );
                if ( tmp_right_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_3 );

                    exception_lineno = 335;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_8 = BINARY_OPERATION_ADD_LONG_LONG( tmp_left_name_3, tmp_right_name_4 );
                Py_DECREF( tmp_left_name_3 );
                Py_DECREF( tmp_right_name_4 );
                assert( !(tmp_assign_source_8 == NULL) );
                assert( var_proto_len == NULL );
                var_proto_len = tmp_assign_source_8;
            }
            goto branch_end_6;
            branch_no_6:;
            {
                PyObject *tmp_assign_source_9;
                tmp_assign_source_9 = const_int_0;
                assert( var_proto_len == NULL );
                Py_INCREF( tmp_assign_source_9 );
                var_proto_len = tmp_assign_source_9;
            }
            branch_end_6:;
        }
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_called_instance_6;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_start_name_1;
            PyObject *tmp_stop_name_1;
            PyObject *tmp_step_name_1;
            CHECK_OBJECT( var_url );
            tmp_subscribed_name_1 = var_url;
            CHECK_OBJECT( var_proto_len );
            tmp_start_name_1 = var_proto_len;
            tmp_stop_name_1 = Py_None;
            tmp_step_name_1 = Py_None;
            tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
            assert( !(tmp_subscript_name_1 == NULL) );
            tmp_called_instance_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscript_name_1 );
            if ( tmp_called_instance_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 339;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 339;
            tmp_assign_source_10 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_47_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_6 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 339;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            assert( var_parts == NULL );
            var_parts = tmp_assign_source_10;
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_len_arg_4;
            CHECK_OBJECT( var_parts );
            tmp_len_arg_4 = var_parts;
            tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_4 );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 340;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_3 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_assign_source_11;
                PyObject *tmp_left_name_5;
                PyObject *tmp_left_name_6;
                PyObject *tmp_left_name_7;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                PyObject *tmp_start_name_2;
                PyObject *tmp_stop_name_2;
                PyObject *tmp_step_name_2;
                PyObject *tmp_right_name_5;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_subscript_name_3;
                PyObject *tmp_right_name_6;
                PyObject *tmp_right_name_7;
                PyObject *tmp_subscribed_name_4;
                PyObject *tmp_called_instance_7;
                PyObject *tmp_subscribed_name_5;
                PyObject *tmp_called_instance_8;
                PyObject *tmp_subscribed_name_6;
                PyObject *tmp_subscribed_name_7;
                PyObject *tmp_subscript_name_4;
                PyObject *tmp_subscript_name_5;
                PyObject *tmp_subscript_name_6;
                PyObject *tmp_subscript_name_7;
                CHECK_OBJECT( var_url );
                tmp_subscribed_name_2 = var_url;
                tmp_start_name_2 = Py_None;
                CHECK_OBJECT( var_proto_len );
                tmp_stop_name_2 = var_proto_len;
                tmp_step_name_2 = Py_None;
                tmp_subscript_name_2 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
                assert( !(tmp_subscript_name_2 == NULL) );
                tmp_left_name_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                Py_DECREF( tmp_subscript_name_2 );
                if ( tmp_left_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 346;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_parts );
                tmp_subscribed_name_3 = var_parts;
                tmp_subscript_name_3 = const_int_0;
                tmp_right_name_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
                if ( tmp_right_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_7 );

                    exception_lineno = 347;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_left_name_6 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_5 );
                Py_DECREF( tmp_left_name_7 );
                Py_DECREF( tmp_right_name_5 );
                if ( tmp_left_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 346;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_6 = const_str_chr_47;
                tmp_left_name_5 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_6, tmp_right_name_6 );
                Py_DECREF( tmp_left_name_6 );
                if ( tmp_left_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 348;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_parts );
                tmp_subscribed_name_7 = var_parts;
                tmp_subscript_name_4 = const_int_pos_1;
                tmp_subscribed_name_6 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_7, tmp_subscript_name_4, 1 );
                if ( tmp_subscribed_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_5 );

                    exception_lineno = 349;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_subscript_name_5 = const_slice_none_int_pos_8_none;
                tmp_called_instance_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_5 );
                Py_DECREF( tmp_subscribed_name_6 );
                if ( tmp_called_instance_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_5 );

                    exception_lineno = 349;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 349;
                tmp_subscribed_name_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_63_tuple, 0 ) );

                Py_DECREF( tmp_called_instance_8 );
                if ( tmp_subscribed_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_5 );

                    exception_lineno = 349;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_subscript_name_6 = const_int_0;
                tmp_called_instance_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_6, 0 );
                Py_DECREF( tmp_subscribed_name_5 );
                if ( tmp_called_instance_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_5 );

                    exception_lineno = 349;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 349;
                tmp_subscribed_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

                Py_DECREF( tmp_called_instance_7 );
                if ( tmp_subscribed_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_5 );

                    exception_lineno = 349;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_subscript_name_7 = const_int_0;
                tmp_right_name_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_7, 0 );
                Py_DECREF( tmp_subscribed_name_4 );
                if ( tmp_right_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_5 );

                    exception_lineno = 349;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_11 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_7 );
                Py_DECREF( tmp_left_name_5 );
                Py_DECREF( tmp_right_name_7 );
                if ( tmp_assign_source_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 349;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_url;
                    assert( old != NULL );
                    var_url = tmp_assign_source_11;
                    Py_DECREF( old );
                }

            }
            branch_no_7:;
        }
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_len_arg_5;
            CHECK_OBJECT( var_url );
            tmp_len_arg_5 = var_url;
            tmp_compexpr_left_4 = BUILTIN_LEN( tmp_len_arg_5 );
            if ( tmp_compexpr_left_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 352;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_4 = const_float_45_0;
            tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            Py_DECREF( tmp_compexpr_left_4 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_assign_source_12;
                PyObject *tmp_subscribed_name_8;
                PyObject *tmp_subscript_name_8;
                CHECK_OBJECT( var_url );
                tmp_subscribed_name_8 = var_url;
                tmp_subscript_name_8 = const_slice_none_int_pos_30_none;
                tmp_assign_source_12 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_8 );
                if ( tmp_assign_source_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 353;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_url;
                    assert( old != NULL );
                    var_url = tmp_assign_source_12;
                    Py_DECREF( old );
                }

            }
            branch_no_8:;
        }
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            if ( var_url == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "url" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 355;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_left_5 = var_url;
            CHECK_OBJECT( var_before_clip );
            tmp_compexpr_right_5 = var_before_clip;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 355;
                type_description_1 = "oooooNoooocccc";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                PyObject *tmp_assign_source_13;
                PyObject *tmp_called_instance_9;
                if ( var_url == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "url" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 356;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_9 = var_url;
                frame_bccf81b17a0b884150a8eafb0fd57813->m_frame.f_lineno = 356;
                tmp_assign_source_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_rfind, &PyTuple_GET_ITEM( const_tuple_str_chr_38_tuple, 0 ) );

                if ( tmp_assign_source_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 356;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                assert( var_amp == NULL );
                var_amp = tmp_assign_source_13;
            }
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_compexpr_left_6;
                PyObject *tmp_compexpr_right_6;
                CHECK_OBJECT( var_amp );
                tmp_compexpr_left_6 = var_amp;
                tmp_compexpr_right_6 = const_int_pos_25;
                tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 358;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_10;
                }
                else
                {
                    goto branch_no_10;
                }
                branch_yes_10:;
                {
                    PyObject *tmp_assign_source_14;
                    PyObject *tmp_subscribed_name_9;
                    PyObject *tmp_subscript_name_9;
                    PyObject *tmp_start_name_3;
                    PyObject *tmp_stop_name_3;
                    PyObject *tmp_step_name_3;
                    if ( var_url == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "url" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 359;
                        type_description_1 = "oooooNoooocccc";
                        goto frame_exception_exit_1;
                    }

                    tmp_subscribed_name_9 = var_url;
                    tmp_start_name_3 = Py_None;
                    CHECK_OBJECT( var_amp );
                    tmp_stop_name_3 = var_amp;
                    tmp_step_name_3 = Py_None;
                    tmp_subscript_name_9 = MAKE_SLICEOBJ3( tmp_start_name_3, tmp_stop_name_3, tmp_step_name_3 );
                    assert( !(tmp_subscript_name_9 == NULL) );
                    tmp_assign_source_14 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_9, tmp_subscript_name_9 );
                    Py_DECREF( tmp_subscript_name_9 );
                    if ( tmp_assign_source_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 359;
                        type_description_1 = "oooooNoooocccc";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_url;
                        var_url = tmp_assign_source_14;
                        Py_XDECREF( old );
                    }

                }
                branch_no_10:;
            }
            {
                PyObject *tmp_assign_source_15;
                PyObject *tmp_left_name_8;
                PyObject *tmp_right_name_8;
                if ( var_url == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "url" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 360;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_8 = var_url;
                tmp_right_name_8 = const_str_digest_3501979af1b70861f5e9d6a0f04129bf;
                tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_8, tmp_right_name_8 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 360;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_15 = tmp_left_name_8;
                var_url = tmp_assign_source_15;

            }
            {
                nuitka_bool tmp_condition_result_11;
                PyObject *tmp_compexpr_left_7;
                PyObject *tmp_compexpr_right_7;
                PyObject *tmp_len_arg_6;
                PyObject *tmp_len_arg_7;
                CHECK_OBJECT( var_url );
                tmp_len_arg_6 = var_url;
                tmp_compexpr_left_7 = BUILTIN_LEN( tmp_len_arg_6 );
                if ( tmp_compexpr_left_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 362;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_before_clip );
                tmp_len_arg_7 = var_before_clip;
                tmp_compexpr_right_7 = BUILTIN_LEN( tmp_len_arg_7 );
                if ( tmp_compexpr_right_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_compexpr_left_7 );

                    exception_lineno = 362;
                    type_description_1 = "oooooNoooocccc";
                    goto frame_exception_exit_1;
                }
                tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                Py_DECREF( tmp_compexpr_left_7 );
                Py_DECREF( tmp_compexpr_right_7 );
                assert( !(tmp_res == -1) );
                tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_11;
                }
                else
                {
                    goto branch_no_11;
                }
                branch_yes_11:;
                {
                    PyObject *tmp_assign_source_16;
                    CHECK_OBJECT( var_before_clip );
                    tmp_assign_source_16 = var_before_clip;
                    {
                        PyObject *old = var_url;
                        assert( old != NULL );
                        var_url = tmp_assign_source_16;
                        Py_INCREF( var_url );
                        Py_DECREF( old );
                    }

                }
                goto branch_end_11;
                branch_no_11:;
                {
                    PyObject *tmp_assign_source_17;
                    PyObject *tmp_left_name_9;
                    PyObject *tmp_right_name_9;
                    PyObject *tmp_left_name_10;
                    PyObject *tmp_right_name_10;
                    CHECK_OBJECT( var_params );
                    tmp_left_name_9 = var_params;
                    tmp_left_name_10 = const_str_digest_e3cd4610161aba9eaa28da8430bf368e;
                    CHECK_OBJECT( var_href );
                    tmp_right_name_10 = var_href;
                    tmp_right_name_9 = BINARY_OPERATION_REMAINDER( tmp_left_name_10, tmp_right_name_10 );
                    if ( tmp_right_name_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 367;
                        type_description_1 = "oooooNoooocccc";
                        goto frame_exception_exit_1;
                    }
                    tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_9, tmp_right_name_9 );
                    Py_DECREF( tmp_right_name_9 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 367;
                        type_description_1 = "oooooNoooocccc";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_17 = tmp_left_name_9;
                    var_params = tmp_assign_source_17;

                }
                branch_end_11:;
            }
            branch_no_9:;
        }
        branch_no_5:;
    }
    {
        PyObject *tmp_left_name_11;
        PyObject *tmp_right_name_11;
        PyObject *tmp_tuple_element_1;
        tmp_left_name_11 = const_str_digest_4a3231db8f125f2cc6fa458948ea5c7b;
        CHECK_OBJECT( var_href );
        tmp_tuple_element_1 = var_href;
        tmp_right_name_11 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_11, 0, tmp_tuple_element_1 );
        if ( var_params == NULL )
        {
            Py_DECREF( tmp_right_name_11 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "params" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 369;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = var_params;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_11, 1, tmp_tuple_element_1 );
        if ( var_url == NULL )
        {
            Py_DECREF( tmp_right_name_11 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "url" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 369;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = var_url;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_11, 2, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_11, tmp_right_name_11 );
        Py_DECREF( tmp_right_name_11 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;
            type_description_1 = "oooooNoooocccc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bccf81b17a0b884150a8eafb0fd57813 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bccf81b17a0b884150a8eafb0fd57813 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bccf81b17a0b884150a8eafb0fd57813 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bccf81b17a0b884150a8eafb0fd57813, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bccf81b17a0b884150a8eafb0fd57813->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bccf81b17a0b884150a8eafb0fd57813, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bccf81b17a0b884150a8eafb0fd57813,
        type_description_1,
        par_m,
        var_url,
        var_proto,
        var_href,
        var_params,
        NULL,
        var_before_clip,
        var_proto_len,
        var_parts,
        var_amp,
        self->m_closure[2],
        self->m_closure[1],
        self->m_closure[0],
        self->m_closure[3]
    );


    // Release cached frame.
    if ( frame_bccf81b17a0b884150a8eafb0fd57813 == cache_frame_bccf81b17a0b884150a8eafb0fd57813 )
    {
        Py_DECREF( frame_bccf81b17a0b884150a8eafb0fd57813 );
    }
    cache_frame_bccf81b17a0b884150a8eafb0fd57813 = NULL;

    assertFrameObject( frame_bccf81b17a0b884150a8eafb0fd57813 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_20_linkify$$$function_1_make_link );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    Py_XDECREF( var_url );
    var_url = NULL;

    CHECK_OBJECT( (PyObject *)var_proto );
    Py_DECREF( var_proto );
    var_proto = NULL;

    Py_XDECREF( var_href );
    var_href = NULL;

    Py_XDECREF( var_params );
    var_params = NULL;

    Py_XDECREF( var_before_clip );
    var_before_clip = NULL;

    Py_XDECREF( var_proto_len );
    var_proto_len = NULL;

    Py_XDECREF( var_parts );
    var_parts = NULL;

    Py_XDECREF( var_amp );
    var_amp = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    Py_XDECREF( var_url );
    var_url = NULL;

    Py_XDECREF( var_proto );
    var_proto = NULL;

    Py_XDECREF( var_href );
    var_href = NULL;

    Py_XDECREF( var_params );
    var_params = NULL;

    Py_XDECREF( var_before_clip );
    var_before_clip = NULL;

    Py_XDECREF( var_proto_len );
    var_proto_len = NULL;

    Py_XDECREF( var_parts );
    var_parts = NULL;

    Py_XDECREF( var_amp );
    var_amp = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_20_linkify$$$function_1_make_link );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_21__convert_entity( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_m = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c5446992fcad4079a33861b5cefae205;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_c5446992fcad4079a33861b5cefae205 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c5446992fcad4079a33861b5cefae205, codeobj_c5446992fcad4079a33861b5cefae205, module_tornado$escape, sizeof(void *) );
    frame_c5446992fcad4079a33861b5cefae205 = cache_frame_c5446992fcad4079a33861b5cefae205;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c5446992fcad4079a33861b5cefae205 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c5446992fcad4079a33861b5cefae205 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_m );
        tmp_called_instance_1 = par_m;
        frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = 379;
        tmp_compexpr_left_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 379;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_chr_35;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 379;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( par_m );
            tmp_called_instance_3 = par_m;
            frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = 381;
            tmp_subscribed_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

            if ( tmp_subscribed_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_1 = "o";
                goto try_except_handler_2;
            }
            tmp_subscript_name_1 = const_slice_none_int_pos_1_none;
            tmp_called_instance_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscribed_name_1 );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_1 = "o";
                goto try_except_handler_2;
            }
            frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = 381;
            tmp_compexpr_left_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_lower );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_1 = "o";
                goto try_except_handler_2;
            }
            tmp_compexpr_right_2 = const_str_plain_x;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_1 = "o";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_chr_arg_1;
                PyObject *tmp_value_name_1;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_subscript_name_2;
                PyObject *tmp_base_name_1;
                CHECK_OBJECT( par_m );
                tmp_called_instance_4 = par_m;
                frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = 382;
                tmp_subscribed_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

                if ( tmp_subscribed_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 382;
                    type_description_1 = "o";
                    goto try_except_handler_2;
                }
                tmp_subscript_name_2 = const_slice_int_pos_1_none_none;
                tmp_value_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                Py_DECREF( tmp_subscribed_name_2 );
                if ( tmp_value_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 382;
                    type_description_1 = "o";
                    goto try_except_handler_2;
                }
                tmp_base_name_1 = const_int_pos_16;
                tmp_chr_arg_1 = BUILTIN_INT2( tmp_value_name_1, tmp_base_name_1 );
                Py_DECREF( tmp_value_name_1 );
                if ( tmp_chr_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 382;
                    type_description_1 = "o";
                    goto try_except_handler_2;
                }
                tmp_return_value = BUILTIN_CHR( tmp_chr_arg_1 );
                Py_DECREF( tmp_chr_arg_1 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 382;
                    type_description_1 = "o";
                    goto try_except_handler_2;
                }
                goto frame_return_exit_1;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_chr_arg_2;
                PyObject *tmp_int_arg_1;
                PyObject *tmp_called_instance_5;
                CHECK_OBJECT( par_m );
                tmp_called_instance_5 = par_m;
                frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = 384;
                tmp_int_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

                if ( tmp_int_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "o";
                    goto try_except_handler_2;
                }
                tmp_chr_arg_2 = PyNumber_Int( tmp_int_arg_1 );
                Py_DECREF( tmp_int_arg_1 );
                if ( tmp_chr_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "o";
                    goto try_except_handler_2;
                }
                tmp_return_value = BUILTIN_CHR( tmp_chr_arg_2 );
                Py_DECREF( tmp_chr_arg_2 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "o";
                    goto try_except_handler_2;
                }
                goto frame_return_exit_1;
            }
            branch_end_2:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_21__convert_entity );
        return NULL;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_c5446992fcad4079a33861b5cefae205, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_c5446992fcad4079a33861b5cefae205, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            tmp_compexpr_left_3 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_3 = PyExc_ValueError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 385;
                type_description_1 = "o";
                goto try_except_handler_3;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_called_instance_6;
                tmp_left_name_1 = const_str_digest_93388c71b47089bccb0fc69365887500;
                CHECK_OBJECT( par_m );
                tmp_called_instance_6 = par_m;
                frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = 386;
                tmp_right_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

                if ( tmp_right_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 386;
                    type_description_1 = "o";
                    goto try_except_handler_3;
                }
                tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 386;
                    type_description_1 = "o";
                    goto try_except_handler_3;
                }
                goto try_return_handler_3;
            }
            goto branch_end_3;
            branch_no_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 380;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_c5446992fcad4079a33861b5cefae205->m_frame) frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "o";
            goto try_except_handler_3;
            branch_end_3:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_21__convert_entity );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        // End of try:
        branch_no_1:;
    }
    // Tried code:
    {
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_called_instance_7;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__HTML_UNICODE_MAP );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__HTML_UNICODE_MAP );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_HTML_UNICODE_MAP" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 388;
            type_description_1 = "o";
            goto try_except_handler_4;
        }

        tmp_subscribed_name_3 = tmp_mvar_value_1;
        CHECK_OBJECT( par_m );
        tmp_called_instance_7 = par_m;
        frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = 388;
        tmp_subscript_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_subscript_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 388;
            type_description_1 = "o";
            goto try_except_handler_4;
        }
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        Py_DECREF( tmp_subscript_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 388;
            type_description_1 = "o";
            goto try_except_handler_4;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_21__convert_entity );
    return NULL;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_c5446992fcad4079a33861b5cefae205, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_c5446992fcad4079a33861b5cefae205, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        tmp_compexpr_left_4 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_4 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 389;
            type_description_1 = "o";
            goto try_except_handler_5;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_called_instance_8;
            tmp_left_name_2 = const_str_digest_0a965353587989cc5fc3cd0b4a3488a4;
            CHECK_OBJECT( par_m );
            tmp_called_instance_8 = par_m;
            frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = 390;
            tmp_right_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 390;
                type_description_1 = "o";
                goto try_except_handler_5;
            }
            tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 390;
                type_description_1 = "o";
                goto try_except_handler_5;
            }
            goto try_return_handler_5;
        }
        goto branch_end_4;
        branch_no_4:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 387;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_c5446992fcad4079a33861b5cefae205->m_frame) frame_c5446992fcad4079a33861b5cefae205->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "o";
        goto try_except_handler_5;
        branch_end_4:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_21__convert_entity );
    return NULL;
    // Return handler code:
    try_return_handler_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c5446992fcad4079a33861b5cefae205 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c5446992fcad4079a33861b5cefae205 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c5446992fcad4079a33861b5cefae205 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c5446992fcad4079a33861b5cefae205, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c5446992fcad4079a33861b5cefae205->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c5446992fcad4079a33861b5cefae205, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c5446992fcad4079a33861b5cefae205,
        type_description_1,
        par_m
    );


    // Release cached frame.
    if ( frame_c5446992fcad4079a33861b5cefae205 == cache_frame_c5446992fcad4079a33861b5cefae205 )
    {
        Py_DECREF( frame_c5446992fcad4079a33861b5cefae205 );
    }
    cache_frame_c5446992fcad4079a33861b5cefae205 = NULL;

    assertFrameObject( frame_c5446992fcad4079a33861b5cefae205 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_21__convert_entity );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_21__convert_entity );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$escape$$$function_22__build_unicode_map( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_unicode_map = NULL;
    PyObject *var_name = NULL;
    PyObject *var_value = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_f8e2b05f388388dd3c768271cd977b2f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_f8e2b05f388388dd3c768271cd977b2f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyDict_New();
        assert( var_unicode_map == NULL );
        var_unicode_map = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f8e2b05f388388dd3c768271cd977b2f, codeobj_f8e2b05f388388dd3c768271cd977b2f, module_tornado$escape, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f8e2b05f388388dd3c768271cd977b2f = cache_frame_f8e2b05f388388dd3c768271cd977b2f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f8e2b05f388388dd3c768271cd977b2f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f8e2b05f388388dd3c768271cd977b2f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_html );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_html );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "html" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 395;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_entities );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 395;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_name2codepoint );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 395;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_f8e2b05f388388dd3c768271cd977b2f->m_frame.f_lineno = 395;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 395;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 395;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                exception_lineno = 395;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 395;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooo";
            exception_lineno = 395;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooo";
            exception_lineno = 395;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooo";
                    exception_lineno = 395;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooo";
            exception_lineno = 395;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_name;
            var_name = tmp_assign_source_7;
            Py_INCREF( var_name );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_value;
            var_value = tmp_assign_source_8;
            Py_INCREF( var_value );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_chr_arg_1;
        CHECK_OBJECT( var_value );
        tmp_chr_arg_1 = var_value;
        tmp_dictset_value = BUILTIN_CHR( tmp_chr_arg_1 );
        if ( tmp_dictset_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 396;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_unicode_map );
        tmp_dictset_dict = var_unicode_map;
        CHECK_OBJECT( var_name );
        tmp_dictset_key = var_name;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 396;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 395;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f8e2b05f388388dd3c768271cd977b2f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f8e2b05f388388dd3c768271cd977b2f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f8e2b05f388388dd3c768271cd977b2f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f8e2b05f388388dd3c768271cd977b2f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f8e2b05f388388dd3c768271cd977b2f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f8e2b05f388388dd3c768271cd977b2f,
        type_description_1,
        var_unicode_map,
        var_name,
        var_value
    );


    // Release cached frame.
    if ( frame_f8e2b05f388388dd3c768271cd977b2f == cache_frame_f8e2b05f388388dd3c768271cd977b2f )
    {
        Py_DECREF( frame_f8e2b05f388388dd3c768271cd977b2f );
    }
    cache_frame_f8e2b05f388388dd3c768271cd977b2f = NULL;

    assertFrameObject( frame_f8e2b05f388388dd3c768271cd977b2f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_unicode_map );
    tmp_return_value = var_unicode_map;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_22__build_unicode_map );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_unicode_map );
    Py_DECREF( var_unicode_map );
    var_unicode_map = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)var_unicode_map );
    Py_DECREF( var_unicode_map );
    var_unicode_map = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$escape$$$function_22__build_unicode_map );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_10_parse_qs_bytes( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_10_parse_qs_bytes,
        const_str_plain_parse_qs_bytes,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c9bbec91076591a2b9c659a133d35e3a,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_fed3a5498cbc1161ae9286cb8a8ce2c4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_11_utf8( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_11_utf8,
        const_str_plain_utf8,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_35ae2d10928327a4980ded94705f9a6d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_12_utf8( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_12_utf8,
        const_str_plain_utf8,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f7b4c7467b5095ddd58b86565ca9250a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_13_utf8( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_13_utf8,
        const_str_plain_utf8,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a70f8a699631d4222d3597a3ef735d6f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_14_utf8( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_14_utf8,
        const_str_plain_utf8,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d58793142ff8d50205aeba9863150c26,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_3fdba945c17629f91d696fe3fa5b508a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_15_to_unicode( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_15_to_unicode,
        const_str_plain_to_unicode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_da713263cce893ee9f551664e2ffb3f4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_16_to_unicode( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_16_to_unicode,
        const_str_plain_to_unicode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_54dc8c426c974d5a18757df31e39e82e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_17_to_unicode( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_17_to_unicode,
        const_str_plain_to_unicode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_783c509865856b70579d4dcb9007eaab,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_18_to_unicode( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_18_to_unicode,
        const_str_plain_to_unicode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_be813ac06ef379d66594c8be487f9abc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_f41e016f8aa911562fdcf3a6a6a5e612,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_19_recursive_unicode( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_19_recursive_unicode,
        const_str_plain_recursive_unicode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7383220ca4a74759df7b67f7b0e48cb7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_70c7667bf24de28cd03ec3fd3eaa87e1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_1_xhtml_escape( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_1_xhtml_escape,
        const_str_plain_xhtml_escape,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0f6e65a8c04585fbfadac2f6fca1e53b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_51fd4efe30577acc0163b2fc96ab3c75,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_1_xhtml_escape$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_1_xhtml_escape$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_923e74d046a976c7e2f576a3b2a89286,
#endif
        codeobj_a27444012d0c3a96ef98053d148cd811,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_20_linkify( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_20_linkify,
        const_str_plain_linkify,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_53cf9b7ffbe8ea9a7750e9bad0c565d9,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_4d6910c1b1b9b2832ed31e1aa91dd2b0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_20_linkify$$$function_1_make_link( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_20_linkify$$$function_1_make_link,
        const_str_plain_make_link,
#if PYTHON_VERSION >= 300
        const_str_digest_3d735498de5062618ac9d67223655804,
#endif
        codeobj_bccf81b17a0b884150a8eafb0fd57813,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        4
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_21__convert_entity( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_21__convert_entity,
        const_str_plain__convert_entity,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c5446992fcad4079a33861b5cefae205,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_22__build_unicode_map( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_22__build_unicode_map,
        const_str_plain__build_unicode_map,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f8e2b05f388388dd3c768271cd977b2f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_2_xhtml_unescape( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_2_xhtml_unescape,
        const_str_plain_xhtml_unescape,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7569a0352786d2405fe05a51c3671a5c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_3cc5261cf52c88f0c57b29fa3bd967e3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_3_json_encode( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_3_json_encode,
        const_str_plain_json_encode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_31922533793f55aa78505742334ebb48,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_cd3210c70bf2f05624551f1b595530e4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_4_json_decode( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_4_json_decode,
        const_str_plain_json_decode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_576ea3dc5ed3c857bd8c79ec4dcf0577,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_44838993fea9df39d3b7bc798cbee383,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_5_squeeze( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_5_squeeze,
        const_str_plain_squeeze,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8ebed34ed6c5d940d9936d13171977c1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_a13637e8cd7cb8c057e4d409319b229a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_6_url_escape( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_6_url_escape,
        const_str_plain_url_escape,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_85917ccd43032c186ec182f453f4f39f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_19e50c71cf2d846e79def17f96a4688e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_7_url_unescape( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_7_url_unescape,
        const_str_plain_url_unescape,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e69d731bae6cdc1640635b65955e830f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_8_url_unescape( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_8_url_unescape,
        const_str_plain_url_unescape,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_77d18306c7988cfa785ee2b705ef4f5f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$escape$$$function_9_url_unescape( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$escape$$$function_9_url_unescape,
        const_str_plain_url_unescape,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e80c64282c52818be9dfd1256a205f0f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$escape,
        const_str_digest_2551c1e4e59cf8c15b7d64978f8ef587,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_tornado$escape =
{
    PyModuleDef_HEAD_INIT,
    "tornado.escape",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(tornado$escape)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(tornado$escape)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_tornado$escape );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("tornado.escape: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.escape: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.escape: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in inittornado$escape" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_tornado$escape = Py_InitModule4(
        "tornado.escape",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_tornado$escape = PyModule_Create( &mdef_tornado$escape );
#endif

    moduledict_tornado$escape = MODULE_DICT( module_tornado$escape );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_tornado$escape,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_tornado$escape,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$escape,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$escape,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_tornado$escape );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_84a3f2fedb1ec7918aa30203da68a052, module_tornado$escape );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_a74e88def304d1fbe08fb4688630b4c4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_83c3338071788a985dd4e30e51ae98c4;
        UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_a74e88def304d1fbe08fb4688630b4c4 = MAKE_MODULE_FRAME( codeobj_a74e88def304d1fbe08fb4688630b4c4, module_tornado$escape );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_a74e88def304d1fbe08fb4688630b4c4 );
    assert( Py_REFCNT( frame_a74e88def304d1fbe08fb4688630b4c4 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_f5862b11e62dc1382b5d1226dd5ff379;
        tmp_globals_name_1 = (PyObject *)moduledict_tornado$escape;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 22;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_html, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_json;
        tmp_globals_name_2 = (PyObject *)moduledict_tornado$escape;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 23;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_json, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_re;
        tmp_globals_name_3 = (PyObject *)moduledict_tornado$escape;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 24;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_04991ea695faff4a76e4efb6a8a8593f;
        tmp_globals_name_4 = (PyObject *)moduledict_tornado$escape;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 25;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_urllib, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_e077944e15accbec54ecb40fd81dafde;
        tmp_globals_name_5 = (PyObject *)moduledict_tornado$escape;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_unicode_type_tuple;
        tmp_level_name_5 = const_int_0;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 27;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_type );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_unicode_type, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_typing;
        tmp_globals_name_6 = (PyObject *)moduledict_tornado$escape;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 29;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_typing;
        tmp_globals_name_7 = (PyObject *)moduledict_tornado$escape;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_56b5d8cec332f08075953109a25f8ca5_tuple;
        tmp_level_name_7 = const_int_0;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 30;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Union );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_Any );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Any, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Optional );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Optional, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_Dict );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Dict, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_6 = tmp_import_from_1__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_List );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_List, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_7 = tmp_import_from_1__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_Callable );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Callable, tmp_assign_source_16 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 33;
        tmp_assign_source_17 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_54e10fe59f47c2b318a42cad76bfb3bf_tuple, 0 ) );

        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__XHTML_ESCAPE_RE, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = PyDict_Copy( const_dict_30bf287ffc06b1900ccad90e04ffd31f );
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__XHTML_ESCAPE_DICT, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_annotations_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_dict_key_1 = const_str_plain_value;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_4;
        tmp_subscript_name_1 = const_tuple_type_str_type_bytes_tuple;
        tmp_dict_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto frame_exception_exit_1;
        }
        tmp_annotations_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_return;
        tmp_dict_value_2 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_19 = MAKE_FUNCTION_tornado$escape$$$function_1_xhtml_escape( tmp_annotations_1 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_xhtml_escape, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_annotations_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        tmp_dict_key_3 = const_str_plain_value;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 59;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_2 = tmp_mvar_value_5;
        tmp_subscript_name_2 = const_tuple_type_str_type_bytes_tuple;
        tmp_dict_value_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto frame_exception_exit_1;
        }
        tmp_annotations_2 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_2, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_return;
        tmp_dict_value_4 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_2, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_20 = MAKE_FUNCTION_tornado$escape$$$function_2_xhtml_unescape( tmp_annotations_2 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_xhtml_unescape, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_annotations_3;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        tmp_dict_key_5 = const_str_plain_value;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Any );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_5 = tmp_mvar_value_6;
        tmp_annotations_3 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_3, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_return;
        tmp_dict_value_6 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_3, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_21 = MAKE_FUNCTION_tornado$escape$$$function_3_json_encode( tmp_annotations_3 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_json_encode, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_annotations_4;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_mvar_value_8;
        tmp_dict_key_7 = const_str_plain_value;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 78;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_3 = tmp_mvar_value_7;
        tmp_subscript_name_3 = const_tuple_type_str_type_bytes_tuple;
        tmp_dict_value_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        if ( tmp_dict_value_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;

            goto frame_exception_exit_1;
        }
        tmp_annotations_4 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_return;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Any );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_annotations_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 78;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_8 = tmp_mvar_value_8;
        tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_22 = MAKE_FUNCTION_tornado$escape$$$function_4_json_decode( tmp_annotations_4 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_json_decode, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_annotations_5;
        tmp_annotations_5 = PyDict_Copy( const_dict_8a2a7d46105ecf8375ebf4493d3a5935 );
        tmp_assign_source_23 = MAKE_FUNCTION_tornado$escape$$$function_5_squeeze( tmp_annotations_5 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_squeeze, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_defaults_1;
        PyObject *tmp_annotations_6;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        tmp_defaults_1 = const_tuple_true_tuple;
        tmp_dict_key_9 = const_str_plain_value;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_4 = tmp_mvar_value_9;
        tmp_subscript_name_4 = const_tuple_type_str_type_bytes_tuple;
        tmp_dict_value_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
        if ( tmp_dict_value_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;

            goto frame_exception_exit_1;
        }
        tmp_annotations_6 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_9, tmp_dict_value_9 );
        Py_DECREF( tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_plus;
        tmp_dict_value_10 = (PyObject *)&PyBool_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_11 = const_str_plain_return;
        tmp_dict_value_11 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_24 = MAKE_FUNCTION_tornado$escape$$$function_6_url_escape( tmp_defaults_1, tmp_annotations_6 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_url_escape, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_defaults_2;
        PyObject *tmp_annotations_7;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_10;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_overload );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto frame_exception_exit_1;
        }
        tmp_defaults_2 = const_tuple_true_tuple;
        tmp_dict_key_12 = const_str_plain_value;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_5 = tmp_mvar_value_11;
        tmp_subscript_name_5 = const_tuple_type_str_type_bytes_tuple;
        tmp_dict_value_12 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
        if ( tmp_dict_value_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 107;

            goto frame_exception_exit_1;
        }
        tmp_annotations_7 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_12, tmp_dict_value_12 );
        Py_DECREF( tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_13 = const_str_plain_encoding;
        tmp_dict_value_13 = Py_None;
        tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_13, tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_14 = const_str_plain_plus;
        tmp_dict_value_14 = (PyObject *)&PyBool_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_14, tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_15 = const_str_plain_return;
        tmp_dict_value_15 = (PyObject *)&PyBytes_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_15, tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        Py_INCREF( tmp_defaults_2 );
        tmp_args_element_name_1 = MAKE_FUNCTION_tornado$escape$$$function_7_url_unescape( tmp_defaults_2, tmp_annotations_7 );



        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 106;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_url_unescape, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_defaults_3;
        PyObject *tmp_annotations_8;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_12;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_overload );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto frame_exception_exit_1;
        }
        tmp_defaults_3 = const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_true_tuple;
        tmp_dict_key_16 = const_str_plain_value;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 113;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_6 = tmp_mvar_value_13;
        tmp_subscript_name_6 = const_tuple_type_str_type_bytes_tuple;
        tmp_dict_value_16 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
        if ( tmp_dict_value_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 113;

            goto frame_exception_exit_1;
        }
        tmp_annotations_8 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_annotations_8, tmp_dict_key_16, tmp_dict_value_16 );
        Py_DECREF( tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_17 = const_str_plain_encoding;
        tmp_dict_value_17 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_8, tmp_dict_key_17, tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_18 = const_str_plain_plus;
        tmp_dict_value_18 = (PyObject *)&PyBool_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_8, tmp_dict_key_18, tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_19 = const_str_plain_return;
        tmp_dict_value_19 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_8, tmp_dict_key_19, tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        Py_INCREF( tmp_defaults_3 );
        tmp_args_element_name_2 = MAKE_FUNCTION_tornado$escape$$$function_8_url_unescape( tmp_defaults_3, tmp_annotations_8 );



        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 111;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_url_unescape, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_defaults_4;
        PyObject *tmp_annotations_9;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_subscribed_name_7;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_subscript_name_7;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_subscribed_name_8;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_subscript_name_8;
        PyObject *tmp_dict_key_22;
        PyObject *tmp_dict_value_22;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_subscribed_name_9;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_subscript_name_9;
        tmp_defaults_4 = const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_true_tuple;
        tmp_dict_key_20 = const_str_plain_value;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_7 = tmp_mvar_value_14;
        tmp_subscript_name_7 = const_tuple_type_str_type_bytes_tuple;
        tmp_dict_value_20 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
        if ( tmp_dict_value_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;

            goto frame_exception_exit_1;
        }
        tmp_annotations_9 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_20, tmp_dict_value_20 );
        Py_DECREF( tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_21 = const_str_plain_encoding;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Optional );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Optional );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_annotations_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Optional" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_8 = tmp_mvar_value_15;
        tmp_subscript_name_8 = (PyObject *)&PyUnicode_Type;
        tmp_dict_value_21 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_8 );
        if ( tmp_dict_value_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_annotations_9 );

            exception_lineno = 119;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_21, tmp_dict_value_21 );
        Py_DECREF( tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_22 = const_str_plain_plus;
        tmp_dict_value_22 = (PyObject *)&PyBool_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_22, tmp_dict_value_22 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_23 = const_str_plain_return;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_annotations_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_9 = tmp_mvar_value_16;
        tmp_subscript_name_9 = const_tuple_type_str_type_bytes_tuple;
        tmp_dict_value_23 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_9, tmp_subscript_name_9 );
        if ( tmp_dict_value_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_annotations_9 );

            exception_lineno = 120;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_23, tmp_dict_value_23 );
        Py_DECREF( tmp_dict_value_23 );
        assert( !(tmp_res != 0) );
        Py_INCREF( tmp_defaults_4 );
        tmp_assign_source_27 = MAKE_FUNCTION_tornado$escape$$$function_9_url_unescape( tmp_defaults_4, tmp_annotations_9 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_url_unescape, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_defaults_5;
        PyObject *tmp_annotations_10;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        PyObject *tmp_subscribed_name_10;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_subscript_name_10;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_subscribed_name_11;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_subscript_name_11;
        tmp_defaults_5 = const_tuple_false_false_tuple;
        tmp_dict_key_24 = const_str_plain_qs;
        tmp_dict_value_24 = (PyObject *)&PyUnicode_Type;
        tmp_annotations_10 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_24, tmp_dict_value_24 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_25 = const_str_plain_keep_blank_values;
        tmp_dict_value_25 = (PyObject *)&PyBool_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_25, tmp_dict_value_25 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_26 = const_str_plain_strict_parsing;
        tmp_dict_value_26 = (PyObject *)&PyBool_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_26, tmp_dict_value_26 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_27 = const_str_plain_return;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Dict );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_annotations_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_10 = tmp_mvar_value_17;
        tmp_tuple_element_1 = (PyObject *)&PyUnicode_Type;
        tmp_subscript_name_10 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_subscript_name_10, 0, tmp_tuple_element_1 );
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_List );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_annotations_10 );
            Py_DECREF( tmp_subscript_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_11 = tmp_mvar_value_18;
        tmp_subscript_name_11 = (PyObject *)&PyBytes_Type;
        tmp_tuple_element_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_11, tmp_subscript_name_11 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_annotations_10 );
            Py_DECREF( tmp_subscript_name_10 );

            exception_lineno = 149;

            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_subscript_name_10, 1, tmp_tuple_element_1 );
        tmp_dict_value_27 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_10, tmp_subscript_name_10 );
        Py_DECREF( tmp_subscript_name_10 );
        if ( tmp_dict_value_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_annotations_10 );

            exception_lineno = 149;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_27, tmp_dict_value_27 );
        Py_DECREF( tmp_dict_value_27 );
        assert( !(tmp_res != 0) );
        Py_INCREF( tmp_defaults_5 );
        tmp_assign_source_28 = MAKE_FUNCTION_tornado$escape$$$function_10_parse_qs_bytes( tmp_defaults_5, tmp_annotations_10 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_parse_qs_bytes, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = const_tuple_type_bytes_anon_NoneType_tuple;
        UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__UTF8_TYPES, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_annotations_11;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_19;
        tmp_annotations_11 = PyDict_Copy( const_dict_0d51aaa043ae9db1f2dbb75fac333587 );
        tmp_args_element_name_3 = MAKE_FUNCTION_tornado$escape$$$function_11_utf8( tmp_annotations_11 );



        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 171;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_30 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_overload, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_utf8, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_annotations_12;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_20;
        tmp_annotations_12 = PyDict_Copy( const_dict_5adb0e5ad5df1b9a044047b0bcbbda85 );
        tmp_args_element_name_4 = MAKE_FUNCTION_tornado$escape$$$function_12_utf8( tmp_annotations_12 );



        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 176;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_31 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_overload, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_utf8, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_annotations_13;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_21 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_21;
        tmp_annotations_13 = PyDict_Copy( const_dict_0755fb5af920d0ea293e582e7b7e51f2 );
        tmp_args_element_name_5 = MAKE_FUNCTION_tornado$escape$$$function_13_utf8( tmp_annotations_13 );



        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 181;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_assign_source_32 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_overload, call_args );
        }

        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_utf8, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_annotations_14;
        PyObject *tmp_dict_key_28;
        PyObject *tmp_dict_value_28;
        PyObject *tmp_subscribed_name_12;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_subscript_name_12;
        PyObject *tmp_dict_key_29;
        PyObject *tmp_dict_value_29;
        PyObject *tmp_subscribed_name_13;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_subscript_name_13;
        tmp_dict_key_28 = const_str_plain_value;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_22 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 186;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_12 = tmp_mvar_value_22;
        tmp_subscript_name_12 = const_tuple_none_type_str_type_bytes_tuple;
        tmp_dict_value_28 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_12, tmp_subscript_name_12 );
        if ( tmp_dict_value_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;

            goto frame_exception_exit_1;
        }
        tmp_annotations_14 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_28, tmp_dict_value_28 );
        Py_DECREF( tmp_dict_value_28 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_29 = const_str_plain_return;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Optional );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Optional );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_annotations_14 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Optional" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 186;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_13 = tmp_mvar_value_23;
        tmp_subscript_name_13 = (PyObject *)&PyBytes_Type;
        tmp_dict_value_29 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_13, tmp_subscript_name_13 );
        if ( tmp_dict_value_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_annotations_14 );

            exception_lineno = 186;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_29, tmp_dict_value_29 );
        Py_DECREF( tmp_dict_value_29 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_33 = MAKE_FUNCTION_tornado$escape$$$function_14_utf8( tmp_annotations_14 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_utf8, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_mvar_value_24;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_unicode_type );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode_type );
        }

        if ( tmp_mvar_value_24 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode_type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 199;

            goto frame_exception_exit_1;
        }

        tmp_tuple_element_2 = tmp_mvar_value_24;
        tmp_assign_source_34 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_assign_source_34, 0, tmp_tuple_element_2 );
        tmp_tuple_element_2 = (PyObject *)Py_TYPE( Py_None );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_assign_source_34, 1, tmp_tuple_element_2 );
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__TO_UNICODE_TYPES, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_annotations_15;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 202;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_5 = tmp_mvar_value_25;
        tmp_annotations_15 = PyDict_Copy( const_dict_8a2a7d46105ecf8375ebf4493d3a5935 );
        tmp_args_element_name_6 = MAKE_FUNCTION_tornado$escape$$$function_15_to_unicode( tmp_annotations_15 );



        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 202;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_assign_source_35 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_overload, call_args );
        }

        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_unicode, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_annotations_16;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_26 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 207;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_6 = tmp_mvar_value_26;
        tmp_annotations_16 = PyDict_Copy( const_dict_231f0a9ff30c1cff2216ed4aa4c3c88e );
        tmp_args_element_name_7 = MAKE_FUNCTION_tornado$escape$$$function_16_to_unicode( tmp_annotations_16 );



        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 207;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_assign_source_36 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_overload, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_unicode, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_annotations_17;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_27 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 212;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_7 = tmp_mvar_value_27;
        tmp_annotations_17 = PyDict_Copy( const_dict_0755fb5af920d0ea293e582e7b7e51f2 );
        tmp_args_element_name_8 = MAKE_FUNCTION_tornado$escape$$$function_17_to_unicode( tmp_annotations_17 );



        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 212;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_assign_source_37 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_overload, call_args );
        }

        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_unicode, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_annotations_18;
        PyObject *tmp_dict_key_30;
        PyObject *tmp_dict_value_30;
        PyObject *tmp_subscribed_name_14;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_subscript_name_14;
        PyObject *tmp_dict_key_31;
        PyObject *tmp_dict_value_31;
        PyObject *tmp_subscribed_name_15;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_subscript_name_15;
        tmp_dict_key_30 = const_str_plain_value;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_28 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 217;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_14 = tmp_mvar_value_28;
        tmp_subscript_name_14 = const_tuple_none_type_str_type_bytes_tuple;
        tmp_dict_value_30 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_14, tmp_subscript_name_14 );
        if ( tmp_dict_value_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 217;

            goto frame_exception_exit_1;
        }
        tmp_annotations_18 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_30, tmp_dict_value_30 );
        Py_DECREF( tmp_dict_value_30 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_31 = const_str_plain_return;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Optional );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Optional );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_annotations_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Optional" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 217;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_15 = tmp_mvar_value_29;
        tmp_subscript_name_15 = (PyObject *)&PyUnicode_Type;
        tmp_dict_value_31 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_15, tmp_subscript_name_15 );
        if ( tmp_dict_value_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_annotations_18 );

            exception_lineno = 217;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_31, tmp_dict_value_31 );
        Py_DECREF( tmp_dict_value_31 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_38 = MAKE_FUNCTION_tornado$escape$$$function_18_to_unicode( tmp_annotations_18 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_unicode, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_mvar_value_30;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_unicode );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_to_unicode );
        }

        CHECK_OBJECT( tmp_mvar_value_30 );
        tmp_assign_source_39 = tmp_mvar_value_30;
        UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__unicode, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_mvar_value_31;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_unicode );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_to_unicode );
        }

        CHECK_OBJECT( tmp_mvar_value_31 );
        tmp_assign_source_40 = tmp_mvar_value_31;
        UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_native_str, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_mvar_value_32;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_unicode );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_to_unicode );
        }

        CHECK_OBJECT( tmp_mvar_value_32 );
        tmp_assign_source_41 = tmp_mvar_value_32;
        UPDATE_STRING_DICT0( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_basestring, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_annotations_19;
        PyObject *tmp_dict_key_32;
        PyObject *tmp_dict_value_32;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_dict_key_33;
        PyObject *tmp_dict_value_33;
        PyObject *tmp_mvar_value_34;
        tmp_dict_key_32 = const_str_plain_obj;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Any );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
        }

        if ( tmp_mvar_value_33 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_32 = tmp_mvar_value_33;
        tmp_annotations_19 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_19, tmp_dict_key_32, tmp_dict_value_32 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_33 = const_str_plain_return;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Any );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_annotations_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_33 = tmp_mvar_value_34;
        tmp_res = PyDict_SetItem( tmp_annotations_19, tmp_dict_key_33, tmp_dict_value_33 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_42 = MAKE_FUNCTION_tornado$escape$$$function_19_recursive_unicode( tmp_annotations_19 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_recursive_unicode, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_36;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_35 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 266;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_35;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_compile );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_to_unicode );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_to_unicode );
        }

        if ( tmp_mvar_value_36 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "to_unicode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 267;

            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_36;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 267;
        tmp_args_element_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_str_digest_c6564c886ff8a30837617a6ed3437441_tuple, 0 ) );

        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 267;

            goto frame_exception_exit_1;
        }
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 266;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_assign_source_43 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__URL_RE, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_defaults_6;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_annotations_20;
        PyObject *tmp_dict_key_34;
        PyObject *tmp_dict_value_34;
        PyObject *tmp_subscribed_name_16;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_subscript_name_16;
        PyObject *tmp_dict_key_35;
        PyObject *tmp_dict_value_35;
        PyObject *tmp_dict_key_36;
        PyObject *tmp_dict_value_36;
        PyObject *tmp_subscribed_name_17;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_subscript_name_17;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_subscribed_name_18;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_subscript_name_18;
        PyObject *tmp_dict_key_37;
        PyObject *tmp_dict_value_37;
        PyObject *tmp_dict_key_38;
        PyObject *tmp_dict_value_38;
        PyObject *tmp_subscribed_name_19;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_subscript_name_19;
        PyObject *tmp_dict_key_39;
        PyObject *tmp_dict_value_39;
        tmp_tuple_element_3 = Py_False;
        tmp_defaults_6 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_defaults_6, 0, tmp_tuple_element_3 );
        tmp_tuple_element_3 = const_str_empty;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_defaults_6, 1, tmp_tuple_element_3 );
        tmp_tuple_element_3 = Py_False;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_defaults_6, 2, tmp_tuple_element_3 );
        tmp_tuple_element_3 = LIST_COPY( const_list_str_plain_http_str_plain_https_list );
        PyTuple_SET_ITEM( tmp_defaults_6, 3, tmp_tuple_element_3 );
        tmp_dict_key_34 = const_str_plain_text;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_37 == NULL )
        {
            Py_DECREF( tmp_defaults_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 274;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_16 = tmp_mvar_value_37;
        tmp_subscript_name_16 = const_tuple_type_str_type_bytes_tuple;
        tmp_dict_value_34 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_16, tmp_subscript_name_16 );
        if ( tmp_dict_value_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_defaults_6 );

            exception_lineno = 274;

            goto frame_exception_exit_1;
        }
        tmp_annotations_20 = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_34, tmp_dict_value_34 );
        Py_DECREF( tmp_dict_value_34 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_35 = const_str_plain_shorten;
        tmp_dict_value_35 = (PyObject *)&PyBool_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_35, tmp_dict_value_35 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_36 = const_str_plain_extra_params;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Union );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
        }

        if ( tmp_mvar_value_38 == NULL )
        {
            Py_DECREF( tmp_defaults_6 );
            Py_DECREF( tmp_annotations_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_17 = tmp_mvar_value_38;
        tmp_tuple_element_4 = (PyObject *)&PyUnicode_Type;
        tmp_subscript_name_17 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_subscript_name_17, 0, tmp_tuple_element_4 );
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Callable );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
        }

        if ( tmp_mvar_value_39 == NULL )
        {
            Py_DECREF( tmp_defaults_6 );
            Py_DECREF( tmp_annotations_20 );
            Py_DECREF( tmp_subscript_name_17 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_18 = tmp_mvar_value_39;
        tmp_subscript_name_18 = DEEP_COPY( const_tuple_list_type_str_list_type_str_tuple );
        tmp_tuple_element_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_18, tmp_subscript_name_18 );
        Py_DECREF( tmp_subscript_name_18 );
        if ( tmp_tuple_element_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_defaults_6 );
            Py_DECREF( tmp_annotations_20 );
            Py_DECREF( tmp_subscript_name_17 );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_subscript_name_17, 1, tmp_tuple_element_4 );
        tmp_dict_value_36 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_17, tmp_subscript_name_17 );
        Py_DECREF( tmp_subscript_name_17 );
        if ( tmp_dict_value_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_defaults_6 );
            Py_DECREF( tmp_annotations_20 );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_36, tmp_dict_value_36 );
        Py_DECREF( tmp_dict_value_36 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_37 = const_str_plain_require_protocol;
        tmp_dict_value_37 = (PyObject *)&PyBool_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_37, tmp_dict_value_37 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_38 = const_str_plain_permitted_protocols;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_List );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
        }

        if ( tmp_mvar_value_40 == NULL )
        {
            Py_DECREF( tmp_defaults_6 );
            Py_DECREF( tmp_annotations_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 278;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_19 = tmp_mvar_value_40;
        tmp_subscript_name_19 = (PyObject *)&PyUnicode_Type;
        tmp_dict_value_38 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_19, tmp_subscript_name_19 );
        if ( tmp_dict_value_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_defaults_6 );
            Py_DECREF( tmp_annotations_20 );

            exception_lineno = 278;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_38, tmp_dict_value_38 );
        Py_DECREF( tmp_dict_value_38 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_39 = const_str_plain_return;
        tmp_dict_value_39 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_39, tmp_dict_value_39 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_44 = MAKE_FUNCTION_tornado$escape$$$function_20_linkify( tmp_defaults_6, tmp_annotations_20 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_linkify, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_annotations_21;
        PyObject *tmp_dict_key_40;
        PyObject *tmp_dict_value_40;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_dict_key_41;
        PyObject *tmp_dict_value_41;
        tmp_dict_key_40 = const_str_plain_m;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_41 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 378;

            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_41;
        tmp_dict_value_40 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_Match );
        if ( tmp_dict_value_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 378;

            goto frame_exception_exit_1;
        }
        tmp_annotations_21 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_annotations_21, tmp_dict_key_40, tmp_dict_value_40 );
        Py_DECREF( tmp_dict_value_40 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_41 = const_str_plain_return;
        tmp_dict_value_41 = (PyObject *)&PyUnicode_Type;
        tmp_res = PyDict_SetItem( tmp_annotations_21, tmp_dict_key_41, tmp_dict_value_41 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_45 = MAKE_FUNCTION_tornado$escape$$$function_21__convert_entity( tmp_annotations_21 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__convert_entity, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_annotations_22;
        PyObject *tmp_dict_key_42;
        PyObject *tmp_dict_value_42;
        PyObject *tmp_subscribed_name_20;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_subscript_name_20;
        tmp_dict_key_42 = const_str_plain_return;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain_Dict );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
        }

        if ( tmp_mvar_value_42 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 393;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_20 = tmp_mvar_value_42;
        tmp_subscript_name_20 = const_tuple_type_str_type_str_tuple;
        tmp_dict_value_42 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_20, tmp_subscript_name_20 );
        if ( tmp_dict_value_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 393;

            goto frame_exception_exit_1;
        }
        tmp_annotations_22 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_annotations_22, tmp_dict_key_42, tmp_dict_value_42 );
        Py_DECREF( tmp_dict_value_42 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_46 = MAKE_FUNCTION_tornado$escape$$$function_22__build_unicode_map( tmp_annotations_22 );



        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__build_unicode_map, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_43;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__build_unicode_map );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__build_unicode_map );
        }

        CHECK_OBJECT( tmp_mvar_value_43 );
        tmp_called_name_5 = tmp_mvar_value_43;
        frame_a74e88def304d1fbe08fb4688630b4c4->m_frame.f_lineno = 400;
        tmp_assign_source_47 = CALL_FUNCTION_NO_ARGS( tmp_called_name_5 );
        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 400;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$escape, (Nuitka_StringObject *)const_str_plain__HTML_UNICODE_MAP, tmp_assign_source_47 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a74e88def304d1fbe08fb4688630b4c4 );
#endif
    popFrameStack();

    assertFrameObject( frame_a74e88def304d1fbe08fb4688630b4c4 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a74e88def304d1fbe08fb4688630b4c4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a74e88def304d1fbe08fb4688630b4c4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a74e88def304d1fbe08fb4688630b4c4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a74e88def304d1fbe08fb4688630b4c4, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_tornado$escape );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
