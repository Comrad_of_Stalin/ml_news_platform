/* Generated code for Python module 'scipy._lib.six'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_scipy$_lib$six" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_scipy$_lib$six;
PyDictObject *moduledict_scipy$_lib$six;

/* The declarations of module constants used, if any. */
extern PyObject *const_tuple_str_plain_i_tuple;
extern PyObject *const_str_plain__func_code;
extern PyObject *const_str_plain_itertools;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_big;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_tuple_str_plain_sep_none_tuple;
extern PyObject *const_str_plain___name__;
extern PyObject *const_int_pos_2147483647;
extern PyObject *const_str_plain_get_method_function;
extern PyObject *const_str_plain_sep;
extern PyObject *const_str_digest_3f74110888c25263bf2dd7e48a46f88c;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_builtins;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_zip;
extern PyObject *const_str_plain_i;
extern PyObject *const_tuple_str_space_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_chr;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_digest_a343d5bd65de7d15893f1a0d02d09f45;
extern PyObject *const_str_plain_arg;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain_bytes;
extern PyObject *const_str_plain_binary_type;
extern PyObject *const_str_plain_encode;
extern PyObject *const_str_plain_print_;
extern PyObject *const_str_plain_ClassType;
static PyObject *const_str_digest_9c1efbd2c0b52e4bf9af9259bd597447;
extern PyObject *const_str_digest_0e07ea38c4cf2fbcefc89e7de4db934c;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_klass_tuple;
extern PyObject *const_str_digest_31d25caf7489e6241458a1761a0b32ce;
static PyObject *const_str_digest_bf654a8a4d8c3e4a63d0fc540f5f4f9e;
extern PyObject *const_str_plain_items;
extern PyObject *const_str_plain_methodcaller;
extern PyObject *const_tuple_str_plain_s_tuple;
extern PyObject *const_str_plain___func__;
extern PyObject *const_str_digest_8f714300b54df1f07c5ea6ab3e1ef1e0;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_end;
static PyObject *const_str_digest_bf6462317c060c308303e28ee40ba3d6;
extern PyObject *const_str_plain_callable;
extern PyObject *const_str_plain__meth_self;
extern PyObject *const_str_digest_c18de21b4af59af9eda7ad848834774a;
extern PyObject *const_str_plain_get_method_self;
extern PyObject *const_str_plain_next;
extern PyObject *const_str_plain_func;
extern PyObject *const_str_plain_stdout;
extern PyObject *const_str_plain_frame;
extern PyObject *const_str_digest_60d0662f4d422d418678c2ec7a6d653a;
extern PyObject *const_str_plain_platform;
extern PyObject *const_tuple_int_pos_1_tuple;
static PyObject *const_str_plain_NewBase;
extern PyObject *const_str_plain_PY3;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_tuple_str_plain_d_tuple;
extern PyObject *const_str_plain_fp;
extern PyObject *const_str_digest_c1940ed36d6c651776a6e3a34c7131e3;
extern PyObject *const_str_plain_meta;
extern PyObject *const_str_plain_advance_iterator;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_tuple_str_plain_end_none_tuple;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_data;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_plain___len__;
extern PyObject *const_str_plain_it;
extern PyObject *const_str_plain_int2byte;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_digest_c88389693d9d476899c1c6e170568469;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_iterkeys;
extern PyObject *const_str_plain_long;
extern PyObject *const_str_plain_get_unbound_function;
static PyObject *const_tuple_53b185132f93cf72d418b8d6bb6abd52_tuple;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_im_func;
extern PyObject *const_str_plain_func_code;
extern PyObject *const_str_plain_int;
extern PyObject *const_str_plain_string_types;
extern PyObject *const_tuple_str_plain_unbound_tuple;
extern PyObject *const_str_plain_value;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_unicode;
extern PyObject *const_str_plain_BytesIO;
static PyObject *const_str_digest_cbad6fa17e35a2e54136d0d2ccd35889;
extern PyObject *const_str_plain_iteritems;
extern PyObject *const_str_digest_a13035c59d0f0e8c04a4feacc0004cd7;
extern PyObject *const_str_plain_f_locals;
extern PyObject *const_str_plain__iterkeys;
extern PyObject *const_str_digest_7e4f7e7d87a44d0e81f11f08170c9bf6;
extern PyObject *const_str_plain_keys;
extern PyObject *const_str_plain__getframe;
extern PyObject *const_str_plain__add_doc;
extern PyObject *const_tuple_str_newline_tuple;
extern PyObject *const_str_plain_f_globals;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_integer_types;
extern PyObject *const_str_plain_enumerate;
extern PyObject *const_str_space;
extern PyObject *const_tuple_str_digest_cac36d305945ac2bbd2d1b0c1a3dadc8_tuple;
extern PyObject *const_str_plain__iteritems;
extern PyObject *const_tuple_9e38f63b318fbc32eee061b0bb0a0d33_tuple;
extern PyObject *const_str_digest_97d8b3849ccb4841962c7ac18f0af059;
static PyObject *const_tuple_str_plain_data_str_plain_fp_tuple;
extern PyObject *const_str_plain_izip;
extern PyObject *const_str_plain_attrgetter;
extern PyObject *const_str_plain_name;
static PyObject *const_str_digest_f0cfbd9f8eb972911b7984f49f2ce579;
extern PyObject *const_str_digest_1e91aab42d00f04e391d84545f09e2f5;
extern PyObject *const_int_pos_9223372036854775807;
extern PyObject *const_str_plain_get_function_defaults;
extern PyObject *const_str_plain_b;
extern PyObject *const_tuple_str_plain_it_tuple;
extern PyObject *const_str_plain_locs;
extern PyObject *const_str_plain_xrange;
extern PyObject *const_str_digest_91987f05961643baf523130276babe51;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_tuple_type_type_tuple;
extern PyObject *const_str_plain_exec_;
extern PyObject *const_str_plain_space;
extern PyObject *const_str_plain__import_module;
extern PyObject *const_str_plain_pop;
extern PyObject *const_str_plain___version__;
extern PyObject *const_int_0;
extern PyObject *const_tuple_str_plain_func_str_plain_doc_tuple;
extern PyObject *const_str_plain_tb;
extern PyObject *const_str_digest_455f96f9ec5d27095599ab17da15470e;
extern PyObject *const_str_plain_maxsize;
extern PyObject *const_str_plain_code;
extern PyObject *const_str_digest_1d2e267f2ccdeb84fbf4cb4191414e9f;
extern PyObject *const_str_plain_unicode_escape;
extern PyObject *const_str_plain__meth_func;
extern PyObject *const_str_plain___author__;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_newline;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_func_defaults;
extern PyObject *const_str_digest_a13cea3eac144232780ec70fab3f1816;
extern PyObject *const_str_plain___code__;
extern PyObject *const_str_digest_ebf751844f7678efef5423dab5acd577;
extern PyObject *const_tuple_type_int_tuple;
extern PyObject *const_str_plain_get_function_code;
extern PyObject *const_str_plain_base;
extern PyObject *const_str_plain_java;
extern PyObject *const_str_plain_Iterator;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_digest_cac36d305945ac2bbd2d1b0c1a3dadc8;
extern PyObject *const_str_digest_cce73c34157ad7a21f7265310a153ec1;
extern PyObject *const_str_plain_d;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_modules;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain___defaults__;
extern PyObject *const_str_plain___traceback__;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_digest_6fdf710e7d63e48e68391e1f587ae0a5;
extern PyObject *const_str_plain_basestring;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_functools;
extern PyObject *const_str_plain_tp;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_io;
extern PyObject *const_str_plain_StringIO;
extern PyObject *const_str_plain_text_type;
extern PyObject *const_str_plain_want_unicode;
extern PyObject *const_str_plain_doc;
extern PyObject *const_str_plain_range;
extern PyObject *const_str_plain___next__;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_values;
extern PyObject *const_tuple_str_plain_tp_str_plain_value_str_plain_tb_tuple;
extern PyObject *const_str_plain___builtin__;
extern PyObject *const_str_plain_with_traceback;
extern PyObject *const_str_plain_reduce;
extern PyObject *const_str_digest_62b7e67e29e99420603c12d205222fa5;
extern PyObject *const_tuple_str_plain_obj_tuple;
extern PyObject *const_str_plain_X;
extern PyObject *const_str_newline;
extern PyObject *const_str_digest_52da6974eceb6e05255f92901fc2e2f2;
extern PyObject *const_str_plain__func_defaults;
extern PyObject *const_str_plain_print;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_types;
extern PyObject *const_int_pos_2147483648;
extern PyObject *const_str_plain_itervalues;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain___mro__;
extern PyObject *const_tuple_str_plain_name_tuple;
extern PyObject *const_str_plain_write;
extern PyObject *const_str_plain_version_info;
extern PyObject *const_tuple_str_plain_java_tuple;
extern PyObject *const_str_plain___call__;
extern PyObject *const_str_plain_im_self;
extern PyObject *const_str_plain_u;
extern PyObject *const_str_plain_operator;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain___self__;
extern PyObject *const_str_plain_MAXSIZE;
extern PyObject *const_str_plain_unbound;
extern PyObject *const_str_plain_class_types;
static PyObject *const_str_plain_to_bytes;
static PyObject *const_tuple_str_plain_meta_str_plain_base_tuple;
extern PyObject *const_str_plain_obj;
extern PyObject *const_str_plain_startswith;
extern PyObject *const_str_plain_globs;
extern PyObject *const_str_angle_string;
extern PyObject *const_str_plain_klass;
extern PyObject *const_tuple_type_str_tuple;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_str_plain_reraise;
extern PyObject *const_str_plain_with_metaclass;
extern PyObject *const_str_plain_file;
static PyObject *const_tuple_str_plain_to_bytes_int_pos_1_str_plain_big_tuple;
extern PyObject *const_tuple_str_digest_62b7e67e29e99420603c12d205222fa5_tuple;
extern PyObject *const_str_plain_exec;
extern PyObject *const_str_plain__itervalues;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_9c1efbd2c0b52e4bf9af9259bd597447 = UNSTREAM_STRING_ASCII( &constant_bin[ 5305777 ], 5, 0 );
    const_str_digest_bf654a8a4d8c3e4a63d0fc540f5f4f9e = UNSTREAM_STRING_ASCII( &constant_bin[ 5305782 ], 24, 0 );
    const_str_digest_bf6462317c060c308303e28ee40ba3d6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5305806 ], 29, 0 );
    const_str_plain_NewBase = UNSTREAM_STRING_ASCII( &constant_bin[ 921513 ], 7, 1 );
    const_tuple_53b185132f93cf72d418b8d6bb6abd52_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_53b185132f93cf72d418b8d6bb6abd52_tuple, 0, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_53b185132f93cf72d418b8d6bb6abd52_tuple, 1, const_str_plain_globs ); Py_INCREF( const_str_plain_globs );
    PyTuple_SET_ITEM( const_tuple_53b185132f93cf72d418b8d6bb6abd52_tuple, 2, const_str_plain_locs ); Py_INCREF( const_str_plain_locs );
    PyTuple_SET_ITEM( const_tuple_53b185132f93cf72d418b8d6bb6abd52_tuple, 3, const_str_plain_frame ); Py_INCREF( const_str_plain_frame );
    const_str_digest_cbad6fa17e35a2e54136d0d2ccd35889 = UNSTREAM_STRING_ASCII( &constant_bin[ 5305835 ], 23, 0 );
    const_tuple_str_plain_data_str_plain_fp_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_data_str_plain_fp_tuple, 0, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_str_plain_data_str_plain_fp_tuple, 1, const_str_plain_fp ); Py_INCREF( const_str_plain_fp );
    const_str_digest_f0cfbd9f8eb972911b7984f49f2ce579 = UNSTREAM_STRING_ASCII( &constant_bin[ 5305858 ], 17, 0 );
    const_str_plain_to_bytes = UNSTREAM_STRING_ASCII( &constant_bin[ 5305875 ], 8, 1 );
    const_tuple_str_plain_meta_str_plain_base_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_meta_str_plain_base_tuple, 0, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    PyTuple_SET_ITEM( const_tuple_str_plain_meta_str_plain_base_tuple, 1, const_str_plain_base ); Py_INCREF( const_str_plain_base );
    const_tuple_str_plain_to_bytes_int_pos_1_str_plain_big_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_to_bytes_int_pos_1_str_plain_big_tuple, 0, const_str_plain_to_bytes ); Py_INCREF( const_str_plain_to_bytes );
    PyTuple_SET_ITEM( const_tuple_str_plain_to_bytes_int_pos_1_str_plain_big_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_to_bytes_int_pos_1_str_plain_big_tuple, 2, const_str_plain_big ); Py_INCREF( const_str_plain_big );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_scipy$_lib$six( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_23001572356707199f6edf7bdf3e8abf;
static PyCodeObject *codeobj_3ee8b0c17a2ecfea3609cfca70f074b6;
static PyCodeObject *codeobj_67dd5b4a8fd268387081fcc7bcbc8cdb;
static PyCodeObject *codeobj_5c528ecdf690c8150d8f95ba5b7f1eaf;
static PyCodeObject *codeobj_2a2c1390bb73bf41f20d2de2acea5c69;
static PyCodeObject *codeobj_05e2aeb98ecfd1508c8ffbd4aa1975ab;
static PyCodeObject *codeobj_82f8e0fe7c152ff2621242cabf01b838;
static PyCodeObject *codeobj_ea10ee9205bf679b152ff579159389da;
static PyCodeObject *codeobj_16fed4ff5646227d79fdff20460eb780;
static PyCodeObject *codeobj_80bb875a2a5fc6d676782ae7a7a3a996;
static PyCodeObject *codeobj_2432f271455b7775874bb4ab49abd71f;
static PyCodeObject *codeobj_6eeeaa6194731f51f4fa045b9dd14ef4;
static PyCodeObject *codeobj_67fcdf844f2101dc4ec15101b7953074;
static PyCodeObject *codeobj_1aaf6b6023698448d5f7ac2e0bf5dd0a;
static PyCodeObject *codeobj_a81fa7d4eca47427b7794e8d692d4801;
static PyCodeObject *codeobj_98ea076e0d11b1f290db6452e882771b;
static PyCodeObject *codeobj_b6030ec6a902e6f0abef7279653d5a06;
static PyCodeObject *codeobj_9fe3ad08dcb715ff85eb1ad47f5900b9;
static PyCodeObject *codeobj_838e87afeffb6c2a0767423807a7a50c;
static PyCodeObject *codeobj_3eaa836dc69e23c1c2be309dafba3082;
static PyCodeObject *codeobj_494be2abb0305286f9cafe7199cd6a2d;
static PyCodeObject *codeobj_daa36a6a2433ddc633503be641b7168f;
static PyCodeObject *codeobj_2342e73875e6dea6fc2fcf23b3cf9222;
static PyCodeObject *codeobj_5a3e090dd71bbef1e8aeeeaa9a1475c0;
static PyCodeObject *codeobj_3e3f8f76419930c63662a1a22a2eb59d;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_f0cfbd9f8eb972911b7984f49f2ce579 );
    codeobj_23001572356707199f6edf7bdf3e8abf = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 131, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_klass_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3ee8b0c17a2ecfea3609cfca70f074b6 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_cbad6fa17e35a2e54136d0d2ccd35889, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_67dd5b4a8fd268387081fcc7bcbc8cdb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Iterator, 136, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_5c528ecdf690c8150d8f95ba5b7f1eaf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_X, 53, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_2a2c1390bb73bf41f20d2de2acea5c69 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___len__, 54, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_05e2aeb98ecfd1508c8ffbd4aa1975ab = MAKE_CODEOBJ( module_filename_obj, const_str_plain__add_doc, 67, const_tuple_str_plain_func_str_plain_doc_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_82f8e0fe7c152ff2621242cabf01b838 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__import_module, 72, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ea10ee9205bf679b152ff579159389da = MAKE_CODEOBJ( module_filename_obj, const_str_plain_advance_iterator, 119, const_tuple_str_plain_it_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_16fed4ff5646227d79fdff20460eb780 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_b, 168, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_80bb875a2a5fc6d676782ae7a7a3a996 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_b, 184, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2432f271455b7775874bb4ab49abd71f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_callable, 130, const_tuple_str_plain_obj_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6eeeaa6194731f51f4fa045b9dd14ef4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_exec_, 209, const_tuple_53b185132f93cf72d418b8d6bb6abd52_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_67fcdf844f2101dc4ec15101b7953074 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_unbound_function, 125, const_tuple_str_plain_unbound_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1aaf6b6023698448d5f7ac2e0bf5dd0a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_unbound_function, 133, const_tuple_str_plain_unbound_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a81fa7d4eca47427b7794e8d692d4801 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_int2byte, 175, const_tuple_str_plain_i_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_98ea076e0d11b1f290db6452e882771b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_iteritems, 162, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b6030ec6a902e6f0abef7279653d5a06 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_iterkeys, 152, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9fe3ad08dcb715ff85eb1ad47f5900b9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_itervalues, 157, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_838e87afeffb6c2a0767423807a7a50c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_next, 138, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3eaa836dc69e23c1c2be309dafba3082 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_print_, 225, const_tuple_9e38f63b318fbc32eee061b0bb0a0d33_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_494be2abb0305286f9cafe7199cd6a2d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reraise, 200, const_tuple_str_plain_tp_str_plain_value_str_plain_tb_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_daa36a6a2433ddc633503be641b7168f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_u, 171, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2342e73875e6dea6fc2fcf23b3cf9222 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_u, 187, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5a3e090dd71bbef1e8aeeeaa9a1475c0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_with_metaclass, 274, const_tuple_str_plain_meta_str_plain_base_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3e3f8f76419930c63662a1a22a2eb59d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_write, 231, const_tuple_str_plain_data_str_plain_fp_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
}

// The module function declarations.
static PyObject *scipy$_lib$six$$$function_6_callable$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_10_itervalues(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_11_iteritems(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_12_b(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_13_u(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_14_int2byte(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_15_b(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_16_u(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_17_reraise( PyObject *defaults );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_18_exec_( PyObject *defaults );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_19_print_(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_19_print_$$$function_1_write(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_1___len__(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_20_with_metaclass( PyObject *defaults );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_2__add_doc(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_3__import_module(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_4_advance_iterator(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_5_get_unbound_function(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_6_callable(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_7_get_unbound_function(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_8_next(  );


static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_9_iterkeys(  );


// The module function definitions.
static PyObject *impl_scipy$_lib$six$$$function_1___len__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_int_pos_2147483648;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_1___len__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_1___len__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_2__add_doc( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_func = python_pars[ 0 ];
    PyObject *par_doc = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_05e2aeb98ecfd1508c8ffbd4aa1975ab;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_05e2aeb98ecfd1508c8ffbd4aa1975ab = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_05e2aeb98ecfd1508c8ffbd4aa1975ab, codeobj_05e2aeb98ecfd1508c8ffbd4aa1975ab, module_scipy$_lib$six, sizeof(void *)+sizeof(void *) );
    frame_05e2aeb98ecfd1508c8ffbd4aa1975ab = cache_frame_05e2aeb98ecfd1508c8ffbd4aa1975ab;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_05e2aeb98ecfd1508c8ffbd4aa1975ab );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_05e2aeb98ecfd1508c8ffbd4aa1975ab ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_doc );
        tmp_assattr_name_1 = par_doc;
        CHECK_OBJECT( par_func );
        tmp_assattr_target_1 = par_func;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain___doc__, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_05e2aeb98ecfd1508c8ffbd4aa1975ab );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_05e2aeb98ecfd1508c8ffbd4aa1975ab );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_05e2aeb98ecfd1508c8ffbd4aa1975ab, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_05e2aeb98ecfd1508c8ffbd4aa1975ab->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_05e2aeb98ecfd1508c8ffbd4aa1975ab, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_05e2aeb98ecfd1508c8ffbd4aa1975ab,
        type_description_1,
        par_func,
        par_doc
    );


    // Release cached frame.
    if ( frame_05e2aeb98ecfd1508c8ffbd4aa1975ab == cache_frame_05e2aeb98ecfd1508c8ffbd4aa1975ab )
    {
        Py_DECREF( frame_05e2aeb98ecfd1508c8ffbd4aa1975ab );
    }
    cache_frame_05e2aeb98ecfd1508c8ffbd4aa1975ab = NULL;

    assertFrameObject( frame_05e2aeb98ecfd1508c8ffbd4aa1975ab );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_2__add_doc );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    CHECK_OBJECT( (PyObject *)par_doc );
    Py_DECREF( par_doc );
    par_doc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    CHECK_OBJECT( (PyObject *)par_doc );
    Py_DECREF( par_doc );
    par_doc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_2__add_doc );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_3__import_module( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_82f8e0fe7c152ff2621242cabf01b838;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_82f8e0fe7c152ff2621242cabf01b838 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_82f8e0fe7c152ff2621242cabf01b838, codeobj_82f8e0fe7c152ff2621242cabf01b838, module_scipy$_lib$six, sizeof(void *) );
    frame_82f8e0fe7c152ff2621242cabf01b838 = cache_frame_82f8e0fe7c152ff2621242cabf01b838;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_82f8e0fe7c152ff2621242cabf01b838 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_82f8e0fe7c152ff2621242cabf01b838 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_name_name_1;
        PyObject *tmp_imported_value_1;
        CHECK_OBJECT( par_name );
        tmp_name_name_1 = par_name;
        frame_82f8e0fe7c152ff2621242cabf01b838->m_frame.f_lineno = 74;
        tmp_imported_value_1 = IMPORT_MODULE1( tmp_name_name_1 );
        if ( tmp_imported_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_imported_value_1 );
    }
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 75;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_modules );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_subscript_name_1 = par_name;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_82f8e0fe7c152ff2621242cabf01b838 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_82f8e0fe7c152ff2621242cabf01b838 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_82f8e0fe7c152ff2621242cabf01b838 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_82f8e0fe7c152ff2621242cabf01b838, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_82f8e0fe7c152ff2621242cabf01b838->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_82f8e0fe7c152ff2621242cabf01b838, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_82f8e0fe7c152ff2621242cabf01b838,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_82f8e0fe7c152ff2621242cabf01b838 == cache_frame_82f8e0fe7c152ff2621242cabf01b838 )
    {
        Py_DECREF( frame_82f8e0fe7c152ff2621242cabf01b838 );
    }
    cache_frame_82f8e0fe7c152ff2621242cabf01b838 = NULL;

    assertFrameObject( frame_82f8e0fe7c152ff2621242cabf01b838 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_3__import_module );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_3__import_module );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_4_advance_iterator( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_it = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ea10ee9205bf679b152ff579159389da;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ea10ee9205bf679b152ff579159389da = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ea10ee9205bf679b152ff579159389da, codeobj_ea10ee9205bf679b152ff579159389da, module_scipy$_lib$six, sizeof(void *) );
    frame_ea10ee9205bf679b152ff579159389da = cache_frame_ea10ee9205bf679b152ff579159389da;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ea10ee9205bf679b152ff579159389da );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ea10ee9205bf679b152ff579159389da ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_it );
        tmp_called_instance_1 = par_it;
        frame_ea10ee9205bf679b152ff579159389da->m_frame.f_lineno = 120;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_next );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ea10ee9205bf679b152ff579159389da );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ea10ee9205bf679b152ff579159389da );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ea10ee9205bf679b152ff579159389da );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ea10ee9205bf679b152ff579159389da, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ea10ee9205bf679b152ff579159389da->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ea10ee9205bf679b152ff579159389da, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ea10ee9205bf679b152ff579159389da,
        type_description_1,
        par_it
    );


    // Release cached frame.
    if ( frame_ea10ee9205bf679b152ff579159389da == cache_frame_ea10ee9205bf679b152ff579159389da )
    {
        Py_DECREF( frame_ea10ee9205bf679b152ff579159389da );
    }
    cache_frame_ea10ee9205bf679b152ff579159389da = NULL;

    assertFrameObject( frame_ea10ee9205bf679b152ff579159389da );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_4_advance_iterator );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_it );
    Py_DECREF( par_it );
    par_it = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_it );
    Py_DECREF( par_it );
    par_it = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_4_advance_iterator );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_5_get_unbound_function( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_unbound = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_unbound );
    tmp_return_value = par_unbound;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_5_get_unbound_function );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_unbound );
    Py_DECREF( par_unbound );
    par_unbound = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_5_get_unbound_function );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_6_callable( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_obj = python_pars[ 0 ];
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_2432f271455b7775874bb4ab49abd71f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2432f271455b7775874bb4ab49abd71f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2432f271455b7775874bb4ab49abd71f, codeobj_2432f271455b7775874bb4ab49abd71f, module_scipy$_lib$six, sizeof(void *) );
    frame_2432f271455b7775874bb4ab49abd71f = cache_frame_2432f271455b7775874bb4ab49abd71f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2432f271455b7775874bb4ab49abd71f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2432f271455b7775874bb4ab49abd71f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_any_arg_1;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_type_arg_1;
            CHECK_OBJECT( par_obj );
            tmp_type_arg_1 = par_obj;
            tmp_source_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
            assert( !(tmp_source_name_1 == NULL) );
            tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___mro__ );
            Py_DECREF( tmp_source_name_1 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_1;
        }
        // Tried code:
        tmp_any_arg_1 = scipy$_lib$six$$$function_6_callable$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_any_arg_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_6_callable );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_6_callable );
        return NULL;
        outline_result_1:;
        tmp_return_value = BUILTIN_ANY( tmp_any_arg_1 );
        Py_DECREF( tmp_any_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2432f271455b7775874bb4ab49abd71f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2432f271455b7775874bb4ab49abd71f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2432f271455b7775874bb4ab49abd71f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2432f271455b7775874bb4ab49abd71f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2432f271455b7775874bb4ab49abd71f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2432f271455b7775874bb4ab49abd71f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2432f271455b7775874bb4ab49abd71f,
        type_description_1,
        par_obj
    );


    // Release cached frame.
    if ( frame_2432f271455b7775874bb4ab49abd71f == cache_frame_2432f271455b7775874bb4ab49abd71f )
    {
        Py_DECREF( frame_2432f271455b7775874bb4ab49abd71f );
    }
    cache_frame_2432f271455b7775874bb4ab49abd71f = NULL;

    assertFrameObject( frame_2432f271455b7775874bb4ab49abd71f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_6_callable );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_6_callable );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct scipy$_lib$six$$$function_6_callable$$$genexpr_1_genexpr_locals {
    PyObject *var_klass;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *scipy$_lib$six$$$function_6_callable$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct scipy$_lib$six$$$function_6_callable$$$genexpr_1_genexpr_locals *generator_heap = (struct scipy$_lib$six$$$function_6_callable$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_klass = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_23001572356707199f6edf7bdf3e8abf, module_scipy$_lib$six, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 131;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_klass;
            generator_heap->var_klass = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_klass );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_compexpr_left_1 = const_str_plain___call__;
        CHECK_OBJECT( generator_heap->var_klass );
        tmp_source_name_1 = generator_heap->var_klass;
        tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE_DICT_SLOT( tmp_source_name_1 );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 131;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 131;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = ( generator_heap->tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_expression_name_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 131;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 131;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_klass
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_klass );
    generator_heap->var_klass = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_klass );
    generator_heap->var_klass = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *scipy$_lib$six$$$function_6_callable$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        scipy$_lib$six$$$function_6_callable$$$genexpr_1_genexpr_context,
        module_scipy$_lib$six,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_3f74110888c25263bf2dd7e48a46f88c,
#endif
        codeobj_23001572356707199f6edf7bdf3e8abf,
        1,
        sizeof(struct scipy$_lib$six$$$function_6_callable$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_scipy$_lib$six$$$function_7_get_unbound_function( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_unbound = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_1aaf6b6023698448d5f7ac2e0bf5dd0a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1aaf6b6023698448d5f7ac2e0bf5dd0a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1aaf6b6023698448d5f7ac2e0bf5dd0a, codeobj_1aaf6b6023698448d5f7ac2e0bf5dd0a, module_scipy$_lib$six, sizeof(void *) );
    frame_1aaf6b6023698448d5f7ac2e0bf5dd0a = cache_frame_1aaf6b6023698448d5f7ac2e0bf5dd0a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1aaf6b6023698448d5f7ac2e0bf5dd0a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1aaf6b6023698448d5f7ac2e0bf5dd0a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_unbound );
        tmp_source_name_1 = par_unbound;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_im_func );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1aaf6b6023698448d5f7ac2e0bf5dd0a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1aaf6b6023698448d5f7ac2e0bf5dd0a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1aaf6b6023698448d5f7ac2e0bf5dd0a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1aaf6b6023698448d5f7ac2e0bf5dd0a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1aaf6b6023698448d5f7ac2e0bf5dd0a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1aaf6b6023698448d5f7ac2e0bf5dd0a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1aaf6b6023698448d5f7ac2e0bf5dd0a,
        type_description_1,
        par_unbound
    );


    // Release cached frame.
    if ( frame_1aaf6b6023698448d5f7ac2e0bf5dd0a == cache_frame_1aaf6b6023698448d5f7ac2e0bf5dd0a )
    {
        Py_DECREF( frame_1aaf6b6023698448d5f7ac2e0bf5dd0a );
    }
    cache_frame_1aaf6b6023698448d5f7ac2e0bf5dd0a = NULL;

    assertFrameObject( frame_1aaf6b6023698448d5f7ac2e0bf5dd0a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_7_get_unbound_function );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_unbound );
    Py_DECREF( par_unbound );
    par_unbound = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_unbound );
    Py_DECREF( par_unbound );
    par_unbound = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_7_get_unbound_function );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_8_next( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_838e87afeffb6c2a0767423807a7a50c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_838e87afeffb6c2a0767423807a7a50c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_838e87afeffb6c2a0767423807a7a50c, codeobj_838e87afeffb6c2a0767423807a7a50c, module_scipy$_lib$six, sizeof(void *) );
    frame_838e87afeffb6c2a0767423807a7a50c = cache_frame_838e87afeffb6c2a0767423807a7a50c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_838e87afeffb6c2a0767423807a7a50c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_838e87afeffb6c2a0767423807a7a50c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_type_arg_1 = par_self;
        tmp_called_instance_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        assert( !(tmp_called_instance_1 == NULL) );
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        frame_838e87afeffb6c2a0767423807a7a50c->m_frame.f_lineno = 139;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain___next__, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_838e87afeffb6c2a0767423807a7a50c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_838e87afeffb6c2a0767423807a7a50c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_838e87afeffb6c2a0767423807a7a50c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_838e87afeffb6c2a0767423807a7a50c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_838e87afeffb6c2a0767423807a7a50c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_838e87afeffb6c2a0767423807a7a50c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_838e87afeffb6c2a0767423807a7a50c,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_838e87afeffb6c2a0767423807a7a50c == cache_frame_838e87afeffb6c2a0767423807a7a50c )
    {
        Py_DECREF( frame_838e87afeffb6c2a0767423807a7a50c );
    }
    cache_frame_838e87afeffb6c2a0767423807a7a50c = NULL;

    assertFrameObject( frame_838e87afeffb6c2a0767423807a7a50c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_8_next );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_8_next );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_9_iterkeys( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_d = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b6030ec6a902e6f0abef7279653d5a06;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b6030ec6a902e6f0abef7279653d5a06 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b6030ec6a902e6f0abef7279653d5a06, codeobj_b6030ec6a902e6f0abef7279653d5a06, module_scipy$_lib$six, sizeof(void *) );
    frame_b6030ec6a902e6f0abef7279653d5a06 = cache_frame_b6030ec6a902e6f0abef7279653d5a06;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b6030ec6a902e6f0abef7279653d5a06 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b6030ec6a902e6f0abef7279653d5a06 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_d );
        tmp_getattr_target_1 = par_d;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__iterkeys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__iterkeys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_iterkeys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 154;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_getattr_attr_1 = tmp_mvar_value_1;
        tmp_called_name_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_b6030ec6a902e6f0abef7279653d5a06->m_frame.f_lineno = 154;
        tmp_iter_arg_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b6030ec6a902e6f0abef7279653d5a06 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b6030ec6a902e6f0abef7279653d5a06 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b6030ec6a902e6f0abef7279653d5a06 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b6030ec6a902e6f0abef7279653d5a06, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b6030ec6a902e6f0abef7279653d5a06->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b6030ec6a902e6f0abef7279653d5a06, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b6030ec6a902e6f0abef7279653d5a06,
        type_description_1,
        par_d
    );


    // Release cached frame.
    if ( frame_b6030ec6a902e6f0abef7279653d5a06 == cache_frame_b6030ec6a902e6f0abef7279653d5a06 )
    {
        Py_DECREF( frame_b6030ec6a902e6f0abef7279653d5a06 );
    }
    cache_frame_b6030ec6a902e6f0abef7279653d5a06 = NULL;

    assertFrameObject( frame_b6030ec6a902e6f0abef7279653d5a06 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_9_iterkeys );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_9_iterkeys );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_10_itervalues( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_d = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9fe3ad08dcb715ff85eb1ad47f5900b9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9fe3ad08dcb715ff85eb1ad47f5900b9 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9fe3ad08dcb715ff85eb1ad47f5900b9, codeobj_9fe3ad08dcb715ff85eb1ad47f5900b9, module_scipy$_lib$six, sizeof(void *) );
    frame_9fe3ad08dcb715ff85eb1ad47f5900b9 = cache_frame_9fe3ad08dcb715ff85eb1ad47f5900b9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9fe3ad08dcb715ff85eb1ad47f5900b9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9fe3ad08dcb715ff85eb1ad47f5900b9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_d );
        tmp_getattr_target_1 = par_d;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__itervalues );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__itervalues );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_itervalues" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_getattr_attr_1 = tmp_mvar_value_1;
        tmp_called_name_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_9fe3ad08dcb715ff85eb1ad47f5900b9->m_frame.f_lineno = 159;
        tmp_iter_arg_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9fe3ad08dcb715ff85eb1ad47f5900b9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9fe3ad08dcb715ff85eb1ad47f5900b9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9fe3ad08dcb715ff85eb1ad47f5900b9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9fe3ad08dcb715ff85eb1ad47f5900b9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9fe3ad08dcb715ff85eb1ad47f5900b9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9fe3ad08dcb715ff85eb1ad47f5900b9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9fe3ad08dcb715ff85eb1ad47f5900b9,
        type_description_1,
        par_d
    );


    // Release cached frame.
    if ( frame_9fe3ad08dcb715ff85eb1ad47f5900b9 == cache_frame_9fe3ad08dcb715ff85eb1ad47f5900b9 )
    {
        Py_DECREF( frame_9fe3ad08dcb715ff85eb1ad47f5900b9 );
    }
    cache_frame_9fe3ad08dcb715ff85eb1ad47f5900b9 = NULL;

    assertFrameObject( frame_9fe3ad08dcb715ff85eb1ad47f5900b9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_10_itervalues );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_10_itervalues );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_11_iteritems( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_d = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_98ea076e0d11b1f290db6452e882771b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_98ea076e0d11b1f290db6452e882771b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_98ea076e0d11b1f290db6452e882771b, codeobj_98ea076e0d11b1f290db6452e882771b, module_scipy$_lib$six, sizeof(void *) );
    frame_98ea076e0d11b1f290db6452e882771b = cache_frame_98ea076e0d11b1f290db6452e882771b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_98ea076e0d11b1f290db6452e882771b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_98ea076e0d11b1f290db6452e882771b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_d );
        tmp_getattr_target_1 = par_d;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__iteritems );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__iteritems );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_iteritems" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_getattr_attr_1 = tmp_mvar_value_1;
        tmp_called_name_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_98ea076e0d11b1f290db6452e882771b->m_frame.f_lineno = 164;
        tmp_iter_arg_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_98ea076e0d11b1f290db6452e882771b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_98ea076e0d11b1f290db6452e882771b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_98ea076e0d11b1f290db6452e882771b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_98ea076e0d11b1f290db6452e882771b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_98ea076e0d11b1f290db6452e882771b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_98ea076e0d11b1f290db6452e882771b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_98ea076e0d11b1f290db6452e882771b,
        type_description_1,
        par_d
    );


    // Release cached frame.
    if ( frame_98ea076e0d11b1f290db6452e882771b == cache_frame_98ea076e0d11b1f290db6452e882771b )
    {
        Py_DECREF( frame_98ea076e0d11b1f290db6452e882771b );
    }
    cache_frame_98ea076e0d11b1f290db6452e882771b = NULL;

    assertFrameObject( frame_98ea076e0d11b1f290db6452e882771b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_11_iteritems );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_11_iteritems );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_12_b( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_16fed4ff5646227d79fdff20460eb780;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_16fed4ff5646227d79fdff20460eb780 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_16fed4ff5646227d79fdff20460eb780, codeobj_16fed4ff5646227d79fdff20460eb780, module_scipy$_lib$six, sizeof(void *) );
    frame_16fed4ff5646227d79fdff20460eb780 = cache_frame_16fed4ff5646227d79fdff20460eb780;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_16fed4ff5646227d79fdff20460eb780 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_16fed4ff5646227d79fdff20460eb780 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_s );
        tmp_called_instance_1 = par_s;
        frame_16fed4ff5646227d79fdff20460eb780->m_frame.f_lineno = 169;
        tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_62b7e67e29e99420603c12d205222fa5_tuple, 0 ) );

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_16fed4ff5646227d79fdff20460eb780 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_16fed4ff5646227d79fdff20460eb780 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_16fed4ff5646227d79fdff20460eb780 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_16fed4ff5646227d79fdff20460eb780, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_16fed4ff5646227d79fdff20460eb780->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_16fed4ff5646227d79fdff20460eb780, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_16fed4ff5646227d79fdff20460eb780,
        type_description_1,
        par_s
    );


    // Release cached frame.
    if ( frame_16fed4ff5646227d79fdff20460eb780 == cache_frame_16fed4ff5646227d79fdff20460eb780 )
    {
        Py_DECREF( frame_16fed4ff5646227d79fdff20460eb780 );
    }
    cache_frame_16fed4ff5646227d79fdff20460eb780 = NULL;

    assertFrameObject( frame_16fed4ff5646227d79fdff20460eb780 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_12_b );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_12_b );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_13_u( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_s );
    tmp_return_value = par_s;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_13_u );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_13_u );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_14_int2byte( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_i = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a81fa7d4eca47427b7794e8d692d4801;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a81fa7d4eca47427b7794e8d692d4801 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a81fa7d4eca47427b7794e8d692d4801, codeobj_a81fa7d4eca47427b7794e8d692d4801, module_scipy$_lib$six, sizeof(void *) );
    frame_a81fa7d4eca47427b7794e8d692d4801 = cache_frame_a81fa7d4eca47427b7794e8d692d4801;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a81fa7d4eca47427b7794e8d692d4801 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a81fa7d4eca47427b7794e8d692d4801 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_bytes_arg_1;
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( par_i );
        tmp_tuple_element_1 = par_i;
        tmp_bytes_arg_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_bytes_arg_1, 0, tmp_tuple_element_1 );
        tmp_return_value = BUILTIN_BYTES1( tmp_bytes_arg_1 );
        Py_DECREF( tmp_bytes_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a81fa7d4eca47427b7794e8d692d4801 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a81fa7d4eca47427b7794e8d692d4801 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a81fa7d4eca47427b7794e8d692d4801 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a81fa7d4eca47427b7794e8d692d4801, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a81fa7d4eca47427b7794e8d692d4801->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a81fa7d4eca47427b7794e8d692d4801, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a81fa7d4eca47427b7794e8d692d4801,
        type_description_1,
        par_i
    );


    // Release cached frame.
    if ( frame_a81fa7d4eca47427b7794e8d692d4801 == cache_frame_a81fa7d4eca47427b7794e8d692d4801 )
    {
        Py_DECREF( frame_a81fa7d4eca47427b7794e8d692d4801 );
    }
    cache_frame_a81fa7d4eca47427b7794e8d692d4801 = NULL;

    assertFrameObject( frame_a81fa7d4eca47427b7794e8d692d4801 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_14_int2byte );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_i );
    Py_DECREF( par_i );
    par_i = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_i );
    Py_DECREF( par_i );
    par_i = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_14_int2byte );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_15_b( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_s );
    tmp_return_value = par_s;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_15_b );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_15_b );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_16_u( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2342e73875e6dea6fc2fcf23b3cf9222;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2342e73875e6dea6fc2fcf23b3cf9222 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2342e73875e6dea6fc2fcf23b3cf9222, codeobj_2342e73875e6dea6fc2fcf23b3cf9222, module_scipy$_lib$six, sizeof(void *) );
    frame_2342e73875e6dea6fc2fcf23b3cf9222 = cache_frame_2342e73875e6dea6fc2fcf23b3cf9222;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2342e73875e6dea6fc2fcf23b3cf9222 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2342e73875e6dea6fc2fcf23b3cf9222 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_unicode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 188;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_s );
        tmp_args_element_name_1 = par_s;
        tmp_args_element_name_2 = const_str_plain_unicode_escape;
        frame_2342e73875e6dea6fc2fcf23b3cf9222->m_frame.f_lineno = 188;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2342e73875e6dea6fc2fcf23b3cf9222 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2342e73875e6dea6fc2fcf23b3cf9222 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2342e73875e6dea6fc2fcf23b3cf9222 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2342e73875e6dea6fc2fcf23b3cf9222, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2342e73875e6dea6fc2fcf23b3cf9222->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2342e73875e6dea6fc2fcf23b3cf9222, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2342e73875e6dea6fc2fcf23b3cf9222,
        type_description_1,
        par_s
    );


    // Release cached frame.
    if ( frame_2342e73875e6dea6fc2fcf23b3cf9222 == cache_frame_2342e73875e6dea6fc2fcf23b3cf9222 )
    {
        Py_DECREF( frame_2342e73875e6dea6fc2fcf23b3cf9222 );
    }
    cache_frame_2342e73875e6dea6fc2fcf23b3cf9222 = NULL;

    assertFrameObject( frame_2342e73875e6dea6fc2fcf23b3cf9222 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_16_u );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_16_u );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_17_reraise( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tp = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    PyObject *par_tb = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_494be2abb0305286f9cafe7199cd6a2d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_494be2abb0305286f9cafe7199cd6a2d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_494be2abb0305286f9cafe7199cd6a2d, codeobj_494be2abb0305286f9cafe7199cd6a2d, module_scipy$_lib$six, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_494be2abb0305286f9cafe7199cd6a2d = cache_frame_494be2abb0305286f9cafe7199cd6a2d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_494be2abb0305286f9cafe7199cd6a2d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_494be2abb0305286f9cafe7199cd6a2d ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_value );
        tmp_source_name_1 = par_value;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___traceback__ );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_tb );
        tmp_compexpr_right_1 = par_tb;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_value );
            tmp_called_instance_1 = par_value;
            CHECK_OBJECT( par_tb );
            tmp_args_element_name_1 = par_tb;
            frame_494be2abb0305286f9cafe7199cd6a2d->m_frame.f_lineno = 202;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_raise_type_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_with_traceback, call_args );
            }

            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 202;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 202;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_raise_type_2;
        CHECK_OBJECT( par_value );
        tmp_raise_type_2 = par_value;
        exception_type = tmp_raise_type_2;
        Py_INCREF( tmp_raise_type_2 );
        exception_lineno = 203;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_494be2abb0305286f9cafe7199cd6a2d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_494be2abb0305286f9cafe7199cd6a2d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_494be2abb0305286f9cafe7199cd6a2d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_494be2abb0305286f9cafe7199cd6a2d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_494be2abb0305286f9cafe7199cd6a2d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_494be2abb0305286f9cafe7199cd6a2d,
        type_description_1,
        par_tp,
        par_value,
        par_tb
    );


    // Release cached frame.
    if ( frame_494be2abb0305286f9cafe7199cd6a2d == cache_frame_494be2abb0305286f9cafe7199cd6a2d )
    {
        Py_DECREF( frame_494be2abb0305286f9cafe7199cd6a2d );
    }
    cache_frame_494be2abb0305286f9cafe7199cd6a2d = NULL;

    assertFrameObject( frame_494be2abb0305286f9cafe7199cd6a2d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_17_reraise );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_tp );
    Py_DECREF( par_tp );
    par_tp = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_tb );
    Py_DECREF( par_tb );
    par_tb = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_17_reraise );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_scipy$_lib$six$$$function_18_exec_( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_code = python_pars[ 0 ];
    PyObject *par_globs = python_pars[ 1 ];
    PyObject *par_locs = python_pars[ 2 ];
    PyObject *var_frame = NULL;
    PyObject *tmp_exec_call_1__globals = NULL;
    PyObject *tmp_exec_call_1__locals = NULL;
    struct Nuitka_FrameObject *frame_6eeeaa6194731f51f4fa045b9dd14ef4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *locals_scipy$_lib$six$$$function_18_exec_ = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_6eeeaa6194731f51f4fa045b9dd14ef4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6eeeaa6194731f51f4fa045b9dd14ef4, codeobj_6eeeaa6194731f51f4fa045b9dd14ef4, module_scipy$_lib$six, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6eeeaa6194731f51f4fa045b9dd14ef4 = cache_frame_6eeeaa6194731f51f4fa045b9dd14ef4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6eeeaa6194731f51f4fa045b9dd14ef4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6eeeaa6194731f51f4fa045b9dd14ef4 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_globs );
        tmp_compexpr_left_1 = par_globs;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 212;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            frame_6eeeaa6194731f51f4fa045b9dd14ef4->m_frame.f_lineno = 212;
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain__getframe, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_frame == NULL );
            var_frame = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( var_frame );
            tmp_source_name_1 = var_frame;
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_f_globals );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 213;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_globs;
                assert( old != NULL );
                par_globs = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_locs );
            tmp_compexpr_left_2 = par_locs;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_source_name_2;
                CHECK_OBJECT( var_frame );
                tmp_source_name_2 = var_frame;
                tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_f_locals );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 215;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_locs;
                    assert( old != NULL );
                    par_locs = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        CHECK_OBJECT( var_frame );
        Py_DECREF( var_frame );
        var_frame = NULL;

        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( par_locs );
            tmp_compexpr_left_3 = par_locs;
            tmp_compexpr_right_3 = Py_None;
            tmp_condition_result_3 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_4;
                CHECK_OBJECT( par_globs );
                tmp_assign_source_4 = par_globs;
                {
                    PyObject *old = par_locs;
                    assert( old != NULL );
                    par_locs = tmp_assign_source_4;
                    Py_INCREF( par_locs );
                    Py_DECREF( old );
                }

            }
            branch_no_3:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_outline_return_value_1;
        {
            PyObject *tmp_assign_source_5;
            if (locals_scipy$_lib$six$$$function_18_exec_ == NULL) locals_scipy$_lib$six$$$function_18_exec_ = PyDict_New();
            tmp_assign_source_5 = locals_scipy$_lib$six$$$function_18_exec_;
            Py_INCREF( tmp_assign_source_5 );
            if ( par_code != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( par_code );
                value = par_code;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_5, (Nuitka_StringObject *)const_str_plain_code, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_assign_source_5, const_str_plain_code );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( par_globs != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( par_globs );
                value = par_globs;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_5, (Nuitka_StringObject *)const_str_plain_globs, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_assign_source_5, const_str_plain_globs );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( par_locs != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( par_locs );
                value = par_locs;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_5, (Nuitka_StringObject *)const_str_plain_locs, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_assign_source_5, const_str_plain_locs );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( var_frame != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( var_frame );
                value = var_frame;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_5, (Nuitka_StringObject *)const_str_plain_frame, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_assign_source_5, const_str_plain_frame );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            assert( tmp_exec_call_1__locals == NULL );
            tmp_exec_call_1__locals = tmp_assign_source_5;
        }
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = (PyObject *)moduledict_scipy$_lib$six;
            assert( tmp_exec_call_1__globals == NULL );
            Py_INCREF( tmp_assign_source_6 );
            tmp_exec_call_1__globals = tmp_assign_source_6;
        }
        // Tried code:
        {
            PyObject *tmp_eval_source_1;
            PyObject *tmp_eval_globals_1;
            PyObject *tmp_eval_locals_1;
            PyObject *tmp_eval_compiled_1;
            tmp_eval_source_1 = const_str_digest_bf654a8a4d8c3e4a63d0fc540f5f4f9e;
            CHECK_OBJECT( tmp_exec_call_1__globals );
            tmp_eval_globals_1 = tmp_exec_call_1__globals;
            CHECK_OBJECT( tmp_exec_call_1__locals );
            tmp_eval_locals_1 = tmp_exec_call_1__locals;
            tmp_eval_compiled_1 = COMPILE_CODE( tmp_eval_source_1, const_str_angle_string, const_str_plain_exec, NULL, NULL, NULL );
            if ( tmp_eval_compiled_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 219;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_outline_return_value_1 = EVAL_CODE( tmp_eval_compiled_1, tmp_eval_globals_1, tmp_eval_locals_1 );
            Py_DECREF( tmp_eval_compiled_1 );
            if ( tmp_outline_return_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 219;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            goto try_return_handler_2;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_18_exec_ );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__globals );
        Py_DECREF( tmp_exec_call_1__globals );
        tmp_exec_call_1__globals = NULL;

        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__locals );
        Py_DECREF( tmp_exec_call_1__locals );
        tmp_exec_call_1__locals = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__globals );
        Py_DECREF( tmp_exec_call_1__globals );
        tmp_exec_call_1__globals = NULL;

        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__locals );
        Py_DECREF( tmp_exec_call_1__locals );
        tmp_exec_call_1__locals = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_18_exec_ );
        return NULL;
        outline_result_1:;
        Py_DECREF( tmp_outline_return_value_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6eeeaa6194731f51f4fa045b9dd14ef4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6eeeaa6194731f51f4fa045b9dd14ef4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6eeeaa6194731f51f4fa045b9dd14ef4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6eeeaa6194731f51f4fa045b9dd14ef4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6eeeaa6194731f51f4fa045b9dd14ef4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6eeeaa6194731f51f4fa045b9dd14ef4,
        type_description_1,
        par_code,
        par_globs,
        par_locs,
        var_frame
    );


    // Release cached frame.
    if ( frame_6eeeaa6194731f51f4fa045b9dd14ef4 == cache_frame_6eeeaa6194731f51f4fa045b9dd14ef4 )
    {
        Py_DECREF( frame_6eeeaa6194731f51f4fa045b9dd14ef4 );
    }
    cache_frame_6eeeaa6194731f51f4fa045b9dd14ef4 = NULL;

    assertFrameObject( frame_6eeeaa6194731f51f4fa045b9dd14ef4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_18_exec_ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_code );
    par_code = NULL;

    Py_XDECREF( par_globs );
    par_globs = NULL;

    Py_XDECREF( par_locs );
    par_locs = NULL;

    Py_XDECREF( var_frame );
    var_frame = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_code );
    par_code = NULL;

    Py_XDECREF( par_globs );
    par_globs = NULL;

    Py_XDECREF( par_locs );
    par_locs = NULL;

    Py_XDECREF( var_frame );
    var_frame = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_18_exec_ );
    return NULL;

function_exception_exit:
    Py_XDECREF( locals_scipy$_lib$six$$$function_18_exec_ );
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.
    Py_XDECREF( locals_scipy$_lib$six$$$function_18_exec_ );


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_19_print_( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    struct Nuitka_CellObject *var_fp = PyCell_EMPTY();
    PyObject *var_write = NULL;
    PyObject *var_want_unicode = NULL;
    PyObject *var_sep = NULL;
    PyObject *var_end = NULL;
    PyObject *var_arg = NULL;
    PyObject *var_newline = NULL;
    PyObject *var_space = NULL;
    PyObject *var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_3eaa836dc69e23c1c2be309dafba3082;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_3eaa836dc69e23c1c2be309dafba3082 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3eaa836dc69e23c1c2be309dafba3082, codeobj_3eaa836dc69e23c1c2be309dafba3082, module_scipy$_lib$six, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3eaa836dc69e23c1c2be309dafba3082 = cache_frame_3eaa836dc69e23c1c2be309dafba3082;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3eaa836dc69e23c1c2be309dafba3082 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3eaa836dc69e23c1c2be309dafba3082 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_kwargs );
        tmp_source_name_1 = par_kwargs;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_pop );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = const_str_plain_file;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 227;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_stdout );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 227;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 227;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_fp ) == NULL );
        PyCell_SET( var_fp, tmp_assign_source_1 );

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( PyCell_GET( var_fp ) );
        tmp_compexpr_left_1 = PyCell_GET( var_fp );
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_scipy$_lib$six$$$function_19_print_$$$function_1_write(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = var_fp;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );


        assert( var_write == NULL );
        var_write = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_False;
        assert( var_want_unicode == NULL );
        Py_INCREF( tmp_assign_source_3 );
        var_want_unicode = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_kwargs );
        tmp_called_instance_1 = par_kwargs;
        frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 236;
        tmp_assign_source_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_str_plain_sep_none_tuple, 0 ) );

        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 236;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_sep == NULL );
        var_sep = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_sep );
        tmp_compexpr_left_2 = var_sep;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_mvar_value_2;
            CHECK_OBJECT( var_sep );
            tmp_isinstance_inst_1 = var_sep;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 238;
                type_description_1 = "oocoooooooo";
                goto frame_exception_exit_1;
            }

            tmp_isinstance_cls_1 = tmp_mvar_value_2;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 238;
                type_description_1 = "oocoooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_5;
                tmp_assign_source_5 = Py_True;
                {
                    PyObject *old = var_want_unicode;
                    assert( old != NULL );
                    var_want_unicode = tmp_assign_source_5;
                    Py_INCREF( var_want_unicode );
                    Py_DECREF( old );
                }

            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_operand_name_1;
                PyObject *tmp_isinstance_inst_2;
                PyObject *tmp_isinstance_cls_2;
                CHECK_OBJECT( var_sep );
                tmp_isinstance_inst_2 = var_sep;
                tmp_isinstance_cls_2 = (PyObject *)&PyUnicode_Type;
                tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 240;
                    type_description_1 = "oocoooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 240;
                    type_description_1 = "oocoooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_raise_type_1;
                    PyObject *tmp_make_exception_arg_1;
                    tmp_make_exception_arg_1 = const_str_digest_a13035c59d0f0e8c04a4feacc0004cd7;
                    frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 241;
                    {
                        PyObject *call_args[] = { tmp_make_exception_arg_1 };
                        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
                    }

                    assert( !(tmp_raise_type_1 == NULL) );
                    exception_type = tmp_raise_type_1;
                    exception_lineno = 241;
                    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oocoooooooo";
                    goto frame_exception_exit_1;
                }
                branch_no_4:;
            }
            branch_end_3:;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_kwargs );
        tmp_called_instance_2 = par_kwargs;
        frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 242;
        tmp_assign_source_6 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_str_plain_end_none_tuple, 0 ) );

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 242;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_end == NULL );
        var_end = tmp_assign_source_6;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        CHECK_OBJECT( var_end );
        tmp_compexpr_left_3 = var_end;
        tmp_compexpr_right_3 = Py_None;
        tmp_condition_result_5 = ( tmp_compexpr_left_3 != tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_isinstance_inst_3;
            PyObject *tmp_isinstance_cls_3;
            PyObject *tmp_mvar_value_3;
            CHECK_OBJECT( var_end );
            tmp_isinstance_inst_3 = var_end;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 244;
                type_description_1 = "oocoooooooo";
                goto frame_exception_exit_1;
            }

            tmp_isinstance_cls_3 = tmp_mvar_value_3;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_3, tmp_isinstance_cls_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 244;
                type_description_1 = "oocoooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_assign_source_7;
                tmp_assign_source_7 = Py_True;
                {
                    PyObject *old = var_want_unicode;
                    var_want_unicode = tmp_assign_source_7;
                    Py_INCREF( var_want_unicode );
                    Py_XDECREF( old );
                }

            }
            goto branch_end_6;
            branch_no_6:;
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_operand_name_2;
                PyObject *tmp_isinstance_inst_4;
                PyObject *tmp_isinstance_cls_4;
                CHECK_OBJECT( var_end );
                tmp_isinstance_inst_4 = var_end;
                tmp_isinstance_cls_4 = (PyObject *)&PyUnicode_Type;
                tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_4, tmp_isinstance_cls_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 246;
                    type_description_1 = "oocoooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 246;
                    type_description_1 = "oocoooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_7 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    PyObject *tmp_raise_type_2;
                    PyObject *tmp_make_exception_arg_2;
                    tmp_make_exception_arg_2 = const_str_digest_7e4f7e7d87a44d0e81f11f08170c9bf6;
                    frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 247;
                    {
                        PyObject *call_args[] = { tmp_make_exception_arg_2 };
                        tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
                    }

                    assert( !(tmp_raise_type_2 == NULL) );
                    exception_type = tmp_raise_type_2;
                    exception_lineno = 247;
                    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oocoooooooo";
                    goto frame_exception_exit_1;
                }
                branch_no_7:;
            }
            branch_end_6:;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_kwargs );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_kwargs );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            PyObject *tmp_raise_type_3;
            PyObject *tmp_make_exception_arg_3;
            tmp_make_exception_arg_3 = const_str_digest_6fdf710e7d63e48e68391e1f587ae0a5;
            frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 249;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_3 };
                tmp_raise_type_3 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
            }

            assert( !(tmp_raise_type_3 == NULL) );
            exception_type = tmp_raise_type_3;
            exception_lineno = 249;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_8:;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_operand_name_3;
        if ( var_want_unicode == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "want_unicode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 250;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_3 = var_want_unicode;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 250;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_9 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_args );
            tmp_iter_arg_1 = par_args;
            tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 251;
                type_description_1 = "oocoooooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_8;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_9 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oocoooooooo";
                    exception_lineno = 251;
                    goto try_except_handler_2;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_9;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_10;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_10 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_arg;
                var_arg = tmp_assign_source_10;
                Py_INCREF( var_arg );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_10;
            PyObject *tmp_isinstance_inst_5;
            PyObject *tmp_isinstance_cls_5;
            PyObject *tmp_mvar_value_4;
            CHECK_OBJECT( var_arg );
            tmp_isinstance_inst_5 = var_arg;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 252;
                type_description_1 = "oocoooooooo";
                goto try_except_handler_2;
            }

            tmp_isinstance_cls_5 = tmp_mvar_value_4;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_5, tmp_isinstance_cls_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 252;
                type_description_1 = "oocoooooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_10;
            }
            else
            {
                goto branch_no_10;
            }
            branch_yes_10:;
            {
                PyObject *tmp_assign_source_11;
                tmp_assign_source_11 = Py_True;
                {
                    PyObject *old = var_want_unicode;
                    var_want_unicode = tmp_assign_source_11;
                    Py_INCREF( var_want_unicode );
                    Py_XDECREF( old );
                }

            }
            goto loop_end_1;
            branch_no_10:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;
            type_description_1 = "oocoooooooo";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        int tmp_truth_name_2;
        if ( var_want_unicode == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "want_unicode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 255;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_2 = CHECK_IF_TRUE( var_want_unicode );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 255;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_11 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_5;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 256;
                type_description_1 = "oocoooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_5;
            frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 256;
            tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_newline_tuple, 0 ) );

            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 256;
                type_description_1 = "oocoooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_newline == NULL );
            var_newline = tmp_assign_source_12;
        }
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_6;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 257;
                type_description_1 = "oocoooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_3 = tmp_mvar_value_6;
            frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 257;
            tmp_assign_source_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_str_space_tuple, 0 ) );

            if ( tmp_assign_source_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 257;
                type_description_1 = "oocoooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_space == NULL );
            var_space = tmp_assign_source_13;
        }
        goto branch_end_11;
        branch_no_11:;
        {
            PyObject *tmp_assign_source_14;
            tmp_assign_source_14 = const_str_newline;
            assert( var_newline == NULL );
            Py_INCREF( tmp_assign_source_14 );
            var_newline = tmp_assign_source_14;
        }
        {
            PyObject *tmp_assign_source_15;
            tmp_assign_source_15 = const_str_space;
            assert( var_space == NULL );
            Py_INCREF( tmp_assign_source_15 );
            var_space = tmp_assign_source_15;
        }
        branch_end_11:;
    }
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        CHECK_OBJECT( var_sep );
        tmp_compexpr_left_4 = var_sep;
        tmp_compexpr_right_4 = Py_None;
        tmp_condition_result_12 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_12;
        }
        else
        {
            goto branch_no_12;
        }
        branch_yes_12:;
        {
            PyObject *tmp_assign_source_16;
            CHECK_OBJECT( var_space );
            tmp_assign_source_16 = var_space;
            {
                PyObject *old = var_sep;
                assert( old != NULL );
                var_sep = tmp_assign_source_16;
                Py_INCREF( var_sep );
                Py_DECREF( old );
            }

        }
        branch_no_12:;
    }
    {
        nuitka_bool tmp_condition_result_13;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        CHECK_OBJECT( var_end );
        tmp_compexpr_left_5 = var_end;
        tmp_compexpr_right_5 = Py_None;
        tmp_condition_result_13 = ( tmp_compexpr_left_5 == tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        {
            PyObject *tmp_assign_source_17;
            CHECK_OBJECT( var_newline );
            tmp_assign_source_17 = var_newline;
            {
                PyObject *old = var_end;
                assert( old != NULL );
                var_end = tmp_assign_source_17;
                Py_INCREF( var_end );
                Py_DECREF( old );
            }

        }
        branch_no_13:;
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_name_4;
        PyObject *tmp_args_element_name_3;
        tmp_called_name_4 = (PyObject *)&PyEnum_Type;
        CHECK_OBJECT( par_args );
        tmp_args_element_name_3 = par_args;
        frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 265;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_iter_arg_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_18 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_2__for_iterator == NULL );
        tmp_for_loop_2__for_iterator = tmp_assign_source_18;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_19;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_19 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_19 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oocoooooooo";
                exception_lineno = 265;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_19;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_iter_arg_3;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_iter_arg_3 = tmp_for_loop_2__iter_value;
        tmp_assign_source_20 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;
            type_description_1 = "oocoooooooo";
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_20;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_21 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_21 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oocoooooooo";
            exception_lineno = 265;
            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_21;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_22 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_22 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oocoooooooo";
            exception_lineno = 265;
            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_22;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oocoooooooo";
                    exception_lineno = 265;
                    goto try_except_handler_5;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oocoooooooo";
            exception_lineno = 265;
            goto try_except_handler_5;
        }
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_4;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_3;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_23;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_23 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_23;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_24;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_24 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_arg;
            var_arg = tmp_assign_source_24;
            Py_INCREF( var_arg );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_14;
        int tmp_truth_name_3;
        CHECK_OBJECT( var_i );
        tmp_truth_name_3 = CHECK_IF_TRUE( var_i );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;
            type_description_1 = "oocoooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_14 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( var_write );
            tmp_called_name_5 = var_write;
            CHECK_OBJECT( var_sep );
            tmp_args_element_name_4 = var_sep;
            frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 267;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 267;
                type_description_1 = "oocoooooooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_14:;
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( var_write );
        tmp_called_name_6 = var_write;
        CHECK_OBJECT( var_arg );
        tmp_args_element_name_5 = var_arg;
        frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 268;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 268;
            type_description_1 = "oocoooooooo";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 265;
        type_description_1 = "oocoooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        PyObject *tmp_called_name_7;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_6;
        CHECK_OBJECT( var_write );
        tmp_called_name_7 = var_write;
        CHECK_OBJECT( var_end );
        tmp_args_element_name_6 = var_end;
        frame_3eaa836dc69e23c1c2be309dafba3082->m_frame.f_lineno = 269;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 269;
            type_description_1 = "oocoooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3eaa836dc69e23c1c2be309dafba3082 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3eaa836dc69e23c1c2be309dafba3082 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3eaa836dc69e23c1c2be309dafba3082 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3eaa836dc69e23c1c2be309dafba3082, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3eaa836dc69e23c1c2be309dafba3082->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3eaa836dc69e23c1c2be309dafba3082, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3eaa836dc69e23c1c2be309dafba3082,
        type_description_1,
        par_args,
        par_kwargs,
        var_fp,
        var_write,
        var_want_unicode,
        var_sep,
        var_end,
        var_arg,
        var_newline,
        var_space,
        var_i
    );


    // Release cached frame.
    if ( frame_3eaa836dc69e23c1c2be309dafba3082 == cache_frame_3eaa836dc69e23c1c2be309dafba3082 )
    {
        Py_DECREF( frame_3eaa836dc69e23c1c2be309dafba3082 );
    }
    cache_frame_3eaa836dc69e23c1c2be309dafba3082 = NULL;

    assertFrameObject( frame_3eaa836dc69e23c1c2be309dafba3082 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_19_print_ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_fp );
    Py_DECREF( var_fp );
    var_fp = NULL;

    Py_XDECREF( var_write );
    var_write = NULL;

    Py_XDECREF( var_want_unicode );
    var_want_unicode = NULL;

    Py_XDECREF( var_sep );
    var_sep = NULL;

    Py_XDECREF( var_end );
    var_end = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    Py_XDECREF( var_newline );
    var_newline = NULL;

    Py_XDECREF( var_space );
    var_space = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_fp );
    Py_DECREF( var_fp );
    var_fp = NULL;

    Py_XDECREF( var_write );
    var_write = NULL;

    Py_XDECREF( var_want_unicode );
    var_want_unicode = NULL;

    Py_XDECREF( var_sep );
    var_sep = NULL;

    Py_XDECREF( var_end );
    var_end = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    Py_XDECREF( var_newline );
    var_newline = NULL;

    Py_XDECREF( var_space );
    var_space = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_19_print_ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_19_print_$$$function_1_write( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_data = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3e3f8f76419930c63662a1a22a2eb59d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_3e3f8f76419930c63662a1a22a2eb59d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3e3f8f76419930c63662a1a22a2eb59d, codeobj_3e3f8f76419930c63662a1a22a2eb59d, module_scipy$_lib$six, sizeof(void *)+sizeof(void *) );
    frame_3e3f8f76419930c63662a1a22a2eb59d = cache_frame_3e3f8f76419930c63662a1a22a2eb59d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3e3f8f76419930c63662a1a22a2eb59d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3e3f8f76419930c63662a1a22a2eb59d ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_data );
        tmp_isinstance_inst_1 = par_data;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_basestring );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_basestring );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "basestring" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 232;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_unicode_arg_1;
            CHECK_OBJECT( par_data );
            tmp_unicode_arg_1 = par_data;
            tmp_assign_source_1 = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 233;
                type_description_1 = "oc";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_data;
                assert( old != NULL );
                par_data = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fp" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 234;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_data );
        tmp_args_element_name_1 = par_data;
        frame_3e3f8f76419930c63662a1a22a2eb59d->m_frame.f_lineno = 234;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_write, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 234;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3e3f8f76419930c63662a1a22a2eb59d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3e3f8f76419930c63662a1a22a2eb59d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3e3f8f76419930c63662a1a22a2eb59d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3e3f8f76419930c63662a1a22a2eb59d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3e3f8f76419930c63662a1a22a2eb59d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3e3f8f76419930c63662a1a22a2eb59d,
        type_description_1,
        par_data,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_3e3f8f76419930c63662a1a22a2eb59d == cache_frame_3e3f8f76419930c63662a1a22a2eb59d )
    {
        Py_DECREF( frame_3e3f8f76419930c63662a1a22a2eb59d );
    }
    cache_frame_3e3f8f76419930c63662a1a22a2eb59d = NULL;

    assertFrameObject( frame_3e3f8f76419930c63662a1a22a2eb59d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_19_print_$$$function_1_write );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_data );
    par_data = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_19_print_$$$function_1_write );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_scipy$_lib$six$$$function_20_with_metaclass( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_meta = python_pars[ 0 ];
    PyObject *par_base = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_5a3e090dd71bbef1e8aeeeaa9a1475c0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5a3e090dd71bbef1e8aeeeaa9a1475c0, codeobj_5a3e090dd71bbef1e8aeeeaa9a1475c0, module_scipy$_lib$six, sizeof(void *)+sizeof(void *) );
    frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 = cache_frame_5a3e090dd71bbef1e8aeeeaa9a1475c0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_meta );
        tmp_called_name_1 = par_meta;
        tmp_args_element_name_1 = const_str_plain_NewBase;
        CHECK_OBJECT( par_base );
        tmp_tuple_element_1 = par_base;
        tmp_args_element_name_2 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_element_name_2, 0, tmp_tuple_element_1 );
        tmp_args_element_name_3 = PyDict_New();
        frame_5a3e090dd71bbef1e8aeeeaa9a1475c0->m_frame.f_lineno = 276;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 276;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5a3e090dd71bbef1e8aeeeaa9a1475c0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5a3e090dd71bbef1e8aeeeaa9a1475c0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5a3e090dd71bbef1e8aeeeaa9a1475c0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5a3e090dd71bbef1e8aeeeaa9a1475c0,
        type_description_1,
        par_meta,
        par_base
    );


    // Release cached frame.
    if ( frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 == cache_frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 )
    {
        Py_DECREF( frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 );
    }
    cache_frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 = NULL;

    assertFrameObject( frame_5a3e090dd71bbef1e8aeeeaa9a1475c0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_20_with_metaclass );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six$$$function_20_with_metaclass );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_10_itervalues(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_10_itervalues,
        const_str_plain_itervalues,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9fe3ad08dcb715ff85eb1ad47f5900b9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        const_str_digest_c18de21b4af59af9eda7ad848834774a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_11_iteritems(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_11_iteritems,
        const_str_plain_iteritems,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_98ea076e0d11b1f290db6452e882771b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        const_str_digest_a343d5bd65de7d15893f1a0d02d09f45,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_12_b(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_12_b,
        const_str_plain_b,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_16fed4ff5646227d79fdff20460eb780,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_13_u(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_13_u,
        const_str_plain_u,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_daa36a6a2433ddc633503be641b7168f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_14_int2byte(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_14_int2byte,
        const_str_plain_int2byte,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a81fa7d4eca47427b7794e8d692d4801,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_15_b(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_15_b,
        const_str_plain_b,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_80bb875a2a5fc6d676782ae7a7a3a996,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_16_u(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_16_u,
        const_str_plain_u,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2342e73875e6dea6fc2fcf23b3cf9222,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_17_reraise( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_17_reraise,
        const_str_plain_reraise,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_494be2abb0305286f9cafe7199cd6a2d,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_18_exec_( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_18_exec_,
        const_str_plain_exec_,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6eeeaa6194731f51f4fa045b9dd14ef4,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        const_str_digest_52da6974eceb6e05255f92901fc2e2f2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_19_print_(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_19_print_,
        const_str_plain_print_,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3eaa836dc69e23c1c2be309dafba3082,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        const_str_digest_bf6462317c060c308303e28ee40ba3d6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_19_print_$$$function_1_write(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_19_print_$$$function_1_write,
        const_str_plain_write,
#if PYTHON_VERSION >= 300
        const_str_digest_60d0662f4d422d418678c2ec7a6d653a,
#endif
        codeobj_3e3f8f76419930c63662a1a22a2eb59d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_1___len__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_1___len__,
        const_str_plain___len__,
#if PYTHON_VERSION >= 300
        const_str_digest_455f96f9ec5d27095599ab17da15470e,
#endif
        codeobj_2a2c1390bb73bf41f20d2de2acea5c69,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_20_with_metaclass( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_20_with_metaclass,
        const_str_plain_with_metaclass,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5a3e090dd71bbef1e8aeeeaa9a1475c0,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        const_str_digest_1d2e267f2ccdeb84fbf4cb4191414e9f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_2__add_doc(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_2__add_doc,
        const_str_plain__add_doc,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_05e2aeb98ecfd1508c8ffbd4aa1975ab,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        const_str_digest_cce73c34157ad7a21f7265310a153ec1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_3__import_module(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_3__import_module,
        const_str_plain__import_module,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_82f8e0fe7c152ff2621242cabf01b838,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        const_str_digest_1e91aab42d00f04e391d84545f09e2f5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_4_advance_iterator(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_4_advance_iterator,
        const_str_plain_advance_iterator,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ea10ee9205bf679b152ff579159389da,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_5_get_unbound_function(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_5_get_unbound_function,
        const_str_plain_get_unbound_function,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_67fcdf844f2101dc4ec15101b7953074,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_6_callable(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_6_callable,
        const_str_plain_callable,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2432f271455b7775874bb4ab49abd71f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_7_get_unbound_function(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_7_get_unbound_function,
        const_str_plain_get_unbound_function,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1aaf6b6023698448d5f7ac2e0bf5dd0a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_8_next(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_8_next,
        const_str_plain_next,
#if PYTHON_VERSION >= 300
        const_str_digest_31d25caf7489e6241458a1761a0b32ce,
#endif
        codeobj_838e87afeffb6c2a0767423807a7a50c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_scipy$_lib$six$$$function_9_iterkeys(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_scipy$_lib$six$$$function_9_iterkeys,
        const_str_plain_iterkeys,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b6030ec6a902e6f0abef7279653d5a06,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_scipy$_lib$six,
        const_str_digest_c1940ed36d6c651776a6e3a34c7131e3,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_scipy$_lib$six =
{
    PyModuleDef_HEAD_INIT,
    "scipy._lib.six",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(scipy$_lib$six)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(scipy$_lib$six)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_scipy$_lib$six );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("scipy._lib.six: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("scipy._lib.six: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("scipy._lib.six: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initscipy$_lib$six" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_scipy$_lib$six = Py_InitModule4(
        "scipy._lib.six",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_scipy$_lib$six = PyModule_Create( &mdef_scipy$_lib$six );
#endif

    moduledict_scipy$_lib$six = MODULE_DICT( module_scipy$_lib$six );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_scipy$_lib$six,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_scipy$_lib$six,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_scipy$_lib$six,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_scipy$_lib$six,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_scipy$_lib$six );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_c88389693d9d476899c1c6e170568469, module_scipy$_lib$six );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    nuitka_bool tmp_try_except_1__unhandled_indicator = NUITKA_BOOL_UNASSIGNED;
    struct Nuitka_FrameObject *frame_3ee8b0c17a2ecfea3609cfca70f074b6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_scipy$_lib$six_53 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *locals_scipy$_lib$six_136 = NULL;
    struct Nuitka_FrameObject *frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_97d8b3849ccb4841962c7ac18f0af059;
        UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_3ee8b0c17a2ecfea3609cfca70f074b6 = MAKE_MODULE_FRAME( codeobj_3ee8b0c17a2ecfea3609cfca70f074b6, module_scipy$_lib$six );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_3ee8b0c17a2ecfea3609cfca70f074b6 );
    assert( Py_REFCNT( frame_3ee8b0c17a2ecfea3609cfca70f074b6 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_operator;
        tmp_globals_name_1 = (PyObject *)moduledict_scipy$_lib$six;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 22;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_operator, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_sys;
        tmp_globals_name_2 = (PyObject *)moduledict_scipy$_lib$six;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 23;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        assert( !(tmp_assign_source_5 == NULL) );
        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_types;
        tmp_globals_name_3 = (PyObject *)moduledict_scipy$_lib$six;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 24;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_types, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = const_str_digest_8f714300b54df1f07c5ea6ab3e1ef1e0;
        UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___author__, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = const_str_digest_9c1efbd2c0b52e4bf9af9259bd597447;
        UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_version_info );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_3;
        tmp_assign_source_9 = RICH_COMPARE_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_PY3, tmp_assign_source_9 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_mvar_value_4;
        int tmp_truth_name_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_mvar_value_4 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_10;
            tmp_assign_source_10 = const_tuple_type_str_tuple;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_string_types, tmp_assign_source_10 );
        }
        {
            PyObject *tmp_assign_source_11;
            tmp_assign_source_11 = const_tuple_type_int_tuple;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_integer_types, tmp_assign_source_11 );
        }
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = const_tuple_type_type_tuple;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_class_types, tmp_assign_source_12 );
        }
        {
            PyObject *tmp_assign_source_13;
            tmp_assign_source_13 = (PyObject *)&PyUnicode_Type;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_text_type, tmp_assign_source_13 );
        }
        {
            PyObject *tmp_assign_source_14;
            tmp_assign_source_14 = (PyObject *)&PyBytes_Type;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_binary_type, tmp_assign_source_14 );
        }
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_5;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 40;

                goto frame_exception_exit_1;
            }

            tmp_source_name_2 = tmp_mvar_value_5;
            tmp_assign_source_15 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_maxsize );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_MAXSIZE, tmp_assign_source_15 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_mvar_value_6;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_basestring );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_basestring );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "basestring" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 42;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_1 = tmp_mvar_value_6;
            tmp_assign_source_16 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_assign_source_16, 0, tmp_tuple_element_1 );
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_string_types, tmp_assign_source_16 );
        }
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_mvar_value_7;
            tmp_tuple_element_2 = (PyObject *)&PyLong_Type;
            tmp_assign_source_17 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_assign_source_17, 0, tmp_tuple_element_2 );
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_long );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_long );
            }

            if ( tmp_mvar_value_7 == NULL )
            {
                Py_DECREF( tmp_assign_source_17 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "long" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 43;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_2 = tmp_mvar_value_7;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_assign_source_17, 1, tmp_tuple_element_2 );
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_integer_types, tmp_assign_source_17 );
        }
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_8;
            tmp_tuple_element_3 = (PyObject *)&PyType_Type;
            tmp_assign_source_18 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_assign_source_18, 0, tmp_tuple_element_3 );
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_types );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_types );
            }

            if ( tmp_mvar_value_8 == NULL )
            {
                Py_DECREF( tmp_assign_source_18 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "types" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 44;

                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = tmp_mvar_value_8;
            tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_ClassType );
            if ( tmp_tuple_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assign_source_18 );

                exception_lineno = 44;

                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_assign_source_18, 1, tmp_tuple_element_3 );
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_class_types, tmp_assign_source_18 );
        }
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_mvar_value_9;
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_9 == NULL ))
            {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_9 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 45;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_19 = tmp_mvar_value_9;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_text_type, tmp_assign_source_19 );
        }
        {
            PyObject *tmp_assign_source_20;
            tmp_assign_source_20 = (PyObject *)&PyUnicode_Type;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_binary_type, tmp_assign_source_20 );
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_call_result_1;
            int tmp_truth_name_2;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 48;

                goto frame_exception_exit_1;
            }

            tmp_source_name_4 = tmp_mvar_value_10;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_platform );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 48;

                goto frame_exception_exit_1;
            }
            frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 48;
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_java_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 48;

                goto frame_exception_exit_1;
            }
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_1 );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_1 );

                exception_lineno = 48;

                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_21;
                tmp_assign_source_21 = const_int_pos_2147483647;
                UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_MAXSIZE, tmp_assign_source_21 );
            }
            goto branch_end_2;
            branch_no_2:;
            // Tried code:
            {
                PyObject *tmp_assign_source_22;
                PyObject *tmp_dircall_arg1_1;
                tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
                Py_INCREF( tmp_dircall_arg1_1 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
                    tmp_assign_source_22 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
                }
                if ( tmp_assign_source_22 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_1;
                }
                assert( tmp_class_creation_1__bases == NULL );
                tmp_class_creation_1__bases = tmp_assign_source_22;
            }
            {
                PyObject *tmp_assign_source_23;
                tmp_assign_source_23 = PyDict_New();
                assert( tmp_class_creation_1__class_decl_dict == NULL );
                tmp_class_creation_1__class_decl_dict = tmp_assign_source_23;
            }
            {
                PyObject *tmp_assign_source_24;
                PyObject *tmp_metaclass_name_1;
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_key_name_1;
                PyObject *tmp_dict_name_1;
                PyObject *tmp_dict_name_2;
                PyObject *tmp_key_name_2;
                nuitka_bool tmp_condition_result_4;
                int tmp_truth_name_3;
                PyObject *tmp_type_arg_1;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                PyObject *tmp_bases_name_1;
                tmp_key_name_1 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_1;
                }
                else
                {
                    goto condexpr_false_1;
                }
                condexpr_true_1:;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
                tmp_key_name_2 = const_str_plain_metaclass;
                tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
                if ( tmp_metaclass_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_1;
                }
                goto condexpr_end_1;
                condexpr_false_1:;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
                if ( tmp_truth_name_3 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_1;
                }
                tmp_condition_result_4 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_2;
                }
                else
                {
                    goto condexpr_false_2;
                }
                condexpr_true_2:;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_subscribed_name_2 = tmp_class_creation_1__bases;
                tmp_subscript_name_2 = const_int_0;
                tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
                if ( tmp_type_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_1;
                }
                tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
                Py_DECREF( tmp_type_arg_1 );
                if ( tmp_metaclass_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_1;
                }
                goto condexpr_end_2;
                condexpr_false_2:;
                tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
                Py_INCREF( tmp_metaclass_name_1 );
                condexpr_end_2:;
                condexpr_end_1:;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_bases_name_1 = tmp_class_creation_1__bases;
                tmp_assign_source_24 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
                Py_DECREF( tmp_metaclass_name_1 );
                if ( tmp_assign_source_24 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_1;
                }
                assert( tmp_class_creation_1__metaclass == NULL );
                tmp_class_creation_1__metaclass = tmp_assign_source_24;
            }
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_key_name_3;
                PyObject *tmp_dict_name_3;
                tmp_key_name_3 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_1;
                }
                tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
                tmp_dictdel_key = const_str_plain_metaclass;
                tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_1;
                }
                branch_no_3:;
            }
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_source_name_5;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_source_name_5 = tmp_class_creation_1__metaclass;
                tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
                tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_25;
                    PyObject *tmp_called_name_1;
                    PyObject *tmp_source_name_6;
                    PyObject *tmp_args_name_1;
                    PyObject *tmp_tuple_element_4;
                    PyObject *tmp_kw_name_1;
                    CHECK_OBJECT( tmp_class_creation_1__metaclass );
                    tmp_source_name_6 = tmp_class_creation_1__metaclass;
                    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
                    if ( tmp_called_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 53;

                        goto try_except_handler_1;
                    }
                    tmp_tuple_element_4 = const_str_plain_X;
                    tmp_args_name_1 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_4 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_4 );
                    CHECK_OBJECT( tmp_class_creation_1__bases );
                    tmp_tuple_element_4 = tmp_class_creation_1__bases;
                    Py_INCREF( tmp_tuple_element_4 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_4 );
                    CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                    tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
                    frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 53;
                    tmp_assign_source_25 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_args_name_1 );
                    if ( tmp_assign_source_25 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 53;

                        goto try_except_handler_1;
                    }
                    assert( tmp_class_creation_1__prepared == NULL );
                    tmp_class_creation_1__prepared = tmp_assign_source_25;
                }
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_operand_name_1;
                    PyObject *tmp_source_name_7;
                    CHECK_OBJECT( tmp_class_creation_1__prepared );
                    tmp_source_name_7 = tmp_class_creation_1__prepared;
                    tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
                    tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 53;

                        goto try_except_handler_1;
                    }
                    tmp_condition_result_7 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_raise_type_1;
                        PyObject *tmp_raise_value_1;
                        PyObject *tmp_left_name_1;
                        PyObject *tmp_right_name_1;
                        PyObject *tmp_tuple_element_5;
                        PyObject *tmp_getattr_target_1;
                        PyObject *tmp_getattr_attr_1;
                        PyObject *tmp_getattr_default_1;
                        PyObject *tmp_source_name_8;
                        PyObject *tmp_type_arg_2;
                        tmp_raise_type_1 = PyExc_TypeError;
                        tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                        CHECK_OBJECT( tmp_class_creation_1__metaclass );
                        tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                        tmp_getattr_attr_1 = const_str_plain___name__;
                        tmp_getattr_default_1 = const_str_angle_metaclass;
                        tmp_tuple_element_5 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                        if ( tmp_tuple_element_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 53;

                            goto try_except_handler_1;
                        }
                        tmp_right_name_1 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_5 );
                        CHECK_OBJECT( tmp_class_creation_1__prepared );
                        tmp_type_arg_2 = tmp_class_creation_1__prepared;
                        tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_2 );
                        assert( !(tmp_source_name_8 == NULL) );
                        tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                        Py_DECREF( tmp_source_name_8 );
                        if ( tmp_tuple_element_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_right_name_1 );

                            exception_lineno = 53;

                            goto try_except_handler_1;
                        }
                        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_5 );
                        tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                        Py_DECREF( tmp_right_name_1 );
                        if ( tmp_raise_value_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 53;

                            goto try_except_handler_1;
                        }
                        exception_type = tmp_raise_type_1;
                        Py_INCREF( tmp_raise_type_1 );
                        exception_value = tmp_raise_value_1;
                        exception_lineno = 53;
                        RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                        goto try_except_handler_1;
                    }
                    branch_no_5:;
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    PyObject *tmp_assign_source_26;
                    tmp_assign_source_26 = PyDict_New();
                    assert( tmp_class_creation_1__prepared == NULL );
                    tmp_class_creation_1__prepared = tmp_assign_source_26;
                }
                branch_end_4:;
            }
            {
                PyObject *tmp_assign_source_27;
                {
                    PyObject *tmp_set_locals_1;
                    CHECK_OBJECT( tmp_class_creation_1__prepared );
                    tmp_set_locals_1 = tmp_class_creation_1__prepared;
                    locals_scipy$_lib$six_53 = tmp_set_locals_1;
                    Py_INCREF( tmp_set_locals_1 );
                }
                // Tried code:
                // Tried code:
                tmp_dictset_value = const_str_digest_c88389693d9d476899c1c6e170568469;
                tmp_res = PyObject_SetItem( locals_scipy$_lib$six_53, const_str_plain___module__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_3;
                }
                tmp_dictset_value = const_str_plain_X;
                tmp_res = PyObject_SetItem( locals_scipy$_lib$six_53, const_str_plain___qualname__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_3;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2, codeobj_5c528ecdf690c8150d8f95ba5b7f1eaf, module_scipy$_lib$six, sizeof(void *) );
                frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 = cache_frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 ) == 2 ); // Frame stack

                // Framed code:
                tmp_dictset_value = MAKE_FUNCTION_scipy$_lib$six$$$function_1___len__(  );



                tmp_res = PyObject_SetItem( locals_scipy$_lib$six_53, const_str_plain___len__, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 54;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

#if 0
                RESTORE_FRAME_EXCEPTION( frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_1;

                frame_exception_exit_2:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2,
                    type_description_2,
                    outline_0_var___class__
                );


                // Release cached frame.
                if ( frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 == cache_frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 )
                {
                    Py_DECREF( frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 );
                }
                cache_frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 = NULL;

                assertFrameObject( frame_5c528ecdf690c8150d8f95ba5b7f1eaf_2 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_1;

                frame_no_exception_1:;
                goto skip_nested_handling_1;
                nested_frame_exit_1:;

                goto try_except_handler_3;
                skip_nested_handling_1:;
                {
                    nuitka_bool tmp_condition_result_8;
                    PyObject *tmp_compexpr_left_2;
                    PyObject *tmp_compexpr_right_2;
                    CHECK_OBJECT( tmp_class_creation_1__bases );
                    tmp_compexpr_left_2 = tmp_class_creation_1__bases;
                    tmp_compexpr_right_2 = const_tuple_type_object_tuple;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 53;

                        goto try_except_handler_3;
                    }
                    tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_6;
                    }
                    else
                    {
                        goto branch_no_6;
                    }
                    branch_yes_6:;
                    tmp_dictset_value = const_tuple_type_object_tuple;
                    tmp_res = PyObject_SetItem( locals_scipy$_lib$six_53, const_str_plain___orig_bases__, tmp_dictset_value );
                    if ( tmp_res != 0 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 53;

                        goto try_except_handler_3;
                    }
                    branch_no_6:;
                }
                {
                    PyObject *tmp_assign_source_28;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_args_name_2;
                    PyObject *tmp_tuple_element_6;
                    PyObject *tmp_kw_name_2;
                    CHECK_OBJECT( tmp_class_creation_1__metaclass );
                    tmp_called_name_2 = tmp_class_creation_1__metaclass;
                    tmp_tuple_element_6 = const_str_plain_X;
                    tmp_args_name_2 = PyTuple_New( 3 );
                    Py_INCREF( tmp_tuple_element_6 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_6 );
                    CHECK_OBJECT( tmp_class_creation_1__bases );
                    tmp_tuple_element_6 = tmp_class_creation_1__bases;
                    Py_INCREF( tmp_tuple_element_6 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_6 );
                    tmp_tuple_element_6 = locals_scipy$_lib$six_53;
                    Py_INCREF( tmp_tuple_element_6 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_6 );
                    CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                    tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
                    frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 53;
                    tmp_assign_source_28 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
                    Py_DECREF( tmp_args_name_2 );
                    if ( tmp_assign_source_28 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 53;

                        goto try_except_handler_3;
                    }
                    assert( outline_0_var___class__ == NULL );
                    outline_0_var___class__ = tmp_assign_source_28;
                }
                CHECK_OBJECT( outline_0_var___class__ );
                tmp_assign_source_27 = outline_0_var___class__;
                Py_INCREF( tmp_assign_source_27 );
                goto try_return_handler_3;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( scipy$_lib$six );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_3:;
                Py_DECREF( locals_scipy$_lib$six_53 );
                locals_scipy$_lib$six_53 = NULL;
                goto try_return_handler_2;
                // Exception handler code:
                try_except_handler_3:;
                exception_keeper_type_1 = exception_type;
                exception_keeper_value_1 = exception_value;
                exception_keeper_tb_1 = exception_tb;
                exception_keeper_lineno_1 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_DECREF( locals_scipy$_lib$six_53 );
                locals_scipy$_lib$six_53 = NULL;
                // Re-raise.
                exception_type = exception_keeper_type_1;
                exception_value = exception_keeper_value_1;
                exception_tb = exception_keeper_tb_1;
                exception_lineno = exception_keeper_lineno_1;

                goto try_except_handler_2;
                // End of try:
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( scipy$_lib$six );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_2:;
                CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
                Py_DECREF( outline_0_var___class__ );
                outline_0_var___class__ = NULL;

                goto outline_result_1;
                // Exception handler code:
                try_except_handler_2:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto outline_exception_1;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( scipy$_lib$six );
                return MOD_RETURN_VALUE( NULL );
                outline_exception_1:;
                exception_lineno = 53;
                goto try_except_handler_1;
                outline_result_1:;
                UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_X, tmp_assign_source_27 );
            }
            goto try_end_1;
            // Exception handler code:
            try_except_handler_1:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_class_creation_1__bases );
            tmp_class_creation_1__bases = NULL;

            Py_XDECREF( tmp_class_creation_1__class_decl_dict );
            tmp_class_creation_1__class_decl_dict = NULL;

            Py_XDECREF( tmp_class_creation_1__metaclass );
            tmp_class_creation_1__metaclass = NULL;

            Py_XDECREF( tmp_class_creation_1__prepared );
            tmp_class_creation_1__prepared = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto frame_exception_exit_1;
            // End of try:
            try_end_1:;
            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
            Py_DECREF( tmp_class_creation_1__bases );
            tmp_class_creation_1__bases = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
            Py_DECREF( tmp_class_creation_1__class_decl_dict );
            tmp_class_creation_1__class_decl_dict = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
            Py_DECREF( tmp_class_creation_1__metaclass );
            tmp_class_creation_1__metaclass = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
            Py_DECREF( tmp_class_creation_1__prepared );
            tmp_class_creation_1__prepared = NULL;

            {
                nuitka_bool tmp_assign_source_29;
                tmp_assign_source_29 = NUITKA_BOOL_TRUE;
                tmp_try_except_1__unhandled_indicator = tmp_assign_source_29;
            }
            // Tried code:
            // Tried code:
            {
                PyObject *tmp_len_arg_1;
                PyObject *tmp_called_name_3;
                PyObject *tmp_mvar_value_11;
                PyObject *tmp_capi_result_1;
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_X );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_X );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "X" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 57;

                    goto try_except_handler_5;
                }

                tmp_called_name_3 = tmp_mvar_value_11;
                frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 57;
                tmp_len_arg_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
                if ( tmp_len_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 57;

                    goto try_except_handler_5;
                }
                tmp_capi_result_1 = BUILTIN_LEN( tmp_len_arg_1 );
                Py_DECREF( tmp_len_arg_1 );
                if ( tmp_capi_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 57;

                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_capi_result_1 );
            }
            goto try_end_2;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            {
                nuitka_bool tmp_assign_source_30;
                tmp_assign_source_30 = NUITKA_BOOL_FALSE;
                tmp_try_except_1__unhandled_indicator = tmp_assign_source_30;
            }
            // Preserve existing published exception.
            exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_1 );
            exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_1 );
            exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_1 );

            if ( exception_keeper_tb_4 == NULL )
            {
                exception_keeper_tb_4 = MAKE_TRACEBACK( frame_3ee8b0c17a2ecfea3609cfca70f074b6, exception_keeper_lineno_4 );
            }
            else if ( exception_keeper_lineno_4 != 0 )
            {
                exception_keeper_tb_4 = ADD_TRACEBACK( exception_keeper_tb_4, frame_3ee8b0c17a2ecfea3609cfca70f074b6, exception_keeper_lineno_4 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
            PyException_SetTraceback( exception_keeper_value_4, (PyObject *)exception_keeper_tb_4 );
            PUBLISH_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_9;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                tmp_compexpr_left_3 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_3 = PyExc_OverflowError;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 58;

                    goto try_except_handler_6;
                }
                tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    PyObject *tmp_assign_source_31;
                    tmp_assign_source_31 = const_int_pos_2147483647;
                    UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_MAXSIZE, tmp_assign_source_31 );
                }
                goto branch_end_7;
                branch_no_7:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 56;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame) frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = exception_tb->tb_lineno;

                goto try_except_handler_6;
                branch_end_7:;
            }
            goto try_end_3;
            // Exception handler code:
            try_except_handler_6:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            // Re-raise.
            exception_type = exception_keeper_type_5;
            exception_value = exception_keeper_value_5;
            exception_tb = exception_keeper_tb_5;
            exception_lineno = exception_keeper_lineno_5;

            goto try_except_handler_4;
            // End of try:
            try_end_3:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            goto try_end_2;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( scipy$_lib$six );
            return MOD_RETURN_VALUE( NULL );
            // End of try:
            try_end_2:;
            {
                nuitka_bool tmp_condition_result_10;
                nuitka_bool tmp_compexpr_left_4;
                nuitka_bool tmp_compexpr_right_4;
                assert( tmp_try_except_1__unhandled_indicator != NUITKA_BOOL_UNASSIGNED);
                tmp_compexpr_left_4 = tmp_try_except_1__unhandled_indicator;
                tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
                tmp_condition_result_10 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                {
                    PyObject *tmp_assign_source_32;
                    tmp_assign_source_32 = const_int_pos_9223372036854775807;
                    UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_MAXSIZE, tmp_assign_source_32 );
                }
                tmp_res = PyDict_DelItem( (PyObject *)moduledict_scipy$_lib$six, const_str_plain_X );
                tmp_result = tmp_res != -1;
                if ( tmp_result == false ) CLEAR_ERROR_OCCURRED();

                if ( tmp_result == false )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "X" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 64;

                    goto try_except_handler_4;
                }

                branch_no_8:;
            }
            goto try_end_4;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto frame_exception_exit_1;
            // End of try:
            try_end_4:;
            branch_end_2:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_33;
        tmp_assign_source_33 = MAKE_FUNCTION_scipy$_lib$six$$$function_2__add_doc(  );



        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__add_doc, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = MAKE_FUNCTION_scipy$_lib$six$$$function_3__import_module(  );



        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__import_module, tmp_assign_source_34 );
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_mvar_value_12;
        int tmp_truth_name_4;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;

            goto frame_exception_exit_1;
        }

        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_mvar_value_12 );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_11 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        {
            PyObject *tmp_assign_source_35;
            PyObject *tmp_name_name_4;
            PyObject *tmp_globals_name_4;
            PyObject *tmp_locals_name_4;
            PyObject *tmp_fromlist_name_4;
            PyObject *tmp_level_name_4;
            tmp_name_name_4 = const_str_plain_builtins;
            tmp_globals_name_4 = (PyObject *)moduledict_scipy$_lib$six;
            tmp_locals_name_4 = Py_None;
            tmp_fromlist_name_4 = Py_None;
            tmp_level_name_4 = const_int_0;
            frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 80;
            tmp_assign_source_35 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
            assert( !(tmp_assign_source_35 == NULL) );
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_builtins, tmp_assign_source_35 );
        }
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_name_name_5;
            PyObject *tmp_globals_name_5;
            PyObject *tmp_locals_name_5;
            PyObject *tmp_fromlist_name_5;
            PyObject *tmp_level_name_5;
            tmp_name_name_5 = const_str_plain_functools;
            tmp_globals_name_5 = (PyObject *)moduledict_scipy$_lib$six;
            tmp_locals_name_5 = Py_None;
            tmp_fromlist_name_5 = Py_None;
            tmp_level_name_5 = const_int_0;
            frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 81;
            tmp_assign_source_36 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
            if ( tmp_assign_source_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_functools, tmp_assign_source_36 );
        }
        {
            PyObject *tmp_assign_source_37;
            PyObject *tmp_source_name_9;
            PyObject *tmp_mvar_value_13;
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_functools );

            if (unlikely( tmp_mvar_value_13 == NULL ))
            {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
            }

            CHECK_OBJECT( tmp_mvar_value_13 );
            tmp_source_name_9 = tmp_mvar_value_13;
            tmp_assign_source_37 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_reduce );
            if ( tmp_assign_source_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 82;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_reduce, tmp_assign_source_37 );
        }
        {
            PyObject *tmp_assign_source_38;
            PyObject *tmp_source_name_10;
            PyObject *tmp_mvar_value_14;
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_builtins );

            if (unlikely( tmp_mvar_value_14 == NULL ))
            {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_builtins );
            }

            if ( tmp_mvar_value_14 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "builtins" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 83;

                goto frame_exception_exit_1;
            }

            tmp_source_name_10 = tmp_mvar_value_14;
            tmp_assign_source_38 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_zip );
            if ( tmp_assign_source_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_zip, tmp_assign_source_38 );
        }
        {
            PyObject *tmp_assign_source_39;
            PyObject *tmp_source_name_11;
            PyObject *tmp_mvar_value_15;
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_builtins );

            if (unlikely( tmp_mvar_value_15 == NULL ))
            {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_builtins );
            }

            if ( tmp_mvar_value_15 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "builtins" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 84;

                goto frame_exception_exit_1;
            }

            tmp_source_name_11 = tmp_mvar_value_15;
            tmp_assign_source_39 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_range );
            if ( tmp_assign_source_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_xrange, tmp_assign_source_39 );
        }
        goto branch_end_9;
        branch_no_9:;
        {
            PyObject *tmp_assign_source_40;
            PyObject *tmp_name_name_6;
            PyObject *tmp_globals_name_6;
            PyObject *tmp_locals_name_6;
            PyObject *tmp_fromlist_name_6;
            PyObject *tmp_level_name_6;
            tmp_name_name_6 = const_str_plain___builtin__;
            tmp_globals_name_6 = (PyObject *)moduledict_scipy$_lib$six;
            tmp_locals_name_6 = Py_None;
            tmp_fromlist_name_6 = Py_None;
            tmp_level_name_6 = const_int_0;
            frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 86;
            tmp_assign_source_40 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
            if ( tmp_assign_source_40 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___builtin__, tmp_assign_source_40 );
        }
        {
            PyObject *tmp_assign_source_41;
            PyObject *tmp_name_name_7;
            PyObject *tmp_globals_name_7;
            PyObject *tmp_locals_name_7;
            PyObject *tmp_fromlist_name_7;
            PyObject *tmp_level_name_7;
            tmp_name_name_7 = const_str_plain_itertools;
            tmp_globals_name_7 = (PyObject *)moduledict_scipy$_lib$six;
            tmp_locals_name_7 = Py_None;
            tmp_fromlist_name_7 = Py_None;
            tmp_level_name_7 = const_int_0;
            frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 87;
            tmp_assign_source_41 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
            assert( !(tmp_assign_source_41 == NULL) );
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_itertools, tmp_assign_source_41 );
        }
        {
            PyObject *tmp_assign_source_42;
            PyObject *tmp_mvar_value_16;
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___builtin__ );

            if (unlikely( tmp_mvar_value_16 == NULL ))
            {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___builtin__ );
            }

            CHECK_OBJECT( tmp_mvar_value_16 );
            tmp_assign_source_42 = tmp_mvar_value_16;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_builtins, tmp_assign_source_42 );
        }
        {
            PyObject *tmp_assign_source_43;
            PyObject *tmp_source_name_12;
            PyObject *tmp_mvar_value_17;
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___builtin__ );

            if (unlikely( tmp_mvar_value_17 == NULL ))
            {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___builtin__ );
            }

            CHECK_OBJECT( tmp_mvar_value_17 );
            tmp_source_name_12 = tmp_mvar_value_17;
            tmp_assign_source_43 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_reduce );
            if ( tmp_assign_source_43 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_reduce, tmp_assign_source_43 );
        }
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_source_name_13;
            PyObject *tmp_mvar_value_18;
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_itertools );

            if (unlikely( tmp_mvar_value_18 == NULL ))
            {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_itertools );
            }

            if ( tmp_mvar_value_18 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "itertools" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 90;

                goto frame_exception_exit_1;
            }

            tmp_source_name_13 = tmp_mvar_value_18;
            tmp_assign_source_44 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_izip );
            if ( tmp_assign_source_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_zip, tmp_assign_source_44 );
        }
        {
            PyObject *tmp_assign_source_45;
            PyObject *tmp_source_name_14;
            PyObject *tmp_mvar_value_19;
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain___builtin__ );

            if (unlikely( tmp_mvar_value_19 == NULL ))
            {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___builtin__ );
            }

            if ( tmp_mvar_value_19 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "__builtin__" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 91;

                goto frame_exception_exit_1;
            }

            tmp_source_name_14 = tmp_mvar_value_19;
            tmp_assign_source_45 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_xrange );
            if ( tmp_assign_source_45 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_xrange, tmp_assign_source_45 );
        }
        branch_end_9:;
    }
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_mvar_value_20;
        int tmp_truth_name_5;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }

        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_mvar_value_20 );
        if ( tmp_truth_name_5 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_12 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_46;
            tmp_assign_source_46 = const_str_plain___func__;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__meth_func, tmp_assign_source_46 );
        }
        {
            PyObject *tmp_assign_source_47;
            tmp_assign_source_47 = const_str_plain___self__;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__meth_self, tmp_assign_source_47 );
        }
        {
            PyObject *tmp_assign_source_48;
            tmp_assign_source_48 = const_str_plain___code__;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__func_code, tmp_assign_source_48 );
        }
        {
            PyObject *tmp_assign_source_49;
            tmp_assign_source_49 = const_str_plain___defaults__;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__func_defaults, tmp_assign_source_49 );
        }
        {
            PyObject *tmp_assign_source_50;
            tmp_assign_source_50 = const_str_plain_keys;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__iterkeys, tmp_assign_source_50 );
        }
        {
            PyObject *tmp_assign_source_51;
            tmp_assign_source_51 = const_str_plain_values;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__itervalues, tmp_assign_source_51 );
        }
        {
            PyObject *tmp_assign_source_52;
            tmp_assign_source_52 = const_str_plain_items;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__iteritems, tmp_assign_source_52 );
        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_53;
            tmp_assign_source_53 = const_str_plain_im_func;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__meth_func, tmp_assign_source_53 );
        }
        {
            PyObject *tmp_assign_source_54;
            tmp_assign_source_54 = const_str_plain_im_self;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__meth_self, tmp_assign_source_54 );
        }
        {
            PyObject *tmp_assign_source_55;
            tmp_assign_source_55 = const_str_plain_func_code;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__func_code, tmp_assign_source_55 );
        }
        {
            PyObject *tmp_assign_source_56;
            tmp_assign_source_56 = const_str_plain_func_defaults;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__func_defaults, tmp_assign_source_56 );
        }
        {
            PyObject *tmp_assign_source_57;
            tmp_assign_source_57 = const_str_plain_iterkeys;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__iterkeys, tmp_assign_source_57 );
        }
        {
            PyObject *tmp_assign_source_58;
            tmp_assign_source_58 = const_str_plain_itervalues;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__itervalues, tmp_assign_source_58 );
        }
        {
            PyObject *tmp_assign_source_59;
            tmp_assign_source_59 = const_str_plain_iteritems;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__iteritems, tmp_assign_source_59 );
        }
        branch_end_10:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_60;
        PyObject *tmp_mvar_value_21;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_next );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_next );
        }

        if ( tmp_mvar_value_21 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "next" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;

            goto try_except_handler_7;
        }

        tmp_assign_source_60 = tmp_mvar_value_21;
        UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_advance_iterator, tmp_assign_source_60 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_7 == NULL )
    {
        exception_keeper_tb_7 = MAKE_TRACEBACK( frame_3ee8b0c17a2ecfea3609cfca70f074b6, exception_keeper_lineno_7 );
    }
    else if ( exception_keeper_lineno_7 != 0 )
    {
        exception_keeper_tb_7 = ADD_TRACEBACK( exception_keeper_tb_7, frame_3ee8b0c17a2ecfea3609cfca70f074b6, exception_keeper_lineno_7 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    PyException_SetTraceback( exception_keeper_value_7, (PyObject *)exception_keeper_tb_7 );
    PUBLISH_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_13;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        tmp_compexpr_left_5 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_5 = PyExc_NameError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_8;
        }
        tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_assign_source_61;
            tmp_assign_source_61 = MAKE_FUNCTION_scipy$_lib$six$$$function_4_advance_iterator(  );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_advance_iterator, tmp_assign_source_61 );
        }
        goto branch_end_11;
        branch_no_11:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 116;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame) frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_8;
        branch_end_11:;
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_5;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( scipy$_lib$six );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_5:;
    {
        PyObject *tmp_assign_source_62;
        PyObject *tmp_mvar_value_22;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_advance_iterator );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_advance_iterator );
        }

        if ( tmp_mvar_value_22 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "advance_iterator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_62 = tmp_mvar_value_22;
        UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_next, tmp_assign_source_62 );
    }
    {
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_mvar_value_23;
        int tmp_truth_name_6;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        if ( tmp_mvar_value_23 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }

        tmp_truth_name_6 = CHECK_IF_TRUE( tmp_mvar_value_23 );
        if ( tmp_truth_name_6 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_14 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_12;
        }
        else
        {
            goto branch_no_12;
        }
        branch_yes_12:;
        {
            PyObject *tmp_assign_source_63;
            tmp_assign_source_63 = MAKE_FUNCTION_scipy$_lib$six$$$function_5_get_unbound_function(  );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_get_unbound_function, tmp_assign_source_63 );
        }
        {
            PyObject *tmp_assign_source_64;
            tmp_assign_source_64 = (PyObject *)&PyBaseObject_Type;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_Iterator, tmp_assign_source_64 );
        }
        {
            PyObject *tmp_assign_source_65;
            tmp_assign_source_65 = MAKE_FUNCTION_scipy$_lib$six$$$function_6_callable(  );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_callable, tmp_assign_source_65 );
        }
        goto branch_end_12;
        branch_no_12:;
        {
            PyObject *tmp_assign_source_66;
            tmp_assign_source_66 = MAKE_FUNCTION_scipy$_lib$six$$$function_7_get_unbound_function(  );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_get_unbound_function, tmp_assign_source_66 );
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_67;
            PyObject *tmp_dircall_arg1_2;
            tmp_dircall_arg1_2 = const_tuple_type_object_tuple;
            Py_INCREF( tmp_dircall_arg1_2 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
                tmp_assign_source_67 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_67 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_9;
            }
            assert( tmp_class_creation_2__bases == NULL );
            tmp_class_creation_2__bases = tmp_assign_source_67;
        }
        {
            PyObject *tmp_assign_source_68;
            tmp_assign_source_68 = PyDict_New();
            assert( tmp_class_creation_2__class_decl_dict == NULL );
            tmp_class_creation_2__class_decl_dict = tmp_assign_source_68;
        }
        {
            PyObject *tmp_assign_source_69;
            PyObject *tmp_metaclass_name_2;
            nuitka_bool tmp_condition_result_15;
            PyObject *tmp_key_name_4;
            PyObject *tmp_dict_name_4;
            PyObject *tmp_dict_name_5;
            PyObject *tmp_key_name_5;
            nuitka_bool tmp_condition_result_16;
            int tmp_truth_name_7;
            PyObject *tmp_type_arg_3;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_bases_name_2;
            tmp_key_name_4 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_9;
            }
            tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
            tmp_key_name_5 = const_str_plain_metaclass;
            tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
            if ( tmp_metaclass_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_9;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_truth_name_7 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
            if ( tmp_truth_name_7 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_9;
            }
            tmp_condition_result_16 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_4;
            }
            else
            {
                goto condexpr_false_4;
            }
            condexpr_true_4:;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_subscribed_name_3 = tmp_class_creation_2__bases;
            tmp_subscript_name_3 = const_int_0;
            tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
            if ( tmp_type_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_9;
            }
            tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
            Py_DECREF( tmp_type_arg_3 );
            if ( tmp_metaclass_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_9;
            }
            goto condexpr_end_4;
            condexpr_false_4:;
            tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_2 );
            condexpr_end_4:;
            condexpr_end_3:;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_bases_name_2 = tmp_class_creation_2__bases;
            tmp_assign_source_69 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
            Py_DECREF( tmp_metaclass_name_2 );
            if ( tmp_assign_source_69 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_9;
            }
            assert( tmp_class_creation_2__metaclass == NULL );
            tmp_class_creation_2__metaclass = tmp_assign_source_69;
        }
        {
            nuitka_bool tmp_condition_result_17;
            PyObject *tmp_key_name_6;
            PyObject *tmp_dict_name_6;
            tmp_key_name_6 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_9;
            }
            tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_13;
            }
            else
            {
                goto branch_no_13;
            }
            branch_yes_13:;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_9;
            }
            branch_no_13:;
        }
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_source_name_15;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_15 = tmp_class_creation_2__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_15, const_str_plain___prepare__ );
            tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_14;
            }
            else
            {
                goto branch_no_14;
            }
            branch_yes_14:;
            {
                PyObject *tmp_assign_source_70;
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_16;
                PyObject *tmp_args_name_3;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_kw_name_3;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_source_name_16 = tmp_class_creation_2__metaclass;
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain___prepare__ );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 136;

                    goto try_except_handler_9;
                }
                tmp_tuple_element_7 = const_str_plain_Iterator;
                tmp_args_name_3 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_7 );
                PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__bases );
                tmp_tuple_element_7 = tmp_class_creation_2__bases;
                Py_INCREF( tmp_tuple_element_7 );
                PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
                tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
                frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 136;
                tmp_assign_source_70 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_3, tmp_kw_name_3 );
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_name_3 );
                if ( tmp_assign_source_70 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 136;

                    goto try_except_handler_9;
                }
                assert( tmp_class_creation_2__prepared == NULL );
                tmp_class_creation_2__prepared = tmp_assign_source_70;
            }
            {
                nuitka_bool tmp_condition_result_19;
                PyObject *tmp_operand_name_2;
                PyObject *tmp_source_name_17;
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_source_name_17 = tmp_class_creation_2__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_17, const_str_plain___getitem__ );
                tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 136;

                    goto try_except_handler_9;
                }
                tmp_condition_result_19 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_15;
                }
                else
                {
                    goto branch_no_15;
                }
                branch_yes_15:;
                {
                    PyObject *tmp_raise_type_2;
                    PyObject *tmp_raise_value_2;
                    PyObject *tmp_left_name_2;
                    PyObject *tmp_right_name_2;
                    PyObject *tmp_tuple_element_8;
                    PyObject *tmp_getattr_target_2;
                    PyObject *tmp_getattr_attr_2;
                    PyObject *tmp_getattr_default_2;
                    PyObject *tmp_source_name_18;
                    PyObject *tmp_type_arg_4;
                    tmp_raise_type_2 = PyExc_TypeError;
                    tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_class_creation_2__metaclass );
                    tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                    tmp_getattr_attr_2 = const_str_plain___name__;
                    tmp_getattr_default_2 = const_str_angle_metaclass;
                    tmp_tuple_element_8 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                    if ( tmp_tuple_element_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 136;

                        goto try_except_handler_9;
                    }
                    tmp_right_name_2 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_8 );
                    CHECK_OBJECT( tmp_class_creation_2__prepared );
                    tmp_type_arg_4 = tmp_class_creation_2__prepared;
                    tmp_source_name_18 = BUILTIN_TYPE1( tmp_type_arg_4 );
                    assert( !(tmp_source_name_18 == NULL) );
                    tmp_tuple_element_8 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_18 );
                    if ( tmp_tuple_element_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_2 );

                        exception_lineno = 136;

                        goto try_except_handler_9;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_8 );
                    tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                    Py_DECREF( tmp_right_name_2 );
                    if ( tmp_raise_value_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 136;

                        goto try_except_handler_9;
                    }
                    exception_type = tmp_raise_type_2;
                    Py_INCREF( tmp_raise_type_2 );
                    exception_value = tmp_raise_value_2;
                    exception_lineno = 136;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                    goto try_except_handler_9;
                }
                branch_no_15:;
            }
            goto branch_end_14;
            branch_no_14:;
            {
                PyObject *tmp_assign_source_71;
                tmp_assign_source_71 = PyDict_New();
                assert( tmp_class_creation_2__prepared == NULL );
                tmp_class_creation_2__prepared = tmp_assign_source_71;
            }
            branch_end_14:;
        }
        {
            PyObject *tmp_assign_source_72;
            {
                PyObject *tmp_set_locals_2;
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_set_locals_2 = tmp_class_creation_2__prepared;
                locals_scipy$_lib$six_136 = tmp_set_locals_2;
                Py_INCREF( tmp_set_locals_2 );
            }
            // Tried code:
            // Tried code:
            tmp_dictset_value = const_str_digest_c88389693d9d476899c1c6e170568469;
            tmp_res = PyObject_SetItem( locals_scipy$_lib$six_136, const_str_plain___module__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_11;
            }
            tmp_dictset_value = const_str_plain_Iterator;
            tmp_res = PyObject_SetItem( locals_scipy$_lib$six_136, const_str_plain___qualname__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;

                goto try_except_handler_11;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3, codeobj_67dd5b4a8fd268387081fcc7bcbc8cdb, module_scipy$_lib$six, sizeof(void *) );
            frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 = cache_frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 ) == 2 ); // Frame stack

            // Framed code:
            tmp_dictset_value = MAKE_FUNCTION_scipy$_lib$six$$$function_8_next(  );



            tmp_res = PyObject_SetItem( locals_scipy$_lib$six_136, const_str_plain_next, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

#if 0
            RESTORE_FRAME_EXCEPTION( frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_2;

            frame_exception_exit_3:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3,
                type_description_2,
                outline_1_var___class__
            );


            // Release cached frame.
            if ( frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 == cache_frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 )
            {
                Py_DECREF( frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 );
            }
            cache_frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 = NULL;

            assertFrameObject( frame_67dd5b4a8fd268387081fcc7bcbc8cdb_3 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_2;

            frame_no_exception_2:;
            goto skip_nested_handling_2;
            nested_frame_exit_2:;

            goto try_except_handler_11;
            skip_nested_handling_2:;
            {
                nuitka_bool tmp_condition_result_20;
                PyObject *tmp_compexpr_left_6;
                PyObject *tmp_compexpr_right_6;
                CHECK_OBJECT( tmp_class_creation_2__bases );
                tmp_compexpr_left_6 = tmp_class_creation_2__bases;
                tmp_compexpr_right_6 = const_tuple_type_object_tuple;
                tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 136;

                    goto try_except_handler_11;
                }
                tmp_condition_result_20 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_16;
                }
                else
                {
                    goto branch_no_16;
                }
                branch_yes_16:;
                tmp_dictset_value = const_tuple_type_object_tuple;
                tmp_res = PyObject_SetItem( locals_scipy$_lib$six_136, const_str_plain___orig_bases__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 136;

                    goto try_except_handler_11;
                }
                branch_no_16:;
            }
            {
                PyObject *tmp_assign_source_73;
                PyObject *tmp_called_name_5;
                PyObject *tmp_args_name_4;
                PyObject *tmp_tuple_element_9;
                PyObject *tmp_kw_name_4;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_called_name_5 = tmp_class_creation_2__metaclass;
                tmp_tuple_element_9 = const_str_plain_Iterator;
                tmp_args_name_4 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_9 );
                PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_9 );
                CHECK_OBJECT( tmp_class_creation_2__bases );
                tmp_tuple_element_9 = tmp_class_creation_2__bases;
                Py_INCREF( tmp_tuple_element_9 );
                PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_9 );
                tmp_tuple_element_9 = locals_scipy$_lib$six_136;
                Py_INCREF( tmp_tuple_element_9 );
                PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_9 );
                CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
                tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
                frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 136;
                tmp_assign_source_73 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_4, tmp_kw_name_4 );
                Py_DECREF( tmp_args_name_4 );
                if ( tmp_assign_source_73 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 136;

                    goto try_except_handler_11;
                }
                assert( outline_1_var___class__ == NULL );
                outline_1_var___class__ = tmp_assign_source_73;
            }
            CHECK_OBJECT( outline_1_var___class__ );
            tmp_assign_source_72 = outline_1_var___class__;
            Py_INCREF( tmp_assign_source_72 );
            goto try_return_handler_11;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( scipy$_lib$six );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_11:;
            Py_DECREF( locals_scipy$_lib$six_136 );
            locals_scipy$_lib$six_136 = NULL;
            goto try_return_handler_10;
            // Exception handler code:
            try_except_handler_11:;
            exception_keeper_type_9 = exception_type;
            exception_keeper_value_9 = exception_value;
            exception_keeper_tb_9 = exception_tb;
            exception_keeper_lineno_9 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_DECREF( locals_scipy$_lib$six_136 );
            locals_scipy$_lib$six_136 = NULL;
            // Re-raise.
            exception_type = exception_keeper_type_9;
            exception_value = exception_keeper_value_9;
            exception_tb = exception_keeper_tb_9;
            exception_lineno = exception_keeper_lineno_9;

            goto try_except_handler_10;
            // End of try:
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( scipy$_lib$six );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_10:;
            CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
            Py_DECREF( outline_1_var___class__ );
            outline_1_var___class__ = NULL;

            goto outline_result_2;
            // Exception handler code:
            try_except_handler_10:;
            exception_keeper_type_10 = exception_type;
            exception_keeper_value_10 = exception_value;
            exception_keeper_tb_10 = exception_tb;
            exception_keeper_lineno_10 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Re-raise.
            exception_type = exception_keeper_type_10;
            exception_value = exception_keeper_value_10;
            exception_tb = exception_keeper_tb_10;
            exception_lineno = exception_keeper_lineno_10;

            goto outline_exception_2;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( scipy$_lib$six );
            return MOD_RETURN_VALUE( NULL );
            outline_exception_2:;
            exception_lineno = 136;
            goto try_except_handler_9;
            outline_result_2:;
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_Iterator, tmp_assign_source_72 );
        }
        goto try_end_7;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_class_creation_2__bases );
        tmp_class_creation_2__bases = NULL;

        Py_XDECREF( tmp_class_creation_2__class_decl_dict );
        tmp_class_creation_2__class_decl_dict = NULL;

        Py_XDECREF( tmp_class_creation_2__metaclass );
        tmp_class_creation_2__metaclass = NULL;

        Py_XDECREF( tmp_class_creation_2__prepared );
        tmp_class_creation_2__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;
        exception_lineno = exception_keeper_lineno_11;

        goto frame_exception_exit_1;
        // End of try:
        try_end_7:;
        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
        Py_DECREF( tmp_class_creation_2__bases );
        tmp_class_creation_2__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
        Py_DECREF( tmp_class_creation_2__class_decl_dict );
        tmp_class_creation_2__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
        Py_DECREF( tmp_class_creation_2__metaclass );
        tmp_class_creation_2__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
        Py_DECREF( tmp_class_creation_2__prepared );
        tmp_class_creation_2__prepared = NULL;

        {
            PyObject *tmp_assign_source_74;
            PyObject *tmp_mvar_value_24;
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_callable );

            if (unlikely( tmp_mvar_value_24 == NULL ))
            {
                tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_callable );
            }

            if ( tmp_mvar_value_24 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "callable" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 141;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_74 = tmp_mvar_value_24;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_callable, tmp_assign_source_74 );
        }
        branch_end_12:;
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__add_doc );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__add_doc );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_add_doc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_25;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_get_unbound_function );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_unbound_function );
        }

        if ( tmp_mvar_value_26 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_unbound_function" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_26;
        tmp_args_element_name_2 = const_str_digest_a13cea3eac144232780ec70fab3f1816;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 142;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_assign_source_75;
        PyObject *tmp_called_name_7;
        PyObject *tmp_source_name_19;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_mvar_value_28;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_27 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }

        tmp_source_name_19 = tmp_mvar_value_27;
        tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_attrgetter );
        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__meth_func );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__meth_func );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_called_name_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_meth_func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = tmp_mvar_value_28;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 146;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_75 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        if ( tmp_assign_source_75 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_get_method_function, tmp_assign_source_75 );
    }
    {
        PyObject *tmp_assign_source_76;
        PyObject *tmp_called_name_8;
        PyObject *tmp_source_name_20;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_mvar_value_30;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_29 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }

        tmp_source_name_20 = tmp_mvar_value_29;
        tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_attrgetter );
        if ( tmp_called_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__meth_self );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__meth_self );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_called_name_8 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_meth_self" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_4 = tmp_mvar_value_30;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 147;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_76 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_called_name_8 );
        if ( tmp_assign_source_76 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_get_method_self, tmp_assign_source_76 );
    }
    {
        PyObject *tmp_assign_source_77;
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_21;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_mvar_value_32;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_31 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;

            goto frame_exception_exit_1;
        }

        tmp_source_name_21 = tmp_mvar_value_31;
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_attrgetter );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__func_code );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__func_code );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_called_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_func_code" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = tmp_mvar_value_32;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 148;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_assign_source_77 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        if ( tmp_assign_source_77 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_get_function_code, tmp_assign_source_77 );
    }
    {
        PyObject *tmp_assign_source_78;
        PyObject *tmp_called_name_10;
        PyObject *tmp_source_name_22;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_mvar_value_34;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_operator );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
        }

        if ( tmp_mvar_value_33 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;

            goto frame_exception_exit_1;
        }

        tmp_source_name_22 = tmp_mvar_value_33;
        tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_attrgetter );
        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__func_defaults );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__func_defaults );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_called_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_func_defaults" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_6 = tmp_mvar_value_34;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 149;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_assign_source_78 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        if ( tmp_assign_source_78 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_get_function_defaults, tmp_assign_source_78 );
    }
    {
        PyObject *tmp_assign_source_79;
        tmp_assign_source_79 = MAKE_FUNCTION_scipy$_lib$six$$$function_9_iterkeys(  );



        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_iterkeys, tmp_assign_source_79 );
    }
    {
        PyObject *tmp_assign_source_80;
        tmp_assign_source_80 = MAKE_FUNCTION_scipy$_lib$six$$$function_10_itervalues(  );



        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_itervalues, tmp_assign_source_80 );
    }
    {
        PyObject *tmp_assign_source_81;
        tmp_assign_source_81 = MAKE_FUNCTION_scipy$_lib$six$$$function_11_iteritems(  );



        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_iteritems, tmp_assign_source_81 );
    }
    {
        nuitka_bool tmp_condition_result_21;
        PyObject *tmp_mvar_value_35;
        int tmp_truth_name_8;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        if ( tmp_mvar_value_35 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_truth_name_8 = CHECK_IF_TRUE( tmp_mvar_value_35 );
        if ( tmp_truth_name_8 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_21 = tmp_truth_name_8 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_17;
        }
        else
        {
            goto branch_no_17;
        }
        branch_yes_17:;
        {
            PyObject *tmp_assign_source_82;
            tmp_assign_source_82 = MAKE_FUNCTION_scipy$_lib$six$$$function_12_b(  );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_b, tmp_assign_source_82 );
        }
        {
            PyObject *tmp_assign_source_83;
            tmp_assign_source_83 = MAKE_FUNCTION_scipy$_lib$six$$$function_13_u(  );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_u, tmp_assign_source_83 );
        }
        {
            nuitka_bool tmp_condition_result_22;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_source_name_23;
            PyObject *tmp_mvar_value_36;
            PyObject *tmp_subscript_name_4;
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_36 == NULL ))
            {
                tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_36 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 174;

                goto frame_exception_exit_1;
            }

            tmp_source_name_23 = tmp_mvar_value_36;
            tmp_subscribed_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_version_info );
            if ( tmp_subscribed_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;

                goto frame_exception_exit_1;
            }
            tmp_subscript_name_4 = const_int_pos_1;
            tmp_compexpr_left_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 1 );
            Py_DECREF( tmp_subscribed_name_4 );
            if ( tmp_compexpr_left_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;

                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_7 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_LTE_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
            Py_DECREF( tmp_compexpr_left_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;

                goto frame_exception_exit_1;
            }
            tmp_condition_result_22 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_18;
            }
            else
            {
                goto branch_no_18;
            }
            branch_yes_18:;
            {
                PyObject *tmp_assign_source_84;
                tmp_assign_source_84 = MAKE_FUNCTION_scipy$_lib$six$$$function_14_int2byte(  );



                UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_int2byte, tmp_assign_source_84 );
            }
            goto branch_end_18;
            branch_no_18:;
            {
                PyObject *tmp_assign_source_85;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_mvar_value_37;
                tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_operator );

                if (unlikely( tmp_mvar_value_37 == NULL ))
                {
                    tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_operator );
                }

                if ( tmp_mvar_value_37 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "operator" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 179;

                    goto frame_exception_exit_1;
                }

                tmp_called_instance_2 = tmp_mvar_value_37;
                frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 179;
                tmp_assign_source_85 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_2, const_str_plain_methodcaller, &PyTuple_GET_ITEM( const_tuple_str_plain_to_bytes_int_pos_1_str_plain_big_tuple, 0 ) );

                if ( tmp_assign_source_85 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 179;

                    goto frame_exception_exit_1;
                }
                UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_int2byte, tmp_assign_source_85 );
            }
            branch_end_18:;
        }
        {
            PyObject *tmp_assign_source_86;
            PyObject *tmp_name_name_8;
            PyObject *tmp_globals_name_8;
            PyObject *tmp_locals_name_8;
            PyObject *tmp_fromlist_name_8;
            PyObject *tmp_level_name_8;
            tmp_name_name_8 = const_str_plain_io;
            tmp_globals_name_8 = (PyObject *)moduledict_scipy$_lib$six;
            tmp_locals_name_8 = Py_None;
            tmp_fromlist_name_8 = Py_None;
            tmp_level_name_8 = const_int_0;
            frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 180;
            tmp_assign_source_86 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
            if ( tmp_assign_source_86 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_io, tmp_assign_source_86 );
        }
        {
            PyObject *tmp_assign_source_87;
            PyObject *tmp_source_name_24;
            PyObject *tmp_mvar_value_38;
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_io );

            if (unlikely( tmp_mvar_value_38 == NULL ))
            {
                tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_io );
            }

            CHECK_OBJECT( tmp_mvar_value_38 );
            tmp_source_name_24 = tmp_mvar_value_38;
            tmp_assign_source_87 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_StringIO );
            if ( tmp_assign_source_87 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 181;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_StringIO, tmp_assign_source_87 );
        }
        {
            PyObject *tmp_assign_source_88;
            PyObject *tmp_source_name_25;
            PyObject *tmp_mvar_value_39;
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_io );

            if (unlikely( tmp_mvar_value_39 == NULL ))
            {
                tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_io );
            }

            if ( tmp_mvar_value_39 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "io" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 182;

                goto frame_exception_exit_1;
            }

            tmp_source_name_25 = tmp_mvar_value_39;
            tmp_assign_source_88 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_BytesIO );
            if ( tmp_assign_source_88 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 182;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_BytesIO, tmp_assign_source_88 );
        }
        goto branch_end_17;
        branch_no_17:;
        {
            PyObject *tmp_assign_source_89;
            tmp_assign_source_89 = MAKE_FUNCTION_scipy$_lib$six$$$function_15_b(  );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_b, tmp_assign_source_89 );
        }
        {
            PyObject *tmp_assign_source_90;
            tmp_assign_source_90 = MAKE_FUNCTION_scipy$_lib$six$$$function_16_u(  );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_u, tmp_assign_source_90 );
        }
        {
            PyObject *tmp_assign_source_91;
            tmp_assign_source_91 = LOOKUP_BUILTIN( const_str_plain_chr );
            assert( tmp_assign_source_91 != NULL );
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_int2byte, tmp_assign_source_91 );
        }
        {
            PyObject *tmp_assign_source_92;
            PyObject *tmp_name_name_9;
            PyObject *tmp_globals_name_9;
            PyObject *tmp_locals_name_9;
            PyObject *tmp_fromlist_name_9;
            PyObject *tmp_level_name_9;
            tmp_name_name_9 = const_str_plain_StringIO;
            tmp_globals_name_9 = (PyObject *)moduledict_scipy$_lib$six;
            tmp_locals_name_9 = Py_None;
            tmp_fromlist_name_9 = Py_None;
            tmp_level_name_9 = const_int_0;
            frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 190;
            tmp_assign_source_92 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
            if ( tmp_assign_source_92 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_StringIO, tmp_assign_source_92 );
        }
        {
            PyObject *tmp_assign_source_93;
            PyObject *tmp_source_name_26;
            PyObject *tmp_mvar_value_40;
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_StringIO );

            if (unlikely( tmp_mvar_value_40 == NULL ))
            {
                tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_StringIO );
            }

            CHECK_OBJECT( tmp_mvar_value_40 );
            tmp_source_name_26 = tmp_mvar_value_40;
            tmp_assign_source_93 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_StringIO );
            if ( tmp_assign_source_93 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 191;

                goto frame_exception_exit_1;
            }
            assert( tmp_assign_unpack_1__assign_source == NULL );
            tmp_assign_unpack_1__assign_source = tmp_assign_source_93;
        }
        {
            PyObject *tmp_assign_source_94;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_assign_source_94 = tmp_assign_unpack_1__assign_source;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_StringIO, tmp_assign_source_94 );
        }
        {
            PyObject *tmp_assign_source_95;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_assign_source_95 = tmp_assign_unpack_1__assign_source;
            UPDATE_STRING_DICT0( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_BytesIO, tmp_assign_source_95 );
        }
        CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
        Py_DECREF( tmp_assign_unpack_1__assign_source );
        tmp_assign_unpack_1__assign_source = NULL;

        branch_end_17:;
    }
    {
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__add_doc );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__add_doc );
        }

        if ( tmp_mvar_value_41 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_add_doc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 192;

            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_41;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_b );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_b );
        }

        if ( tmp_mvar_value_42 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "b" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 192;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_7 = tmp_mvar_value_42;
        tmp_args_element_name_8 = const_str_digest_0e07ea38c4cf2fbcefc89e7de4db934c;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 192;
        {
            PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
        }

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__add_doc );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__add_doc );
        }

        if ( tmp_mvar_value_43 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_add_doc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 193;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_43;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_u );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_u );
        }

        if ( tmp_mvar_value_44 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "u" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 193;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_9 = tmp_mvar_value_44;
        tmp_args_element_name_10 = const_str_digest_ebf751844f7678efef5423dab5acd577;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 193;
        {
            PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_12, call_args );
        }

        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        nuitka_bool tmp_condition_result_23;
        PyObject *tmp_mvar_value_45;
        int tmp_truth_name_9;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        if ( tmp_mvar_value_45 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 196;

            goto frame_exception_exit_1;
        }

        tmp_truth_name_9 = CHECK_IF_TRUE( tmp_mvar_value_45 );
        if ( tmp_truth_name_9 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_23 = tmp_truth_name_9 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_19;
        }
        else
        {
            goto branch_no_19;
        }
        branch_yes_19:;
        {
            PyObject *tmp_assign_source_96;
            PyObject *tmp_name_name_10;
            PyObject *tmp_globals_name_10;
            PyObject *tmp_locals_name_10;
            PyObject *tmp_fromlist_name_10;
            PyObject *tmp_level_name_10;
            tmp_name_name_10 = const_str_plain_builtins;
            tmp_globals_name_10 = (PyObject *)moduledict_scipy$_lib$six;
            tmp_locals_name_10 = Py_None;
            tmp_fromlist_name_10 = Py_None;
            tmp_level_name_10 = const_int_0;
            frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 197;
            tmp_assign_source_96 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
            assert( !(tmp_assign_source_96 == NULL) );
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_builtins, tmp_assign_source_96 );
        }
        {
            PyObject *tmp_assign_source_97;
            PyObject *tmp_getattr_target_3;
            PyObject *tmp_mvar_value_46;
            PyObject *tmp_getattr_attr_3;
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_builtins );

            if (unlikely( tmp_mvar_value_46 == NULL ))
            {
                tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_builtins );
            }

            CHECK_OBJECT( tmp_mvar_value_46 );
            tmp_getattr_target_3 = tmp_mvar_value_46;
            tmp_getattr_attr_3 = const_str_plain_exec;
            tmp_assign_source_97 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, NULL );
            if ( tmp_assign_source_97 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 198;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_exec_, tmp_assign_source_97 );
        }
        {
            PyObject *tmp_assign_source_98;
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_assign_source_98 = MAKE_FUNCTION_scipy$_lib$six$$$function_17_reraise( tmp_defaults_1 );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_reraise, tmp_assign_source_98 );
        }
        {
            PyObject *tmp_assign_source_99;
            PyObject *tmp_getattr_target_4;
            PyObject *tmp_mvar_value_47;
            PyObject *tmp_getattr_attr_4;
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_builtins );

            if (unlikely( tmp_mvar_value_47 == NULL ))
            {
                tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_builtins );
            }

            CHECK_OBJECT( tmp_mvar_value_47 );
            tmp_getattr_target_4 = tmp_mvar_value_47;
            tmp_getattr_attr_4 = const_str_plain_print;
            tmp_assign_source_99 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, NULL );
            if ( tmp_assign_source_99 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_print_, tmp_assign_source_99 );
        }
        tmp_res = PyDict_DelItem( (PyObject *)moduledict_scipy$_lib$six, const_str_plain_builtins );
        if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

        goto branch_end_19;
        branch_no_19:;
        {
            PyObject *tmp_assign_source_100;
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_none_none_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_assign_source_100 = MAKE_FUNCTION_scipy$_lib$six$$$function_18_exec_( tmp_defaults_2 );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_exec_, tmp_assign_source_100 );
        }
        {
            PyObject *tmp_called_name_13;
            PyObject *tmp_mvar_value_48;
            PyObject *tmp_call_result_5;
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_exec_ );

            if (unlikely( tmp_mvar_value_48 == NULL ))
            {
                tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_exec_ );
            }

            CHECK_OBJECT( tmp_mvar_value_48 );
            tmp_called_name_13 = tmp_mvar_value_48;
            frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 221;
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_digest_cac36d305945ac2bbd2d1b0c1a3dadc8_tuple, 0 ) );

            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 221;

                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        {
            PyObject *tmp_assign_source_101;
            tmp_assign_source_101 = MAKE_FUNCTION_scipy$_lib$six$$$function_19_print_(  );



            UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_print_, tmp_assign_source_101 );
        }
        branch_end_19:;
    }
    {
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_args_element_name_12;
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain__add_doc );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__add_doc );
        }

        if ( tmp_mvar_value_49 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_add_doc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 271;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_49;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_reraise );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_reraise );
        }

        if ( tmp_mvar_value_50 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "reraise" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 271;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_11 = tmp_mvar_value_50;
        tmp_args_element_name_12 = const_str_digest_91987f05961643baf523130276babe51;
        frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame.f_lineno = 271;
        {
            PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12 };
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_14, call_args );
        }

        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3ee8b0c17a2ecfea3609cfca70f074b6 );
#endif
    popFrameStack();

    assertFrameObject( frame_3ee8b0c17a2ecfea3609cfca70f074b6 );

    goto frame_no_exception_3;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3ee8b0c17a2ecfea3609cfca70f074b6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3ee8b0c17a2ecfea3609cfca70f074b6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3ee8b0c17a2ecfea3609cfca70f074b6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3ee8b0c17a2ecfea3609cfca70f074b6, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_3:;
    {
        PyObject *tmp_assign_source_102;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_102 = MAKE_FUNCTION_scipy$_lib$six$$$function_20_with_metaclass( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_scipy$_lib$six, (Nuitka_StringObject *)const_str_plain_with_metaclass, tmp_assign_source_102 );
    }

    return MOD_RETURN_VALUE( module_scipy$_lib$six );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
