/* Generated code for Python module 'colorama.ansitowin32'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_colorama$ansitowin32" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_colorama$ansitowin32;
PyDictObject *moduledict_colorama$ansitowin32;

/* The declarations of module constants used, if any. */
static PyObject *const_tuple_str_plain_m_tuple_int_0_tuple_tuple;
static PyObject *const_tuple_str_plain_WinTerm_str_plain_WinColor_str_plain_WinStyle_tuple;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_tuple_str_plain_self_str_plain_text_tuple;
static PyObject *const_str_digest_8b74e46bfedfd735c1c235776b43d037;
static PyObject *const_str_plain_get_win32_calls;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_plain_ABCD;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_GREEN;
extern PyObject *const_slice_int_pos_1_none_none;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain_D;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_str_digest_12f00e9c5ea1145863283678c119e25d_tuple;
static PyObject *const_str_digest_34a67dadeab4c4d03f1d54dad88c97b1;
extern PyObject *const_str_plain_args;
static PyObject *const_str_plain_call_win32;
extern PyObject *const_str_plain_WinTerm;
extern PyObject *const_str_plain_RESET_ALL;
extern PyObject *const_str_plain_AnsiStyle;
static PyObject *const_str_plain_convert_ansi;
static PyObject *const_str_digest_928c809ee71e28d36f2148a6bed057ab;
static PyObject *const_str_plain_is_a_tty;
extern PyObject *const_str_plain_param;
static PyObject *const_str_plain_write_plain_text;
extern PyObject *const_str_plain_m;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_should_wrap;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_end;
static PyObject *const_str_plain__StreamWrapper__wrapped;
extern PyObject *const_str_plain_windll;
extern PyObject *const_str_plain_strip;
static PyObject *const_str_digest_1a5ad2f5181b31351fc00ac34c24ddab;
extern PyObject *const_tuple_str_chr_59_tuple;
extern PyObject *const_str_plain_MAGENTA;
extern PyObject *const_str_plain_command;
extern PyObject *const_str_plain_func;
extern PyObject *const_str_plain_flush;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_p_tuple;
extern PyObject *const_str_plain_winapi_test;
static PyObject *const_str_digest_7aaf004607c7f29a352ffea6f27e877f;
extern PyObject *const_str_plain_start;
static PyObject *const_str_digest_6d8abad3a3b124a7281f9dc9e1e34a11;
extern PyObject *const_str_plain_DIM;
static PyObject *const_str_plain_Hf;
extern PyObject *const_str_plain_wrapped;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_re;
static PyObject *const_tuple_5b534ed71e28174e5a83c7d90b7e2caa_tuple;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_plain_JKm;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_plain_paramstring;
extern PyObject *const_str_plain_closed;
extern PyObject *const_str_plain_GREY;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_AnsiFore;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_J;
static PyObject *const_str_digest_392147c20a5a9d9a44bc925e35734e74;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
static PyObject *const_str_plain_win32_calls;
extern PyObject *const_str_plain_p;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_style;
static PyObject *const_tuple_str_digest_fbcd3ce8b63fb326a2a412670188a257_tuple;
extern PyObject *const_str_plain_autoreset;
static PyObject *const_str_plain_ANSI_CSI_RE;
extern PyObject *const_str_plain_convert;
extern PyObject *const_str_plain_02;
static PyObject *const_tuple_8f81b70d2be11e6473ba94714a88c33f_tuple;
static PyObject *const_str_digest_7896e17c08b161dbfc0846fd3f3b6b00;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_0ba5e85965a3daf6ffa2bae2718fb27f;
extern PyObject *const_str_plain___getattr__;
static PyObject *const_str_plain_convert_osc;
extern PyObject *const_str_plain_stderr;
static PyObject *const_str_digest_f5af88de3b2f0f8929cc483ea37fac79;
extern PyObject *const_str_chr_7;
extern PyObject *const_str_plain_Style;
extern PyObject *const_str_plain_LIGHTMAGENTA_EX;
extern PyObject *const_str_plain_LIGHTBLUE_EX;
extern PyObject *const_str_plain_name;
static PyObject *const_str_digest_79cd0bf1af22d7e869007982a39be255;
extern PyObject *const_str_plain_span;
extern PyObject *const_tuple_str_plain_stream_tuple;
static PyObject *const_str_plain_on_windows;
static PyObject *const_str_plain__StreamWrapper__convertor;
extern PyObject *const_str_plain_compile;
static PyObject *const_str_plain_func_args;
extern PyObject *const_str_plain_LIGHTCYAN_EX;
static PyObject *const_tuple_29030d9ee4a795c543fbb4180730a92e_tuple;
extern PyObject *const_str_plain_LIGHTYELLOW_EX;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_match;
extern PyObject *const_tuple_int_0_tuple;
static PyObject *const_str_digest_b55785af5af936c70fc8a8934fb97656;
extern PyObject *const_str_plain_nt;
extern PyObject *const_str_plain_LIGHTRED_EX;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_WinStyle;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_stream;
extern PyObject *const_str_plain_finditer;
extern PyObject *const_str_plain_CYAN;
static PyObject *const_str_digest_12f00e9c5ea1145863283678c119e25d;
extern PyObject *const_str_plain_on_stderr;
extern PyObject *const_str_plain_K;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_RESET;
extern PyObject *const_str_plain_ansi;
static PyObject *const_str_digest_66a4513a750acaa6ee49c5cda19fde96;
static PyObject *const_str_digest_fbcd3ce8b63fb326a2a412670188a257;
extern PyObject *const_str_plain_text;
static PyObject *const_str_digest_bf3478830e517c9cf8c7c37bdb5fda5d;
static PyObject *const_str_digest_d0fbf93e4794204f0bd3840b8cf75768;
extern PyObject *const_str_plain_B;
extern PyObject *const_str_plain_origin;
extern PyObject *const_tuple_str_plain_self_str_plain_name_tuple;
static PyObject *const_str_digest_fef18de45debf62cfc3d599573cbfc03;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_x;
static PyObject *const_str_digest_d0187f6bb3b1368675fb4a5c0a71f6a7;
extern PyObject *const_str_plain_C;
extern PyObject *const_str_plain_converter;
extern PyObject *const_str_plain_WinColor;
static PyObject *const_str_digest_33d0019995e5085fa6592d50035e8a67;
static PyObject *const_str_plain_winterm;
extern PyObject *const_str_plain_LIGHTGREEN_EX;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_YELLOW;
static PyObject *const_str_digest_b554bbb6f75b8ef4430bba0e93759fc9;
static PyObject *const_tuple_str_plain_windll_str_plain_winapi_test_tuple;
extern PyObject *const_str_plain_RED;
static PyObject *const_str_plain_write_and_convert;
static PyObject *const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
static PyObject *const_tuple_str_plain_self_str_plain_text_str_plain_start_str_plain_end_tuple;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_AnsiBack;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_plain_StreamWrapper;
static PyObject *const_tuple_bb77970ac6a0b5e611d50732d417cdfb_tuple;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_params;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_erase_line;
extern PyObject *const_str_plain_LIGHTBLACK_EX;
extern PyObject *const_str_plain_y;
static PyObject *const_str_digest_890d683a23990d5f4204f0081b005a5d;
extern PyObject *const_str_plain_set_title;
extern PyObject *const_str_plain_BRIGHT;
extern PyObject *const_str_plain_NORMAL;
static PyObject *const_str_plain_extract_params;
static PyObject *const_tuple_a8fd651be59e49467b579fd304c1983a_tuple;
extern PyObject *const_str_plain_BLACK;
static PyObject *const_str_digest_23c27944ab8d207f712ce0dfec9f7e71;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_WHITE;
extern PyObject *const_str_plain_LIGHTWHITE_EX;
extern PyObject *const_str_plain_self;
static PyObject *const_tuple_54098978423f2f9d9e24124423386916_tuple;
extern PyObject *const_str_plain_groups;
static PyObject *const_tuple_str_plain_self_str_plain_wrapped_str_plain_converter_tuple;
extern PyObject *const_str_plain_write;
extern PyObject *const_str_plain_isatty;
extern PyObject *const_str_plain_cursor_adjust;
extern PyObject *const_str_plain_set_cursor_position;
extern PyObject *const_str_chr_59;
extern PyObject *const_tuple_none_none_false_tuple;
extern PyObject *const_str_plain_erase_screen;
static PyObject *const_str_plain_ANSI_OSC_RE;
static PyObject *const_str_digest_8402885c26cd327c9ea84e47155c3b69;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_reset_all;
extern PyObject *const_str_plain_A;
extern PyObject *const_str_plain_AnsiToWin32;
static PyObject *const_str_plain_is_stream_closed;
extern PyObject *const_str_plain_BLUE;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_cursor;
static PyObject *const_str_plain_conversion_supported;
extern PyObject *const_str_plain_fore;
extern PyObject *const_str_plain_win32;
extern PyObject *const_str_plain_back;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_str_plain_m_tuple_int_0_tuple_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_m_tuple_int_0_tuple_tuple, 0, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_str_plain_m_tuple_int_0_tuple_tuple, 1, const_tuple_int_0_tuple ); Py_INCREF( const_tuple_int_0_tuple );
    const_tuple_str_plain_WinTerm_str_plain_WinColor_str_plain_WinStyle_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_WinTerm_str_plain_WinColor_str_plain_WinStyle_tuple, 0, const_str_plain_WinTerm ); Py_INCREF( const_str_plain_WinTerm );
    PyTuple_SET_ITEM( const_tuple_str_plain_WinTerm_str_plain_WinColor_str_plain_WinStyle_tuple, 1, const_str_plain_WinColor ); Py_INCREF( const_str_plain_WinColor );
    PyTuple_SET_ITEM( const_tuple_str_plain_WinTerm_str_plain_WinColor_str_plain_WinStyle_tuple, 2, const_str_plain_WinStyle ); Py_INCREF( const_str_plain_WinStyle );
    const_str_digest_8b74e46bfedfd735c1c235776b43d037 = UNSTREAM_STRING_ASCII( &constant_bin[ 563015 ], 20, 0 );
    const_str_plain_get_win32_calls = UNSTREAM_STRING_ASCII( &constant_bin[ 563035 ], 15, 1 );
    const_str_plain_ABCD = UNSTREAM_STRING_ASCII( &constant_bin[ 73674 ], 4, 1 );
    const_tuple_str_digest_12f00e9c5ea1145863283678c119e25d_tuple = PyTuple_New( 1 );
    const_str_digest_12f00e9c5ea1145863283678c119e25d = UNSTREAM_STRING_ASCII( &constant_bin[ 563050 ], 28, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_12f00e9c5ea1145863283678c119e25d_tuple, 0, const_str_digest_12f00e9c5ea1145863283678c119e25d ); Py_INCREF( const_str_digest_12f00e9c5ea1145863283678c119e25d );
    const_str_digest_34a67dadeab4c4d03f1d54dad88c97b1 = UNSTREAM_STRING_ASCII( &constant_bin[ 563078 ], 26, 0 );
    const_str_plain_call_win32 = UNSTREAM_STRING_ASCII( &constant_bin[ 563104 ], 10, 1 );
    const_str_plain_convert_ansi = UNSTREAM_STRING_ASCII( &constant_bin[ 563114 ], 12, 1 );
    const_str_digest_928c809ee71e28d36f2148a6bed057ab = UNSTREAM_STRING_ASCII( &constant_bin[ 563126 ], 17, 0 );
    const_str_plain_is_a_tty = UNSTREAM_STRING_ASCII( &constant_bin[ 563143 ], 8, 1 );
    const_str_plain_write_plain_text = UNSTREAM_STRING_ASCII( &constant_bin[ 563151 ], 16, 1 );
    const_str_plain__StreamWrapper__wrapped = UNSTREAM_STRING_ASCII( &constant_bin[ 563167 ], 23, 1 );
    const_str_digest_1a5ad2f5181b31351fc00ac34c24ddab = UNSTREAM_STRING_ASCII( &constant_bin[ 563190 ], 27, 0 );
    const_str_digest_7aaf004607c7f29a352ffea6f27e877f = UNSTREAM_STRING_ASCII( &constant_bin[ 563217 ], 22, 0 );
    const_str_digest_6d8abad3a3b124a7281f9dc9e1e34a11 = UNSTREAM_STRING_ASCII( &constant_bin[ 563239 ], 45, 0 );
    const_str_plain_Hf = UNSTREAM_STRING_ASCII( &constant_bin[ 563284 ], 2, 1 );
    const_tuple_5b534ed71e28174e5a83c7d90b7e2caa_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_5b534ed71e28174e5a83c7d90b7e2caa_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_5b534ed71e28174e5a83c7d90b7e2caa_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_5b534ed71e28174e5a83c7d90b7e2caa_tuple, 2, const_str_plain_cursor ); Py_INCREF( const_str_plain_cursor );
    PyTuple_SET_ITEM( const_tuple_5b534ed71e28174e5a83c7d90b7e2caa_tuple, 3, const_str_plain_match ); Py_INCREF( const_str_plain_match );
    PyTuple_SET_ITEM( const_tuple_5b534ed71e28174e5a83c7d90b7e2caa_tuple, 4, const_str_plain_start ); Py_INCREF( const_str_plain_start );
    PyTuple_SET_ITEM( const_tuple_5b534ed71e28174e5a83c7d90b7e2caa_tuple, 5, const_str_plain_end ); Py_INCREF( const_str_plain_end );
    const_str_plain_JKm = UNSTREAM_STRING_ASCII( &constant_bin[ 563286 ], 3, 1 );
    const_str_plain_paramstring = UNSTREAM_STRING_ASCII( &constant_bin[ 563289 ], 11, 1 );
    const_str_digest_392147c20a5a9d9a44bc925e35734e74 = UNSTREAM_STRING_ASCII( &constant_bin[ 563300 ], 29, 0 );
    const_str_plain_win32_calls = UNSTREAM_STRING_ASCII( &constant_bin[ 563039 ], 11, 1 );
    const_tuple_str_digest_fbcd3ce8b63fb326a2a412670188a257_tuple = PyTuple_New( 1 );
    const_str_digest_fbcd3ce8b63fb326a2a412670188a257 = UNSTREAM_STRING_ASCII( &constant_bin[ 563329 ], 21, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_fbcd3ce8b63fb326a2a412670188a257_tuple, 0, const_str_digest_fbcd3ce8b63fb326a2a412670188a257 ); Py_INCREF( const_str_digest_fbcd3ce8b63fb326a2a412670188a257 );
    const_str_plain_ANSI_CSI_RE = UNSTREAM_STRING_ASCII( &constant_bin[ 563350 ], 11, 1 );
    const_tuple_8f81b70d2be11e6473ba94714a88c33f_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_8f81b70d2be11e6473ba94714a88c33f_tuple, 0, const_str_plain_AnsiFore ); Py_INCREF( const_str_plain_AnsiFore );
    PyTuple_SET_ITEM( const_tuple_8f81b70d2be11e6473ba94714a88c33f_tuple, 1, const_str_plain_AnsiBack ); Py_INCREF( const_str_plain_AnsiBack );
    PyTuple_SET_ITEM( const_tuple_8f81b70d2be11e6473ba94714a88c33f_tuple, 2, const_str_plain_AnsiStyle ); Py_INCREF( const_str_plain_AnsiStyle );
    PyTuple_SET_ITEM( const_tuple_8f81b70d2be11e6473ba94714a88c33f_tuple, 3, const_str_plain_Style ); Py_INCREF( const_str_plain_Style );
    const_str_digest_7896e17c08b161dbfc0846fd3f3b6b00 = UNSTREAM_STRING_ASCII( &constant_bin[ 563361 ], 19, 0 );
    const_str_digest_0ba5e85965a3daf6ffa2bae2718fb27f = UNSTREAM_STRING_ASCII( &constant_bin[ 563380 ], 20, 0 );
    const_str_plain_convert_osc = UNSTREAM_STRING_ASCII( &constant_bin[ 563400 ], 11, 1 );
    const_str_digest_f5af88de3b2f0f8929cc483ea37fac79 = UNSTREAM_STRING_ASCII( &constant_bin[ 563411 ], 362, 0 );
    const_str_digest_79cd0bf1af22d7e869007982a39be255 = UNSTREAM_STRING_ASCII( &constant_bin[ 563773 ], 23, 0 );
    const_str_plain_on_windows = UNSTREAM_STRING_ASCII( &constant_bin[ 563796 ], 10, 1 );
    const_str_plain__StreamWrapper__convertor = UNSTREAM_STRING_ASCII( &constant_bin[ 563806 ], 25, 1 );
    const_str_plain_func_args = UNSTREAM_STRING_ASCII( &constant_bin[ 563831 ], 9, 1 );
    const_tuple_29030d9ee4a795c543fbb4180730a92e_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_29030d9ee4a795c543fbb4180730a92e_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_29030d9ee4a795c543fbb4180730a92e_tuple, 1, const_str_plain_paramstring ); Py_INCREF( const_str_plain_paramstring );
    PyTuple_SET_ITEM( const_tuple_29030d9ee4a795c543fbb4180730a92e_tuple, 2, const_str_plain_command ); Py_INCREF( const_str_plain_command );
    PyTuple_SET_ITEM( const_tuple_29030d9ee4a795c543fbb4180730a92e_tuple, 3, const_str_plain_params ); Py_INCREF( const_str_plain_params );
    const_str_digest_b55785af5af936c70fc8a8934fb97656 = UNSTREAM_STRING_ASCII( &constant_bin[ 563840 ], 170, 0 );
    const_str_digest_66a4513a750acaa6ee49c5cda19fde96 = UNSTREAM_STRING_ASCII( &constant_bin[ 564010 ], 22, 0 );
    const_str_digest_bf3478830e517c9cf8c7c37bdb5fda5d = UNSTREAM_STRING_ASCII( &constant_bin[ 564032 ], 24, 0 );
    const_str_digest_d0fbf93e4794204f0bd3840b8cf75768 = UNSTREAM_STRING_ASCII( &constant_bin[ 564056 ], 29, 0 );
    const_str_digest_fef18de45debf62cfc3d599573cbfc03 = UNSTREAM_STRING_ASCII( &constant_bin[ 564085 ], 23, 0 );
    const_str_digest_d0187f6bb3b1368675fb4a5c0a71f6a7 = UNSTREAM_STRING_ASCII( &constant_bin[ 564108 ], 28, 0 );
    const_str_digest_33d0019995e5085fa6592d50035e8a67 = UNSTREAM_STRING_ASCII( &constant_bin[ 564136 ], 180, 0 );
    const_str_plain_winterm = UNSTREAM_STRING_ASCII( &constant_bin[ 564316 ], 7, 1 );
    const_str_digest_b554bbb6f75b8ef4430bba0e93759fc9 = UNSTREAM_STRING_ASCII( &constant_bin[ 564323 ], 25, 0 );
    const_tuple_str_plain_windll_str_plain_winapi_test_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_windll_str_plain_winapi_test_tuple, 0, const_str_plain_windll ); Py_INCREF( const_str_plain_windll );
    PyTuple_SET_ITEM( const_tuple_str_plain_windll_str_plain_winapi_test_tuple, 1, const_str_plain_winapi_test ); Py_INCREF( const_str_plain_winapi_test );
    const_str_plain_write_and_convert = UNSTREAM_STRING_ASCII( &constant_bin[ 563312 ], 17, 1 );
    const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple, 1, const_str_plain_wrapped ); Py_INCREF( const_str_plain_wrapped );
    PyTuple_SET_ITEM( const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple, 2, const_str_plain_convert ); Py_INCREF( const_str_plain_convert );
    PyTuple_SET_ITEM( const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple, 3, const_str_plain_strip ); Py_INCREF( const_str_plain_strip );
    PyTuple_SET_ITEM( const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple, 4, const_str_plain_autoreset ); Py_INCREF( const_str_plain_autoreset );
    PyTuple_SET_ITEM( const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple, 5, const_str_plain_on_windows ); Py_INCREF( const_str_plain_on_windows );
    const_str_plain_conversion_supported = UNSTREAM_STRING_ASCII( &constant_bin[ 564348 ], 20, 1 );
    PyTuple_SET_ITEM( const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple, 6, const_str_plain_conversion_supported ); Py_INCREF( const_str_plain_conversion_supported );
    const_tuple_str_plain_self_str_plain_text_str_plain_start_str_plain_end_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_text_str_plain_start_str_plain_end_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_text_str_plain_start_str_plain_end_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_text_str_plain_start_str_plain_end_tuple, 2, const_str_plain_start ); Py_INCREF( const_str_plain_start );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_text_str_plain_start_str_plain_end_tuple, 3, const_str_plain_end ); Py_INCREF( const_str_plain_end );
    const_str_plain_StreamWrapper = UNSTREAM_STRING_ASCII( &constant_bin[ 563168 ], 13, 1 );
    const_tuple_bb77970ac6a0b5e611d50732d417cdfb_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_bb77970ac6a0b5e611d50732d417cdfb_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_bb77970ac6a0b5e611d50732d417cdfb_tuple, 1, const_str_plain_command ); Py_INCREF( const_str_plain_command );
    PyTuple_SET_ITEM( const_tuple_bb77970ac6a0b5e611d50732d417cdfb_tuple, 2, const_str_plain_paramstring ); Py_INCREF( const_str_plain_paramstring );
    PyTuple_SET_ITEM( const_tuple_bb77970ac6a0b5e611d50732d417cdfb_tuple, 3, const_str_plain_params ); Py_INCREF( const_str_plain_params );
    const_str_digest_890d683a23990d5f4204f0081b005a5d = UNSTREAM_STRING_ASCII( &constant_bin[ 564368 ], 190, 0 );
    const_str_plain_extract_params = UNSTREAM_STRING_ASCII( &constant_bin[ 563090 ], 14, 1 );
    const_tuple_a8fd651be59e49467b579fd304c1983a_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 1, const_str_plain_command ); Py_INCREF( const_str_plain_command );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 2, const_str_plain_params ); Py_INCREF( const_str_plain_params );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 3, const_str_plain_param ); Py_INCREF( const_str_plain_param );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 4, const_str_plain_func_args ); Py_INCREF( const_str_plain_func_args );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 5, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 6, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 7, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 8, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 9, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 10, const_str_plain_y ); Py_INCREF( const_str_plain_y );
    const_str_digest_23c27944ab8d207f712ce0dfec9f7e71 = UNSTREAM_STRING_ASCII( &constant_bin[ 564558 ], 21, 0 );
    const_tuple_54098978423f2f9d9e24124423386916_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_54098978423f2f9d9e24124423386916_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_54098978423f2f9d9e24124423386916_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_54098978423f2f9d9e24124423386916_tuple, 2, const_str_plain_match ); Py_INCREF( const_str_plain_match );
    PyTuple_SET_ITEM( const_tuple_54098978423f2f9d9e24124423386916_tuple, 3, const_str_plain_start ); Py_INCREF( const_str_plain_start );
    PyTuple_SET_ITEM( const_tuple_54098978423f2f9d9e24124423386916_tuple, 4, const_str_plain_end ); Py_INCREF( const_str_plain_end );
    PyTuple_SET_ITEM( const_tuple_54098978423f2f9d9e24124423386916_tuple, 5, const_str_plain_paramstring ); Py_INCREF( const_str_plain_paramstring );
    PyTuple_SET_ITEM( const_tuple_54098978423f2f9d9e24124423386916_tuple, 6, const_str_plain_command ); Py_INCREF( const_str_plain_command );
    PyTuple_SET_ITEM( const_tuple_54098978423f2f9d9e24124423386916_tuple, 7, const_str_plain_params ); Py_INCREF( const_str_plain_params );
    const_tuple_str_plain_self_str_plain_wrapped_str_plain_converter_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_wrapped_str_plain_converter_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_wrapped_str_plain_converter_tuple, 1, const_str_plain_wrapped ); Py_INCREF( const_str_plain_wrapped );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_wrapped_str_plain_converter_tuple, 2, const_str_plain_converter ); Py_INCREF( const_str_plain_converter );
    const_str_plain_ANSI_OSC_RE = UNSTREAM_STRING_ASCII( &constant_bin[ 564579 ], 11, 1 );
    const_str_digest_8402885c26cd327c9ea84e47155c3b69 = UNSTREAM_STRING_ASCII( &constant_bin[ 564590 ], 23, 0 );
    const_str_plain_is_stream_closed = UNSTREAM_STRING_ASCII( &constant_bin[ 564613 ], 16, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_colorama$ansitowin32( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_9e58dcca515e2ebc4da0213de2a9fe04;
static PyCodeObject *codeobj_4ef184cd40fed0ed5cbd310342c675a3;
static PyCodeObject *codeobj_c7c1edf74d76a4f7d3531ba1a6f72392;
static PyCodeObject *codeobj_0bcb4b72c940cc4aca9a50383f2a0a0c;
static PyCodeObject *codeobj_10f461ff57d79775dd6ad24cd4d0fa8f;
static PyCodeObject *codeobj_8c9f73d9161c1708130582bb98a58dd0;
static PyCodeObject *codeobj_c60fd1aab1357bd227b9619135681256;
static PyCodeObject *codeobj_46d92e07eb089fe5110f225c04032098;
static PyCodeObject *codeobj_ff111fc7a144fc84db5b1deb24d971e5;
static PyCodeObject *codeobj_b9f85ad14e06fcaa2244836eea90ab9f;
static PyCodeObject *codeobj_15f0df1a6b992d1b91e723b34d2f497c;
static PyCodeObject *codeobj_115269f6e409823d899b6879fc9d15af;
static PyCodeObject *codeobj_a003ea1c36c39fae1e7c9dec04eef415;
static PyCodeObject *codeobj_f21898405dd391350f5af25b32d482a1;
static PyCodeObject *codeobj_6cb02b7c3356f7c1c68a77ab56b4e949;
static PyCodeObject *codeobj_cf5052a1b20aa30dd29b919afb9d8888;
static PyCodeObject *codeobj_e03e253bce6ff320b3eabc6ec6832bad;
static PyCodeObject *codeobj_0222d6ff7ea866c930b0d1302657f92e;
static PyCodeObject *codeobj_d0ef161b5c50b704b24c67a4dd1d1adb;
static PyCodeObject *codeobj_4da7e8e481330a4b56a18a75ee8093f4;
static PyCodeObject *codeobj_94899803123637240dcec2c2d73832e4;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_79cd0bf1af22d7e869007982a39be255 );
    codeobj_9e58dcca515e2ebc4da0213de2a9fe04 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 186, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_p_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4ef184cd40fed0ed5cbd310342c675a3 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 191, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_p_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c7c1edf74d76a4f7d3531ba1a6f72392 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_d0fbf93e4794204f0bd3840b8cf75768, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_0bcb4b72c940cc4aca9a50383f2a0a0c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AnsiToWin32, 43, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_10f461ff57d79775dd6ad24cd4d0fa8f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_StreamWrapper, 24, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_8c9f73d9161c1708130582bb98a58dd0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___getattr__, 36, const_tuple_str_plain_self_str_plain_name_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c60fd1aab1357bd227b9619135681256 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 52, const_tuple_1504142ac50c53203f0d2c502b6a6376_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_46d92e07eb089fe5110f225c04032098 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 30, const_tuple_str_plain_self_str_plain_wrapped_str_plain_converter_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ff111fc7a144fc84db5b1deb24d971e5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_call_win32, 202, const_tuple_a8fd651be59e49467b579fd304c1983a_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b9f85ad14e06fcaa2244836eea90ab9f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_convert_ansi, 178, const_tuple_29030d9ee4a795c543fbb4180730a92e_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_15f0df1a6b992d1b91e723b34d2f497c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_convert_osc, 224, const_tuple_54098978423f2f9d9e24124423386916_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_115269f6e409823d899b6879fc9d15af = MAKE_CODEOBJ( module_filename_obj, const_str_plain_extract_params, 184, const_tuple_bb77970ac6a0b5e611d50732d417cdfb_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a003ea1c36c39fae1e7c9dec04eef415 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_win32_calls, 95, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f21898405dd391350f5af25b32d482a1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_is_a_tty, 20, const_tuple_str_plain_stream_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6cb02b7c3356f7c1c68a77ab56b4e949 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_is_stream_closed, 16, const_tuple_str_plain_stream_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cf5052a1b20aa30dd29b919afb9d8888 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reset_all, 149, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e03e253bce6ff320b3eabc6ec6832bad = MAKE_CODEOBJ( module_filename_obj, const_str_plain_should_wrap, 85, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0222d6ff7ea866c930b0d1302657f92e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_write, 39, const_tuple_str_plain_self_str_plain_text_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d0ef161b5c50b704b24c67a4dd1d1adb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_write, 139, const_tuple_str_plain_self_str_plain_text_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4da7e8e481330a4b56a18a75ee8093f4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_write_and_convert, 156, const_tuple_5b534ed71e28174e5a83c7d90b7e2caa_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_94899803123637240dcec2c2d73832e4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_write_plain_text, 172, const_tuple_str_plain_self_str_plain_text_str_plain_start_str_plain_end_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *colorama$ansitowin32$$$function_14_extract_params$$$genexpr_1_genexpr_maker( void );


static PyObject *colorama$ansitowin32$$$function_14_extract_params$$$genexpr_2_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_4_complex_call_helper_star_list( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_10_reset_all(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_11_write_and_convert(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_12_write_plain_text(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_13_convert_ansi(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_14_extract_params(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_15_call_win32(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_16_convert_osc(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_1_is_stream_closed(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_2_is_a_tty(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_3___init__(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_4___getattr__(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_5_write(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_6___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_7_should_wrap(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_8_get_win32_calls(  );


static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_9_write(  );


// The module function definitions.
static PyObject *impl_colorama$ansitowin32$$$function_1_is_stream_closed( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stream = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6cb02b7c3356f7c1c68a77ab56b4e949;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6cb02b7c3356f7c1c68a77ab56b4e949 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6cb02b7c3356f7c1c68a77ab56b4e949, codeobj_6cb02b7c3356f7c1c68a77ab56b4e949, module_colorama$ansitowin32, sizeof(void *) );
    frame_6cb02b7c3356f7c1c68a77ab56b4e949 = cache_frame_6cb02b7c3356f7c1c68a77ab56b4e949;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6cb02b7c3356f7c1c68a77ab56b4e949 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6cb02b7c3356f7c1c68a77ab56b4e949 ) == 2 ); // Frame stack

    // Framed code:
    {
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_stream );
        tmp_source_name_1 = par_stream;
        tmp_attribute_name_1 = const_str_plain_closed;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = ( tmp_res == 0 ) ? Py_True : Py_False;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( par_stream );
        tmp_source_name_2 = par_stream;
        tmp_or_right_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_closed );
        if ( tmp_or_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_return_value = tmp_or_left_value_1;
        or_end_1:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6cb02b7c3356f7c1c68a77ab56b4e949 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6cb02b7c3356f7c1c68a77ab56b4e949 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6cb02b7c3356f7c1c68a77ab56b4e949 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6cb02b7c3356f7c1c68a77ab56b4e949, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6cb02b7c3356f7c1c68a77ab56b4e949->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6cb02b7c3356f7c1c68a77ab56b4e949, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6cb02b7c3356f7c1c68a77ab56b4e949,
        type_description_1,
        par_stream
    );


    // Release cached frame.
    if ( frame_6cb02b7c3356f7c1c68a77ab56b4e949 == cache_frame_6cb02b7c3356f7c1c68a77ab56b4e949 )
    {
        Py_DECREF( frame_6cb02b7c3356f7c1c68a77ab56b4e949 );
    }
    cache_frame_6cb02b7c3356f7c1c68a77ab56b4e949 = NULL;

    assertFrameObject( frame_6cb02b7c3356f7c1c68a77ab56b4e949 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_1_is_stream_closed );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_1_is_stream_closed );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_2_is_a_tty( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stream = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_f21898405dd391350f5af25b32d482a1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f21898405dd391350f5af25b32d482a1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f21898405dd391350f5af25b32d482a1, codeobj_f21898405dd391350f5af25b32d482a1, module_colorama$ansitowin32, sizeof(void *) );
    frame_f21898405dd391350f5af25b32d482a1 = cache_frame_f21898405dd391350f5af25b32d482a1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f21898405dd391350f5af25b32d482a1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f21898405dd391350f5af25b32d482a1 ) == 2 ); // Frame stack

    // Framed code:
    {
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_name_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_stream );
        tmp_source_name_1 = par_stream;
        tmp_attribute_name_1 = const_str_plain_isatty;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_stream );
        tmp_called_instance_1 = par_stream;
        frame_f21898405dd391350f5af25b32d482a1->m_frame.f_lineno = 21;
        tmp_and_right_value_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_isatty );
        if ( tmp_and_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        Py_INCREF( tmp_and_left_value_1 );
        tmp_return_value = tmp_and_left_value_1;
        and_end_1:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f21898405dd391350f5af25b32d482a1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f21898405dd391350f5af25b32d482a1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f21898405dd391350f5af25b32d482a1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f21898405dd391350f5af25b32d482a1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f21898405dd391350f5af25b32d482a1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f21898405dd391350f5af25b32d482a1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f21898405dd391350f5af25b32d482a1,
        type_description_1,
        par_stream
    );


    // Release cached frame.
    if ( frame_f21898405dd391350f5af25b32d482a1 == cache_frame_f21898405dd391350f5af25b32d482a1 )
    {
        Py_DECREF( frame_f21898405dd391350f5af25b32d482a1 );
    }
    cache_frame_f21898405dd391350f5af25b32d482a1 = NULL;

    assertFrameObject( frame_f21898405dd391350f5af25b32d482a1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_2_is_a_tty );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stream );
    Py_DECREF( par_stream );
    par_stream = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_2_is_a_tty );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_3___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_wrapped = python_pars[ 1 ];
    PyObject *par_converter = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_46d92e07eb089fe5110f225c04032098;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_46d92e07eb089fe5110f225c04032098 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_46d92e07eb089fe5110f225c04032098, codeobj_46d92e07eb089fe5110f225c04032098, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_46d92e07eb089fe5110f225c04032098 = cache_frame_46d92e07eb089fe5110f225c04032098;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_46d92e07eb089fe5110f225c04032098 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_46d92e07eb089fe5110f225c04032098 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_wrapped );
        tmp_assattr_name_1 = par_wrapped;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__StreamWrapper__wrapped, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_converter );
        tmp_assattr_name_2 = par_converter;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__StreamWrapper__convertor, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_46d92e07eb089fe5110f225c04032098 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_46d92e07eb089fe5110f225c04032098 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_46d92e07eb089fe5110f225c04032098, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_46d92e07eb089fe5110f225c04032098->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_46d92e07eb089fe5110f225c04032098, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_46d92e07eb089fe5110f225c04032098,
        type_description_1,
        par_self,
        par_wrapped,
        par_converter
    );


    // Release cached frame.
    if ( frame_46d92e07eb089fe5110f225c04032098 == cache_frame_46d92e07eb089fe5110f225c04032098 )
    {
        Py_DECREF( frame_46d92e07eb089fe5110f225c04032098 );
    }
    cache_frame_46d92e07eb089fe5110f225c04032098 = NULL;

    assertFrameObject( frame_46d92e07eb089fe5110f225c04032098 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_3___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_wrapped );
    Py_DECREF( par_wrapped );
    par_wrapped = NULL;

    CHECK_OBJECT( (PyObject *)par_converter );
    Py_DECREF( par_converter );
    par_converter = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_wrapped );
    Py_DECREF( par_wrapped );
    par_wrapped = NULL;

    CHECK_OBJECT( (PyObject *)par_converter );
    Py_DECREF( par_converter );
    par_converter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_3___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_4___getattr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_8c9f73d9161c1708130582bb98a58dd0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8c9f73d9161c1708130582bb98a58dd0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8c9f73d9161c1708130582bb98a58dd0, codeobj_8c9f73d9161c1708130582bb98a58dd0, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *) );
    frame_8c9f73d9161c1708130582bb98a58dd0 = cache_frame_8c9f73d9161c1708130582bb98a58dd0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8c9f73d9161c1708130582bb98a58dd0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8c9f73d9161c1708130582bb98a58dd0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_getattr_attr_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_getattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__StreamWrapper__wrapped );
        if ( tmp_getattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_getattr_attr_1 = par_name;
        tmp_return_value = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        Py_DECREF( tmp_getattr_target_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c9f73d9161c1708130582bb98a58dd0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c9f73d9161c1708130582bb98a58dd0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c9f73d9161c1708130582bb98a58dd0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8c9f73d9161c1708130582bb98a58dd0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8c9f73d9161c1708130582bb98a58dd0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8c9f73d9161c1708130582bb98a58dd0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8c9f73d9161c1708130582bb98a58dd0,
        type_description_1,
        par_self,
        par_name
    );


    // Release cached frame.
    if ( frame_8c9f73d9161c1708130582bb98a58dd0 == cache_frame_8c9f73d9161c1708130582bb98a58dd0 )
    {
        Py_DECREF( frame_8c9f73d9161c1708130582bb98a58dd0 );
    }
    cache_frame_8c9f73d9161c1708130582bb98a58dd0 = NULL;

    assertFrameObject( frame_8c9f73d9161c1708130582bb98a58dd0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_4___getattr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_4___getattr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_5_write( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_0222d6ff7ea866c930b0d1302657f92e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0222d6ff7ea866c930b0d1302657f92e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0222d6ff7ea866c930b0d1302657f92e, codeobj_0222d6ff7ea866c930b0d1302657f92e, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *) );
    frame_0222d6ff7ea866c930b0d1302657f92e = cache_frame_0222d6ff7ea866c930b0d1302657f92e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0222d6ff7ea866c930b0d1302657f92e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0222d6ff7ea866c930b0d1302657f92e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__StreamWrapper__convertor );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_0222d6ff7ea866c930b0d1302657f92e->m_frame.f_lineno = 40;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_write, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0222d6ff7ea866c930b0d1302657f92e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0222d6ff7ea866c930b0d1302657f92e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0222d6ff7ea866c930b0d1302657f92e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0222d6ff7ea866c930b0d1302657f92e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0222d6ff7ea866c930b0d1302657f92e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0222d6ff7ea866c930b0d1302657f92e,
        type_description_1,
        par_self,
        par_text
    );


    // Release cached frame.
    if ( frame_0222d6ff7ea866c930b0d1302657f92e == cache_frame_0222d6ff7ea866c930b0d1302657f92e )
    {
        Py_DECREF( frame_0222d6ff7ea866c930b0d1302657f92e );
    }
    cache_frame_0222d6ff7ea866c930b0d1302657f92e = NULL;

    assertFrameObject( frame_0222d6ff7ea866c930b0d1302657f92e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_5_write );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_5_write );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_6___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_wrapped = python_pars[ 1 ];
    PyObject *par_convert = python_pars[ 2 ];
    PyObject *par_strip = python_pars[ 3 ];
    PyObject *par_autoreset = python_pars[ 4 ];
    PyObject *var_on_windows = NULL;
    PyObject *var_conversion_supported = NULL;
    struct Nuitka_FrameObject *frame_c60fd1aab1357bd227b9619135681256;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_c60fd1aab1357bd227b9619135681256 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c60fd1aab1357bd227b9619135681256, codeobj_c60fd1aab1357bd227b9619135681256, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c60fd1aab1357bd227b9619135681256 = cache_frame_c60fd1aab1357bd227b9619135681256;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c60fd1aab1357bd227b9619135681256 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c60fd1aab1357bd227b9619135681256 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_wrapped );
        tmp_assattr_name_1 = par_wrapped;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_wrapped, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_autoreset );
        tmp_assattr_name_2 = par_autoreset;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_autoreset, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_assattr_target_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_StreamWrapper );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_StreamWrapper );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "StreamWrapper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_wrapped );
        tmp_args_element_name_1 = par_wrapped;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_2 = par_self;
        frame_c60fd1aab1357bd227b9619135681256->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assattr_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_stream, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_2;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_name );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_nt;
        tmp_assign_source_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_on_windows == NULL );
        var_on_windows = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        CHECK_OBJECT( var_on_windows );
        tmp_and_left_value_1 = var_on_windows;
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winapi_test );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winapi_test );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winapi_test" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        frame_c60fd1aab1357bd227b9619135681256->m_frame.f_lineno = 67;
        tmp_and_right_value_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_and_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        Py_INCREF( tmp_and_left_value_1 );
        tmp_assign_source_2 = tmp_and_left_value_1;
        and_end_1:;
        assert( var_conversion_supported == NULL );
        var_conversion_supported = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_strip );
        tmp_compexpr_left_2 = par_strip;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            int tmp_or_left_truth_1;
            PyObject *tmp_or_left_value_1;
            PyObject *tmp_or_right_value_1;
            int tmp_and_left_truth_2;
            PyObject *tmp_and_left_value_2;
            PyObject *tmp_and_right_value_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( var_conversion_supported );
            tmp_or_left_value_1 = var_conversion_supported;
            tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
            if ( tmp_or_left_truth_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            if ( tmp_or_left_truth_1 == 1 )
            {
                goto or_left_1;
            }
            else
            {
                goto or_right_1;
            }
            or_right_1:;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_is_stream_closed );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_stream_closed );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_stream_closed" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 71;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_3 = tmp_mvar_value_4;
            CHECK_OBJECT( par_wrapped );
            tmp_args_element_name_3 = par_wrapped;
            frame_c60fd1aab1357bd227b9619135681256->m_frame.f_lineno = 71;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_2 = ( tmp_res == 0 ) ? Py_True : Py_False;
            tmp_and_left_truth_2 = CHECK_IF_TRUE( tmp_and_left_value_2 );
            if ( tmp_and_left_truth_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            if ( tmp_and_left_truth_2 == 1 )
            {
                goto and_right_2;
            }
            else
            {
                goto and_left_2;
            }
            and_right_2:;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_is_a_tty );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_a_tty );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_a_tty" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 71;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_4 = tmp_mvar_value_5;
            CHECK_OBJECT( par_wrapped );
            tmp_args_element_name_4 = par_wrapped;
            frame_c60fd1aab1357bd227b9619135681256->m_frame.f_lineno = 71;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_operand_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            if ( tmp_operand_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            Py_DECREF( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_2 = ( tmp_res == 0 ) ? Py_True : Py_False;
            tmp_or_right_value_1 = tmp_and_right_value_2;
            goto and_end_2;
            and_left_2:;
            tmp_or_right_value_1 = tmp_and_left_value_2;
            and_end_2:;
            tmp_assign_source_3 = tmp_or_right_value_1;
            goto or_end_1;
            or_left_1:;
            tmp_assign_source_3 = tmp_or_left_value_1;
            or_end_1:;
            {
                PyObject *old = par_strip;
                assert( old != NULL );
                par_strip = tmp_assign_source_3;
                Py_INCREF( par_strip );
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        CHECK_OBJECT( par_strip );
        tmp_assattr_name_4 = par_strip;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_strip, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        CHECK_OBJECT( par_convert );
        tmp_compexpr_left_3 = par_convert;
        tmp_compexpr_right_3 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_4;
            int tmp_and_left_truth_3;
            PyObject *tmp_and_left_value_3;
            PyObject *tmp_and_right_value_3;
            int tmp_and_left_truth_4;
            PyObject *tmp_and_left_value_4;
            PyObject *tmp_and_right_value_4;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_called_name_6;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( var_conversion_supported );
            tmp_and_left_value_3 = var_conversion_supported;
            tmp_and_left_truth_3 = CHECK_IF_TRUE( tmp_and_left_value_3 );
            if ( tmp_and_left_truth_3 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            if ( tmp_and_left_truth_3 == 1 )
            {
                goto and_right_3;
            }
            else
            {
                goto and_left_3;
            }
            and_right_3:;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_is_stream_closed );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_stream_closed );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_stream_closed" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 76;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_5 = tmp_mvar_value_6;
            CHECK_OBJECT( par_wrapped );
            tmp_args_element_name_5 = par_wrapped;
            frame_c60fd1aab1357bd227b9619135681256->m_frame.f_lineno = 76;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_operand_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            if ( tmp_operand_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            Py_DECREF( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_4 = ( tmp_res == 0 ) ? Py_True : Py_False;
            tmp_and_left_truth_4 = CHECK_IF_TRUE( tmp_and_left_value_4 );
            if ( tmp_and_left_truth_4 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            if ( tmp_and_left_truth_4 == 1 )
            {
                goto and_right_4;
            }
            else
            {
                goto and_left_4;
            }
            and_right_4:;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_is_a_tty );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_a_tty );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_a_tty" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 76;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_6 = tmp_mvar_value_7;
            CHECK_OBJECT( par_wrapped );
            tmp_args_element_name_6 = par_wrapped;
            frame_c60fd1aab1357bd227b9619135681256->m_frame.f_lineno = 76;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_and_right_value_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            if ( tmp_and_right_value_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_3 = tmp_and_right_value_4;
            goto and_end_4;
            and_left_4:;
            Py_INCREF( tmp_and_left_value_4 );
            tmp_and_right_value_3 = tmp_and_left_value_4;
            and_end_4:;
            tmp_assign_source_4 = tmp_and_right_value_3;
            goto and_end_3;
            and_left_3:;
            Py_INCREF( tmp_and_left_value_3 );
            tmp_assign_source_4 = tmp_and_left_value_3;
            and_end_3:;
            {
                PyObject *old = par_convert;
                assert( old != NULL );
                par_convert = tmp_assign_source_4;
                Py_DECREF( old );
            }

        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_assattr_target_5;
        CHECK_OBJECT( par_convert );
        tmp_assattr_name_5 = par_convert;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_convert, tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_assattr_target_6;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_c60fd1aab1357bd227b9619135681256->m_frame.f_lineno = 80;
        tmp_assattr_name_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_win32_calls );
        if ( tmp_assattr_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_win32_calls, tmp_assattr_name_6 );
        Py_DECREF( tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_assattr_target_7;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_wrapped );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_compexpr_left_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_8;
        tmp_compexpr_right_4 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_stderr );
        if ( tmp_compexpr_right_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_4 );

            exception_lineno = 83;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_7 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? Py_True : Py_False;
        Py_DECREF( tmp_compexpr_left_4 );
        Py_DECREF( tmp_compexpr_right_4 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_on_stderr, tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c60fd1aab1357bd227b9619135681256 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c60fd1aab1357bd227b9619135681256 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c60fd1aab1357bd227b9619135681256, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c60fd1aab1357bd227b9619135681256->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c60fd1aab1357bd227b9619135681256, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c60fd1aab1357bd227b9619135681256,
        type_description_1,
        par_self,
        par_wrapped,
        par_convert,
        par_strip,
        par_autoreset,
        var_on_windows,
        var_conversion_supported
    );


    // Release cached frame.
    if ( frame_c60fd1aab1357bd227b9619135681256 == cache_frame_c60fd1aab1357bd227b9619135681256 )
    {
        Py_DECREF( frame_c60fd1aab1357bd227b9619135681256 );
    }
    cache_frame_c60fd1aab1357bd227b9619135681256 = NULL;

    assertFrameObject( frame_c60fd1aab1357bd227b9619135681256 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_6___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_wrapped );
    Py_DECREF( par_wrapped );
    par_wrapped = NULL;

    CHECK_OBJECT( (PyObject *)par_convert );
    Py_DECREF( par_convert );
    par_convert = NULL;

    CHECK_OBJECT( (PyObject *)par_strip );
    Py_DECREF( par_strip );
    par_strip = NULL;

    CHECK_OBJECT( (PyObject *)par_autoreset );
    Py_DECREF( par_autoreset );
    par_autoreset = NULL;

    CHECK_OBJECT( (PyObject *)var_on_windows );
    Py_DECREF( var_on_windows );
    var_on_windows = NULL;

    CHECK_OBJECT( (PyObject *)var_conversion_supported );
    Py_DECREF( var_conversion_supported );
    var_conversion_supported = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_wrapped );
    Py_DECREF( par_wrapped );
    par_wrapped = NULL;

    Py_XDECREF( par_convert );
    par_convert = NULL;

    Py_XDECREF( par_strip );
    par_strip = NULL;

    CHECK_OBJECT( (PyObject *)par_autoreset );
    Py_DECREF( par_autoreset );
    par_autoreset = NULL;

    Py_XDECREF( var_on_windows );
    var_on_windows = NULL;

    Py_XDECREF( var_conversion_supported );
    var_conversion_supported = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_6___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_7_should_wrap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_e03e253bce6ff320b3eabc6ec6832bad;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e03e253bce6ff320b3eabc6ec6832bad = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e03e253bce6ff320b3eabc6ec6832bad, codeobj_e03e253bce6ff320b3eabc6ec6832bad, module_colorama$ansitowin32, sizeof(void *) );
    frame_e03e253bce6ff320b3eabc6ec6832bad = cache_frame_e03e253bce6ff320b3eabc6ec6832bad;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e03e253bce6ff320b3eabc6ec6832bad );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e03e253bce6ff320b3eabc6ec6832bad ) == 2 ); // Frame stack

    // Framed code:
    {
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_source_name_1;
        int tmp_or_left_truth_2;
        PyObject *tmp_or_left_value_2;
        PyObject *tmp_or_right_value_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_or_left_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_convert );
        if ( tmp_or_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_or_left_value_1 );

            exception_lineno = 93;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        Py_DECREF( tmp_or_left_value_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_or_left_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_strip );
        if ( tmp_or_left_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
        if ( tmp_or_left_truth_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_or_left_value_2 );

            exception_lineno = 93;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_2 == 1 )
        {
            goto or_left_2;
        }
        else
        {
            goto or_right_2;
        }
        or_right_2:;
        Py_DECREF( tmp_or_left_value_2 );
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_or_right_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_autoreset );
        if ( tmp_or_right_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = tmp_or_right_value_2;
        goto or_end_2;
        or_left_2:;
        tmp_or_right_value_1 = tmp_or_left_value_2;
        or_end_2:;
        tmp_return_value = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_return_value = tmp_or_left_value_1;
        or_end_1:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e03e253bce6ff320b3eabc6ec6832bad );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e03e253bce6ff320b3eabc6ec6832bad );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e03e253bce6ff320b3eabc6ec6832bad );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e03e253bce6ff320b3eabc6ec6832bad, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e03e253bce6ff320b3eabc6ec6832bad->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e03e253bce6ff320b3eabc6ec6832bad, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e03e253bce6ff320b3eabc6ec6832bad,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_e03e253bce6ff320b3eabc6ec6832bad == cache_frame_e03e253bce6ff320b3eabc6ec6832bad )
    {
        Py_DECREF( frame_e03e253bce6ff320b3eabc6ec6832bad );
    }
    cache_frame_e03e253bce6ff320b3eabc6ec6832bad = NULL;

    assertFrameObject( frame_e03e253bce6ff320b3eabc6ec6832bad );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_7_should_wrap );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_7_should_wrap );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_8_get_win32_calls( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a003ea1c36c39fae1e7c9dec04eef415;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_a003ea1c36c39fae1e7c9dec04eef415 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a003ea1c36c39fae1e7c9dec04eef415, codeobj_a003ea1c36c39fae1e7c9dec04eef415, module_colorama$ansitowin32, sizeof(void *) );
    frame_a003ea1c36c39fae1e7c9dec04eef415 = cache_frame_a003ea1c36c39fae1e7c9dec04eef415;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a003ea1c36c39fae1e7c9dec04eef415 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a003ea1c36c39fae1e7c9dec04eef415 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_mvar_value_1;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_convert );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 96;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_mvar_value_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_source_name_6;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_source_name_8;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_source_name_9;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_dict_key_4;
            PyObject *tmp_dict_value_4;
            PyObject *tmp_source_name_10;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_source_name_11;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_source_name_12;
            PyObject *tmp_mvar_value_12;
            PyObject *tmp_dict_key_5;
            PyObject *tmp_dict_value_5;
            PyObject *tmp_source_name_13;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_source_name_14;
            PyObject *tmp_mvar_value_14;
            PyObject *tmp_source_name_15;
            PyObject *tmp_mvar_value_15;
            PyObject *tmp_dict_key_6;
            PyObject *tmp_dict_value_6;
            PyObject *tmp_source_name_16;
            PyObject *tmp_mvar_value_16;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_source_name_17;
            PyObject *tmp_mvar_value_17;
            PyObject *tmp_source_name_18;
            PyObject *tmp_mvar_value_18;
            PyObject *tmp_dict_key_7;
            PyObject *tmp_dict_value_7;
            PyObject *tmp_source_name_19;
            PyObject *tmp_mvar_value_19;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_source_name_20;
            PyObject *tmp_mvar_value_20;
            PyObject *tmp_source_name_21;
            PyObject *tmp_mvar_value_21;
            PyObject *tmp_dict_key_8;
            PyObject *tmp_dict_value_8;
            PyObject *tmp_source_name_22;
            PyObject *tmp_mvar_value_22;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_source_name_23;
            PyObject *tmp_mvar_value_23;
            PyObject *tmp_source_name_24;
            PyObject *tmp_mvar_value_24;
            PyObject *tmp_dict_key_9;
            PyObject *tmp_dict_value_9;
            PyObject *tmp_source_name_25;
            PyObject *tmp_mvar_value_25;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_source_name_26;
            PyObject *tmp_mvar_value_26;
            PyObject *tmp_source_name_27;
            PyObject *tmp_mvar_value_27;
            PyObject *tmp_dict_key_10;
            PyObject *tmp_dict_value_10;
            PyObject *tmp_source_name_28;
            PyObject *tmp_mvar_value_28;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_source_name_29;
            PyObject *tmp_mvar_value_29;
            PyObject *tmp_source_name_30;
            PyObject *tmp_mvar_value_30;
            PyObject *tmp_dict_key_11;
            PyObject *tmp_dict_value_11;
            PyObject *tmp_source_name_31;
            PyObject *tmp_mvar_value_31;
            PyObject *tmp_tuple_element_11;
            PyObject *tmp_source_name_32;
            PyObject *tmp_mvar_value_32;
            PyObject *tmp_source_name_33;
            PyObject *tmp_mvar_value_33;
            PyObject *tmp_dict_key_12;
            PyObject *tmp_dict_value_12;
            PyObject *tmp_source_name_34;
            PyObject *tmp_mvar_value_34;
            PyObject *tmp_tuple_element_12;
            PyObject *tmp_source_name_35;
            PyObject *tmp_mvar_value_35;
            PyObject *tmp_source_name_36;
            PyObject *tmp_mvar_value_36;
            PyObject *tmp_dict_key_13;
            PyObject *tmp_dict_value_13;
            PyObject *tmp_source_name_37;
            PyObject *tmp_mvar_value_37;
            PyObject *tmp_tuple_element_13;
            PyObject *tmp_source_name_38;
            PyObject *tmp_mvar_value_38;
            PyObject *tmp_dict_key_14;
            PyObject *tmp_dict_value_14;
            PyObject *tmp_source_name_39;
            PyObject *tmp_mvar_value_39;
            PyObject *tmp_tuple_element_14;
            PyObject *tmp_source_name_40;
            PyObject *tmp_mvar_value_40;
            PyObject *tmp_source_name_41;
            PyObject *tmp_mvar_value_41;
            PyObject *tmp_dict_key_15;
            PyObject *tmp_dict_value_15;
            PyObject *tmp_source_name_42;
            PyObject *tmp_mvar_value_42;
            PyObject *tmp_tuple_element_15;
            PyObject *tmp_source_name_43;
            PyObject *tmp_mvar_value_43;
            PyObject *tmp_source_name_44;
            PyObject *tmp_mvar_value_44;
            PyObject *tmp_dict_key_16;
            PyObject *tmp_dict_value_16;
            PyObject *tmp_source_name_45;
            PyObject *tmp_mvar_value_45;
            PyObject *tmp_tuple_element_16;
            PyObject *tmp_source_name_46;
            PyObject *tmp_mvar_value_46;
            PyObject *tmp_source_name_47;
            PyObject *tmp_mvar_value_47;
            PyObject *tmp_dict_key_17;
            PyObject *tmp_dict_value_17;
            PyObject *tmp_source_name_48;
            PyObject *tmp_mvar_value_48;
            PyObject *tmp_tuple_element_17;
            PyObject *tmp_source_name_49;
            PyObject *tmp_mvar_value_49;
            PyObject *tmp_source_name_50;
            PyObject *tmp_mvar_value_50;
            PyObject *tmp_dict_key_18;
            PyObject *tmp_dict_value_18;
            PyObject *tmp_source_name_51;
            PyObject *tmp_mvar_value_51;
            PyObject *tmp_tuple_element_18;
            PyObject *tmp_source_name_52;
            PyObject *tmp_mvar_value_52;
            PyObject *tmp_source_name_53;
            PyObject *tmp_mvar_value_53;
            PyObject *tmp_dict_key_19;
            PyObject *tmp_dict_value_19;
            PyObject *tmp_source_name_54;
            PyObject *tmp_mvar_value_54;
            PyObject *tmp_tuple_element_19;
            PyObject *tmp_source_name_55;
            PyObject *tmp_mvar_value_55;
            PyObject *tmp_source_name_56;
            PyObject *tmp_mvar_value_56;
            PyObject *tmp_dict_key_20;
            PyObject *tmp_dict_value_20;
            PyObject *tmp_source_name_57;
            PyObject *tmp_mvar_value_57;
            PyObject *tmp_tuple_element_20;
            PyObject *tmp_source_name_58;
            PyObject *tmp_mvar_value_58;
            PyObject *tmp_source_name_59;
            PyObject *tmp_mvar_value_59;
            PyObject *tmp_dict_key_21;
            PyObject *tmp_dict_value_21;
            PyObject *tmp_source_name_60;
            PyObject *tmp_mvar_value_60;
            PyObject *tmp_tuple_element_21;
            PyObject *tmp_source_name_61;
            PyObject *tmp_mvar_value_61;
            PyObject *tmp_source_name_62;
            PyObject *tmp_mvar_value_62;
            PyObject *tmp_dict_key_22;
            PyObject *tmp_dict_value_22;
            PyObject *tmp_source_name_63;
            PyObject *tmp_mvar_value_63;
            PyObject *tmp_tuple_element_22;
            PyObject *tmp_source_name_64;
            PyObject *tmp_mvar_value_64;
            PyObject *tmp_source_name_65;
            PyObject *tmp_mvar_value_65;
            PyObject *tmp_dict_key_23;
            PyObject *tmp_dict_value_23;
            PyObject *tmp_source_name_66;
            PyObject *tmp_mvar_value_66;
            PyObject *tmp_tuple_element_23;
            PyObject *tmp_source_name_67;
            PyObject *tmp_mvar_value_67;
            PyObject *tmp_source_name_68;
            PyObject *tmp_mvar_value_68;
            PyObject *tmp_dict_key_24;
            PyObject *tmp_dict_value_24;
            PyObject *tmp_source_name_69;
            PyObject *tmp_mvar_value_69;
            PyObject *tmp_tuple_element_24;
            PyObject *tmp_source_name_70;
            PyObject *tmp_mvar_value_70;
            PyObject *tmp_source_name_71;
            PyObject *tmp_mvar_value_71;
            PyObject *tmp_dict_key_25;
            PyObject *tmp_dict_value_25;
            PyObject *tmp_source_name_72;
            PyObject *tmp_mvar_value_72;
            PyObject *tmp_tuple_element_25;
            PyObject *tmp_source_name_73;
            PyObject *tmp_mvar_value_73;
            PyObject *tmp_source_name_74;
            PyObject *tmp_mvar_value_74;
            PyObject *tmp_dict_key_26;
            PyObject *tmp_dict_value_26;
            PyObject *tmp_source_name_75;
            PyObject *tmp_mvar_value_75;
            PyObject *tmp_tuple_element_26;
            PyObject *tmp_source_name_76;
            PyObject *tmp_mvar_value_76;
            PyObject *tmp_source_name_77;
            PyObject *tmp_mvar_value_77;
            PyObject *tmp_dict_key_27;
            PyObject *tmp_dict_value_27;
            PyObject *tmp_source_name_78;
            PyObject *tmp_mvar_value_78;
            PyObject *tmp_tuple_element_27;
            PyObject *tmp_source_name_79;
            PyObject *tmp_mvar_value_79;
            PyObject *tmp_source_name_80;
            PyObject *tmp_mvar_value_80;
            PyObject *tmp_dict_key_28;
            PyObject *tmp_dict_value_28;
            PyObject *tmp_source_name_81;
            PyObject *tmp_mvar_value_81;
            PyObject *tmp_tuple_element_28;
            PyObject *tmp_source_name_82;
            PyObject *tmp_mvar_value_82;
            PyObject *tmp_source_name_83;
            PyObject *tmp_mvar_value_83;
            PyObject *tmp_dict_key_29;
            PyObject *tmp_dict_value_29;
            PyObject *tmp_source_name_84;
            PyObject *tmp_mvar_value_84;
            PyObject *tmp_tuple_element_29;
            PyObject *tmp_source_name_85;
            PyObject *tmp_mvar_value_85;
            PyObject *tmp_source_name_86;
            PyObject *tmp_mvar_value_86;
            PyObject *tmp_dict_key_30;
            PyObject *tmp_dict_value_30;
            PyObject *tmp_source_name_87;
            PyObject *tmp_mvar_value_87;
            PyObject *tmp_tuple_element_30;
            PyObject *tmp_source_name_88;
            PyObject *tmp_mvar_value_88;
            PyObject *tmp_dict_key_31;
            PyObject *tmp_dict_value_31;
            PyObject *tmp_source_name_89;
            PyObject *tmp_mvar_value_89;
            PyObject *tmp_tuple_element_31;
            PyObject *tmp_source_name_90;
            PyObject *tmp_mvar_value_90;
            PyObject *tmp_source_name_91;
            PyObject *tmp_mvar_value_91;
            PyObject *tmp_dict_key_32;
            PyObject *tmp_dict_value_32;
            PyObject *tmp_source_name_92;
            PyObject *tmp_mvar_value_92;
            PyObject *tmp_tuple_element_32;
            PyObject *tmp_source_name_93;
            PyObject *tmp_mvar_value_93;
            PyObject *tmp_source_name_94;
            PyObject *tmp_mvar_value_94;
            PyObject *tmp_dict_key_33;
            PyObject *tmp_dict_value_33;
            PyObject *tmp_source_name_95;
            PyObject *tmp_mvar_value_95;
            PyObject *tmp_tuple_element_33;
            PyObject *tmp_source_name_96;
            PyObject *tmp_mvar_value_96;
            PyObject *tmp_source_name_97;
            PyObject *tmp_mvar_value_97;
            PyObject *tmp_dict_key_34;
            PyObject *tmp_dict_value_34;
            PyObject *tmp_source_name_98;
            PyObject *tmp_mvar_value_98;
            PyObject *tmp_tuple_element_34;
            PyObject *tmp_source_name_99;
            PyObject *tmp_mvar_value_99;
            PyObject *tmp_source_name_100;
            PyObject *tmp_mvar_value_100;
            PyObject *tmp_dict_key_35;
            PyObject *tmp_dict_value_35;
            PyObject *tmp_source_name_101;
            PyObject *tmp_mvar_value_101;
            PyObject *tmp_tuple_element_35;
            PyObject *tmp_source_name_102;
            PyObject *tmp_mvar_value_102;
            PyObject *tmp_source_name_103;
            PyObject *tmp_mvar_value_103;
            PyObject *tmp_dict_key_36;
            PyObject *tmp_dict_value_36;
            PyObject *tmp_source_name_104;
            PyObject *tmp_mvar_value_104;
            PyObject *tmp_tuple_element_36;
            PyObject *tmp_source_name_105;
            PyObject *tmp_mvar_value_105;
            PyObject *tmp_source_name_106;
            PyObject *tmp_mvar_value_106;
            PyObject *tmp_dict_key_37;
            PyObject *tmp_dict_value_37;
            PyObject *tmp_source_name_107;
            PyObject *tmp_mvar_value_107;
            PyObject *tmp_tuple_element_37;
            PyObject *tmp_source_name_108;
            PyObject *tmp_mvar_value_108;
            PyObject *tmp_source_name_109;
            PyObject *tmp_mvar_value_109;
            PyObject *tmp_dict_key_38;
            PyObject *tmp_dict_value_38;
            PyObject *tmp_source_name_110;
            PyObject *tmp_mvar_value_110;
            PyObject *tmp_tuple_element_38;
            PyObject *tmp_source_name_111;
            PyObject *tmp_mvar_value_111;
            PyObject *tmp_source_name_112;
            PyObject *tmp_mvar_value_112;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiStyle );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiStyle );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiStyle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 98;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_2 = tmp_mvar_value_2;
            tmp_dict_key_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_RESET_ALL );
            if ( tmp_dict_key_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 98;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_dict_key_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 98;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = tmp_mvar_value_3;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_reset_all );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_dict_key_1 );

                exception_lineno = 98;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_dict_value_1, 0, tmp_tuple_element_1 );
            tmp_return_value = _PyDict_NewPresized( 38 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            Py_DECREF( tmp_dict_key_1 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiStyle );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiStyle );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiStyle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 99;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_4 = tmp_mvar_value_4;
            tmp_dict_key_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_BRIGHT );
            if ( tmp_dict_key_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 99;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_5 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 99;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_5 = tmp_mvar_value_5;
            tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_style );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_2 );

                exception_lineno = 99;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_2 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_2, 0, tmp_tuple_element_2 );
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinStyle );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinStyle );
            }

            if ( tmp_mvar_value_6 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_2 );
                Py_DECREF( tmp_dict_value_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinStyle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 99;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_6 = tmp_mvar_value_6;
            tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_BRIGHT );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_2 );
                Py_DECREF( tmp_dict_value_2 );

                exception_lineno = 99;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_2, 1, tmp_tuple_element_2 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            Py_DECREF( tmp_dict_key_2 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiStyle );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiStyle );
            }

            if ( tmp_mvar_value_7 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiStyle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 100;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_7 = tmp_mvar_value_7;
            tmp_dict_key_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_DIM );
            if ( tmp_dict_key_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 100;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_8 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 100;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_8 = tmp_mvar_value_8;
            tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_style );
            if ( tmp_tuple_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_3 );

                exception_lineno = 100;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_3 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_3, 0, tmp_tuple_element_3 );
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinStyle );

            if (unlikely( tmp_mvar_value_9 == NULL ))
            {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinStyle );
            }

            if ( tmp_mvar_value_9 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_3 );
                Py_DECREF( tmp_dict_value_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinStyle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 100;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_9 = tmp_mvar_value_9;
            tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_NORMAL );
            if ( tmp_tuple_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_3 );
                Py_DECREF( tmp_dict_value_3 );

                exception_lineno = 100;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_3, 1, tmp_tuple_element_3 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_3, tmp_dict_value_3 );
            Py_DECREF( tmp_dict_value_3 );
            Py_DECREF( tmp_dict_key_3 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiStyle );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiStyle );
            }

            if ( tmp_mvar_value_10 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiStyle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 101;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_10 = tmp_mvar_value_10;
            tmp_dict_key_4 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_NORMAL );
            if ( tmp_dict_key_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 101;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_11 == NULL ))
            {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_11 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_4 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 101;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_11 = tmp_mvar_value_11;
            tmp_tuple_element_4 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_style );
            if ( tmp_tuple_element_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_4 );

                exception_lineno = 101;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_4 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_4, 0, tmp_tuple_element_4 );
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinStyle );

            if (unlikely( tmp_mvar_value_12 == NULL ))
            {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinStyle );
            }

            if ( tmp_mvar_value_12 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_4 );
                Py_DECREF( tmp_dict_value_4 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinStyle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 101;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_12 = tmp_mvar_value_12;
            tmp_tuple_element_4 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_NORMAL );
            if ( tmp_tuple_element_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_4 );
                Py_DECREF( tmp_dict_value_4 );

                exception_lineno = 101;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_4, 1, tmp_tuple_element_4 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_4, tmp_dict_value_4 );
            Py_DECREF( tmp_dict_value_4 );
            Py_DECREF( tmp_dict_key_4 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_13 == NULL ))
            {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_13 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 102;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_13 = tmp_mvar_value_13;
            tmp_dict_key_5 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_BLACK );
            if ( tmp_dict_key_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 102;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_14 == NULL ))
            {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_14 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_5 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 102;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_14 = tmp_mvar_value_14;
            tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_fore );
            if ( tmp_tuple_element_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_5 );

                exception_lineno = 102;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_5 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_5, 0, tmp_tuple_element_5 );
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_15 == NULL ))
            {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_15 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_5 );
                Py_DECREF( tmp_dict_value_5 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 102;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_15 = tmp_mvar_value_15;
            tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_BLACK );
            if ( tmp_tuple_element_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_5 );
                Py_DECREF( tmp_dict_value_5 );

                exception_lineno = 102;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_5, 1, tmp_tuple_element_5 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_5, tmp_dict_value_5 );
            Py_DECREF( tmp_dict_value_5 );
            Py_DECREF( tmp_dict_key_5 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_16 == NULL ))
            {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_16 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 103;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_16 = tmp_mvar_value_16;
            tmp_dict_key_6 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_RED );
            if ( tmp_dict_key_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 103;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_17 == NULL ))
            {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_17 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_6 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 103;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_17 = tmp_mvar_value_17;
            tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_fore );
            if ( tmp_tuple_element_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_6 );

                exception_lineno = 103;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_6 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_6, 0, tmp_tuple_element_6 );
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_18 == NULL ))
            {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_18 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_6 );
                Py_DECREF( tmp_dict_value_6 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 103;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_18 = tmp_mvar_value_18;
            tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_RED );
            if ( tmp_tuple_element_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_6 );
                Py_DECREF( tmp_dict_value_6 );

                exception_lineno = 103;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_6, 1, tmp_tuple_element_6 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_6, tmp_dict_value_6 );
            Py_DECREF( tmp_dict_value_6 );
            Py_DECREF( tmp_dict_key_6 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_19 == NULL ))
            {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_19 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 104;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_19 = tmp_mvar_value_19;
            tmp_dict_key_7 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_GREEN );
            if ( tmp_dict_key_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 104;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_20 == NULL ))
            {
                tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_20 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_7 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 104;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_20 = tmp_mvar_value_20;
            tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_fore );
            if ( tmp_tuple_element_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_7 );

                exception_lineno = 104;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_7 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_7, 0, tmp_tuple_element_7 );
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_21 == NULL ))
            {
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_21 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_7 );
                Py_DECREF( tmp_dict_value_7 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 104;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_21 = tmp_mvar_value_21;
            tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_GREEN );
            if ( tmp_tuple_element_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_7 );
                Py_DECREF( tmp_dict_value_7 );

                exception_lineno = 104;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_7, 1, tmp_tuple_element_7 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_7, tmp_dict_value_7 );
            Py_DECREF( tmp_dict_value_7 );
            Py_DECREF( tmp_dict_key_7 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_22 == NULL ))
            {
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_22 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 105;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_22 = tmp_mvar_value_22;
            tmp_dict_key_8 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_YELLOW );
            if ( tmp_dict_key_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 105;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_23 == NULL ))
            {
                tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_23 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_8 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 105;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_23 = tmp_mvar_value_23;
            tmp_tuple_element_8 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_fore );
            if ( tmp_tuple_element_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_8 );

                exception_lineno = 105;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_8 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_8, 0, tmp_tuple_element_8 );
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_24 == NULL ))
            {
                tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_24 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_8 );
                Py_DECREF( tmp_dict_value_8 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 105;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_24 = tmp_mvar_value_24;
            tmp_tuple_element_8 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_YELLOW );
            if ( tmp_tuple_element_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_8 );
                Py_DECREF( tmp_dict_value_8 );

                exception_lineno = 105;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_8, 1, tmp_tuple_element_8 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_8, tmp_dict_value_8 );
            Py_DECREF( tmp_dict_value_8 );
            Py_DECREF( tmp_dict_key_8 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_25 == NULL ))
            {
                tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_25 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 106;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_25 = tmp_mvar_value_25;
            tmp_dict_key_9 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_BLUE );
            if ( tmp_dict_key_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 106;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_26 == NULL ))
            {
                tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_26 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_9 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 106;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_26 = tmp_mvar_value_26;
            tmp_tuple_element_9 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_fore );
            if ( tmp_tuple_element_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_9 );

                exception_lineno = 106;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_9 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_9, 0, tmp_tuple_element_9 );
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_27 == NULL ))
            {
                tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_27 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_9 );
                Py_DECREF( tmp_dict_value_9 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 106;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_27 = tmp_mvar_value_27;
            tmp_tuple_element_9 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_BLUE );
            if ( tmp_tuple_element_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_9 );
                Py_DECREF( tmp_dict_value_9 );

                exception_lineno = 106;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_9, 1, tmp_tuple_element_9 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_9, tmp_dict_value_9 );
            Py_DECREF( tmp_dict_value_9 );
            Py_DECREF( tmp_dict_key_9 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_28 == NULL ))
            {
                tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_28 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 107;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_28 = tmp_mvar_value_28;
            tmp_dict_key_10 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_MAGENTA );
            if ( tmp_dict_key_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 107;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_29 == NULL ))
            {
                tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_29 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_10 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 107;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_29 = tmp_mvar_value_29;
            tmp_tuple_element_10 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_fore );
            if ( tmp_tuple_element_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_10 );

                exception_lineno = 107;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_10 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_10, 0, tmp_tuple_element_10 );
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_30 == NULL ))
            {
                tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_30 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_10 );
                Py_DECREF( tmp_dict_value_10 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 107;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_30 = tmp_mvar_value_30;
            tmp_tuple_element_10 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_MAGENTA );
            if ( tmp_tuple_element_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_10 );
                Py_DECREF( tmp_dict_value_10 );

                exception_lineno = 107;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_10, 1, tmp_tuple_element_10 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_10, tmp_dict_value_10 );
            Py_DECREF( tmp_dict_value_10 );
            Py_DECREF( tmp_dict_key_10 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_31 == NULL ))
            {
                tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_31 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 108;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_31 = tmp_mvar_value_31;
            tmp_dict_key_11 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_CYAN );
            if ( tmp_dict_key_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 108;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_32 == NULL ))
            {
                tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_32 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 108;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_32 = tmp_mvar_value_32;
            tmp_tuple_element_11 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_fore );
            if ( tmp_tuple_element_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_11 );

                exception_lineno = 108;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_11 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_11, 0, tmp_tuple_element_11 );
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_33 == NULL ))
            {
                tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_33 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_11 );
                Py_DECREF( tmp_dict_value_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 108;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_33 = tmp_mvar_value_33;
            tmp_tuple_element_11 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_CYAN );
            if ( tmp_tuple_element_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_11 );
                Py_DECREF( tmp_dict_value_11 );

                exception_lineno = 108;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_11, 1, tmp_tuple_element_11 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_11, tmp_dict_value_11 );
            Py_DECREF( tmp_dict_value_11 );
            Py_DECREF( tmp_dict_key_11 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_34 == NULL ))
            {
                tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_34 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 109;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_34 = tmp_mvar_value_34;
            tmp_dict_key_12 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain_WHITE );
            if ( tmp_dict_key_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 109;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_35 == NULL ))
            {
                tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_35 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_12 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 109;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_35 = tmp_mvar_value_35;
            tmp_tuple_element_12 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain_fore );
            if ( tmp_tuple_element_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_12 );

                exception_lineno = 109;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_12 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_12, 0, tmp_tuple_element_12 );
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_36 == NULL ))
            {
                tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_36 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_12 );
                Py_DECREF( tmp_dict_value_12 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 109;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_36 = tmp_mvar_value_36;
            tmp_tuple_element_12 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain_GREY );
            if ( tmp_tuple_element_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_12 );
                Py_DECREF( tmp_dict_value_12 );

                exception_lineno = 109;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_12, 1, tmp_tuple_element_12 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_12, tmp_dict_value_12 );
            Py_DECREF( tmp_dict_value_12 );
            Py_DECREF( tmp_dict_key_12 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_37 == NULL ))
            {
                tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_37 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 110;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_37 = tmp_mvar_value_37;
            tmp_dict_key_13 = LOOKUP_ATTRIBUTE( tmp_source_name_37, const_str_plain_RESET );
            if ( tmp_dict_key_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 110;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_38 == NULL ))
            {
                tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_38 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_13 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 110;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_38 = tmp_mvar_value_38;
            tmp_tuple_element_13 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain_fore );
            if ( tmp_tuple_element_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_13 );

                exception_lineno = 110;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_13 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_dict_value_13, 0, tmp_tuple_element_13 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_13, tmp_dict_value_13 );
            Py_DECREF( tmp_dict_value_13 );
            Py_DECREF( tmp_dict_key_13 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_39 == NULL ))
            {
                tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_39 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 111;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_39 = tmp_mvar_value_39;
            tmp_dict_key_14 = LOOKUP_ATTRIBUTE( tmp_source_name_39, const_str_plain_LIGHTBLACK_EX );
            if ( tmp_dict_key_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 111;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_40 == NULL ))
            {
                tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_40 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_14 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 111;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_40 = tmp_mvar_value_40;
            tmp_tuple_element_14 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain_fore );
            if ( tmp_tuple_element_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_14 );

                exception_lineno = 111;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_14 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_14, 0, tmp_tuple_element_14 );
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_41 == NULL ))
            {
                tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_41 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_14 );
                Py_DECREF( tmp_dict_value_14 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 111;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_41 = tmp_mvar_value_41;
            tmp_tuple_element_14 = LOOKUP_ATTRIBUTE( tmp_source_name_41, const_str_plain_BLACK );
            if ( tmp_tuple_element_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_14 );
                Py_DECREF( tmp_dict_value_14 );

                exception_lineno = 111;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_14, 1, tmp_tuple_element_14 );
            tmp_tuple_element_14 = Py_True;
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_dict_value_14, 2, tmp_tuple_element_14 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_14, tmp_dict_value_14 );
            Py_DECREF( tmp_dict_value_14 );
            Py_DECREF( tmp_dict_key_14 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_42 == NULL ))
            {
                tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_42 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 112;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_42 = tmp_mvar_value_42;
            tmp_dict_key_15 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain_LIGHTRED_EX );
            if ( tmp_dict_key_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 112;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_43 == NULL ))
            {
                tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_43 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_15 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 112;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_43 = tmp_mvar_value_43;
            tmp_tuple_element_15 = LOOKUP_ATTRIBUTE( tmp_source_name_43, const_str_plain_fore );
            if ( tmp_tuple_element_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_15 );

                exception_lineno = 112;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_15 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_15, 0, tmp_tuple_element_15 );
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_44 == NULL ))
            {
                tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_44 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_15 );
                Py_DECREF( tmp_dict_value_15 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 112;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_44 = tmp_mvar_value_44;
            tmp_tuple_element_15 = LOOKUP_ATTRIBUTE( tmp_source_name_44, const_str_plain_RED );
            if ( tmp_tuple_element_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_15 );
                Py_DECREF( tmp_dict_value_15 );

                exception_lineno = 112;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_15, 1, tmp_tuple_element_15 );
            tmp_tuple_element_15 = Py_True;
            Py_INCREF( tmp_tuple_element_15 );
            PyTuple_SET_ITEM( tmp_dict_value_15, 2, tmp_tuple_element_15 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_15, tmp_dict_value_15 );
            Py_DECREF( tmp_dict_value_15 );
            Py_DECREF( tmp_dict_key_15 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_45 == NULL ))
            {
                tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_45 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 113;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_45 = tmp_mvar_value_45;
            tmp_dict_key_16 = LOOKUP_ATTRIBUTE( tmp_source_name_45, const_str_plain_LIGHTGREEN_EX );
            if ( tmp_dict_key_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 113;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_46 == NULL ))
            {
                tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_46 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_16 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 113;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_46 = tmp_mvar_value_46;
            tmp_tuple_element_16 = LOOKUP_ATTRIBUTE( tmp_source_name_46, const_str_plain_fore );
            if ( tmp_tuple_element_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_16 );

                exception_lineno = 113;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_16 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_16, 0, tmp_tuple_element_16 );
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_47 == NULL ))
            {
                tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_47 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_16 );
                Py_DECREF( tmp_dict_value_16 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 113;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_47 = tmp_mvar_value_47;
            tmp_tuple_element_16 = LOOKUP_ATTRIBUTE( tmp_source_name_47, const_str_plain_GREEN );
            if ( tmp_tuple_element_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_16 );
                Py_DECREF( tmp_dict_value_16 );

                exception_lineno = 113;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_16, 1, tmp_tuple_element_16 );
            tmp_tuple_element_16 = Py_True;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_dict_value_16, 2, tmp_tuple_element_16 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_16, tmp_dict_value_16 );
            Py_DECREF( tmp_dict_value_16 );
            Py_DECREF( tmp_dict_key_16 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_48 == NULL ))
            {
                tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_48 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 114;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_48 = tmp_mvar_value_48;
            tmp_dict_key_17 = LOOKUP_ATTRIBUTE( tmp_source_name_48, const_str_plain_LIGHTYELLOW_EX );
            if ( tmp_dict_key_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 114;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_49 == NULL ))
            {
                tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_49 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_17 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 114;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_49 = tmp_mvar_value_49;
            tmp_tuple_element_17 = LOOKUP_ATTRIBUTE( tmp_source_name_49, const_str_plain_fore );
            if ( tmp_tuple_element_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_17 );

                exception_lineno = 114;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_17 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_17, 0, tmp_tuple_element_17 );
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_50 == NULL ))
            {
                tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_50 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_17 );
                Py_DECREF( tmp_dict_value_17 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 114;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_50 = tmp_mvar_value_50;
            tmp_tuple_element_17 = LOOKUP_ATTRIBUTE( tmp_source_name_50, const_str_plain_YELLOW );
            if ( tmp_tuple_element_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_17 );
                Py_DECREF( tmp_dict_value_17 );

                exception_lineno = 114;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_17, 1, tmp_tuple_element_17 );
            tmp_tuple_element_17 = Py_True;
            Py_INCREF( tmp_tuple_element_17 );
            PyTuple_SET_ITEM( tmp_dict_value_17, 2, tmp_tuple_element_17 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_17, tmp_dict_value_17 );
            Py_DECREF( tmp_dict_value_17 );
            Py_DECREF( tmp_dict_key_17 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_51 == NULL ))
            {
                tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_51 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 115;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_51 = tmp_mvar_value_51;
            tmp_dict_key_18 = LOOKUP_ATTRIBUTE( tmp_source_name_51, const_str_plain_LIGHTBLUE_EX );
            if ( tmp_dict_key_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 115;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_52 == NULL ))
            {
                tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_52 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_18 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 115;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_52 = tmp_mvar_value_52;
            tmp_tuple_element_18 = LOOKUP_ATTRIBUTE( tmp_source_name_52, const_str_plain_fore );
            if ( tmp_tuple_element_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_18 );

                exception_lineno = 115;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_18 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_18, 0, tmp_tuple_element_18 );
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_53 == NULL ))
            {
                tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_53 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_18 );
                Py_DECREF( tmp_dict_value_18 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 115;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_53 = tmp_mvar_value_53;
            tmp_tuple_element_18 = LOOKUP_ATTRIBUTE( tmp_source_name_53, const_str_plain_BLUE );
            if ( tmp_tuple_element_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_18 );
                Py_DECREF( tmp_dict_value_18 );

                exception_lineno = 115;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_18, 1, tmp_tuple_element_18 );
            tmp_tuple_element_18 = Py_True;
            Py_INCREF( tmp_tuple_element_18 );
            PyTuple_SET_ITEM( tmp_dict_value_18, 2, tmp_tuple_element_18 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_18, tmp_dict_value_18 );
            Py_DECREF( tmp_dict_value_18 );
            Py_DECREF( tmp_dict_key_18 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_54 == NULL ))
            {
                tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_54 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 116;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_54 = tmp_mvar_value_54;
            tmp_dict_key_19 = LOOKUP_ATTRIBUTE( tmp_source_name_54, const_str_plain_LIGHTMAGENTA_EX );
            if ( tmp_dict_key_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 116;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_55 == NULL ))
            {
                tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_55 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_19 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 116;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_55 = tmp_mvar_value_55;
            tmp_tuple_element_19 = LOOKUP_ATTRIBUTE( tmp_source_name_55, const_str_plain_fore );
            if ( tmp_tuple_element_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_19 );

                exception_lineno = 116;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_19 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_19, 0, tmp_tuple_element_19 );
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_56 == NULL ))
            {
                tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_56 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_19 );
                Py_DECREF( tmp_dict_value_19 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 116;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_56 = tmp_mvar_value_56;
            tmp_tuple_element_19 = LOOKUP_ATTRIBUTE( tmp_source_name_56, const_str_plain_MAGENTA );
            if ( tmp_tuple_element_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_19 );
                Py_DECREF( tmp_dict_value_19 );

                exception_lineno = 116;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_19, 1, tmp_tuple_element_19 );
            tmp_tuple_element_19 = Py_True;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_dict_value_19, 2, tmp_tuple_element_19 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_19, tmp_dict_value_19 );
            Py_DECREF( tmp_dict_value_19 );
            Py_DECREF( tmp_dict_key_19 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_57 == NULL ))
            {
                tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_57 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 117;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_57 = tmp_mvar_value_57;
            tmp_dict_key_20 = LOOKUP_ATTRIBUTE( tmp_source_name_57, const_str_plain_LIGHTCYAN_EX );
            if ( tmp_dict_key_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 117;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_58 == NULL ))
            {
                tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_58 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_20 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 117;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_58 = tmp_mvar_value_58;
            tmp_tuple_element_20 = LOOKUP_ATTRIBUTE( tmp_source_name_58, const_str_plain_fore );
            if ( tmp_tuple_element_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_20 );

                exception_lineno = 117;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_20 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_20, 0, tmp_tuple_element_20 );
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_59 == NULL ))
            {
                tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_59 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_20 );
                Py_DECREF( tmp_dict_value_20 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 117;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_59 = tmp_mvar_value_59;
            tmp_tuple_element_20 = LOOKUP_ATTRIBUTE( tmp_source_name_59, const_str_plain_CYAN );
            if ( tmp_tuple_element_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_20 );
                Py_DECREF( tmp_dict_value_20 );

                exception_lineno = 117;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_20, 1, tmp_tuple_element_20 );
            tmp_tuple_element_20 = Py_True;
            Py_INCREF( tmp_tuple_element_20 );
            PyTuple_SET_ITEM( tmp_dict_value_20, 2, tmp_tuple_element_20 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_20, tmp_dict_value_20 );
            Py_DECREF( tmp_dict_value_20 );
            Py_DECREF( tmp_dict_key_20 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore );

            if (unlikely( tmp_mvar_value_60 == NULL ))
            {
                tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiFore );
            }

            if ( tmp_mvar_value_60 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiFore" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 118;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_60 = tmp_mvar_value_60;
            tmp_dict_key_21 = LOOKUP_ATTRIBUTE( tmp_source_name_60, const_str_plain_LIGHTWHITE_EX );
            if ( tmp_dict_key_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 118;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_61 == NULL ))
            {
                tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_61 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_21 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 118;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_61 = tmp_mvar_value_61;
            tmp_tuple_element_21 = LOOKUP_ATTRIBUTE( tmp_source_name_61, const_str_plain_fore );
            if ( tmp_tuple_element_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_21 );

                exception_lineno = 118;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_21 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_21, 0, tmp_tuple_element_21 );
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_62 == NULL ))
            {
                tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_62 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_21 );
                Py_DECREF( tmp_dict_value_21 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 118;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_62 = tmp_mvar_value_62;
            tmp_tuple_element_21 = LOOKUP_ATTRIBUTE( tmp_source_name_62, const_str_plain_GREY );
            if ( tmp_tuple_element_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_21 );
                Py_DECREF( tmp_dict_value_21 );

                exception_lineno = 118;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_21, 1, tmp_tuple_element_21 );
            tmp_tuple_element_21 = Py_True;
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_dict_value_21, 2, tmp_tuple_element_21 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_21, tmp_dict_value_21 );
            Py_DECREF( tmp_dict_value_21 );
            Py_DECREF( tmp_dict_key_21 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_63 == NULL ))
            {
                tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_63 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 119;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_63 = tmp_mvar_value_63;
            tmp_dict_key_22 = LOOKUP_ATTRIBUTE( tmp_source_name_63, const_str_plain_BLACK );
            if ( tmp_dict_key_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 119;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_64 == NULL ))
            {
                tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_64 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_22 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 119;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_64 = tmp_mvar_value_64;
            tmp_tuple_element_22 = LOOKUP_ATTRIBUTE( tmp_source_name_64, const_str_plain_back );
            if ( tmp_tuple_element_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_22 );

                exception_lineno = 119;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_22 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_22, 0, tmp_tuple_element_22 );
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_65 == NULL ))
            {
                tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_65 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_22 );
                Py_DECREF( tmp_dict_value_22 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 119;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_65 = tmp_mvar_value_65;
            tmp_tuple_element_22 = LOOKUP_ATTRIBUTE( tmp_source_name_65, const_str_plain_BLACK );
            if ( tmp_tuple_element_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_22 );
                Py_DECREF( tmp_dict_value_22 );

                exception_lineno = 119;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_22, 1, tmp_tuple_element_22 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_22, tmp_dict_value_22 );
            Py_DECREF( tmp_dict_value_22 );
            Py_DECREF( tmp_dict_key_22 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_66 == NULL ))
            {
                tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_66 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 120;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_66 = tmp_mvar_value_66;
            tmp_dict_key_23 = LOOKUP_ATTRIBUTE( tmp_source_name_66, const_str_plain_RED );
            if ( tmp_dict_key_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 120;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_67 == NULL ))
            {
                tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_67 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_23 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 120;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_67 = tmp_mvar_value_67;
            tmp_tuple_element_23 = LOOKUP_ATTRIBUTE( tmp_source_name_67, const_str_plain_back );
            if ( tmp_tuple_element_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_23 );

                exception_lineno = 120;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_23 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_23, 0, tmp_tuple_element_23 );
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_68 == NULL ))
            {
                tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_68 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_23 );
                Py_DECREF( tmp_dict_value_23 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 120;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_68 = tmp_mvar_value_68;
            tmp_tuple_element_23 = LOOKUP_ATTRIBUTE( tmp_source_name_68, const_str_plain_RED );
            if ( tmp_tuple_element_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_23 );
                Py_DECREF( tmp_dict_value_23 );

                exception_lineno = 120;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_23, 1, tmp_tuple_element_23 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_23, tmp_dict_value_23 );
            Py_DECREF( tmp_dict_value_23 );
            Py_DECREF( tmp_dict_key_23 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_69 == NULL ))
            {
                tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_69 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 121;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_69 = tmp_mvar_value_69;
            tmp_dict_key_24 = LOOKUP_ATTRIBUTE( tmp_source_name_69, const_str_plain_GREEN );
            if ( tmp_dict_key_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 121;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_70 == NULL ))
            {
                tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_70 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_24 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 121;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_70 = tmp_mvar_value_70;
            tmp_tuple_element_24 = LOOKUP_ATTRIBUTE( tmp_source_name_70, const_str_plain_back );
            if ( tmp_tuple_element_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_24 );

                exception_lineno = 121;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_24 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_24, 0, tmp_tuple_element_24 );
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_71 == NULL ))
            {
                tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_71 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_24 );
                Py_DECREF( tmp_dict_value_24 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 121;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_71 = tmp_mvar_value_71;
            tmp_tuple_element_24 = LOOKUP_ATTRIBUTE( tmp_source_name_71, const_str_plain_GREEN );
            if ( tmp_tuple_element_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_24 );
                Py_DECREF( tmp_dict_value_24 );

                exception_lineno = 121;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_24, 1, tmp_tuple_element_24 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_24, tmp_dict_value_24 );
            Py_DECREF( tmp_dict_value_24 );
            Py_DECREF( tmp_dict_key_24 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_72 == NULL ))
            {
                tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_72 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 122;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_72 = tmp_mvar_value_72;
            tmp_dict_key_25 = LOOKUP_ATTRIBUTE( tmp_source_name_72, const_str_plain_YELLOW );
            if ( tmp_dict_key_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 122;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_73 == NULL ))
            {
                tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_73 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_25 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 122;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_73 = tmp_mvar_value_73;
            tmp_tuple_element_25 = LOOKUP_ATTRIBUTE( tmp_source_name_73, const_str_plain_back );
            if ( tmp_tuple_element_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_25 );

                exception_lineno = 122;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_25 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_25, 0, tmp_tuple_element_25 );
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_74 == NULL ))
            {
                tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_74 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_25 );
                Py_DECREF( tmp_dict_value_25 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 122;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_74 = tmp_mvar_value_74;
            tmp_tuple_element_25 = LOOKUP_ATTRIBUTE( tmp_source_name_74, const_str_plain_YELLOW );
            if ( tmp_tuple_element_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_25 );
                Py_DECREF( tmp_dict_value_25 );

                exception_lineno = 122;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_25, 1, tmp_tuple_element_25 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_25, tmp_dict_value_25 );
            Py_DECREF( tmp_dict_value_25 );
            Py_DECREF( tmp_dict_key_25 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_75 == NULL ))
            {
                tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_75 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 123;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_75 = tmp_mvar_value_75;
            tmp_dict_key_26 = LOOKUP_ATTRIBUTE( tmp_source_name_75, const_str_plain_BLUE );
            if ( tmp_dict_key_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 123;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_76 == NULL ))
            {
                tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_76 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_26 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 123;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_76 = tmp_mvar_value_76;
            tmp_tuple_element_26 = LOOKUP_ATTRIBUTE( tmp_source_name_76, const_str_plain_back );
            if ( tmp_tuple_element_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_26 );

                exception_lineno = 123;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_26 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_26, 0, tmp_tuple_element_26 );
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_77 == NULL ))
            {
                tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_77 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_26 );
                Py_DECREF( tmp_dict_value_26 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 123;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_77 = tmp_mvar_value_77;
            tmp_tuple_element_26 = LOOKUP_ATTRIBUTE( tmp_source_name_77, const_str_plain_BLUE );
            if ( tmp_tuple_element_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_26 );
                Py_DECREF( tmp_dict_value_26 );

                exception_lineno = 123;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_26, 1, tmp_tuple_element_26 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_26, tmp_dict_value_26 );
            Py_DECREF( tmp_dict_value_26 );
            Py_DECREF( tmp_dict_key_26 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_78 == NULL ))
            {
                tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_78 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 124;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_78 = tmp_mvar_value_78;
            tmp_dict_key_27 = LOOKUP_ATTRIBUTE( tmp_source_name_78, const_str_plain_MAGENTA );
            if ( tmp_dict_key_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 124;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_79 == NULL ))
            {
                tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_79 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_27 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 124;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_79 = tmp_mvar_value_79;
            tmp_tuple_element_27 = LOOKUP_ATTRIBUTE( tmp_source_name_79, const_str_plain_back );
            if ( tmp_tuple_element_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_27 );

                exception_lineno = 124;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_27 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_27, 0, tmp_tuple_element_27 );
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_80 == NULL ))
            {
                tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_80 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_27 );
                Py_DECREF( tmp_dict_value_27 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 124;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_80 = tmp_mvar_value_80;
            tmp_tuple_element_27 = LOOKUP_ATTRIBUTE( tmp_source_name_80, const_str_plain_MAGENTA );
            if ( tmp_tuple_element_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_27 );
                Py_DECREF( tmp_dict_value_27 );

                exception_lineno = 124;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_27, 1, tmp_tuple_element_27 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_27, tmp_dict_value_27 );
            Py_DECREF( tmp_dict_value_27 );
            Py_DECREF( tmp_dict_key_27 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_81 == NULL ))
            {
                tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_81 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 125;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_81 = tmp_mvar_value_81;
            tmp_dict_key_28 = LOOKUP_ATTRIBUTE( tmp_source_name_81, const_str_plain_CYAN );
            if ( tmp_dict_key_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 125;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_82 == NULL ))
            {
                tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_82 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_28 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 125;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_82 = tmp_mvar_value_82;
            tmp_tuple_element_28 = LOOKUP_ATTRIBUTE( tmp_source_name_82, const_str_plain_back );
            if ( tmp_tuple_element_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_28 );

                exception_lineno = 125;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_28 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_28, 0, tmp_tuple_element_28 );
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_83 == NULL ))
            {
                tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_83 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_28 );
                Py_DECREF( tmp_dict_value_28 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 125;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_83 = tmp_mvar_value_83;
            tmp_tuple_element_28 = LOOKUP_ATTRIBUTE( tmp_source_name_83, const_str_plain_CYAN );
            if ( tmp_tuple_element_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_28 );
                Py_DECREF( tmp_dict_value_28 );

                exception_lineno = 125;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_28, 1, tmp_tuple_element_28 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_28, tmp_dict_value_28 );
            Py_DECREF( tmp_dict_value_28 );
            Py_DECREF( tmp_dict_key_28 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_84 == NULL ))
            {
                tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_84 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 126;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_84 = tmp_mvar_value_84;
            tmp_dict_key_29 = LOOKUP_ATTRIBUTE( tmp_source_name_84, const_str_plain_WHITE );
            if ( tmp_dict_key_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 126;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_85 == NULL ))
            {
                tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_85 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_29 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 126;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_85 = tmp_mvar_value_85;
            tmp_tuple_element_29 = LOOKUP_ATTRIBUTE( tmp_source_name_85, const_str_plain_back );
            if ( tmp_tuple_element_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_29 );

                exception_lineno = 126;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_29 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_29, 0, tmp_tuple_element_29 );
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_86 == NULL ))
            {
                tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_86 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_29 );
                Py_DECREF( tmp_dict_value_29 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 126;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_86 = tmp_mvar_value_86;
            tmp_tuple_element_29 = LOOKUP_ATTRIBUTE( tmp_source_name_86, const_str_plain_GREY );
            if ( tmp_tuple_element_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_29 );
                Py_DECREF( tmp_dict_value_29 );

                exception_lineno = 126;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_29, 1, tmp_tuple_element_29 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_29, tmp_dict_value_29 );
            Py_DECREF( tmp_dict_value_29 );
            Py_DECREF( tmp_dict_key_29 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_87 == NULL ))
            {
                tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_87 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 127;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_87 = tmp_mvar_value_87;
            tmp_dict_key_30 = LOOKUP_ATTRIBUTE( tmp_source_name_87, const_str_plain_RESET );
            if ( tmp_dict_key_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 127;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_88 == NULL ))
            {
                tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_88 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_30 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 127;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_88 = tmp_mvar_value_88;
            tmp_tuple_element_30 = LOOKUP_ATTRIBUTE( tmp_source_name_88, const_str_plain_back );
            if ( tmp_tuple_element_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_30 );

                exception_lineno = 127;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_30 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_dict_value_30, 0, tmp_tuple_element_30 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_30, tmp_dict_value_30 );
            Py_DECREF( tmp_dict_value_30 );
            Py_DECREF( tmp_dict_key_30 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_89 == NULL ))
            {
                tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_89 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 128;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_89 = tmp_mvar_value_89;
            tmp_dict_key_31 = LOOKUP_ATTRIBUTE( tmp_source_name_89, const_str_plain_LIGHTBLACK_EX );
            if ( tmp_dict_key_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 128;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_90 == NULL ))
            {
                tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_90 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_31 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 128;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_90 = tmp_mvar_value_90;
            tmp_tuple_element_31 = LOOKUP_ATTRIBUTE( tmp_source_name_90, const_str_plain_back );
            if ( tmp_tuple_element_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_31 );

                exception_lineno = 128;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_31 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_31, 0, tmp_tuple_element_31 );
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_91 == NULL ))
            {
                tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_91 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_31 );
                Py_DECREF( tmp_dict_value_31 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 128;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_91 = tmp_mvar_value_91;
            tmp_tuple_element_31 = LOOKUP_ATTRIBUTE( tmp_source_name_91, const_str_plain_BLACK );
            if ( tmp_tuple_element_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_31 );
                Py_DECREF( tmp_dict_value_31 );

                exception_lineno = 128;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_31, 1, tmp_tuple_element_31 );
            tmp_tuple_element_31 = Py_True;
            Py_INCREF( tmp_tuple_element_31 );
            PyTuple_SET_ITEM( tmp_dict_value_31, 2, tmp_tuple_element_31 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_31, tmp_dict_value_31 );
            Py_DECREF( tmp_dict_value_31 );
            Py_DECREF( tmp_dict_key_31 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_92 == NULL ))
            {
                tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_92 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_92 = tmp_mvar_value_92;
            tmp_dict_key_32 = LOOKUP_ATTRIBUTE( tmp_source_name_92, const_str_plain_LIGHTRED_EX );
            if ( tmp_dict_key_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_93 == NULL ))
            {
                tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_93 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_32 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_93 = tmp_mvar_value_93;
            tmp_tuple_element_32 = LOOKUP_ATTRIBUTE( tmp_source_name_93, const_str_plain_back );
            if ( tmp_tuple_element_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_32 );

                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_32 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_32, 0, tmp_tuple_element_32 );
            tmp_mvar_value_94 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_94 == NULL ))
            {
                tmp_mvar_value_94 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_94 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_32 );
                Py_DECREF( tmp_dict_value_32 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_94 = tmp_mvar_value_94;
            tmp_tuple_element_32 = LOOKUP_ATTRIBUTE( tmp_source_name_94, const_str_plain_RED );
            if ( tmp_tuple_element_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_32 );
                Py_DECREF( tmp_dict_value_32 );

                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_32, 1, tmp_tuple_element_32 );
            tmp_tuple_element_32 = Py_True;
            Py_INCREF( tmp_tuple_element_32 );
            PyTuple_SET_ITEM( tmp_dict_value_32, 2, tmp_tuple_element_32 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_32, tmp_dict_value_32 );
            Py_DECREF( tmp_dict_value_32 );
            Py_DECREF( tmp_dict_key_32 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_95 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_95 == NULL ))
            {
                tmp_mvar_value_95 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_95 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 130;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_95 = tmp_mvar_value_95;
            tmp_dict_key_33 = LOOKUP_ATTRIBUTE( tmp_source_name_95, const_str_plain_LIGHTGREEN_EX );
            if ( tmp_dict_key_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 130;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_96 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_96 == NULL ))
            {
                tmp_mvar_value_96 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_96 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_33 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 130;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_96 = tmp_mvar_value_96;
            tmp_tuple_element_33 = LOOKUP_ATTRIBUTE( tmp_source_name_96, const_str_plain_back );
            if ( tmp_tuple_element_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_33 );

                exception_lineno = 130;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_33 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_33, 0, tmp_tuple_element_33 );
            tmp_mvar_value_97 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_97 == NULL ))
            {
                tmp_mvar_value_97 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_97 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_33 );
                Py_DECREF( tmp_dict_value_33 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 130;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_97 = tmp_mvar_value_97;
            tmp_tuple_element_33 = LOOKUP_ATTRIBUTE( tmp_source_name_97, const_str_plain_GREEN );
            if ( tmp_tuple_element_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_33 );
                Py_DECREF( tmp_dict_value_33 );

                exception_lineno = 130;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_33, 1, tmp_tuple_element_33 );
            tmp_tuple_element_33 = Py_True;
            Py_INCREF( tmp_tuple_element_33 );
            PyTuple_SET_ITEM( tmp_dict_value_33, 2, tmp_tuple_element_33 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_33, tmp_dict_value_33 );
            Py_DECREF( tmp_dict_value_33 );
            Py_DECREF( tmp_dict_key_33 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_98 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_98 == NULL ))
            {
                tmp_mvar_value_98 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_98 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_98 = tmp_mvar_value_98;
            tmp_dict_key_34 = LOOKUP_ATTRIBUTE( tmp_source_name_98, const_str_plain_LIGHTYELLOW_EX );
            if ( tmp_dict_key_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_99 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_99 == NULL ))
            {
                tmp_mvar_value_99 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_99 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_34 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_99 = tmp_mvar_value_99;
            tmp_tuple_element_34 = LOOKUP_ATTRIBUTE( tmp_source_name_99, const_str_plain_back );
            if ( tmp_tuple_element_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_34 );

                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_34 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_34, 0, tmp_tuple_element_34 );
            tmp_mvar_value_100 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_100 == NULL ))
            {
                tmp_mvar_value_100 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_100 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_34 );
                Py_DECREF( tmp_dict_value_34 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_100 = tmp_mvar_value_100;
            tmp_tuple_element_34 = LOOKUP_ATTRIBUTE( tmp_source_name_100, const_str_plain_YELLOW );
            if ( tmp_tuple_element_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_34 );
                Py_DECREF( tmp_dict_value_34 );

                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_34, 1, tmp_tuple_element_34 );
            tmp_tuple_element_34 = Py_True;
            Py_INCREF( tmp_tuple_element_34 );
            PyTuple_SET_ITEM( tmp_dict_value_34, 2, tmp_tuple_element_34 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_34, tmp_dict_value_34 );
            Py_DECREF( tmp_dict_value_34 );
            Py_DECREF( tmp_dict_key_34 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_101 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_101 == NULL ))
            {
                tmp_mvar_value_101 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_101 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 132;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_101 = tmp_mvar_value_101;
            tmp_dict_key_35 = LOOKUP_ATTRIBUTE( tmp_source_name_101, const_str_plain_LIGHTBLUE_EX );
            if ( tmp_dict_key_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 132;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_102 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_102 == NULL ))
            {
                tmp_mvar_value_102 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_102 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_35 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 132;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_102 = tmp_mvar_value_102;
            tmp_tuple_element_35 = LOOKUP_ATTRIBUTE( tmp_source_name_102, const_str_plain_back );
            if ( tmp_tuple_element_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_35 );

                exception_lineno = 132;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_35 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_35, 0, tmp_tuple_element_35 );
            tmp_mvar_value_103 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_103 == NULL ))
            {
                tmp_mvar_value_103 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_103 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_35 );
                Py_DECREF( tmp_dict_value_35 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 132;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_103 = tmp_mvar_value_103;
            tmp_tuple_element_35 = LOOKUP_ATTRIBUTE( tmp_source_name_103, const_str_plain_BLUE );
            if ( tmp_tuple_element_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_35 );
                Py_DECREF( tmp_dict_value_35 );

                exception_lineno = 132;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_35, 1, tmp_tuple_element_35 );
            tmp_tuple_element_35 = Py_True;
            Py_INCREF( tmp_tuple_element_35 );
            PyTuple_SET_ITEM( tmp_dict_value_35, 2, tmp_tuple_element_35 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_35, tmp_dict_value_35 );
            Py_DECREF( tmp_dict_value_35 );
            Py_DECREF( tmp_dict_key_35 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_104 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_104 == NULL ))
            {
                tmp_mvar_value_104 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_104 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 133;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_104 = tmp_mvar_value_104;
            tmp_dict_key_36 = LOOKUP_ATTRIBUTE( tmp_source_name_104, const_str_plain_LIGHTMAGENTA_EX );
            if ( tmp_dict_key_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 133;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_105 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_105 == NULL ))
            {
                tmp_mvar_value_105 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_105 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_36 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 133;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_105 = tmp_mvar_value_105;
            tmp_tuple_element_36 = LOOKUP_ATTRIBUTE( tmp_source_name_105, const_str_plain_back );
            if ( tmp_tuple_element_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_36 );

                exception_lineno = 133;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_36 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_36, 0, tmp_tuple_element_36 );
            tmp_mvar_value_106 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_106 == NULL ))
            {
                tmp_mvar_value_106 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_106 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_36 );
                Py_DECREF( tmp_dict_value_36 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 133;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_106 = tmp_mvar_value_106;
            tmp_tuple_element_36 = LOOKUP_ATTRIBUTE( tmp_source_name_106, const_str_plain_MAGENTA );
            if ( tmp_tuple_element_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_36 );
                Py_DECREF( tmp_dict_value_36 );

                exception_lineno = 133;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_36, 1, tmp_tuple_element_36 );
            tmp_tuple_element_36 = Py_True;
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_dict_value_36, 2, tmp_tuple_element_36 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_36, tmp_dict_value_36 );
            Py_DECREF( tmp_dict_value_36 );
            Py_DECREF( tmp_dict_key_36 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_107 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_107 == NULL ))
            {
                tmp_mvar_value_107 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_107 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 134;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_107 = tmp_mvar_value_107;
            tmp_dict_key_37 = LOOKUP_ATTRIBUTE( tmp_source_name_107, const_str_plain_LIGHTCYAN_EX );
            if ( tmp_dict_key_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 134;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_108 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_108 == NULL ))
            {
                tmp_mvar_value_108 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_108 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_37 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 134;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_108 = tmp_mvar_value_108;
            tmp_tuple_element_37 = LOOKUP_ATTRIBUTE( tmp_source_name_108, const_str_plain_back );
            if ( tmp_tuple_element_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_37 );

                exception_lineno = 134;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_37 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_37, 0, tmp_tuple_element_37 );
            tmp_mvar_value_109 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_109 == NULL ))
            {
                tmp_mvar_value_109 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_109 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_37 );
                Py_DECREF( tmp_dict_value_37 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 134;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_109 = tmp_mvar_value_109;
            tmp_tuple_element_37 = LOOKUP_ATTRIBUTE( tmp_source_name_109, const_str_plain_CYAN );
            if ( tmp_tuple_element_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_37 );
                Py_DECREF( tmp_dict_value_37 );

                exception_lineno = 134;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_37, 1, tmp_tuple_element_37 );
            tmp_tuple_element_37 = Py_True;
            Py_INCREF( tmp_tuple_element_37 );
            PyTuple_SET_ITEM( tmp_dict_value_37, 2, tmp_tuple_element_37 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_37, tmp_dict_value_37 );
            Py_DECREF( tmp_dict_value_37 );
            Py_DECREF( tmp_dict_key_37 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_110 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack );

            if (unlikely( tmp_mvar_value_110 == NULL ))
            {
                tmp_mvar_value_110 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnsiBack );
            }

            if ( tmp_mvar_value_110 == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnsiBack" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 135;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_110 = tmp_mvar_value_110;
            tmp_dict_key_38 = LOOKUP_ATTRIBUTE( tmp_source_name_110, const_str_plain_LIGHTWHITE_EX );
            if ( tmp_dict_key_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 135;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_111 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

            if (unlikely( tmp_mvar_value_111 == NULL ))
            {
                tmp_mvar_value_111 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
            }

            if ( tmp_mvar_value_111 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_38 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 135;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_111 = tmp_mvar_value_111;
            tmp_tuple_element_38 = LOOKUP_ATTRIBUTE( tmp_source_name_111, const_str_plain_back );
            if ( tmp_tuple_element_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_38 );

                exception_lineno = 135;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_38 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_dict_value_38, 0, tmp_tuple_element_38 );
            tmp_mvar_value_112 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor );

            if (unlikely( tmp_mvar_value_112 == NULL ))
            {
                tmp_mvar_value_112 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinColor );
            }

            if ( tmp_mvar_value_112 == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_38 );
                Py_DECREF( tmp_dict_value_38 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinColor" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 135;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_source_name_112 = tmp_mvar_value_112;
            tmp_tuple_element_38 = LOOKUP_ATTRIBUTE( tmp_source_name_112, const_str_plain_GREY );
            if ( tmp_tuple_element_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_dict_key_38 );
                Py_DECREF( tmp_dict_value_38 );

                exception_lineno = 135;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_dict_value_38, 1, tmp_tuple_element_38 );
            tmp_tuple_element_38 = Py_True;
            Py_INCREF( tmp_tuple_element_38 );
            PyTuple_SET_ITEM( tmp_dict_value_38, 2, tmp_tuple_element_38 );
            tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_38, tmp_dict_value_38 );
            Py_DECREF( tmp_dict_value_38 );
            Py_DECREF( tmp_dict_key_38 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 97;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a003ea1c36c39fae1e7c9dec04eef415 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a003ea1c36c39fae1e7c9dec04eef415 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a003ea1c36c39fae1e7c9dec04eef415 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a003ea1c36c39fae1e7c9dec04eef415, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a003ea1c36c39fae1e7c9dec04eef415->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a003ea1c36c39fae1e7c9dec04eef415, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a003ea1c36c39fae1e7c9dec04eef415,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_a003ea1c36c39fae1e7c9dec04eef415 == cache_frame_a003ea1c36c39fae1e7c9dec04eef415 )
    {
        Py_DECREF( frame_a003ea1c36c39fae1e7c9dec04eef415 );
    }
    cache_frame_a003ea1c36c39fae1e7c9dec04eef415 = NULL;

    assertFrameObject( frame_a003ea1c36c39fae1e7c9dec04eef415 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = PyDict_New();
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_8_get_win32_calls );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_8_get_win32_calls );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_9_write( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_d0ef161b5c50b704b24c67a4dd1d1adb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d0ef161b5c50b704b24c67a4dd1d1adb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d0ef161b5c50b704b24c67a4dd1d1adb, codeobj_d0ef161b5c50b704b24c67a4dd1d1adb, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *) );
    frame_d0ef161b5c50b704b24c67a4dd1d1adb = cache_frame_d0ef161b5c50b704b24c67a4dd1d1adb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d0ef161b5c50b704b24c67a4dd1d1adb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d0ef161b5c50b704b24c67a4dd1d1adb ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_attribute_value_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_strip );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_attribute_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_convert );
        if ( tmp_attribute_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_2 );

            exception_lineno = 140;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_2 );
        tmp_condition_result_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_1 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_self );
            tmp_called_instance_1 = par_self;
            CHECK_OBJECT( par_text );
            tmp_args_element_name_1 = par_text;
            frame_d0ef161b5c50b704b24c67a4dd1d1adb->m_frame.f_lineno = 141;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_write_and_convert, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_wrapped );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_text );
            tmp_args_element_name_2 = par_text;
            frame_d0ef161b5c50b704b24c67a4dd1d1adb->m_frame.f_lineno = 143;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_write, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_wrapped );
            if ( tmp_called_instance_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_d0ef161b5c50b704b24c67a4dd1d1adb->m_frame.f_lineno = 144;
            tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_flush );
            Py_DECREF( tmp_called_instance_3 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_end_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_source_name_5;
        PyObject *tmp_attribute_value_3;
        int tmp_truth_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_attribute_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_autoreset );
        if ( tmp_attribute_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_attribute_value_3 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_3 );

            exception_lineno = 145;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_3 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_instance_4;
            PyObject *tmp_call_result_4;
            CHECK_OBJECT( par_self );
            tmp_called_instance_4 = par_self;
            frame_d0ef161b5c50b704b24c67a4dd1d1adb->m_frame.f_lineno = 146;
            tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_reset_all );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 146;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d0ef161b5c50b704b24c67a4dd1d1adb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d0ef161b5c50b704b24c67a4dd1d1adb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d0ef161b5c50b704b24c67a4dd1d1adb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d0ef161b5c50b704b24c67a4dd1d1adb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d0ef161b5c50b704b24c67a4dd1d1adb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d0ef161b5c50b704b24c67a4dd1d1adb,
        type_description_1,
        par_self,
        par_text
    );


    // Release cached frame.
    if ( frame_d0ef161b5c50b704b24c67a4dd1d1adb == cache_frame_d0ef161b5c50b704b24c67a4dd1d1adb )
    {
        Py_DECREF( frame_d0ef161b5c50b704b24c67a4dd1d1adb );
    }
    cache_frame_d0ef161b5c50b704b24c67a4dd1d1adb = NULL;

    assertFrameObject( frame_d0ef161b5c50b704b24c67a4dd1d1adb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_9_write );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_9_write );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_10_reset_all( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_cf5052a1b20aa30dd29b919afb9d8888;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_cf5052a1b20aa30dd29b919afb9d8888 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cf5052a1b20aa30dd29b919afb9d8888, codeobj_cf5052a1b20aa30dd29b919afb9d8888, module_colorama$ansitowin32, sizeof(void *) );
    frame_cf5052a1b20aa30dd29b919afb9d8888 = cache_frame_cf5052a1b20aa30dd29b919afb9d8888;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cf5052a1b20aa30dd29b919afb9d8888 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cf5052a1b20aa30dd29b919afb9d8888 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_convert );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 150;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_self );
            tmp_called_instance_1 = par_self;
            frame_cf5052a1b20aa30dd29b919afb9d8888->m_frame.f_lineno = 151;
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_call_win32, &PyTuple_GET_ITEM( const_tuple_str_plain_m_tuple_int_0_tuple_tuple, 0 ) );

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_strip );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 152;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 152;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_is_stream_closed );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_stream_closed );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_stream_closed" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 152;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_wrapped );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 152;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_cf5052a1b20aa30dd29b919afb9d8888->m_frame.f_lineno = 152;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_operand_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_operand_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 152;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            Py_DECREF( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 152;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_2 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_name_2;
                PyObject *tmp_source_name_4;
                PyObject *tmp_source_name_5;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_source_name_6;
                PyObject *tmp_mvar_value_2;
                CHECK_OBJECT( par_self );
                tmp_source_name_5 = par_self;
                tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_wrapped );
                if ( tmp_source_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_write );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_called_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_Style );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Style );
                }

                if ( tmp_mvar_value_2 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Style" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 153;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_6 = tmp_mvar_value_2;
                tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_RESET_ALL );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_2 );

                    exception_lineno = 153;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                frame_cf5052a1b20aa30dd29b919afb9d8888->m_frame.f_lineno = 153;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                }

                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cf5052a1b20aa30dd29b919afb9d8888 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cf5052a1b20aa30dd29b919afb9d8888 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cf5052a1b20aa30dd29b919afb9d8888, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cf5052a1b20aa30dd29b919afb9d8888->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cf5052a1b20aa30dd29b919afb9d8888, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cf5052a1b20aa30dd29b919afb9d8888,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_cf5052a1b20aa30dd29b919afb9d8888 == cache_frame_cf5052a1b20aa30dd29b919afb9d8888 )
    {
        Py_DECREF( frame_cf5052a1b20aa30dd29b919afb9d8888 );
    }
    cache_frame_cf5052a1b20aa30dd29b919afb9d8888 = NULL;

    assertFrameObject( frame_cf5052a1b20aa30dd29b919afb9d8888 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_10_reset_all );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_10_reset_all );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_11_write_and_convert( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *var_cursor = NULL;
    PyObject *var_match = NULL;
    PyObject *var_start = NULL;
    PyObject *var_end = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_4da7e8e481330a4b56a18a75ee8093f4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_4da7e8e481330a4b56a18a75ee8093f4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_int_0;
        assert( var_cursor == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_cursor = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4da7e8e481330a4b56a18a75ee8093f4, codeobj_4da7e8e481330a4b56a18a75ee8093f4, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4da7e8e481330a4b56a18a75ee8093f4 = cache_frame_4da7e8e481330a4b56a18a75ee8093f4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4da7e8e481330a4b56a18a75ee8093f4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4da7e8e481330a4b56a18a75ee8093f4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_4da7e8e481330a4b56a18a75ee8093f4->m_frame.f_lineno = 163;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_convert_osc, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_text;
            assert( old != NULL );
            par_text = tmp_assign_source_2;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ANSI_CSI_RE );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_text );
        tmp_args_element_name_2 = par_text;
        frame_4da7e8e481330a4b56a18a75ee8093f4->m_frame.f_lineno = 164;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_finditer, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                exception_lineno = 164;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_match;
            var_match = tmp_assign_source_5;
            Py_INCREF( var_match );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( var_match );
        tmp_called_instance_3 = var_match;
        frame_4da7e8e481330a4b56a18a75ee8093f4->m_frame.f_lineno = 165;
        tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_span );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 165;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 165;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 165;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 165;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_start;
            var_start = tmp_assign_source_9;
            Py_INCREF( var_start );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_10 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_end;
            var_end = tmp_assign_source_10;
            Py_INCREF( var_end );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( par_self );
        tmp_called_instance_4 = par_self;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_3 = par_text;
        CHECK_OBJECT( var_cursor );
        tmp_args_element_name_4 = var_cursor;
        CHECK_OBJECT( var_start );
        tmp_args_element_name_5 = var_start;
        frame_4da7e8e481330a4b56a18a75ee8093f4->m_frame.f_lineno = 166;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_4, const_str_plain_write_plain_text, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_convert_ansi );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_match );
        tmp_called_instance_5 = var_match;
        frame_4da7e8e481330a4b56a18a75ee8093f4->m_frame.f_lineno = 167;
        tmp_dircall_arg2_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_groups );
        if ( tmp_dircall_arg2_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );

            exception_lineno = 167;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
            tmp_call_result_2 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
        }
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( var_end );
        tmp_assign_source_11 = var_end;
        {
            PyObject *old = var_cursor;
            assert( old != NULL );
            var_cursor = tmp_assign_source_11;
            Py_INCREF( var_cursor );
            Py_DECREF( old );
        }

    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 164;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_write_plain_text );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_text );
        tmp_args_element_name_6 = par_text;
        CHECK_OBJECT( var_cursor );
        tmp_args_element_name_7 = var_cursor;
        CHECK_OBJECT( par_text );
        tmp_len_arg_1 = par_text;
        tmp_args_element_name_8 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 169;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_4da7e8e481330a4b56a18a75ee8093f4->m_frame.f_lineno = 169;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4da7e8e481330a4b56a18a75ee8093f4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4da7e8e481330a4b56a18a75ee8093f4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4da7e8e481330a4b56a18a75ee8093f4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4da7e8e481330a4b56a18a75ee8093f4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4da7e8e481330a4b56a18a75ee8093f4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4da7e8e481330a4b56a18a75ee8093f4,
        type_description_1,
        par_self,
        par_text,
        var_cursor,
        var_match,
        var_start,
        var_end
    );


    // Release cached frame.
    if ( frame_4da7e8e481330a4b56a18a75ee8093f4 == cache_frame_4da7e8e481330a4b56a18a75ee8093f4 )
    {
        Py_DECREF( frame_4da7e8e481330a4b56a18a75ee8093f4 );
    }
    cache_frame_4da7e8e481330a4b56a18a75ee8093f4 = NULL;

    assertFrameObject( frame_4da7e8e481330a4b56a18a75ee8093f4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_11_write_and_convert );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)var_cursor );
    Py_DECREF( var_cursor );
    var_cursor = NULL;

    Py_XDECREF( var_match );
    var_match = NULL;

    Py_XDECREF( var_start );
    var_start = NULL;

    Py_XDECREF( var_end );
    var_end = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_cursor );
    var_cursor = NULL;

    Py_XDECREF( var_match );
    var_match = NULL;

    Py_XDECREF( var_start );
    var_start = NULL;

    Py_XDECREF( var_end );
    var_end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_11_write_and_convert );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_12_write_plain_text( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *par_start = python_pars[ 2 ];
    PyObject *par_end = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_94899803123637240dcec2c2d73832e4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_94899803123637240dcec2c2d73832e4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_94899803123637240dcec2c2d73832e4, codeobj_94899803123637240dcec2c2d73832e4, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_94899803123637240dcec2c2d73832e4 = cache_frame_94899803123637240dcec2c2d73832e4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_94899803123637240dcec2c2d73832e4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_94899803123637240dcec2c2d73832e4 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_start );
        tmp_compexpr_left_1 = par_start;
        CHECK_OBJECT( par_end );
        tmp_compexpr_right_1 = par_end;
        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_start_name_1;
            PyObject *tmp_stop_name_1;
            PyObject *tmp_step_name_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_wrapped );
            if ( tmp_source_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_write );
            Py_DECREF( tmp_source_name_1 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_text );
            tmp_subscribed_name_1 = par_text;
            CHECK_OBJECT( par_start );
            tmp_start_name_1 = par_start;
            CHECK_OBJECT( par_end );
            tmp_stop_name_1 = par_end;
            tmp_step_name_1 = Py_None;
            tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
            assert( !(tmp_subscript_name_1 == NULL) );
            tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscript_name_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 174;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_94899803123637240dcec2c2d73832e4->m_frame.f_lineno = 174;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_wrapped );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_94899803123637240dcec2c2d73832e4->m_frame.f_lineno = 175;
            tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_flush );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_94899803123637240dcec2c2d73832e4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_94899803123637240dcec2c2d73832e4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_94899803123637240dcec2c2d73832e4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_94899803123637240dcec2c2d73832e4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_94899803123637240dcec2c2d73832e4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_94899803123637240dcec2c2d73832e4,
        type_description_1,
        par_self,
        par_text,
        par_start,
        par_end
    );


    // Release cached frame.
    if ( frame_94899803123637240dcec2c2d73832e4 == cache_frame_94899803123637240dcec2c2d73832e4 )
    {
        Py_DECREF( frame_94899803123637240dcec2c2d73832e4 );
    }
    cache_frame_94899803123637240dcec2c2d73832e4 = NULL;

    assertFrameObject( frame_94899803123637240dcec2c2d73832e4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_12_write_plain_text );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_start );
    Py_DECREF( par_start );
    par_start = NULL;

    CHECK_OBJECT( (PyObject *)par_end );
    Py_DECREF( par_end );
    par_end = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_start );
    Py_DECREF( par_start );
    par_start = NULL;

    CHECK_OBJECT( (PyObject *)par_end );
    Py_DECREF( par_end );
    par_end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_12_write_plain_text );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_13_convert_ansi( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_paramstring = python_pars[ 1 ];
    PyObject *par_command = python_pars[ 2 ];
    PyObject *var_params = NULL;
    struct Nuitka_FrameObject *frame_b9f85ad14e06fcaa2244836eea90ab9f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b9f85ad14e06fcaa2244836eea90ab9f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b9f85ad14e06fcaa2244836eea90ab9f, codeobj_b9f85ad14e06fcaa2244836eea90ab9f, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_b9f85ad14e06fcaa2244836eea90ab9f = cache_frame_b9f85ad14e06fcaa2244836eea90ab9f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b9f85ad14e06fcaa2244836eea90ab9f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b9f85ad14e06fcaa2244836eea90ab9f ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_convert );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 179;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_self );
            tmp_called_instance_1 = par_self;
            CHECK_OBJECT( par_command );
            tmp_args_element_name_1 = par_command;
            CHECK_OBJECT( par_paramstring );
            tmp_args_element_name_2 = par_paramstring;
            frame_b9f85ad14e06fcaa2244836eea90ab9f->m_frame.f_lineno = 180;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_extract_params, call_args );
            }

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_params == NULL );
            var_params = tmp_assign_source_1;
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( par_self );
            tmp_called_instance_2 = par_self;
            CHECK_OBJECT( par_command );
            tmp_args_element_name_3 = par_command;
            CHECK_OBJECT( var_params );
            tmp_args_element_name_4 = var_params;
            frame_b9f85ad14e06fcaa2244836eea90ab9f->m_frame.f_lineno = 181;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_call_win32, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 181;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b9f85ad14e06fcaa2244836eea90ab9f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b9f85ad14e06fcaa2244836eea90ab9f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b9f85ad14e06fcaa2244836eea90ab9f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b9f85ad14e06fcaa2244836eea90ab9f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b9f85ad14e06fcaa2244836eea90ab9f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b9f85ad14e06fcaa2244836eea90ab9f,
        type_description_1,
        par_self,
        par_paramstring,
        par_command,
        var_params
    );


    // Release cached frame.
    if ( frame_b9f85ad14e06fcaa2244836eea90ab9f == cache_frame_b9f85ad14e06fcaa2244836eea90ab9f )
    {
        Py_DECREF( frame_b9f85ad14e06fcaa2244836eea90ab9f );
    }
    cache_frame_b9f85ad14e06fcaa2244836eea90ab9f = NULL;

    assertFrameObject( frame_b9f85ad14e06fcaa2244836eea90ab9f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_13_convert_ansi );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_paramstring );
    Py_DECREF( par_paramstring );
    par_paramstring = NULL;

    CHECK_OBJECT( (PyObject *)par_command );
    Py_DECREF( par_command );
    par_command = NULL;

    Py_XDECREF( var_params );
    var_params = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_paramstring );
    Py_DECREF( par_paramstring );
    par_paramstring = NULL;

    CHECK_OBJECT( (PyObject *)par_command );
    Py_DECREF( par_command );
    par_command = NULL;

    Py_XDECREF( var_params );
    var_params = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_13_convert_ansi );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_14_extract_params( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_command = python_pars[ 1 ];
    PyObject *par_paramstring = python_pars[ 2 ];
    PyObject *var_params = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_genexpr_2__$0 = NULL;
    struct Nuitka_FrameObject *frame_115269f6e409823d899b6879fc9d15af;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_115269f6e409823d899b6879fc9d15af = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_115269f6e409823d899b6879fc9d15af, codeobj_115269f6e409823d899b6879fc9d15af, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_115269f6e409823d899b6879fc9d15af = cache_frame_115269f6e409823d899b6879fc9d15af;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_115269f6e409823d899b6879fc9d15af );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_115269f6e409823d899b6879fc9d15af ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_command );
        tmp_compexpr_left_1 = par_command;
        tmp_compexpr_right_1 = const_str_plain_Hf;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_tuple_arg_1;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_iter_arg_1;
                PyObject *tmp_called_instance_1;
                CHECK_OBJECT( par_paramstring );
                tmp_called_instance_1 = par_paramstring;
                frame_115269f6e409823d899b6879fc9d15af->m_frame.f_lineno = 186;
                tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_59_tuple, 0 ) );

                if ( tmp_iter_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 186;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
                Py_DECREF( tmp_iter_arg_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 186;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_genexpr_1__$0 == NULL );
                tmp_genexpr_1__$0 = tmp_assign_source_2;
            }
            // Tried code:
            tmp_tuple_arg_1 = colorama$ansitowin32$$$function_14_extract_params$$$genexpr_1_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_tuple_arg_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


            goto try_return_handler_2;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_14_extract_params );
            return NULL;
            // Return handler code:
            try_return_handler_2:;
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            goto outline_result_1;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_14_extract_params );
            return NULL;
            outline_result_1:;
            tmp_assign_source_1 = PySequence_Tuple( tmp_tuple_arg_1 );
            Py_DECREF( tmp_tuple_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 186;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_params == NULL );
            var_params = tmp_assign_source_1;
        }
        loop_start_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_len_arg_1;
            CHECK_OBJECT( var_params );
            tmp_len_arg_1 = var_params;
            tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 187;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_int_pos_2;
            tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            goto loop_end_1;
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            CHECK_OBJECT( var_params );
            tmp_left_name_1 = var_params;
            tmp_right_name_1 = const_tuple_int_pos_1_tuple;
            tmp_assign_source_3 = BINARY_OPERATION_ADD_OBJECT_TUPLE( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_params;
                assert( old != NULL );
                var_params = tmp_assign_source_3;
                Py_DECREF( old );
            }

        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto loop_start_1;
        loop_end_1:;
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_tuple_arg_2;
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_iter_arg_2;
                PyObject *tmp_called_instance_2;
                CHECK_OBJECT( par_paramstring );
                tmp_called_instance_2 = par_paramstring;
                frame_115269f6e409823d899b6879fc9d15af->m_frame.f_lineno = 191;
                tmp_iter_arg_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_59_tuple, 0 ) );

                if ( tmp_iter_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 191;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_2 );
                Py_DECREF( tmp_iter_arg_2 );
                if ( tmp_assign_source_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 191;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_genexpr_2__$0 == NULL );
                tmp_genexpr_2__$0 = tmp_assign_source_5;
            }
            // Tried code:
            tmp_tuple_arg_2 = colorama$ansitowin32$$$function_14_extract_params$$$genexpr_2_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_tuple_arg_2)->m_closure[0] = PyCell_NEW0( tmp_genexpr_2__$0 );


            goto try_return_handler_3;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_14_extract_params );
            return NULL;
            // Return handler code:
            try_return_handler_3:;
            CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
            Py_DECREF( tmp_genexpr_2__$0 );
            tmp_genexpr_2__$0 = NULL;

            goto outline_result_2;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
            Py_DECREF( tmp_genexpr_2__$0 );
            tmp_genexpr_2__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_14_extract_params );
            return NULL;
            outline_result_2:;
            tmp_assign_source_4 = PySequence_Tuple( tmp_tuple_arg_2 );
            Py_DECREF( tmp_tuple_arg_2 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 191;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_params == NULL );
            var_params = tmp_assign_source_4;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_len_arg_2;
            CHECK_OBJECT( var_params );
            tmp_len_arg_2 = var_params;
            tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_2 );
            assert( !(tmp_compexpr_left_3 == NULL) );
            tmp_compexpr_right_3 = const_int_0;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                CHECK_OBJECT( par_command );
                tmp_compexpr_left_4 = par_command;
                tmp_compexpr_right_4 = const_str_plain_JKm;
                tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 194;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_6;
                    tmp_assign_source_6 = const_tuple_int_0_tuple;
                    {
                        PyObject *old = var_params;
                        assert( old != NULL );
                        var_params = tmp_assign_source_6;
                        Py_INCREF( var_params );
                        Py_DECREF( old );
                    }

                }
                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    CHECK_OBJECT( par_command );
                    tmp_compexpr_left_5 = par_command;
                    tmp_compexpr_right_5 = const_str_plain_ABCD;
                    tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 196;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_5 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_assign_source_7;
                        tmp_assign_source_7 = const_tuple_int_pos_1_tuple;
                        {
                            PyObject *old = var_params;
                            assert( old != NULL );
                            var_params = tmp_assign_source_7;
                            Py_INCREF( var_params );
                            Py_DECREF( old );
                        }

                    }
                    branch_no_5:;
                }
                branch_end_4:;
            }
            branch_no_3:;
        }
        branch_end_1:;
    }
    if ( var_params == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "params" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 199;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_return_value = var_params;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_115269f6e409823d899b6879fc9d15af );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_115269f6e409823d899b6879fc9d15af );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_115269f6e409823d899b6879fc9d15af );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_115269f6e409823d899b6879fc9d15af, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_115269f6e409823d899b6879fc9d15af->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_115269f6e409823d899b6879fc9d15af, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_115269f6e409823d899b6879fc9d15af,
        type_description_1,
        par_self,
        par_command,
        par_paramstring,
        var_params
    );


    // Release cached frame.
    if ( frame_115269f6e409823d899b6879fc9d15af == cache_frame_115269f6e409823d899b6879fc9d15af )
    {
        Py_DECREF( frame_115269f6e409823d899b6879fc9d15af );
    }
    cache_frame_115269f6e409823d899b6879fc9d15af = NULL;

    assertFrameObject( frame_115269f6e409823d899b6879fc9d15af );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_14_extract_params );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_command );
    Py_DECREF( par_command );
    par_command = NULL;

    CHECK_OBJECT( (PyObject *)par_paramstring );
    Py_DECREF( par_paramstring );
    par_paramstring = NULL;

    Py_XDECREF( var_params );
    var_params = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_command );
    Py_DECREF( par_command );
    par_command = NULL;

    CHECK_OBJECT( (PyObject *)par_paramstring );
    Py_DECREF( par_paramstring );
    par_paramstring = NULL;

    Py_XDECREF( var_params );
    var_params = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_14_extract_params );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct colorama$ansitowin32$$$function_14_extract_params$$$genexpr_1_genexpr_locals {
    PyObject *var_p;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *colorama$ansitowin32$$$function_14_extract_params$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct colorama$ansitowin32$$$function_14_extract_params$$$genexpr_1_genexpr_locals *generator_heap = (struct colorama$ansitowin32$$$function_14_extract_params$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_p = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_9e58dcca515e2ebc4da0213de2a9fe04, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 186;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_p;
            generator_heap->var_p = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_p );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_int_arg_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_p );
        tmp_len_arg_1 = generator_heap->var_p;
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 186;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_1 = const_int_0;
        generator_heap->tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(generator_heap->tmp_res == -1) );
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( generator_heap->var_p );
        tmp_int_arg_1 = generator_heap->var_p;
        tmp_expression_name_1 = PyNumber_Int( tmp_int_arg_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 186;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_expression_name_1 = const_int_pos_1;
        Py_INCREF( tmp_expression_name_1 );
        condexpr_end_1:;
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_int_arg_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_int_arg_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 186;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 186;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_p
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_p );
    generator_heap->var_p = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_p );
    generator_heap->var_p = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *colorama$ansitowin32$$$function_14_extract_params$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        colorama$ansitowin32$$$function_14_extract_params$$$genexpr_1_genexpr_context,
        module_colorama$ansitowin32,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_6d8abad3a3b124a7281f9dc9e1e34a11,
#endif
        codeobj_9e58dcca515e2ebc4da0213de2a9fe04,
        1,
        sizeof(struct colorama$ansitowin32$$$function_14_extract_params$$$genexpr_1_genexpr_locals)
    );
}



struct colorama$ansitowin32$$$function_14_extract_params$$$genexpr_2_genexpr_locals {
    PyObject *var_p;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *colorama$ansitowin32$$$function_14_extract_params$$$genexpr_2_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct colorama$ansitowin32$$$function_14_extract_params$$$genexpr_2_genexpr_locals *generator_heap = (struct colorama$ansitowin32$$$function_14_extract_params$$$genexpr_2_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_p = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_4ef184cd40fed0ed5cbd310342c675a3, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 191;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_p;
            generator_heap->var_p = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_p );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( generator_heap->var_p );
        tmp_len_arg_1 = generator_heap->var_p;
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_1 = const_int_0;
        generator_heap->tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(generator_heap->tmp_res == -1) );
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_expression_name_1;
            PyObject *tmp_int_arg_1;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
            CHECK_OBJECT( generator_heap->var_p );
            tmp_int_arg_1 = generator_heap->var_p;
            tmp_expression_name_1 = PyNumber_Int( tmp_int_arg_1 );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 191;
                generator_heap->type_description_1 = "No";
                goto try_except_handler_2;
            }
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_int_arg_1, sizeof(PyObject *), NULL );
            generator->m_yield_return_index = 1;
            return tmp_expression_name_1;
            yield_return_1:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_int_arg_1, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 191;
                generator_heap->type_description_1 = "No";
                goto try_except_handler_2;
            }
            tmp_yield_result_1 = yield_return_value;
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 191;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_p
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_p );
    generator_heap->var_p = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_p );
    generator_heap->var_p = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *colorama$ansitowin32$$$function_14_extract_params$$$genexpr_2_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        colorama$ansitowin32$$$function_14_extract_params$$$genexpr_2_genexpr_context,
        module_colorama$ansitowin32,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_6d8abad3a3b124a7281f9dc9e1e34a11,
#endif
        codeobj_4ef184cd40fed0ed5cbd310342c675a3,
        1,
        sizeof(struct colorama$ansitowin32$$$function_14_extract_params$$$genexpr_2_genexpr_locals)
    );
}


static PyObject *impl_colorama$ansitowin32$$$function_15_call_win32( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_command = python_pars[ 1 ];
    PyObject *par_params = python_pars[ 2 ];
    PyObject *var_param = NULL;
    PyObject *var_func_args = NULL;
    PyObject *var_func = NULL;
    PyObject *var_args = NULL;
    PyObject *var_kwargs = NULL;
    PyObject *var_n = NULL;
    PyObject *var_x = NULL;
    PyObject *var_y = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_ff111fc7a144fc84db5b1deb24d971e5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_ff111fc7a144fc84db5b1deb24d971e5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ff111fc7a144fc84db5b1deb24d971e5, codeobj_ff111fc7a144fc84db5b1deb24d971e5, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ff111fc7a144fc84db5b1deb24d971e5 = cache_frame_ff111fc7a144fc84db5b1deb24d971e5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ff111fc7a144fc84db5b1deb24d971e5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ff111fc7a144fc84db5b1deb24d971e5 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_command );
        tmp_compexpr_left_1 = par_command;
        tmp_compexpr_right_1 = const_str_plain_m;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_params );
            tmp_iter_arg_1 = par_params;
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 204;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_1;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_2;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooo";
                    exception_lineno = 204;
                    goto try_except_handler_2;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_2;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_3;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_3 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_param;
                var_param = tmp_assign_source_3;
                Py_INCREF( var_param );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( var_param );
            tmp_compexpr_left_2 = var_param;
            CHECK_OBJECT( par_self );
            tmp_source_name_1 = par_self;
            tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_win32_calls );
            if ( tmp_compexpr_right_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_source_name_2;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( par_self );
                tmp_source_name_2 = par_self;
                tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_win32_calls );
                if ( tmp_subscribed_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 206;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_param );
                tmp_subscript_name_1 = var_param;
                tmp_assign_source_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                Py_DECREF( tmp_subscribed_name_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 206;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = var_func_args;
                    var_func_args = tmp_assign_source_4;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                CHECK_OBJECT( var_func_args );
                tmp_subscribed_name_2 = var_func_args;
                tmp_subscript_name_2 = const_int_0;
                tmp_assign_source_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
                if ( tmp_assign_source_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = var_func;
                    var_func = tmp_assign_source_5;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_subscript_name_3;
                CHECK_OBJECT( var_func_args );
                tmp_subscribed_name_3 = var_func_args;
                tmp_subscript_name_3 = const_slice_int_pos_1_none_none;
                tmp_assign_source_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 208;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = var_args;
                    var_args = tmp_assign_source_6;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_7;
                PyObject *tmp_dict_key_1;
                PyObject *tmp_dict_value_1;
                PyObject *tmp_source_name_3;
                tmp_dict_key_1 = const_str_plain_on_stderr;
                CHECK_OBJECT( par_self );
                tmp_source_name_3 = par_self;
                tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_on_stderr );
                if ( tmp_dict_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 209;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_assign_source_7 = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_1, tmp_dict_value_1 );
                Py_DECREF( tmp_dict_value_1 );
                assert( !(tmp_res != 0) );
                {
                    PyObject *old = var_kwargs;
                    var_kwargs = tmp_assign_source_7;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_dircall_arg1_1;
                PyObject *tmp_dircall_arg2_1;
                PyObject *tmp_dircall_arg3_1;
                PyObject *tmp_call_result_1;
                CHECK_OBJECT( var_func );
                tmp_dircall_arg1_1 = var_func;
                CHECK_OBJECT( var_args );
                tmp_dircall_arg2_1 = var_args;
                CHECK_OBJECT( var_kwargs );
                tmp_dircall_arg3_1 = var_kwargs;
                Py_INCREF( tmp_dircall_arg1_1 );
                Py_INCREF( tmp_dircall_arg2_1 );
                Py_INCREF( tmp_dircall_arg3_1 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
                    tmp_call_result_1 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
                }
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 210;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            branch_no_2:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( par_command );
            tmp_compexpr_left_3 = par_command;
            tmp_compexpr_right_3 = const_str_plain_J;
            tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_subscribed_name_4;
                PyObject *tmp_subscript_name_4;
                PyObject *tmp_kw_name_1;
                PyObject *tmp_dict_key_2;
                PyObject *tmp_dict_value_2;
                PyObject *tmp_source_name_5;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 212;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_4 = tmp_mvar_value_1;
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_erase_screen );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 212;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_params );
                tmp_subscribed_name_4 = par_params;
                tmp_subscript_name_4 = const_int_0;
                tmp_tuple_element_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );

                    exception_lineno = 212;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_name_1 = PyTuple_New( 1 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
                tmp_dict_key_2 = const_str_plain_on_stderr;
                CHECK_OBJECT( par_self );
                tmp_source_name_5 = par_self;
                tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_on_stderr );
                if ( tmp_dict_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_args_name_1 );

                    exception_lineno = 212;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_kw_name_1 = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
                Py_DECREF( tmp_dict_value_2 );
                assert( !(tmp_res != 0) );
                frame_ff111fc7a144fc84db5b1deb24d971e5->m_frame.f_lineno = 212;
                tmp_call_result_2 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 212;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                CHECK_OBJECT( par_command );
                tmp_compexpr_left_4 = par_command;
                tmp_compexpr_right_4 = const_str_plain_K;
                tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 213;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_source_name_6;
                    PyObject *tmp_mvar_value_2;
                    PyObject *tmp_call_result_3;
                    PyObject *tmp_args_name_2;
                    PyObject *tmp_tuple_element_2;
                    PyObject *tmp_subscribed_name_5;
                    PyObject *tmp_subscript_name_5;
                    PyObject *tmp_kw_name_2;
                    PyObject *tmp_dict_key_3;
                    PyObject *tmp_dict_value_3;
                    PyObject *tmp_source_name_7;
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

                    if (unlikely( tmp_mvar_value_2 == NULL ))
                    {
                        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
                    }

                    if ( tmp_mvar_value_2 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 214;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_6 = tmp_mvar_value_2;
                    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_erase_line );
                    if ( tmp_called_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 214;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( par_params );
                    tmp_subscribed_name_5 = par_params;
                    tmp_subscript_name_5 = const_int_0;
                    tmp_tuple_element_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 0 );
                    if ( tmp_tuple_element_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 214;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_args_name_2 = PyTuple_New( 1 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
                    tmp_dict_key_3 = const_str_plain_on_stderr;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_7 = par_self;
                    tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_on_stderr );
                    if ( tmp_dict_value_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );
                        Py_DECREF( tmp_args_name_2 );

                        exception_lineno = 214;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_kw_name_2 = _PyDict_NewPresized( 1 );
                    tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
                    Py_DECREF( tmp_dict_value_3 );
                    assert( !(tmp_res != 0) );
                    frame_ff111fc7a144fc84db5b1deb24d971e5->m_frame.f_lineno = 214;
                    tmp_call_result_3 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_args_name_2 );
                    Py_DECREF( tmp_kw_name_2 );
                    if ( tmp_call_result_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 214;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_3 );
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    CHECK_OBJECT( par_command );
                    tmp_compexpr_left_5 = par_command;
                    tmp_compexpr_right_5 = const_str_plain_Hf;
                    tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 215;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_5 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_called_name_3;
                        PyObject *tmp_source_name_8;
                        PyObject *tmp_mvar_value_3;
                        PyObject *tmp_call_result_4;
                        PyObject *tmp_args_name_3;
                        PyObject *tmp_tuple_element_3;
                        PyObject *tmp_kw_name_3;
                        PyObject *tmp_dict_key_4;
                        PyObject *tmp_dict_value_4;
                        PyObject *tmp_source_name_9;
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

                        if (unlikely( tmp_mvar_value_3 == NULL ))
                        {
                            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
                        }

                        if ( tmp_mvar_value_3 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 216;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_source_name_8 = tmp_mvar_value_3;
                        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_set_cursor_position );
                        if ( tmp_called_name_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 216;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        CHECK_OBJECT( par_params );
                        tmp_tuple_element_3 = par_params;
                        tmp_args_name_3 = PyTuple_New( 1 );
                        Py_INCREF( tmp_tuple_element_3 );
                        PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_3 );
                        tmp_dict_key_4 = const_str_plain_on_stderr;
                        CHECK_OBJECT( par_self );
                        tmp_source_name_9 = par_self;
                        tmp_dict_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_on_stderr );
                        if ( tmp_dict_value_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_called_name_3 );
                            Py_DECREF( tmp_args_name_3 );

                            exception_lineno = 216;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_kw_name_3 = _PyDict_NewPresized( 1 );
                        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_4, tmp_dict_value_4 );
                        Py_DECREF( tmp_dict_value_4 );
                        assert( !(tmp_res != 0) );
                        frame_ff111fc7a144fc84db5b1deb24d971e5->m_frame.f_lineno = 216;
                        tmp_call_result_4 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
                        Py_DECREF( tmp_called_name_3 );
                        Py_DECREF( tmp_args_name_3 );
                        Py_DECREF( tmp_kw_name_3 );
                        if ( tmp_call_result_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 216;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        Py_DECREF( tmp_call_result_4 );
                    }
                    goto branch_end_5;
                    branch_no_5:;
                    {
                        nuitka_bool tmp_condition_result_6;
                        PyObject *tmp_compexpr_left_6;
                        PyObject *tmp_compexpr_right_6;
                        CHECK_OBJECT( par_command );
                        tmp_compexpr_left_6 = par_command;
                        tmp_compexpr_right_6 = const_str_plain_ABCD;
                        tmp_res = PySequence_Contains( tmp_compexpr_right_6, tmp_compexpr_left_6 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 217;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_6;
                        }
                        else
                        {
                            goto branch_no_6;
                        }
                        branch_yes_6:;
                        {
                            PyObject *tmp_assign_source_8;
                            PyObject *tmp_subscribed_name_6;
                            PyObject *tmp_subscript_name_6;
                            CHECK_OBJECT( par_params );
                            tmp_subscribed_name_6 = par_params;
                            tmp_subscript_name_6 = const_int_0;
                            tmp_assign_source_8 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
                            if ( tmp_assign_source_8 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 218;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            assert( var_n == NULL );
                            var_n = tmp_assign_source_8;
                        }
                        // Tried code:
                        {
                            PyObject *tmp_assign_source_9;
                            PyObject *tmp_iter_arg_2;
                            PyObject *tmp_subscribed_name_7;
                            PyObject *tmp_dict_key_5;
                            PyObject *tmp_dict_value_5;
                            PyObject *tmp_tuple_element_4;
                            PyObject *tmp_operand_name_1;
                            PyObject *tmp_dict_key_6;
                            PyObject *tmp_dict_value_6;
                            PyObject *tmp_tuple_element_5;
                            PyObject *tmp_dict_key_7;
                            PyObject *tmp_dict_value_7;
                            PyObject *tmp_tuple_element_6;
                            PyObject *tmp_dict_key_8;
                            PyObject *tmp_dict_value_8;
                            PyObject *tmp_tuple_element_7;
                            PyObject *tmp_operand_name_2;
                            PyObject *tmp_subscript_name_7;
                            tmp_dict_key_5 = const_str_plain_A;
                            tmp_tuple_element_4 = const_int_0;
                            tmp_dict_value_5 = PyTuple_New( 2 );
                            Py_INCREF( tmp_tuple_element_4 );
                            PyTuple_SET_ITEM( tmp_dict_value_5, 0, tmp_tuple_element_4 );
                            CHECK_OBJECT( var_n );
                            tmp_operand_name_1 = var_n;
                            tmp_tuple_element_4 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
                            if ( tmp_tuple_element_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_dict_value_5 );

                                exception_lineno = 220;
                                type_description_1 = "ooooooooooo";
                                goto try_except_handler_3;
                            }
                            PyTuple_SET_ITEM( tmp_dict_value_5, 1, tmp_tuple_element_4 );
                            tmp_subscribed_name_7 = _PyDict_NewPresized( 4 );
                            tmp_res = PyDict_SetItem( tmp_subscribed_name_7, tmp_dict_key_5, tmp_dict_value_5 );
                            Py_DECREF( tmp_dict_value_5 );
                            assert( !(tmp_res != 0) );
                            tmp_dict_key_6 = const_str_plain_B;
                            tmp_tuple_element_5 = const_int_0;
                            tmp_dict_value_6 = PyTuple_New( 2 );
                            Py_INCREF( tmp_tuple_element_5 );
                            PyTuple_SET_ITEM( tmp_dict_value_6, 0, tmp_tuple_element_5 );
                            CHECK_OBJECT( var_n );
                            tmp_tuple_element_5 = var_n;
                            Py_INCREF( tmp_tuple_element_5 );
                            PyTuple_SET_ITEM( tmp_dict_value_6, 1, tmp_tuple_element_5 );
                            tmp_res = PyDict_SetItem( tmp_subscribed_name_7, tmp_dict_key_6, tmp_dict_value_6 );
                            Py_DECREF( tmp_dict_value_6 );
                            assert( !(tmp_res != 0) );
                            tmp_dict_key_7 = const_str_plain_C;
                            CHECK_OBJECT( var_n );
                            tmp_tuple_element_6 = var_n;
                            tmp_dict_value_7 = PyTuple_New( 2 );
                            Py_INCREF( tmp_tuple_element_6 );
                            PyTuple_SET_ITEM( tmp_dict_value_7, 0, tmp_tuple_element_6 );
                            tmp_tuple_element_6 = const_int_0;
                            Py_INCREF( tmp_tuple_element_6 );
                            PyTuple_SET_ITEM( tmp_dict_value_7, 1, tmp_tuple_element_6 );
                            tmp_res = PyDict_SetItem( tmp_subscribed_name_7, tmp_dict_key_7, tmp_dict_value_7 );
                            Py_DECREF( tmp_dict_value_7 );
                            assert( !(tmp_res != 0) );
                            tmp_dict_key_8 = const_str_plain_D;
                            CHECK_OBJECT( var_n );
                            tmp_operand_name_2 = var_n;
                            tmp_tuple_element_7 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_2 );
                            if ( tmp_tuple_element_7 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_subscribed_name_7 );

                                exception_lineno = 220;
                                type_description_1 = "ooooooooooo";
                                goto try_except_handler_3;
                            }
                            tmp_dict_value_8 = PyTuple_New( 2 );
                            PyTuple_SET_ITEM( tmp_dict_value_8, 0, tmp_tuple_element_7 );
                            tmp_tuple_element_7 = const_int_0;
                            Py_INCREF( tmp_tuple_element_7 );
                            PyTuple_SET_ITEM( tmp_dict_value_8, 1, tmp_tuple_element_7 );
                            tmp_res = PyDict_SetItem( tmp_subscribed_name_7, tmp_dict_key_8, tmp_dict_value_8 );
                            Py_DECREF( tmp_dict_value_8 );
                            assert( !(tmp_res != 0) );
                            CHECK_OBJECT( par_command );
                            tmp_subscript_name_7 = par_command;
                            tmp_iter_arg_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
                            Py_DECREF( tmp_subscribed_name_7 );
                            if ( tmp_iter_arg_2 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 220;
                                type_description_1 = "ooooooooooo";
                                goto try_except_handler_3;
                            }
                            tmp_assign_source_9 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
                            Py_DECREF( tmp_iter_arg_2 );
                            if ( tmp_assign_source_9 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 220;
                                type_description_1 = "ooooooooooo";
                                goto try_except_handler_3;
                            }
                            assert( tmp_tuple_unpack_1__source_iter == NULL );
                            tmp_tuple_unpack_1__source_iter = tmp_assign_source_9;
                        }
                        // Tried code:
                        {
                            PyObject *tmp_assign_source_10;
                            PyObject *tmp_unpack_1;
                            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                            tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
                            tmp_assign_source_10 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
                            if ( tmp_assign_source_10 == NULL )
                            {
                                if ( !ERROR_OCCURRED() )
                                {
                                    exception_type = PyExc_StopIteration;
                                    Py_INCREF( exception_type );
                                    exception_value = NULL;
                                    exception_tb = NULL;
                                }
                                else
                                {
                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                }


                                type_description_1 = "ooooooooooo";
                                exception_lineno = 220;
                                goto try_except_handler_4;
                            }
                            assert( tmp_tuple_unpack_1__element_1 == NULL );
                            tmp_tuple_unpack_1__element_1 = tmp_assign_source_10;
                        }
                        {
                            PyObject *tmp_assign_source_11;
                            PyObject *tmp_unpack_2;
                            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                            tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
                            tmp_assign_source_11 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
                            if ( tmp_assign_source_11 == NULL )
                            {
                                if ( !ERROR_OCCURRED() )
                                {
                                    exception_type = PyExc_StopIteration;
                                    Py_INCREF( exception_type );
                                    exception_value = NULL;
                                    exception_tb = NULL;
                                }
                                else
                                {
                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                }


                                type_description_1 = "ooooooooooo";
                                exception_lineno = 220;
                                goto try_except_handler_4;
                            }
                            assert( tmp_tuple_unpack_1__element_2 == NULL );
                            tmp_tuple_unpack_1__element_2 = tmp_assign_source_11;
                        }
                        {
                            PyObject *tmp_iterator_name_1;
                            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                            tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
                            // Check if iterator has left-over elements.
                            CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

                            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

                            if (likely( tmp_iterator_attempt == NULL ))
                            {
                                PyObject *error = GET_ERROR_OCCURRED();

                                if ( error != NULL )
                                {
                                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                                    {
                                        CLEAR_ERROR_OCCURRED();
                                    }
                                    else
                                    {
                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                        type_description_1 = "ooooooooooo";
                                        exception_lineno = 220;
                                        goto try_except_handler_4;
                                    }
                                }
                            }
                            else
                            {
                                Py_DECREF( tmp_iterator_attempt );

                                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                type_description_1 = "ooooooooooo";
                                exception_lineno = 220;
                                goto try_except_handler_4;
                            }
                        }
                        goto try_end_2;
                        // Exception handler code:
                        try_except_handler_4:;
                        exception_keeper_type_2 = exception_type;
                        exception_keeper_value_2 = exception_value;
                        exception_keeper_tb_2 = exception_tb;
                        exception_keeper_lineno_2 = exception_lineno;
                        exception_type = NULL;
                        exception_value = NULL;
                        exception_tb = NULL;
                        exception_lineno = 0;

                        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
                        Py_DECREF( tmp_tuple_unpack_1__source_iter );
                        tmp_tuple_unpack_1__source_iter = NULL;

                        // Re-raise.
                        exception_type = exception_keeper_type_2;
                        exception_value = exception_keeper_value_2;
                        exception_tb = exception_keeper_tb_2;
                        exception_lineno = exception_keeper_lineno_2;

                        goto try_except_handler_3;
                        // End of try:
                        try_end_2:;
                        goto try_end_3;
                        // Exception handler code:
                        try_except_handler_3:;
                        exception_keeper_type_3 = exception_type;
                        exception_keeper_value_3 = exception_value;
                        exception_keeper_tb_3 = exception_tb;
                        exception_keeper_lineno_3 = exception_lineno;
                        exception_type = NULL;
                        exception_value = NULL;
                        exception_tb = NULL;
                        exception_lineno = 0;

                        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
                        tmp_tuple_unpack_1__element_1 = NULL;

                        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
                        tmp_tuple_unpack_1__element_2 = NULL;

                        // Re-raise.
                        exception_type = exception_keeper_type_3;
                        exception_value = exception_keeper_value_3;
                        exception_tb = exception_keeper_tb_3;
                        exception_lineno = exception_keeper_lineno_3;

                        goto frame_exception_exit_1;
                        // End of try:
                        try_end_3:;
                        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
                        Py_DECREF( tmp_tuple_unpack_1__source_iter );
                        tmp_tuple_unpack_1__source_iter = NULL;

                        {
                            PyObject *tmp_assign_source_12;
                            CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
                            tmp_assign_source_12 = tmp_tuple_unpack_1__element_1;
                            assert( var_x == NULL );
                            Py_INCREF( tmp_assign_source_12 );
                            var_x = tmp_assign_source_12;
                        }
                        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
                        tmp_tuple_unpack_1__element_1 = NULL;

                        {
                            PyObject *tmp_assign_source_13;
                            CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
                            tmp_assign_source_13 = tmp_tuple_unpack_1__element_2;
                            assert( var_y == NULL );
                            Py_INCREF( tmp_assign_source_13 );
                            var_y = tmp_assign_source_13;
                        }
                        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
                        tmp_tuple_unpack_1__element_2 = NULL;

                        {
                            PyObject *tmp_called_name_4;
                            PyObject *tmp_source_name_10;
                            PyObject *tmp_mvar_value_4;
                            PyObject *tmp_call_result_5;
                            PyObject *tmp_args_name_4;
                            PyObject *tmp_tuple_element_8;
                            PyObject *tmp_kw_name_4;
                            PyObject *tmp_dict_key_9;
                            PyObject *tmp_dict_value_9;
                            PyObject *tmp_source_name_11;
                            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

                            if (unlikely( tmp_mvar_value_4 == NULL ))
                            {
                                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
                            }

                            if ( tmp_mvar_value_4 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 221;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }

                            tmp_source_name_10 = tmp_mvar_value_4;
                            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_cursor_adjust );
                            if ( tmp_called_name_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 221;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            CHECK_OBJECT( var_x );
                            tmp_tuple_element_8 = var_x;
                            tmp_args_name_4 = PyTuple_New( 2 );
                            Py_INCREF( tmp_tuple_element_8 );
                            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
                            CHECK_OBJECT( var_y );
                            tmp_tuple_element_8 = var_y;
                            Py_INCREF( tmp_tuple_element_8 );
                            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
                            tmp_dict_key_9 = const_str_plain_on_stderr;
                            CHECK_OBJECT( par_self );
                            tmp_source_name_11 = par_self;
                            tmp_dict_value_9 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_on_stderr );
                            if ( tmp_dict_value_9 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_called_name_4 );
                                Py_DECREF( tmp_args_name_4 );

                                exception_lineno = 221;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_kw_name_4 = _PyDict_NewPresized( 1 );
                            tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_9, tmp_dict_value_9 );
                            Py_DECREF( tmp_dict_value_9 );
                            assert( !(tmp_res != 0) );
                            frame_ff111fc7a144fc84db5b1deb24d971e5->m_frame.f_lineno = 221;
                            tmp_call_result_5 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_args_name_4 );
                            Py_DECREF( tmp_kw_name_4 );
                            if ( tmp_call_result_5 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 221;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            Py_DECREF( tmp_call_result_5 );
                        }
                        branch_no_6:;
                    }
                    branch_end_5:;
                }
                branch_end_4:;
            }
            branch_end_3:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff111fc7a144fc84db5b1deb24d971e5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff111fc7a144fc84db5b1deb24d971e5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ff111fc7a144fc84db5b1deb24d971e5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ff111fc7a144fc84db5b1deb24d971e5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ff111fc7a144fc84db5b1deb24d971e5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ff111fc7a144fc84db5b1deb24d971e5,
        type_description_1,
        par_self,
        par_command,
        par_params,
        var_param,
        var_func_args,
        var_func,
        var_args,
        var_kwargs,
        var_n,
        var_x,
        var_y
    );


    // Release cached frame.
    if ( frame_ff111fc7a144fc84db5b1deb24d971e5 == cache_frame_ff111fc7a144fc84db5b1deb24d971e5 )
    {
        Py_DECREF( frame_ff111fc7a144fc84db5b1deb24d971e5 );
    }
    cache_frame_ff111fc7a144fc84db5b1deb24d971e5 = NULL;

    assertFrameObject( frame_ff111fc7a144fc84db5b1deb24d971e5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_15_call_win32 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_command );
    Py_DECREF( par_command );
    par_command = NULL;

    CHECK_OBJECT( (PyObject *)par_params );
    Py_DECREF( par_params );
    par_params = NULL;

    Py_XDECREF( var_param );
    var_param = NULL;

    Py_XDECREF( var_func_args );
    var_func_args = NULL;

    Py_XDECREF( var_func );
    var_func = NULL;

    Py_XDECREF( var_args );
    var_args = NULL;

    Py_XDECREF( var_kwargs );
    var_kwargs = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    Py_XDECREF( var_y );
    var_y = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_command );
    Py_DECREF( par_command );
    par_command = NULL;

    CHECK_OBJECT( (PyObject *)par_params );
    Py_DECREF( par_params );
    par_params = NULL;

    Py_XDECREF( var_param );
    var_param = NULL;

    Py_XDECREF( var_func_args );
    var_func_args = NULL;

    Py_XDECREF( var_func );
    var_func = NULL;

    Py_XDECREF( var_args );
    var_args = NULL;

    Py_XDECREF( var_kwargs );
    var_kwargs = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    Py_XDECREF( var_y );
    var_y = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_15_call_win32 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$ansitowin32$$$function_16_convert_osc( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *var_match = NULL;
    PyObject *var_start = NULL;
    PyObject *var_end = NULL;
    PyObject *var_paramstring = NULL;
    PyObject *var_command = NULL;
    PyObject *var_params = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    struct Nuitka_FrameObject *frame_15f0df1a6b992d1b91e723b34d2f497c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    int tmp_res;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_15f0df1a6b992d1b91e723b34d2f497c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_15f0df1a6b992d1b91e723b34d2f497c, codeobj_15f0df1a6b992d1b91e723b34d2f497c, module_colorama$ansitowin32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_15f0df1a6b992d1b91e723b34d2f497c = cache_frame_15f0df1a6b992d1b91e723b34d2f497c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_15f0df1a6b992d1b91e723b34d2f497c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_15f0df1a6b992d1b91e723b34d2f497c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ANSI_OSC_RE );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_15f0df1a6b992d1b91e723b34d2f497c->m_frame.f_lineno = 225;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_finditer, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 225;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_match;
            var_match = tmp_assign_source_3;
            Py_INCREF( var_match );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_match );
        tmp_called_instance_2 = var_match;
        frame_15f0df1a6b992d1b91e723b34d2f497c->m_frame.f_lineno = 226;
        tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_span );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 226;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 226;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooo";
                    exception_lineno = 226;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooo";
            exception_lineno = 226;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_start;
            var_start = tmp_assign_source_7;
            Py_INCREF( var_start );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_end;
            var_end = tmp_assign_source_8;
            Py_INCREF( var_end );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_left_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_step_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_start_name_2;
        PyObject *tmp_stop_name_2;
        PyObject *tmp_step_name_2;
        CHECK_OBJECT( par_text );
        tmp_subscribed_name_1 = par_text;
        tmp_start_name_1 = Py_None;
        CHECK_OBJECT( var_start );
        tmp_stop_name_1 = var_start;
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_left_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_text );
        tmp_subscribed_name_2 = par_text;
        CHECK_OBJECT( var_end );
        tmp_start_name_2 = var_end;
        tmp_stop_name_2 = Py_None;
        tmp_step_name_2 = Py_None;
        tmp_subscript_name_2 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
        assert( !(tmp_subscript_name_2 == NULL) );
        tmp_right_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        Py_DECREF( tmp_subscript_name_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 227;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_9 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = par_text;
            assert( old != NULL );
            par_text = tmp_assign_source_9;
            Py_DECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( var_match );
        tmp_called_instance_3 = var_match;
        frame_15f0df1a6b992d1b91e723b34d2f497c->m_frame.f_lineno = 228;
        tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_groups );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
        tmp_assign_source_10 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__source_iter;
            tmp_tuple_unpack_2__source_iter = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_11 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_11 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 228;
            goto try_except_handler_6;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_1;
            tmp_tuple_unpack_2__element_1 = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_12 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_12 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 228;
            goto try_except_handler_6;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_2;
            tmp_tuple_unpack_2__element_2 = tmp_assign_source_12;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooo";
                    exception_lineno = 228;
                    goto try_except_handler_6;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooo";
            exception_lineno = 228;
            goto try_except_handler_6;
        }
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_5;
    // End of try:
    try_end_3:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_13 = tmp_tuple_unpack_2__element_1;
        {
            PyObject *old = var_paramstring;
            var_paramstring = tmp_assign_source_13;
            Py_INCREF( var_paramstring );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_14;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_14 = tmp_tuple_unpack_2__element_2;
        {
            PyObject *old = var_command;
            var_command = tmp_assign_source_14;
            Py_INCREF( var_command );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_command );
        tmp_compexpr_left_1 = var_command;
        tmp_compexpr_right_1 = const_str_chr_7;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 229;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_called_instance_4;
            CHECK_OBJECT( var_paramstring );
            tmp_called_instance_4 = var_paramstring;
            frame_15f0df1a6b992d1b91e723b34d2f497c->m_frame.f_lineno = 230;
            tmp_assign_source_15 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_59_tuple, 0 ) );

            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 230;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = var_params;
                var_params = tmp_assign_source_15;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_subscript_name_3;
            CHECK_OBJECT( var_params );
            tmp_subscribed_name_3 = var_params;
            tmp_subscript_name_3 = const_int_0;
            tmp_compexpr_left_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            tmp_compexpr_right_2 = const_str_plain_02;
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_2;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_subscribed_name_4;
                PyObject *tmp_subscript_name_4;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_winterm );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "winterm" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 235;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_2;
                }

                tmp_source_name_2 = tmp_mvar_value_1;
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_set_title );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 235;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_params );
                tmp_subscribed_name_4 = var_params;
                tmp_subscript_name_4 = const_int_pos_1;
                tmp_args_element_name_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 1 );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );

                    exception_lineno = 235;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_2;
                }
                frame_15f0df1a6b992d1b91e723b34d2f497c->m_frame.f_lineno = 235;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 235;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            branch_no_2:;
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 225;
        type_description_1 = "oooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_15f0df1a6b992d1b91e723b34d2f497c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_15f0df1a6b992d1b91e723b34d2f497c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_15f0df1a6b992d1b91e723b34d2f497c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_15f0df1a6b992d1b91e723b34d2f497c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_15f0df1a6b992d1b91e723b34d2f497c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_15f0df1a6b992d1b91e723b34d2f497c,
        type_description_1,
        par_self,
        par_text,
        var_match,
        var_start,
        var_end,
        var_paramstring,
        var_command,
        var_params
    );


    // Release cached frame.
    if ( frame_15f0df1a6b992d1b91e723b34d2f497c == cache_frame_15f0df1a6b992d1b91e723b34d2f497c )
    {
        Py_DECREF( frame_15f0df1a6b992d1b91e723b34d2f497c );
    }
    cache_frame_15f0df1a6b992d1b91e723b34d2f497c = NULL;

    assertFrameObject( frame_15f0df1a6b992d1b91e723b34d2f497c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( par_text );
    tmp_return_value = par_text;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_16_convert_osc );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_match );
    var_match = NULL;

    Py_XDECREF( var_start );
    var_start = NULL;

    Py_XDECREF( var_end );
    var_end = NULL;

    Py_XDECREF( var_paramstring );
    var_paramstring = NULL;

    Py_XDECREF( var_command );
    var_command = NULL;

    Py_XDECREF( var_params );
    var_params = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_match );
    var_match = NULL;

    Py_XDECREF( var_start );
    var_start = NULL;

    Py_XDECREF( var_end );
    var_end = NULL;

    Py_XDECREF( var_paramstring );
    var_paramstring = NULL;

    Py_XDECREF( var_command );
    var_command = NULL;

    Py_XDECREF( var_params );
    var_params = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$ansitowin32$$$function_16_convert_osc );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_10_reset_all(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_10_reset_all,
        const_str_plain_reset_all,
#if PYTHON_VERSION >= 300
        const_str_digest_23c27944ab8d207f712ce0dfec9f7e71,
#endif
        codeobj_cf5052a1b20aa30dd29b919afb9d8888,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_11_write_and_convert(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_11_write_and_convert,
        const_str_plain_write_and_convert,
#if PYTHON_VERSION >= 300
        const_str_digest_392147c20a5a9d9a44bc925e35734e74,
#endif
        codeobj_4da7e8e481330a4b56a18a75ee8093f4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        const_str_digest_b55785af5af936c70fc8a8934fb97656,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_12_write_plain_text(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_12_write_plain_text,
        const_str_plain_write_plain_text,
#if PYTHON_VERSION >= 300
        const_str_digest_d0187f6bb3b1368675fb4a5c0a71f6a7,
#endif
        codeobj_94899803123637240dcec2c2d73832e4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_13_convert_ansi(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_13_convert_ansi,
        const_str_plain_convert_ansi,
#if PYTHON_VERSION >= 300
        const_str_digest_bf3478830e517c9cf8c7c37bdb5fda5d,
#endif
        codeobj_b9f85ad14e06fcaa2244836eea90ab9f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_14_extract_params(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_14_extract_params,
        const_str_plain_extract_params,
#if PYTHON_VERSION >= 300
        const_str_digest_34a67dadeab4c4d03f1d54dad88c97b1,
#endif
        codeobj_115269f6e409823d899b6879fc9d15af,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_15_call_win32(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_15_call_win32,
        const_str_plain_call_win32,
#if PYTHON_VERSION >= 300
        const_str_digest_7aaf004607c7f29a352ffea6f27e877f,
#endif
        codeobj_ff111fc7a144fc84db5b1deb24d971e5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_16_convert_osc(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_16_convert_osc,
        const_str_plain_convert_osc,
#if PYTHON_VERSION >= 300
        const_str_digest_fef18de45debf62cfc3d599573cbfc03,
#endif
        codeobj_15f0df1a6b992d1b91e723b34d2f497c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_1_is_stream_closed(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_1_is_stream_closed,
        const_str_plain_is_stream_closed,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6cb02b7c3356f7c1c68a77ab56b4e949,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_2_is_a_tty(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_2_is_a_tty,
        const_str_plain_is_a_tty,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f21898405dd391350f5af25b32d482a1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_3___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_3___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_66a4513a750acaa6ee49c5cda19fde96,
#endif
        codeobj_46d92e07eb089fe5110f225c04032098,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_4___getattr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_4___getattr__,
        const_str_plain___getattr__,
#if PYTHON_VERSION >= 300
        const_str_digest_b554bbb6f75b8ef4430bba0e93759fc9,
#endif
        codeobj_8c9f73d9161c1708130582bb98a58dd0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_5_write(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_5_write,
        const_str_plain_write,
#if PYTHON_VERSION >= 300
        const_str_digest_7896e17c08b161dbfc0846fd3f3b6b00,
#endif
        codeobj_0222d6ff7ea866c930b0d1302657f92e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_6___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_6___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_8b74e46bfedfd735c1c235776b43d037,
#endif
        codeobj_c60fd1aab1357bd227b9619135681256,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_7_should_wrap(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_7_should_wrap,
        const_str_plain_should_wrap,
#if PYTHON_VERSION >= 300
        const_str_digest_8402885c26cd327c9ea84e47155c3b69,
#endif
        codeobj_e03e253bce6ff320b3eabc6ec6832bad,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        const_str_digest_f5af88de3b2f0f8929cc483ea37fac79,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_8_get_win32_calls(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_8_get_win32_calls,
        const_str_plain_get_win32_calls,
#if PYTHON_VERSION >= 300
        const_str_digest_1a5ad2f5181b31351fc00ac34c24ddab,
#endif
        codeobj_a003ea1c36c39fae1e7c9dec04eef415,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$ansitowin32$$$function_9_write(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$ansitowin32$$$function_9_write,
        const_str_plain_write,
#if PYTHON_VERSION >= 300
        const_str_digest_928c809ee71e28d36f2148a6bed057ab,
#endif
        codeobj_d0ef161b5c50b704b24c67a4dd1d1adb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$ansitowin32,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_colorama$ansitowin32 =
{
    PyModuleDef_HEAD_INIT,
    "colorama.ansitowin32",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(colorama$ansitowin32)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(colorama$ansitowin32)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_colorama$ansitowin32 );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("colorama.ansitowin32: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("colorama.ansitowin32: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("colorama.ansitowin32: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initcolorama$ansitowin32" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_colorama$ansitowin32 = Py_InitModule4(
        "colorama.ansitowin32",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_colorama$ansitowin32 = PyModule_Create( &mdef_colorama$ansitowin32 );
#endif

    moduledict_colorama$ansitowin32 = MODULE_DICT( module_colorama$ansitowin32 );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_colorama$ansitowin32,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_colorama$ansitowin32,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_colorama$ansitowin32,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_colorama$ansitowin32,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_colorama$ansitowin32 );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_0ba5e85965a3daf6ffa2bae2718fb27f, module_colorama$ansitowin32 );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_c7c1edf74d76a4f7d3531ba1a6f72392;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_colorama$ansitowin32_24 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_10f461ff57d79775dd6ad24cd4d0fa8f_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *locals_colorama$ansitowin32_43 = NULL;
    struct Nuitka_FrameObject *frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_c7c1edf74d76a4f7d3531ba1a6f72392 = MAKE_MODULE_FRAME( codeobj_c7c1edf74d76a4f7d3531ba1a6f72392, module_colorama$ansitowin32 );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_c7c1edf74d76a4f7d3531ba1a6f72392 );
    assert( Py_REFCNT( frame_c7c1edf74d76a4f7d3531ba1a6f72392 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_re;
        tmp_globals_name_1 = (PyObject *)moduledict_colorama$ansitowin32;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 2;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_sys;
        tmp_globals_name_2 = (PyObject *)moduledict_colorama$ansitowin32;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 3;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        assert( !(tmp_assign_source_5 == NULL) );
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_os;
        tmp_globals_name_3 = (PyObject *)moduledict_colorama$ansitowin32;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 4;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_ansi;
        tmp_globals_name_4 = (PyObject *)moduledict_colorama$ansitowin32;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_8f81b70d2be11e6473ba94714a88c33f_tuple;
        tmp_level_name_4 = const_int_pos_1;
        frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 6;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_7;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_8 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_colorama$ansitowin32,
                const_str_plain_AnsiFore,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_AnsiFore );
        }

        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiFore, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_9 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_colorama$ansitowin32,
                const_str_plain_AnsiBack,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_AnsiBack );
        }

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiBack, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_3 ) )
        {
           tmp_assign_source_10 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_3,
                (PyObject *)moduledict_colorama$ansitowin32,
                const_str_plain_AnsiStyle,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_AnsiStyle );
        }

        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiStyle, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_4 ) )
        {
           tmp_assign_source_11 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_4,
                (PyObject *)moduledict_colorama$ansitowin32,
                const_str_plain_Style,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Style );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_Style, tmp_assign_source_11 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_winterm;
        tmp_globals_name_5 = (PyObject *)moduledict_colorama$ansitowin32;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_WinTerm_str_plain_WinColor_str_plain_WinStyle_tuple;
        tmp_level_name_5 = const_int_pos_1;
        frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 7;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_12;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_5 ) )
        {
           tmp_assign_source_13 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_5,
                (PyObject *)moduledict_colorama$ansitowin32,
                const_str_plain_WinTerm,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_WinTerm );
        }

        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinTerm, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_6 ) )
        {
           tmp_assign_source_14 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_6,
                (PyObject *)moduledict_colorama$ansitowin32,
                const_str_plain_WinColor,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_WinColor );
        }

        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinColor, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_7 ) )
        {
           tmp_assign_source_15 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_7,
                (PyObject *)moduledict_colorama$ansitowin32,
                const_str_plain_WinStyle,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_WinStyle );
        }

        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinStyle, tmp_assign_source_15 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_win32;
        tmp_globals_name_6 = (PyObject *)moduledict_colorama$ansitowin32;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_windll_str_plain_winapi_test_tuple;
        tmp_level_name_6 = const_int_pos_1;
        frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 8;
        tmp_assign_source_16 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_16;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_8 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_17 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_colorama$ansitowin32,
                const_str_plain_windll,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_windll );
        }

        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_windll, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_9 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_9 ) )
        {
           tmp_assign_source_18 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_9,
                (PyObject *)moduledict_colorama$ansitowin32,
                const_str_plain_winapi_test,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_winapi_test );
        }

        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winapi_test, tmp_assign_source_18 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = Py_None;
        UPDATE_STRING_DICT0( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm, tmp_assign_source_19 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_windll );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_windll );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "windll" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 12;

            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = tmp_mvar_value_3;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_4;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_WinTerm );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WinTerm );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WinTerm" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 13;

                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_4;
            frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 13;
            tmp_assign_source_20 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            if ( tmp_assign_source_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 13;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_winterm, tmp_assign_source_20 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_colorama$ansitowin32$$$function_1_is_stream_closed(  );



        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_is_stream_closed, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = MAKE_FUNCTION_colorama$ansitowin32$$$function_2_is_a_tty(  );



        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_is_a_tty, tmp_assign_source_22 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_23 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_23;
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_24;
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_4;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_4;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_4;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_4;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_4;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_25 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_25;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_4;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_4;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;

                goto try_except_handler_4;
            }
            tmp_tuple_element_1 = const_str_plain_StreamWrapper;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 24;
            tmp_assign_source_26 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_26;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;

                goto try_except_handler_4;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 24;

                    goto try_except_handler_4;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 24;

                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 24;

                    goto try_except_handler_4;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 24;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_27;
            tmp_assign_source_27 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_27;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_28;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_colorama$ansitowin32_24 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_0ba5e85965a3daf6ffa2bae2718fb27f;
        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_24, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_digest_33d0019995e5085fa6592d50035e8a67;
        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_24, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_plain_StreamWrapper;
        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_24, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_10f461ff57d79775dd6ad24cd4d0fa8f_2, codeobj_10f461ff57d79775dd6ad24cd4d0fa8f, module_colorama$ansitowin32, sizeof(void *) );
        frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 = cache_frame_10f461ff57d79775dd6ad24cd4d0fa8f_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_3___init__(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_24, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_4___getattr__(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_24, const_str_plain___getattr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_5_write(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_24, const_str_plain_write, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_10f461ff57d79775dd6ad24cd4d0fa8f_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_10f461ff57d79775dd6ad24cd4d0fa8f_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_10f461ff57d79775dd6ad24cd4d0fa8f_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_10f461ff57d79775dd6ad24cd4d0fa8f_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 == cache_frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 )
        {
            Py_DECREF( frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 );
        }
        cache_frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 = NULL;

        assertFrameObject( frame_10f461ff57d79775dd6ad24cd4d0fa8f_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_6;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_2 = tmp_class_creation_1__bases;
            tmp_compexpr_right_2 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;

                goto try_except_handler_6;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_24, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;

                goto try_except_handler_6;
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_3 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_StreamWrapper;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_colorama$ansitowin32_24;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 24;
            tmp_assign_source_29 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;

                goto try_except_handler_6;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_29;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_28 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_28 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( colorama$ansitowin32 );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        Py_DECREF( locals_colorama$ansitowin32_24 );
        locals_colorama$ansitowin32_24 = NULL;
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_colorama$ansitowin32_24 );
        locals_colorama$ansitowin32_24 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( colorama$ansitowin32 );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( colorama$ansitowin32 );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 24;
        goto try_except_handler_4;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_StreamWrapper, tmp_assign_source_28 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_dircall_arg1_2;
        tmp_dircall_arg1_2 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_30 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_30;
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_7;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_7;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_7;
        }
        tmp_condition_result_9 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_7;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_7;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_32 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_32;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_7;
        }
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_7;
        }
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_7;
            }
            tmp_tuple_element_4 = const_str_plain_AnsiToWin32;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_4 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 43;
            tmp_assign_source_33 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_7;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_33;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_7;
            }
            tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_5;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_5 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 43;

                    goto try_except_handler_7;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_5 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 43;

                    goto try_except_handler_7;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_5 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 43;

                    goto try_except_handler_7;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 43;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_7;
            }
            branch_no_8:;
        }
        goto branch_end_7;
        branch_no_7:;
        {
            PyObject *tmp_assign_source_34;
            tmp_assign_source_34 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_34;
        }
        branch_end_7:;
    }
    {
        PyObject *tmp_assign_source_35;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_colorama$ansitowin32_43 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_0ba5e85965a3daf6ffa2bae2718fb27f;
        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_digest_890d683a23990d5f4204f0081b005a5d;
        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_plain_AnsiToWin32;
        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto try_except_handler_9;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3, codeobj_0bcb4b72c940cc4aca9a50383f2a0a0c, module_colorama$ansitowin32, sizeof(void *) );
        frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 = cache_frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_5;
            tmp_called_instance_1 = PyObject_GetItem( locals_colorama$ansitowin32_43, const_str_plain_re );

            if ( tmp_called_instance_1 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 49;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_called_instance_1 = tmp_mvar_value_5;
                Py_INCREF( tmp_called_instance_1 );
                }
            }

            frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3->m_frame.f_lineno = 49;
            tmp_dictset_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_12f00e9c5ea1145863283678c119e25d_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_ANSI_CSI_RE, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_6;
            tmp_called_instance_2 = PyObject_GetItem( locals_colorama$ansitowin32_43, const_str_plain_re );

            if ( tmp_called_instance_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 50;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_called_instance_2 = tmp_mvar_value_6;
                Py_INCREF( tmp_called_instance_2 );
                }
            }

            frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3->m_frame.f_lineno = 50;
            tmp_dictset_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_fbcd3ce8b63fb326a2a412670188a257_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_ANSI_OSC_RE, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_none_false_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_6___init__( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_7_should_wrap(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_should_wrap, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_8_get_win32_calls(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_get_win32_calls, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_9_write(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_write, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_10_reset_all(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_reset_all, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_11_write_and_convert(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_write_and_convert, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_12_write_plain_text(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_write_plain_text, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_13_convert_ansi(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_convert_ansi, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_14_extract_params(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_extract_params, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_15_call_win32(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_call_win32, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_colorama$ansitowin32$$$function_16_convert_osc(  );



        tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain_convert_osc, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 == cache_frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 )
        {
            Py_DECREF( frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 );
        }
        cache_frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 = NULL;

        assertFrameObject( frame_0bcb4b72c940cc4aca9a50383f2a0a0c_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_9;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_3 = tmp_class_creation_2__bases;
            tmp_compexpr_right_3 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_9;
            }
            tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_colorama$ansitowin32_43, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_9;
            }
            branch_no_9:;
        }
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_5 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_6 = const_str_plain_AnsiToWin32;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_6 );
            tmp_tuple_element_6 = locals_colorama$ansitowin32_43;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame.f_lineno = 43;
            tmp_assign_source_36 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_9;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_36;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_35 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_35 );
        goto try_return_handler_9;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( colorama$ansitowin32 );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        Py_DECREF( locals_colorama$ansitowin32_43 );
        locals_colorama$ansitowin32_43 = NULL;
        goto try_return_handler_8;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_colorama$ansitowin32_43 );
        locals_colorama$ansitowin32_43 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_8;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( colorama$ansitowin32 );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( colorama$ansitowin32 );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 43;
        goto try_except_handler_7;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_colorama$ansitowin32, (Nuitka_StringObject *)const_str_plain_AnsiToWin32, tmp_assign_source_35 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7c1edf74d76a4f7d3531ba1a6f72392 );
#endif
    popFrameStack();

    assertFrameObject( frame_c7c1edf74d76a4f7d3531ba1a6f72392 );

    goto frame_no_exception_3;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7c1edf74d76a4f7d3531ba1a6f72392 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c7c1edf74d76a4f7d3531ba1a6f72392, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c7c1edf74d76a4f7d3531ba1a6f72392->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c7c1edf74d76a4f7d3531ba1a6f72392, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;


    return MOD_RETURN_VALUE( module_colorama$ansitowin32 );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
