/* Generated code for Python module 'prompt_toolkit.key_binding.bindings.emacs'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$key_binding$bindings$emacs" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$key_binding$bindings$emacs;
PyDictObject *moduledict_prompt_toolkit$key_binding$bindings$emacs;

/* The declarations of module constants used, if any. */
static PyObject *const_tuple_str_plain_escape_str_dot_tuple;
extern PyObject *const_str_plain_complete_state;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_d10127819ffc31ff0704f8ccddefdadf;
static PyObject *const_tuple_str_plain_KeyBindings_str_plain_ConditionalKeyBindings_tuple;
extern PyObject *const_str_digest_a71cd96de41b1d83d8e604317aa9a7eb;
extern PyObject *const_str_plain_CompleteEvent;
extern PyObject *const_tuple_str_digest_594981de1deffed7357a6afed608966a_tuple;
extern PyObject *const_str_chr_42;
extern PyObject *const_tuple_str_digest_89633640ede9c2fabde46085a768738e_tuple;
extern PyObject *const_tuple_str_digest_fb5c11df9835340c3dd4a642c111b2eb_tuple;
extern PyObject *const_str_digest_594981de1deffed7357a6afed608966a;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_chr_41;
extern PyObject *const_tuple_str_digest_daa690fd2d0c2002d9c7aad7dc2bacf6_tuple;
extern PyObject *const_str_angle_genexpr;
static PyObject *const_tuple_str_plain_escape_str_plain_d_tuple;
extern PyObject *const_str_digest_402d95161f3800c90c59735f4d8c4c59;
extern PyObject *const_str_chr_45;
static PyObject *const_tuple_a66d5d72b25c583e30161ec2ed9ead6d_tuple;
extern PyObject *const_str_plain_clipboard;
static PyObject *const_str_digest_19937bb605e4ff97939df877fb3a3dba;
extern PyObject *const_str_digest_70508f93fca682b73b6276fb31ac0cad;
static PyObject *const_str_digest_23ee9375b47833896f17de0216290961;
static PyObject *const_tuple_str_plain_escape_str_chr_60_tuple;
extern PyObject *const_str_digest_f2a75caf8ec802255b8dafde1721755d;
extern PyObject *const_str_plain_event;
extern PyObject *const_tuple_str_digest_4e5a5ed7c51e9863d1e36202b87fe43e_tuple;
extern PyObject *const_str_plain_Keys;
extern PyObject *const_str_plain_backspace;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_up;
extern PyObject *const_tuple_str_digest_402d95161f3800c90c59735f4d8c4c59_tuple;
extern PyObject *const_str_digest_faa2df820fbcc927d6a1cbff6da2d7e2;
extern PyObject *const_str_digest_bec9411c13b58be3a688ffe0c42ba2da;
extern PyObject *const_int_0;
extern PyObject *const_tuple_str_plain_enter_tuple;
extern PyObject *const_str_plain_cursor_position;
extern PyObject *const_str_plain_start_completion;
extern PyObject *const_str_digest_89633640ede9c2fabde46085a768738e;
extern PyObject *const_str_plain_find_next_word_beginning;
extern PyObject *const_str_plain_append_to_arg_count;
extern PyObject *const_str_plain_buff;
extern PyObject *const_tuple_str_plain_down_tuple;
extern PyObject *const_str_digest_4d94ac19d21ce83f39e74e91d067ed52;
extern PyObject *const_tuple_str_plain_n_tuple;
extern PyObject *const_str_digest_ae692cb46ab2cf52c0be9c11d31b5318;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_key_bindings;
static PyObject *const_str_digest_7895461a6e5df04c005e365a196a2cd9;
extern PyObject *const_str_plain_include_current_position;
extern PyObject *const_str_digest_9102ee6eb9329cb31f5264512e00165d;
static PyObject *const_tuple_fff953a5aec6db6db5efa6eefa572277_tuple;
extern PyObject *const_tuple_str_digest_c8c3694ea6fe9becb903618b30b45e7b_tuple;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_805a29513a8af8331b0890a04c5e3b8d;
extern PyObject *const_str_plain__arg;
static PyObject *const_tuple_str_plain_escape_str_plain_l_tuple;
extern PyObject *const_tuple_str_digest_4e2aeb083e5702a53bc119c9bb3ab9d7_tuple;
extern PyObject *const_tuple_str_digest_1a6ebe85acbb908025352a60a51a93a4_tuple;
extern PyObject *const_str_plain_get_by_name;
static PyObject *const_tuple_str_plain_escape_str_chr_35_tuple;
extern PyObject *const_str_plain_completion_requested;
static PyObject *const_str_digest_4142ad0b126076d41adfa300a4f7e7ce;
extern PyObject *const_str_digest_820ffb9c299a4fe53ab8241c74e4288a;
extern PyObject *const_str_plain_set_data;
extern PyObject *const_str_plain_get_start_of_line_position;
static PyObject *const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f;
static PyObject *const_str_digest_25ba7d00fad2cf2267463c12875fd008;
static PyObject *const_tuple_str_plain_escape_str_plain_backspace_tuple;
static PyObject *const_list_ef292982c39f5011b8a1e46b69b128d9_list;
extern PyObject *const_str_plain_find_previous_word_beginning;
extern PyObject *const_str_plain_find;
static PyObject *const_str_digest_0be999944cc03256ef7b26af4ee4697c;
extern PyObject *const_tuple_str_digest_bec9411c13b58be3a688ffe0c42ba2da_tuple;
static PyObject *const_tuple_str_plain_escape_str_plain_y_tuple;
extern PyObject *const_str_digest_d4d66fa4b26f35cc7cfe3fc57dce3f65;
extern PyObject *const_tuple_str_digest_a71cd96de41b1d83d8e604317aa9a7eb_tuple;
extern PyObject *const_str_plain_enter;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_digest_3e8226f37423ae997480ac891b15fb8d_tuple;
static PyObject *const_str_digest_36d4b9f6b8c00f3caaf59bb3cc6d8f8e;
extern PyObject *const_str_plain_unindent;
extern PyObject *const_str_plain_get_end_of_document_position;
static PyObject *const_tuple_str_plain_escape_str_chr_92_tuple;
extern PyObject *const_str_plain_abort_search;
extern PyObject *const_tuple_str_plain_escape_tuple;
extern PyObject *const_str_plain_insert_mode;
static PyObject *const_tuple_str_plain_CompleteEvent_tuple;
extern PyObject *const_tuple_str_digest_d306eb7c71f9186853263120b43be12e_tuple;
extern PyObject *const_str_plain_filter;
static PyObject *const_str_digest_d529933db36a700e95541891c167c7d9;
static PyObject *const_tuple_str_plain_escape_str_chr_62_tuple;
extern PyObject *const_tuple_str_digest_c014cbade4743da3cd8e8b1fadea68a6_tuple;
extern PyObject *const_str_plain_select_first;
extern PyObject *const_str_chr_62;
extern PyObject *const_tuple_str_plain_up_tuple;
extern PyObject *const_str_plain_CHARACTERS;
static PyObject *const_str_digest_303c2ff1d1bb2781ed8ba7f5913d7adc;
extern PyObject *const_str_digest_0adea339c095a653a19ec3c66bfd3784;
extern PyObject *const_str_plain_yank;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_is_cursor_at_the_end_of_line;
static PyObject *const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple;
extern PyObject *const_tuple_str_digest_a36417e3feea8b19ddc0146dc9edb326_tuple;
extern PyObject *const_str_plain_w;
extern PyObject *const_str_plain_accept_search;
static PyObject *const_tuple_str_plain_SelectionType_str_plain_indent_str_plain_unindent_tuple;
static PyObject *const_str_digest_dbfc7da45fdc782269b5d187f3020039;
static PyObject *const_str_digest_0e21e72a23eccf69d90ace0672b18eea;
extern PyObject *const_str_plain_e;
extern PyObject *const_tuple_str_digest_faa2df820fbcc927d6a1cbff6da2d7e2_tuple;
extern PyObject *const_tuple_str_plain_event_str_plain_data_tuple;
extern PyObject *const_tuple_str_plain_event_str_plain_buffer_tuple;
extern PyObject *const_tuple_str_chr_45_tuple;
extern PyObject *const_str_plain_l;
extern PyObject *const_tuple_str_digest_ee5885bc3d3f26ad23a483060cd09f03_tuple;
extern PyObject *const_str_digest_d306eb7c71f9186853263120b43be12e;
static PyObject *const_tuple_str_plain_escape_str_plain_w_tuple;
extern PyObject *const_str_plain_validation_error;
extern PyObject *const_str_plain_after_whitespace;
static PyObject *const_str_digest_0f25972f441f75ed5ada6831e33e8280;
extern PyObject *const_str_plain_SelectionType;
static PyObject *const_str_digest_f05828e2431524886539ce0953f53624;
extern PyObject *const_str_plain_buffer;
extern PyObject *const_str_plain_k;
extern PyObject *const_str_plain___all__;
extern PyObject *const_str_digest_db82dd148fa872efe07c2eb351c2c33f;
extern PyObject *const_str_digest_7230e58f12e9188dd6e7dddd9636a803;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_digest_3e8226f37423ae997480ac891b15fb8d;
extern PyObject *const_str_digest_c014cbade4743da3cd8e8b1fadea68a6;
static PyObject *const_tuple_str_plain_escape_str_plain_u_tuple;
extern PyObject *const_str_digest_3a16fa49bea795e638aac8247b0f7f8d;
extern PyObject *const_str_digest_23b6565e2fad8b5df75562a1b722c5fa;
static PyObject *const_str_digest_51765d060514397eb589107966de5d76;
extern PyObject *const_str_plain_text;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_56e920264daa106f062e68a1ec701d29;
static PyObject *const_tuple_a95f436c69d0cb7b7ffbbe853fc5d8cf_tuple;
extern PyObject *const_tuple_str_plain_get_app_tuple;
static PyObject *const_tuple_aaf40bfabaa80b6521580829dfb88a19_tuple;
extern PyObject *const_tuple_str_plain_event_str_plain_buff_tuple;
extern PyObject *const_str_chr_40;
static PyObject *const_tuple_str_plain_escape_str_plain_b_tuple;
extern PyObject *const_tuple_str_digest_0adea339c095a653a19ec3c66bfd3784_tuple;
static PyObject *const_str_digest_3d9afdb148843bb4992622c22022e531;
static PyObject *const_str_digest_57b0696d1e802eca5a7454735008d012;
extern PyObject *const_str_plain_d;
extern PyObject *const_str_plain_current_buffer;
extern PyObject *const_str_digest_90c174e3cde8d76e0cf5b8e86689f4a5;
extern PyObject *const_str_plain_0123456789;
extern PyObject *const_tuple_str_digest_7230e58f12e9188dd6e7dddd9636a803_tuple;
extern PyObject *const_str_digest_03dce90a9bd25ea1ea957edc60d7b71e;
extern PyObject *const_str_plain_to;
extern PyObject *const_str_plain_vi_search_direction_reversed;
extern PyObject *const_str_chr_47;
static PyObject *const_dict_b3a49b9216296cad44ade60138ab7bee;
extern PyObject *const_str_plain_eager;
extern PyObject *const_dict_5b4e82b0d7f6ce57036ec2983d8d8c39;
extern PyObject *const_str_digest_cccb566811e08cc4fd2e3ae406952507;
extern PyObject *const_str_plain_y;
static PyObject *const_tuple_str_plain_escape_str_plain_t_tuple;
extern PyObject *const_str_plain_ConditionalKeyBindings;
static PyObject *const_str_digest_e57e9cdba81c4bdcc73a534630ea08f1;
extern PyObject *const_str_digest_e13b285e26a38d1a90b94ccc0668450c;
extern PyObject *const_str_plain_left;
extern PyObject *const_tuple_str_digest_23b6565e2fad8b5df75562a1b722c5fa_tuple;
static PyObject *const_tuple_str_plain_escape_str_plain_c_tuple;
extern PyObject *const_str_digest_d8156c92c3ac39743ec9345e59bfd2ed;
extern PyObject *const_tuple_str_plain_undo_tuple;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_is_read_only;
extern PyObject *const_str_plain_emacs_insert_mode;
extern PyObject *const_str_plain_apply_search;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_insert_text;
extern PyObject *const_str_plain_start_forward_incremental_search;
static PyObject *const_str_digest_1a4a8eb7fcb290bfaf7f2a74ec3bd6e6;
extern PyObject *const_str_plain_get_end_of_line_position;
extern PyObject *const_tuple_str_plain_event_str_plain_b_tuple;
extern PyObject *const_str_plain_current_search_state;
static PyObject *const_str_digest_7680d57b355abe15172bd7731382572c;
extern PyObject *const_str_plain_find_backwards;
static PyObject *const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_60_tuple;
extern PyObject *const_tuple_str_plain_event_tuple;
extern PyObject *const_str_digest_a36417e3feea8b19ddc0146dc9edb326;
extern PyObject *const_str_plain_has_selection;
static PyObject *const_str_plain_text_to_insert;
extern PyObject *const_str_digest_a969c3e8d69c0076a5175c71af8240d8;
extern PyObject *const_str_plain_N;
static PyObject *const_tuple_55db4264d8c477500950268ab07846c9_tuple;
static PyObject *const_str_digest_4ad83595a950c69d845ee734d684b5de;
extern PyObject *const_str_plain_named_commands;
extern PyObject *const_str_plain_selection_type;
extern PyObject *const_tuple_str_digest_f2a75caf8ec802255b8dafde1721755d_tuple;
extern PyObject *const_tuple_str_plain_search_tuple;
extern PyObject *const_tuple_str_plain_yank_tuple;
extern PyObject *const_str_plain___doc__;
static PyObject *const_tuple_str_plain_escape_str_plain_right_tuple;
extern PyObject *const_str_plain_exit_selection;
static PyObject *const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_62_tuple;
extern PyObject *const_str_plain_data;
extern PyObject *const_tuple_str_digest_3e8aa0ff62897226c7230752efac5de8_tuple;
extern PyObject *const_tuple_str_digest_d4d66fa4b26f35cc7cfe3fc57dce3f65_tuple;
extern PyObject *const_str_plain_add;
static PyObject *const_tuple_94ff0c5df8d461e28a821d8cf948c033_tuple;
extern PyObject *const_tuple_str_digest_21e45c1f878d59fe84fced59f3e10b63_tuple;
extern PyObject *const_str_digest_2da2a9c575783ef454fa30e9f4e246ee;
extern PyObject *const_str_digest_e371fcb47bb33b8f0db02b1ad9764d6e;
extern PyObject *const_str_plain_Condition;
extern PyObject *const_tuple_str_digest_9102ee6eb9329cb31f5264512e00165d_tuple;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_complete_next;
extern PyObject *const_str_chr_35;
extern PyObject *const_str_digest_b7cd7d3061b29ef6badd990d7c83ddb1;
static PyObject *const_str_digest_23b64858594527c331ccb1200cf7f9fb;
extern PyObject *const_str_digest_21e45c1f878d59fe84fced59f3e10b63;
static PyObject *const_str_digest_673bac2daa78cc2280d7761be6670617;
extern PyObject *const_str_digest_4c9694e2a2207ea9dfca6d786b312099;
extern PyObject *const_str_digest_99c20ff6137166a3040444a10775418c;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_space;
static PyObject *const_tuple_7ed3ae015e22d01e89cce13238d41ef3_tuple;
extern PyObject *const_str_plain_auto_down;
extern PyObject *const_str_plain_handle;
extern PyObject *const_str_plain_translate_index_to_position;
extern PyObject *const_str_chr_92;
static PyObject *const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_41_tuple;
extern PyObject *const_dict_baa3e9b9ceb00288faad435c619c9225;
extern PyObject *const_str_plain_Any;
extern PyObject *const_str_plain_start_selection;
extern PyObject *const_tuple_str_digest_10c978016ae019e97a4da8b4078edf46_tuple;
extern PyObject *const_str_digest_1cc17ecfecf8f4814db97df6f8dcaebf;
extern PyObject *const_str_plain_b;
extern PyObject *const_tuple_str_plain_e_tuple;
extern PyObject *const_str_plain_KeyBindings;
static PyObject *const_str_digest_db2a23c722daba3c9afababc05121cfb;
static PyObject *const_tuple_str_plain_escape_str_plain_left_tuple;
extern PyObject *const_tuple_str_digest_f8c276bab5a9ae14be8475d6e0e75e09_tuple;
extern PyObject *const_tuple_str_chr_63_tuple;
extern PyObject *const_str_digest_daa690fd2d0c2002d9c7aad7dc2bacf6;
static PyObject *const_str_digest_13a72683b8db98826bde02b62e2ff1e5;
extern PyObject *const_str_digest_4dcf85ba674584b7335275c899c0ab24;
extern PyObject *const_str_plain_get_app;
static PyObject *const_tuple_str_plain_escape_str_chr_45_tuple;
extern PyObject *const_str_digest_f8c276bab5a9ae14be8475d6e0e75e09;
extern PyObject *const_str_plain_c;
extern PyObject *const_str_plain_complete_event;
extern PyObject *const_tuple_str_digest_03dce90a9bd25ea1ea957edc60d7b71e_tuple;
extern PyObject *const_str_plain_app;
extern PyObject *const_tuple_str_digest_820ffb9c299a4fe53ab8241c74e4288a_tuple;
extern PyObject *const_str_plain__;
extern PyObject *const_str_digest_637ae2db1c15757d3f68020910fc7af5;
extern PyObject *const_str_digest_722e5e8c7f53b34663d9669ca7ddc056;
extern PyObject *const_str_plain_is_multiline;
extern PyObject *const_str_plain_save_before;
static PyObject *const_tuple_str_plain_escape_str_plain___tuple;
static PyObject *const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_plain_e_tuple;
extern PyObject *const_tuple_str_chr_47_tuple;
static PyObject *const_str_digest_552610faf02e8a2270c9a3dac66f8598;
static PyObject *const_str_plain_handle_digit;
extern PyObject *const_str_plain_text_inserted;
static PyObject *const_tuple_str_plain_escape_str_plain_a_tuple;
extern PyObject *const_str_plain_u;
static PyObject *const_tuple_33d0ceddfbe393d5297a58d84190f675_tuple;
extern PyObject *const_str_plain_completions;
extern PyObject *const_str_plain_escape;
static PyObject *const_tuple_str_plain_event_str_plain_character_search_tuple;
static PyObject *const_str_digest_bad39abf5991d6848a5eaabb10c0d642;
extern PyObject *const_tuple_str_digest_d8156c92c3ac39743ec9345e59bfd2ed_tuple;
static PyObject *const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_40_tuple;
extern PyObject *const_tuple_str_digest_a969c3e8d69c0076a5175c71af8240d8_tuple;
extern PyObject *const_str_plain_completer;
extern PyObject *const_str_chr_60;
extern PyObject *const_str_plain_in_current_line;
extern PyObject *const_str_plain_load_emacs_bindings;
extern PyObject *const_str_plain_arg;
extern PyObject *const_str_digest_3e8aa0ff62897226c7230752efac5de8;
extern PyObject *const_str_plain_a;
extern PyObject *const_tuple_str_plain_Keys_tuple;
static PyObject *const_tuple_str_plain_escape_str_chr_47_tuple;
extern PyObject *const_tuple_str_digest_bc52841ed7d0c0e991090611ebb55c62_tuple;
extern PyObject *const_str_plain_from_;
static PyObject *const_str_digest_0762d8674f4ac16d5fe4664d8da8a610;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_digest_5bbf98712488fb50e0bf411a1625446e;
extern PyObject *const_str_chr_63;
extern PyObject *const_tuple_str_digest_4dcf85ba674584b7335275c899c0ab24_tuple;
extern PyObject *const_tuple_str_plain_get_by_name_tuple;
extern PyObject *const_str_digest_73eb09babb290e590b9a69835b5223c4;
extern PyObject *const_str_dot;
static PyObject *const_str_digest_16336d64d361bd5d736d7f5f0813c051;
extern PyObject *const_str_digest_bc52841ed7d0c0e991090611ebb55c62;
static PyObject *const_tuple_str_plain_event_str_plain_c_tuple;
extern PyObject *const_str_digest_1a6ebe85acbb908025352a60a51a93a4;
extern PyObject *const_str_plain_down;
extern PyObject *const_str_plain_t;
extern PyObject *const_tuple_str_digest_db82dd148fa872efe07c2eb351c2c33f_tuple;
extern PyObject *const_str_plain_get_completions;
extern PyObject *const_tuple_str_digest_90c174e3cde8d76e0cf5b8e86689f4a5_tuple;
static PyObject *const_str_digest_e2ead212ecbf1246fa221343cd9ec59b;
extern PyObject *const_str_digest_8f4424fc4ecbf6f26054d535e25dd9b5;
static PyObject *const_str_digest_2841ebef76882408ceaa61749f1115dc;
extern PyObject *const_str_plain_copy_selection;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_c_tuple;
static PyObject *const_tuple_str_plain_escape_str_digest_73eb09babb290e590b9a69835b5223c4_tuple;
extern PyObject *const_tuple_str_digest_73eb09babb290e590b9a69835b5223c4_tuple;
extern PyObject *const_str_plain_indent;
extern PyObject *const_tuple_str_plain_N_tuple;
extern PyObject *const_tuple_str_digest_637ae2db1c15757d3f68020910fc7af5_tuple;
extern PyObject *const_str_plain_emacs_mode;
extern PyObject *const_str_plain_selection_range;
extern PyObject *const_str_plain_r;
extern PyObject *const_str_plain_document;
extern PyObject *const_str_digest_4e2aeb083e5702a53bc119c9bb3ab9d7;
extern PyObject *const_str_plain_start_reverse_incremental_search;
extern PyObject *const_str_plain_match;
extern PyObject *const_tuple_str_digest_4d94ac19d21ce83f39e74e91d067ed52_tuple;
extern PyObject *const_str_digest_ee5885bc3d3f26ad23a483060cd09f03;
extern PyObject *const_str_plain_char;
extern PyObject *const_tuple_str_digest_1e115ca000973c4222f04cf175132c7e_tuple;
extern PyObject *const_str_digest_c8c3694ea6fe9becb903618b30b45e7b;
static PyObject *const_dict_cdf87466f932e58c7352e4a0bd2d6033;
extern PyObject *const_str_plain_count;
extern PyObject *const_tuple_str_digest_99c20ff6137166a3040444a10775418c_tuple;
extern PyObject *const_str_digest_4e5a5ed7c51e9863d1e36202b87fe43e;
extern PyObject *const_tuple_str_plain_escape_str_plain_f_tuple;
extern PyObject *const_str_plain_has_arg;
extern PyObject *const_str_plain_search;
static PyObject *const_tuple_str_plain_escape_str_plain_enter_tuple;
extern PyObject *const_str_plain_key_processor;
extern PyObject *const_str_digest_10c978016ae019e97a4da8b4078edf46;
extern PyObject *const_str_plain_right;
extern PyObject *const_str_angle_lambda;
static PyObject *const_str_digest_8ec48734954c8fd44d61dd501dd2ddfa;
extern PyObject *const_str_digest_fb5c11df9835340c3dd4a642c111b2eb;
extern PyObject *const_str_plain_forward_incremental_search;
extern PyObject *const_tuple_str_digest_ae692cb46ab2cf52c0be9c11d31b5318_tuple;
static PyObject *const_tuple_str_plain_c_str_plain___str_plain_handle_tuple;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_tuple_str_digest_70508f93fca682b73b6276fb31ac0cad_tuple;
extern PyObject *const_str_plain_reverse_incremental_search;
static PyObject *const_str_plain_character_search;
extern PyObject *const_str_plain_is_returnable;
extern PyObject *const_str_plain_load_emacs_search_bindings;
static PyObject *const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple;
static PyObject *const_tuple_str_plain_escape_str_plain_e_tuple;
extern PyObject *const_str_plain_cut_selection;
extern PyObject *const_str_plain_undo;
extern PyObject *const_tuple_str_digest_e13b285e26a38d1a90b94ccc0668450c_tuple;
static PyObject *const_tuple_str_plain_escape_str_chr_42_tuple;
extern PyObject *const_str_plain_auto_up;
extern PyObject *const_dict_0bf8402271879b64e927f26f79aacd8b;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_digest_1e115ca000973c4222f04cf175132c7e;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_str_plain_escape_str_dot_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_dot_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_dot_tuple, 1, const_str_dot ); Py_INCREF( const_str_dot );
    const_str_digest_d10127819ffc31ff0704f8ccddefdadf = UNSTREAM_STRING_ASCII( &constant_bin[ 4680280 ], 36, 0 );
    const_tuple_str_plain_KeyBindings_str_plain_ConditionalKeyBindings_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_KeyBindings_str_plain_ConditionalKeyBindings_tuple, 0, const_str_plain_KeyBindings ); Py_INCREF( const_str_plain_KeyBindings );
    PyTuple_SET_ITEM( const_tuple_str_plain_KeyBindings_str_plain_ConditionalKeyBindings_tuple, 1, const_str_plain_ConditionalKeyBindings ); Py_INCREF( const_str_plain_ConditionalKeyBindings );
    const_tuple_str_plain_escape_str_plain_d_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_d_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_d_tuple, 1, const_str_plain_d ); Py_INCREF( const_str_plain_d );
    const_tuple_a66d5d72b25c583e30161ec2ed9ead6d_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_a66d5d72b25c583e30161ec2ed9ead6d_tuple, 0, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_a66d5d72b25c583e30161ec2ed9ead6d_tuple, 1, const_str_plain_char ); Py_INCREF( const_str_plain_char );
    PyTuple_SET_ITEM( const_tuple_a66d5d72b25c583e30161ec2ed9ead6d_tuple, 2, const_str_plain_count ); Py_INCREF( const_str_plain_count );
    PyTuple_SET_ITEM( const_tuple_a66d5d72b25c583e30161ec2ed9ead6d_tuple, 3, const_str_plain_match ); Py_INCREF( const_str_plain_match );
    const_str_digest_19937bb605e4ff97939df877fb3a3dba = UNSTREAM_STRING_ASCII( &constant_bin[ 4680316 ], 39, 0 );
    const_str_digest_23ee9375b47833896f17de0216290961 = UNSTREAM_STRING_ASCII( &constant_bin[ 4680355 ], 33, 0 );
    const_tuple_str_plain_escape_str_chr_60_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_60_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_60_tuple, 1, const_str_chr_60 ); Py_INCREF( const_str_chr_60 );
    const_str_digest_7895461a6e5df04c005e365a196a2cd9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4680388 ], 32, 0 );
    const_tuple_fff953a5aec6db6db5efa6eefa572277_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_fff953a5aec6db6db5efa6eefa572277_tuple, 0, const_str_digest_5bbf98712488fb50e0bf411a1625446e ); Py_INCREF( const_str_digest_5bbf98712488fb50e0bf411a1625446e );
    PyTuple_SET_ITEM( const_tuple_fff953a5aec6db6db5efa6eefa572277_tuple, 1, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_fff953a5aec6db6db5efa6eefa572277_tuple, 2, const_str_plain_y ); Py_INCREF( const_str_plain_y );
    const_str_digest_805a29513a8af8331b0890a04c5e3b8d = UNSTREAM_STRING_ASCII( &constant_bin[ 4680420 ], 112, 0 );
    const_tuple_str_plain_escape_str_plain_l_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_l_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_l_tuple, 1, const_str_plain_l ); Py_INCREF( const_str_plain_l );
    const_tuple_str_plain_escape_str_chr_35_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_35_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_35_tuple, 1, const_str_chr_35 ); Py_INCREF( const_str_chr_35 );
    const_str_digest_4142ad0b126076d41adfa300a4f7e7ce = UNSTREAM_STRING_ASCII( &constant_bin[ 4680532 ], 78, 0 );
    const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f = UNSTREAM_STRING_ASCII( &constant_bin[ 4680610 ], 30, 0 );
    const_str_digest_25ba7d00fad2cf2267463c12875fd008 = UNSTREAM_STRING_ASCII( &constant_bin[ 4680640 ], 47, 0 );
    const_tuple_str_plain_escape_str_plain_backspace_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_backspace_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_backspace_tuple, 1, const_str_plain_backspace ); Py_INCREF( const_str_plain_backspace );
    const_list_ef292982c39f5011b8a1e46b69b128d9_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_ef292982c39f5011b8a1e46b69b128d9_list, 0, const_str_plain_load_emacs_bindings ); Py_INCREF( const_str_plain_load_emacs_bindings );
    PyList_SET_ITEM( const_list_ef292982c39f5011b8a1e46b69b128d9_list, 1, const_str_plain_load_emacs_search_bindings ); Py_INCREF( const_str_plain_load_emacs_search_bindings );
    const_str_digest_0be999944cc03256ef7b26af4ee4697c = UNSTREAM_STRING_ASCII( &constant_bin[ 4680687 ], 60, 0 );
    const_tuple_str_plain_escape_str_plain_y_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_y_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_y_tuple, 1, const_str_plain_y ); Py_INCREF( const_str_plain_y );
    const_str_digest_36d4b9f6b8c00f3caaf59bb3cc6d8f8e = UNSTREAM_STRING_ASCII( &constant_bin[ 4680747 ], 41, 0 );
    const_tuple_str_plain_escape_str_chr_92_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_92_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_92_tuple, 1, const_str_chr_92 ); Py_INCREF( const_str_chr_92 );
    const_tuple_str_plain_CompleteEvent_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_CompleteEvent_tuple, 0, const_str_plain_CompleteEvent ); Py_INCREF( const_str_plain_CompleteEvent );
    const_str_digest_d529933db36a700e95541891c167c7d9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4680788 ], 28, 0 );
    const_tuple_str_plain_escape_str_chr_62_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_62_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_62_tuple, 1, const_str_chr_62 ); Py_INCREF( const_str_chr_62 );
    const_str_digest_303c2ff1d1bb2781ed8ba7f5913d7adc = UNSTREAM_STRING_ASCII( &constant_bin[ 4680816 ], 26, 0 );
    const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple, 0, const_str_plain_key_bindings ); Py_INCREF( const_str_plain_key_bindings );
    PyTuple_SET_ITEM( const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple, 1, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    PyTuple_SET_ITEM( const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple, 2, const_str_plain_insert_mode ); Py_INCREF( const_str_plain_insert_mode );
    PyTuple_SET_ITEM( const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple, 3, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    const_str_plain_handle_digit = UNSTREAM_STRING_ASCII( &constant_bin[ 4680842 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple, 4, const_str_plain_handle_digit ); Py_INCREF( const_str_plain_handle_digit );
    PyTuple_SET_ITEM( const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple, 5, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    PyTuple_SET_ITEM( const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple, 6, const_str_plain_is_returnable ); Py_INCREF( const_str_plain_is_returnable );
    const_str_plain_character_search = UNSTREAM_STRING_ASCII( &constant_bin[ 4680854 ], 16, 1 );
    PyTuple_SET_ITEM( const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple, 7, const_str_plain_character_search ); Py_INCREF( const_str_plain_character_search );
    const_tuple_str_plain_SelectionType_str_plain_indent_str_plain_unindent_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_SelectionType_str_plain_indent_str_plain_unindent_tuple, 0, const_str_plain_SelectionType ); Py_INCREF( const_str_plain_SelectionType );
    PyTuple_SET_ITEM( const_tuple_str_plain_SelectionType_str_plain_indent_str_plain_unindent_tuple, 1, const_str_plain_indent ); Py_INCREF( const_str_plain_indent );
    PyTuple_SET_ITEM( const_tuple_str_plain_SelectionType_str_plain_indent_str_plain_unindent_tuple, 2, const_str_plain_unindent ); Py_INCREF( const_str_plain_unindent );
    const_str_digest_dbfc7da45fdc782269b5d187f3020039 = UNSTREAM_STRING_ASCII( &constant_bin[ 4680870 ], 35, 0 );
    const_str_digest_0e21e72a23eccf69d90ace0672b18eea = UNSTREAM_STRING_ASCII( &constant_bin[ 4680905 ], 44, 0 );
    const_tuple_str_plain_escape_str_plain_w_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_w_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_w_tuple, 1, const_str_plain_w ); Py_INCREF( const_str_plain_w );
    const_str_digest_0f25972f441f75ed5ada6831e33e8280 = UNSTREAM_STRING_ASCII( &constant_bin[ 4680949 ], 50, 0 );
    const_str_digest_f05828e2431524886539ce0953f53624 = UNSTREAM_STRING_ASCII( &constant_bin[ 4680999 ], 49, 0 );
    const_tuple_str_plain_escape_str_plain_u_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_u_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_u_tuple, 1, const_str_plain_u ); Py_INCREF( const_str_plain_u );
    const_str_digest_51765d060514397eb589107966de5d76 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681048 ], 42, 0 );
    const_str_digest_56e920264daa106f062e68a1ec701d29 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681090 ], 37, 0 );
    const_tuple_a95f436c69d0cb7b7ffbbe853fc5d8cf_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_a95f436c69d0cb7b7ffbbe853fc5d8cf_tuple, 0, const_str_digest_5bbf98712488fb50e0bf411a1625446e ); Py_INCREF( const_str_digest_5bbf98712488fb50e0bf411a1625446e );
    PyTuple_SET_ITEM( const_tuple_a95f436c69d0cb7b7ffbbe853fc5d8cf_tuple, 1, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_a95f436c69d0cb7b7ffbbe853fc5d8cf_tuple, 2, const_str_plain_k ); Py_INCREF( const_str_plain_k );
    const_tuple_aaf40bfabaa80b6521580829dfb88a19_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_aaf40bfabaa80b6521580829dfb88a19_tuple, 0, const_str_plain_key_bindings ); Py_INCREF( const_str_plain_key_bindings );
    PyTuple_SET_ITEM( const_tuple_aaf40bfabaa80b6521580829dfb88a19_tuple, 1, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    PyTuple_SET_ITEM( const_tuple_aaf40bfabaa80b6521580829dfb88a19_tuple, 2, const_str_plain_search ); Py_INCREF( const_str_plain_search );
    PyTuple_SET_ITEM( const_tuple_aaf40bfabaa80b6521580829dfb88a19_tuple, 3, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    const_tuple_str_plain_escape_str_plain_b_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_b_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_b_tuple, 1, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    const_str_digest_3d9afdb148843bb4992622c22022e531 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681127 ], 51, 0 );
    const_str_digest_57b0696d1e802eca5a7454735008d012 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681178 ], 21, 0 );
    const_dict_b3a49b9216296cad44ade60138ab7bee = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_b3a49b9216296cad44ade60138ab7bee, const_str_plain_eager, Py_True );
    assert( PyDict_Size( const_dict_b3a49b9216296cad44ade60138ab7bee ) == 1 );
    const_tuple_str_plain_escape_str_plain_t_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_t_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_t_tuple, 1, const_str_plain_t ); Py_INCREF( const_str_plain_t );
    const_str_digest_e57e9cdba81c4bdcc73a534630ea08f1 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681199 ], 75, 0 );
    const_tuple_str_plain_escape_str_plain_c_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_c_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_c_tuple, 1, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_str_digest_1a4a8eb7fcb290bfaf7f2a74ec3bd6e6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681274 ], 37, 0 );
    const_str_digest_7680d57b355abe15172bd7731382572c = UNSTREAM_STRING_ASCII( &constant_bin[ 4681311 ], 348, 0 );
    const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_60_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_60_tuple, 0, const_str_digest_99c20ff6137166a3040444a10775418c ); Py_INCREF( const_str_digest_99c20ff6137166a3040444a10775418c );
    PyTuple_SET_ITEM( const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_60_tuple, 1, const_str_chr_60 ); Py_INCREF( const_str_chr_60 );
    const_str_plain_text_to_insert = UNSTREAM_STRING_ASCII( &constant_bin[ 4681659 ], 14, 1 );
    const_tuple_55db4264d8c477500950268ab07846c9_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_55db4264d8c477500950268ab07846c9_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_55db4264d8c477500950268ab07846c9_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_55db4264d8c477500950268ab07846c9_tuple, 2, const_str_plain_complete_event ); Py_INCREF( const_str_plain_complete_event );
    PyTuple_SET_ITEM( const_tuple_55db4264d8c477500950268ab07846c9_tuple, 3, const_str_plain_completions ); Py_INCREF( const_str_plain_completions );
    PyTuple_SET_ITEM( const_tuple_55db4264d8c477500950268ab07846c9_tuple, 4, const_str_plain_text_to_insert ); Py_INCREF( const_str_plain_text_to_insert );
    const_str_digest_4ad83595a950c69d845ee734d684b5de = UNSTREAM_STRING_ASCII( &constant_bin[ 4681673 ], 45, 0 );
    const_tuple_str_plain_escape_str_plain_right_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_right_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_right_tuple, 1, const_str_plain_right ); Py_INCREF( const_str_plain_right );
    const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_62_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_62_tuple, 0, const_str_digest_99c20ff6137166a3040444a10775418c ); Py_INCREF( const_str_digest_99c20ff6137166a3040444a10775418c );
    PyTuple_SET_ITEM( const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_62_tuple, 1, const_str_chr_62 ); Py_INCREF( const_str_chr_62 );
    const_tuple_94ff0c5df8d461e28a821d8cf948c033_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_94ff0c5df8d461e28a821d8cf948c033_tuple, 0, const_str_digest_5bbf98712488fb50e0bf411a1625446e ); Py_INCREF( const_str_digest_5bbf98712488fb50e0bf411a1625446e );
    PyTuple_SET_ITEM( const_tuple_94ff0c5df8d461e28a821d8cf948c033_tuple, 1, const_str_digest_2da2a9c575783ef454fa30e9f4e246ee ); Py_INCREF( const_str_digest_2da2a9c575783ef454fa30e9f4e246ee );
    const_str_digest_23b64858594527c331ccb1200cf7f9fb = UNSTREAM_STRING_ASCII( &constant_bin[ 4681718 ], 60, 0 );
    const_str_digest_673bac2daa78cc2280d7761be6670617 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681778 ], 41, 0 );
    const_tuple_7ed3ae015e22d01e89cce13238d41ef3_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_7ed3ae015e22d01e89cce13238d41ef3_tuple, 0, const_str_digest_5bbf98712488fb50e0bf411a1625446e ); Py_INCREF( const_str_digest_5bbf98712488fb50e0bf411a1625446e );
    PyTuple_SET_ITEM( const_tuple_7ed3ae015e22d01e89cce13238d41ef3_tuple, 1, const_str_digest_5bbf98712488fb50e0bf411a1625446e ); Py_INCREF( const_str_digest_5bbf98712488fb50e0bf411a1625446e );
    const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_41_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_41_tuple, 0, const_str_digest_5bbf98712488fb50e0bf411a1625446e ); Py_INCREF( const_str_digest_5bbf98712488fb50e0bf411a1625446e );
    PyTuple_SET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_41_tuple, 1, const_str_chr_41 ); Py_INCREF( const_str_chr_41 );
    const_str_digest_db2a23c722daba3c9afababc05121cfb = UNSTREAM_STRING_ASCII( &constant_bin[ 4681819 ], 52, 0 );
    const_tuple_str_plain_escape_str_plain_left_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_left_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_left_tuple, 1, const_str_plain_left ); Py_INCREF( const_str_plain_left );
    const_str_digest_13a72683b8db98826bde02b62e2ff1e5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681871 ], 16, 0 );
    const_tuple_str_plain_escape_str_chr_45_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_45_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_45_tuple, 1, const_str_chr_45 ); Py_INCREF( const_str_chr_45 );
    const_tuple_str_plain_escape_str_plain___tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain___tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain___tuple, 1, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_plain_e_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_plain_e_tuple, 0, const_str_digest_5bbf98712488fb50e0bf411a1625446e ); Py_INCREF( const_str_digest_5bbf98712488fb50e0bf411a1625446e );
    PyTuple_SET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_plain_e_tuple, 1, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_str_digest_552610faf02e8a2270c9a3dac66f8598 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681887 ], 101, 0 );
    const_tuple_str_plain_escape_str_plain_a_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_a_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_a_tuple, 1, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    const_tuple_33d0ceddfbe393d5297a58d84190f675_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_33d0ceddfbe393d5297a58d84190f675_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_33d0ceddfbe393d5297a58d84190f675_tuple, 1, const_str_plain_buffer ); Py_INCREF( const_str_plain_buffer );
    PyTuple_SET_ITEM( const_tuple_33d0ceddfbe393d5297a58d84190f675_tuple, 2, const_str_plain_from_ ); Py_INCREF( const_str_plain_from_ );
    PyTuple_SET_ITEM( const_tuple_33d0ceddfbe393d5297a58d84190f675_tuple, 3, const_str_plain_to ); Py_INCREF( const_str_plain_to );
    PyTuple_SET_ITEM( const_tuple_33d0ceddfbe393d5297a58d84190f675_tuple, 4, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    const_tuple_str_plain_event_str_plain_character_search_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_character_search_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_character_search_tuple, 1, const_str_plain_character_search ); Py_INCREF( const_str_plain_character_search );
    const_str_digest_bad39abf5991d6848a5eaabb10c0d642 = UNSTREAM_STRING_ASCII( &constant_bin[ 4681988 ], 99, 0 );
    const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_40_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_40_tuple, 0, const_str_digest_5bbf98712488fb50e0bf411a1625446e ); Py_INCREF( const_str_digest_5bbf98712488fb50e0bf411a1625446e );
    PyTuple_SET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_40_tuple, 1, const_str_chr_40 ); Py_INCREF( const_str_chr_40 );
    const_tuple_str_plain_escape_str_chr_47_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_47_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_47_tuple, 1, const_str_chr_47 ); Py_INCREF( const_str_chr_47 );
    const_str_digest_0762d8674f4ac16d5fe4664d8da8a610 = UNSTREAM_STRING_ASCII( &constant_bin[ 4682087 ], 82, 0 );
    const_str_digest_16336d64d361bd5d736d7f5f0813c051 = UNSTREAM_STRING_ASCII( &constant_bin[ 4682169 ], 25, 0 );
    const_tuple_str_plain_event_str_plain_c_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_c_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_c_tuple, 1, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_str_digest_e2ead212ecbf1246fa221343cd9ec59b = UNSTREAM_STRING_ASCII( &constant_bin[ 4682194 ], 37, 0 );
    const_str_digest_2841ebef76882408ceaa61749f1115dc = UNSTREAM_STRING_ASCII( &constant_bin[ 4682231 ], 12, 0 );
    const_tuple_str_plain_escape_str_digest_73eb09babb290e590b9a69835b5223c4_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_digest_73eb09babb290e590b9a69835b5223c4_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_digest_73eb09babb290e590b9a69835b5223c4_tuple, 1, const_str_digest_73eb09babb290e590b9a69835b5223c4 ); Py_INCREF( const_str_digest_73eb09babb290e590b9a69835b5223c4 );
    const_dict_cdf87466f932e58c7352e4a0bd2d6033 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_cdf87466f932e58c7352e4a0bd2d6033, const_str_plain_text_inserted, Py_False );
    PyDict_SetItem( const_dict_cdf87466f932e58c7352e4a0bd2d6033, const_str_plain_completion_requested, Py_True );
    assert( PyDict_Size( const_dict_cdf87466f932e58c7352e4a0bd2d6033 ) == 2 );
    const_tuple_str_plain_escape_str_plain_enter_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_enter_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_enter_tuple, 1, const_str_plain_enter ); Py_INCREF( const_str_plain_enter );
    const_str_digest_8ec48734954c8fd44d61dd501dd2ddfa = UNSTREAM_STRING_ASCII( &constant_bin[ 4682243 ], 20, 0 );
    const_tuple_str_plain_c_str_plain___str_plain_handle_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_c_str_plain___str_plain_handle_tuple, 0, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    PyTuple_SET_ITEM( const_tuple_str_plain_c_str_plain___str_plain_handle_tuple, 1, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    PyTuple_SET_ITEM( const_tuple_str_plain_c_str_plain___str_plain_handle_tuple, 2, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple, 0, const_str_plain_Condition ); Py_INCREF( const_str_plain_Condition );
    PyTuple_SET_ITEM( const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple, 1, const_str_plain_emacs_mode ); Py_INCREF( const_str_plain_emacs_mode );
    PyTuple_SET_ITEM( const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple, 2, const_str_plain_has_selection ); Py_INCREF( const_str_plain_has_selection );
    PyTuple_SET_ITEM( const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple, 3, const_str_plain_emacs_insert_mode ); Py_INCREF( const_str_plain_emacs_insert_mode );
    PyTuple_SET_ITEM( const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple, 4, const_str_plain_has_arg ); Py_INCREF( const_str_plain_has_arg );
    PyTuple_SET_ITEM( const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple, 5, const_str_plain_is_multiline ); Py_INCREF( const_str_plain_is_multiline );
    PyTuple_SET_ITEM( const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple, 6, const_str_plain_is_read_only ); Py_INCREF( const_str_plain_is_read_only );
    PyTuple_SET_ITEM( const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple, 7, const_str_plain_vi_search_direction_reversed ); Py_INCREF( const_str_plain_vi_search_direction_reversed );
    const_tuple_str_plain_escape_str_plain_e_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_e_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_plain_e_tuple, 1, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_tuple_str_plain_escape_str_chr_42_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_42_tuple, 0, const_str_plain_escape ); Py_INCREF( const_str_plain_escape );
    PyTuple_SET_ITEM( const_tuple_str_plain_escape_str_chr_42_tuple, 1, const_str_chr_42 ); Py_INCREF( const_str_chr_42 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$key_binding$bindings$emacs( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_5117690e35214a1ef887b09ec807a92c;
static PyCodeObject *codeobj_d408982fffef1590d6d9cd23cab783d4;
static PyCodeObject *codeobj_88b6e14da00dc5c2b3ce10164404de82;
static PyCodeObject *codeobj_a9096c986027d7c4cc17980f4c730da6;
static PyCodeObject *codeobj_99891091142b0215282b60d85f3a4651;
static PyCodeObject *codeobj_8924278ddaa3cb41198aaa3d5407fada;
static PyCodeObject *codeobj_b4d2d8c8516dc5482abf143712f11867;
static PyCodeObject *codeobj_fde0797c15931de70c425eaf5d1dc63e;
static PyCodeObject *codeobj_339b0bf3b0b5dafd995be974b89551c1;
static PyCodeObject *codeobj_e5eda8746a8b596dc9f4d379d11774c7;
static PyCodeObject *codeobj_2430e93442085971c66cb9eef5ca8a3e;
static PyCodeObject *codeobj_c07ea4e26aa4f8d1ab23551a798518af;
static PyCodeObject *codeobj_dfc06c882257802987b44fa5c42e515d;
static PyCodeObject *codeobj_cf42bea14394d91a76668f60f023eacc;
static PyCodeObject *codeobj_a20344d433cd6984d126314d57bceb35;
static PyCodeObject *codeobj_c59078db7e911c13f595ce86066b5552;
static PyCodeObject *codeobj_69d615a74aa792779c7ce0296ec03609;
static PyCodeObject *codeobj_3a411fcaca5bb9e0abca1c4a101dd287;
static PyCodeObject *codeobj_655ef0697fd9f535491eaf3ff1799405;
static PyCodeObject *codeobj_15b84f706ba18ad1c9fcbf547a8ed6b0;
static PyCodeObject *codeobj_b616f61388861cc23d80906312549a0c;
static PyCodeObject *codeobj_f34edee97ddfaa882297fe4bceb24d98;
static PyCodeObject *codeobj_edf6eaa6807f966e1e63a2560ffd3d0e;
static PyCodeObject *codeobj_095ee85caa19b794d8d193fe122b546a;
static PyCodeObject *codeobj_7e5974495860987fed4908a679b90371;
static PyCodeObject *codeobj_dc77a1cef49efbb1da8d023e89ab872d;
static PyCodeObject *codeobj_22049ef5e24d71ef8e11931cfb18870a;
static PyCodeObject *codeobj_cb46efa5acfd6e83bed442fb852708bd;
static PyCodeObject *codeobj_70b09fedff819bd1cc7106a3c27b3945;
static PyCodeObject *codeobj_3499e630a1c715a6782317aaad22ae6e;
static PyCodeObject *codeobj_ad505444d20353014afe05006e6e8d3a;
static PyCodeObject *codeobj_3a44b69542262cbf32c6df5db6fce978;
static PyCodeObject *codeobj_9aef691f2da727794e9cdacaa2210a0d;
static PyCodeObject *codeobj_5dbd4e3413cc7f67ed8c68e401cdc48e;
static PyCodeObject *codeobj_9de8f726ea4501a75389de56bbf7ef36;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_0e21e72a23eccf69d90ace0672b18eea );
    codeobj_5117690e35214a1ef887b09ec807a92c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 184, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_c_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d408982fffef1590d6d9cd23cab783d4 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 114, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_88b6e14da00dc5c2b3ce10164404de82 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 60, const_tuple_str_plain_e_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a9096c986027d7c4cc17980f4c730da6 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 63, const_tuple_str_plain_e_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_99891091142b0215282b60d85f3a4651 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_0f25972f441f75ed5ada6831e33e8280, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8924278ddaa3cb41198aaa3d5407fada = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 29, const_tuple_str_plain_event_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b4d2d8c8516dc5482abf143712f11867 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 84, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_fde0797c15931de70c425eaf5d1dc63e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 89, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_339b0bf3b0b5dafd995be974b89551c1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 107, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e5eda8746a8b596dc9f4d379d11774c7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 114, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2430e93442085971c66cb9eef5ca8a3e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 155, const_tuple_str_plain_event_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c07ea4e26aa4f8d1ab23551a798518af = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 160, const_tuple_str_plain_event_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_dfc06c882257802987b44fa5c42e515d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 165, const_tuple_str_plain_event_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_cf42bea14394d91a76668f60f023eacc = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 210, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a20344d433cd6984d126314d57bceb35 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 218, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c59078db7e911c13f595ce86066b5552 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 338, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_69d615a74aa792779c7ce0296ec03609 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 346, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3a411fcaca5bb9e0abca1c4a101dd287 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 259, const_tuple_str_plain_event_str_plain_b_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_655ef0697fd9f535491eaf3ff1799405 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 200, const_tuple_str_plain_event_str_plain_buff_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_15b84f706ba18ad1c9fcbf547a8ed6b0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 172, const_tuple_55db4264d8c477500950268ab07846c9_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b616f61388861cc23d80906312549a0c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 187, const_tuple_str_plain_event_str_plain_buffer_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f34edee97ddfaa882297fe4bceb24d98 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 242, const_tuple_str_plain_event_str_plain_buffer_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_edf6eaa6807f966e1e63a2560ffd3d0e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 250, const_tuple_str_plain_event_str_plain_buffer_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_095ee85caa19b794d8d193fe122b546a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 270, const_tuple_33d0ceddfbe393d5297a58d84190f675_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7e5974495860987fed4908a679b90371 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 285, const_tuple_33d0ceddfbe393d5297a58d84190f675_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_dc77a1cef49efbb1da8d023e89ab872d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 99, const_tuple_str_plain_event_str_plain_c_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_22049ef5e24d71ef8e11931cfb18870a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 143, const_tuple_str_plain_event_str_plain_character_search_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_cb46efa5acfd6e83bed442fb852708bd = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 149, const_tuple_str_plain_event_str_plain_character_search_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_70b09fedff819bd1cc7106a3c27b3945 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 225, const_tuple_str_plain_event_str_plain_data_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3499e630a1c715a6782317aaad22ae6e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 234, const_tuple_str_plain_event_str_plain_data_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ad505444d20353014afe05006e6e8d3a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_character_search, 134, const_tuple_a66d5d72b25c583e30161ec2ed9ead6d_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3a44b69542262cbf32c6df5db6fce978 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_handle_digit, 94, const_tuple_str_plain_c_str_plain___str_plain_handle_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9aef691f2da727794e9cdacaa2210a0d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_is_returnable, 122, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5dbd4e3413cc7f67ed8c68e401cdc48e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_load_emacs_bindings, 18, const_tuple_9cf3b6044ade464f2eb14b9a66f6296b_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9de8f726ea4501a75389de56bbf7ef36 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_load_emacs_search_bindings, 301, const_tuple_aaf40bfabaa80b6521580829dfb88a19_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__$$$genexpr_1_genexpr_maker( void );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_10_is_returnable(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_11_character_search(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_12__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_13__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_14__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_15__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_16__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_18__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_19__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_1__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_20__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_21__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_22__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_23__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_24__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_25__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_26__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_27__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_28__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_2_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_3_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_4__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_5__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit$$$function_1__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_7__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_8__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_9_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_1__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_2__(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_key_bindings = NULL;
    struct Nuitka_CellObject *var_handle = PyCell_EMPTY();
    PyObject *var_insert_mode = NULL;
    PyObject *var__ = NULL;
    PyObject *var_handle_digit = NULL;
    PyObject *var_c = NULL;
    PyObject *var_is_returnable = NULL;
    struct Nuitka_CellObject *var_character_search = PyCell_EMPTY();
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_5dbd4e3413cc7f67ed8c68e401cdc48e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_5dbd4e3413cc7f67ed8c68e401cdc48e = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5dbd4e3413cc7f67ed8c68e401cdc48e, codeobj_5dbd4e3413cc7f67ed8c68e401cdc48e, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5dbd4e3413cc7f67ed8c68e401cdc48e = cache_frame_5dbd4e3413cc7f67ed8c68e401cdc48e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5dbd4e3413cc7f67ed8c68e401cdc48e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5dbd4e3413cc7f67ed8c68e401cdc48e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_KeyBindings );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KeyBindings );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "KeyBindings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 24;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 24;
        tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        assert( var_key_bindings == NULL );
        var_key_bindings = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_key_bindings );
        tmp_source_name_1 = var_key_bindings;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_add );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_handle ) == NULL );
        PyCell_SET( var_handle, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_emacs_insert_mode );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_emacs_insert_mode );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "emacs_insert_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 27;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_3 = tmp_mvar_value_2;
        assert( var_insert_mode == NULL );
        Py_INCREF( tmp_assign_source_3 );
        var_insert_mode = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_3 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 29;
        tmp_called_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_str_plain_escape_tuple, 0 ) );

        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_1__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 29;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        assert( var__ == NULL );
        var__ = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_called_name_5;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_3;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_5 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 41;
        tmp_called_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_str_digest_03dce90a9bd25ea1ea957edc60d7b71e_tuple, 0 ) );

        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_3;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 41;
        tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_str_digest_637ae2db1c15757d3f68020910fc7af5_tuple, 0 ) );

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 41;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_7;
        PyObject *tmp_called_name_8;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_4;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_8 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 42;
        tmp_called_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_str_digest_1a6ebe85acbb908025352a60a51a93a4_tuple, 0 ) );

        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_called_name_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_4;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 42;
        tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_digest_21e45c1f878d59fe84fced59f3e10b63_tuple, 0 ) );

        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_7 );

            exception_lineno = 42;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 42;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_10;
        PyObject *tmp_called_name_11;
        PyObject *tmp_args_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_5;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_11 = PyCell_GET( var_handle );
        tmp_args_name_1 = const_tuple_str_digest_9102ee6eb9329cb31f5264512e00165d_tuple;
        tmp_dict_key_1 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_1 = var_insert_mode;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 43;
        tmp_called_name_10 = CALL_FUNCTION( tmp_called_name_11, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_5;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 43;
        tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_str_digest_d306eb7c71f9186853263120b43be12e_tuple, 0 ) );

        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 43;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 43;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_name_13;
        PyObject *tmp_called_name_14;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_6;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_14 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 44;
        tmp_called_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, &PyTuple_GET_ITEM( const_tuple_str_digest_a36417e3feea8b19ddc0146dc9edb326_tuple, 0 ) );

        if ( tmp_called_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_13 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_6;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 44;
        tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_digest_4dcf85ba674584b7335275c899c0ab24_tuple, 0 ) );

        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_13 );

            exception_lineno = 44;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 44;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
        }

        Py_DECREF( tmp_called_name_13 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_name_16;
        PyObject *tmp_called_name_17;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_7;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_17 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 45;
        tmp_called_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_digest_89633640ede9c2fabde46085a768738e_tuple, 0 ) );

        if ( tmp_called_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_16 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_7;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 45;
        tmp_args_element_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_str_digest_820ffb9c299a4fe53ab8241c74e4288a_tuple, 0 ) );

        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_16 );

            exception_lineno = 45;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 45;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_called_name_16 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_called_name_19;
        PyObject *tmp_called_name_20;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_21;
        PyObject *tmp_mvar_value_8;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_20 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 46;
        tmp_called_name_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, &PyTuple_GET_ITEM( const_tuple_str_digest_3e8226f37423ae997480ac891b15fb8d_tuple, 0 ) );

        if ( tmp_called_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_called_name_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_21 = tmp_mvar_value_8;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 46;
        tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, &PyTuple_GET_ITEM( const_tuple_str_digest_23b6565e2fad8b5df75562a1b722c5fa_tuple, 0 ) );

        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_19 );

            exception_lineno = 46;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 46;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
        }

        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        PyObject *tmp_called_name_22;
        PyObject *tmp_called_name_23;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_called_name_24;
        PyObject *tmp_mvar_value_9;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_23 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 47;
        tmp_called_name_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, &PyTuple_GET_ITEM( const_tuple_str_digest_a969c3e8d69c0076a5175c71af8240d8_tuple, 0 ) );

        if ( tmp_called_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_called_name_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_24 = tmp_mvar_value_9;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 47;
        tmp_args_element_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, &PyTuple_GET_ITEM( const_tuple_str_digest_1e115ca000973c4222f04cf175132c7e_tuple, 0 ) );

        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_22 );

            exception_lineno = 47;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 47;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_called_name_22 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        PyObject *tmp_called_name_25;
        PyObject *tmp_called_name_26;
        PyObject *tmp_args_name_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_call_result_8;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_called_name_27;
        PyObject *tmp_mvar_value_10;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_26 = PyCell_GET( var_handle );
        tmp_args_name_2 = const_tuple_fff953a5aec6db6db5efa6eefa572277_tuple;
        tmp_dict_key_2 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_2 = var_insert_mode;
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 48;
        tmp_called_name_25 = CALL_FUNCTION( tmp_called_name_26, tmp_args_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_called_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_called_name_25 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_27 = tmp_mvar_value_10;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 48;
        tmp_args_element_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, &PyTuple_GET_ITEM( const_tuple_str_plain_yank_tuple, 0 ) );

        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_25 );

            exception_lineno = 48;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, call_args );
        }

        Py_DECREF( tmp_called_name_25 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_call_result_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_8 );
    }
    {
        PyObject *tmp_called_name_28;
        PyObject *tmp_called_name_29;
        PyObject *tmp_args_name_3;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_call_result_9;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_11;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_29 = PyCell_GET( var_handle );
        tmp_args_name_3 = const_tuple_str_digest_73eb09babb290e590b9a69835b5223c4_tuple;
        tmp_dict_key_3 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_3 = var_insert_mode;
        tmp_kw_name_3 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 49;
        tmp_called_name_28 = CALL_FUNCTION( tmp_called_name_29, tmp_args_name_3, tmp_kw_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_called_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_called_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 49;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_11;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 49;
        tmp_args_element_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_30, &PyTuple_GET_ITEM( const_tuple_str_plain_yank_tuple, 0 ) );

        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 49;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 49;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_call_result_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
        }

        Py_DECREF( tmp_called_name_28 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_call_result_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_9 );
    }
    {
        PyObject *tmp_called_name_31;
        PyObject *tmp_called_name_32;
        PyObject *tmp_call_result_10;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_name_33;
        PyObject *tmp_mvar_value_12;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_32 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 50;
        tmp_called_name_31 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_32, &PyTuple_GET_ITEM( const_tuple_str_plain_escape_str_plain_b_tuple, 0 ) );

        if ( tmp_called_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_called_name_31 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 50;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_33 = tmp_mvar_value_12;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 50;
        tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_33, &PyTuple_GET_ITEM( const_tuple_str_digest_23b6565e2fad8b5df75562a1b722c5fa_tuple, 0 ) );

        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_31 );

            exception_lineno = 50;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 50;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_call_result_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, call_args );
        }

        Py_DECREF( tmp_called_name_31 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_call_result_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_10 );
    }
    {
        PyObject *tmp_called_name_34;
        PyObject *tmp_called_name_35;
        PyObject *tmp_args_name_4;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_call_result_11;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_name_36;
        PyObject *tmp_mvar_value_13;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_35 = PyCell_GET( var_handle );
        tmp_args_name_4 = const_tuple_str_plain_escape_str_plain_c_tuple;
        tmp_dict_key_4 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_4 = var_insert_mode;
        tmp_kw_name_4 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 51;
        tmp_called_name_34 = CALL_FUNCTION( tmp_called_name_35, tmp_args_name_4, tmp_kw_name_4 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_called_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_called_name_34 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_36 = tmp_mvar_value_13;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 51;
        tmp_args_element_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_36, &PyTuple_GET_ITEM( const_tuple_str_digest_4e2aeb083e5702a53bc119c9bb3ab9d7_tuple, 0 ) );

        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_34 );

            exception_lineno = 51;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 51;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_call_result_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, call_args );
        }

        Py_DECREF( tmp_called_name_34 );
        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_call_result_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_11 );
    }
    {
        PyObject *tmp_called_name_37;
        PyObject *tmp_called_name_38;
        PyObject *tmp_args_name_5;
        PyObject *tmp_kw_name_5;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_call_result_12;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_called_name_39;
        PyObject *tmp_mvar_value_14;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_38 = PyCell_GET( var_handle );
        tmp_args_name_5 = const_tuple_str_plain_escape_str_plain_d_tuple;
        tmp_dict_key_5 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_5 = var_insert_mode;
        tmp_kw_name_5 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 52;
        tmp_called_name_37 = CALL_FUNCTION( tmp_called_name_38, tmp_args_name_5, tmp_kw_name_5 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_called_name_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_called_name_37 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_39 = tmp_mvar_value_14;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 52;
        tmp_args_element_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_39, &PyTuple_GET_ITEM( const_tuple_str_digest_d306eb7c71f9186853263120b43be12e_tuple, 0 ) );

        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_37 );

            exception_lineno = 52;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 52;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_call_result_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_37, call_args );
        }

        Py_DECREF( tmp_called_name_37 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_call_result_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_12 );
    }
    {
        PyObject *tmp_called_name_40;
        PyObject *tmp_called_name_41;
        PyObject *tmp_call_result_13;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_called_name_42;
        PyObject *tmp_mvar_value_15;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_41 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 53;
        tmp_called_name_40 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_41, &PyTuple_GET_ITEM( const_tuple_str_plain_escape_str_plain_f_tuple, 0 ) );

        if ( tmp_called_name_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_called_name_40 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_42 = tmp_mvar_value_15;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 53;
        tmp_args_element_name_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_42, &PyTuple_GET_ITEM( const_tuple_str_digest_1e115ca000973c4222f04cf175132c7e_tuple, 0 ) );

        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_40 );

            exception_lineno = 53;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 53;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_call_result_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_40, call_args );
        }

        Py_DECREF( tmp_called_name_40 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_call_result_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_13 );
    }
    {
        PyObject *tmp_called_name_43;
        PyObject *tmp_called_name_44;
        PyObject *tmp_args_name_6;
        PyObject *tmp_kw_name_6;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_call_result_14;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_called_name_45;
        PyObject *tmp_mvar_value_16;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_44 = PyCell_GET( var_handle );
        tmp_args_name_6 = const_tuple_str_plain_escape_str_plain_l_tuple;
        tmp_dict_key_6 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_6 = var_insert_mode;
        tmp_kw_name_6 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_6, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 54;
        tmp_called_name_43 = CALL_FUNCTION( tmp_called_name_44, tmp_args_name_6, tmp_kw_name_6 );
        Py_DECREF( tmp_kw_name_6 );
        if ( tmp_called_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_called_name_43 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_45 = tmp_mvar_value_16;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 54;
        tmp_args_element_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_45, &PyTuple_GET_ITEM( const_tuple_str_digest_3e8aa0ff62897226c7230752efac5de8_tuple, 0 ) );

        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_43 );

            exception_lineno = 54;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 54;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_call_result_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_43, call_args );
        }

        Py_DECREF( tmp_called_name_43 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_call_result_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_14 );
    }
    {
        PyObject *tmp_called_name_46;
        PyObject *tmp_called_name_47;
        PyObject *tmp_args_name_7;
        PyObject *tmp_kw_name_7;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_call_result_15;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_called_name_48;
        PyObject *tmp_mvar_value_17;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_47 = PyCell_GET( var_handle );
        tmp_args_name_7 = const_tuple_str_plain_escape_str_plain_u_tuple;
        tmp_dict_key_7 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_7 = var_insert_mode;
        tmp_kw_name_7 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 55;
        tmp_called_name_46 = CALL_FUNCTION( tmp_called_name_47, tmp_args_name_7, tmp_kw_name_7 );
        Py_DECREF( tmp_kw_name_7 );
        if ( tmp_called_name_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_called_name_46 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_48 = tmp_mvar_value_17;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 55;
        tmp_args_element_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_48, &PyTuple_GET_ITEM( const_tuple_str_digest_e13b285e26a38d1a90b94ccc0668450c_tuple, 0 ) );

        if ( tmp_args_element_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_46 );

            exception_lineno = 55;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_call_result_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_46, call_args );
        }

        Py_DECREF( tmp_called_name_46 );
        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_call_result_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_15 );
    }
    {
        PyObject *tmp_called_name_49;
        PyObject *tmp_called_name_50;
        PyObject *tmp_args_name_8;
        PyObject *tmp_kw_name_8;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_call_result_16;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_called_name_51;
        PyObject *tmp_mvar_value_18;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_50 = PyCell_GET( var_handle );
        tmp_args_name_8 = const_tuple_str_plain_escape_str_plain_y_tuple;
        tmp_dict_key_8 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_8 = var_insert_mode;
        tmp_kw_name_8 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_8, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 56;
        tmp_called_name_49 = CALL_FUNCTION( tmp_called_name_50, tmp_args_name_8, tmp_kw_name_8 );
        Py_DECREF( tmp_kw_name_8 );
        if ( tmp_called_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_called_name_49 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 56;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_51 = tmp_mvar_value_18;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 56;
        tmp_args_element_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_51, &PyTuple_GET_ITEM( const_tuple_str_digest_0adea339c095a653a19ec3c66bfd3784_tuple, 0 ) );

        if ( tmp_args_element_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_49 );

            exception_lineno = 56;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 56;
        {
            PyObject *call_args[] = { tmp_args_element_name_17 };
            tmp_call_result_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_49, call_args );
        }

        Py_DECREF( tmp_called_name_49 );
        Py_DECREF( tmp_args_element_name_17 );
        if ( tmp_call_result_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_16 );
    }
    {
        PyObject *tmp_called_name_52;
        PyObject *tmp_called_name_53;
        PyObject *tmp_args_name_9;
        PyObject *tmp_kw_name_9;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_call_result_17;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_called_name_54;
        PyObject *tmp_mvar_value_19;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_53 = PyCell_GET( var_handle );
        tmp_args_name_9 = const_tuple_str_plain_escape_str_plain_backspace_tuple;
        tmp_dict_key_9 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_9 = var_insert_mode;
        tmp_kw_name_9 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_9, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 57;
        tmp_called_name_52 = CALL_FUNCTION( tmp_called_name_53, tmp_args_name_9, tmp_kw_name_9 );
        Py_DECREF( tmp_kw_name_9 );
        if ( tmp_called_name_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_19 == NULL )
        {
            Py_DECREF( tmp_called_name_52 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_54 = tmp_mvar_value_19;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 57;
        tmp_args_element_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_54, &PyTuple_GET_ITEM( const_tuple_str_digest_10c978016ae019e97a4da8b4078edf46_tuple, 0 ) );

        if ( tmp_args_element_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_52 );

            exception_lineno = 57;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 57;
        {
            PyObject *call_args[] = { tmp_args_element_name_18 };
            tmp_call_result_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_52, call_args );
        }

        Py_DECREF( tmp_called_name_52 );
        Py_DECREF( tmp_args_element_name_18 );
        if ( tmp_call_result_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_17 );
    }
    {
        PyObject *tmp_called_name_55;
        PyObject *tmp_called_name_56;
        PyObject *tmp_args_name_10;
        PyObject *tmp_kw_name_10;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_call_result_18;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_called_name_57;
        PyObject *tmp_mvar_value_20;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_56 = PyCell_GET( var_handle );
        tmp_args_name_10 = const_tuple_str_plain_escape_str_chr_92_tuple;
        tmp_dict_key_10 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_10 = var_insert_mode;
        tmp_kw_name_10 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_10, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 58;
        tmp_called_name_55 = CALL_FUNCTION( tmp_called_name_56, tmp_args_name_10, tmp_kw_name_10 );
        Py_DECREF( tmp_kw_name_10 );
        if ( tmp_called_name_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_20 == NULL )
        {
            Py_DECREF( tmp_called_name_55 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_57 = tmp_mvar_value_20;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 58;
        tmp_args_element_name_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_57, &PyTuple_GET_ITEM( const_tuple_str_digest_4d94ac19d21ce83f39e74e91d067ed52_tuple, 0 ) );

        if ( tmp_args_element_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_55 );

            exception_lineno = 58;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 58;
        {
            PyObject *call_args[] = { tmp_args_element_name_19 };
            tmp_call_result_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_55, call_args );
        }

        Py_DECREF( tmp_called_name_55 );
        Py_DECREF( tmp_args_element_name_19 );
        if ( tmp_call_result_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_18 );
    }
    {
        PyObject *tmp_called_name_58;
        PyObject *tmp_called_name_59;
        PyObject *tmp_args_name_11;
        PyObject *tmp_kw_name_11;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_call_result_19;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_called_name_60;
        PyObject *tmp_mvar_value_21;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_59 = PyCell_GET( var_handle );
        tmp_args_name_11 = const_tuple_str_digest_7230e58f12e9188dd6e7dddd9636a803_tuple;
        tmp_dict_key_11 = const_str_plain_save_before;
        tmp_dict_value_11 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_2_lambda(  );



        tmp_kw_name_11 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_11, tmp_dict_key_11, tmp_dict_value_11 );
        Py_DECREF( tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_12 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_12 = var_insert_mode;
        tmp_res = PyDict_SetItem( tmp_kw_name_11, tmp_dict_key_12, tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 60;
        tmp_called_name_58 = CALL_FUNCTION( tmp_called_name_59, tmp_args_name_11, tmp_kw_name_11 );
        Py_DECREF( tmp_kw_name_11 );
        if ( tmp_called_name_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_called_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_60 = tmp_mvar_value_21;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 61;
        tmp_args_element_name_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_60, &PyTuple_GET_ITEM( const_tuple_str_plain_undo_tuple, 0 ) );

        if ( tmp_args_element_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_58 );

            exception_lineno = 61;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_20 };
            tmp_call_result_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_58, call_args );
        }

        Py_DECREF( tmp_called_name_58 );
        Py_DECREF( tmp_args_element_name_20 );
        if ( tmp_call_result_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_19 );
    }
    {
        PyObject *tmp_called_name_61;
        PyObject *tmp_called_name_62;
        PyObject *tmp_args_name_12;
        PyObject *tmp_kw_name_12;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_call_result_20;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_called_name_63;
        PyObject *tmp_mvar_value_22;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_62 = PyCell_GET( var_handle );
        tmp_args_name_12 = const_tuple_94ff0c5df8d461e28a821d8cf948c033_tuple;
        tmp_dict_key_13 = const_str_plain_save_before;
        tmp_dict_value_13 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_3_lambda(  );



        tmp_kw_name_12 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_12, tmp_dict_key_13, tmp_dict_value_13 );
        Py_DECREF( tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_14 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_14 = var_insert_mode;
        tmp_res = PyDict_SetItem( tmp_kw_name_12, tmp_dict_key_14, tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 63;
        tmp_called_name_61 = CALL_FUNCTION( tmp_called_name_62, tmp_args_name_12, tmp_kw_name_12 );
        Py_DECREF( tmp_kw_name_12 );
        if ( tmp_called_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_called_name_61 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_63 = tmp_mvar_value_22;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 64;
        tmp_args_element_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_63, &PyTuple_GET_ITEM( const_tuple_str_plain_undo_tuple, 0 ) );

        if ( tmp_args_element_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_61 );

            exception_lineno = 64;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 63;
        {
            PyObject *call_args[] = { tmp_args_element_name_21 };
            tmp_call_result_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_61, call_args );
        }

        Py_DECREF( tmp_called_name_61 );
        Py_DECREF( tmp_args_element_name_21 );
        if ( tmp_call_result_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_20 );
    }
    {
        PyObject *tmp_called_name_64;
        PyObject *tmp_called_name_65;
        PyObject *tmp_args_name_13;
        PyObject *tmp_kw_name_13;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_call_result_21;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_called_name_66;
        PyObject *tmp_mvar_value_24;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_65 = PyCell_GET( var_handle );
        tmp_args_name_13 = const_tuple_str_plain_escape_str_chr_60_tuple;
        tmp_dict_key_15 = const_str_plain_filter;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_23 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 66;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_1 = tmp_mvar_value_23;
        tmp_dict_value_15 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_1 );
        if ( tmp_dict_value_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_13 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_13, tmp_dict_key_15, tmp_dict_value_15 );
        Py_DECREF( tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 66;
        tmp_called_name_64 = CALL_FUNCTION( tmp_called_name_65, tmp_args_name_13, tmp_kw_name_13 );
        Py_DECREF( tmp_kw_name_13 );
        if ( tmp_called_name_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_called_name_64 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 66;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_66 = tmp_mvar_value_24;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 66;
        tmp_args_element_name_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_66, &PyTuple_GET_ITEM( const_tuple_str_digest_4e5a5ed7c51e9863d1e36202b87fe43e_tuple, 0 ) );

        if ( tmp_args_element_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_64 );

            exception_lineno = 66;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 66;
        {
            PyObject *call_args[] = { tmp_args_element_name_22 };
            tmp_call_result_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_64, call_args );
        }

        Py_DECREF( tmp_called_name_64 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_call_result_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_21 );
    }
    {
        PyObject *tmp_called_name_67;
        PyObject *tmp_called_name_68;
        PyObject *tmp_args_name_14;
        PyObject *tmp_kw_name_14;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_call_result_22;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_called_name_69;
        PyObject *tmp_mvar_value_26;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_68 = PyCell_GET( var_handle );
        tmp_args_name_14 = const_tuple_str_plain_escape_str_chr_62_tuple;
        tmp_dict_key_16 = const_str_plain_filter;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_2 = tmp_mvar_value_25;
        tmp_dict_value_16 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_2 );
        if ( tmp_dict_value_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_14 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_14, tmp_dict_key_16, tmp_dict_value_16 );
        Py_DECREF( tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 67;
        tmp_called_name_67 = CALL_FUNCTION( tmp_called_name_68, tmp_args_name_14, tmp_kw_name_14 );
        Py_DECREF( tmp_kw_name_14 );
        if ( tmp_called_name_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_called_name_67 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_69 = tmp_mvar_value_26;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 67;
        tmp_args_element_name_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_69, &PyTuple_GET_ITEM( const_tuple_str_digest_70508f93fca682b73b6276fb31ac0cad_tuple, 0 ) );

        if ( tmp_args_element_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_67 );

            exception_lineno = 67;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_args_element_name_23 };
            tmp_call_result_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_67, call_args );
        }

        Py_DECREF( tmp_called_name_67 );
        Py_DECREF( tmp_args_element_name_23 );
        if ( tmp_call_result_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_22 );
    }
    {
        PyObject *tmp_called_name_70;
        PyObject *tmp_called_name_71;
        PyObject *tmp_args_name_15;
        PyObject *tmp_kw_name_15;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_call_result_23;
        PyObject *tmp_args_element_name_24;
        PyObject *tmp_called_name_72;
        PyObject *tmp_mvar_value_27;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_71 = PyCell_GET( var_handle );
        tmp_args_name_15 = const_tuple_str_plain_escape_str_dot_tuple;
        tmp_dict_key_17 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_17 = var_insert_mode;
        tmp_kw_name_15 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_15, tmp_dict_key_17, tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 69;
        tmp_called_name_70 = CALL_FUNCTION( tmp_called_name_71, tmp_args_name_15, tmp_kw_name_15 );
        Py_DECREF( tmp_kw_name_15 );
        if ( tmp_called_name_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_called_name_70 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_72 = tmp_mvar_value_27;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 69;
        tmp_args_element_name_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_72, &PyTuple_GET_ITEM( const_tuple_str_digest_c8c3694ea6fe9becb903618b30b45e7b_tuple, 0 ) );

        if ( tmp_args_element_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_70 );

            exception_lineno = 69;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 69;
        {
            PyObject *call_args[] = { tmp_args_element_name_24 };
            tmp_call_result_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_70, call_args );
        }

        Py_DECREF( tmp_called_name_70 );
        Py_DECREF( tmp_args_element_name_24 );
        if ( tmp_call_result_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_23 );
    }
    {
        PyObject *tmp_called_name_73;
        PyObject *tmp_called_name_74;
        PyObject *tmp_args_name_16;
        PyObject *tmp_kw_name_16;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_call_result_24;
        PyObject *tmp_args_element_name_25;
        PyObject *tmp_called_name_75;
        PyObject *tmp_mvar_value_28;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_74 = PyCell_GET( var_handle );
        tmp_args_name_16 = const_tuple_str_plain_escape_str_plain___tuple;
        tmp_dict_key_18 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_18 = var_insert_mode;
        tmp_kw_name_16 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_16, tmp_dict_key_18, tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 70;
        tmp_called_name_73 = CALL_FUNCTION( tmp_called_name_74, tmp_args_name_16, tmp_kw_name_16 );
        Py_DECREF( tmp_kw_name_16 );
        if ( tmp_called_name_73 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_called_name_73 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 70;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_75 = tmp_mvar_value_28;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 70;
        tmp_args_element_name_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_75, &PyTuple_GET_ITEM( const_tuple_str_digest_c8c3694ea6fe9becb903618b30b45e7b_tuple, 0 ) );

        if ( tmp_args_element_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_73 );

            exception_lineno = 70;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 70;
        {
            PyObject *call_args[] = { tmp_args_element_name_25 };
            tmp_call_result_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_73, call_args );
        }

        Py_DECREF( tmp_called_name_73 );
        Py_DECREF( tmp_args_element_name_25 );
        if ( tmp_call_result_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_24 );
    }
    {
        PyObject *tmp_called_name_76;
        PyObject *tmp_called_name_77;
        PyObject *tmp_args_name_17;
        PyObject *tmp_kw_name_17;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_call_result_25;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_called_name_78;
        PyObject *tmp_mvar_value_29;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_77 = PyCell_GET( var_handle );
        tmp_args_name_17 = const_tuple_str_plain_escape_str_digest_73eb09babb290e590b9a69835b5223c4_tuple;
        tmp_dict_key_19 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_19 = var_insert_mode;
        tmp_kw_name_17 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_17, tmp_dict_key_19, tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 71;
        tmp_called_name_76 = CALL_FUNCTION( tmp_called_name_77, tmp_args_name_17, tmp_kw_name_17 );
        Py_DECREF( tmp_kw_name_17 );
        if ( tmp_called_name_76 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_called_name_76 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_78 = tmp_mvar_value_29;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 71;
        tmp_args_element_name_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_78, &PyTuple_GET_ITEM( const_tuple_str_digest_bec9411c13b58be3a688ffe0c42ba2da_tuple, 0 ) );

        if ( tmp_args_element_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_76 );

            exception_lineno = 71;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 71;
        {
            PyObject *call_args[] = { tmp_args_element_name_26 };
            tmp_call_result_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_76, call_args );
        }

        Py_DECREF( tmp_called_name_76 );
        Py_DECREF( tmp_args_element_name_26 );
        if ( tmp_call_result_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_25 );
    }
    {
        PyObject *tmp_called_name_79;
        PyObject *tmp_called_name_80;
        PyObject *tmp_args_name_18;
        PyObject *tmp_kw_name_18;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_call_result_26;
        PyObject *tmp_args_element_name_27;
        PyObject *tmp_called_name_81;
        PyObject *tmp_mvar_value_30;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_80 = PyCell_GET( var_handle );
        tmp_args_name_18 = const_tuple_str_plain_escape_str_chr_35_tuple;
        tmp_dict_key_20 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_20 = var_insert_mode;
        tmp_kw_name_18 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_18, tmp_dict_key_20, tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 72;
        tmp_called_name_79 = CALL_FUNCTION( tmp_called_name_80, tmp_args_name_18, tmp_kw_name_18 );
        Py_DECREF( tmp_kw_name_18 );
        if ( tmp_called_name_79 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_called_name_79 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_81 = tmp_mvar_value_30;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 72;
        tmp_args_element_name_27 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_81, &PyTuple_GET_ITEM( const_tuple_str_digest_fb5c11df9835340c3dd4a642c111b2eb_tuple, 0 ) );

        if ( tmp_args_element_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_79 );

            exception_lineno = 72;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_27 };
            tmp_call_result_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_79, call_args );
        }

        Py_DECREF( tmp_called_name_79 );
        Py_DECREF( tmp_args_element_name_27 );
        if ( tmp_call_result_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_26 );
    }
    {
        PyObject *tmp_called_name_82;
        PyObject *tmp_called_name_83;
        PyObject *tmp_call_result_27;
        PyObject *tmp_args_element_name_28;
        PyObject *tmp_called_name_84;
        PyObject *tmp_mvar_value_31;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_83 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 73;
        tmp_called_name_82 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_83, &PyTuple_GET_ITEM( const_tuple_str_digest_90c174e3cde8d76e0cf5b8e86689f4a5_tuple, 0 ) );

        if ( tmp_called_name_82 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_31 == NULL )
        {
            Py_DECREF( tmp_called_name_82 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_84 = tmp_mvar_value_31;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 73;
        tmp_args_element_name_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_84, &PyTuple_GET_ITEM( const_tuple_str_digest_d4d66fa4b26f35cc7cfe3fc57dce3f65_tuple, 0 ) );

        if ( tmp_args_element_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_82 );

            exception_lineno = 73;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 73;
        {
            PyObject *call_args[] = { tmp_args_element_name_28 };
            tmp_call_result_27 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_82, call_args );
        }

        Py_DECREF( tmp_called_name_82 );
        Py_DECREF( tmp_args_element_name_28 );
        if ( tmp_call_result_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_27 );
    }
    {
        PyObject *tmp_called_name_85;
        PyObject *tmp_called_name_86;
        PyObject *tmp_args_name_19;
        PyObject *tmp_kw_name_19;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_operand_name_3;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_call_result_28;
        PyObject *tmp_args_element_name_29;
        PyObject *tmp_called_name_87;
        PyObject *tmp_mvar_value_33;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_86 = PyCell_GET( var_handle );
        tmp_args_name_19 = const_tuple_str_digest_ee5885bc3d3f26ad23a483060cd09f03_tuple;
        tmp_dict_key_21 = const_str_plain_filter;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_32 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 78;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_3 = tmp_mvar_value_32;
        tmp_dict_value_21 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_3 );
        if ( tmp_dict_value_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_19 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_19, tmp_dict_key_21, tmp_dict_value_21 );
        Py_DECREF( tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 78;
        tmp_called_name_85 = CALL_FUNCTION( tmp_called_name_86, tmp_args_name_19, tmp_kw_name_19 );
        Py_DECREF( tmp_kw_name_19 );
        if ( tmp_called_name_85 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_called_name_85 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 78;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_87 = tmp_mvar_value_33;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 78;
        tmp_args_element_name_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_87, &PyTuple_GET_ITEM( const_tuple_str_digest_f8c276bab5a9ae14be8475d6e0e75e09_tuple, 0 ) );

        if ( tmp_args_element_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_85 );

            exception_lineno = 78;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 78;
        {
            PyObject *call_args[] = { tmp_args_element_name_29 };
            tmp_call_result_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_85, call_args );
        }

        Py_DECREF( tmp_called_name_85 );
        Py_DECREF( tmp_args_element_name_29 );
        if ( tmp_call_result_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_28 );
    }
    {
        PyObject *tmp_called_name_88;
        PyObject *tmp_called_name_89;
        PyObject *tmp_call_result_29;
        PyObject *tmp_args_element_name_30;
        PyObject *tmp_called_name_90;
        PyObject *tmp_mvar_value_34;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_89 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 80;
        tmp_called_name_88 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_89, &PyTuple_GET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_40_tuple, 0 ) );

        if ( tmp_called_name_88 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_called_name_88 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_90 = tmp_mvar_value_34;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 80;
        tmp_args_element_name_30 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_90, &PyTuple_GET_ITEM( const_tuple_str_digest_402d95161f3800c90c59735f4d8c4c59_tuple, 0 ) );

        if ( tmp_args_element_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_88 );

            exception_lineno = 80;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 80;
        {
            PyObject *call_args[] = { tmp_args_element_name_30 };
            tmp_call_result_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_88, call_args );
        }

        Py_DECREF( tmp_called_name_88 );
        Py_DECREF( tmp_args_element_name_30 );
        if ( tmp_call_result_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_29 );
    }
    {
        PyObject *tmp_called_name_91;
        PyObject *tmp_called_name_92;
        PyObject *tmp_call_result_30;
        PyObject *tmp_args_element_name_31;
        PyObject *tmp_called_name_93;
        PyObject *tmp_mvar_value_35;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_92 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 81;
        tmp_called_name_91 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_92, &PyTuple_GET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_chr_41_tuple, 0 ) );

        if ( tmp_called_name_91 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_35 == NULL )
        {
            Py_DECREF( tmp_called_name_91 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_93 = tmp_mvar_value_35;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 81;
        tmp_args_element_name_31 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_93, &PyTuple_GET_ITEM( const_tuple_str_digest_c014cbade4743da3cd8e8b1fadea68a6_tuple, 0 ) );

        if ( tmp_args_element_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_91 );

            exception_lineno = 81;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 81;
        {
            PyObject *call_args[] = { tmp_args_element_name_31 };
            tmp_call_result_30 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_91, call_args );
        }

        Py_DECREF( tmp_called_name_91 );
        Py_DECREF( tmp_args_element_name_31 );
        if ( tmp_call_result_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_30 );
    }
    {
        PyObject *tmp_called_name_94;
        PyObject *tmp_called_name_95;
        PyObject *tmp_call_result_31;
        PyObject *tmp_args_element_name_32;
        PyObject *tmp_called_name_96;
        PyObject *tmp_mvar_value_36;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_95 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 82;
        tmp_called_name_94 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_95, &PyTuple_GET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_str_plain_e_tuple, 0 ) );

        if ( tmp_called_name_94 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_36 == NULL )
        {
            Py_DECREF( tmp_called_name_94 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_96 = tmp_mvar_value_36;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 82;
        tmp_args_element_name_32 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_96, &PyTuple_GET_ITEM( const_tuple_str_digest_ae692cb46ab2cf52c0be9c11d31b5318_tuple, 0 ) );

        if ( tmp_args_element_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_94 );

            exception_lineno = 82;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 82;
        {
            PyObject *call_args[] = { tmp_args_element_name_32 };
            tmp_call_result_31 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_94, call_args );
        }

        Py_DECREF( tmp_called_name_94 );
        Py_DECREF( tmp_args_element_name_32 );
        if ( tmp_call_result_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_31 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_97;
        PyObject *tmp_called_name_98;
        PyObject *tmp_args_element_name_33;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_98 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 84;
        tmp_called_name_97 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_98, &PyTuple_GET_ITEM( const_tuple_str_digest_daa690fd2d0c2002d9c7aad7dc2bacf6_tuple, 0 ) );

        if ( tmp_called_name_97 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_33 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_4__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 84;
        {
            PyObject *call_args[] = { tmp_args_element_name_33 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_97, call_args );
        }

        Py_DECREF( tmp_called_name_97 );
        Py_DECREF( tmp_args_element_name_33 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_5;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_99;
        PyObject *tmp_called_name_100;
        PyObject *tmp_args_element_name_34;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_100 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 89;
        tmp_called_name_99 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_100, &PyTuple_GET_ITEM( const_tuple_str_digest_db82dd148fa872efe07c2eb351c2c33f_tuple, 0 ) );

        if ( tmp_called_name_99 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_34 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_5__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 89;
        {
            PyObject *call_args[] = { tmp_args_element_name_34 };
            tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_99, call_args );
        }

        Py_DECREF( tmp_called_name_99 );
        Py_DECREF( tmp_args_element_name_34 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_6;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[0] = var_handle;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[0] );


        assert( var_handle_digit == NULL );
        var_handle_digit = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_iter_arg_1;
        tmp_iter_arg_1 = const_str_plain_0123456789;
        tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_1 );
        assert( !(tmp_assign_source_8 == NULL) );
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_8;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_9 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ocoooooc";
                exception_lineno = 104;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_10 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_c;
            var_c = tmp_assign_source_10;
            Py_INCREF( var_c );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_101;
        PyObject *tmp_call_result_32;
        PyObject *tmp_args_element_name_35;
        CHECK_OBJECT( var_handle_digit );
        tmp_called_name_101 = var_handle_digit;
        CHECK_OBJECT( var_c );
        tmp_args_element_name_35 = var_c;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 105;
        {
            PyObject *call_args[] = { tmp_args_element_name_35 };
            tmp_call_result_32 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_101, call_args );
        }

        if ( tmp_call_result_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "ocoooooc";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_32 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;
        type_description_1 = "ocoooooc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_name_102;
        PyObject *tmp_called_name_103;
        PyObject *tmp_args_name_20;
        PyObject *tmp_kw_name_20;
        PyObject *tmp_dict_key_22;
        PyObject *tmp_dict_value_22;
        PyObject *tmp_operand_name_4;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_args_element_name_36;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_103 = PyCell_GET( var_handle );
        tmp_args_name_20 = const_tuple_str_plain_escape_str_chr_45_tuple;
        tmp_dict_key_22 = const_str_plain_filter;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_arg );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_arg );
        }

        if ( tmp_mvar_value_37 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_arg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_4 = tmp_mvar_value_37;
        tmp_dict_value_22 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_4 );
        if ( tmp_dict_value_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_20 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_20, tmp_dict_key_22, tmp_dict_value_22 );
        Py_DECREF( tmp_dict_value_22 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 107;
        tmp_called_name_102 = CALL_FUNCTION( tmp_called_name_103, tmp_args_name_20, tmp_kw_name_20 );
        Py_DECREF( tmp_kw_name_20 );
        if ( tmp_called_name_102 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_36 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_7__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 107;
        {
            PyObject *call_args[] = { tmp_args_element_name_36 };
            tmp_assign_source_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_102, call_args );
        }

        Py_DECREF( tmp_called_name_102 );
        Py_DECREF( tmp_args_element_name_36 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_11;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_called_name_104;
        PyObject *tmp_called_name_105;
        PyObject *tmp_args_name_21;
        PyObject *tmp_kw_name_21;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_called_name_106;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_args_element_name_37;
        PyObject *tmp_args_element_name_38;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_105 = PyCell_GET( var_handle );
        tmp_args_name_21 = const_tuple_str_chr_45_tuple;
        tmp_dict_key_23 = const_str_plain_filter;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_Condition );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Condition );
        }

        if ( tmp_mvar_value_38 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Condition" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_106 = tmp_mvar_value_38;
        tmp_args_element_name_37 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_9_lambda(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 114;
        {
            PyObject *call_args[] = { tmp_args_element_name_37 };
            tmp_dict_value_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_106, call_args );
        }

        Py_DECREF( tmp_args_element_name_37 );
        if ( tmp_dict_value_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_21 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_21, tmp_dict_key_23, tmp_dict_value_23 );
        Py_DECREF( tmp_dict_value_23 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 114;
        tmp_called_name_104 = CALL_FUNCTION( tmp_called_name_105, tmp_args_name_21, tmp_kw_name_21 );
        Py_DECREF( tmp_kw_name_21 );
        if ( tmp_called_name_104 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_38 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_8__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 114;
        {
            PyObject *call_args[] = { tmp_args_element_name_38 };
            tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_104, call_args );
        }

        Py_DECREF( tmp_called_name_104 );
        Py_DECREF( tmp_args_element_name_38 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_12;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_called_name_107;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_args_element_name_39;
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_Condition );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Condition );
        }

        if ( tmp_mvar_value_39 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Condition" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_107 = tmp_mvar_value_39;
        tmp_args_element_name_39 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_10_is_returnable(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_39 };
            tmp_assign_source_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_107, call_args );
        }

        Py_DECREF( tmp_args_element_name_39 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        assert( var_is_returnable == NULL );
        var_is_returnable = tmp_assign_source_13;
    }
    {
        PyObject *tmp_called_name_108;
        PyObject *tmp_called_name_109;
        PyObject *tmp_args_name_22;
        PyObject *tmp_kw_name_22;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_call_result_33;
        PyObject *tmp_args_element_name_40;
        PyObject *tmp_called_name_110;
        PyObject *tmp_mvar_value_40;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_109 = PyCell_GET( var_handle );
        tmp_args_name_22 = const_tuple_str_plain_escape_str_plain_enter_tuple;
        tmp_dict_key_24 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_left_name_1 = var_insert_mode;
        CHECK_OBJECT( var_is_returnable );
        tmp_right_name_1 = var_is_returnable;
        tmp_dict_value_24 = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_dict_value_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_22 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_22, tmp_dict_key_24, tmp_dict_value_24 );
        Py_DECREF( tmp_dict_value_24 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 127;
        tmp_called_name_108 = CALL_FUNCTION( tmp_called_name_109, tmp_args_name_22, tmp_kw_name_22 );
        Py_DECREF( tmp_kw_name_22 );
        if ( tmp_called_name_108 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_40 == NULL )
        {
            Py_DECREF( tmp_called_name_108 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_110 = tmp_mvar_value_40;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 128;
        tmp_args_element_name_40 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_110, &PyTuple_GET_ITEM( const_tuple_str_digest_bc52841ed7d0c0e991090611ebb55c62_tuple, 0 ) );

        if ( tmp_args_element_name_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_108 );

            exception_lineno = 128;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 127;
        {
            PyObject *call_args[] = { tmp_args_element_name_40 };
            tmp_call_result_33 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_108, call_args );
        }

        Py_DECREF( tmp_called_name_108 );
        Py_DECREF( tmp_args_element_name_40 );
        if ( tmp_call_result_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_33 );
    }
    {
        PyObject *tmp_called_name_111;
        PyObject *tmp_called_name_112;
        PyObject *tmp_args_name_23;
        PyObject *tmp_kw_name_23;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_operand_name_5;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_call_result_34;
        PyObject *tmp_args_element_name_41;
        PyObject *tmp_called_name_113;
        PyObject *tmp_mvar_value_42;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_112 = PyCell_GET( var_handle );
        tmp_args_name_23 = const_tuple_str_plain_enter_tuple;
        tmp_dict_key_25 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_left_name_3 = var_insert_mode;
        CHECK_OBJECT( var_is_returnable );
        tmp_right_name_2 = var_is_returnable;
        tmp_left_name_2 = BINARY_OPERATION( PyNumber_And, tmp_left_name_3, tmp_right_name_2 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_is_multiline );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_multiline );
        }

        if ( tmp_mvar_value_41 == NULL )
        {
            Py_DECREF( tmp_left_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_multiline" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_5 = tmp_mvar_value_41;
        tmp_right_name_3 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_5 );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_2 );

            exception_lineno = 131;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_25 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_2 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_dict_value_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_23 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_23, tmp_dict_key_25, tmp_dict_value_25 );
        Py_DECREF( tmp_dict_value_25 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 131;
        tmp_called_name_111 = CALL_FUNCTION( tmp_called_name_112, tmp_args_name_23, tmp_kw_name_23 );
        Py_DECREF( tmp_kw_name_23 );
        if ( tmp_called_name_111 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_42 == NULL )
        {
            Py_DECREF( tmp_called_name_111 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_113 = tmp_mvar_value_42;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 132;
        tmp_args_element_name_41 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_113, &PyTuple_GET_ITEM( const_tuple_str_digest_bc52841ed7d0c0e991090611ebb55c62_tuple, 0 ) );

        if ( tmp_args_element_name_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_111 );

            exception_lineno = 132;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 131;
        {
            PyObject *call_args[] = { tmp_args_element_name_41 };
            tmp_call_result_34 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_111, call_args );
        }

        Py_DECREF( tmp_called_name_111 );
        Py_DECREF( tmp_args_element_name_41 );
        if ( tmp_call_result_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_34 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_11_character_search(  );



        assert( PyCell_GET( var_character_search ) == NULL );
        PyCell_SET( var_character_search, tmp_assign_source_14 );

    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_called_name_114;
        PyObject *tmp_called_name_115;
        PyObject *tmp_args_element_name_42;
        PyObject *tmp_args_element_name_43;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_args_element_name_44;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_115 = PyCell_GET( var_handle );
        tmp_args_element_name_42 = const_str_digest_cccb566811e08cc4fd2e3ae406952507;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_43 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_43;
        tmp_args_element_name_43 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_Any );
        if ( tmp_args_element_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 143;
        {
            PyObject *call_args[] = { tmp_args_element_name_42, tmp_args_element_name_43 };
            tmp_called_name_114 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_115, call_args );
        }

        Py_DECREF( tmp_args_element_name_43 );
        if ( tmp_called_name_114 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_44 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_12__(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_44)->m_closure[0] = var_character_search;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_44)->m_closure[0] );


        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 143;
        {
            PyObject *call_args[] = { tmp_args_element_name_44 };
            tmp_assign_source_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_114, call_args );
        }

        Py_DECREF( tmp_called_name_114 );
        Py_DECREF( tmp_args_element_name_44 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_15;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_called_name_116;
        PyObject *tmp_called_name_117;
        PyObject *tmp_args_element_name_45;
        PyObject *tmp_args_element_name_46;
        PyObject *tmp_args_element_name_47;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_args_element_name_48;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_117 = PyCell_GET( var_handle );
        tmp_args_element_name_45 = const_str_plain_escape;
        tmp_args_element_name_46 = const_str_digest_cccb566811e08cc4fd2e3ae406952507;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_44 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_44;
        tmp_args_element_name_47 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_Any );
        if ( tmp_args_element_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 149;
        {
            PyObject *call_args[] = { tmp_args_element_name_45, tmp_args_element_name_46, tmp_args_element_name_47 };
            tmp_called_name_116 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_117, call_args );
        }

        Py_DECREF( tmp_args_element_name_47 );
        if ( tmp_called_name_116 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_48 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_13__(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_48)->m_closure[0] = var_character_search;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_48)->m_closure[0] );


        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 149;
        {
            PyObject *call_args[] = { tmp_args_element_name_48 };
            tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_116, call_args );
        }

        Py_DECREF( tmp_called_name_116 );
        Py_DECREF( tmp_args_element_name_48 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_16;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_called_name_118;
        PyObject *tmp_called_name_119;
        PyObject *tmp_args_element_name_49;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_119 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 155;
        tmp_called_name_118 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_119, &PyTuple_GET_ITEM( const_tuple_str_plain_escape_str_plain_a_tuple, 0 ) );

        if ( tmp_called_name_118 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_49 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_14__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 155;
        {
            PyObject *call_args[] = { tmp_args_element_name_49 };
            tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_118, call_args );
        }

        Py_DECREF( tmp_called_name_118 );
        Py_DECREF( tmp_args_element_name_49 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_17;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_called_name_120;
        PyObject *tmp_called_name_121;
        PyObject *tmp_args_element_name_50;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_121 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 160;
        tmp_called_name_120 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_121, &PyTuple_GET_ITEM( const_tuple_str_plain_escape_str_plain_e_tuple, 0 ) );

        if ( tmp_called_name_120 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_50 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_15__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 160;
        {
            PyObject *call_args[] = { tmp_args_element_name_50 };
            tmp_assign_source_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_120, call_args );
        }

        Py_DECREF( tmp_called_name_120 );
        Py_DECREF( tmp_args_element_name_50 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_18;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_called_name_122;
        PyObject *tmp_called_name_123;
        PyObject *tmp_args_name_24;
        PyObject *tmp_kw_name_24;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_args_element_name_51;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_123 = PyCell_GET( var_handle );
        tmp_args_name_24 = const_tuple_str_plain_escape_str_plain_t_tuple;
        tmp_dict_key_26 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_26 = var_insert_mode;
        tmp_kw_name_24 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_24, tmp_dict_key_26, tmp_dict_value_26 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 165;
        tmp_called_name_122 = CALL_FUNCTION( tmp_called_name_123, tmp_args_name_24, tmp_kw_name_24 );
        Py_DECREF( tmp_kw_name_24 );
        if ( tmp_called_name_122 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_51 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_16__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 165;
        {
            PyObject *call_args[] = { tmp_args_element_name_51 };
            tmp_assign_source_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_122, call_args );
        }

        Py_DECREF( tmp_called_name_122 );
        Py_DECREF( tmp_args_element_name_51 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_19;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_called_name_124;
        PyObject *tmp_called_name_125;
        PyObject *tmp_args_name_25;
        PyObject *tmp_kw_name_25;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        PyObject *tmp_args_element_name_52;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_125 = PyCell_GET( var_handle );
        tmp_args_name_25 = const_tuple_str_plain_escape_str_chr_42_tuple;
        tmp_dict_key_27 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_27 = var_insert_mode;
        tmp_kw_name_25 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_25, tmp_dict_key_27, tmp_dict_value_27 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 172;
        tmp_called_name_124 = CALL_FUNCTION( tmp_called_name_125, tmp_args_name_25, tmp_kw_name_25 );
        Py_DECREF( tmp_kw_name_25 );
        if ( tmp_called_name_124 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_52 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 172;
        {
            PyObject *call_args[] = { tmp_args_element_name_52 };
            tmp_assign_source_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_124, call_args );
        }

        Py_DECREF( tmp_called_name_124 );
        Py_DECREF( tmp_args_element_name_52 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_20;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_name_126;
        PyObject *tmp_called_name_127;
        PyObject *tmp_args_element_name_53;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_127 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 187;
        tmp_called_name_126 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_127, &PyTuple_GET_ITEM( const_tuple_7ed3ae015e22d01e89cce13238d41ef3_tuple, 0 ) );

        if ( tmp_called_name_126 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_53 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_18__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 187;
        {
            PyObject *call_args[] = { tmp_args_element_name_53 };
            tmp_assign_source_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_126, call_args );
        }

        Py_DECREF( tmp_called_name_126 );
        Py_DECREF( tmp_args_element_name_53 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_21;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_called_name_128;
        PyObject *tmp_called_name_129;
        PyObject *tmp_args_element_name_54;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_129 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 200;
        tmp_called_name_128 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_129, &PyTuple_GET_ITEM( const_tuple_str_digest_594981de1deffed7357a6afed608966a_tuple, 0 ) );

        if ( tmp_called_name_128 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 200;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_54 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_19__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 200;
        {
            PyObject *call_args[] = { tmp_args_element_name_54 };
            tmp_assign_source_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_128, call_args );
        }

        Py_DECREF( tmp_called_name_128 );
        Py_DECREF( tmp_args_element_name_54 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 200;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_22;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_called_name_130;
        PyObject *tmp_called_name_131;
        PyObject *tmp_args_name_26;
        PyObject *tmp_kw_name_26;
        PyObject *tmp_dict_key_28;
        PyObject *tmp_dict_value_28;
        PyObject *tmp_operand_name_6;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_args_element_name_55;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_131 = PyCell_GET( var_handle );
        tmp_args_name_26 = const_tuple_str_digest_faa2df820fbcc927d6a1cbff6da2d7e2_tuple;
        tmp_dict_key_28 = const_str_plain_filter;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_45 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 210;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_6 = tmp_mvar_value_45;
        tmp_dict_value_28 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_6 );
        if ( tmp_dict_value_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_26 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_26, tmp_dict_key_28, tmp_dict_value_28 );
        Py_DECREF( tmp_dict_value_28 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 210;
        tmp_called_name_130 = CALL_FUNCTION( tmp_called_name_131, tmp_args_name_26, tmp_kw_name_26 );
        Py_DECREF( tmp_kw_name_26 );
        if ( tmp_called_name_130 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_55 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_20__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 210;
        {
            PyObject *call_args[] = { tmp_args_element_name_55 };
            tmp_assign_source_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_130, call_args );
        }

        Py_DECREF( tmp_called_name_130 );
        Py_DECREF( tmp_args_element_name_55 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_23;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_called_name_132;
        PyObject *tmp_called_name_133;
        PyObject *tmp_args_name_27;
        PyObject *tmp_kw_name_27;
        PyObject *tmp_dict_key_29;
        PyObject *tmp_dict_value_29;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_args_element_name_56;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_133 = PyCell_GET( var_handle );
        tmp_args_name_27 = const_tuple_str_digest_faa2df820fbcc927d6a1cbff6da2d7e2_tuple;
        tmp_dict_key_29 = const_str_plain_filter;
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_46 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 218;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_29 = tmp_mvar_value_46;
        tmp_kw_name_27 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_27, tmp_dict_key_29, tmp_dict_value_29 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 218;
        tmp_called_name_132 = CALL_FUNCTION( tmp_called_name_133, tmp_args_name_27, tmp_kw_name_27 );
        Py_DECREF( tmp_kw_name_27 );
        if ( tmp_called_name_132 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_56 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_21__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 218;
        {
            PyObject *call_args[] = { tmp_args_element_name_56 };
            tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_132, call_args );
        }

        Py_DECREF( tmp_called_name_132 );
        Py_DECREF( tmp_args_element_name_56 );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_24;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_name_134;
        PyObject *tmp_called_name_135;
        PyObject *tmp_args_name_28;
        PyObject *tmp_kw_name_28;
        PyObject *tmp_dict_key_30;
        PyObject *tmp_dict_value_30;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_args_element_name_57;
        PyObject *tmp_called_name_136;
        PyObject *tmp_called_name_137;
        PyObject *tmp_args_name_29;
        PyObject *tmp_kw_name_29;
        PyObject *tmp_dict_key_31;
        PyObject *tmp_dict_value_31;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_args_element_name_58;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_135 = PyCell_GET( var_handle );
        tmp_args_name_28 = const_tuple_str_digest_d8156c92c3ac39743ec9345e59bfd2ed_tuple;
        tmp_dict_key_30 = const_str_plain_filter;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_47 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 225;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_30 = tmp_mvar_value_47;
        tmp_kw_name_28 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_28, tmp_dict_key_30, tmp_dict_value_30 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 225;
        tmp_called_name_134 = CALL_FUNCTION( tmp_called_name_135, tmp_args_name_28, tmp_kw_name_28 );
        Py_DECREF( tmp_kw_name_28 );
        if ( tmp_called_name_134 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_137 = PyCell_GET( var_handle );
        tmp_args_name_29 = const_tuple_a95f436c69d0cb7b7ffbbe853fc5d8cf_tuple;
        tmp_dict_key_31 = const_str_plain_filter;
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_48 == NULL )
        {
            Py_DECREF( tmp_called_name_134 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 226;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_31 = tmp_mvar_value_48;
        tmp_kw_name_29 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_29, tmp_dict_key_31, tmp_dict_value_31 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 226;
        tmp_called_name_136 = CALL_FUNCTION( tmp_called_name_137, tmp_args_name_29, tmp_kw_name_29 );
        Py_DECREF( tmp_kw_name_29 );
        if ( tmp_called_name_136 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_134 );

            exception_lineno = 226;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_58 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_22__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 226;
        {
            PyObject *call_args[] = { tmp_args_element_name_58 };
            tmp_args_element_name_57 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_136, call_args );
        }

        Py_DECREF( tmp_called_name_136 );
        Py_DECREF( tmp_args_element_name_58 );
        if ( tmp_args_element_name_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_134 );

            exception_lineno = 226;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 225;
        {
            PyObject *call_args[] = { tmp_args_element_name_57 };
            tmp_assign_source_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_134, call_args );
        }

        Py_DECREF( tmp_called_name_134 );
        Py_DECREF( tmp_args_element_name_57 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_25;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_called_name_138;
        PyObject *tmp_called_name_139;
        PyObject *tmp_args_name_30;
        PyObject *tmp_kw_name_30;
        PyObject *tmp_dict_key_32;
        PyObject *tmp_dict_value_32;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_args_element_name_59;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_139 = PyCell_GET( var_handle );
        tmp_args_name_30 = const_tuple_str_plain_escape_str_plain_w_tuple;
        tmp_dict_key_32 = const_str_plain_filter;
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_49 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 234;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_32 = tmp_mvar_value_49;
        tmp_kw_name_30 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_30, tmp_dict_key_32, tmp_dict_value_32 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 234;
        tmp_called_name_138 = CALL_FUNCTION( tmp_called_name_139, tmp_args_name_30, tmp_kw_name_30 );
        Py_DECREF( tmp_kw_name_30 );
        if ( tmp_called_name_138 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 234;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_59 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_23__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 234;
        {
            PyObject *call_args[] = { tmp_args_element_name_59 };
            tmp_assign_source_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_138, call_args );
        }

        Py_DECREF( tmp_called_name_138 );
        Py_DECREF( tmp_args_element_name_59 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 234;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_26;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_called_name_140;
        PyObject *tmp_called_name_141;
        PyObject *tmp_args_element_name_60;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_141 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 242;
        tmp_called_name_140 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_141, &PyTuple_GET_ITEM( const_tuple_str_plain_escape_str_plain_left_tuple, 0 ) );

        if ( tmp_called_name_140 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 242;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_60 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_24__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 242;
        {
            PyObject *call_args[] = { tmp_args_element_name_60 };
            tmp_assign_source_27 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_140, call_args );
        }

        Py_DECREF( tmp_called_name_140 );
        Py_DECREF( tmp_args_element_name_60 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 242;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_27;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_called_name_142;
        PyObject *tmp_called_name_143;
        PyObject *tmp_args_element_name_61;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_143 = PyCell_GET( var_handle );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 250;
        tmp_called_name_142 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_143, &PyTuple_GET_ITEM( const_tuple_str_plain_escape_str_plain_right_tuple, 0 ) );

        if ( tmp_called_name_142 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 250;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_61 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_25__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 250;
        {
            PyObject *call_args[] = { tmp_args_element_name_61 };
            tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_142, call_args );
        }

        Py_DECREF( tmp_called_name_142 );
        Py_DECREF( tmp_args_element_name_61 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 250;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_28;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_called_name_144;
        PyObject *tmp_called_name_145;
        PyObject *tmp_args_name_31;
        PyObject *tmp_kw_name_31;
        PyObject *tmp_dict_key_33;
        PyObject *tmp_dict_value_33;
        PyObject *tmp_args_element_name_62;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_145 = PyCell_GET( var_handle );
        tmp_args_name_31 = const_tuple_str_plain_escape_str_chr_47_tuple;
        tmp_dict_key_33 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_33 = var_insert_mode;
        tmp_kw_name_31 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_31, tmp_dict_key_33, tmp_dict_value_33 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 259;
        tmp_called_name_144 = CALL_FUNCTION( tmp_called_name_145, tmp_args_name_31, tmp_kw_name_31 );
        Py_DECREF( tmp_kw_name_31 );
        if ( tmp_called_name_144 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_62 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_26__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 259;
        {
            PyObject *call_args[] = { tmp_args_element_name_62 };
            tmp_assign_source_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_144, call_args );
        }

        Py_DECREF( tmp_called_name_144 );
        Py_DECREF( tmp_args_element_name_62 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_29;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_called_name_146;
        PyObject *tmp_called_name_147;
        PyObject *tmp_args_name_32;
        PyObject *tmp_kw_name_32;
        PyObject *tmp_dict_key_34;
        PyObject *tmp_dict_value_34;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_args_element_name_63;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_147 = PyCell_GET( var_handle );
        tmp_args_name_32 = const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_62_tuple;
        tmp_dict_key_34 = const_str_plain_filter;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_50 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 270;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_34 = tmp_mvar_value_50;
        tmp_kw_name_32 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_32, tmp_dict_key_34, tmp_dict_value_34 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 270;
        tmp_called_name_146 = CALL_FUNCTION( tmp_called_name_147, tmp_args_name_32, tmp_kw_name_32 );
        Py_DECREF( tmp_kw_name_32 );
        if ( tmp_called_name_146 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 270;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_63 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_27__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 270;
        {
            PyObject *call_args[] = { tmp_args_element_name_63 };
            tmp_assign_source_30 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_146, call_args );
        }

        Py_DECREF( tmp_called_name_146 );
        Py_DECREF( tmp_args_element_name_63 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 270;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_30;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_called_name_148;
        PyObject *tmp_called_name_149;
        PyObject *tmp_args_name_33;
        PyObject *tmp_kw_name_33;
        PyObject *tmp_dict_key_35;
        PyObject *tmp_dict_value_35;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_args_element_name_64;
        CHECK_OBJECT( PyCell_GET( var_handle ) );
        tmp_called_name_149 = PyCell_GET( var_handle );
        tmp_args_name_33 = const_tuple_str_digest_99c20ff6137166a3040444a10775418c_str_chr_60_tuple;
        tmp_dict_key_35 = const_str_plain_filter;
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_51 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 285;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_35 = tmp_mvar_value_51;
        tmp_kw_name_33 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_33, tmp_dict_key_35, tmp_dict_value_35 );
        assert( !(tmp_res != 0) );
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 285;
        tmp_called_name_148 = CALL_FUNCTION( tmp_called_name_149, tmp_args_name_33, tmp_kw_name_33 );
        Py_DECREF( tmp_kw_name_33 );
        if ( tmp_called_name_148 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 285;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_64 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_28__(  );



        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 285;
        {
            PyObject *call_args[] = { tmp_args_element_name_64 };
            tmp_assign_source_31 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_148, call_args );
        }

        Py_DECREF( tmp_called_name_148 );
        Py_DECREF( tmp_args_element_name_64 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 285;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_31;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_150;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_args_element_name_65;
        PyObject *tmp_args_element_name_66;
        PyObject *tmp_mvar_value_53;
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_ConditionalKeyBindings );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ConditionalKeyBindings );
        }

        if ( tmp_mvar_value_52 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ConditionalKeyBindings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 298;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_150 = tmp_mvar_value_52;
        CHECK_OBJECT( var_key_bindings );
        tmp_args_element_name_65 = var_key_bindings;
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_emacs_mode );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_emacs_mode );
        }

        if ( tmp_mvar_value_53 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "emacs_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 298;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_66 = tmp_mvar_value_53;
        frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame.f_lineno = 298;
        {
            PyObject *call_args[] = { tmp_args_element_name_65, tmp_args_element_name_66 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_150, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 298;
            type_description_1 = "ocoooooc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5dbd4e3413cc7f67ed8c68e401cdc48e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5dbd4e3413cc7f67ed8c68e401cdc48e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5dbd4e3413cc7f67ed8c68e401cdc48e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5dbd4e3413cc7f67ed8c68e401cdc48e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5dbd4e3413cc7f67ed8c68e401cdc48e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5dbd4e3413cc7f67ed8c68e401cdc48e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5dbd4e3413cc7f67ed8c68e401cdc48e,
        type_description_1,
        var_key_bindings,
        var_handle,
        var_insert_mode,
        var__,
        var_handle_digit,
        var_c,
        var_is_returnable,
        var_character_search
    );


    // Release cached frame.
    if ( frame_5dbd4e3413cc7f67ed8c68e401cdc48e == cache_frame_5dbd4e3413cc7f67ed8c68e401cdc48e )
    {
        Py_DECREF( frame_5dbd4e3413cc7f67ed8c68e401cdc48e );
    }
    cache_frame_5dbd4e3413cc7f67ed8c68e401cdc48e = NULL;

    assertFrameObject( frame_5dbd4e3413cc7f67ed8c68e401cdc48e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_key_bindings );
    Py_DECREF( var_key_bindings );
    var_key_bindings = NULL;

    CHECK_OBJECT( (PyObject *)var_handle );
    Py_DECREF( var_handle );
    var_handle = NULL;

    CHECK_OBJECT( (PyObject *)var_insert_mode );
    Py_DECREF( var_insert_mode );
    var_insert_mode = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    CHECK_OBJECT( (PyObject *)var_handle_digit );
    Py_DECREF( var_handle_digit );
    var_handle_digit = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    CHECK_OBJECT( (PyObject *)var_is_returnable );
    Py_DECREF( var_is_returnable );
    var_is_returnable = NULL;

    CHECK_OBJECT( (PyObject *)var_character_search );
    Py_DECREF( var_character_search );
    var_character_search = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_key_bindings );
    var_key_bindings = NULL;

    CHECK_OBJECT( (PyObject *)var_handle );
    Py_DECREF( var_handle );
    var_handle = NULL;

    Py_XDECREF( var_insert_mode );
    var_insert_mode = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_handle_digit );
    var_handle_digit = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    Py_XDECREF( var_is_returnable );
    var_is_returnable = NULL;

    CHECK_OBJECT( (PyObject *)var_character_search );
    Py_DECREF( var_character_search );
    var_character_search = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_1__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_1__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_1__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_2_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_e = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_False;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_2_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_2_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_3_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_e = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_False;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_3_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_e );
    Py_DECREF( par_e );
    par_e = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_3_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_4__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b4d2d8c8516dc5482abf143712f11867;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b4d2d8c8516dc5482abf143712f11867 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b4d2d8c8516dc5482abf143712f11867, codeobj_b4d2d8c8516dc5482abf143712f11867, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *) );
    frame_b4d2d8c8516dc5482abf143712f11867 = cache_frame_b4d2d8c8516dc5482abf143712f11867;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b4d2d8c8516dc5482abf143712f11867 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b4d2d8c8516dc5482abf143712f11867 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_b4d2d8c8516dc5482abf143712f11867->m_frame.f_lineno = 87;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_auto_down );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b4d2d8c8516dc5482abf143712f11867 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b4d2d8c8516dc5482abf143712f11867 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b4d2d8c8516dc5482abf143712f11867, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b4d2d8c8516dc5482abf143712f11867->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b4d2d8c8516dc5482abf143712f11867, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b4d2d8c8516dc5482abf143712f11867,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_b4d2d8c8516dc5482abf143712f11867 == cache_frame_b4d2d8c8516dc5482abf143712f11867 )
    {
        Py_DECREF( frame_b4d2d8c8516dc5482abf143712f11867 );
    }
    cache_frame_b4d2d8c8516dc5482abf143712f11867 = NULL;

    assertFrameObject( frame_b4d2d8c8516dc5482abf143712f11867 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_4__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_4__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_5__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_fde0797c15931de70c425eaf5d1dc63e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_fde0797c15931de70c425eaf5d1dc63e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_fde0797c15931de70c425eaf5d1dc63e, codeobj_fde0797c15931de70c425eaf5d1dc63e, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *) );
    frame_fde0797c15931de70c425eaf5d1dc63e = cache_frame_fde0797c15931de70c425eaf5d1dc63e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_fde0797c15931de70c425eaf5d1dc63e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_fde0797c15931de70c425eaf5d1dc63e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_auto_up );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 92;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_fde0797c15931de70c425eaf5d1dc63e->m_frame.f_lineno = 92;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fde0797c15931de70c425eaf5d1dc63e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fde0797c15931de70c425eaf5d1dc63e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_fde0797c15931de70c425eaf5d1dc63e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_fde0797c15931de70c425eaf5d1dc63e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_fde0797c15931de70c425eaf5d1dc63e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_fde0797c15931de70c425eaf5d1dc63e,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_fde0797c15931de70c425eaf5d1dc63e == cache_frame_fde0797c15931de70c425eaf5d1dc63e )
    {
        Py_DECREF( frame_fde0797c15931de70c425eaf5d1dc63e );
    }
    cache_frame_fde0797c15931de70c425eaf5d1dc63e = NULL;

    assertFrameObject( frame_fde0797c15931de70c425eaf5d1dc63e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_5__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_5__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_c = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var__ = NULL;
    struct Nuitka_FrameObject *frame_3a44b69542262cbf32c6df5db6fce978;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_3a44b69542262cbf32c6df5db6fce978 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3a44b69542262cbf32c6df5db6fce978, codeobj_3a44b69542262cbf32c6df5db6fce978, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3a44b69542262cbf32c6df5db6fce978 = cache_frame_3a44b69542262cbf32c6df5db6fce978;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3a44b69542262cbf32c6df5db6fce978 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3a44b69542262cbf32c6df5db6fce978 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "handle" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( PyCell_GET( par_c ) );
        tmp_tuple_element_1 = PyCell_GET( par_c );
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_filter;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_arg );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_arg );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_arg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_1 = tmp_mvar_value_1;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_3a44b69542262cbf32c6df5db6fce978->m_frame.f_lineno = 99;
        tmp_called_name_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "handle" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = PyCell_GET( self->m_closure[0] );
        tmp_args_element_name_2 = const_str_plain_escape;
        CHECK_OBJECT( PyCell_GET( par_c ) );
        tmp_args_element_name_3 = PyCell_GET( par_c );
        frame_3a44b69542262cbf32c6df5db6fce978->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_called_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 100;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_4 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit$$$function_1__(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_4)->m_closure[0] = par_c;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_4)->m_closure[0] );


        frame_3a44b69542262cbf32c6df5db6fce978->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 100;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }
        frame_3a44b69542262cbf32c6df5db6fce978->m_frame.f_lineno = 99;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }
        assert( var__ == NULL );
        var__ = tmp_assign_source_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3a44b69542262cbf32c6df5db6fce978 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3a44b69542262cbf32c6df5db6fce978 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3a44b69542262cbf32c6df5db6fce978, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3a44b69542262cbf32c6df5db6fce978->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3a44b69542262cbf32c6df5db6fce978, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3a44b69542262cbf32c6df5db6fce978,
        type_description_1,
        par_c,
        var__,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_3a44b69542262cbf32c6df5db6fce978 == cache_frame_3a44b69542262cbf32c6df5db6fce978 )
    {
        Py_DECREF( frame_3a44b69542262cbf32c6df5db6fce978 );
    }
    cache_frame_3a44b69542262cbf32c6df5db6fce978 = NULL;

    assertFrameObject( frame_3a44b69542262cbf32c6df5db6fce978 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_c );
    Py_DECREF( par_c );
    par_c = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_c );
    Py_DECREF( par_c );
    par_c = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit$$$function_1__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_dc77a1cef49efbb1da8d023e89ab872d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_dc77a1cef49efbb1da8d023e89ab872d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dc77a1cef49efbb1da8d023e89ab872d, codeobj_dc77a1cef49efbb1da8d023e89ab872d, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_dc77a1cef49efbb1da8d023e89ab872d = cache_frame_dc77a1cef49efbb1da8d023e89ab872d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dc77a1cef49efbb1da8d023e89ab872d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dc77a1cef49efbb1da8d023e89ab872d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append_to_arg_count );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "c" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[0] );
        frame_dc77a1cef49efbb1da8d023e89ab872d->m_frame.f_lineno = 102;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc77a1cef49efbb1da8d023e89ab872d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc77a1cef49efbb1da8d023e89ab872d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dc77a1cef49efbb1da8d023e89ab872d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dc77a1cef49efbb1da8d023e89ab872d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dc77a1cef49efbb1da8d023e89ab872d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dc77a1cef49efbb1da8d023e89ab872d,
        type_description_1,
        par_event,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_dc77a1cef49efbb1da8d023e89ab872d == cache_frame_dc77a1cef49efbb1da8d023e89ab872d )
    {
        Py_DECREF( frame_dc77a1cef49efbb1da8d023e89ab872d );
    }
    cache_frame_dc77a1cef49efbb1da8d023e89ab872d = NULL;

    assertFrameObject( frame_dc77a1cef49efbb1da8d023e89ab872d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit$$$function_1__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit$$$function_1__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_7__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_339b0bf3b0b5dafd995be974b89551c1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_339b0bf3b0b5dafd995be974b89551c1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_339b0bf3b0b5dafd995be974b89551c1, codeobj_339b0bf3b0b5dafd995be974b89551c1, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *) );
    frame_339b0bf3b0b5dafd995be974b89551c1 = cache_frame_339b0bf3b0b5dafd995be974b89551c1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_339b0bf3b0b5dafd995be974b89551c1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_339b0bf3b0b5dafd995be974b89551c1 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__arg );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_event );
            tmp_called_instance_1 = par_event;
            frame_339b0bf3b0b5dafd995be974b89551c1->m_frame.f_lineno = 112;
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append_to_arg_count, &PyTuple_GET_ITEM( const_tuple_str_chr_45_tuple, 0 ) );

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_339b0bf3b0b5dafd995be974b89551c1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_339b0bf3b0b5dafd995be974b89551c1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_339b0bf3b0b5dafd995be974b89551c1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_339b0bf3b0b5dafd995be974b89551c1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_339b0bf3b0b5dafd995be974b89551c1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_339b0bf3b0b5dafd995be974b89551c1,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_339b0bf3b0b5dafd995be974b89551c1 == cache_frame_339b0bf3b0b5dafd995be974b89551c1 )
    {
        Py_DECREF( frame_339b0bf3b0b5dafd995be974b89551c1 );
    }
    cache_frame_339b0bf3b0b5dafd995be974b89551c1 = NULL;

    assertFrameObject( frame_339b0bf3b0b5dafd995be974b89551c1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_7__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_7__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_9_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_d408982fffef1590d6d9cd23cab783d4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d408982fffef1590d6d9cd23cab783d4 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_d408982fffef1590d6d9cd23cab783d4, codeobj_d408982fffef1590d6d9cd23cab783d4, module_prompt_toolkit$key_binding$bindings$emacs, 0 );
    frame_d408982fffef1590d6d9cd23cab783d4 = cache_frame_d408982fffef1590d6d9cd23cab783d4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d408982fffef1590d6d9cd23cab783d4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d408982fffef1590d6d9cd23cab783d4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_d408982fffef1590d6d9cd23cab783d4->m_frame.f_lineno = 114;
        tmp_source_name_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;

            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_key_processor );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_arg );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_chr_45;
        tmp_return_value = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;

            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d408982fffef1590d6d9cd23cab783d4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d408982fffef1590d6d9cd23cab783d4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d408982fffef1590d6d9cd23cab783d4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d408982fffef1590d6d9cd23cab783d4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d408982fffef1590d6d9cd23cab783d4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d408982fffef1590d6d9cd23cab783d4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d408982fffef1590d6d9cd23cab783d4,
        type_description_1
    );


    // Release cached frame.
    if ( frame_d408982fffef1590d6d9cd23cab783d4 == cache_frame_d408982fffef1590d6d9cd23cab783d4 )
    {
        Py_DECREF( frame_d408982fffef1590d6d9cd23cab783d4 );
    }
    cache_frame_d408982fffef1590d6d9cd23cab783d4 = NULL;

    assertFrameObject( frame_d408982fffef1590d6d9cd23cab783d4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_9_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_8__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_e5eda8746a8b596dc9f4d379d11774c7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_e5eda8746a8b596dc9f4d379d11774c7 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e5eda8746a8b596dc9f4d379d11774c7, codeobj_e5eda8746a8b596dc9f4d379d11774c7, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *) );
    frame_e5eda8746a8b596dc9f4d379d11774c7 = cache_frame_e5eda8746a8b596dc9f4d379d11774c7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e5eda8746a8b596dc9f4d379d11774c7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e5eda8746a8b596dc9f4d379d11774c7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        tmp_assattr_name_1 = const_str_chr_45;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_app );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_key_processor );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_arg, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_target_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5eda8746a8b596dc9f4d379d11774c7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5eda8746a8b596dc9f4d379d11774c7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e5eda8746a8b596dc9f4d379d11774c7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e5eda8746a8b596dc9f4d379d11774c7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e5eda8746a8b596dc9f4d379d11774c7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e5eda8746a8b596dc9f4d379d11774c7,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_e5eda8746a8b596dc9f4d379d11774c7 == cache_frame_e5eda8746a8b596dc9f4d379d11774c7 )
    {
        Py_DECREF( frame_e5eda8746a8b596dc9f4d379d11774c7 );
    }
    cache_frame_e5eda8746a8b596dc9f4d379d11774c7 = NULL;

    assertFrameObject( frame_e5eda8746a8b596dc9f4d379d11774c7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_8__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_8__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_10_is_returnable( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_9aef691f2da727794e9cdacaa2210a0d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9aef691f2da727794e9cdacaa2210a0d = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_9aef691f2da727794e9cdacaa2210a0d, codeobj_9aef691f2da727794e9cdacaa2210a0d, module_prompt_toolkit$key_binding$bindings$emacs, 0 );
    frame_9aef691f2da727794e9cdacaa2210a0d = cache_frame_9aef691f2da727794e9cdacaa2210a0d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9aef691f2da727794e9cdacaa2210a0d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9aef691f2da727794e9cdacaa2210a0d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_9aef691f2da727794e9cdacaa2210a0d->m_frame.f_lineno = 124;
        tmp_source_name_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_returnable );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9aef691f2da727794e9cdacaa2210a0d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9aef691f2da727794e9cdacaa2210a0d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9aef691f2da727794e9cdacaa2210a0d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9aef691f2da727794e9cdacaa2210a0d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9aef691f2da727794e9cdacaa2210a0d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9aef691f2da727794e9cdacaa2210a0d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9aef691f2da727794e9cdacaa2210a0d,
        type_description_1
    );


    // Release cached frame.
    if ( frame_9aef691f2da727794e9cdacaa2210a0d == cache_frame_9aef691f2da727794e9cdacaa2210a0d )
    {
        Py_DECREF( frame_9aef691f2da727794e9cdacaa2210a0d );
    }
    cache_frame_9aef691f2da727794e9cdacaa2210a0d = NULL;

    assertFrameObject( frame_9aef691f2da727794e9cdacaa2210a0d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_10_is_returnable );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_11_character_search( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_buff = python_pars[ 0 ];
    PyObject *par_char = python_pars[ 1 ];
    PyObject *par_count = python_pars[ 2 ];
    PyObject *var_match = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_ad505444d20353014afe05006e6e8d3a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_ad505444d20353014afe05006e6e8d3a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ad505444d20353014afe05006e6e8d3a, codeobj_ad505444d20353014afe05006e6e8d3a, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ad505444d20353014afe05006e6e8d3a = cache_frame_ad505444d20353014afe05006e6e8d3a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ad505444d20353014afe05006e6e8d3a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ad505444d20353014afe05006e6e8d3a ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_count );
        tmp_compexpr_left_1 = par_count;
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_operand_name_1;
            CHECK_OBJECT( par_buff );
            tmp_source_name_2 = par_buff;
            tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_document );
            if ( tmp_source_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_find_backwards );
            Py_DECREF( tmp_source_name_1 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_char );
            tmp_tuple_element_1 = par_char;
            tmp_args_name_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_dict_key_1 = const_str_plain_in_current_line;
            tmp_dict_value_1 = Py_True;
            tmp_kw_name_1 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_2 = const_str_plain_count;
            CHECK_OBJECT( par_count );
            tmp_operand_name_1 = par_count;
            tmp_dict_value_2 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
            if ( tmp_dict_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );

                exception_lineno = 136;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            frame_ad505444d20353014afe05006e6e8d3a->m_frame.f_lineno = 136;
            tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_match == NULL );
            var_match = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            PyObject *tmp_dict_key_4;
            PyObject *tmp_dict_value_4;
            CHECK_OBJECT( par_buff );
            tmp_source_name_4 = par_buff;
            tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
            if ( tmp_source_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_find );
            Py_DECREF( tmp_source_name_3 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_char );
            tmp_tuple_element_2 = par_char;
            tmp_args_name_2 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
            tmp_dict_key_3 = const_str_plain_in_current_line;
            tmp_dict_value_3 = Py_True;
            tmp_kw_name_2 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_4 = const_str_plain_count;
            CHECK_OBJECT( par_count );
            tmp_dict_value_4 = par_count;
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
            assert( !(tmp_res != 0) );
            frame_ad505444d20353014afe05006e6e8d3a->m_frame.f_lineno = 138;
            tmp_assign_source_2 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_match == NULL );
            var_match = tmp_assign_source_2;
        }
        branch_end_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_match );
        tmp_compexpr_left_2 = var_match;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( par_buff );
            tmp_source_name_5 = par_buff;
            tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_cursor_position );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_inplace_assign_attr_1__start == NULL );
            tmp_inplace_assign_attr_1__start = tmp_assign_source_3;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
            tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
            CHECK_OBJECT( var_match );
            tmp_right_name_1 = var_match;
            tmp_assign_source_4 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            assert( tmp_inplace_assign_attr_1__end == NULL );
            tmp_inplace_assign_attr_1__end = tmp_assign_source_4;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
            tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
            CHECK_OBJECT( par_buff );
            tmp_assattr_target_1 = par_buff;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        branch_no_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ad505444d20353014afe05006e6e8d3a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ad505444d20353014afe05006e6e8d3a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ad505444d20353014afe05006e6e8d3a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ad505444d20353014afe05006e6e8d3a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ad505444d20353014afe05006e6e8d3a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ad505444d20353014afe05006e6e8d3a,
        type_description_1,
        par_buff,
        par_char,
        par_count,
        var_match
    );


    // Release cached frame.
    if ( frame_ad505444d20353014afe05006e6e8d3a == cache_frame_ad505444d20353014afe05006e6e8d3a )
    {
        Py_DECREF( frame_ad505444d20353014afe05006e6e8d3a );
    }
    cache_frame_ad505444d20353014afe05006e6e8d3a = NULL;

    assertFrameObject( frame_ad505444d20353014afe05006e6e8d3a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_11_character_search );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_buff );
    Py_DECREF( par_buff );
    par_buff = NULL;

    CHECK_OBJECT( (PyObject *)par_char );
    Py_DECREF( par_char );
    par_char = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    CHECK_OBJECT( (PyObject *)var_match );
    Py_DECREF( var_match );
    var_match = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_buff );
    Py_DECREF( par_buff );
    par_buff = NULL;

    CHECK_OBJECT( (PyObject *)par_char );
    Py_DECREF( par_char );
    par_char = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    Py_XDECREF( var_match );
    var_match = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_11_character_search );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_12__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_22049ef5e24d71ef8e11931cfb18870a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_22049ef5e24d71ef8e11931cfb18870a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_22049ef5e24d71ef8e11931cfb18870a, codeobj_22049ef5e24d71ef8e11931cfb18870a, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_22049ef5e24d71ef8e11931cfb18870a = cache_frame_22049ef5e24d71ef8e11931cfb18870a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_22049ef5e24d71ef8e11931cfb18870a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_22049ef5e24d71ef8e11931cfb18870a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_3;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "character_search" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_data );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 147;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 147;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        frame_22049ef5e24d71ef8e11931cfb18870a->m_frame.f_lineno = 147;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22049ef5e24d71ef8e11931cfb18870a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22049ef5e24d71ef8e11931cfb18870a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_22049ef5e24d71ef8e11931cfb18870a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_22049ef5e24d71ef8e11931cfb18870a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_22049ef5e24d71ef8e11931cfb18870a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_22049ef5e24d71ef8e11931cfb18870a,
        type_description_1,
        par_event,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_22049ef5e24d71ef8e11931cfb18870a == cache_frame_22049ef5e24d71ef8e11931cfb18870a )
    {
        Py_DECREF( frame_22049ef5e24d71ef8e11931cfb18870a );
    }
    cache_frame_22049ef5e24d71ef8e11931cfb18870a = NULL;

    assertFrameObject( frame_22049ef5e24d71ef8e11931cfb18870a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_12__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_12__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_13__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_cb46efa5acfd6e83bed442fb852708bd;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_cb46efa5acfd6e83bed442fb852708bd = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cb46efa5acfd6e83bed442fb852708bd, codeobj_cb46efa5acfd6e83bed442fb852708bd, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_cb46efa5acfd6e83bed442fb852708bd = cache_frame_cb46efa5acfd6e83bed442fb852708bd;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cb46efa5acfd6e83bed442fb852708bd );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cb46efa5acfd6e83bed442fb852708bd ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_3;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "character_search" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 153;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_data );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 153;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 153;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 153;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        frame_cb46efa5acfd6e83bed442fb852708bd->m_frame.f_lineno = 153;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb46efa5acfd6e83bed442fb852708bd );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb46efa5acfd6e83bed442fb852708bd );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cb46efa5acfd6e83bed442fb852708bd, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cb46efa5acfd6e83bed442fb852708bd->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cb46efa5acfd6e83bed442fb852708bd, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cb46efa5acfd6e83bed442fb852708bd,
        type_description_1,
        par_event,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_cb46efa5acfd6e83bed442fb852708bd == cache_frame_cb46efa5acfd6e83bed442fb852708bd )
    {
        Py_DECREF( frame_cb46efa5acfd6e83bed442fb852708bd );
    }
    cache_frame_cb46efa5acfd6e83bed442fb852708bd = NULL;

    assertFrameObject( frame_cb46efa5acfd6e83bed442fb852708bd );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_13__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_13__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_14__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_14__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_14__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_15__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_15__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_15__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_16__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_16__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_16__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_complete_event = NULL;
    PyObject *var_completions = NULL;
    PyObject *var_text_to_insert = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_15b84f706ba18ad1c9fcbf547a8ed6b0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_15b84f706ba18ad1c9fcbf547a8ed6b0 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_15b84f706ba18ad1c9fcbf547a8ed6b0, codeobj_15b84f706ba18ad1c9fcbf547a8ed6b0, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_15b84f706ba18ad1c9fcbf547a8ed6b0 = cache_frame_15b84f706ba18ad1c9fcbf547a8ed6b0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_15b84f706ba18ad1c9fcbf547a8ed6b0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_15b84f706ba18ad1c9fcbf547a8ed6b0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_CompleteEvent );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompleteEvent );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompleteEvent" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 180;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_kw_name_1 = PyDict_Copy( const_dict_cdf87466f932e58c7352e4a0bd2d6033 );
        frame_15b84f706ba18ad1c9fcbf547a8ed6b0->m_frame.f_lineno = 180;
        tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_complete_event == NULL );
        var_complete_event = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_completer );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_get_completions );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_buff );
        tmp_source_name_4 = var_buff;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 181;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_complete_event );
        tmp_args_element_name_2 = var_complete_event;
        frame_15b84f706ba18ad1c9fcbf547a8ed6b0->m_frame.f_lineno = 181;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_list_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_completions == NULL );
        var_completions = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_3;
        tmp_source_name_5 = const_str_space;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_join );
        assert( !(tmp_called_name_3 == NULL) );
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( var_completions );
            tmp_iter_arg_1 = var_completions;
            tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 184;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_5;
        }
        // Tried code:
        tmp_args_element_name_3 = prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_3)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__ );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__ );
        return NULL;
        outline_result_1:;
        frame_15b84f706ba18ad1c9fcbf547a8ed6b0->m_frame.f_lineno = 184;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_text_to_insert == NULL );
        var_text_to_insert = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( var_buff );
        tmp_called_instance_1 = var_buff;
        CHECK_OBJECT( var_text_to_insert );
        tmp_args_element_name_4 = var_text_to_insert;
        frame_15b84f706ba18ad1c9fcbf547a8ed6b0->m_frame.f_lineno = 185;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_insert_text, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_15b84f706ba18ad1c9fcbf547a8ed6b0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_15b84f706ba18ad1c9fcbf547a8ed6b0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_15b84f706ba18ad1c9fcbf547a8ed6b0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_15b84f706ba18ad1c9fcbf547a8ed6b0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_15b84f706ba18ad1c9fcbf547a8ed6b0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_15b84f706ba18ad1c9fcbf547a8ed6b0,
        type_description_1,
        par_event,
        var_buff,
        var_complete_event,
        var_completions,
        var_text_to_insert
    );


    // Release cached frame.
    if ( frame_15b84f706ba18ad1c9fcbf547a8ed6b0 == cache_frame_15b84f706ba18ad1c9fcbf547a8ed6b0 )
    {
        Py_DECREF( frame_15b84f706ba18ad1c9fcbf547a8ed6b0 );
    }
    cache_frame_15b84f706ba18ad1c9fcbf547a8ed6b0 = NULL;

    assertFrameObject( frame_15b84f706ba18ad1c9fcbf547a8ed6b0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_complete_event );
    Py_DECREF( var_complete_event );
    var_complete_event = NULL;

    CHECK_OBJECT( (PyObject *)var_completions );
    Py_DECREF( var_completions );
    var_completions = NULL;

    CHECK_OBJECT( (PyObject *)var_text_to_insert );
    Py_DECREF( var_text_to_insert );
    var_text_to_insert = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_complete_event );
    var_complete_event = NULL;

    Py_XDECREF( var_completions );
    var_completions = NULL;

    Py_XDECREF( var_text_to_insert );
    var_text_to_insert = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__$$$genexpr_1_genexpr_locals {
    PyObject *var_c;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__$$$genexpr_1_genexpr_locals *generator_heap = (struct prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_c = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_5117690e35214a1ef887b09ec807a92c, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 184;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_c;
            generator_heap->var_c = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_c );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_source_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_c );
        tmp_source_name_1 = generator_heap->var_c;
        tmp_expression_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_text );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 184;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_source_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_source_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 184;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 184;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_c
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__$$$genexpr_1_genexpr_context,
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_f05828e2431524886539ce0953f53624,
#endif
        codeobj_5117690e35214a1ef887b09ec807a92c,
        1,
        sizeof(struct prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_18__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buffer = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    PyObject *tmp_inplace_assign_attr_2__end = NULL;
    PyObject *tmp_inplace_assign_attr_2__start = NULL;
    struct Nuitka_FrameObject *frame_b616f61388861cc23d80906312549a0c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_b616f61388861cc23d80906312549a0c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b616f61388861cc23d80906312549a0c, codeobj_b616f61388861cc23d80906312549a0c, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_b616f61388861cc23d80906312549a0c = cache_frame_b616f61388861cc23d80906312549a0c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b616f61388861cc23d80906312549a0c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b616f61388861cc23d80906312549a0c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buffer == NULL );
        var_buffer = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_3 = var_buffer;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 195;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_is_cursor_at_the_end_of_line );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 195;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 195;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( var_buffer );
            tmp_source_name_4 = var_buffer;
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_cursor_position );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( tmp_inplace_assign_attr_1__start == NULL );
            tmp_inplace_assign_attr_1__start = tmp_assign_source_2;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_5;
            PyObject *tmp_source_name_6;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
            tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
            CHECK_OBJECT( var_buffer );
            tmp_source_name_6 = var_buffer;
            tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_document );
            if ( tmp_source_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_get_start_of_line_position );
            Py_DECREF( tmp_source_name_5 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            tmp_kw_name_1 = PyDict_Copy( const_dict_0bf8402271879b64e927f26f79aacd8b );
            frame_b616f61388861cc23d80906312549a0c->m_frame.f_lineno = 196;
            tmp_right_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            tmp_assign_source_3 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            assert( tmp_inplace_assign_attr_1__end == NULL );
            tmp_inplace_assign_attr_1__end = tmp_assign_source_3;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
            tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
            CHECK_OBJECT( var_buffer );
            tmp_assattr_target_1 = var_buffer;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "oo";
                goto try_except_handler_3;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( var_buffer );
            tmp_source_name_7 = var_buffer;
            tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_cursor_position );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 198;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( tmp_inplace_assign_attr_2__start == NULL );
            tmp_inplace_assign_attr_2__start = tmp_assign_source_4;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( tmp_inplace_assign_attr_2__start );
            tmp_left_name_2 = tmp_inplace_assign_attr_2__start;
            CHECK_OBJECT( var_buffer );
            tmp_source_name_8 = var_buffer;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_document );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 198;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            frame_b616f61388861cc23d80906312549a0c->m_frame.f_lineno = 198;
            tmp_right_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_end_of_line_position );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 198;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            tmp_assign_source_5 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 198;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            assert( tmp_inplace_assign_attr_2__end == NULL );
            tmp_inplace_assign_attr_2__end = tmp_assign_source_5;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_assattr_target_2;
            CHECK_OBJECT( tmp_inplace_assign_attr_2__end );
            tmp_assattr_name_2 = tmp_inplace_assign_attr_2__end;
            CHECK_OBJECT( var_buffer );
            tmp_assattr_target_2 = var_buffer;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_cursor_position, tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 198;
                type_description_1 = "oo";
                goto try_except_handler_5;
            }
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__end );
        Py_DECREF( tmp_inplace_assign_attr_2__end );
        tmp_inplace_assign_attr_2__end = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        try_end_3:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__start );
        Py_DECREF( tmp_inplace_assign_attr_2__start );
        tmp_inplace_assign_attr_2__start = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__end );
        Py_DECREF( tmp_inplace_assign_attr_2__end );
        tmp_inplace_assign_attr_2__end = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__start );
        Py_DECREF( tmp_inplace_assign_attr_2__start );
        tmp_inplace_assign_attr_2__start = NULL;

        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b616f61388861cc23d80906312549a0c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b616f61388861cc23d80906312549a0c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b616f61388861cc23d80906312549a0c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b616f61388861cc23d80906312549a0c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b616f61388861cc23d80906312549a0c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b616f61388861cc23d80906312549a0c,
        type_description_1,
        par_event,
        var_buffer
    );


    // Release cached frame.
    if ( frame_b616f61388861cc23d80906312549a0c == cache_frame_b616f61388861cc23d80906312549a0c )
    {
        Py_DECREF( frame_b616f61388861cc23d80906312549a0c );
    }
    cache_frame_b616f61388861cc23d80906312549a0c = NULL;

    assertFrameObject( frame_b616f61388861cc23d80906312549a0c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_18__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buffer );
    Py_DECREF( var_buffer );
    var_buffer = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buffer );
    var_buffer = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_18__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_19__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    struct Nuitka_FrameObject *frame_655ef0697fd9f535491eaf3ff1799405;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_655ef0697fd9f535491eaf3ff1799405 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_655ef0697fd9f535491eaf3ff1799405, codeobj_655ef0697fd9f535491eaf3ff1799405, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_655ef0697fd9f535491eaf3ff1799405 = cache_frame_655ef0697fd9f535491eaf3ff1799405;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_655ef0697fd9f535491eaf3ff1799405 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_655ef0697fd9f535491eaf3ff1799405 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 206;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_2 = var_buff;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_text );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 207;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_1;
            CHECK_OBJECT( var_buff );
            tmp_source_name_3 = var_buff;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_start_selection );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 208;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_dict_key_1 = const_str_plain_selection_type;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_SelectionType );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SelectionType );
            }

            if ( tmp_mvar_value_1 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SelectionType" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 208;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_4 = tmp_mvar_value_1;
            tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_CHARACTERS );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 208;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_655ef0697fd9f535491eaf3ff1799405->m_frame.f_lineno = 208;
            tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 208;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_655ef0697fd9f535491eaf3ff1799405 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_655ef0697fd9f535491eaf3ff1799405 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_655ef0697fd9f535491eaf3ff1799405, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_655ef0697fd9f535491eaf3ff1799405->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_655ef0697fd9f535491eaf3ff1799405, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_655ef0697fd9f535491eaf3ff1799405,
        type_description_1,
        par_event,
        var_buff
    );


    // Release cached frame.
    if ( frame_655ef0697fd9f535491eaf3ff1799405 == cache_frame_655ef0697fd9f535491eaf3ff1799405 )
    {
        Py_DECREF( frame_655ef0697fd9f535491eaf3ff1799405 );
    }
    cache_frame_655ef0697fd9f535491eaf3ff1799405 = NULL;

    assertFrameObject( frame_655ef0697fd9f535491eaf3ff1799405 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_19__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_19__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_20__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_cf42bea14394d91a76668f60f023eacc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_cf42bea14394d91a76668f60f023eacc = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cf42bea14394d91a76668f60f023eacc, codeobj_cf42bea14394d91a76668f60f023eacc, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *) );
    frame_cf42bea14394d91a76668f60f023eacc = cache_frame_cf42bea14394d91a76668f60f023eacc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cf42bea14394d91a76668f60f023eacc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cf42bea14394d91a76668f60f023eacc ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_source_name_1;
        tmp_assattr_name_1 = Py_None;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 215;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_complete_state, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_target_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 215;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_source_name_2;
        tmp_assattr_name_2 = Py_None;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_assattr_target_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_assattr_target_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_validation_error, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_target_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cf42bea14394d91a76668f60f023eacc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cf42bea14394d91a76668f60f023eacc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cf42bea14394d91a76668f60f023eacc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cf42bea14394d91a76668f60f023eacc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cf42bea14394d91a76668f60f023eacc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cf42bea14394d91a76668f60f023eacc,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_cf42bea14394d91a76668f60f023eacc == cache_frame_cf42bea14394d91a76668f60f023eacc )
    {
        Py_DECREF( frame_cf42bea14394d91a76668f60f023eacc );
    }
    cache_frame_cf42bea14394d91a76668f60f023eacc = NULL;

    assertFrameObject( frame_cf42bea14394d91a76668f60f023eacc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_20__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_20__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_21__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a20344d433cd6984d126314d57bceb35;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a20344d433cd6984d126314d57bceb35 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a20344d433cd6984d126314d57bceb35, codeobj_a20344d433cd6984d126314d57bceb35, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *) );
    frame_a20344d433cd6984d126314d57bceb35 = cache_frame_a20344d433cd6984d126314d57bceb35;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a20344d433cd6984d126314d57bceb35 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a20344d433cd6984d126314d57bceb35 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_a20344d433cd6984d126314d57bceb35->m_frame.f_lineno = 223;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_exit_selection );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a20344d433cd6984d126314d57bceb35 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a20344d433cd6984d126314d57bceb35 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a20344d433cd6984d126314d57bceb35, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a20344d433cd6984d126314d57bceb35->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a20344d433cd6984d126314d57bceb35, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a20344d433cd6984d126314d57bceb35,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_a20344d433cd6984d126314d57bceb35 == cache_frame_a20344d433cd6984d126314d57bceb35 )
    {
        Py_DECREF( frame_a20344d433cd6984d126314d57bceb35 );
    }
    cache_frame_a20344d433cd6984d126314d57bceb35 = NULL;

    assertFrameObject( frame_a20344d433cd6984d126314d57bceb35 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_21__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_21__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_22__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_data = NULL;
    struct Nuitka_FrameObject *frame_70b09fedff819bd1cc7106a3c27b3945;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_70b09fedff819bd1cc7106a3c27b3945 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_70b09fedff819bd1cc7106a3c27b3945, codeobj_70b09fedff819bd1cc7106a3c27b3945, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_70b09fedff819bd1cc7106a3c27b3945 = cache_frame_70b09fedff819bd1cc7106a3c27b3945;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_70b09fedff819bd1cc7106a3c27b3945 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_70b09fedff819bd1cc7106a3c27b3945 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_70b09fedff819bd1cc7106a3c27b3945->m_frame.f_lineno = 231;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_cut_selection );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_data == NULL );
        var_data = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_clipboard );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_data );
        tmp_args_element_name_1 = var_data;
        frame_70b09fedff819bd1cc7106a3c27b3945->m_frame.f_lineno = 232;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_data, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_70b09fedff819bd1cc7106a3c27b3945 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_70b09fedff819bd1cc7106a3c27b3945 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_70b09fedff819bd1cc7106a3c27b3945, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_70b09fedff819bd1cc7106a3c27b3945->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_70b09fedff819bd1cc7106a3c27b3945, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_70b09fedff819bd1cc7106a3c27b3945,
        type_description_1,
        par_event,
        var_data
    );


    // Release cached frame.
    if ( frame_70b09fedff819bd1cc7106a3c27b3945 == cache_frame_70b09fedff819bd1cc7106a3c27b3945 )
    {
        Py_DECREF( frame_70b09fedff819bd1cc7106a3c27b3945 );
    }
    cache_frame_70b09fedff819bd1cc7106a3c27b3945 = NULL;

    assertFrameObject( frame_70b09fedff819bd1cc7106a3c27b3945 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_22__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_data );
    Py_DECREF( var_data );
    var_data = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_22__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_23__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_data = NULL;
    struct Nuitka_FrameObject *frame_3499e630a1c715a6782317aaad22ae6e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3499e630a1c715a6782317aaad22ae6e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3499e630a1c715a6782317aaad22ae6e, codeobj_3499e630a1c715a6782317aaad22ae6e, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_3499e630a1c715a6782317aaad22ae6e = cache_frame_3499e630a1c715a6782317aaad22ae6e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3499e630a1c715a6782317aaad22ae6e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3499e630a1c715a6782317aaad22ae6e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_3499e630a1c715a6782317aaad22ae6e->m_frame.f_lineno = 239;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_copy_selection );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_data == NULL );
        var_data = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_clipboard );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_data );
        tmp_args_element_name_1 = var_data;
        frame_3499e630a1c715a6782317aaad22ae6e->m_frame.f_lineno = 240;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_data, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3499e630a1c715a6782317aaad22ae6e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3499e630a1c715a6782317aaad22ae6e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3499e630a1c715a6782317aaad22ae6e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3499e630a1c715a6782317aaad22ae6e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3499e630a1c715a6782317aaad22ae6e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3499e630a1c715a6782317aaad22ae6e,
        type_description_1,
        par_event,
        var_data
    );


    // Release cached frame.
    if ( frame_3499e630a1c715a6782317aaad22ae6e == cache_frame_3499e630a1c715a6782317aaad22ae6e )
    {
        Py_DECREF( frame_3499e630a1c715a6782317aaad22ae6e );
    }
    cache_frame_3499e630a1c715a6782317aaad22ae6e = NULL;

    assertFrameObject( frame_3499e630a1c715a6782317aaad22ae6e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_23__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_data );
    Py_DECREF( var_data );
    var_data = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_23__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_24__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buffer = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_f34edee97ddfaa882297fe4bceb24d98;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_f34edee97ddfaa882297fe4bceb24d98 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f34edee97ddfaa882297fe4bceb24d98, codeobj_f34edee97ddfaa882297fe4bceb24d98, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_f34edee97ddfaa882297fe4bceb24d98 = cache_frame_f34edee97ddfaa882297fe4bceb24d98;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f34edee97ddfaa882297fe4bceb24d98 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f34edee97ddfaa882297fe4bceb24d98 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buffer == NULL );
        var_buffer = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_2 = var_buffer;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cursor_position );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( tmp_inplace_assign_attr_1__start == NULL );
        tmp_inplace_assign_attr_1__start = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
        tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_4 = var_buffer;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_find_previous_word_beginning );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 248;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_f34edee97ddfaa882297fe4bceb24d98->m_frame.f_lineno = 248;
        tmp_or_left_value_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_or_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_or_left_value_1 );

            exception_lineno = 248;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        Py_DECREF( tmp_or_left_value_1 );
        tmp_or_right_value_1 = const_int_0;
        Py_INCREF( tmp_or_right_value_1 );
        tmp_right_name_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_right_name_1 = tmp_or_left_value_1;
        or_end_1:;
        tmp_assign_source_3 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_inplace_assign_attr_1__end == NULL );
        tmp_inplace_assign_attr_1__end = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
        tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
        CHECK_OBJECT( var_buffer );
        tmp_assattr_target_1 = var_buffer;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f34edee97ddfaa882297fe4bceb24d98 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f34edee97ddfaa882297fe4bceb24d98 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f34edee97ddfaa882297fe4bceb24d98, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f34edee97ddfaa882297fe4bceb24d98->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f34edee97ddfaa882297fe4bceb24d98, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f34edee97ddfaa882297fe4bceb24d98,
        type_description_1,
        par_event,
        var_buffer
    );


    // Release cached frame.
    if ( frame_f34edee97ddfaa882297fe4bceb24d98 == cache_frame_f34edee97ddfaa882297fe4bceb24d98 )
    {
        Py_DECREF( frame_f34edee97ddfaa882297fe4bceb24d98 );
    }
    cache_frame_f34edee97ddfaa882297fe4bceb24d98 = NULL;

    assertFrameObject( frame_f34edee97ddfaa882297fe4bceb24d98 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_24__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buffer );
    Py_DECREF( var_buffer );
    var_buffer = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buffer );
    var_buffer = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_24__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_25__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buffer = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_edf6eaa6807f966e1e63a2560ffd3d0e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_edf6eaa6807f966e1e63a2560ffd3d0e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_edf6eaa6807f966e1e63a2560ffd3d0e, codeobj_edf6eaa6807f966e1e63a2560ffd3d0e, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_edf6eaa6807f966e1e63a2560ffd3d0e = cache_frame_edf6eaa6807f966e1e63a2560ffd3d0e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_edf6eaa6807f966e1e63a2560ffd3d0e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_edf6eaa6807f966e1e63a2560ffd3d0e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 255;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buffer == NULL );
        var_buffer = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_2 = var_buffer;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cursor_position );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( tmp_inplace_assign_attr_1__start == NULL );
        tmp_inplace_assign_attr_1__start = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
        tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_4 = var_buffer;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_find_next_word_beginning );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 256;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_edf6eaa6807f966e1e63a2560ffd3d0e->m_frame.f_lineno = 256;
        tmp_or_left_value_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_or_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_or_left_value_1 );

            exception_lineno = 257;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        Py_DECREF( tmp_or_left_value_1 );
        CHECK_OBJECT( var_buffer );
        tmp_source_name_6 = var_buffer;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_document );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        frame_edf6eaa6807f966e1e63a2560ffd3d0e->m_frame.f_lineno = 257;
        tmp_or_right_value_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_end_of_document_position );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_or_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_right_name_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_right_name_1 = tmp_or_left_value_1;
        or_end_1:;
        tmp_assign_source_3 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_inplace_assign_attr_1__end == NULL );
        tmp_inplace_assign_attr_1__end = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
        tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
        CHECK_OBJECT( var_buffer );
        tmp_assattr_target_1 = var_buffer;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_edf6eaa6807f966e1e63a2560ffd3d0e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_edf6eaa6807f966e1e63a2560ffd3d0e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_edf6eaa6807f966e1e63a2560ffd3d0e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_edf6eaa6807f966e1e63a2560ffd3d0e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_edf6eaa6807f966e1e63a2560ffd3d0e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_edf6eaa6807f966e1e63a2560ffd3d0e,
        type_description_1,
        par_event,
        var_buffer
    );


    // Release cached frame.
    if ( frame_edf6eaa6807f966e1e63a2560ffd3d0e == cache_frame_edf6eaa6807f966e1e63a2560ffd3d0e )
    {
        Py_DECREF( frame_edf6eaa6807f966e1e63a2560ffd3d0e );
    }
    cache_frame_edf6eaa6807f966e1e63a2560ffd3d0e = NULL;

    assertFrameObject( frame_edf6eaa6807f966e1e63a2560ffd3d0e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_25__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buffer );
    Py_DECREF( var_buffer );
    var_buffer = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buffer );
    var_buffer = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_25__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_26__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_b = NULL;
    struct Nuitka_FrameObject *frame_3a411fcaca5bb9e0abca1c4a101dd287;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3a411fcaca5bb9e0abca1c4a101dd287 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3a411fcaca5bb9e0abca1c4a101dd287, codeobj_3a411fcaca5bb9e0abca1c4a101dd287, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *) );
    frame_3a411fcaca5bb9e0abca1c4a101dd287 = cache_frame_3a411fcaca5bb9e0abca1c4a101dd287;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3a411fcaca5bb9e0abca1c4a101dd287 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3a411fcaca5bb9e0abca1c4a101dd287 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 264;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_b );
        tmp_source_name_2 = var_b;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_complete_state );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 265;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( var_b );
            tmp_called_instance_1 = var_b;
            frame_3a411fcaca5bb9e0abca1c4a101dd287->m_frame.f_lineno = 266;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_complete_next );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 266;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( var_b );
            tmp_source_name_3 = var_b;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_start_completion );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 268;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = PyDict_Copy( const_dict_baa3e9b9ceb00288faad435c619c9225 );
            frame_3a411fcaca5bb9e0abca1c4a101dd287->m_frame.f_lineno = 268;
            tmp_call_result_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 268;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3a411fcaca5bb9e0abca1c4a101dd287 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3a411fcaca5bb9e0abca1c4a101dd287 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3a411fcaca5bb9e0abca1c4a101dd287, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3a411fcaca5bb9e0abca1c4a101dd287->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3a411fcaca5bb9e0abca1c4a101dd287, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3a411fcaca5bb9e0abca1c4a101dd287,
        type_description_1,
        par_event,
        var_b
    );


    // Release cached frame.
    if ( frame_3a411fcaca5bb9e0abca1c4a101dd287 == cache_frame_3a411fcaca5bb9e0abca1c4a101dd287 )
    {
        Py_DECREF( frame_3a411fcaca5bb9e0abca1c4a101dd287 );
    }
    cache_frame_3a411fcaca5bb9e0abca1c4a101dd287 = NULL;

    assertFrameObject( frame_3a411fcaca5bb9e0abca1c4a101dd287 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_26__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_26__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_27__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buffer = NULL;
    PyObject *var_from_ = NULL;
    PyObject *var_to = NULL;
    PyObject *var__ = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    struct Nuitka_FrameObject *frame_095ee85caa19b794d8d193fe122b546a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_095ee85caa19b794d8d193fe122b546a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_095ee85caa19b794d8d193fe122b546a, codeobj_095ee85caa19b794d8d193fe122b546a, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_095ee85caa19b794d8d193fe122b546a = cache_frame_095ee85caa19b794d8d193fe122b546a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_095ee85caa19b794d8d193fe122b546a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_095ee85caa19b794d8d193fe122b546a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_buffer == NULL );
        var_buffer = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_2 = var_buffer;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cursor_position );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_inplace_assign_attr_1__start == NULL );
        tmp_inplace_assign_attr_1__start = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
        tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_4 = var_buffer;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_get_start_of_line_position );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_5b4e82b0d7f6ce57036ec2983d8d8c39 );
        frame_095ee85caa19b794d8d193fe122b546a->m_frame.f_lineno = 277;
        tmp_right_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        assert( tmp_inplace_assign_attr_1__end == NULL );
        tmp_inplace_assign_attr_1__end = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
        tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
        CHECK_OBJECT( var_buffer );
        tmp_assattr_target_1 = var_buffer;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_5 = var_buffer;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_document );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 279;
            type_description_1 = "ooooo";
            goto try_except_handler_4;
        }
        frame_095ee85caa19b794d8d193fe122b546a->m_frame.f_lineno = 279;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_selection_range );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 279;
            type_description_1 = "ooooo";
            goto try_except_handler_4;
        }
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 279;
            type_description_1 = "ooooo";
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 279;
            goto try_except_handler_5;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 279;
            goto try_except_handler_5;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooo";
                    exception_lineno = 279;
                    goto try_except_handler_5;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooo";
            exception_lineno = 279;
            goto try_except_handler_5;
        }
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_4;
    // End of try:
    try_end_3:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        assert( var_from_ == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_from_ = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        assert( var_to == NULL );
        Py_INCREF( tmp_assign_source_8 );
        var_to = tmp_assign_source_8;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_6 = var_buffer;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_document );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 280;
            type_description_1 = "ooooo";
            goto try_except_handler_6;
        }
        CHECK_OBJECT( var_from_ );
        tmp_args_element_name_1 = var_from_;
        frame_095ee85caa19b794d8d193fe122b546a->m_frame.f_lineno = 280;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_translate_index_to_position, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 280;
            type_description_1 = "ooooo";
            goto try_except_handler_6;
        }
        tmp_assign_source_9 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 280;
            type_description_1 = "ooooo";
            goto try_except_handler_6;
        }
        assert( tmp_tuple_unpack_2__source_iter == NULL );
        tmp_tuple_unpack_2__source_iter = tmp_assign_source_9;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_10 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_10 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 280;
            goto try_except_handler_7;
        }
        assert( tmp_tuple_unpack_2__element_1 == NULL );
        tmp_tuple_unpack_2__element_1 = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_11 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_11 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 280;
            goto try_except_handler_7;
        }
        assert( tmp_tuple_unpack_2__element_2 == NULL );
        tmp_tuple_unpack_2__element_2 = tmp_assign_source_11;
    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooo";
                    exception_lineno = 280;
                    goto try_except_handler_7;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooo";
            exception_lineno = 280;
            goto try_except_handler_7;
        }
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_6;
    // End of try:
    try_end_5:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_12;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_12 = tmp_tuple_unpack_2__element_1;
        {
            PyObject *old = var_from_;
            assert( old != NULL );
            var_from_ = tmp_assign_source_12;
            Py_INCREF( var_from_ );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_13 = tmp_tuple_unpack_2__element_2;
        assert( var__ == NULL );
        Py_INCREF( tmp_assign_source_13 );
        var__ = tmp_assign_source_13;
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_7;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_7 = var_buffer;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_document );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;
            type_description_1 = "ooooo";
            goto try_except_handler_8;
        }
        CHECK_OBJECT( var_to );
        tmp_args_element_name_2 = var_to;
        frame_095ee85caa19b794d8d193fe122b546a->m_frame.f_lineno = 281;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_iter_arg_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_translate_index_to_position, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;
            type_description_1 = "ooooo";
            goto try_except_handler_8;
        }
        tmp_assign_source_14 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;
            type_description_1 = "ooooo";
            goto try_except_handler_8;
        }
        assert( tmp_tuple_unpack_3__source_iter == NULL );
        tmp_tuple_unpack_3__source_iter = tmp_assign_source_14;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_unpack_5;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_15 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
        if ( tmp_assign_source_15 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 281;
            goto try_except_handler_9;
        }
        assert( tmp_tuple_unpack_3__element_1 == NULL );
        tmp_tuple_unpack_3__element_1 = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_unpack_6;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_16 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
        if ( tmp_assign_source_16 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 281;
            goto try_except_handler_9;
        }
        assert( tmp_tuple_unpack_3__element_2 == NULL );
        tmp_tuple_unpack_3__element_2 = tmp_assign_source_16;
    }
    {
        PyObject *tmp_iterator_name_3;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_iterator_name_3 = tmp_tuple_unpack_3__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooo";
                    exception_lineno = 281;
                    goto try_except_handler_9;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooo";
            exception_lineno = 281;
            goto try_except_handler_9;
        }
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto try_except_handler_8;
    // End of try:
    try_end_7:;
    goto try_end_8;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    {
        PyObject *tmp_assign_source_17;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
        tmp_assign_source_17 = tmp_tuple_unpack_3__element_1;
        {
            PyObject *old = var_to;
            assert( old != NULL );
            var_to = tmp_assign_source_17;
            Py_INCREF( var_to );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    {
        PyObject *tmp_assign_source_18;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
        tmp_assign_source_18 = tmp_tuple_unpack_3__element_2;
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_18;
            Py_INCREF( var__ );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_8;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_indent );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_indent );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "indent" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 283;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( var_buffer );
        tmp_tuple_element_1 = var_buffer;
        tmp_args_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_from_ );
        tmp_tuple_element_1 = var_from_;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_to );
        tmp_left_name_2 = var_to;
        tmp_right_name_2 = const_int_pos_1;
        tmp_tuple_element_1 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 283;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_8 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 283;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_095ee85caa19b794d8d193fe122b546a->m_frame.f_lineno = 283;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 283;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_095ee85caa19b794d8d193fe122b546a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_095ee85caa19b794d8d193fe122b546a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_095ee85caa19b794d8d193fe122b546a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_095ee85caa19b794d8d193fe122b546a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_095ee85caa19b794d8d193fe122b546a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_095ee85caa19b794d8d193fe122b546a,
        type_description_1,
        par_event,
        var_buffer,
        var_from_,
        var_to,
        var__
    );


    // Release cached frame.
    if ( frame_095ee85caa19b794d8d193fe122b546a == cache_frame_095ee85caa19b794d8d193fe122b546a )
    {
        Py_DECREF( frame_095ee85caa19b794d8d193fe122b546a );
    }
    cache_frame_095ee85caa19b794d8d193fe122b546a = NULL;

    assertFrameObject( frame_095ee85caa19b794d8d193fe122b546a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_27__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buffer );
    Py_DECREF( var_buffer );
    var_buffer = NULL;

    CHECK_OBJECT( (PyObject *)var_from_ );
    Py_DECREF( var_from_ );
    var_from_ = NULL;

    CHECK_OBJECT( (PyObject *)var_to );
    Py_DECREF( var_to );
    var_to = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buffer );
    var_buffer = NULL;

    Py_XDECREF( var_from_ );
    var_from_ = NULL;

    Py_XDECREF( var_to );
    var_to = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_27__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_28__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buffer = NULL;
    PyObject *var_from_ = NULL;
    PyObject *var_to = NULL;
    PyObject *var__ = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    struct Nuitka_FrameObject *frame_7e5974495860987fed4908a679b90371;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_7e5974495860987fed4908a679b90371 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7e5974495860987fed4908a679b90371, codeobj_7e5974495860987fed4908a679b90371, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7e5974495860987fed4908a679b90371 = cache_frame_7e5974495860987fed4908a679b90371;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7e5974495860987fed4908a679b90371 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7e5974495860987fed4908a679b90371 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 290;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_buffer == NULL );
        var_buffer = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_2 = var_buffer;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_document );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 292;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        frame_7e5974495860987fed4908a679b90371->m_frame.f_lineno = 292;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_selection_range );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 292;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 292;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 292;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 292;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooo";
                    exception_lineno = 292;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooo";
            exception_lineno = 292;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;
        assert( var_from_ == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_from_ = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;
        assert( var_to == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_to = tmp_assign_source_6;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_3 = var_buffer;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 293;
            type_description_1 = "ooooo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( var_from_ );
        tmp_args_element_name_1 = var_from_;
        frame_7e5974495860987fed4908a679b90371->m_frame.f_lineno = 293;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_translate_index_to_position, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 293;
            type_description_1 = "ooooo";
            goto try_except_handler_4;
        }
        tmp_assign_source_7 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 293;
            type_description_1 = "ooooo";
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_2__source_iter == NULL );
        tmp_tuple_unpack_2__source_iter = tmp_assign_source_7;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 293;
            goto try_except_handler_5;
        }
        assert( tmp_tuple_unpack_2__element_1 == NULL );
        tmp_tuple_unpack_2__element_1 = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_9 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_9 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 293;
            goto try_except_handler_5;
        }
        assert( tmp_tuple_unpack_2__element_2 == NULL );
        tmp_tuple_unpack_2__element_2 = tmp_assign_source_9;
    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooo";
                    exception_lineno = 293;
                    goto try_except_handler_5;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooo";
            exception_lineno = 293;
            goto try_except_handler_5;
        }
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_4;
    // End of try:
    try_end_3:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_10 = tmp_tuple_unpack_2__element_1;
        {
            PyObject *old = var_from_;
            assert( old != NULL );
            var_from_ = tmp_assign_source_10;
            Py_INCREF( var_from_ );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_11 = tmp_tuple_unpack_2__element_2;
        assert( var__ == NULL );
        Py_INCREF( tmp_assign_source_11 );
        var__ = tmp_assign_source_11;
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_4 = var_buffer;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "ooooo";
            goto try_except_handler_6;
        }
        CHECK_OBJECT( var_to );
        tmp_args_element_name_2 = var_to;
        frame_7e5974495860987fed4908a679b90371->m_frame.f_lineno = 294;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_iter_arg_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_translate_index_to_position, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "ooooo";
            goto try_except_handler_6;
        }
        tmp_assign_source_12 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "ooooo";
            goto try_except_handler_6;
        }
        assert( tmp_tuple_unpack_3__source_iter == NULL );
        tmp_tuple_unpack_3__source_iter = tmp_assign_source_12;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_unpack_5;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
        if ( tmp_assign_source_13 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 294;
            goto try_except_handler_7;
        }
        assert( tmp_tuple_unpack_3__element_1 == NULL );
        tmp_tuple_unpack_3__element_1 = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_unpack_6;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_14 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
        if ( tmp_assign_source_14 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 294;
            goto try_except_handler_7;
        }
        assert( tmp_tuple_unpack_3__element_2 == NULL );
        tmp_tuple_unpack_3__element_2 = tmp_assign_source_14;
    }
    {
        PyObject *tmp_iterator_name_3;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_iterator_name_3 = tmp_tuple_unpack_3__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooo";
                    exception_lineno = 294;
                    goto try_except_handler_7;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooo";
            exception_lineno = 294;
            goto try_except_handler_7;
        }
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_6;
    // End of try:
    try_end_5:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    {
        PyObject *tmp_assign_source_15;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
        tmp_assign_source_15 = tmp_tuple_unpack_3__element_1;
        {
            PyObject *old = var_to;
            assert( old != NULL );
            var_to = tmp_assign_source_15;
            Py_INCREF( var_to );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    {
        PyObject *tmp_assign_source_16;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
        tmp_assign_source_16 = tmp_tuple_unpack_3__element_2;
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_16;
            Py_INCREF( var__ );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_5;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_unindent );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unindent );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unindent" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 296;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_buffer );
        tmp_tuple_element_1 = var_buffer;
        tmp_args_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_from_ );
        tmp_tuple_element_1 = var_from_;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_to );
        tmp_left_name_1 = var_to;
        tmp_right_name_1 = const_int_pos_1;
        tmp_tuple_element_1 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 296;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 296;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_7e5974495860987fed4908a679b90371->m_frame.f_lineno = 296;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 296;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7e5974495860987fed4908a679b90371 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7e5974495860987fed4908a679b90371 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7e5974495860987fed4908a679b90371, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7e5974495860987fed4908a679b90371->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7e5974495860987fed4908a679b90371, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7e5974495860987fed4908a679b90371,
        type_description_1,
        par_event,
        var_buffer,
        var_from_,
        var_to,
        var__
    );


    // Release cached frame.
    if ( frame_7e5974495860987fed4908a679b90371 == cache_frame_7e5974495860987fed4908a679b90371 )
    {
        Py_DECREF( frame_7e5974495860987fed4908a679b90371 );
    }
    cache_frame_7e5974495860987fed4908a679b90371 = NULL;

    assertFrameObject( frame_7e5974495860987fed4908a679b90371 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_28__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buffer );
    Py_DECREF( var_buffer );
    var_buffer = NULL;

    CHECK_OBJECT( (PyObject *)var_from_ );
    Py_DECREF( var_from_ );
    var_from_ = NULL;

    CHECK_OBJECT( (PyObject *)var_to );
    Py_DECREF( var_to );
    var_to = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buffer );
    var_buffer = NULL;

    Py_XDECREF( var_from_ );
    var_from_ = NULL;

    Py_XDECREF( var_to );
    var_to = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_28__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_key_bindings = NULL;
    PyObject *var_handle = NULL;
    PyObject *var_search = NULL;
    PyObject *var__ = NULL;
    struct Nuitka_FrameObject *frame_9de8f726ea4501a75389de56bbf7ef36;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_9de8f726ea4501a75389de56bbf7ef36 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9de8f726ea4501a75389de56bbf7ef36, codeobj_9de8f726ea4501a75389de56bbf7ef36, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9de8f726ea4501a75389de56bbf7ef36 = cache_frame_9de8f726ea4501a75389de56bbf7ef36;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9de8f726ea4501a75389de56bbf7ef36 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9de8f726ea4501a75389de56bbf7ef36 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_KeyBindings );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KeyBindings );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "KeyBindings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 302;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 302;
        tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 302;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_key_bindings == NULL );
        var_key_bindings = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_key_bindings );
        tmp_source_name_1 = var_key_bindings;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_add );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 303;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_handle == NULL );
        var_handle = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_empty;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_search_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 304;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_3 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs,
                const_str_plain_search,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_3 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_search );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_search == NULL );
        var_search = tmp_assign_source_3;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_handle );
        tmp_called_name_3 = var_handle;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 310;
        tmp_called_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_str_digest_a71cd96de41b1d83d8e604317aa9a7eb_tuple, 0 ) );

        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_2 = var_search;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_start_reverse_incremental_search );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 310;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 310;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_called_name_5;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_handle );
        tmp_called_name_5 = var_handle;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 311;
        tmp_called_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_str_digest_f2a75caf8ec802255b8dafde1721755d_tuple, 0 ) );

        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 311;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_3 = var_search;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_start_forward_incremental_search );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 311;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 311;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 311;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_called_name_7;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_handle );
        tmp_called_name_7 = var_handle;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 313;
        tmp_called_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_str_digest_99c20ff6137166a3040444a10775418c_tuple, 0 ) );

        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_4 = var_search;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_abort_search );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 313;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 313;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 313;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_name_8;
        PyObject *tmp_called_name_9;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( var_handle );
        tmp_called_name_9 = var_handle;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 314;
        tmp_called_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_digest_faa2df820fbcc927d6a1cbff6da2d7e2_tuple, 0 ) );

        if ( tmp_called_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_5 = var_search;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_abort_search );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_8 );

            exception_lineno = 314;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 314;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_called_name_8 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_name_10;
        PyObject *tmp_called_name_11;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( var_handle );
        tmp_called_name_11 = var_handle;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 315;
        tmp_called_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_str_digest_a71cd96de41b1d83d8e604317aa9a7eb_tuple, 0 ) );

        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 315;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_6 = var_search;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_reverse_incremental_search );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 315;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 315;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 315;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_called_name_12;
        PyObject *tmp_called_name_13;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( var_handle );
        tmp_called_name_13 = var_handle;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 316;
        tmp_called_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_digest_f2a75caf8ec802255b8dafde1721755d_tuple, 0 ) );

        if ( tmp_called_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_7 = var_search;
        tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_forward_incremental_search );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_12 );

            exception_lineno = 316;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 316;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
        }

        Py_DECREF( tmp_called_name_12 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        PyObject *tmp_called_name_14;
        PyObject *tmp_called_name_15;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_source_name_8;
        CHECK_OBJECT( var_handle );
        tmp_called_name_15 = var_handle;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 317;
        tmp_called_name_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_plain_up_tuple, 0 ) );

        if ( tmp_called_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_8 = var_search;
        tmp_args_element_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_reverse_incremental_search );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_14 );

            exception_lineno = 317;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 317;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_called_name_14 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        PyObject *tmp_called_name_16;
        PyObject *tmp_called_name_17;
        PyObject *tmp_call_result_8;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( var_handle );
        tmp_called_name_17 = var_handle;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 318;
        tmp_called_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_plain_down_tuple, 0 ) );

        if ( tmp_called_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 318;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_9 = var_search;
        tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_forward_incremental_search );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_16 );

            exception_lineno = 318;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 318;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_called_name_16 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_call_result_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 318;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_8 );
    }
    {
        PyObject *tmp_called_name_18;
        PyObject *tmp_called_name_19;
        PyObject *tmp_call_result_9;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_source_name_10;
        CHECK_OBJECT( var_handle );
        tmp_called_name_19 = var_handle;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 319;
        tmp_called_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, &PyTuple_GET_ITEM( const_tuple_str_plain_enter_tuple, 0 ) );

        if ( tmp_called_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_10 = var_search;
        tmp_args_element_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_accept_search );
        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_18 );

            exception_lineno = 319;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 319;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_call_result_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
        }

        Py_DECREF( tmp_called_name_18 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_call_result_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_9 );
    }
    {
        PyObject *tmp_called_name_20;
        PyObject *tmp_called_name_21;
        PyObject *tmp_args_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_call_result_10;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_source_name_11;
        CHECK_OBJECT( var_handle );
        tmp_called_name_21 = var_handle;
        tmp_args_name_1 = const_tuple_str_plain_escape_tuple;
        tmp_kw_name_1 = PyDict_Copy( const_dict_b3a49b9216296cad44ade60138ab7bee );
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 322;
        tmp_called_name_20 = CALL_FUNCTION( tmp_called_name_21, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 322;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_11 = var_search;
        tmp_args_element_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_accept_search );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_20 );

            exception_lineno = 322;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 322;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_call_result_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
        }

        Py_DECREF( tmp_called_name_20 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_call_result_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 322;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_10 );
    }
    {
        PyObject *tmp_called_name_22;
        PyObject *tmp_called_name_23;
        PyObject *tmp_args_name_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_11;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_source_name_12;
        CHECK_OBJECT( var_handle );
        tmp_called_name_23 = var_handle;
        tmp_args_name_2 = const_tuple_str_chr_63_tuple;
        tmp_dict_key_1 = const_str_plain_filter;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_is_read_only );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_read_only );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_read_only" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_1 = tmp_mvar_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_vi_search_direction_reversed );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_search_direction_reversed );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_search_direction_reversed" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_1 = tmp_mvar_value_3;
        tmp_right_name_1 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 333;
        tmp_called_name_22 = CALL_FUNCTION( tmp_called_name_23, tmp_args_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_called_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_12 = var_search;
        tmp_args_element_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_start_reverse_incremental_search );
        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_22 );

            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 333;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_call_result_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_called_name_22 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_call_result_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_11 );
    }
    {
        PyObject *tmp_called_name_24;
        PyObject *tmp_called_name_25;
        PyObject *tmp_args_name_3;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_left_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_right_name_2;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_call_result_12;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_source_name_13;
        CHECK_OBJECT( var_handle );
        tmp_called_name_25 = var_handle;
        tmp_args_name_3 = const_tuple_str_chr_47_tuple;
        tmp_dict_key_2 = const_str_plain_filter;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_is_read_only );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_read_only );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_read_only" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 334;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_2 = tmp_mvar_value_4;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_vi_search_direction_reversed );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_search_direction_reversed );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_search_direction_reversed" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 334;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_2 = tmp_mvar_value_5;
        tmp_right_name_2 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_2 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_2 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_3 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 334;
        tmp_called_name_24 = CALL_FUNCTION( tmp_called_name_25, tmp_args_name_3, tmp_kw_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_called_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_13 = var_search;
        tmp_args_element_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_start_forward_incremental_search );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_24 );

            exception_lineno = 334;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 334;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_call_result_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, call_args );
        }

        Py_DECREF( tmp_called_name_24 );
        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_call_result_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_12 );
    }
    {
        PyObject *tmp_called_name_26;
        PyObject *tmp_called_name_27;
        PyObject *tmp_args_name_4;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_left_name_3;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_right_name_3;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_call_result_13;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_source_name_14;
        CHECK_OBJECT( var_handle );
        tmp_called_name_27 = var_handle;
        tmp_args_name_4 = const_tuple_str_chr_63_tuple;
        tmp_dict_key_3 = const_str_plain_filter;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_is_read_only );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_read_only );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_read_only" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 335;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_3 = tmp_mvar_value_6;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_vi_search_direction_reversed );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_search_direction_reversed );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_search_direction_reversed" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 335;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_3 = tmp_mvar_value_7;
        tmp_dict_value_3 = BINARY_OPERATION( PyNumber_And, tmp_left_name_3, tmp_right_name_3 );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 335;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_4 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 335;
        tmp_called_name_26 = CALL_FUNCTION( tmp_called_name_27, tmp_args_name_4, tmp_kw_name_4 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_called_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 335;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_14 = var_search;
        tmp_args_element_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_start_forward_incremental_search );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_26 );

            exception_lineno = 335;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 335;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_call_result_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, call_args );
        }

        Py_DECREF( tmp_called_name_26 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_call_result_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 335;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_13 );
    }
    {
        PyObject *tmp_called_name_28;
        PyObject *tmp_called_name_29;
        PyObject *tmp_args_name_5;
        PyObject *tmp_kw_name_5;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_left_name_4;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_right_name_4;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_call_result_14;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_source_name_15;
        CHECK_OBJECT( var_handle );
        tmp_called_name_29 = var_handle;
        tmp_args_name_5 = const_tuple_str_chr_47_tuple;
        tmp_dict_key_4 = const_str_plain_filter;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_is_read_only );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_read_only );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_read_only" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 336;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_4 = tmp_mvar_value_8;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_vi_search_direction_reversed );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_search_direction_reversed );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_search_direction_reversed" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 336;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_4 = tmp_mvar_value_9;
        tmp_dict_value_4 = BINARY_OPERATION( PyNumber_And, tmp_left_name_4, tmp_right_name_4 );
        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 336;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_5 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 336;
        tmp_called_name_28 = CALL_FUNCTION( tmp_called_name_29, tmp_args_name_5, tmp_kw_name_5 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_called_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 336;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_search );
        tmp_source_name_15 = var_search;
        tmp_args_element_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_start_reverse_incremental_search );
        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 336;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 336;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_call_result_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
        }

        Py_DECREF( tmp_called_name_28 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_call_result_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 336;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_14 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_30;
        PyObject *tmp_called_name_31;
        PyObject *tmp_args_name_6;
        PyObject *tmp_kw_name_6;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_15;
        CHECK_OBJECT( var_handle );
        tmp_called_name_31 = var_handle;
        tmp_args_name_6 = const_tuple_str_plain_n_tuple;
        tmp_dict_key_5 = const_str_plain_filter;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_is_read_only );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_read_only );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_read_only" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 338;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_5 = tmp_mvar_value_10;
        tmp_kw_name_6 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_6, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 338;
        tmp_called_name_30 = CALL_FUNCTION( tmp_called_name_31, tmp_args_name_6, tmp_kw_name_6 );
        Py_DECREF( tmp_kw_name_6 );
        if ( tmp_called_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 338;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_15 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_1__(  );



        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 338;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_30, call_args );
        }

        Py_DECREF( tmp_called_name_30 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 338;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var__ == NULL );
        var__ = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_32;
        PyObject *tmp_called_name_33;
        PyObject *tmp_args_name_7;
        PyObject *tmp_kw_name_7;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_16;
        CHECK_OBJECT( var_handle );
        tmp_called_name_33 = var_handle;
        tmp_args_name_7 = const_tuple_str_plain_N_tuple;
        tmp_dict_key_6 = const_str_plain_filter;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_is_read_only );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_read_only );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_read_only" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 346;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_6 = tmp_mvar_value_11;
        tmp_kw_name_7 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 346;
        tmp_called_name_32 = CALL_FUNCTION( tmp_called_name_33, tmp_args_name_7, tmp_kw_name_7 );
        Py_DECREF( tmp_kw_name_7 );
        if ( tmp_called_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_16 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_2__(  );



        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 346;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_32, call_args );
        }

        Py_DECREF( tmp_called_name_32 );
        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_5;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_34;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_ConditionalKeyBindings );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ConditionalKeyBindings );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ConditionalKeyBindings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 354;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_34 = tmp_mvar_value_12;
        CHECK_OBJECT( var_key_bindings );
        tmp_args_element_name_17 = var_key_bindings;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_emacs_mode );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_emacs_mode );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "emacs_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 354;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_18 = tmp_mvar_value_13;
        frame_9de8f726ea4501a75389de56bbf7ef36->m_frame.f_lineno = 354;
        {
            PyObject *call_args[] = { tmp_args_element_name_17, tmp_args_element_name_18 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_34, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 354;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9de8f726ea4501a75389de56bbf7ef36 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9de8f726ea4501a75389de56bbf7ef36 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9de8f726ea4501a75389de56bbf7ef36 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9de8f726ea4501a75389de56bbf7ef36, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9de8f726ea4501a75389de56bbf7ef36->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9de8f726ea4501a75389de56bbf7ef36, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9de8f726ea4501a75389de56bbf7ef36,
        type_description_1,
        var_key_bindings,
        var_handle,
        var_search,
        var__
    );


    // Release cached frame.
    if ( frame_9de8f726ea4501a75389de56bbf7ef36 == cache_frame_9de8f726ea4501a75389de56bbf7ef36 )
    {
        Py_DECREF( frame_9de8f726ea4501a75389de56bbf7ef36 );
    }
    cache_frame_9de8f726ea4501a75389de56bbf7ef36 = NULL;

    assertFrameObject( frame_9de8f726ea4501a75389de56bbf7ef36 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_key_bindings );
    Py_DECREF( var_key_bindings );
    var_key_bindings = NULL;

    CHECK_OBJECT( (PyObject *)var_handle );
    Py_DECREF( var_handle );
    var_handle = NULL;

    CHECK_OBJECT( (PyObject *)var_search );
    Py_DECREF( var_search );
    var_search = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_key_bindings );
    var_key_bindings = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    Py_XDECREF( var_search );
    var_search = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_1__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c59078db7e911c13f595ce86066b5552;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_c59078db7e911c13f595ce86066b5552 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c59078db7e911c13f595ce86066b5552, codeobj_c59078db7e911c13f595ce86066b5552, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *) );
    frame_c59078db7e911c13f595ce86066b5552 = cache_frame_c59078db7e911c13f595ce86066b5552;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c59078db7e911c13f595ce86066b5552 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c59078db7e911c13f595ce86066b5552 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 341;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_apply_search );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 341;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_app );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 342;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_current_search_state );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 342;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_include_current_position;
        tmp_dict_value_1 = Py_False;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 344;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_c59078db7e911c13f595ce86066b5552->m_frame.f_lineno = 341;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 341;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c59078db7e911c13f595ce86066b5552 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c59078db7e911c13f595ce86066b5552 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c59078db7e911c13f595ce86066b5552, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c59078db7e911c13f595ce86066b5552->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c59078db7e911c13f595ce86066b5552, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c59078db7e911c13f595ce86066b5552,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_c59078db7e911c13f595ce86066b5552 == cache_frame_c59078db7e911c13f595ce86066b5552 )
    {
        Py_DECREF( frame_c59078db7e911c13f595ce86066b5552 );
    }
    cache_frame_c59078db7e911c13f595ce86066b5552 = NULL;

    assertFrameObject( frame_c59078db7e911c13f595ce86066b5552 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_1__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_1__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_2__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_69d615a74aa792779c7ce0296ec03609;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_69d615a74aa792779c7ce0296ec03609 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_69d615a74aa792779c7ce0296ec03609, codeobj_69d615a74aa792779c7ce0296ec03609, module_prompt_toolkit$key_binding$bindings$emacs, sizeof(void *) );
    frame_69d615a74aa792779c7ce0296ec03609 = cache_frame_69d615a74aa792779c7ce0296ec03609;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_69d615a74aa792779c7ce0296ec03609 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_69d615a74aa792779c7ce0296ec03609 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 349;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_apply_search );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 349;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_app );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 350;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_current_search_state );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 350;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 350;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_include_current_position;
        tmp_dict_value_1 = Py_False;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 352;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_69d615a74aa792779c7ce0296ec03609->m_frame.f_lineno = 349;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 349;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_69d615a74aa792779c7ce0296ec03609 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_69d615a74aa792779c7ce0296ec03609 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_69d615a74aa792779c7ce0296ec03609, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_69d615a74aa792779c7ce0296ec03609->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_69d615a74aa792779c7ce0296ec03609, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_69d615a74aa792779c7ce0296ec03609,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_69d615a74aa792779c7ce0296ec03609 == cache_frame_69d615a74aa792779c7ce0296ec03609 )
    {
        Py_DECREF( frame_69d615a74aa792779c7ce0296ec03609 );
    }
    cache_frame_69d615a74aa792779c7ce0296ec03609 = NULL;

    assertFrameObject( frame_69d615a74aa792779c7ce0296ec03609 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_2__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_2__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings,
        const_str_plain_load_emacs_bindings,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5dbd4e3413cc7f67ed8c68e401cdc48e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_23ee9375b47833896f17de0216290961,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_10_is_returnable(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_10_is_returnable,
        const_str_plain_is_returnable,
#if PYTHON_VERSION >= 300
        const_str_digest_51765d060514397eb589107966de5d76,
#endif
        codeobj_9aef691f2da727794e9cdacaa2210a0d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_11_character_search(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_11_character_search,
        const_str_plain_character_search,
#if PYTHON_VERSION >= 300
        const_str_digest_4ad83595a950c69d845ee734d684b5de,
#endif
        codeobj_ad505444d20353014afe05006e6e8d3a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_12__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_12__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_22049ef5e24d71ef8e11931cfb18870a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_23b64858594527c331ccb1200cf7f9fb,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_13__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_13__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_cb46efa5acfd6e83bed442fb852708bd,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_d529933db36a700e95541891c167c7d9,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_14__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_14__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_2430e93442085971c66cb9eef5ca8a3e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_8ec48734954c8fd44d61dd501dd2ddfa,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_15__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_15__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_c07ea4e26aa4f8d1ab23551a798518af,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_303c2ff1d1bb2781ed8ba7f5913d7adc,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_16__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_16__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_dfc06c882257802987b44fa5c42e515d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_0be999944cc03256ef7b26af4ee4697c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_17__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_15b84f706ba18ad1c9fcbf547a8ed6b0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_0762d8674f4ac16d5fe4664d8da8a610,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_18__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_18__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_b616f61388861cc23d80906312549a0c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_bad39abf5991d6848a5eaabb10c0d642,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_19__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_19__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_655ef0697fd9f535491eaf3ff1799405,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_4142ad0b126076d41adfa300a4f7e7ce,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_1__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_1__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_8924278ddaa3cb41198aaa3d5407fada,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_7680d57b355abe15172bd7731382572c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_20__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_20__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_cf42bea14394d91a76668f60f023eacc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_e57e9cdba81c4bdcc73a534630ea08f1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_21__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_21__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_a20344d433cd6984d126314d57bceb35,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_dbfc7da45fdc782269b5d187f3020039,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_22__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_22__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_70b09fedff819bd1cc7106a3c27b3945,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_d10127819ffc31ff0704f8ccddefdadf,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_23__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_23__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_3499e630a1c715a6782317aaad22ae6e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_56e920264daa106f062e68a1ec701d29,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_24__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_24__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_f34edee97ddfaa882297fe4bceb24d98,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_3d9afdb148843bb4992622c22022e531,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_25__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_25__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_edf6eaa6807f966e1e63a2560ffd3d0e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_25ba7d00fad2cf2267463c12875fd008,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_26__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_26__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_3a411fcaca5bb9e0abca1c4a101dd287,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_7895461a6e5df04c005e365a196a2cd9,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_27__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_27__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_095ee85caa19b794d8d193fe122b546a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_19937bb605e4ff97939df877fb3a3dba,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_28__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_28__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_7e5974495860987fed4908a679b90371,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_36d4b9f6b8c00f3caaf59bb3cc6d8f8e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_2_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_2_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_1a4a8eb7fcb290bfaf7f2a74ec3bd6e6,
#endif
        codeobj_88b6e14da00dc5c2b3ce10164404de82,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_3_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_3_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_1a4a8eb7fcb290bfaf7f2a74ec3bd6e6,
#endif
        codeobj_a9096c986027d7c4cc17980f4c730da6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_4__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_4__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_b4d2d8c8516dc5482abf143712f11867,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_2841ebef76882408ceaa61749f1115dc,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_5__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_5__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_fde0797c15931de70c425eaf5d1dc63e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_13a72683b8db98826bde02b62e2ff1e5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit,
        const_str_plain_handle_digit,
#if PYTHON_VERSION >= 300
        const_str_digest_673bac2daa78cc2280d7761be6670617,
#endif
        codeobj_3a44b69542262cbf32c6df5db6fce978,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_552610faf02e8a2270c9a3dac66f8598,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit$$$function_1__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_6_handle_digit$$$function_1__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_db2a23c722daba3c9afababc05121cfb,
#endif
        codeobj_dc77a1cef49efbb1da8d023e89ab872d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_7__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_7__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_339b0bf3b0b5dafd995be974b89551c1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_1cc17ecfecf8f4814db97df6f8dcaebf,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_8__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_8__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_fffe26fedcc3b982df3e5829a1bb0b2f,
#endif
        codeobj_e5eda8746a8b596dc9f4d379d11774c7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_805a29513a8af8331b0890a04c5e3b8d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_9_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings$$$function_9_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_1a4a8eb7fcb290bfaf7f2a74ec3bd6e6,
#endif
        codeobj_d408982fffef1590d6d9cd23cab783d4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings,
        const_str_plain_load_emacs_search_bindings,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9de8f726ea4501a75389de56bbf7ef36,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_1__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_1__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_e2ead212ecbf1246fa221343cd9ec59b,
#endif
        codeobj_c59078db7e911c13f595ce86066b5552,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_57b0696d1e802eca5a7454735008d012,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_2__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings$$$function_2__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_e2ead212ecbf1246fa221343cd9ec59b,
#endif
        codeobj_69d615a74aa792779c7ce0296ec03609,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$emacs,
        const_str_digest_16336d64d361bd5d736d7f5f0813c051,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$key_binding$bindings$emacs =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.key_binding.bindings.emacs",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$emacs)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$key_binding$bindings$emacs)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$emacs );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.emacs: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.emacs: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.emacs: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$key_binding$bindings$emacs" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$key_binding$bindings$emacs = Py_InitModule4(
        "prompt_toolkit.key_binding.bindings.emacs",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$key_binding$bindings$emacs = PyModule_Create( &mdef_prompt_toolkit$key_binding$bindings$emacs );
#endif

    moduledict_prompt_toolkit$key_binding$bindings$emacs = MODULE_DICT( module_prompt_toolkit$key_binding$bindings$emacs );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$key_binding$bindings$emacs,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$key_binding$bindings$emacs,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$emacs,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$emacs,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$key_binding$bindings$emacs );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_4c9694e2a2207ea9dfca6d786b312099, module_prompt_toolkit$key_binding$bindings$emacs );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_99891091142b0215282b60d85f3a4651;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_99891091142b0215282b60d85f3a4651 = MAKE_MODULE_FRAME( codeobj_99891091142b0215282b60d85f3a4651, module_prompt_toolkit$key_binding$bindings$emacs );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_99891091142b0215282b60d85f3a4651 );
    assert( Py_REFCNT( frame_99891091142b0215282b60d85f3a4651 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_99891091142b0215282b60d85f3a4651->m_frame.f_lineno = 2;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_722e5e8c7f53b34663d9669ca7ddc056;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_get_app_tuple;
        tmp_level_name_1 = const_int_0;
        frame_99891091142b0215282b60d85f3a4651->m_frame.f_lineno = 3;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_get_app );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_app, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_8f4424fc4ecbf6f26054d535e25dd9b5;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_SelectionType_str_plain_indent_str_plain_unindent_tuple;
        tmp_level_name_2 = const_int_0;
        frame_99891091142b0215282b60d85f3a4651->m_frame.f_lineno = 4;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_SelectionType );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_SelectionType, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_indent );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_indent, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_unindent );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_unindent, tmp_assign_source_9 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_e371fcb47bb33b8f0db02b1ad9764d6e;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_CompleteEvent_tuple;
        tmp_level_name_3 = const_int_0;
        frame_99891091142b0215282b60d85f3a4651->m_frame.f_lineno = 5;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_CompleteEvent );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_CompleteEvent, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_3a16fa49bea795e638aac8247b0f7f8d;
        tmp_globals_name_4 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_ebe3cadf3a8384c69b1ac22f254fafbf_tuple;
        tmp_level_name_4 = const_int_0;
        frame_99891091142b0215282b60d85f3a4651->m_frame.f_lineno = 6;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_11;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_Condition );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_Condition, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_8 = tmp_import_from_2__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_emacs_mode );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_emacs_mode, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_9 = tmp_import_from_2__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_has_selection );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_selection, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_10 = tmp_import_from_2__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_emacs_insert_mode );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_emacs_insert_mode, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_11 = tmp_import_from_2__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_has_arg );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_has_arg, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_12 = tmp_import_from_2__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_is_multiline );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_is_multiline, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_13 = tmp_import_from_2__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_is_read_only );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_is_read_only, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_14 = tmp_import_from_2__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_vi_search_direction_reversed );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_vi_search_direction_reversed, tmp_assign_source_19 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_15;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_b7cd7d3061b29ef6badd990d7c83ddb1;
        tmp_globals_name_5 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_Keys_tuple;
        tmp_level_name_5 = const_int_0;
        frame_99891091142b0215282b60d85f3a4651->m_frame.f_lineno = 7;
        tmp_import_name_from_15 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_Keys );
        Py_DECREF( tmp_import_name_from_15 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_Keys, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_16;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_named_commands;
        tmp_globals_name_6 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_get_by_name_tuple;
        tmp_level_name_6 = const_int_pos_1;
        frame_99891091142b0215282b60d85f3a4651->m_frame.f_lineno = 9;
        tmp_import_name_from_16 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_16 ) )
        {
           tmp_assign_source_21 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_16,
                (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs,
                const_str_plain_get_by_name,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_get_by_name );
        }

        Py_DECREF( tmp_import_name_from_16 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_get_by_name, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_key_bindings;
        tmp_globals_name_7 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_KeyBindings_str_plain_ConditionalKeyBindings_tuple;
        tmp_level_name_7 = const_int_pos_2;
        frame_99891091142b0215282b60d85f3a4651->m_frame.f_lineno = 10;
        tmp_assign_source_22 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_22;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_17;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_17 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_17 ) )
        {
           tmp_assign_source_23 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_17,
                (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs,
                const_str_plain_KeyBindings,
                const_int_pos_2
            );
        }
        else
        {
           tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_KeyBindings );
        }

        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_KeyBindings, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_18;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_18 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_18 ) )
        {
           tmp_assign_source_24 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_18,
                (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$emacs,
                const_str_plain_ConditionalKeyBindings,
                const_int_pos_2
            );
        }
        else
        {
           tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_ConditionalKeyBindings );
        }

        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_ConditionalKeyBindings, tmp_assign_source_24 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_99891091142b0215282b60d85f3a4651 );
#endif
    popFrameStack();

    assertFrameObject( frame_99891091142b0215282b60d85f3a4651 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_99891091142b0215282b60d85f3a4651 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_99891091142b0215282b60d85f3a4651, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_99891091142b0215282b60d85f3a4651->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_99891091142b0215282b60d85f3a4651, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = LIST_COPY( const_list_ef292982c39f5011b8a1e46b69b128d9_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_1_load_emacs_bindings(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_load_emacs_bindings, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$emacs$$$function_2_load_emacs_search_bindings(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$emacs, (Nuitka_StringObject *)const_str_plain_load_emacs_search_bindings, tmp_assign_source_27 );
    }

    return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$emacs );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
