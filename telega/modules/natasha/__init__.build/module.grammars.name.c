/* Generated code for Python module 'grammars.name'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_grammars$name" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_grammars$name;
PyDictObject *moduledict_grammars$name;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_ABBR;
extern PyObject *const_str_plain_tag;
extern PyObject *const_str_plain_Surn;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_MIDDLE;
static PyObject *const_str_plain_IN_FIRST;
extern PyObject *const_str_plain_length_eq;
extern PyObject *const_tuple_str_plain_I_tuple;
extern PyObject *const_str_plain_is_single;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_Rule;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_RuleTransformator;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_digest_da8e2b182ea1b8ec695e560d19a11118;
static PyObject *const_str_plain_ABBR_FIRST_LAST;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_e03d7748093bf21067c608cbcbb0520b;
static PyObject *const_str_digest_36abc8afa39dea317e9ff44ac7f08e5d;
extern PyObject *const_tuple_str_plain_RuleTransformator_tuple;
extern PyObject *const_tuple_str_plain_Surn_tuple;
extern PyObject *const_tuple_str_plain___tuple;
static PyObject *const_tuple_str_digest_ada8f42db66cef8a7d7a1e3d7384891a_tuple;
static PyObject *const_str_plain_LAST_FIRST_MIDDLE;
static PyObject *const_tuple_84328fd8c468c36e1a3a183eee8888a4_tuple;
extern PyObject *const_str_digest_941859e23d487ab2c1309773110d70db;
extern PyObject *const_str_plain_DictionaryPredicate;
static PyObject *const_str_digest_ada8f42db66cef8a7d7a1e3d7384891a;
static PyObject *const_str_plain_LAST_ABBR_FIRST;
static PyObject *const_str_plain_LAST;
static PyObject *const_str_plain_nick;
static PyObject *const_str_plain_NAME_CRF;
static PyObject *const_tuple_str_plain_self_str_plain_item_str_plain_predicates_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_AndPredicate;
extern PyObject *const_str_plain_Abbr;
extern PyObject *const_tuple_str_plain_fact_tuple;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_gnc_relation;
extern PyObject *const_tuple_str_plain_gnc_relation_tuple;
extern PyObject *const_tuple_str_plain_Rule_tuple;
extern PyObject *const_str_plain_item;
extern PyObject *const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
extern PyObject *const_str_plain_transform;
extern PyObject *const_str_digest_28ca5c1e855316b7737046f32ddceb9c;
static PyObject *const_str_digest_205c5a53f3ec252adcadf2d6b60e1eaa;
static PyObject *const_tuple_str_digest_36abc8afa39dea317e9ff44ac7f08e5d_tuple;
extern PyObject *const_str_plain_inflected;
static PyObject *const_str_plain_LAST_DICT;
extern PyObject *const_str_plain_or_;
extern PyObject *const_str_plain_TITLE;
static PyObject *const_str_digest_d2959e8ad98facc1a0ddb08096270444;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_LAST_ABBR_FIRST_MIDDLE;
extern PyObject *const_str_plain_gnc;
extern PyObject *const_str_plain_interpretation;
static PyObject *const_str_plain_FIRST_DICT;
static PyObject *const_str_plain_JUST_FIRST;
extern PyObject *const_str_digest_3b0d1c4c904989a80e46e8a766322a96;
static PyObject *const_str_plain_JUST_LAST;
extern PyObject *const_str_digest_ea78dcde38a5066c777d6274d25657b3;
extern PyObject *const_str_plain_match;
static PyObject *const_list_469fa93c52dcace8522b7619f9316836_list;
extern PyObject *const_str_plain_eq;
static PyObject *const_str_plain_IN_MAYBE_FIRST;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_yargy;
extern PyObject *const_str_plain_first;
extern PyObject *const_tuple_str_plain_rule_str_plain_and__str_plain_or__str_plain_not__tuple;
extern PyObject *const_str_plain_I;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_NAME;
static PyObject *const_tuple_str_plain_Patr_tuple;
static PyObject *const_str_plain_LAST_FIRST;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_is_capitalized;
extern PyObject *const_str_plain_visit_term;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_SIMPLE_NAME;
static PyObject *const_str_plain_IN_LAST;
static PyObject *const_str_plain_FIRST_MIDDLE;
static PyObject *const_tuple_str_plain_DictionaryPredicate_tuple;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_fact;
static PyObject *const_str_plain_FIRST_ABBR;
static PyObject *const_str_plain_MIDDLE_ABBR;
extern PyObject *const_str_plain_load_dict;
extern PyObject *const_tuple_str_plain_NOUN_tuple;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_plain_StripCrfTransformator;
extern PyObject *const_str_plain__;
static PyObject *const_tuple_str_digest_4fc2c0763b0cc54c1f0bea0c0c10e5d4_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_rule;
static PyObject *const_str_plain_PATR;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_NOUN;
extern PyObject *const_tuple_str_plain_Name_tuple;
static PyObject *const_str_digest_ab60f2b066d2efbbb1ecddbc6d6a6e18;
static PyObject *const_str_plain_MAYBE_FIRST_DICT;
extern PyObject *const_str_plain_visit;
extern PyObject *const_str_plain_last;
extern PyObject *const_str_plain_middle;
extern PyObject *const_str_digest_ece91ea33402621841e0e9196aa27a3d;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_str_plain_FIRST;
extern PyObject *const_str_plain_self;
static PyObject *const_tuple_str_plain_AndPredicate_tuple;
static PyObject *const_str_plain_FIRST_LAST;
extern PyObject *const_str_plain_Name;
static PyObject *const_str_plain_SURN;
extern PyObject *const_str_plain_not_;
extern PyObject *const_str_plain_dictionary;
extern PyObject *const_str_digest_66728ae14f823f921e32c1bfb6975ec9;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_tuple_str_plain_Abbr_tuple;
static PyObject *const_str_plain_Patr;
static PyObject *const_str_plain_FIRST_MIDDLE_LAST;
extern PyObject *const_str_plain_and_;
extern PyObject *const_str_plain_predicates;
static PyObject *const_str_digest_4fc2c0763b0cc54c1f0bea0c0c10e5d4;
extern PyObject *const_tuple_str_plain_load_dict_tuple;
static PyObject *const_str_plain_ABBR_FIRST_MIDDLE_LAST;
extern PyObject *const_str_plain_gram;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_IN_FIRST = UNSTREAM_STRING_ASCII( &constant_bin[ 679236 ], 8, 1 );
    const_str_plain_ABBR_FIRST_LAST = UNSTREAM_STRING_ASCII( &constant_bin[ 679244 ], 15, 1 );
    const_str_digest_e03d7748093bf21067c608cbcbb0520b = UNSTREAM_STRING_ASCII( &constant_bin[ 679259 ], 22, 0 );
    const_str_digest_36abc8afa39dea317e9ff44ac7f08e5d = UNSTREAM_STRING_ASCII( &constant_bin[ 679281 ], 9, 0 );
    const_tuple_str_digest_ada8f42db66cef8a7d7a1e3d7384891a_tuple = PyTuple_New( 1 );
    const_str_digest_ada8f42db66cef8a7d7a1e3d7384891a = UNSTREAM_STRING_ASCII( &constant_bin[ 679290 ], 15, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_ada8f42db66cef8a7d7a1e3d7384891a_tuple, 0, const_str_digest_ada8f42db66cef8a7d7a1e3d7384891a ); Py_INCREF( const_str_digest_ada8f42db66cef8a7d7a1e3d7384891a );
    const_str_plain_LAST_FIRST_MIDDLE = UNSTREAM_STRING_ASCII( &constant_bin[ 679305 ], 17, 1 );
    const_tuple_84328fd8c468c36e1a3a183eee8888a4_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_84328fd8c468c36e1a3a183eee8888a4_tuple, 0, const_str_plain_eq ); Py_INCREF( const_str_plain_eq );
    PyTuple_SET_ITEM( const_tuple_84328fd8c468c36e1a3a183eee8888a4_tuple, 1, const_str_plain_length_eq ); Py_INCREF( const_str_plain_length_eq );
    PyTuple_SET_ITEM( const_tuple_84328fd8c468c36e1a3a183eee8888a4_tuple, 2, const_str_plain_gram ); Py_INCREF( const_str_plain_gram );
    PyTuple_SET_ITEM( const_tuple_84328fd8c468c36e1a3a183eee8888a4_tuple, 3, const_str_plain_tag ); Py_INCREF( const_str_plain_tag );
    PyTuple_SET_ITEM( const_tuple_84328fd8c468c36e1a3a183eee8888a4_tuple, 4, const_str_plain_is_single ); Py_INCREF( const_str_plain_is_single );
    PyTuple_SET_ITEM( const_tuple_84328fd8c468c36e1a3a183eee8888a4_tuple, 5, const_str_plain_is_capitalized ); Py_INCREF( const_str_plain_is_capitalized );
    const_str_plain_LAST_ABBR_FIRST = UNSTREAM_STRING_ASCII( &constant_bin[ 679322 ], 15, 1 );
    const_str_plain_LAST = UNSTREAM_STRING_ASCII( &constant_bin[ 628862 ], 4, 1 );
    const_str_plain_nick = UNSTREAM_STRING_ASCII( &constant_bin[ 679337 ], 4, 1 );
    const_str_plain_NAME_CRF = UNSTREAM_STRING_ASCII( &constant_bin[ 679341 ], 8, 1 );
    const_tuple_str_plain_self_str_plain_item_str_plain_predicates_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_item_str_plain_predicates_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_item_str_plain_predicates_tuple, 1, const_str_plain_item ); Py_INCREF( const_str_plain_item );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_item_str_plain_predicates_tuple, 2, const_str_plain_predicates ); Py_INCREF( const_str_plain_predicates );
    const_str_digest_205c5a53f3ec252adcadf2d6b60e1eaa = UNSTREAM_STRING_ASCII( &constant_bin[ 679349 ], 12, 0 );
    const_tuple_str_digest_36abc8afa39dea317e9ff44ac7f08e5d_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_36abc8afa39dea317e9ff44ac7f08e5d_tuple, 0, const_str_digest_36abc8afa39dea317e9ff44ac7f08e5d ); Py_INCREF( const_str_digest_36abc8afa39dea317e9ff44ac7f08e5d );
    const_str_plain_LAST_DICT = UNSTREAM_STRING_ASCII( &constant_bin[ 679361 ], 9, 1 );
    const_str_digest_d2959e8ad98facc1a0ddb08096270444 = UNSTREAM_STRING_ASCII( &constant_bin[ 679370 ], 16, 0 );
    const_str_plain_LAST_ABBR_FIRST_MIDDLE = UNSTREAM_STRING_ASCII( &constant_bin[ 679386 ], 22, 1 );
    const_str_plain_FIRST_DICT = UNSTREAM_STRING_ASCII( &constant_bin[ 679408 ], 10, 1 );
    const_str_plain_JUST_FIRST = UNSTREAM_STRING_ASCII( &constant_bin[ 679418 ], 10, 1 );
    const_str_plain_JUST_LAST = UNSTREAM_STRING_ASCII( &constant_bin[ 679428 ], 9, 1 );
    const_list_469fa93c52dcace8522b7619f9316836_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_469fa93c52dcace8522b7619f9316836_list, 0, const_str_plain_first ); Py_INCREF( const_str_plain_first );
    PyList_SET_ITEM( const_list_469fa93c52dcace8522b7619f9316836_list, 1, const_str_plain_middle ); Py_INCREF( const_str_plain_middle );
    PyList_SET_ITEM( const_list_469fa93c52dcace8522b7619f9316836_list, 2, const_str_plain_last ); Py_INCREF( const_str_plain_last );
    PyList_SET_ITEM( const_list_469fa93c52dcace8522b7619f9316836_list, 3, const_str_plain_nick ); Py_INCREF( const_str_plain_nick );
    const_str_plain_IN_MAYBE_FIRST = UNSTREAM_STRING_ASCII( &constant_bin[ 679437 ], 14, 1 );
    const_tuple_str_plain_Patr_tuple = PyTuple_New( 1 );
    const_str_plain_Patr = UNSTREAM_STRING_ASCII( &constant_bin[ 679451 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Patr_tuple, 0, const_str_plain_Patr ); Py_INCREF( const_str_plain_Patr );
    const_str_plain_LAST_FIRST = UNSTREAM_STRING_ASCII( &constant_bin[ 679305 ], 10, 1 );
    const_str_plain_IN_LAST = UNSTREAM_STRING_ASCII( &constant_bin[ 679455 ], 7, 1 );
    const_str_plain_FIRST_MIDDLE = UNSTREAM_STRING_ASCII( &constant_bin[ 679310 ], 12, 1 );
    const_tuple_str_plain_DictionaryPredicate_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_DictionaryPredicate_tuple, 0, const_str_plain_DictionaryPredicate ); Py_INCREF( const_str_plain_DictionaryPredicate );
    const_str_plain_FIRST_ABBR = UNSTREAM_STRING_ASCII( &constant_bin[ 679462 ], 10, 1 );
    const_str_plain_MIDDLE_ABBR = UNSTREAM_STRING_ASCII( &constant_bin[ 679472 ], 11, 1 );
    const_str_plain_StripCrfTransformator = UNSTREAM_STRING_ASCII( &constant_bin[ 679483 ], 21, 1 );
    const_tuple_str_digest_4fc2c0763b0cc54c1f0bea0c0c10e5d4_tuple = PyTuple_New( 1 );
    const_str_digest_4fc2c0763b0cc54c1f0bea0c0c10e5d4 = UNSTREAM_STRING_ASCII( &constant_bin[ 679504 ], 8, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4fc2c0763b0cc54c1f0bea0c0c10e5d4_tuple, 0, const_str_digest_4fc2c0763b0cc54c1f0bea0c0c10e5d4 ); Py_INCREF( const_str_digest_4fc2c0763b0cc54c1f0bea0c0c10e5d4 );
    const_str_plain_PATR = UNSTREAM_STRING_ASCII( &constant_bin[ 679512 ], 4, 1 );
    const_str_digest_ab60f2b066d2efbbb1ecddbc6d6a6e18 = UNSTREAM_STRING_ASCII( &constant_bin[ 679516 ], 32, 0 );
    const_str_plain_MAYBE_FIRST_DICT = UNSTREAM_STRING_ASCII( &constant_bin[ 679548 ], 16, 1 );
    const_str_plain_FIRST = UNSTREAM_STRING_ASCII( &constant_bin[ 679239 ], 5, 1 );
    const_tuple_str_plain_AndPredicate_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_AndPredicate_tuple, 0, const_str_plain_AndPredicate ); Py_INCREF( const_str_plain_AndPredicate );
    const_str_plain_FIRST_LAST = UNSTREAM_STRING_ASCII( &constant_bin[ 679249 ], 10, 1 );
    const_str_plain_SURN = UNSTREAM_STRING_ASCII( &constant_bin[ 679564 ], 4, 1 );
    const_str_plain_FIRST_MIDDLE_LAST = UNSTREAM_STRING_ASCII( &constant_bin[ 679568 ], 17, 1 );
    const_str_plain_ABBR_FIRST_MIDDLE_LAST = UNSTREAM_STRING_ASCII( &constant_bin[ 679585 ], 22, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_grammars$name( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_eac9615156730b9c2fb5d3e706bc8e09;
static PyCodeObject *codeobj_93532fb9474d08d19660930364fca938;
static PyCodeObject *codeobj_7981df3f44e238524b4b3b6e6ab8386b;
static PyCodeObject *codeobj_30932b1212e455a8e5b46f197cd81116;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_d2959e8ad98facc1a0ddb08096270444 );
    codeobj_eac9615156730b9c2fb5d3e706bc8e09 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 251, const_tuple_str_plain___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_93532fb9474d08d19660930364fca938 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_e03d7748093bf21067c608cbcbb0520b, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7981df3f44e238524b4b3b6e6ab8386b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_StripCrfTransformator, 246, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_30932b1212e455a8e5b46f197cd81116 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_visit_term, 247, const_tuple_str_plain_self_str_plain_item_str_plain_predicates_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_grammars$name$$$function_1_visit_term(  );


// The module function definitions.
static PyObject *impl_grammars$name$$$function_1_visit_term( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_item = python_pars[ 1 ];
    PyObject *var_predicates = NULL;
    PyObject *outline_0_var__ = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_30932b1212e455a8e5b46f197cd81116;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    struct Nuitka_FrameObject *frame_eac9615156730b9c2fb5d3e706bc8e09_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_eac9615156730b9c2fb5d3e706bc8e09_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_30932b1212e455a8e5b46f197cd81116 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_30932b1212e455a8e5b46f197cd81116, codeobj_30932b1212e455a8e5b46f197cd81116, module_grammars$name, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_30932b1212e455a8e5b46f197cd81116 = cache_frame_30932b1212e455a8e5b46f197cd81116;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_30932b1212e455a8e5b46f197cd81116 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_30932b1212e455a8e5b46f197cd81116 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_item );
        tmp_isinstance_inst_1 = par_item;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_Rule );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Rule );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 248;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_self );
            tmp_called_instance_1 = par_self;
            CHECK_OBJECT( par_item );
            tmp_args_element_name_1 = par_item;
            frame_30932b1212e455a8e5b46f197cd81116->m_frame.f_lineno = 249;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_visit, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 249;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            PyObject *tmp_mvar_value_2;
            CHECK_OBJECT( par_item );
            tmp_isinstance_inst_2 = par_item;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_AndPredicate );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AndPredicate );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AndPredicate" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 250;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_isinstance_cls_2 = tmp_mvar_value_2;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 250;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_1;
                // Tried code:
                {
                    PyObject *tmp_assign_source_2;
                    PyObject *tmp_iter_arg_1;
                    PyObject *tmp_source_name_1;
                    CHECK_OBJECT( par_item );
                    tmp_source_name_1 = par_item;
                    tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_predicates );
                    if ( tmp_iter_arg_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 251;
                        type_description_1 = "ooo";
                        goto try_except_handler_2;
                    }
                    tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
                    Py_DECREF( tmp_iter_arg_1 );
                    if ( tmp_assign_source_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 251;
                        type_description_1 = "ooo";
                        goto try_except_handler_2;
                    }
                    assert( tmp_listcomp_1__$0 == NULL );
                    tmp_listcomp_1__$0 = tmp_assign_source_2;
                }
                {
                    PyObject *tmp_assign_source_3;
                    tmp_assign_source_3 = PyList_New( 0 );
                    assert( tmp_listcomp_1__contraction == NULL );
                    tmp_listcomp_1__contraction = tmp_assign_source_3;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_eac9615156730b9c2fb5d3e706bc8e09_2, codeobj_eac9615156730b9c2fb5d3e706bc8e09, module_grammars$name, sizeof(void *) );
                frame_eac9615156730b9c2fb5d3e706bc8e09_2 = cache_frame_eac9615156730b9c2fb5d3e706bc8e09_2;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_eac9615156730b9c2fb5d3e706bc8e09_2 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_eac9615156730b9c2fb5d3e706bc8e09_2 ) == 2 ); // Frame stack

                // Framed code:
                // Tried code:
                loop_start_1:;
                {
                    PyObject *tmp_next_source_1;
                    PyObject *tmp_assign_source_4;
                    CHECK_OBJECT( tmp_listcomp_1__$0 );
                    tmp_next_source_1 = tmp_listcomp_1__$0;
                    tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
                    if ( tmp_assign_source_4 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_1;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_2 = "o";
                            exception_lineno = 251;
                            goto try_except_handler_3;
                        }
                    }

                    {
                        PyObject *old = tmp_listcomp_1__iter_value_0;
                        tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_5;
                    CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                    tmp_assign_source_5 = tmp_listcomp_1__iter_value_0;
                    {
                        PyObject *old = outline_0_var__;
                        outline_0_var__ = tmp_assign_source_5;
                        Py_INCREF( outline_0_var__ );
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_3;
                    PyObject *tmp_compexpr_left_1;
                    PyObject *tmp_compexpr_right_1;
                    PyObject *tmp_mvar_value_3;
                    CHECK_OBJECT( outline_0_var__ );
                    tmp_compexpr_left_1 = outline_0_var__;
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_NAME_CRF );

                    if (unlikely( tmp_mvar_value_3 == NULL ))
                    {
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAME_CRF );
                    }

                    if ( tmp_mvar_value_3 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAME_CRF" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 251;
                        type_description_2 = "o";
                        goto try_except_handler_3;
                    }

                    tmp_compexpr_right_1 = tmp_mvar_value_3;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 251;
                        type_description_2 = "o";
                        goto try_except_handler_3;
                    }
                    tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_3;
                    }
                    else
                    {
                        goto branch_no_3;
                    }
                    branch_yes_3:;
                    {
                        PyObject *tmp_append_list_1;
                        PyObject *tmp_append_value_1;
                        CHECK_OBJECT( tmp_listcomp_1__contraction );
                        tmp_append_list_1 = tmp_listcomp_1__contraction;
                        CHECK_OBJECT( outline_0_var__ );
                        tmp_append_value_1 = outline_0_var__;
                        assert( PyList_Check( tmp_append_list_1 ) );
                        tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 251;
                            type_description_2 = "o";
                            goto try_except_handler_3;
                        }
                    }
                    branch_no_3:;
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 251;
                    type_description_2 = "o";
                    goto try_except_handler_3;
                }
                goto loop_start_1;
                loop_end_1:;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_assign_source_1 = tmp_listcomp_1__contraction;
                Py_INCREF( tmp_assign_source_1 );
                goto try_return_handler_3;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( grammars$name$$$function_1_visit_term );
                return NULL;
                // Return handler code:
                try_return_handler_3:;
                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                goto frame_return_exit_2;
                // Exception handler code:
                try_except_handler_3:;
                exception_keeper_type_1 = exception_type;
                exception_keeper_value_1 = exception_value;
                exception_keeper_tb_1 = exception_tb;
                exception_keeper_lineno_1 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_1;
                exception_value = exception_keeper_value_1;
                exception_tb = exception_keeper_tb_1;
                exception_lineno = exception_keeper_lineno_1;

                goto frame_exception_exit_2;
                // End of try:

#if 0
                RESTORE_FRAME_EXCEPTION( frame_eac9615156730b9c2fb5d3e706bc8e09_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_1;

                frame_return_exit_2:;
#if 0
                RESTORE_FRAME_EXCEPTION( frame_eac9615156730b9c2fb5d3e706bc8e09_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto try_return_handler_2;

                frame_exception_exit_2:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_eac9615156730b9c2fb5d3e706bc8e09_2 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_eac9615156730b9c2fb5d3e706bc8e09_2, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_eac9615156730b9c2fb5d3e706bc8e09_2->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_eac9615156730b9c2fb5d3e706bc8e09_2, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_eac9615156730b9c2fb5d3e706bc8e09_2,
                    type_description_2,
                    outline_0_var__
                );


                // Release cached frame.
                if ( frame_eac9615156730b9c2fb5d3e706bc8e09_2 == cache_frame_eac9615156730b9c2fb5d3e706bc8e09_2 )
                {
                    Py_DECREF( frame_eac9615156730b9c2fb5d3e706bc8e09_2 );
                }
                cache_frame_eac9615156730b9c2fb5d3e706bc8e09_2 = NULL;

                assertFrameObject( frame_eac9615156730b9c2fb5d3e706bc8e09_2 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_1;

                frame_no_exception_1:;
                goto skip_nested_handling_1;
                nested_frame_exit_1:;
                type_description_1 = "ooo";
                goto try_except_handler_2;
                skip_nested_handling_1:;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( grammars$name$$$function_1_visit_term );
                return NULL;
                // Return handler code:
                try_return_handler_2:;
                Py_XDECREF( outline_0_var__ );
                outline_0_var__ = NULL;

                goto outline_result_1;
                // Exception handler code:
                try_except_handler_2:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( outline_0_var__ );
                outline_0_var__ = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto outline_exception_1;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( grammars$name$$$function_1_visit_term );
                return NULL;
                outline_exception_1:;
                exception_lineno = 251;
                goto frame_exception_exit_1;
                outline_result_1:;
                assert( var_predicates == NULL );
                var_predicates = tmp_assign_source_1;
            }
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_args_element_name_2;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_AndPredicate );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AndPredicate );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AndPredicate" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 252;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_1 = tmp_mvar_value_4;
                CHECK_OBJECT( var_predicates );
                tmp_args_element_name_2 = var_predicates;
                frame_30932b1212e455a8e5b46f197cd81116->m_frame.f_lineno = 252;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 252;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            goto branch_end_2;
            branch_no_2:;
            CHECK_OBJECT( par_item );
            tmp_return_value = par_item;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            branch_end_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_30932b1212e455a8e5b46f197cd81116 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_30932b1212e455a8e5b46f197cd81116 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_30932b1212e455a8e5b46f197cd81116 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_30932b1212e455a8e5b46f197cd81116, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_30932b1212e455a8e5b46f197cd81116->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_30932b1212e455a8e5b46f197cd81116, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_30932b1212e455a8e5b46f197cd81116,
        type_description_1,
        par_self,
        par_item,
        var_predicates
    );


    // Release cached frame.
    if ( frame_30932b1212e455a8e5b46f197cd81116 == cache_frame_30932b1212e455a8e5b46f197cd81116 )
    {
        Py_DECREF( frame_30932b1212e455a8e5b46f197cd81116 );
    }
    cache_frame_30932b1212e455a8e5b46f197cd81116 = NULL;

    assertFrameObject( frame_30932b1212e455a8e5b46f197cd81116 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( grammars$name$$$function_1_visit_term );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_item );
    Py_DECREF( par_item );
    par_item = NULL;

    Py_XDECREF( var_predicates );
    var_predicates = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_item );
    Py_DECREF( par_item );
    par_item = NULL;

    Py_XDECREF( var_predicates );
    var_predicates = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( grammars$name$$$function_1_visit_term );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_grammars$name$$$function_1_visit_term(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_grammars$name$$$function_1_visit_term,
        const_str_plain_visit_term,
#if PYTHON_VERSION >= 300
        const_str_digest_ab60f2b066d2efbbb1ecddbc6d6a6e18,
#endif
        codeobj_30932b1212e455a8e5b46f197cd81116,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_grammars$name,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_grammars$name =
{
    PyModuleDef_HEAD_INIT,
    "grammars.name",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(grammars$name)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(grammars$name)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_grammars$name );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("grammars.name: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.name: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.name: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initgrammars$name" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_grammars$name = Py_InitModule4(
        "grammars.name",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_grammars$name = PyModule_Create( &mdef_grammars$name );
#endif

    moduledict_grammars$name = MODULE_DICT( module_grammars$name );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_grammars$name,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_grammars$name,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$name,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$name,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_grammars$name );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_28ca5c1e855316b7737046f32ddceb9c, module_grammars$name );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_93532fb9474d08d19660930364fca938;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_grammars$name_246 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_7981df3f44e238524b4b3b6e6ab8386b_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_7981df3f44e238524b4b3b6e6ab8386b_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_93532fb9474d08d19660930364fca938 = MAKE_MODULE_FRAME( codeobj_93532fb9474d08d19660930364fca938, module_grammars$name );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_93532fb9474d08d19660930364fca938 );
    assert( Py_REFCNT( frame_93532fb9474d08d19660930364fca938 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 2;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_yargy;
        tmp_globals_name_1 = (PyObject *)moduledict_grammars$name;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_rule_str_plain_and__str_plain_or__str_plain_not__tuple;
        tmp_level_name_1 = const_int_0;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 4;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_rule );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_and_ );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_and_, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_or_ );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_or_, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_not_ );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_not_, tmp_assign_source_9 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
        tmp_globals_name_2 = (PyObject *)moduledict_grammars$name;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_fact_tuple;
        tmp_level_name_2 = const_int_0;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 8;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_fact );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_fact, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_ea78dcde38a5066c777d6274d25657b3;
        tmp_globals_name_3 = (PyObject *)moduledict_grammars$name;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_84328fd8c468c36e1a3a183eee8888a4_tuple;
        tmp_level_name_3 = const_int_0;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 9;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_11;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_eq );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_eq, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_8 = tmp_import_from_2__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_length_eq );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_length_eq, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_9 = tmp_import_from_2__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_gram );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gram, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_10 = tmp_import_from_2__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_tag );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_tag, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_11 = tmp_import_from_2__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_is_single );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_is_single, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_12 = tmp_import_from_2__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_is_capitalized );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_is_capitalized, tmp_assign_source_17 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_13;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_941859e23d487ab2c1309773110d70db;
        tmp_globals_name_4 = (PyObject *)moduledict_grammars$name;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_DictionaryPredicate_tuple;
        tmp_level_name_4 = const_int_0;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 14;
        tmp_import_name_from_13 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_DictionaryPredicate );
        Py_DECREF( tmp_import_name_from_13 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_dictionary, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_14;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_3b0d1c4c904989a80e46e8a766322a96;
        tmp_globals_name_5 = (PyObject *)moduledict_grammars$name;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_gnc_relation_tuple;
        tmp_level_name_5 = const_int_0;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 15;
        tmp_import_name_from_14 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_gnc_relation );
        Py_DECREF( tmp_import_name_from_14 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gnc_relation, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_15;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_205c5a53f3ec252adcadf2d6b60e1eaa;
        tmp_globals_name_6 = (PyObject *)moduledict_grammars$name;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_load_dict_tuple;
        tmp_level_name_6 = const_int_0;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 17;
        tmp_import_name_from_15 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_load_dict );
        Py_DECREF( tmp_import_name_from_15 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_load_dict, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_16;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_ece91ea33402621841e0e9196aa27a3d;
        tmp_globals_name_7 = (PyObject *)moduledict_grammars$name;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_RuleTransformator_tuple;
        tmp_level_name_7 = const_int_0;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 19;
        tmp_import_name_from_16 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_RuleTransformator );
        Py_DECREF( tmp_import_name_from_16 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_RuleTransformator, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_17;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_da8e2b182ea1b8ec695e560d19a11118;
        tmp_globals_name_8 = (PyObject *)moduledict_grammars$name;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_Rule_tuple;
        tmp_level_name_8 = const_int_0;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 20;
        tmp_import_name_from_17 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_Rule );
        Py_DECREF( tmp_import_name_from_17 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_Rule, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_18;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_66728ae14f823f921e32c1bfb6975ec9;
        tmp_globals_name_9 = (PyObject *)moduledict_grammars$name;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_AndPredicate_tuple;
        tmp_level_name_9 = const_int_0;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 21;
        tmp_import_name_from_18 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_AndPredicate );
        Py_DECREF( tmp_import_name_from_18 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_AndPredicate, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_call_arg_element_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_fact );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fact );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fact" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 24;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_call_arg_element_1 = const_str_plain_Name;
        tmp_call_arg_element_2 = LIST_COPY( const_list_469fa93c52dcace8522b7619f9316836_list );
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 24;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
            tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_Name, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_set_arg_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_load_dict );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_load_dict );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "load_dict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 30;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_4;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 30;
        tmp_set_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_digest_36abc8afa39dea317e9ff44ac7f08e5d_tuple, 0 ) );

        if ( tmp_set_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_25 = PySet_New( tmp_set_arg_1 );
        Py_DECREF( tmp_set_arg_1 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_DICT, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_set_arg_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_load_dict );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_load_dict );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "load_dict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 31;

            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_5;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 31;
        tmp_set_arg_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_str_digest_ada8f42db66cef8a7d7a1e3d7384891a_tuple, 0 ) );

        if ( tmp_set_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_26 = PySet_New( tmp_set_arg_2 );
        Py_DECREF( tmp_set_arg_2 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_MAYBE_FIRST_DICT, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_set_arg_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_load_dict );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_load_dict );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "load_dict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 32;

            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_6;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 32;
        tmp_set_arg_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_str_digest_4fc2c0763b0cc54c1f0bea0c0c10e5d4_tuple, 0 ) );

        if ( tmp_set_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_27 = PySet_New( tmp_set_arg_3 );
        Py_DECREF( tmp_set_arg_3 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_DICT, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;

            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_7;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_DICT );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST_DICT );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST_DICT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_8;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 42;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_IN_FIRST, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_9;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_MAYBE_FIRST_DICT );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MAYBE_FIRST_DICT );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MAYBE_FIRST_DICT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_10;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 43;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_IN_MAYBE_FIRST, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;

            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_11;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_DICT );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST_DICT );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST_DICT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = tmp_mvar_value_12;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 44;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_30 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_IN_LAST, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gnc_relation );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc_relation );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_13;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 46;
        tmp_assign_source_31 = CALL_FUNCTION_NO_ARGS( tmp_called_name_8 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gnc, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_14;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_is_capitalized );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_capitalized );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_capitalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 56;

            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_14;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 56;
        tmp_assign_source_32 = CALL_FUNCTION_NO_ARGS( tmp_called_name_9 );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_TITLE, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_15;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;

            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_15;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 58;
        tmp_assign_source_33 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, &PyTuple_GET_ITEM( const_tuple_str_plain_NOUN_tuple, 0 ) );

        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_NOUN, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_16;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_tag );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tag );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tag" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 59;

            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_16;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 59;
        tmp_assign_source_34 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_str_plain_I_tuple, 0 ) );

        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_NAME_CRF, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_17;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_17;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 61;
        tmp_assign_source_35 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_str_plain_Abbr_tuple, 0 ) );

        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_ABBR, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_called_name_13;
        PyObject *tmp_mvar_value_18;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }

        tmp_called_name_13 = tmp_mvar_value_18;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 62;
        tmp_assign_source_36 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_plain_Surn_tuple, 0 ) );

        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_SURN, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_mvar_value_22;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_19;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_20;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 64;
        tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_plain_Name_tuple, 0 ) );

        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_not_ );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_not_ );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_args_element_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "not_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 65;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_21;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_ABBR );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ABBR );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_args_element_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 65;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_6 = tmp_mvar_value_22;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 65;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_4 );

            exception_lineno = 65;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 63;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_assign_source_37 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_NAME, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_called_name_19;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_mvar_value_26;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_23 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_23;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_24 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 68;

            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_24;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 68;
        tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_str_plain_Patr_tuple, 0 ) );

        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_not_ );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_not_ );
        }

        if ( tmp_mvar_value_25 == NULL )
        {
            Py_DECREF( tmp_args_element_name_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "not_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }

        tmp_called_name_19 = tmp_mvar_value_25;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_ABBR );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ABBR );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_args_element_name_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_9 = tmp_mvar_value_26;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 69;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_args_element_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
        }

        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_7 );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_assign_source_38 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_17, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_PATR, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_called_name_20;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_21;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_name_23;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_mvar_value_34;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_27 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_27;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_NAME_CRF );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAME_CRF );
        }

        if ( tmp_mvar_value_28 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAME_CRF" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_10 = tmp_mvar_value_28;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_29 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 74;

            goto frame_exception_exit_1;
        }

        tmp_called_name_23 = tmp_mvar_value_29;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_NAME );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAME );
        }

        if ( tmp_mvar_value_30 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 75;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_12 = tmp_mvar_value_30;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_IN_MAYBE_FIRST );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IN_MAYBE_FIRST );
        }

        if ( tmp_mvar_value_31 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IN_MAYBE_FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_13 = tmp_mvar_value_31;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_IN_FIRST );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IN_FIRST );
        }

        if ( tmp_mvar_value_32 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IN_FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_14 = tmp_mvar_value_32;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 74;
        {
            PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
            tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_23, call_args );
        }

        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11 };
            tmp_source_name_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        tmp_called_name_21 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_called_name_21 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_33;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_first );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_21 );

            exception_lineno = 80;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 80;
        tmp_args_element_name_15 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_21 );

            exception_lineno = 80;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_source_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, call_args );
        }

        Py_DECREF( tmp_called_name_21 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        tmp_called_name_20 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_match );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_called_name_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_16 = tmp_mvar_value_34;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_assign_source_39 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
        }

        Py_DECREF( tmp_called_name_20 );
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_called_name_24;
        PyObject *tmp_source_name_4;
        PyObject *tmp_called_name_25;
        PyObject *tmp_source_name_5;
        PyObject *tmp_called_name_26;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_mvar_value_39;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_35 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;

            goto frame_exception_exit_1;
        }

        tmp_called_name_26 = tmp_mvar_value_35;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_ABBR );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ABBR );
        }

        if ( tmp_mvar_value_36 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 84;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_17 = tmp_mvar_value_36;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_TITLE );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TITLE );
        }

        if ( tmp_mvar_value_37 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TITLE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_18 = tmp_mvar_value_37;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 83;
        {
            PyObject *call_args[] = { tmp_args_element_name_17, tmp_args_element_name_18 };
            tmp_source_name_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_26, call_args );
        }

        if ( tmp_source_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        tmp_called_name_25 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_5 );
        if ( tmp_called_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_38 == NULL )
        {
            Py_DECREF( tmp_called_name_25 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 87;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_38;
        tmp_args_element_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_first );
        if ( tmp_args_element_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_25 );

            exception_lineno = 87;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 83;
        {
            PyObject *call_args[] = { tmp_args_element_name_19 };
            tmp_source_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, call_args );
        }

        Py_DECREF( tmp_called_name_25 );
        Py_DECREF( tmp_args_element_name_19 );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        tmp_called_name_24 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_match );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_called_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_39 == NULL )
        {
            Py_DECREF( tmp_called_name_24 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 88;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_20 = tmp_mvar_value_39;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 83;
        {
            PyObject *call_args[] = { tmp_args_element_name_20 };
            tmp_assign_source_40 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, call_args );
        }

        Py_DECREF( tmp_called_name_24 );
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_ABBR, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_called_name_27;
        PyObject *tmp_source_name_7;
        PyObject *tmp_called_name_28;
        PyObject *tmp_source_name_8;
        PyObject *tmp_called_name_29;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_args_element_name_24;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_args_element_name_25;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_mvar_value_46;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_40 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 98;

            goto frame_exception_exit_1;
        }

        tmp_called_name_29 = tmp_mvar_value_40;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_NAME_CRF );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAME_CRF );
        }

        if ( tmp_mvar_value_41 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAME_CRF" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_21 = tmp_mvar_value_41;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_42 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;

            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_42;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_SURN );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SURN );
        }

        if ( tmp_mvar_value_43 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SURN" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_23 = tmp_mvar_value_43;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_IN_LAST );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IN_LAST );
        }

        if ( tmp_mvar_value_44 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IN_LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_24 = tmp_mvar_value_44;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_args_element_name_23, tmp_args_element_name_24 };
            tmp_args_element_name_22 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_30, call_args );
        }

        if ( tmp_args_element_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_21, tmp_args_element_name_22 };
            tmp_source_name_8 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_29, call_args );
        }

        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_source_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;

            goto frame_exception_exit_1;
        }
        tmp_called_name_28 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_8 );
        if ( tmp_called_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_45 == NULL )
        {
            Py_DECREF( tmp_called_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_45;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_last );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 105;
        tmp_args_element_name_25 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_args_element_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_25 };
            tmp_source_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
        }

        Py_DECREF( tmp_called_name_28 );
        Py_DECREF( tmp_args_element_name_25 );
        if ( tmp_source_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;

            goto frame_exception_exit_1;
        }
        tmp_called_name_27 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_match );
        Py_DECREF( tmp_source_name_7 );
        if ( tmp_called_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_46 == NULL )
        {
            Py_DECREF( tmp_called_name_27 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_26 = tmp_mvar_value_46;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_26 };
            tmp_assign_source_41 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, call_args );
        }

        Py_DECREF( tmp_called_name_27 );
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_called_name_31;
        PyObject *tmp_source_name_10;
        PyObject *tmp_called_name_32;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_args_element_name_27;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_args_element_name_28;
        PyObject *tmp_mvar_value_49;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_PATR );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PATR );
        }

        if ( tmp_mvar_value_47 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PATR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 116;

            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_47;
        tmp_called_name_32 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_interpretation );
        if ( tmp_called_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_48 == NULL )
        {
            Py_DECREF( tmp_called_name_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;

            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_48;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_middle );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_32 );

            exception_lineno = 117;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 117;
        tmp_args_element_name_27 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_args_element_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_32 );

            exception_lineno = 117;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 116;
        {
            PyObject *call_args[] = { tmp_args_element_name_27 };
            tmp_source_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_32, call_args );
        }

        Py_DECREF( tmp_called_name_32 );
        Py_DECREF( tmp_args_element_name_27 );
        if ( tmp_source_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;

            goto frame_exception_exit_1;
        }
        tmp_called_name_31 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_match );
        Py_DECREF( tmp_source_name_10 );
        if ( tmp_called_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_49 == NULL )
        {
            Py_DECREF( tmp_called_name_31 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_28 = tmp_mvar_value_49;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 116;
        {
            PyObject *call_args[] = { tmp_args_element_name_28 };
            tmp_assign_source_42 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, call_args );
        }

        Py_DECREF( tmp_called_name_31 );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_MIDDLE, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_called_name_33;
        PyObject *tmp_source_name_13;
        PyObject *tmp_called_name_34;
        PyObject *tmp_source_name_14;
        PyObject *tmp_called_name_35;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_args_element_name_29;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_args_element_name_30;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_args_element_name_31;
        PyObject *tmp_source_name_15;
        PyObject *tmp_mvar_value_53;
        PyObject *tmp_args_element_name_32;
        PyObject *tmp_mvar_value_54;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_50 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;

            goto frame_exception_exit_1;
        }

        tmp_called_name_35 = tmp_mvar_value_50;
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_ABBR );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ABBR );
        }

        if ( tmp_mvar_value_51 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_29 = tmp_mvar_value_51;
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_TITLE );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TITLE );
        }

        if ( tmp_mvar_value_52 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TITLE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_30 = tmp_mvar_value_52;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 120;
        {
            PyObject *call_args[] = { tmp_args_element_name_29, tmp_args_element_name_30 };
            tmp_source_name_14 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_35, call_args );
        }

        if ( tmp_source_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;

            goto frame_exception_exit_1;
        }
        tmp_called_name_34 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_14 );
        if ( tmp_called_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_53 == NULL )
        {
            Py_DECREF( tmp_called_name_34 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }

        tmp_source_name_15 = tmp_mvar_value_53;
        tmp_args_element_name_31 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_middle );
        if ( tmp_args_element_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_34 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 120;
        {
            PyObject *call_args[] = { tmp_args_element_name_31 };
            tmp_source_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, call_args );
        }

        Py_DECREF( tmp_called_name_34 );
        Py_DECREF( tmp_args_element_name_31 );
        if ( tmp_source_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;

            goto frame_exception_exit_1;
        }
        tmp_called_name_33 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_match );
        Py_DECREF( tmp_source_name_13 );
        if ( tmp_called_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_54 == NULL )
        {
            Py_DECREF( tmp_called_name_33 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_32 = tmp_mvar_value_54;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 120;
        {
            PyObject *call_args[] = { tmp_args_element_name_32 };
            tmp_assign_source_43 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_33, call_args );
        }

        Py_DECREF( tmp_called_name_33 );
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_MIDDLE_ABBR, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_called_name_36;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_args_element_name_33;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_args_element_name_34;
        PyObject *tmp_mvar_value_57;
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_55 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;

            goto frame_exception_exit_1;
        }

        tmp_called_name_36 = tmp_mvar_value_55;
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST );
        }

        if ( tmp_mvar_value_56 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_33 = tmp_mvar_value_56;
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST );
        }

        if ( tmp_mvar_value_57 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 137;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_34 = tmp_mvar_value_57;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 135;
        {
            PyObject *call_args[] = { tmp_args_element_name_33, tmp_args_element_name_34 };
            tmp_assign_source_44 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_36, call_args );
        }

        if ( tmp_assign_source_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_LAST, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_called_name_37;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_args_element_name_35;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_args_element_name_36;
        PyObject *tmp_mvar_value_60;
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_58 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;

            goto frame_exception_exit_1;
        }

        tmp_called_name_37 = tmp_mvar_value_58;
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST );
        }

        if ( tmp_mvar_value_59 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 141;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_35 = tmp_mvar_value_59;
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST );
        }

        if ( tmp_mvar_value_60 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_36 = tmp_mvar_value_60;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 140;
        {
            PyObject *call_args[] = { tmp_args_element_name_35, tmp_args_element_name_36 };
            tmp_assign_source_45 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_37, call_args );
        }

        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_FIRST, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_called_name_38;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_args_element_name_37;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_args_element_name_38;
        PyObject *tmp_args_element_name_39;
        PyObject *tmp_mvar_value_63;
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_61 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 153;

            goto frame_exception_exit_1;
        }

        tmp_called_name_38 = tmp_mvar_value_61;
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_ABBR );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST_ABBR );
        }

        if ( tmp_mvar_value_62 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST_ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 154;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_37 = tmp_mvar_value_62;
        tmp_args_element_name_38 = const_str_dot;
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST );
        }

        if ( tmp_mvar_value_63 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_39 = tmp_mvar_value_63;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 153;
        {
            PyObject *call_args[] = { tmp_args_element_name_37, tmp_args_element_name_38, tmp_args_element_name_39 };
            tmp_assign_source_46 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_38, call_args );
        }

        if ( tmp_assign_source_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_ABBR_FIRST_LAST, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_called_name_39;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_args_element_name_40;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_args_element_name_41;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_args_element_name_42;
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_64 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }

        tmp_called_name_39 = tmp_mvar_value_64;
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST );
        }

        if ( tmp_mvar_value_65 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_40 = tmp_mvar_value_65;
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_ABBR );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST_ABBR );
        }

        if ( tmp_mvar_value_66 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST_ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 161;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_41 = tmp_mvar_value_66;
        tmp_args_element_name_42 = const_str_dot;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 159;
        {
            PyObject *call_args[] = { tmp_args_element_name_40, tmp_args_element_name_41, tmp_args_element_name_42 };
            tmp_assign_source_47 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_39, call_args );
        }

        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_ABBR_FIRST, tmp_assign_source_47 );
    }
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_called_name_40;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_args_element_name_43;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_args_element_name_44;
        PyObject *tmp_args_element_name_45;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_args_element_name_46;
        PyObject *tmp_args_element_name_47;
        PyObject *tmp_mvar_value_70;
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_67 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }

        tmp_called_name_40 = tmp_mvar_value_67;
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_ABBR );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST_ABBR );
        }

        if ( tmp_mvar_value_68 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST_ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_43 = tmp_mvar_value_68;
        tmp_args_element_name_44 = const_str_dot;
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_MIDDLE_ABBR );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MIDDLE_ABBR );
        }

        if ( tmp_mvar_value_69 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MIDDLE_ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 168;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_45 = tmp_mvar_value_69;
        tmp_args_element_name_46 = const_str_dot;
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST );
        }

        if ( tmp_mvar_value_70 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 170;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_47 = tmp_mvar_value_70;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 165;
        {
            PyObject *call_args[] = { tmp_args_element_name_43, tmp_args_element_name_44, tmp_args_element_name_45, tmp_args_element_name_46, tmp_args_element_name_47 };
            tmp_assign_source_48 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_40, call_args );
        }

        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_ABBR_FIRST_MIDDLE_LAST, tmp_assign_source_48 );
    }
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_called_name_41;
        PyObject *tmp_mvar_value_71;
        PyObject *tmp_args_element_name_48;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_args_element_name_49;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_args_element_name_50;
        PyObject *tmp_args_element_name_51;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_args_element_name_52;
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_71 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }

        tmp_called_name_41 = tmp_mvar_value_71;
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST );
        }

        if ( tmp_mvar_value_72 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_48 = tmp_mvar_value_72;
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_ABBR );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST_ABBR );
        }

        if ( tmp_mvar_value_73 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST_ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_49 = tmp_mvar_value_73;
        tmp_args_element_name_50 = const_str_dot;
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_MIDDLE_ABBR );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MIDDLE_ABBR );
        }

        if ( tmp_mvar_value_74 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MIDDLE_ABBR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 177;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_51 = tmp_mvar_value_74;
        tmp_args_element_name_52 = const_str_dot;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 173;
        {
            PyObject *call_args[] = { tmp_args_element_name_48, tmp_args_element_name_49, tmp_args_element_name_50, tmp_args_element_name_51, tmp_args_element_name_52 };
            tmp_assign_source_49 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_41, call_args );
        }

        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_ABBR_FIRST_MIDDLE, tmp_assign_source_49 );
    }
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_called_name_42;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_args_element_name_53;
        PyObject *tmp_mvar_value_76;
        PyObject *tmp_args_element_name_54;
        PyObject *tmp_mvar_value_77;
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_75 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;

            goto frame_exception_exit_1;
        }

        tmp_called_name_42 = tmp_mvar_value_75;
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST );
        }

        if ( tmp_mvar_value_76 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 190;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_53 = tmp_mvar_value_76;
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_MIDDLE );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MIDDLE );
        }

        if ( tmp_mvar_value_77 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MIDDLE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 191;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_54 = tmp_mvar_value_77;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 189;
        {
            PyObject *call_args[] = { tmp_args_element_name_53, tmp_args_element_name_54 };
            tmp_assign_source_50 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_42, call_args );
        }

        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_MIDDLE, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_called_name_43;
        PyObject *tmp_mvar_value_78;
        PyObject *tmp_args_element_name_55;
        PyObject *tmp_mvar_value_79;
        PyObject *tmp_args_element_name_56;
        PyObject *tmp_mvar_value_80;
        PyObject *tmp_args_element_name_57;
        PyObject *tmp_mvar_value_81;
        tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_78 == NULL ))
        {
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_78 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 194;

            goto frame_exception_exit_1;
        }

        tmp_called_name_43 = tmp_mvar_value_78;
        tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST );

        if (unlikely( tmp_mvar_value_79 == NULL ))
        {
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST );
        }

        if ( tmp_mvar_value_79 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 195;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_55 = tmp_mvar_value_79;
        tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_MIDDLE );

        if (unlikely( tmp_mvar_value_80 == NULL ))
        {
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MIDDLE );
        }

        if ( tmp_mvar_value_80 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MIDDLE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 196;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_56 = tmp_mvar_value_80;
        tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST );

        if (unlikely( tmp_mvar_value_81 == NULL ))
        {
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST );
        }

        if ( tmp_mvar_value_81 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_57 = tmp_mvar_value_81;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 194;
        {
            PyObject *call_args[] = { tmp_args_element_name_55, tmp_args_element_name_56, tmp_args_element_name_57 };
            tmp_assign_source_51 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_43, call_args );
        }

        if ( tmp_assign_source_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_MIDDLE_LAST, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        PyObject *tmp_called_name_44;
        PyObject *tmp_mvar_value_82;
        PyObject *tmp_args_element_name_58;
        PyObject *tmp_mvar_value_83;
        PyObject *tmp_args_element_name_59;
        PyObject *tmp_mvar_value_84;
        PyObject *tmp_args_element_name_60;
        PyObject *tmp_mvar_value_85;
        tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_82 == NULL ))
        {
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_82 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 200;

            goto frame_exception_exit_1;
        }

        tmp_called_name_44 = tmp_mvar_value_82;
        tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST );

        if (unlikely( tmp_mvar_value_83 == NULL ))
        {
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST );
        }

        if ( tmp_mvar_value_83 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 201;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_58 = tmp_mvar_value_83;
        tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST );

        if (unlikely( tmp_mvar_value_84 == NULL ))
        {
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST );
        }

        if ( tmp_mvar_value_84 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 202;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_59 = tmp_mvar_value_84;
        tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_MIDDLE );

        if (unlikely( tmp_mvar_value_85 == NULL ))
        {
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MIDDLE );
        }

        if ( tmp_mvar_value_85 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MIDDLE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 203;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_60 = tmp_mvar_value_85;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 200;
        {
            PyObject *call_args[] = { tmp_args_element_name_58, tmp_args_element_name_59, tmp_args_element_name_60 };
            tmp_assign_source_52 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_44, call_args );
        }

        if ( tmp_assign_source_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 200;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_FIRST_MIDDLE, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        PyObject *tmp_mvar_value_86;
        tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST );

        if (unlikely( tmp_mvar_value_86 == NULL ))
        {
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST );
        }

        if ( tmp_mvar_value_86 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 214;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_53 = tmp_mvar_value_86;
        UPDATE_STRING_DICT0( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_JUST_FIRST, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        PyObject *tmp_mvar_value_87;
        tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST );

        if (unlikely( tmp_mvar_value_87 == NULL ))
        {
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST );
        }

        if ( tmp_mvar_value_87 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 216;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_54 = tmp_mvar_value_87;
        UPDATE_STRING_DICT0( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_JUST_LAST, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_called_name_45;
        PyObject *tmp_source_name_16;
        PyObject *tmp_called_name_46;
        PyObject *tmp_mvar_value_88;
        PyObject *tmp_args_element_name_61;
        PyObject *tmp_mvar_value_89;
        PyObject *tmp_args_element_name_62;
        PyObject *tmp_mvar_value_90;
        PyObject *tmp_args_element_name_63;
        PyObject *tmp_mvar_value_91;
        PyObject *tmp_args_element_name_64;
        PyObject *tmp_mvar_value_92;
        PyObject *tmp_args_element_name_65;
        PyObject *tmp_mvar_value_93;
        PyObject *tmp_args_element_name_66;
        PyObject *tmp_mvar_value_94;
        PyObject *tmp_args_element_name_67;
        PyObject *tmp_mvar_value_95;
        PyObject *tmp_args_element_name_68;
        PyObject *tmp_mvar_value_96;
        PyObject *tmp_args_element_name_69;
        PyObject *tmp_mvar_value_97;
        PyObject *tmp_args_element_name_70;
        PyObject *tmp_mvar_value_98;
        PyObject *tmp_args_element_name_71;
        PyObject *tmp_mvar_value_99;
        PyObject *tmp_args_element_name_72;
        PyObject *tmp_mvar_value_100;
        tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_88 == NULL ))
        {
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_88 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 226;

            goto frame_exception_exit_1;
        }

        tmp_called_name_46 = tmp_mvar_value_88;
        tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_LAST );

        if (unlikely( tmp_mvar_value_89 == NULL ))
        {
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST_LAST );
        }

        if ( tmp_mvar_value_89 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST_LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 227;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_61 = tmp_mvar_value_89;
        tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_FIRST );

        if (unlikely( tmp_mvar_value_90 == NULL ))
        {
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST_FIRST );
        }

        if ( tmp_mvar_value_90 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST_FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 228;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_62 = tmp_mvar_value_90;
        tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_ABBR_FIRST_LAST );

        if (unlikely( tmp_mvar_value_91 == NULL ))
        {
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ABBR_FIRST_LAST );
        }

        if ( tmp_mvar_value_91 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ABBR_FIRST_LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 230;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_63 = tmp_mvar_value_91;
        tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_ABBR_FIRST );

        if (unlikely( tmp_mvar_value_92 == NULL ))
        {
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST_ABBR_FIRST );
        }

        if ( tmp_mvar_value_92 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST_ABBR_FIRST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 231;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_64 = tmp_mvar_value_92;
        tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_ABBR_FIRST_MIDDLE_LAST );

        if (unlikely( tmp_mvar_value_93 == NULL ))
        {
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ABBR_FIRST_MIDDLE_LAST );
        }

        if ( tmp_mvar_value_93 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ABBR_FIRST_MIDDLE_LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 232;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_65 = tmp_mvar_value_93;
        tmp_mvar_value_94 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_ABBR_FIRST_MIDDLE );

        if (unlikely( tmp_mvar_value_94 == NULL ))
        {
            tmp_mvar_value_94 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST_ABBR_FIRST_MIDDLE );
        }

        if ( tmp_mvar_value_94 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LAST_ABBR_FIRST_MIDDLE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 233;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_66 = tmp_mvar_value_94;
        tmp_mvar_value_95 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_MIDDLE );

        if (unlikely( tmp_mvar_value_95 == NULL ))
        {
            tmp_mvar_value_95 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST_MIDDLE );
        }

        if ( tmp_mvar_value_95 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST_MIDDLE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 235;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_67 = tmp_mvar_value_95;
        tmp_mvar_value_96 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_FIRST_MIDDLE_LAST );

        if (unlikely( tmp_mvar_value_96 == NULL ))
        {
            tmp_mvar_value_96 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FIRST_MIDDLE_LAST );
        }

        if ( tmp_mvar_value_96 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FIRST_MIDDLE_LAST" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 236;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_68 = tmp_mvar_value_96;
        tmp_mvar_value_97 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_LAST_FIRST_MIDDLE );

        if (unlikely( tmp_mvar_value_97 == NULL ))
        {
            tmp_mvar_value_97 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LAST_FIRST_MIDDLE );
        }

        CHECK_OBJECT( tmp_mvar_value_97 );
        tmp_args_element_name_69 = tmp_mvar_value_97;
        tmp_mvar_value_98 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_JUST_FIRST );

        if (unlikely( tmp_mvar_value_98 == NULL ))
        {
            tmp_mvar_value_98 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_JUST_FIRST );
        }

        CHECK_OBJECT( tmp_mvar_value_98 );
        tmp_args_element_name_70 = tmp_mvar_value_98;
        tmp_mvar_value_99 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_JUST_LAST );

        if (unlikely( tmp_mvar_value_99 == NULL ))
        {
            tmp_mvar_value_99 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_JUST_LAST );
        }

        CHECK_OBJECT( tmp_mvar_value_99 );
        tmp_args_element_name_71 = tmp_mvar_value_99;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 226;
        {
            PyObject *call_args[] = { tmp_args_element_name_61, tmp_args_element_name_62, tmp_args_element_name_63, tmp_args_element_name_64, tmp_args_element_name_65, tmp_args_element_name_66, tmp_args_element_name_67, tmp_args_element_name_68, tmp_args_element_name_69, tmp_args_element_name_70, tmp_args_element_name_71 };
            tmp_source_name_16 = CALL_FUNCTION_WITH_ARGS11( tmp_called_name_46, call_args );
        }

        if ( tmp_source_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;

            goto frame_exception_exit_1;
        }
        tmp_called_name_45 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_16 );
        if ( tmp_called_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_100 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_100 == NULL ))
        {
            tmp_mvar_value_100 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_100 == NULL )
        {
            Py_DECREF( tmp_called_name_45 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 242;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_72 = tmp_mvar_value_100;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 226;
        {
            PyObject *call_args[] = { tmp_args_element_name_72 };
            tmp_assign_source_55 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_45, call_args );
        }

        Py_DECREF( tmp_called_name_45 );
        if ( tmp_assign_source_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_NAME, tmp_assign_source_55 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_56;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_101;
        tmp_mvar_value_101 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_RuleTransformator );

        if (unlikely( tmp_mvar_value_101 == NULL ))
        {
            tmp_mvar_value_101 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RuleTransformator );
        }

        if ( tmp_mvar_value_101 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RuleTransformator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 246;

            goto try_except_handler_3;
        }

        tmp_tuple_element_1 = tmp_mvar_value_101;
        tmp_assign_source_56 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_56, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_56;
    }
    {
        PyObject *tmp_assign_source_57;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_57 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_57;
    }
    {
        PyObject *tmp_assign_source_58;
        tmp_assign_source_58 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_58;
    }
    {
        PyObject *tmp_assign_source_59;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_3;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_3;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_3;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_59 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_59;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_3;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_17;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_17 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_17, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_60;
            PyObject *tmp_called_name_47;
            PyObject *tmp_source_name_18;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_18 = tmp_class_creation_1__metaclass;
            tmp_called_name_47 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain___prepare__ );
            if ( tmp_called_name_47 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 246;

                goto try_except_handler_3;
            }
            tmp_tuple_element_2 = const_str_plain_StripCrfTransformator;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 246;
            tmp_assign_source_60 = CALL_FUNCTION( tmp_called_name_47, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_47 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_60 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 246;

                goto try_except_handler_3;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_60;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_19;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_19 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_19, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 246;

                goto try_except_handler_3;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_20;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 246;

                    goto try_except_handler_3;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_20 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_20 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_20 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 246;

                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 246;

                    goto try_except_handler_3;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 246;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_3;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_61;
            tmp_assign_source_61 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_61;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_62;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_grammars$name_246 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_28ca5c1e855316b7737046f32ddceb9c;
        tmp_res = PyObject_SetItem( locals_grammars$name_246, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_plain_StripCrfTransformator;
        tmp_res = PyObject_SetItem( locals_grammars$name_246, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto try_except_handler_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_7981df3f44e238524b4b3b6e6ab8386b_2, codeobj_7981df3f44e238524b4b3b6e6ab8386b, module_grammars$name, sizeof(void *) );
        frame_7981df3f44e238524b4b3b6e6ab8386b_2 = cache_frame_7981df3f44e238524b4b3b6e6ab8386b_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_7981df3f44e238524b4b3b6e6ab8386b_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_7981df3f44e238524b4b3b6e6ab8386b_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_grammars$name$$$function_1_visit_term(  );



        tmp_res = PyObject_SetItem( locals_grammars$name_246, const_str_plain_visit_term, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_7981df3f44e238524b4b3b6e6ab8386b_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_7981df3f44e238524b4b3b6e6ab8386b_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_7981df3f44e238524b4b3b6e6ab8386b_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_7981df3f44e238524b4b3b6e6ab8386b_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_7981df3f44e238524b4b3b6e6ab8386b_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_7981df3f44e238524b4b3b6e6ab8386b_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_7981df3f44e238524b4b3b6e6ab8386b_2 == cache_frame_7981df3f44e238524b4b3b6e6ab8386b_2 )
        {
            Py_DECREF( frame_7981df3f44e238524b4b3b6e6ab8386b_2 );
        }
        cache_frame_7981df3f44e238524b4b3b6e6ab8386b_2 = NULL;

        assertFrameObject( frame_7981df3f44e238524b4b3b6e6ab8386b_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_5;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 246;

                goto try_except_handler_5;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_grammars$name_246, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 246;

                goto try_except_handler_5;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_63;
            PyObject *tmp_called_name_48;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_48 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_StripCrfTransformator;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_grammars$name_246;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 246;
            tmp_assign_source_63 = CALL_FUNCTION( tmp_called_name_48, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_63 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 246;

                goto try_except_handler_5;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_63;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_62 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_62 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$name );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        Py_DECREF( locals_grammars$name_246 );
        locals_grammars$name_246 = NULL;
        goto try_return_handler_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_grammars$name_246 );
        locals_grammars$name_246 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$name );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( grammars$name );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 246;
        goto try_except_handler_3;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_StripCrfTransformator, tmp_assign_source_62 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_64;
        PyObject *tmp_called_name_49;
        PyObject *tmp_source_name_21;
        PyObject *tmp_mvar_value_102;
        PyObject *tmp_args_element_name_73;
        PyObject *tmp_mvar_value_103;
        tmp_mvar_value_102 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_NAME );

        if (unlikely( tmp_mvar_value_102 == NULL ))
        {
            tmp_mvar_value_102 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAME );
        }

        if ( tmp_mvar_value_102 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 257;

            goto frame_exception_exit_1;
        }

        tmp_source_name_21 = tmp_mvar_value_102;
        tmp_called_name_49 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_transform );
        if ( tmp_called_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_103 = GET_STRING_DICT_VALUE( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_StripCrfTransformator );

        if (unlikely( tmp_mvar_value_103 == NULL ))
        {
            tmp_mvar_value_103 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_StripCrfTransformator );
        }

        if ( tmp_mvar_value_103 == NULL )
        {
            Py_DECREF( tmp_called_name_49 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "StripCrfTransformator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 258;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_73 = tmp_mvar_value_103;
        frame_93532fb9474d08d19660930364fca938->m_frame.f_lineno = 257;
        {
            PyObject *call_args[] = { tmp_args_element_name_73 };
            tmp_assign_source_64 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_49, call_args );
        }

        Py_DECREF( tmp_called_name_49 );
        if ( tmp_assign_source_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$name, (Nuitka_StringObject *)const_str_plain_SIMPLE_NAME, tmp_assign_source_64 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_93532fb9474d08d19660930364fca938 );
#endif
    popFrameStack();

    assertFrameObject( frame_93532fb9474d08d19660930364fca938 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_93532fb9474d08d19660930364fca938 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_93532fb9474d08d19660930364fca938, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_93532fb9474d08d19660930364fca938->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_93532fb9474d08d19660930364fca938, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_grammars$name );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
