/* Generated code for Python module 'jinja2._compat'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jinja2$_compat" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jinja2$_compat;
PyDictObject *moduledict_jinja2$_compat;

/* The declarations of module constants used, if any. */
extern PyObject *const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple;
static PyObject *const_str_plain_quote_from_bytes;
extern PyObject *const_str_plain_itertools;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_tuple_str_plain_filename_tuple;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_cPickle;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_zip;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_chr;
extern PyObject *const_tuple_str_plain_cls_tuple;
extern PyObject *const_str_plain_NativeStringIO;
static PyObject *const_str_digest_5d2ffb1908e620b66755f75dffe722b4;
extern PyObject *const_str_plain_encode;
extern PyObject *const_str_plain_implements_iterator;
extern PyObject *const_str_plain_items;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_implements_to_string;
extern PyObject *const_str_plain_filter;
extern PyObject *const_str_plain_cls;
extern PyObject *const_str_plain_next;
extern PyObject *const_str_digest_04991ea695faff4a76e4efb6a8a8593f;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_tuple_str_plain_d_tuple;
static PyObject *const_str_digest_d08ec648c9222f75f8f40823bebe3549;
extern PyObject *const_str_plain_meta;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_quote;
extern PyObject *const_str_plain_urllib;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain_PYPY;
static PyObject *const_tuple_str_plain_quote_from_bytes_tuple;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_iterkeys;
extern PyObject *const_str_plain_long;
static PyObject *const_tuple_str_plain_StringIO_str_plain_StringIO_tuple;
extern PyObject *const_str_plain_int;
extern PyObject *const_str_plain_string_types;
extern PyObject *const_str_plain_bases;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_unicode;
static PyObject *const_str_digest_4302a97a76df5c6767c7651984407c7d;
extern PyObject *const_str_plain_BytesIO;
extern PyObject *const_str_plain_iteritems;
static PyObject *const_str_plain__identity;
extern PyObject *const_str_plain_keys;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_integer_types;
extern PyObject *const_str_plain_this_bases;
extern PyObject *const_str_plain_pickle;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_izip;
extern PyObject *const_str_plain_encode_filename;
extern PyObject *const_tuple_161fc844a072f0af4aef9f4d296def14_tuple;
extern PyObject *const_str_plain_temporary_class;
extern PyObject *const_tuple_str_plain_x_tuple;
extern PyObject *const_str_plain_xrange;
extern PyObject *const_str_plain_map;
static PyObject *const_str_digest_34af662f0034f7e15e1b09ae3898b2f2;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
extern PyObject *const_tuple_type_type_tuple;
extern PyObject *const_str_plain___new__;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_tb;
extern PyObject *const_str_digest_1d2e267f2ccdeb84fbf4cb4191414e9f;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_x;
extern PyObject *const_tuple_type_int_tuple;
extern PyObject *const_str_plain_exec;
extern PyObject *const_tuple_str_plain_quote_tuple;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_digest_3b1b70a1c7fd81346af552ff341f9b33;
extern PyObject *const_str_plain___unicode__;
extern PyObject *const_str_plain_d;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_imap;
extern PyObject *const_str_plain_range_type;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_plain_pypy_translation_info;
extern PyObject *const_str_plain___traceback__;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_PY2;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain___str__;
extern PyObject *const_str_plain_tp;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_io;
static PyObject *const_str_digest_0d6ce0117d1ae67c31c295ce5a8ff6c0;
extern PyObject *const_str_plain_filename;
extern PyObject *const_str_plain_StringIO;
extern PyObject *const_str_plain_text_type;
extern PyObject *const_str_plain_range;
extern PyObject *const_str_plain___next__;
extern PyObject *const_str_plain_intern;
extern PyObject *const_str_plain_values;
extern PyObject *const_tuple_str_plain_tp_str_plain_value_str_plain_tb_tuple;
extern PyObject *const_str_plain_with_traceback;
static PyObject *const_tuple_str_plain_BytesIO_str_plain_StringIO_tuple;
static PyObject *const_tuple_str_plain_imap_str_plain_izip_str_plain_ifilter_tuple;
extern PyObject *const_str_plain_unichr;
extern PyObject *const_tuple_str_plain_meta_str_plain_bases_str_plain_metaclass_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_itervalues;
extern PyObject *const_str_plain_version_info;
extern PyObject *const_str_plain_cStringIO;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_digest_7b0ddbb29047de4d3d5b1987c8f9a853;
extern PyObject *const_str_plain_url_quote;
extern PyObject *const_str_angle_string;
extern PyObject *const_tuple_type_str_tuple;
extern PyObject *const_str_plain_reraise;
extern PyObject *const_str_plain_ifilter;
extern PyObject *const_str_plain_with_metaclass;
extern PyObject *const_str_digest_124f1473eafa684c185fd606074efc0f;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_quote_from_bytes = UNSTREAM_STRING_ASCII( &constant_bin[ 1028596 ], 16, 1 );
    const_str_digest_5d2ffb1908e620b66755f75dffe722b4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1028612 ], 38, 0 );
    const_str_digest_d08ec648c9222f75f8f40823bebe3549 = UNSTREAM_STRING_ASCII( &constant_bin[ 1028650 ], 53, 0 );
    const_tuple_str_plain_quote_from_bytes_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_quote_from_bytes_tuple, 0, const_str_plain_quote_from_bytes ); Py_INCREF( const_str_plain_quote_from_bytes );
    const_tuple_str_plain_StringIO_str_plain_StringIO_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_StringIO_str_plain_StringIO_tuple, 0, const_str_plain_StringIO ); Py_INCREF( const_str_plain_StringIO );
    PyTuple_SET_ITEM( const_tuple_str_plain_StringIO_str_plain_StringIO_tuple, 1, const_str_plain_StringIO ); Py_INCREF( const_str_plain_StringIO );
    const_str_digest_4302a97a76df5c6767c7651984407c7d = UNSTREAM_STRING_ASCII( &constant_bin[ 1028703 ], 291, 0 );
    const_str_plain__identity = UNSTREAM_STRING_ASCII( &constant_bin[ 1028994 ], 9, 1 );
    const_str_digest_34af662f0034f7e15e1b09ae3898b2f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1029003 ], 17, 0 );
    const_str_plain_pypy_translation_info = UNSTREAM_STRING_ASCII( &constant_bin[ 1029020 ], 21, 1 );
    const_str_digest_0d6ce0117d1ae67c31c295ce5a8ff6c0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1029041 ], 23, 0 );
    const_tuple_str_plain_BytesIO_str_plain_StringIO_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_BytesIO_str_plain_StringIO_tuple, 0, const_str_plain_BytesIO ); Py_INCREF( const_str_plain_BytesIO );
    PyTuple_SET_ITEM( const_tuple_str_plain_BytesIO_str_plain_StringIO_tuple, 1, const_str_plain_StringIO ); Py_INCREF( const_str_plain_StringIO );
    const_tuple_str_plain_imap_str_plain_izip_str_plain_ifilter_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_imap_str_plain_izip_str_plain_ifilter_tuple, 0, const_str_plain_imap ); Py_INCREF( const_str_plain_imap );
    PyTuple_SET_ITEM( const_tuple_str_plain_imap_str_plain_izip_str_plain_ifilter_tuple, 1, const_str_plain_izip ); Py_INCREF( const_str_plain_izip );
    PyTuple_SET_ITEM( const_tuple_str_plain_imap_str_plain_izip_str_plain_ifilter_tuple, 2, const_str_plain_ifilter ); Py_INCREF( const_str_plain_ifilter );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jinja2$_compat( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_3603369d093afe9ccac09e1a678c49cb;
static PyCodeObject *codeobj_7020809c07687b28e2dc1a941256c86f;
static PyCodeObject *codeobj_915a2afec675a666b1376e473ac9b8c3;
static PyCodeObject *codeobj_9c96ddfee9e5827ce95117d7cdd98cb2;
static PyCodeObject *codeobj_10456fef9802c6d05f5819de7e77c5d8;
static PyCodeObject *codeobj_1aa6cbc6f58c7c1ee56c4855a4ecbe34;
static PyCodeObject *codeobj_2bcd082985528a608090f6ff22aad94c;
static PyCodeObject *codeobj_885ccd8e4969682b06fea7936240e66a;
static PyCodeObject *codeobj_043376fe1fee96ea8431d169afe72993;
static PyCodeObject *codeobj_bdcfc5b233da37e1755d2a6c851c3d1f;
static PyCodeObject *codeobj_5e35507247490c8c21695a8b41204567;
static PyCodeObject *codeobj_75d1f253c38a875b32b4f321c0fc626a;
static PyCodeObject *codeobj_695872afca5d17df2dbbbcfeb5648b2e;
static PyCodeObject *codeobj_0a52ff8fab87deb9b57f9fd5a826bc01;
static PyCodeObject *codeobj_6cea72d09512ba6d4d219b91a55df92e;
static PyCodeObject *codeobj_8519c969d12ed75d2126f20a212bb9d5;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_34af662f0034f7e15e1b09ae3898b2f2 );
    codeobj_3603369d093afe9ccac09e1a678c49cb = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 27, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7020809c07687b28e2dc1a941256c86f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 28, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_915a2afec675a666b1376e473ac9b8c3 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 29, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9c96ddfee9e5827ce95117d7cdd98cb2 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 56, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_10456fef9802c6d05f5819de7e77c5d8 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 57, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1aa6cbc6f58c7c1ee56c4855a4ecbe34 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 58, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2bcd082985528a608090f6ff22aad94c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 17, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_885ccd8e4969682b06fea7936240e66a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 76, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_043376fe1fee96ea8431d169afe72993 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_0d6ce0117d1ae67c31c295ce5a8ff6c0, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_bdcfc5b233da37e1755d2a6c851c3d1f = MAKE_CODEOBJ( module_filename_obj, const_str_plain___new__, 91, const_tuple_161fc844a072f0af4aef9f4d296def14_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_5e35507247490c8c21695a8b41204567 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_encode_filename, 79, const_tuple_str_plain_filename_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_75d1f253c38a875b32b4f321c0fc626a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_implements_iterator, 69, const_tuple_str_plain_cls_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_695872afca5d17df2dbbbcfeb5648b2e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_implements_to_string, 74, const_tuple_str_plain_cls_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0a52ff8fab87deb9b57f9fd5a826bc01 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_metaclass, 90, const_tuple_str_plain___class___tuple, 0, 0, CO_OPTIMIZED | CO_NOFREE );
    codeobj_6cea72d09512ba6d4d219b91a55df92e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reraise, 35, const_tuple_str_plain_tp_str_plain_value_str_plain_tb_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8519c969d12ed75d2126f20a212bb9d5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_with_metaclass, 85, const_tuple_str_plain_meta_str_plain_bases_str_plain_metaclass_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_10_implements_to_string(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_10_implements_to_string$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_11_encode_filename(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_12_with_metaclass(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_12_with_metaclass$$$function_1___new__(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_2_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_3_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_4_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_5_reraise( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_6_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_7_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_8_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_9_implements_iterator(  );


// The module function definitions.
static PyObject *impl_jinja2$_compat$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_x );
    tmp_return_value = par_x;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_1_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_2_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_d = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3603369d093afe9ccac09e1a678c49cb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3603369d093afe9ccac09e1a678c49cb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3603369d093afe9ccac09e1a678c49cb, codeobj_3603369d093afe9ccac09e1a678c49cb, module_jinja2$_compat, sizeof(void *) );
    frame_3603369d093afe9ccac09e1a678c49cb = cache_frame_3603369d093afe9ccac09e1a678c49cb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3603369d093afe9ccac09e1a678c49cb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3603369d093afe9ccac09e1a678c49cb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_d );
        tmp_called_instance_1 = par_d;
        frame_3603369d093afe9ccac09e1a678c49cb->m_frame.f_lineno = 27;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_keys );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3603369d093afe9ccac09e1a678c49cb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3603369d093afe9ccac09e1a678c49cb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3603369d093afe9ccac09e1a678c49cb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3603369d093afe9ccac09e1a678c49cb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3603369d093afe9ccac09e1a678c49cb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3603369d093afe9ccac09e1a678c49cb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3603369d093afe9ccac09e1a678c49cb,
        type_description_1,
        par_d
    );


    // Release cached frame.
    if ( frame_3603369d093afe9ccac09e1a678c49cb == cache_frame_3603369d093afe9ccac09e1a678c49cb )
    {
        Py_DECREF( frame_3603369d093afe9ccac09e1a678c49cb );
    }
    cache_frame_3603369d093afe9ccac09e1a678c49cb = NULL;

    assertFrameObject( frame_3603369d093afe9ccac09e1a678c49cb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_2_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_2_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_3_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_d = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7020809c07687b28e2dc1a941256c86f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7020809c07687b28e2dc1a941256c86f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7020809c07687b28e2dc1a941256c86f, codeobj_7020809c07687b28e2dc1a941256c86f, module_jinja2$_compat, sizeof(void *) );
    frame_7020809c07687b28e2dc1a941256c86f = cache_frame_7020809c07687b28e2dc1a941256c86f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7020809c07687b28e2dc1a941256c86f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7020809c07687b28e2dc1a941256c86f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_d );
        tmp_called_instance_1 = par_d;
        frame_7020809c07687b28e2dc1a941256c86f->m_frame.f_lineno = 28;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_values );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7020809c07687b28e2dc1a941256c86f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7020809c07687b28e2dc1a941256c86f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7020809c07687b28e2dc1a941256c86f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7020809c07687b28e2dc1a941256c86f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7020809c07687b28e2dc1a941256c86f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7020809c07687b28e2dc1a941256c86f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7020809c07687b28e2dc1a941256c86f,
        type_description_1,
        par_d
    );


    // Release cached frame.
    if ( frame_7020809c07687b28e2dc1a941256c86f == cache_frame_7020809c07687b28e2dc1a941256c86f )
    {
        Py_DECREF( frame_7020809c07687b28e2dc1a941256c86f );
    }
    cache_frame_7020809c07687b28e2dc1a941256c86f = NULL;

    assertFrameObject( frame_7020809c07687b28e2dc1a941256c86f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_3_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_3_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_4_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_d = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_915a2afec675a666b1376e473ac9b8c3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_915a2afec675a666b1376e473ac9b8c3 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_915a2afec675a666b1376e473ac9b8c3, codeobj_915a2afec675a666b1376e473ac9b8c3, module_jinja2$_compat, sizeof(void *) );
    frame_915a2afec675a666b1376e473ac9b8c3 = cache_frame_915a2afec675a666b1376e473ac9b8c3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_915a2afec675a666b1376e473ac9b8c3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_915a2afec675a666b1376e473ac9b8c3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_d );
        tmp_called_instance_1 = par_d;
        frame_915a2afec675a666b1376e473ac9b8c3->m_frame.f_lineno = 29;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_915a2afec675a666b1376e473ac9b8c3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_915a2afec675a666b1376e473ac9b8c3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_915a2afec675a666b1376e473ac9b8c3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_915a2afec675a666b1376e473ac9b8c3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_915a2afec675a666b1376e473ac9b8c3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_915a2afec675a666b1376e473ac9b8c3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_915a2afec675a666b1376e473ac9b8c3,
        type_description_1,
        par_d
    );


    // Release cached frame.
    if ( frame_915a2afec675a666b1376e473ac9b8c3 == cache_frame_915a2afec675a666b1376e473ac9b8c3 )
    {
        Py_DECREF( frame_915a2afec675a666b1376e473ac9b8c3 );
    }
    cache_frame_915a2afec675a666b1376e473ac9b8c3 = NULL;

    assertFrameObject( frame_915a2afec675a666b1376e473ac9b8c3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_4_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_4_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_5_reraise( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tp = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    PyObject *par_tb = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_6cea72d09512ba6d4d219b91a55df92e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6cea72d09512ba6d4d219b91a55df92e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6cea72d09512ba6d4d219b91a55df92e, codeobj_6cea72d09512ba6d4d219b91a55df92e, module_jinja2$_compat, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6cea72d09512ba6d4d219b91a55df92e = cache_frame_6cea72d09512ba6d4d219b91a55df92e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6cea72d09512ba6d4d219b91a55df92e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6cea72d09512ba6d4d219b91a55df92e ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_value );
        tmp_source_name_1 = par_value;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___traceback__ );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_tb );
        tmp_compexpr_right_1 = par_tb;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_value );
            tmp_called_instance_1 = par_value;
            CHECK_OBJECT( par_tb );
            tmp_args_element_name_1 = par_tb;
            frame_6cea72d09512ba6d4d219b91a55df92e->m_frame.f_lineno = 37;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_raise_type_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_with_traceback, call_args );
            }

            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 37;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_raise_type_2;
        CHECK_OBJECT( par_value );
        tmp_raise_type_2 = par_value;
        exception_type = tmp_raise_type_2;
        Py_INCREF( tmp_raise_type_2 );
        exception_lineno = 38;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6cea72d09512ba6d4d219b91a55df92e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6cea72d09512ba6d4d219b91a55df92e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6cea72d09512ba6d4d219b91a55df92e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6cea72d09512ba6d4d219b91a55df92e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6cea72d09512ba6d4d219b91a55df92e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6cea72d09512ba6d4d219b91a55df92e,
        type_description_1,
        par_tp,
        par_value,
        par_tb
    );


    // Release cached frame.
    if ( frame_6cea72d09512ba6d4d219b91a55df92e == cache_frame_6cea72d09512ba6d4d219b91a55df92e )
    {
        Py_DECREF( frame_6cea72d09512ba6d4d219b91a55df92e );
    }
    cache_frame_6cea72d09512ba6d4d219b91a55df92e = NULL;

    assertFrameObject( frame_6cea72d09512ba6d4d219b91a55df92e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_5_reraise );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_tp );
    Py_DECREF( par_tp );
    par_tp = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_tb );
    Py_DECREF( par_tb );
    par_tb = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_5_reraise );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_jinja2$_compat$$$function_6_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_d = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9c96ddfee9e5827ce95117d7cdd98cb2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9c96ddfee9e5827ce95117d7cdd98cb2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9c96ddfee9e5827ce95117d7cdd98cb2, codeobj_9c96ddfee9e5827ce95117d7cdd98cb2, module_jinja2$_compat, sizeof(void *) );
    frame_9c96ddfee9e5827ce95117d7cdd98cb2 = cache_frame_9c96ddfee9e5827ce95117d7cdd98cb2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9c96ddfee9e5827ce95117d7cdd98cb2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9c96ddfee9e5827ce95117d7cdd98cb2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_d );
        tmp_called_instance_1 = par_d;
        frame_9c96ddfee9e5827ce95117d7cdd98cb2->m_frame.f_lineno = 56;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_iterkeys );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9c96ddfee9e5827ce95117d7cdd98cb2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9c96ddfee9e5827ce95117d7cdd98cb2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9c96ddfee9e5827ce95117d7cdd98cb2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9c96ddfee9e5827ce95117d7cdd98cb2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9c96ddfee9e5827ce95117d7cdd98cb2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9c96ddfee9e5827ce95117d7cdd98cb2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9c96ddfee9e5827ce95117d7cdd98cb2,
        type_description_1,
        par_d
    );


    // Release cached frame.
    if ( frame_9c96ddfee9e5827ce95117d7cdd98cb2 == cache_frame_9c96ddfee9e5827ce95117d7cdd98cb2 )
    {
        Py_DECREF( frame_9c96ddfee9e5827ce95117d7cdd98cb2 );
    }
    cache_frame_9c96ddfee9e5827ce95117d7cdd98cb2 = NULL;

    assertFrameObject( frame_9c96ddfee9e5827ce95117d7cdd98cb2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_6_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_6_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_7_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_d = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_10456fef9802c6d05f5819de7e77c5d8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_10456fef9802c6d05f5819de7e77c5d8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_10456fef9802c6d05f5819de7e77c5d8, codeobj_10456fef9802c6d05f5819de7e77c5d8, module_jinja2$_compat, sizeof(void *) );
    frame_10456fef9802c6d05f5819de7e77c5d8 = cache_frame_10456fef9802c6d05f5819de7e77c5d8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_10456fef9802c6d05f5819de7e77c5d8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_10456fef9802c6d05f5819de7e77c5d8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_d );
        tmp_called_instance_1 = par_d;
        frame_10456fef9802c6d05f5819de7e77c5d8->m_frame.f_lineno = 57;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_itervalues );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_10456fef9802c6d05f5819de7e77c5d8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_10456fef9802c6d05f5819de7e77c5d8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_10456fef9802c6d05f5819de7e77c5d8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_10456fef9802c6d05f5819de7e77c5d8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_10456fef9802c6d05f5819de7e77c5d8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_10456fef9802c6d05f5819de7e77c5d8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_10456fef9802c6d05f5819de7e77c5d8,
        type_description_1,
        par_d
    );


    // Release cached frame.
    if ( frame_10456fef9802c6d05f5819de7e77c5d8 == cache_frame_10456fef9802c6d05f5819de7e77c5d8 )
    {
        Py_DECREF( frame_10456fef9802c6d05f5819de7e77c5d8 );
    }
    cache_frame_10456fef9802c6d05f5819de7e77c5d8 = NULL;

    assertFrameObject( frame_10456fef9802c6d05f5819de7e77c5d8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_7_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_7_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_8_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_d = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34, codeobj_1aa6cbc6f58c7c1ee56c4855a4ecbe34, module_jinja2$_compat, sizeof(void *) );
    frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 = cache_frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_d );
        tmp_called_instance_1 = par_d;
        frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34->m_frame.f_lineno = 58;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_iteritems );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34,
        type_description_1,
        par_d
    );


    // Release cached frame.
    if ( frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 == cache_frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 )
    {
        Py_DECREF( frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 );
    }
    cache_frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 = NULL;

    assertFrameObject( frame_1aa6cbc6f58c7c1ee56c4855a4ecbe34 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_8_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_8_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_9_implements_iterator( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_75d1f253c38a875b32b4f321c0fc626a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_75d1f253c38a875b32b4f321c0fc626a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_75d1f253c38a875b32b4f321c0fc626a, codeobj_75d1f253c38a875b32b4f321c0fc626a, module_jinja2$_compat, sizeof(void *) );
    frame_75d1f253c38a875b32b4f321c0fc626a = cache_frame_75d1f253c38a875b32b4f321c0fc626a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_75d1f253c38a875b32b4f321c0fc626a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_75d1f253c38a875b32b4f321c0fc626a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_cls );
        tmp_source_name_1 = par_cls;
        tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___next__ );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_cls );
        tmp_assattr_target_1 = par_cls;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_next, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_attrdel_target_1;
        CHECK_OBJECT( par_cls );
        tmp_attrdel_target_1 = par_cls;
        tmp_res = PyObject_DelAttr( tmp_attrdel_target_1, const_str_plain___next__ );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_75d1f253c38a875b32b4f321c0fc626a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_75d1f253c38a875b32b4f321c0fc626a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_75d1f253c38a875b32b4f321c0fc626a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_75d1f253c38a875b32b4f321c0fc626a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_75d1f253c38a875b32b4f321c0fc626a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_75d1f253c38a875b32b4f321c0fc626a,
        type_description_1,
        par_cls
    );


    // Release cached frame.
    if ( frame_75d1f253c38a875b32b4f321c0fc626a == cache_frame_75d1f253c38a875b32b4f321c0fc626a )
    {
        Py_DECREF( frame_75d1f253c38a875b32b4f321c0fc626a );
    }
    cache_frame_75d1f253c38a875b32b4f321c0fc626a = NULL;

    assertFrameObject( frame_75d1f253c38a875b32b4f321c0fc626a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_cls );
    tmp_return_value = par_cls;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_9_implements_iterator );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_9_implements_iterator );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_10_implements_to_string( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_695872afca5d17df2dbbbcfeb5648b2e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_695872afca5d17df2dbbbcfeb5648b2e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_695872afca5d17df2dbbbcfeb5648b2e, codeobj_695872afca5d17df2dbbbcfeb5648b2e, module_jinja2$_compat, sizeof(void *) );
    frame_695872afca5d17df2dbbbcfeb5648b2e = cache_frame_695872afca5d17df2dbbbcfeb5648b2e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_695872afca5d17df2dbbbcfeb5648b2e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_695872afca5d17df2dbbbcfeb5648b2e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_cls );
        tmp_source_name_1 = par_cls;
        tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___str__ );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_cls );
        tmp_assattr_target_1 = par_cls;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain___unicode__, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = MAKE_FUNCTION_jinja2$_compat$$$function_10_implements_to_string$$$function_1_lambda(  );



        CHECK_OBJECT( par_cls );
        tmp_assattr_target_2 = par_cls;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain___str__, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_695872afca5d17df2dbbbcfeb5648b2e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_695872afca5d17df2dbbbcfeb5648b2e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_695872afca5d17df2dbbbcfeb5648b2e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_695872afca5d17df2dbbbcfeb5648b2e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_695872afca5d17df2dbbbcfeb5648b2e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_695872afca5d17df2dbbbcfeb5648b2e,
        type_description_1,
        par_cls
    );


    // Release cached frame.
    if ( frame_695872afca5d17df2dbbbcfeb5648b2e == cache_frame_695872afca5d17df2dbbbcfeb5648b2e )
    {
        Py_DECREF( frame_695872afca5d17df2dbbbcfeb5648b2e );
    }
    cache_frame_695872afca5d17df2dbbbcfeb5648b2e = NULL;

    assertFrameObject( frame_695872afca5d17df2dbbbcfeb5648b2e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_cls );
    tmp_return_value = par_cls;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_10_implements_to_string );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_10_implements_to_string );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_10_implements_to_string$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_885ccd8e4969682b06fea7936240e66a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_885ccd8e4969682b06fea7936240e66a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_885ccd8e4969682b06fea7936240e66a, codeobj_885ccd8e4969682b06fea7936240e66a, module_jinja2$_compat, sizeof(void *) );
    frame_885ccd8e4969682b06fea7936240e66a = cache_frame_885ccd8e4969682b06fea7936240e66a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_885ccd8e4969682b06fea7936240e66a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_885ccd8e4969682b06fea7936240e66a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_x );
        tmp_called_instance_2 = par_x;
        frame_885ccd8e4969682b06fea7936240e66a->m_frame.f_lineno = 76;
        tmp_called_instance_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain___unicode__ );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_885ccd8e4969682b06fea7936240e66a->m_frame.f_lineno = 76;
        tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_885ccd8e4969682b06fea7936240e66a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_885ccd8e4969682b06fea7936240e66a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_885ccd8e4969682b06fea7936240e66a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_885ccd8e4969682b06fea7936240e66a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_885ccd8e4969682b06fea7936240e66a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_885ccd8e4969682b06fea7936240e66a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_885ccd8e4969682b06fea7936240e66a,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_885ccd8e4969682b06fea7936240e66a == cache_frame_885ccd8e4969682b06fea7936240e66a )
    {
        Py_DECREF( frame_885ccd8e4969682b06fea7936240e66a );
    }
    cache_frame_885ccd8e4969682b06fea7936240e66a = NULL;

    assertFrameObject( frame_885ccd8e4969682b06fea7936240e66a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_10_implements_to_string$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_10_implements_to_string$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_11_encode_filename( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_filename = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5e35507247490c8c21695a8b41204567;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_5e35507247490c8c21695a8b41204567 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5e35507247490c8c21695a8b41204567, codeobj_5e35507247490c8c21695a8b41204567, module_jinja2$_compat, sizeof(void *) );
    frame_5e35507247490c8c21695a8b41204567 = cache_frame_5e35507247490c8c21695a8b41204567;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5e35507247490c8c21695a8b41204567 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5e35507247490c8c21695a8b41204567 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_filename );
        tmp_isinstance_inst_1 = par_filename;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_unicode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_filename );
            tmp_called_instance_1 = par_filename;
            frame_5e35507247490c8c21695a8b41204567->m_frame.f_lineno = 81;
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5e35507247490c8c21695a8b41204567 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5e35507247490c8c21695a8b41204567 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5e35507247490c8c21695a8b41204567 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5e35507247490c8c21695a8b41204567, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5e35507247490c8c21695a8b41204567->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5e35507247490c8c21695a8b41204567, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5e35507247490c8c21695a8b41204567,
        type_description_1,
        par_filename
    );


    // Release cached frame.
    if ( frame_5e35507247490c8c21695a8b41204567 == cache_frame_5e35507247490c8c21695a8b41204567 )
    {
        Py_DECREF( frame_5e35507247490c8c21695a8b41204567 );
    }
    cache_frame_5e35507247490c8c21695a8b41204567 = NULL;

    assertFrameObject( frame_5e35507247490c8c21695a8b41204567 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_filename );
    tmp_return_value = par_filename;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_11_encode_filename );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_11_encode_filename );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_12_with_metaclass( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_meta = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_bases = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *var_metaclass = NULL;
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    struct Nuitka_FrameObject *frame_8519c969d12ed75d2126f20a212bb9d5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    bool tmp_result;
    PyObject *locals_jinja2$_compat$$$function_12_with_metaclass_90 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_0a52ff8fab87deb9b57f9fd5a826bc01_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_8519c969d12ed75d2126f20a212bb9d5 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8519c969d12ed75d2126f20a212bb9d5, codeobj_8519c969d12ed75d2126f20a212bb9d5, module_jinja2$_compat, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8519c969d12ed75d2126f20a212bb9d5 = cache_frame_8519c969d12ed75d2126f20a212bb9d5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8519c969d12ed75d2126f20a212bb9d5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8519c969d12ed75d2126f20a212bb9d5 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_type_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_1 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_3 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "cco";
                goto try_except_handler_2;
            }
            tmp_tuple_element_1 = const_str_plain_metaclass;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_8519c969d12ed75d2126f20a212bb9d5->m_frame.f_lineno = 90;
            tmp_assign_source_4 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "cco";
                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_4;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "cco";
                goto try_except_handler_2;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 90;
                    type_description_1 = "cco";
                    goto try_except_handler_2;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 90;
                    type_description_1 = "cco";
                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 90;
                    type_description_1 = "cco";
                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 90;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "cco";
                goto try_except_handler_2;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_5;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_6;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_jinja2$_compat$$$function_12_with_metaclass_90 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_124f1473eafa684c185fd606074efc0f;
        tmp_res = PyObject_SetItem( locals_jinja2$_compat$$$function_12_with_metaclass_90, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_digest_3b1b70a1c7fd81346af552ff341f9b33;
        tmp_res = PyObject_SetItem( locals_jinja2$_compat$$$function_12_with_metaclass_90, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto try_except_handler_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_0a52ff8fab87deb9b57f9fd5a826bc01_2, codeobj_0a52ff8fab87deb9b57f9fd5a826bc01, module_jinja2$_compat, sizeof(void *) );
        frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 = cache_frame_0a52ff8fab87deb9b57f9fd5a826bc01_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_jinja2$_compat$$$function_12_with_metaclass$$$function_1___new__(  );

        ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = par_bases;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] = par_meta;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] );


        tmp_res = PyObject_SetItem( locals_jinja2$_compat$$$function_12_with_metaclass_90, const_str_plain___new__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_0a52ff8fab87deb9b57f9fd5a826bc01_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_0a52ff8fab87deb9b57f9fd5a826bc01_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_0a52ff8fab87deb9b57f9fd5a826bc01_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_0a52ff8fab87deb9b57f9fd5a826bc01_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 == cache_frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 )
        {
            Py_DECREF( frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 );
        }
        cache_frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 = NULL;

        assertFrameObject( frame_0a52ff8fab87deb9b57f9fd5a826bc01_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "cco";
        goto try_except_handler_4;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_type_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "cco";
                goto try_except_handler_4;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_type_tuple;
            tmp_res = PyObject_SetItem( locals_jinja2$_compat$$$function_12_with_metaclass_90, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "cco";
                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_metaclass;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_jinja2$_compat$$$function_12_with_metaclass_90;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_8519c969d12ed75d2126f20a212bb9d5->m_frame.f_lineno = 90;
            tmp_assign_source_7 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "cco";
                goto try_except_handler_4;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_7;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_6 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_6 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_12_with_metaclass );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_jinja2$_compat$$$function_12_with_metaclass_90 );
        locals_jinja2$_compat$$$function_12_with_metaclass_90 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jinja2$_compat$$$function_12_with_metaclass_90 );
        locals_jinja2$_compat$$$function_12_with_metaclass_90 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_12_with_metaclass );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_12_with_metaclass );
        return NULL;
        outline_exception_1:;
        exception_lineno = 90;
        goto try_except_handler_2;
        outline_result_1:;
        assert( var_metaclass == NULL );
        var_metaclass = tmp_assign_source_6;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_called_instance_1 = (PyObject *)&PyType_Type;
        CHECK_OBJECT( var_metaclass );
        tmp_args_element_name_1 = var_metaclass;
        tmp_args_element_name_2 = const_str_plain_temporary_class;
        tmp_args_element_name_3 = const_tuple_empty;
        tmp_args_element_name_4 = PyDict_New();
        frame_8519c969d12ed75d2126f20a212bb9d5->m_frame.f_lineno = 93;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_return_value = CALL_METHOD_WITH_ARGS4( tmp_called_instance_1, const_str_plain___new__, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8519c969d12ed75d2126f20a212bb9d5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8519c969d12ed75d2126f20a212bb9d5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8519c969d12ed75d2126f20a212bb9d5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8519c969d12ed75d2126f20a212bb9d5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8519c969d12ed75d2126f20a212bb9d5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8519c969d12ed75d2126f20a212bb9d5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8519c969d12ed75d2126f20a212bb9d5,
        type_description_1,
        par_meta,
        par_bases,
        var_metaclass
    );


    // Release cached frame.
    if ( frame_8519c969d12ed75d2126f20a212bb9d5 == cache_frame_8519c969d12ed75d2126f20a212bb9d5 )
    {
        Py_DECREF( frame_8519c969d12ed75d2126f20a212bb9d5 );
    }
    cache_frame_8519c969d12ed75d2126f20a212bb9d5 = NULL;

    assertFrameObject( frame_8519c969d12ed75d2126f20a212bb9d5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_12_with_metaclass );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    CHECK_OBJECT( (PyObject *)par_bases );
    Py_DECREF( par_bases );
    par_bases = NULL;

    CHECK_OBJECT( (PyObject *)var_metaclass );
    Py_DECREF( var_metaclass );
    var_metaclass = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    CHECK_OBJECT( (PyObject *)par_bases );
    Py_DECREF( par_bases );
    par_bases = NULL;

    Py_XDECREF( var_metaclass );
    var_metaclass = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_12_with_metaclass );
    return NULL;

function_exception_exit:
    Py_XDECREF( locals_jinja2$_compat$$$function_12_with_metaclass_90 );
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.
    Py_XDECREF( locals_jinja2$_compat$$$function_12_with_metaclass_90 );


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$_compat$$$function_12_with_metaclass$$$function_1___new__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *par_this_bases = python_pars[ 2 ];
    PyObject *par_d = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_bdcfc5b233da37e1755d2a6c851c3d1f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_bdcfc5b233da37e1755d2a6c851c3d1f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bdcfc5b233da37e1755d2a6c851c3d1f, codeobj_bdcfc5b233da37e1755d2a6c851c3d1f, module_jinja2$_compat, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bdcfc5b233da37e1755d2a6c851c3d1f = cache_frame_bdcfc5b233da37e1755d2a6c851c3d1f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bdcfc5b233da37e1755d2a6c851c3d1f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bdcfc5b233da37e1755d2a6c851c3d1f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "meta" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[1] );
        CHECK_OBJECT( par_name );
        tmp_args_element_name_1 = par_name;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "bases" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_d );
        tmp_args_element_name_3 = par_d;
        frame_bdcfc5b233da37e1755d2a6c851c3d1f->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bdcfc5b233da37e1755d2a6c851c3d1f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bdcfc5b233da37e1755d2a6c851c3d1f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bdcfc5b233da37e1755d2a6c851c3d1f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bdcfc5b233da37e1755d2a6c851c3d1f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bdcfc5b233da37e1755d2a6c851c3d1f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bdcfc5b233da37e1755d2a6c851c3d1f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bdcfc5b233da37e1755d2a6c851c3d1f,
        type_description_1,
        par_cls,
        par_name,
        par_this_bases,
        par_d,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_bdcfc5b233da37e1755d2a6c851c3d1f == cache_frame_bdcfc5b233da37e1755d2a6c851c3d1f )
    {
        Py_DECREF( frame_bdcfc5b233da37e1755d2a6c851c3d1f );
    }
    cache_frame_bdcfc5b233da37e1755d2a6c851c3d1f = NULL;

    assertFrameObject( frame_bdcfc5b233da37e1755d2a6c851c3d1f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_12_with_metaclass$$$function_1___new__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_this_bases );
    Py_DECREF( par_this_bases );
    par_this_bases = NULL;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_this_bases );
    Py_DECREF( par_this_bases );
    par_this_bases = NULL;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$_compat$$$function_12_with_metaclass$$$function_1___new__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_10_implements_to_string(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_10_implements_to_string,
        const_str_plain_implements_to_string,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_695872afca5d17df2dbbbcfeb5648b2e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_10_implements_to_string$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_10_implements_to_string$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_5d2ffb1908e620b66755f75dffe722b4,
#endif
        codeobj_885ccd8e4969682b06fea7936240e66a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_11_encode_filename(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_11_encode_filename,
        const_str_plain_encode_filename,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5e35507247490c8c21695a8b41204567,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_12_with_metaclass(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_12_with_metaclass,
        const_str_plain_with_metaclass,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8519c969d12ed75d2126f20a212bb9d5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        const_str_digest_1d2e267f2ccdeb84fbf4cb4191414e9f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_12_with_metaclass$$$function_1___new__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_12_with_metaclass$$$function_1___new__,
        const_str_plain___new__,
#if PYTHON_VERSION >= 300
        const_str_digest_7b0ddbb29047de4d3d5b1987c8f9a853,
#endif
        codeobj_bdcfc5b233da37e1755d2a6c851c3d1f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2bcd082985528a608090f6ff22aad94c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_2_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_2_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3603369d093afe9ccac09e1a678c49cb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_3_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_3_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7020809c07687b28e2dc1a941256c86f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_4_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_4_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_915a2afec675a666b1376e473ac9b8c3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_5_reraise( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_5_reraise,
        const_str_plain_reraise,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6cea72d09512ba6d4d219b91a55df92e,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_6_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_6_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9c96ddfee9e5827ce95117d7cdd98cb2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_7_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_7_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_10456fef9802c6d05f5819de7e77c5d8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_8_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_8_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1aa6cbc6f58c7c1ee56c4855a4ecbe34,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$_compat$$$function_9_implements_iterator(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$_compat$$$function_9_implements_iterator,
        const_str_plain_implements_iterator,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_75d1f253c38a875b32b4f321c0fc626a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$_compat,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jinja2$_compat =
{
    PyModuleDef_HEAD_INIT,
    "jinja2._compat",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jinja2$_compat)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jinja2$_compat)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jinja2$_compat );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jinja2._compat: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jinja2._compat: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jinja2._compat: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjinja2$_compat" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jinja2$_compat = Py_InitModule4(
        "jinja2._compat",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jinja2$_compat = PyModule_Create( &mdef_jinja2$_compat );
#endif

    moduledict_jinja2$_compat = MODULE_DICT( module_jinja2$_compat );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jinja2$_compat,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jinja2$_compat,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jinja2$_compat,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jinja2$_compat,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jinja2$_compat );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_124f1473eafa684c185fd606074efc0f, module_jinja2$_compat );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_exec_call_1__globals = NULL;
    PyObject *tmp_exec_call_1__locals = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_043376fe1fee96ea8431d169afe72993;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_4302a97a76df5c6767c7651984407c7d;
        UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_043376fe1fee96ea8431d169afe72993 = MAKE_MODULE_FRAME( codeobj_043376fe1fee96ea8431d169afe72993, module_jinja2$_compat );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_043376fe1fee96ea8431d169afe72993 );
    assert( Py_REFCNT( frame_043376fe1fee96ea8431d169afe72993 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_sys;
        tmp_globals_name_1 = (PyObject *)moduledict_jinja2$_compat;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_043376fe1fee96ea8431d169afe72993->m_frame.f_lineno = 13;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        assert( !(tmp_assign_source_4 == NULL) );
        UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_version_info );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_2;
        tmp_assign_source_5 = RICH_COMPARE_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_PY2, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_attribute_name_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 16;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_4;
        tmp_attribute_name_1 = const_str_plain_pypy_translation_info;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_2, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = ( tmp_res != 0 ) ? Py_True : Py_False;
        UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_PYPY, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = MAKE_FUNCTION_jinja2$_compat$$$function_1_lambda(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain__identity, tmp_assign_source_7 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_PY2 );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY2 );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_operand_name_1 = tmp_mvar_value_5;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_8;
            tmp_assign_source_8 = LOOKUP_BUILTIN( const_str_plain_chr );
            assert( tmp_assign_source_8 != NULL );
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_unichr, tmp_assign_source_8 );
        }
        {
            PyObject *tmp_assign_source_9;
            tmp_assign_source_9 = (PyObject *)&PyRange_Type;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_range_type, tmp_assign_source_9 );
        }
        {
            PyObject *tmp_assign_source_10;
            tmp_assign_source_10 = (PyObject *)&PyUnicode_Type;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_text_type, tmp_assign_source_10 );
        }
        {
            PyObject *tmp_assign_source_11;
            tmp_assign_source_11 = const_tuple_type_str_tuple;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_string_types, tmp_assign_source_11 );
        }
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = const_tuple_type_int_tuple;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_integer_types, tmp_assign_source_12 );
        }
        {
            PyObject *tmp_assign_source_13;
            tmp_assign_source_13 = MAKE_FUNCTION_jinja2$_compat$$$function_2_lambda(  );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_iterkeys, tmp_assign_source_13 );
        }
        {
            PyObject *tmp_assign_source_14;
            tmp_assign_source_14 = MAKE_FUNCTION_jinja2$_compat$$$function_3_lambda(  );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_itervalues, tmp_assign_source_14 );
        }
        {
            PyObject *tmp_assign_source_15;
            tmp_assign_source_15 = MAKE_FUNCTION_jinja2$_compat$$$function_4_lambda(  );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_iteritems, tmp_assign_source_15 );
        }
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_name_name_2;
            PyObject *tmp_globals_name_2;
            PyObject *tmp_locals_name_2;
            PyObject *tmp_fromlist_name_2;
            PyObject *tmp_level_name_2;
            tmp_name_name_2 = const_str_plain_pickle;
            tmp_globals_name_2 = (PyObject *)moduledict_jinja2$_compat;
            tmp_locals_name_2 = Py_None;
            tmp_fromlist_name_2 = Py_None;
            tmp_level_name_2 = const_int_0;
            frame_043376fe1fee96ea8431d169afe72993->m_frame.f_lineno = 31;
            tmp_assign_source_16 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
            if ( tmp_assign_source_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 31;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_pickle, tmp_assign_source_16 );
        }
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_name_name_3;
            PyObject *tmp_globals_name_3;
            PyObject *tmp_locals_name_3;
            PyObject *tmp_fromlist_name_3;
            PyObject *tmp_level_name_3;
            tmp_name_name_3 = const_str_plain_io;
            tmp_globals_name_3 = (PyObject *)moduledict_jinja2$_compat;
            tmp_locals_name_3 = Py_None;
            tmp_fromlist_name_3 = const_tuple_str_plain_BytesIO_str_plain_StringIO_tuple;
            tmp_level_name_3 = const_int_0;
            frame_043376fe1fee96ea8431d169afe72993->m_frame.f_lineno = 32;
            tmp_assign_source_17 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;

                goto frame_exception_exit_1;
            }
            assert( tmp_import_from_1__module == NULL );
            tmp_import_from_1__module = tmp_assign_source_17;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_import_name_from_1;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_1 = tmp_import_from_1__module;
            tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_BytesIO );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_BytesIO, tmp_assign_source_18 );
        }
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_import_name_from_2;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_2 = tmp_import_from_1__module;
            tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_StringIO );
            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_StringIO, tmp_assign_source_19 );
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_1:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
        Py_DECREF( tmp_import_from_1__module );
        tmp_import_from_1__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
        Py_DECREF( tmp_import_from_1__module );
        tmp_import_from_1__module = NULL;

        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_mvar_value_6;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_StringIO );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_StringIO );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "StringIO" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 33;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_20 = tmp_mvar_value_6;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_NativeStringIO, tmp_assign_source_20 );
        }
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_assign_source_21 = MAKE_FUNCTION_jinja2$_compat$$$function_5_reraise( tmp_defaults_1 );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_reraise, tmp_assign_source_21 );
        }
        {
            PyObject *tmp_assign_source_22;
            tmp_assign_source_22 = (PyObject *)&PyFilter_Type;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_ifilter, tmp_assign_source_22 );
        }
        {
            PyObject *tmp_assign_source_23;
            tmp_assign_source_23 = (PyObject *)&PyMap_Type;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_imap, tmp_assign_source_23 );
        }
        {
            PyObject *tmp_assign_source_24;
            tmp_assign_source_24 = (PyObject *)&PyZip_Type;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_izip, tmp_assign_source_24 );
        }
        {
            PyObject *tmp_assign_source_25;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_7;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 43;

                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = tmp_mvar_value_7;
            tmp_assign_source_25 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_intern );
            if ( tmp_assign_source_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_intern, tmp_assign_source_25 );
        }
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_mvar_value_8;
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain__identity );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__identity );
            }

            if ( tmp_mvar_value_8 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_identity" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 45;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_26 = tmp_mvar_value_8;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_implements_iterator, tmp_assign_source_26 );
        }
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_mvar_value_9;
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain__identity );

            if (unlikely( tmp_mvar_value_9 == NULL ))
            {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__identity );
            }

            if ( tmp_mvar_value_9 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_identity" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 46;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_27 = tmp_mvar_value_9;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_implements_to_string, tmp_assign_source_27 );
        }
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_mvar_value_10;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain__identity );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__identity );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_identity" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 47;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_28 = tmp_mvar_value_10;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_encode_filename, tmp_assign_source_28 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_mvar_value_11;
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_unichr );

            if (unlikely( tmp_mvar_value_11 == NULL ))
            {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unichr );
            }

            if ( tmp_mvar_value_11 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unichr" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 50;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_29 = tmp_mvar_value_11;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_unichr, tmp_assign_source_29 );
        }
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_mvar_value_12;
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_12 == NULL ))
            {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_12 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 51;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_30 = tmp_mvar_value_12;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_text_type, tmp_assign_source_30 );
        }
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_mvar_value_13;
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_xrange );

            if (unlikely( tmp_mvar_value_13 == NULL ))
            {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_xrange );
            }

            if ( tmp_mvar_value_13 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "xrange" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 52;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_31 = tmp_mvar_value_13;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_range_type, tmp_assign_source_31 );
        }
        {
            PyObject *tmp_assign_source_32;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_mvar_value_14;
            tmp_tuple_element_1 = (PyObject *)&PyUnicode_Type;
            tmp_assign_source_32 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_assign_source_32, 0, tmp_tuple_element_1 );
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_14 == NULL ))
            {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_14 == NULL )
            {
                Py_DECREF( tmp_assign_source_32 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 53;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_1 = tmp_mvar_value_14;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_assign_source_32, 1, tmp_tuple_element_1 );
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_string_types, tmp_assign_source_32 );
        }
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_mvar_value_15;
            tmp_tuple_element_2 = (PyObject *)&PyLong_Type;
            tmp_assign_source_33 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_assign_source_33, 0, tmp_tuple_element_2 );
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_long );

            if (unlikely( tmp_mvar_value_15 == NULL ))
            {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_long );
            }

            if ( tmp_mvar_value_15 == NULL )
            {
                Py_DECREF( tmp_assign_source_33 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "long" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 54;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_2 = tmp_mvar_value_15;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_assign_source_33, 1, tmp_tuple_element_2 );
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_integer_types, tmp_assign_source_33 );
        }
        {
            PyObject *tmp_assign_source_34;
            tmp_assign_source_34 = MAKE_FUNCTION_jinja2$_compat$$$function_6_lambda(  );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_iterkeys, tmp_assign_source_34 );
        }
        {
            PyObject *tmp_assign_source_35;
            tmp_assign_source_35 = MAKE_FUNCTION_jinja2$_compat$$$function_7_lambda(  );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_itervalues, tmp_assign_source_35 );
        }
        {
            PyObject *tmp_assign_source_36;
            tmp_assign_source_36 = MAKE_FUNCTION_jinja2$_compat$$$function_8_lambda(  );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_iteritems, tmp_assign_source_36 );
        }
        {
            PyObject *tmp_assign_source_37;
            PyObject *tmp_name_name_4;
            PyObject *tmp_globals_name_4;
            PyObject *tmp_locals_name_4;
            PyObject *tmp_fromlist_name_4;
            PyObject *tmp_level_name_4;
            tmp_name_name_4 = const_str_plain_cPickle;
            tmp_globals_name_4 = (PyObject *)moduledict_jinja2$_compat;
            tmp_locals_name_4 = Py_None;
            tmp_fromlist_name_4 = Py_None;
            tmp_level_name_4 = const_int_0;
            frame_043376fe1fee96ea8431d169afe72993->m_frame.f_lineno = 60;
            tmp_assign_source_37 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
            if ( tmp_assign_source_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_pickle, tmp_assign_source_37 );
        }
        {
            PyObject *tmp_assign_source_38;
            PyObject *tmp_name_name_5;
            PyObject *tmp_globals_name_5;
            PyObject *tmp_locals_name_5;
            PyObject *tmp_fromlist_name_5;
            PyObject *tmp_level_name_5;
            tmp_name_name_5 = const_str_plain_cStringIO;
            tmp_globals_name_5 = (PyObject *)moduledict_jinja2$_compat;
            tmp_locals_name_5 = Py_None;
            tmp_fromlist_name_5 = const_tuple_str_plain_StringIO_str_plain_StringIO_tuple;
            tmp_level_name_5 = const_int_0;
            frame_043376fe1fee96ea8431d169afe72993->m_frame.f_lineno = 61;
            tmp_assign_source_38 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
            if ( tmp_assign_source_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto frame_exception_exit_1;
            }
            assert( tmp_import_from_2__module == NULL );
            tmp_import_from_2__module = tmp_assign_source_38;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_39;
            PyObject *tmp_import_name_from_3;
            CHECK_OBJECT( tmp_import_from_2__module );
            tmp_import_name_from_3 = tmp_import_from_2__module;
            tmp_assign_source_39 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_StringIO );
            if ( tmp_assign_source_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto try_except_handler_2;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_BytesIO, tmp_assign_source_39 );
        }
        {
            PyObject *tmp_assign_source_40;
            PyObject *tmp_import_name_from_4;
            CHECK_OBJECT( tmp_import_from_2__module );
            tmp_import_name_from_4 = tmp_import_from_2__module;
            tmp_assign_source_40 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_StringIO );
            if ( tmp_assign_source_40 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto try_except_handler_2;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_StringIO, tmp_assign_source_40 );
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
        Py_DECREF( tmp_import_from_2__module );
        tmp_import_from_2__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
        Py_DECREF( tmp_import_from_2__module );
        tmp_import_from_2__module = NULL;

        {
            PyObject *tmp_assign_source_41;
            PyObject *tmp_mvar_value_16;
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_BytesIO );

            if (unlikely( tmp_mvar_value_16 == NULL ))
            {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BytesIO );
            }

            if ( tmp_mvar_value_16 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BytesIO" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 62;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_41 = tmp_mvar_value_16;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_NativeStringIO, tmp_assign_source_41 );
        }
        {
            PyObject *tmp_outline_return_value_1;
            {
                PyObject *tmp_assign_source_42;
                tmp_assign_source_42 = (PyObject *)moduledict_jinja2$_compat;
                assert( tmp_exec_call_1__locals == NULL );
                Py_INCREF( tmp_assign_source_42 );
                tmp_exec_call_1__locals = tmp_assign_source_42;
            }
            {
                PyObject *tmp_assign_source_43;
                tmp_assign_source_43 = (PyObject *)moduledict_jinja2$_compat;
                assert( tmp_exec_call_1__globals == NULL );
                Py_INCREF( tmp_assign_source_43 );
                tmp_exec_call_1__globals = tmp_assign_source_43;
            }
            // Tried code:
            {
                PyObject *tmp_eval_source_1;
                PyObject *tmp_eval_globals_1;
                PyObject *tmp_eval_locals_1;
                PyObject *tmp_eval_compiled_1;
                tmp_eval_source_1 = const_str_digest_d08ec648c9222f75f8f40823bebe3549;
                CHECK_OBJECT( tmp_exec_call_1__globals );
                tmp_eval_globals_1 = tmp_exec_call_1__globals;
                CHECK_OBJECT( tmp_exec_call_1__locals );
                tmp_eval_locals_1 = tmp_exec_call_1__locals;
                tmp_eval_compiled_1 = COMPILE_CODE( tmp_eval_source_1, const_str_angle_string, const_str_plain_exec, NULL, NULL, NULL );
                if ( tmp_eval_compiled_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 64;

                    goto try_except_handler_3;
                }
                tmp_outline_return_value_1 = EVAL_CODE( tmp_eval_compiled_1, tmp_eval_globals_1, tmp_eval_locals_1 );
                Py_DECREF( tmp_eval_compiled_1 );
                if ( tmp_outline_return_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 64;

                    goto try_except_handler_3;
                }
                goto try_return_handler_3;
            }
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jinja2$_compat );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_3:;
            CHECK_OBJECT( (PyObject *)tmp_exec_call_1__globals );
            Py_DECREF( tmp_exec_call_1__globals );
            tmp_exec_call_1__globals = NULL;

            CHECK_OBJECT( (PyObject *)tmp_exec_call_1__locals );
            Py_DECREF( tmp_exec_call_1__locals );
            tmp_exec_call_1__locals = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_exec_call_1__globals );
            Py_DECREF( tmp_exec_call_1__globals );
            tmp_exec_call_1__globals = NULL;

            CHECK_OBJECT( (PyObject *)tmp_exec_call_1__locals );
            Py_DECREF( tmp_exec_call_1__locals );
            tmp_exec_call_1__locals = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto frame_exception_exit_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( jinja2$_compat );
            return MOD_RETURN_VALUE( NULL );
            outline_result_1:;
            Py_DECREF( tmp_outline_return_value_1 );
        }
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_name_name_6;
            PyObject *tmp_globals_name_6;
            PyObject *tmp_locals_name_6;
            PyObject *tmp_fromlist_name_6;
            PyObject *tmp_level_name_6;
            tmp_name_name_6 = const_str_plain_itertools;
            tmp_globals_name_6 = (PyObject *)moduledict_jinja2$_compat;
            tmp_locals_name_6 = Py_None;
            tmp_fromlist_name_6 = const_tuple_str_plain_imap_str_plain_izip_str_plain_ifilter_tuple;
            tmp_level_name_6 = const_int_0;
            frame_043376fe1fee96ea8431d169afe72993->m_frame.f_lineno = 66;
            tmp_assign_source_44 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
            assert( !(tmp_assign_source_44 == NULL) );
            assert( tmp_import_from_3__module == NULL );
            tmp_import_from_3__module = tmp_assign_source_44;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_45;
            PyObject *tmp_import_name_from_5;
            CHECK_OBJECT( tmp_import_from_3__module );
            tmp_import_name_from_5 = tmp_import_from_3__module;
            tmp_assign_source_45 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_imap );
            if ( tmp_assign_source_45 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_4;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_imap, tmp_assign_source_45 );
        }
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_import_name_from_6;
            CHECK_OBJECT( tmp_import_from_3__module );
            tmp_import_name_from_6 = tmp_import_from_3__module;
            tmp_assign_source_46 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_izip );
            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_4;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_izip, tmp_assign_source_46 );
        }
        {
            PyObject *tmp_assign_source_47;
            PyObject *tmp_import_name_from_7;
            CHECK_OBJECT( tmp_import_from_3__module );
            tmp_import_name_from_7 = tmp_import_from_3__module;
            tmp_assign_source_47 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_ifilter );
            if ( tmp_assign_source_47 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_4;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_ifilter, tmp_assign_source_47 );
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
        Py_DECREF( tmp_import_from_3__module );
        tmp_import_from_3__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_3:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
        Py_DECREF( tmp_import_from_3__module );
        tmp_import_from_3__module = NULL;

        {
            PyObject *tmp_assign_source_48;
            PyObject *tmp_mvar_value_17;
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_intern );

            if (unlikely( tmp_mvar_value_17 == NULL ))
            {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_intern );
            }

            if ( tmp_mvar_value_17 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "intern" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 67;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_48 = tmp_mvar_value_17;
            UPDATE_STRING_DICT0( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_intern, tmp_assign_source_48 );
        }
        {
            PyObject *tmp_assign_source_49;
            tmp_assign_source_49 = MAKE_FUNCTION_jinja2$_compat$$$function_9_implements_iterator(  );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_implements_iterator, tmp_assign_source_49 );
        }
        {
            PyObject *tmp_assign_source_50;
            tmp_assign_source_50 = MAKE_FUNCTION_jinja2$_compat$$$function_10_implements_to_string(  );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_implements_to_string, tmp_assign_source_50 );
        }
        {
            PyObject *tmp_assign_source_51;
            tmp_assign_source_51 = MAKE_FUNCTION_jinja2$_compat$$$function_11_encode_filename(  );



            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_encode_filename, tmp_assign_source_51 );
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_52;
        tmp_assign_source_52 = MAKE_FUNCTION_jinja2$_compat$$$function_12_with_metaclass(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_with_metaclass, tmp_assign_source_52 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_53;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_04991ea695faff4a76e4efb6a8a8593f;
        tmp_globals_name_7 = (PyObject *)moduledict_jinja2$_compat;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_quote_from_bytes_tuple;
        tmp_level_name_7 = const_int_0;
        frame_043376fe1fee96ea8431d169afe72993->m_frame.f_lineno = 97;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto try_except_handler_5;
        }
        tmp_assign_source_53 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_quote_from_bytes );
        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_url_quote, tmp_assign_source_53 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_5 == NULL )
    {
        exception_keeper_tb_5 = MAKE_TRACEBACK( frame_043376fe1fee96ea8431d169afe72993, exception_keeper_lineno_5 );
    }
    else if ( exception_keeper_lineno_5 != 0 )
    {
        exception_keeper_tb_5 = ADD_TRACEBACK( exception_keeper_tb_5, frame_043376fe1fee96ea8431d169afe72993, exception_keeper_lineno_5 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
    PyException_SetTraceback( exception_keeper_value_5, (PyObject *)exception_keeper_tb_5 );
    PUBLISH_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;

            goto try_except_handler_6;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_54;
            PyObject *tmp_import_name_from_9;
            PyObject *tmp_name_name_8;
            PyObject *tmp_globals_name_8;
            PyObject *tmp_locals_name_8;
            PyObject *tmp_fromlist_name_8;
            PyObject *tmp_level_name_8;
            tmp_name_name_8 = const_str_plain_urllib;
            tmp_globals_name_8 = (PyObject *)moduledict_jinja2$_compat;
            tmp_locals_name_8 = Py_None;
            tmp_fromlist_name_8 = const_tuple_str_plain_quote_tuple;
            tmp_level_name_8 = const_int_0;
            frame_043376fe1fee96ea8431d169afe72993->m_frame.f_lineno = 99;
            tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
            if ( tmp_import_name_from_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;

                goto try_except_handler_6;
            }
            tmp_assign_source_54 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_quote );
            Py_DECREF( tmp_import_name_from_9 );
            if ( tmp_assign_source_54 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;

                goto try_except_handler_6;
            }
            UPDATE_STRING_DICT1( moduledict_jinja2$_compat, (Nuitka_StringObject *)const_str_plain_url_quote, tmp_assign_source_54 );
        }
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 96;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_043376fe1fee96ea8431d169afe72993->m_frame) frame_043376fe1fee96ea8431d169afe72993->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_6;
        branch_end_2:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_4;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$_compat );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_4:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_043376fe1fee96ea8431d169afe72993 );
#endif
    popFrameStack();

    assertFrameObject( frame_043376fe1fee96ea8431d169afe72993 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_043376fe1fee96ea8431d169afe72993 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_043376fe1fee96ea8431d169afe72993, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_043376fe1fee96ea8431d169afe72993->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_043376fe1fee96ea8431d169afe72993, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_jinja2$_compat );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
