/* Generated code for Python module 'PIL.ImageQt'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_PIL$ImageQt" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_PIL$ImageQt;
PyDictObject *moduledict_PIL$ImageQt;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_warn;
static PyObject *const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple;
static PyObject *const_str_plain_qt_module;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_tuple_int_pos_255_tuple;
extern PyObject *const_str_plain__util;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_plain_Format_ARGB32;
static PyObject *const_tuple_str_plain_qt_version_tuple;
static PyObject *const_str_plain_bits_per_line;
static PyObject *const_list_str_plain_5_str_plain_PyQt5_list;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_plain_im_data;
static PyObject *const_str_digest_fa2a7634e136862a6999ab8f4c7c2497;
static PyObject *const_str_digest_98de5bc41c0485ecbec0e675e0740b32;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_palette;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_save;
extern PyObject *const_str_plain_PyQt5;
static PyObject *const_str_plain_bytes_per_line;
static PyObject *const_tuple_str_plain_im_str_plain_qimage_tuple;
extern PyObject *const_str_plain_bytes;
extern PyObject *const_str_plain_merge;
extern PyObject *const_str_plain_a;
static PyObject *const_str_plain_hasAlphaChannel;
extern PyObject *const_str_plain_mode;
static PyObject *const_list_0538c06cd28541d8072070a86c8cfda8_list;
extern PyObject *const_str_plain_size;
static PyObject *const_str_plain_qRgba;
extern PyObject *const_str_plain_seek;
static PyObject *const_str_plain_remaining_bits_per_line;
static PyObject *const_str_plain___data;
static PyObject *const_tuple_type_ImportError_type_RuntimeError_tuple;
extern PyObject *const_str_plain_new_data;
static PyObject *const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple;
static PyObject *const_list_str_plain_side2_str_plain_PySide2_list;
static PyObject *const_str_plain_ReadWrite;
extern PyObject *const_str_plain_png;
static PyObject *const_str_plain_Format_Mono;
extern PyObject *const_str_plain_ImageQt;
extern PyObject *const_str_plain_RGB;
extern PyObject *const_str_plain_join;
static PyObject *const_str_plain_setColorTable;
extern PyObject *const_str_digest_ac630a4d3871c74696d88f29085358f6;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_reverse;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_L;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_plain__toqclass_helper;
extern PyObject *const_str_plain_data;
extern PyObject *const_str_plain_close;
static PyObject *const_str_plain_bits_per_pixel;
extern PyObject *const_str_plain_warnings;
static PyObject *const_list_str_plain_side_str_plain_PySide_list;
static PyObject *const_str_digest_78c6c34928177e40925be2026c51a3f2;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_tuple_str_plain_raw_str_plain_BGRA_tuple;
static PyObject *const_str_plain_qimage;
static PyObject *const_str_digest_f9bfb6fba4ce859d971e49ded4f308c4;
static PyObject *const_str_plain__ImageQt__data;
static PyObject *const_str_plain_align8to32;
static PyObject *const_str_digest_ad6c57ee9646a00a202f40e2d20b8b2e;
extern PyObject *const_str_plain_g;
static PyObject *const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple;
extern PyObject *const_str_plain_ppm;
extern PyObject *const_str_plain_BytesIO;
extern PyObject *const_str_plain_unicode;
static PyObject *const_str_digest_c0dc88c870b231b33b027e9493c434a1;
extern PyObject *const_str_plain_DeprecationWarning;
static PyObject *const_str_digest_3fc503d1a74f09528d49ecf123c3bbf8;
extern PyObject *const_int_pos_8;
extern PyObject *const_str_plain_PySide2;
extern PyObject *const_str_plain_convert;
extern PyObject *const_str_plain_py3;
static PyObject *const_str_plain_QBuffer;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_9f3bbfc48b4b17b32ffa583a21edd739;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_isPath;
extern PyObject *const_str_plain_rgb;
static PyObject *const_str_plain_QImage;
extern PyObject *const_str_plain_r;
extern PyObject *const_str_plain_raw;
extern PyObject *const_str_plain_side;
extern PyObject *const_str_plain_P;
extern PyObject *const_str_plain_PySide;
static PyObject *const_list_str_plain_4_str_plain_PyQt4_list;
static PyObject *const_str_plain_qt_version;
extern PyObject *const_str_plain_PyQt4;
extern PyObject *const_str_plain_b;
extern PyObject *const_tuple_str_plain_BytesIO_tuple;
extern PyObject *const_str_plain_split;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_tuple_str_plain_RGBA_tuple;
static PyObject *const_str_digest_274dd8b0a28882b307cec02c0df12901;
extern PyObject *const_tuple_str_plain_Image_tuple;
extern PyObject *const_int_pos_4294967295;
extern PyObject *const_str_plain_fromqpixmap;
extern PyObject *const_str_plain_False;
extern PyObject *const_tuple_str_plain_im_tuple;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_buffer;
extern PyObject *const_str_plain_fromqimage;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
static PyObject *const_tuple_str_plain_im_str_plain_buffer_str_plain_b_tuple;
extern PyObject *const_str_plain_BGRX;
static PyObject *const_str_plain_fromImage;
static PyObject *const_str_plain_qt_versions;
static PyObject *const_dict_d65e53817bae224a8ca46ec5dd007f86;
extern PyObject *const_int_0;
static PyObject *const_str_plain_extra_padding;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_Format_RGB32;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_809eeb80d7cfe4ba517479debf29cb9d;
extern PyObject *const_int_pos_4;
extern PyObject *const_str_plain_getpalette;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_plain_type;
extern PyObject *const_bytes_empty;
static PyObject *const_tuple_str_plain_raw_str_plain_BGRX_tuple;
extern PyObject *const_str_plain_qt_is_installed;
extern PyObject *const_xrange_0_256;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_modules;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_BGRA;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_io;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_7a7fadd42583da786441c79549bd3d64;
extern PyObject *const_int_pos_256;
static PyObject *const_str_plain_full_bytes_per_line;
extern PyObject *const_str_plain_1;
extern PyObject *const_str_plain_open;
static PyObject *const_str_plain_QPixmap;
static PyObject *const_tuple_str_plain_self_str_plain_im_str_plain_im_data_tuple;
extern PyObject *const_str_plain_width;
extern PyObject *const_str_plain_4;
extern PyObject *const_str_plain_key;
extern PyObject *const_str_plain_toqimage;
static PyObject *const_str_digest_1776dd8497d285ed28d588470b84f38f;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_str_plain_toUtf8;
extern PyObject *const_int_pos_255;
extern PyObject *const_str_plain_im;
static PyObject *const_str_digest_b00f80bcc4ec2a87a678e79cd3c2f0f5;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_5;
static PyObject *const_str_digest_47e034e36982872ed6479ef51bdba8d8;
extern PyObject *const_str_plain_write;
extern PyObject *const_str_plain_RGBA;
static PyObject *const_str_plain_WARNING_TEXT;
extern PyObject *const_str_digest_91d3fabe5e3af50ef0546b25193cf397;
extern PyObject *const_str_plain_has_location;
static PyObject *const_tuple_str_plain_QBuffer_str_plain_QIODevice_tuple;
static PyObject *const_str_plain_colortable;
extern PyObject *const_str_plain_sort;
extern PyObject *const_bytes_chr_0;
static PyObject *const_tuple_str_plain_QImage_str_plain_qRgba_str_plain_QPixmap_tuple;
extern PyObject *const_str_plain_Image;
extern PyObject *const_str_plain_toqpixmap;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_tobytes;
extern PyObject *const_str_empty;
static PyObject *const_str_plain_Format_Indexed8;
static PyObject *const_str_plain_side2;
static PyObject *const_tuple_str_plain_isPath_str_plain_py3_tuple;
static PyObject *const_str_digest_155e15f871b1d690f132957bc9eefa2e;
static PyObject *const_str_digest_5d4e096d92c52b0a9cd8cd0f2047cd77;
static PyObject *const_str_plain_QIODevice;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 0, const_str_plain_bytes ); Py_INCREF( const_str_plain_bytes );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 1, const_str_plain_width ); Py_INCREF( const_str_plain_width );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 2, const_str_plain_mode ); Py_INCREF( const_str_plain_mode );
    const_str_plain_bits_per_pixel = UNSTREAM_STRING_ASCII( &constant_bin[ 74499 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 3, const_str_plain_bits_per_pixel ); Py_INCREF( const_str_plain_bits_per_pixel );
    const_str_plain_bits_per_line = UNSTREAM_STRING_ASCII( &constant_bin[ 74513 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 4, const_str_plain_bits_per_line ); Py_INCREF( const_str_plain_bits_per_line );
    const_str_plain_full_bytes_per_line = UNSTREAM_STRING_ASCII( &constant_bin[ 74526 ], 19, 1 );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 5, const_str_plain_full_bytes_per_line ); Py_INCREF( const_str_plain_full_bytes_per_line );
    const_str_plain_remaining_bits_per_line = UNSTREAM_STRING_ASCII( &constant_bin[ 74545 ], 23, 1 );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 6, const_str_plain_remaining_bits_per_line ); Py_INCREF( const_str_plain_remaining_bits_per_line );
    const_str_plain_bytes_per_line = UNSTREAM_STRING_ASCII( &constant_bin[ 74531 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 7, const_str_plain_bytes_per_line ); Py_INCREF( const_str_plain_bytes_per_line );
    const_str_plain_extra_padding = UNSTREAM_STRING_ASCII( &constant_bin[ 74568 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 8, const_str_plain_extra_padding ); Py_INCREF( const_str_plain_extra_padding );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 9, const_str_plain_new_data ); Py_INCREF( const_str_plain_new_data );
    PyTuple_SET_ITEM( const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 10, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    const_str_plain_qt_module = UNSTREAM_STRING_ASCII( &constant_bin[ 74581 ], 9, 1 );
    const_str_plain_Format_ARGB32 = UNSTREAM_STRING_ASCII( &constant_bin[ 74590 ], 13, 1 );
    const_tuple_str_plain_qt_version_tuple = PyTuple_New( 1 );
    const_str_plain_qt_version = UNSTREAM_STRING_ASCII( &constant_bin[ 74603 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_qt_version_tuple, 0, const_str_plain_qt_version ); Py_INCREF( const_str_plain_qt_version );
    const_list_str_plain_5_str_plain_PyQt5_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_5_str_plain_PyQt5_list, 0, const_str_plain_5 ); Py_INCREF( const_str_plain_5 );
    PyList_SET_ITEM( const_list_str_plain_5_str_plain_PyQt5_list, 1, const_str_plain_PyQt5 ); Py_INCREF( const_str_plain_PyQt5 );
    const_str_plain_im_data = UNSTREAM_STRING_ASCII( &constant_bin[ 74613 ], 7, 1 );
    const_str_digest_fa2a7634e136862a6999ab8f4c7c2497 = UNSTREAM_STRING_ASCII( &constant_bin[ 74620 ], 242, 0 );
    const_str_digest_98de5bc41c0485ecbec0e675e0740b32 = UNSTREAM_STRING_ASCII( &constant_bin[ 74862 ], 12, 0 );
    const_tuple_str_plain_im_str_plain_qimage_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_im_str_plain_qimage_tuple, 0, const_str_plain_im ); Py_INCREF( const_str_plain_im );
    const_str_plain_qimage = UNSTREAM_STRING_ASCII( &constant_bin[ 21617 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_im_str_plain_qimage_tuple, 1, const_str_plain_qimage ); Py_INCREF( const_str_plain_qimage );
    const_str_plain_hasAlphaChannel = UNSTREAM_STRING_ASCII( &constant_bin[ 74874 ], 15, 1 );
    const_list_0538c06cd28541d8072070a86c8cfda8_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_0538c06cd28541d8072070a86c8cfda8_list, 0, const_list_str_plain_5_str_plain_PyQt5_list ); Py_INCREF( const_list_str_plain_5_str_plain_PyQt5_list );
    const_list_str_plain_side2_str_plain_PySide2_list = PyList_New( 2 );
    const_str_plain_side2 = UNSTREAM_STRING_ASCII( &constant_bin[ 74889 ], 5, 1 );
    PyList_SET_ITEM( const_list_str_plain_side2_str_plain_PySide2_list, 0, const_str_plain_side2 ); Py_INCREF( const_str_plain_side2 );
    PyList_SET_ITEM( const_list_str_plain_side2_str_plain_PySide2_list, 1, const_str_plain_PySide2 ); Py_INCREF( const_str_plain_PySide2 );
    PyList_SET_ITEM( const_list_0538c06cd28541d8072070a86c8cfda8_list, 1, const_list_str_plain_side2_str_plain_PySide2_list ); Py_INCREF( const_list_str_plain_side2_str_plain_PySide2_list );
    const_list_str_plain_4_str_plain_PyQt4_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_4_str_plain_PyQt4_list, 0, const_str_plain_4 ); Py_INCREF( const_str_plain_4 );
    PyList_SET_ITEM( const_list_str_plain_4_str_plain_PyQt4_list, 1, const_str_plain_PyQt4 ); Py_INCREF( const_str_plain_PyQt4 );
    PyList_SET_ITEM( const_list_0538c06cd28541d8072070a86c8cfda8_list, 2, const_list_str_plain_4_str_plain_PyQt4_list ); Py_INCREF( const_list_str_plain_4_str_plain_PyQt4_list );
    const_list_str_plain_side_str_plain_PySide_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_side_str_plain_PySide_list, 0, const_str_plain_side ); Py_INCREF( const_str_plain_side );
    PyList_SET_ITEM( const_list_str_plain_side_str_plain_PySide_list, 1, const_str_plain_PySide ); Py_INCREF( const_str_plain_PySide );
    PyList_SET_ITEM( const_list_0538c06cd28541d8072070a86c8cfda8_list, 3, const_list_str_plain_side_str_plain_PySide_list ); Py_INCREF( const_list_str_plain_side_str_plain_PySide_list );
    const_str_plain_qRgba = UNSTREAM_STRING_ASCII( &constant_bin[ 74894 ], 5, 1 );
    const_str_plain___data = UNSTREAM_STRING_ASCII( &constant_bin[ 74899 ], 6, 1 );
    const_tuple_type_ImportError_type_RuntimeError_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_type_ImportError_type_RuntimeError_tuple, 0, (PyObject *)PyExc_ImportError ); Py_INCREF( (PyObject *)PyExc_ImportError );
    PyTuple_SET_ITEM( const_tuple_type_ImportError_type_RuntimeError_tuple, 1, (PyObject *)PyExc_RuntimeError ); Py_INCREF( (PyObject *)PyExc_RuntimeError );
    const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 0, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 1, const_str_plain_g ); Py_INCREF( const_str_plain_g );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 2, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 3, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    const_str_plain_ReadWrite = UNSTREAM_STRING_ASCII( &constant_bin[ 74905 ], 9, 1 );
    const_str_plain_Format_Mono = UNSTREAM_STRING_ASCII( &constant_bin[ 74914 ], 11, 1 );
    const_str_plain_setColorTable = UNSTREAM_STRING_ASCII( &constant_bin[ 74925 ], 13, 1 );
    const_str_plain__toqclass_helper = UNSTREAM_STRING_ASCII( &constant_bin[ 74938 ], 16, 1 );
    const_str_digest_78c6c34928177e40925be2026c51a3f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 74954 ], 69, 0 );
    const_tuple_str_plain_raw_str_plain_BGRA_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_raw_str_plain_BGRA_tuple, 0, const_str_plain_raw ); Py_INCREF( const_str_plain_raw );
    PyTuple_SET_ITEM( const_tuple_str_plain_raw_str_plain_BGRA_tuple, 1, const_str_plain_BGRA ); Py_INCREF( const_str_plain_BGRA );
    const_str_digest_f9bfb6fba4ce859d971e49ded4f308c4 = UNSTREAM_STRING_ASCII( &constant_bin[ 75023 ], 25, 0 );
    const_str_plain__ImageQt__data = UNSTREAM_STRING_ASCII( &constant_bin[ 75048 ], 14, 1 );
    const_str_plain_align8to32 = UNSTREAM_STRING_ASCII( &constant_bin[ 75062 ], 10, 1 );
    const_str_digest_ad6c57ee9646a00a202f40e2d20b8b2e = UNSTREAM_STRING_ASCII( &constant_bin[ 75072 ], 13, 0 );
    const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 0, const_str_plain_im ); Py_INCREF( const_str_plain_im );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 1, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    const_str_plain_colortable = UNSTREAM_STRING_ASCII( &constant_bin[ 75085 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 2, const_str_plain_colortable ); Py_INCREF( const_str_plain_colortable );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 3, const_str_plain_format ); Py_INCREF( const_str_plain_format );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 4, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 5, const_str_plain_palette ); Py_INCREF( const_str_plain_palette );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 6, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 7, const_str_plain_g ); Py_INCREF( const_str_plain_g );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 8, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 9, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyTuple_SET_ITEM( const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 10, const_str_plain___data ); Py_INCREF( const_str_plain___data );
    const_str_digest_c0dc88c870b231b33b027e9493c434a1 = UNSTREAM_STRING_ASCII( &constant_bin[ 75095 ], 14, 0 );
    const_str_digest_3fc503d1a74f09528d49ecf123c3bbf8 = UNSTREAM_STRING_ASCII( &constant_bin[ 75109 ], 11, 0 );
    const_str_plain_QBuffer = UNSTREAM_STRING_ASCII( &constant_bin[ 75120 ], 7, 1 );
    const_str_digest_9f3bbfc48b4b17b32ffa583a21edd739 = UNSTREAM_STRING_ASCII( &constant_bin[ 75127 ], 109, 0 );
    const_str_plain_QImage = UNSTREAM_STRING_ASCII( &constant_bin[ 33826 ], 6, 1 );
    const_str_digest_274dd8b0a28882b307cec02c0df12901 = UNSTREAM_STRING_ASCII( &constant_bin[ 75236 ], 11, 0 );
    const_tuple_str_plain_im_str_plain_buffer_str_plain_b_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_im_str_plain_buffer_str_plain_b_tuple, 0, const_str_plain_im ); Py_INCREF( const_str_plain_im );
    PyTuple_SET_ITEM( const_tuple_str_plain_im_str_plain_buffer_str_plain_b_tuple, 1, const_str_plain_buffer ); Py_INCREF( const_str_plain_buffer );
    PyTuple_SET_ITEM( const_tuple_str_plain_im_str_plain_buffer_str_plain_b_tuple, 2, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    const_str_plain_fromImage = UNSTREAM_STRING_ASCII( &constant_bin[ 75247 ], 9, 1 );
    const_str_plain_qt_versions = UNSTREAM_STRING_ASCII( &constant_bin[ 75256 ], 11, 1 );
    const_dict_d65e53817bae224a8ca46ec5dd007f86 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_d65e53817bae224a8ca46ec5dd007f86, const_str_plain_1, const_int_pos_1 );
    PyDict_SetItem( const_dict_d65e53817bae224a8ca46ec5dd007f86, const_str_plain_L, const_int_pos_8 );
    PyDict_SetItem( const_dict_d65e53817bae224a8ca46ec5dd007f86, const_str_plain_P, const_int_pos_8 );
    assert( PyDict_Size( const_dict_d65e53817bae224a8ca46ec5dd007f86 ) == 3 );
    const_str_plain_Format_RGB32 = UNSTREAM_STRING_ASCII( &constant_bin[ 75267 ], 12, 1 );
    const_str_digest_809eeb80d7cfe4ba517479debf29cb9d = UNSTREAM_STRING_ASCII( &constant_bin[ 75279 ], 14, 0 );
    const_tuple_str_plain_raw_str_plain_BGRX_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_raw_str_plain_BGRX_tuple, 0, const_str_plain_raw ); Py_INCREF( const_str_plain_raw );
    PyTuple_SET_ITEM( const_tuple_str_plain_raw_str_plain_BGRX_tuple, 1, const_str_plain_BGRX ); Py_INCREF( const_str_plain_BGRX );
    const_str_digest_7a7fadd42583da786441c79549bd3d64 = UNSTREAM_STRING_ASCII( &constant_bin[ 75293 ], 115, 0 );
    const_str_plain_QPixmap = UNSTREAM_STRING_ASCII( &constant_bin[ 18306 ], 7, 1 );
    const_tuple_str_plain_self_str_plain_im_str_plain_im_data_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_im_str_plain_im_data_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_im_str_plain_im_data_tuple, 1, const_str_plain_im ); Py_INCREF( const_str_plain_im );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_im_str_plain_im_data_tuple, 2, const_str_plain_im_data ); Py_INCREF( const_str_plain_im_data );
    const_str_digest_1776dd8497d285ed28d588470b84f38f = UNSTREAM_STRING_ASCII( &constant_bin[ 75408 ], 16, 0 );
    const_str_plain_toUtf8 = UNSTREAM_STRING_ASCII( &constant_bin[ 75424 ], 6, 1 );
    const_str_digest_b00f80bcc4ec2a87a678e79cd3c2f0f5 = UNSTREAM_STRING_ASCII( &constant_bin[ 75430 ], 20, 0 );
    const_str_digest_47e034e36982872ed6479ef51bdba8d8 = UNSTREAM_STRING_ASCII( &constant_bin[ 75450 ], 13, 0 );
    const_str_plain_WARNING_TEXT = UNSTREAM_STRING_ASCII( &constant_bin[ 75463 ], 12, 1 );
    const_tuple_str_plain_QBuffer_str_plain_QIODevice_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_QBuffer_str_plain_QIODevice_tuple, 0, const_str_plain_QBuffer ); Py_INCREF( const_str_plain_QBuffer );
    const_str_plain_QIODevice = UNSTREAM_STRING_ASCII( &constant_bin[ 75475 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_QBuffer_str_plain_QIODevice_tuple, 1, const_str_plain_QIODevice ); Py_INCREF( const_str_plain_QIODevice );
    const_tuple_str_plain_QImage_str_plain_qRgba_str_plain_QPixmap_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_QImage_str_plain_qRgba_str_plain_QPixmap_tuple, 0, const_str_plain_QImage ); Py_INCREF( const_str_plain_QImage );
    PyTuple_SET_ITEM( const_tuple_str_plain_QImage_str_plain_qRgba_str_plain_QPixmap_tuple, 1, const_str_plain_qRgba ); Py_INCREF( const_str_plain_qRgba );
    PyTuple_SET_ITEM( const_tuple_str_plain_QImage_str_plain_qRgba_str_plain_QPixmap_tuple, 2, const_str_plain_QPixmap ); Py_INCREF( const_str_plain_QPixmap );
    const_str_plain_Format_Indexed8 = UNSTREAM_STRING_ASCII( &constant_bin[ 75484 ], 15, 1 );
    const_tuple_str_plain_isPath_str_plain_py3_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_isPath_str_plain_py3_tuple, 0, const_str_plain_isPath ); Py_INCREF( const_str_plain_isPath );
    PyTuple_SET_ITEM( const_tuple_str_plain_isPath_str_plain_py3_tuple, 1, const_str_plain_py3 ); Py_INCREF( const_str_plain_py3 );
    const_str_digest_155e15f871b1d690f132957bc9eefa2e = UNSTREAM_STRING_ASCII( &constant_bin[ 75499 ], 12, 0 );
    const_str_digest_5d4e096d92c52b0a9cd8cd0f2047cd77 = UNSTREAM_STRING_ASCII( &constant_bin[ 75511 ], 65, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_PIL$ImageQt( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_742a30896665ce34b6dfa8e142b690ac;
static PyCodeObject *codeobj_65cbf88a980d87c3d3a2f1bba32e8950;
static PyCodeObject *codeobj_fd43ac5b458178f026e6bce0e6bc2a6c;
static PyCodeObject *codeobj_8f2c54b91515721f2199c44813e24fa5;
static PyCodeObject *codeobj_aa8d8da8ecba6f51192b4600955be72d;
static PyCodeObject *codeobj_6b294a6c221fdbc575de8d51adf92460;
static PyCodeObject *codeobj_55e248f7349a3005d1457163d79da71a;
static PyCodeObject *codeobj_42c90e91907a455baa9f7b66c51e22f9;
static PyCodeObject *codeobj_7dccdce203e0aff37ce07367cf12cdbf;
static PyCodeObject *codeobj_558e4da1872eab7221eac3f845dca723;
static PyCodeObject *codeobj_dfd9f4c88e2a6575fbf33c5da893641b;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_809eeb80d7cfe4ba517479debf29cb9d );
    codeobj_742a30896665ce34b6dfa8e142b690ac = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 38, const_tuple_str_plain_qt_version_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_65cbf88a980d87c3d3a2f1bba32e8950 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_b00f80bcc4ec2a87a678e79cd3c2f0f5, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_fd43ac5b458178f026e6bce0e6bc2a6c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ImageQt, 192, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_8f2c54b91515721f2199c44813e24fa5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 194, const_tuple_str_plain_self_str_plain_im_str_plain_im_data_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_aa8d8da8ecba6f51192b4600955be72d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__toqclass_helper, 144, const_tuple_8ab360d6cf172b5b5fe4e667c7a98cc6_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6b294a6c221fdbc575de8d51adf92460 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_align8to32, 114, const_tuple_a117ef550a0148f2e2af59682892e2ed_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_55e248f7349a3005d1457163d79da71a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_fromqimage, 74, const_tuple_str_plain_im_str_plain_buffer_str_plain_b_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_42c90e91907a455baa9f7b66c51e22f9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_fromqpixmap, 100, const_tuple_str_plain_im_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7dccdce203e0aff37ce07367cf12cdbf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_rgb, 67, const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_558e4da1872eab7221eac3f845dca723 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_toqimage, 215, const_tuple_str_plain_im_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_dfd9f4c88e2a6575fbf33c5da893641b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_toqpixmap, 219, const_tuple_str_plain_im_str_plain_qimage_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_4_complex_call_helper_star_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_2_rgb( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_3_fromqimage(  );


static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_4_fromqpixmap(  );


static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_5_align8to32(  );


static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_6__toqclass_helper(  );


static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_7___init__(  );


static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_8_toqimage(  );


static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_9_toqpixmap(  );


// The module function definitions.
static PyObject *impl_PIL$ImageQt$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_qt_version = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_742a30896665ce34b6dfa8e142b690ac;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_742a30896665ce34b6dfa8e142b690ac = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_742a30896665ce34b6dfa8e142b690ac, codeobj_742a30896665ce34b6dfa8e142b690ac, module_PIL$ImageQt, sizeof(void *) );
    frame_742a30896665ce34b6dfa8e142b690ac = cache_frame_742a30896665ce34b6dfa8e142b690ac;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_742a30896665ce34b6dfa8e142b690ac );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_742a30896665ce34b6dfa8e142b690ac ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_qt_version );
        tmp_subscribed_name_1 = par_qt_version;
        tmp_subscript_name_1 = const_int_pos_1;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_compexpr_left_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 38;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_modules );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_1 );

            exception_lineno = 38;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_742a30896665ce34b6dfa8e142b690ac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_742a30896665ce34b6dfa8e142b690ac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_742a30896665ce34b6dfa8e142b690ac );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_742a30896665ce34b6dfa8e142b690ac, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_742a30896665ce34b6dfa8e142b690ac->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_742a30896665ce34b6dfa8e142b690ac, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_742a30896665ce34b6dfa8e142b690ac,
        type_description_1,
        par_qt_version
    );


    // Release cached frame.
    if ( frame_742a30896665ce34b6dfa8e142b690ac == cache_frame_742a30896665ce34b6dfa8e142b690ac )
    {
        Py_DECREF( frame_742a30896665ce34b6dfa8e142b690ac );
    }
    cache_frame_742a30896665ce34b6dfa8e142b690ac = NULL;

    assertFrameObject( frame_742a30896665ce34b6dfa8e142b690ac );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_qt_version );
    Py_DECREF( par_qt_version );
    par_qt_version = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_qt_version );
    Py_DECREF( par_qt_version );
    par_qt_version = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageQt$$$function_2_rgb( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_r = python_pars[ 0 ];
    PyObject *par_g = python_pars[ 1 ];
    PyObject *par_b = python_pars[ 2 ];
    PyObject *par_a = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_7dccdce203e0aff37ce07367cf12cdbf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7dccdce203e0aff37ce07367cf12cdbf = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7dccdce203e0aff37ce07367cf12cdbf, codeobj_7dccdce203e0aff37ce07367cf12cdbf, module_PIL$ImageQt, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7dccdce203e0aff37ce07367cf12cdbf = cache_frame_7dccdce203e0aff37ce07367cf12cdbf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7dccdce203e0aff37ce07367cf12cdbf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7dccdce203e0aff37ce07367cf12cdbf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_right_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qRgba );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qRgba );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "qRgba" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_r );
        tmp_args_element_name_1 = par_r;
        CHECK_OBJECT( par_g );
        tmp_args_element_name_2 = par_g;
        CHECK_OBJECT( par_b );
        tmp_args_element_name_3 = par_b;
        CHECK_OBJECT( par_a );
        tmp_args_element_name_4 = par_a;
        frame_7dccdce203e0aff37ce07367cf12cdbf->m_frame.f_lineno = 71;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_left_name_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = const_int_pos_4294967295;
        tmp_return_value = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7dccdce203e0aff37ce07367cf12cdbf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7dccdce203e0aff37ce07367cf12cdbf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7dccdce203e0aff37ce07367cf12cdbf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7dccdce203e0aff37ce07367cf12cdbf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7dccdce203e0aff37ce07367cf12cdbf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7dccdce203e0aff37ce07367cf12cdbf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7dccdce203e0aff37ce07367cf12cdbf,
        type_description_1,
        par_r,
        par_g,
        par_b,
        par_a
    );


    // Release cached frame.
    if ( frame_7dccdce203e0aff37ce07367cf12cdbf == cache_frame_7dccdce203e0aff37ce07367cf12cdbf )
    {
        Py_DECREF( frame_7dccdce203e0aff37ce07367cf12cdbf );
    }
    cache_frame_7dccdce203e0aff37ce07367cf12cdbf = NULL;

    assertFrameObject( frame_7dccdce203e0aff37ce07367cf12cdbf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_2_rgb );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    CHECK_OBJECT( (PyObject *)par_g );
    Py_DECREF( par_g );
    par_g = NULL;

    CHECK_OBJECT( (PyObject *)par_b );
    Py_DECREF( par_b );
    par_b = NULL;

    CHECK_OBJECT( (PyObject *)par_a );
    Py_DECREF( par_a );
    par_a = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    CHECK_OBJECT( (PyObject *)par_g );
    Py_DECREF( par_g );
    par_g = NULL;

    CHECK_OBJECT( (PyObject *)par_b );
    Py_DECREF( par_b );
    par_b = NULL;

    CHECK_OBJECT( (PyObject *)par_a );
    Py_DECREF( par_a );
    par_a = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_2_rgb );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageQt$$$function_3_fromqimage( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_im = python_pars[ 0 ];
    PyObject *var_buffer = NULL;
    PyObject *var_b = NULL;
    struct Nuitka_FrameObject *frame_55e248f7349a3005d1457163d79da71a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_55e248f7349a3005d1457163d79da71a = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_55e248f7349a3005d1457163d79da71a, codeobj_55e248f7349a3005d1457163d79da71a, module_PIL$ImageQt, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_55e248f7349a3005d1457163d79da71a = cache_frame_55e248f7349a3005d1457163d79da71a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_55e248f7349a3005d1457163d79da71a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_55e248f7349a3005d1457163d79da71a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QBuffer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QBuffer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QBuffer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 79;
        tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_buffer == NULL );
        var_buffer = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_buffer );
        tmp_source_name_1 = var_buffer;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_open );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QIODevice );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QIODevice );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QIODevice" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_ReadWrite );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 80;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 80;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_im );
        tmp_called_instance_1 = par_im;
        frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 83;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_hasAlphaChannel );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 83;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            CHECK_OBJECT( par_im );
            tmp_called_instance_2 = par_im;
            CHECK_OBJECT( var_buffer );
            tmp_args_element_name_2 = var_buffer;
            tmp_args_element_name_3 = const_str_plain_png;
            frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 84;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_save, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( par_im );
            tmp_called_instance_3 = par_im;
            CHECK_OBJECT( var_buffer );
            tmp_args_element_name_4 = var_buffer;
            tmp_args_element_name_5 = const_str_plain_ppm;
            frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 86;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_call_result_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_3, const_str_plain_save, call_args );
            }

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_BytesIO );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BytesIO );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BytesIO" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 88;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 88;
        tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_called_instance_4;
        CHECK_OBJECT( var_b );
        tmp_source_name_3 = var_b;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_write );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_buffer );
        tmp_called_instance_4 = var_buffer;
        frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 90;
        tmp_args_element_name_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_data );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 90;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 90;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_55e248f7349a3005d1457163d79da71a, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_55e248f7349a3005d1457163d79da71a, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_TypeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_4;
            PyObject *tmp_call_result_6;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_unicode_arg_1;
            PyObject *tmp_called_instance_5;
            CHECK_OBJECT( var_b );
            tmp_source_name_4 = var_b;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_write );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_buffer );
            tmp_called_instance_5 = var_buffer;
            frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 93;
            tmp_unicode_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_data );
            if ( tmp_unicode_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 93;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
            tmp_args_element_name_7 = PyObject_Unicode( tmp_unicode_arg_1 );
            Py_DECREF( tmp_unicode_arg_1 );
            if ( tmp_args_element_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 93;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
            frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 93;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_call_result_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_6 );
        }
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 89;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_55e248f7349a3005d1457163d79da71a->m_frame) frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooo";
        goto try_except_handler_3;
        branch_end_2:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_3_fromqimage );
    return NULL;
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_called_instance_6;
        PyObject *tmp_call_result_7;
        CHECK_OBJECT( var_buffer );
        tmp_called_instance_6 = var_buffer;
        frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 94;
        tmp_call_result_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_close );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        PyObject *tmp_called_instance_7;
        PyObject *tmp_call_result_8;
        CHECK_OBJECT( var_b );
        tmp_called_instance_7 = var_b;
        frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 95;
        tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_seek, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_call_result_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_8 );
    }
    {
        PyObject *tmp_called_instance_8;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_8 = tmp_mvar_value_4;
        CHECK_OBJECT( var_b );
        tmp_args_element_name_8 = var_b;
        frame_55e248f7349a3005d1457163d79da71a->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_open, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_55e248f7349a3005d1457163d79da71a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_55e248f7349a3005d1457163d79da71a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_55e248f7349a3005d1457163d79da71a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_55e248f7349a3005d1457163d79da71a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_55e248f7349a3005d1457163d79da71a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_55e248f7349a3005d1457163d79da71a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_55e248f7349a3005d1457163d79da71a,
        type_description_1,
        par_im,
        var_buffer,
        var_b
    );


    // Release cached frame.
    if ( frame_55e248f7349a3005d1457163d79da71a == cache_frame_55e248f7349a3005d1457163d79da71a )
    {
        Py_DECREF( frame_55e248f7349a3005d1457163d79da71a );
    }
    cache_frame_55e248f7349a3005d1457163d79da71a = NULL;

    assertFrameObject( frame_55e248f7349a3005d1457163d79da71a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_3_fromqimage );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    CHECK_OBJECT( (PyObject *)var_buffer );
    Py_DECREF( var_buffer );
    var_buffer = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    Py_XDECREF( var_buffer );
    var_buffer = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_3_fromqimage );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageQt$$$function_4_fromqpixmap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_im = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_42c90e91907a455baa9f7b66c51e22f9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_42c90e91907a455baa9f7b66c51e22f9 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_42c90e91907a455baa9f7b66c51e22f9, codeobj_42c90e91907a455baa9f7b66c51e22f9, module_PIL$ImageQt, sizeof(void *) );
    frame_42c90e91907a455baa9f7b66c51e22f9 = cache_frame_42c90e91907a455baa9f7b66c51e22f9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_42c90e91907a455baa9f7b66c51e22f9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_42c90e91907a455baa9f7b66c51e22f9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_fromqimage );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fromqimage );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fromqimage" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_im );
        tmp_args_element_name_1 = par_im;
        frame_42c90e91907a455baa9f7b66c51e22f9->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_42c90e91907a455baa9f7b66c51e22f9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_42c90e91907a455baa9f7b66c51e22f9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_42c90e91907a455baa9f7b66c51e22f9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_42c90e91907a455baa9f7b66c51e22f9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_42c90e91907a455baa9f7b66c51e22f9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_42c90e91907a455baa9f7b66c51e22f9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_42c90e91907a455baa9f7b66c51e22f9,
        type_description_1,
        par_im
    );


    // Release cached frame.
    if ( frame_42c90e91907a455baa9f7b66c51e22f9 == cache_frame_42c90e91907a455baa9f7b66c51e22f9 )
    {
        Py_DECREF( frame_42c90e91907a455baa9f7b66c51e22f9 );
    }
    cache_frame_42c90e91907a455baa9f7b66c51e22f9 = NULL;

    assertFrameObject( frame_42c90e91907a455baa9f7b66c51e22f9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_4_fromqpixmap );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_4_fromqpixmap );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageQt$$$function_5_align8to32( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_bytes = python_pars[ 0 ];
    PyObject *par_width = python_pars[ 1 ];
    PyObject *par_mode = python_pars[ 2 ];
    PyObject *var_bits_per_pixel = NULL;
    PyObject *var_bits_per_line = NULL;
    PyObject *var_full_bytes_per_line = NULL;
    PyObject *var_remaining_bits_per_line = NULL;
    PyObject *var_bytes_per_line = NULL;
    PyObject *var_extra_padding = NULL;
    PyObject *var_new_data = NULL;
    PyObject *var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_6b294a6c221fdbc575de8d51adf92460;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_6b294a6c221fdbc575de8d51adf92460 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6b294a6c221fdbc575de8d51adf92460, codeobj_6b294a6c221fdbc575de8d51adf92460, module_PIL$ImageQt, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6b294a6c221fdbc575de8d51adf92460 = cache_frame_6b294a6c221fdbc575de8d51adf92460;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6b294a6c221fdbc575de8d51adf92460 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6b294a6c221fdbc575de8d51adf92460 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        tmp_subscribed_name_1 = PyDict_Copy( const_dict_d65e53817bae224a8ca46ec5dd007f86 );
        CHECK_OBJECT( par_mode );
        tmp_subscript_name_1 = par_mode;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_bits_per_pixel == NULL );
        var_bits_per_pixel = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( var_bits_per_pixel );
        tmp_left_name_1 = var_bits_per_pixel;
        CHECK_OBJECT( par_width );
        tmp_right_name_1 = par_width;
        tmp_assign_source_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_bits_per_line == NULL );
        var_bits_per_line = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( var_bits_per_line );
        tmp_left_name_2 = var_bits_per_line;
        tmp_right_name_2 = const_int_pos_8;
        tmp_iter_arg_1 = BUILTIN_DIVMOD( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooo";
            exception_lineno = 127;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooo";
            exception_lineno = 127;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooo";
                    exception_lineno = 127;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooo";
            exception_lineno = 127;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
        assert( var_full_bytes_per_line == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_full_bytes_per_line = tmp_assign_source_6;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
        assert( var_remaining_bits_per_line == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_remaining_bits_per_line = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_full_bytes_per_line );
        tmp_left_name_3 = var_full_bytes_per_line;
        CHECK_OBJECT( var_remaining_bits_per_line );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_remaining_bits_per_line );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        tmp_right_name_3 = const_int_pos_1;
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_right_name_3 = const_int_0;
        condexpr_end_1:;
        tmp_assign_source_8 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_bytes_per_line == NULL );
        var_bytes_per_line = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_left_name_4;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_right_name_4;
        CHECK_OBJECT( var_bytes_per_line );
        tmp_operand_name_1 = var_bytes_per_line;
        tmp_left_name_4 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_4 = const_int_pos_4;
        tmp_assign_source_9 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_extra_padding == NULL );
        var_extra_padding = tmp_assign_source_9;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_2;
        CHECK_OBJECT( var_extra_padding );
        tmp_operand_name_2 = var_extra_padding;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_bytes );
        tmp_return_value = par_bytes;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = PyList_New( 0 );
        assert( var_new_data == NULL );
        var_new_data = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_xrange_low_1;
        PyObject *tmp_left_name_5;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_right_name_5;
        CHECK_OBJECT( par_bytes );
        tmp_len_arg_1 = par_bytes;
        tmp_left_name_5 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_left_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bytes_per_line );
        tmp_right_name_5 = var_bytes_per_line;
        tmp_xrange_low_1 = BINARY_OPERATION_FLOORDIV_LONG_OBJECT( tmp_left_name_5, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_5 );
        if ( tmp_xrange_low_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_iter_arg_2 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
        Py_DECREF( tmp_xrange_low_1 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_11;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_12;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_12 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooo";
                exception_lineno = 137;
                goto try_except_handler_4;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_12;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_13 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_13;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_6;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_start_name_1;
        PyObject *tmp_left_name_7;
        PyObject *tmp_right_name_6;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_left_name_8;
        PyObject *tmp_left_name_9;
        PyObject *tmp_right_name_7;
        PyObject *tmp_right_name_8;
        PyObject *tmp_step_name_1;
        PyObject *tmp_right_name_9;
        PyObject *tmp_left_name_10;
        PyObject *tmp_right_name_10;
        CHECK_OBJECT( var_new_data );
        tmp_source_name_1 = var_new_data;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( par_bytes );
        tmp_subscribed_name_2 = par_bytes;
        CHECK_OBJECT( var_i );
        tmp_left_name_7 = var_i;
        CHECK_OBJECT( var_bytes_per_line );
        tmp_right_name_6 = var_bytes_per_line;
        tmp_start_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_6 );
        if ( tmp_start_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 138;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( var_i );
        tmp_left_name_9 = var_i;
        tmp_right_name_7 = const_int_pos_1;
        tmp_left_name_8 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_9, tmp_right_name_7 );
        if ( tmp_left_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_start_name_1 );

            exception_lineno = 138;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( var_bytes_per_line );
        tmp_right_name_8 = var_bytes_per_line;
        tmp_stop_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_8, tmp_right_name_8 );
        Py_DECREF( tmp_left_name_8 );
        if ( tmp_stop_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_start_name_1 );

            exception_lineno = 138;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_2 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        Py_DECREF( tmp_start_name_1 );
        Py_DECREF( tmp_stop_name_1 );
        assert( !(tmp_subscript_name_2 == NULL) );
        tmp_left_name_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        Py_DECREF( tmp_subscript_name_2 );
        if ( tmp_left_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 138;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        tmp_left_name_10 = const_bytes_chr_0;
        CHECK_OBJECT( var_extra_padding );
        tmp_right_name_10 = var_extra_padding;
        tmp_right_name_9 = BINARY_OPERATION_MUL_BYTES_OBJECT( tmp_left_name_10, tmp_right_name_10 );
        if ( tmp_right_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_6 );

            exception_lineno = 139;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_9 );
        Py_DECREF( tmp_left_name_6 );
        Py_DECREF( tmp_right_name_9 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 138;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        frame_6b294a6c221fdbc575de8d51adf92460->m_frame.f_lineno = 138;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 137;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_4;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_2;
        tmp_called_instance_1 = const_bytes_empty;
        CHECK_OBJECT( var_new_data );
        tmp_args_element_name_2 = var_new_data;
        frame_6b294a6c221fdbc575de8d51adf92460->m_frame.f_lineno = 141;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_join, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b294a6c221fdbc575de8d51adf92460 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b294a6c221fdbc575de8d51adf92460 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b294a6c221fdbc575de8d51adf92460 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6b294a6c221fdbc575de8d51adf92460, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6b294a6c221fdbc575de8d51adf92460->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6b294a6c221fdbc575de8d51adf92460, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6b294a6c221fdbc575de8d51adf92460,
        type_description_1,
        par_bytes,
        par_width,
        par_mode,
        var_bits_per_pixel,
        var_bits_per_line,
        var_full_bytes_per_line,
        var_remaining_bits_per_line,
        var_bytes_per_line,
        var_extra_padding,
        var_new_data,
        var_i
    );


    // Release cached frame.
    if ( frame_6b294a6c221fdbc575de8d51adf92460 == cache_frame_6b294a6c221fdbc575de8d51adf92460 )
    {
        Py_DECREF( frame_6b294a6c221fdbc575de8d51adf92460 );
    }
    cache_frame_6b294a6c221fdbc575de8d51adf92460 = NULL;

    assertFrameObject( frame_6b294a6c221fdbc575de8d51adf92460 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_5_align8to32 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_bytes );
    Py_DECREF( par_bytes );
    par_bytes = NULL;

    CHECK_OBJECT( (PyObject *)par_width );
    Py_DECREF( par_width );
    par_width = NULL;

    CHECK_OBJECT( (PyObject *)par_mode );
    Py_DECREF( par_mode );
    par_mode = NULL;

    CHECK_OBJECT( (PyObject *)var_bits_per_pixel );
    Py_DECREF( var_bits_per_pixel );
    var_bits_per_pixel = NULL;

    CHECK_OBJECT( (PyObject *)var_bits_per_line );
    Py_DECREF( var_bits_per_line );
    var_bits_per_line = NULL;

    CHECK_OBJECT( (PyObject *)var_full_bytes_per_line );
    Py_DECREF( var_full_bytes_per_line );
    var_full_bytes_per_line = NULL;

    CHECK_OBJECT( (PyObject *)var_remaining_bits_per_line );
    Py_DECREF( var_remaining_bits_per_line );
    var_remaining_bits_per_line = NULL;

    CHECK_OBJECT( (PyObject *)var_bytes_per_line );
    Py_DECREF( var_bytes_per_line );
    var_bytes_per_line = NULL;

    CHECK_OBJECT( (PyObject *)var_extra_padding );
    Py_DECREF( var_extra_padding );
    var_extra_padding = NULL;

    Py_XDECREF( var_new_data );
    var_new_data = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_bytes );
    Py_DECREF( par_bytes );
    par_bytes = NULL;

    CHECK_OBJECT( (PyObject *)par_width );
    Py_DECREF( par_width );
    par_width = NULL;

    CHECK_OBJECT( (PyObject *)par_mode );
    Py_DECREF( par_mode );
    par_mode = NULL;

    Py_XDECREF( var_bits_per_pixel );
    var_bits_per_pixel = NULL;

    Py_XDECREF( var_bits_per_line );
    var_bits_per_line = NULL;

    Py_XDECREF( var_full_bytes_per_line );
    var_full_bytes_per_line = NULL;

    Py_XDECREF( var_remaining_bits_per_line );
    var_remaining_bits_per_line = NULL;

    Py_XDECREF( var_bytes_per_line );
    var_bytes_per_line = NULL;

    Py_XDECREF( var_extra_padding );
    var_extra_padding = NULL;

    Py_XDECREF( var_new_data );
    var_new_data = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_5_align8to32 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageQt$$$function_6__toqclass_helper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_im = python_pars[ 0 ];
    PyObject *var_data = NULL;
    PyObject *var_colortable = NULL;
    PyObject *var_format = NULL;
    PyObject *var_i = NULL;
    PyObject *var_palette = NULL;
    PyObject *var_r = NULL;
    PyObject *var_g = NULL;
    PyObject *var_b = NULL;
    PyObject *var_a = NULL;
    PyObject *var___data = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__element_4 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_aa8d8da8ecba6f51192b4600955be72d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    bool tmp_result;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_aa8d8da8ecba6f51192b4600955be72d = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        assert( var_data == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_data = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = Py_None;
        assert( var_colortable == NULL );
        Py_INCREF( tmp_assign_source_2 );
        var_colortable = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_aa8d8da8ecba6f51192b4600955be72d, codeobj_aa8d8da8ecba6f51192b4600955be72d, module_PIL$ImageQt, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_aa8d8da8ecba6f51192b4600955be72d = cache_frame_aa8d8da8ecba6f51192b4600955be72d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_aa8d8da8ecba6f51192b4600955be72d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_aa8d8da8ecba6f51192b4600955be72d ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_name_1;
        CHECK_OBJECT( par_im );
        tmp_source_name_1 = par_im;
        tmp_attribute_name_1 = const_str_plain_toUtf8;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_mvar_value_1;
            int tmp_truth_name_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_py3 );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_py3 );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "py3" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 151;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_mvar_value_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_unicode_arg_1;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_unicode_encoding_1;
                CHECK_OBJECT( par_im );
                tmp_called_instance_1 = par_im;
                frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 152;
                tmp_unicode_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_toUtf8 );
                if ( tmp_unicode_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 152;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_unicode_encoding_1 = const_str_digest_c075052d723d6707083e869a0e3659bb;
                tmp_assign_source_3 = TO_UNICODE3( tmp_unicode_arg_1, tmp_unicode_encoding_1, NULL );
                Py_DECREF( tmp_unicode_arg_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 152;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_im;
                    assert( old != NULL );
                    par_im = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_2;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_args_element_name_2;
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_unicode );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 154;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_1 = tmp_mvar_value_2;
                CHECK_OBJECT( par_im );
                tmp_called_instance_2 = par_im;
                frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 154;
                tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_toUtf8 );
                if ( tmp_args_element_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_element_name_2 = const_str_digest_c075052d723d6707083e869a0e3659bb;
                frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 154;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                    tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_args_element_name_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_im;
                    assert( old != NULL );
                    par_im = tmp_assign_source_4;
                    Py_DECREF( old );
                }

            }
            branch_end_2:;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        int tmp_truth_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_isPath );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_isPath );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "isPath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        if ( par_im == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = par_im;
        frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 155;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 155;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_4;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_Image );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 156;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_2 = tmp_mvar_value_4;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_open );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            if ( par_im == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 156;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_4 = par_im;
            frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 156;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_im;
                par_im = tmp_assign_source_5;
                Py_XDECREF( old );
            }

        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_3;
        if ( par_im == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 158;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = par_im;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_mode );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_5;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QImage );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QImage" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 159;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_4 = tmp_mvar_value_5;
            tmp_assign_source_6 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_Format_Mono );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 159;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_format == NULL );
            var_format = tmp_assign_source_6;
        }
        goto branch_end_4;
        branch_no_4:;
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_5;
            if ( par_im == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 160;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_5 = par_im;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_mode );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_str_plain_L;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_7;
                PyObject *tmp_source_name_6;
                PyObject *tmp_mvar_value_6;
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QImage );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QImage" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 161;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_6 = tmp_mvar_value_6;
                tmp_assign_source_7 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_Format_Indexed8 );
                if ( tmp_assign_source_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 161;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_format == NULL );
                var_format = tmp_assign_source_7;
            }
            {
                PyObject *tmp_assign_source_8;
                tmp_assign_source_8 = PyList_New( 0 );
                {
                    PyObject *old = var_colortable;
                    assert( old != NULL );
                    var_colortable = tmp_assign_source_8;
                    Py_DECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_iter_arg_1;
                tmp_iter_arg_1 = const_xrange_0_256;
                tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_1 );
                assert( !(tmp_assign_source_9 == NULL) );
                assert( tmp_for_loop_1__for_iterator == NULL );
                tmp_for_loop_1__for_iterator = tmp_assign_source_9;
            }
            // Tried code:
            loop_start_1:;
            {
                PyObject *tmp_next_source_1;
                PyObject *tmp_assign_source_10;
                CHECK_OBJECT( tmp_for_loop_1__for_iterator );
                tmp_next_source_1 = tmp_for_loop_1__for_iterator;
                tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_1 );
                if ( tmp_assign_source_10 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_1;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "ooooooooooo";
                        exception_lineno = 163;
                        goto try_except_handler_2;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_1__iter_value;
                    tmp_for_loop_1__iter_value = tmp_assign_source_10;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_11;
                CHECK_OBJECT( tmp_for_loop_1__iter_value );
                tmp_assign_source_11 = tmp_for_loop_1__iter_value;
                {
                    PyObject *old = var_i;
                    var_i = tmp_assign_source_11;
                    Py_INCREF( var_i );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_7;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_called_name_5;
                PyObject *tmp_mvar_value_7;
                PyObject *tmp_args_element_name_6;
                PyObject *tmp_args_element_name_7;
                PyObject *tmp_args_element_name_8;
                CHECK_OBJECT( var_colortable );
                tmp_source_name_7 = var_colortable;
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_append );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_rgb );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rgb );
                }

                if ( tmp_mvar_value_7 == NULL )
                {
                    Py_DECREF( tmp_called_name_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rgb" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 164;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_called_name_5 = tmp_mvar_value_7;
                CHECK_OBJECT( var_i );
                tmp_args_element_name_6 = var_i;
                CHECK_OBJECT( var_i );
                tmp_args_element_name_7 = var_i;
                CHECK_OBJECT( var_i );
                tmp_args_element_name_8 = var_i;
                frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 164;
                {
                    PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
                    tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, call_args );
                }

                if ( tmp_args_element_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_4 );

                    exception_lineno = 164;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 164;
                {
                    PyObject *call_args[] = { tmp_args_element_name_5 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                }

                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_5 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 163;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            goto loop_start_1;
            loop_end_1:;
            goto try_end_1;
            // Exception handler code:
            try_except_handler_2:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_1__iter_value );
            tmp_for_loop_1__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
            Py_DECREF( tmp_for_loop_1__for_iterator );
            tmp_for_loop_1__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_1;
            exception_value = exception_keeper_value_1;
            exception_tb = exception_keeper_tb_1;
            exception_lineno = exception_keeper_lineno_1;

            goto frame_exception_exit_1;
            // End of try:
            try_end_1:;
            Py_XDECREF( tmp_for_loop_1__iter_value );
            tmp_for_loop_1__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
            Py_DECREF( tmp_for_loop_1__for_iterator );
            tmp_for_loop_1__for_iterator = NULL;

            goto branch_end_5;
            branch_no_5:;
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                PyObject *tmp_source_name_8;
                if ( par_im == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 165;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_8 = par_im;
                tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_mode );
                if ( tmp_compexpr_left_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 165;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_3 = const_str_plain_P;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                Py_DECREF( tmp_compexpr_left_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 165;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                {
                    PyObject *tmp_assign_source_12;
                    PyObject *tmp_source_name_9;
                    PyObject *tmp_mvar_value_8;
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage );

                    if (unlikely( tmp_mvar_value_8 == NULL ))
                    {
                        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QImage );
                    }

                    if ( tmp_mvar_value_8 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QImage" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 166;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_9 = tmp_mvar_value_8;
                    tmp_assign_source_12 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_Format_Indexed8 );
                    if ( tmp_assign_source_12 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 166;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_format == NULL );
                    var_format = tmp_assign_source_12;
                }
                {
                    PyObject *tmp_assign_source_13;
                    tmp_assign_source_13 = PyList_New( 0 );
                    {
                        PyObject *old = var_colortable;
                        assert( old != NULL );
                        var_colortable = tmp_assign_source_13;
                        Py_DECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_14;
                    PyObject *tmp_called_instance_3;
                    if ( par_im == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 168;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_instance_3 = par_im;
                    frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 168;
                    tmp_assign_source_14 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_getpalette );
                    if ( tmp_assign_source_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 168;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_palette == NULL );
                    var_palette = tmp_assign_source_14;
                }
                {
                    PyObject *tmp_assign_source_15;
                    PyObject *tmp_iter_arg_2;
                    PyObject *tmp_xrange_low_1;
                    PyObject *tmp_xrange_high_1;
                    PyObject *tmp_len_arg_1;
                    PyObject *tmp_xrange_step_1;
                    tmp_xrange_low_1 = const_int_0;
                    CHECK_OBJECT( var_palette );
                    tmp_len_arg_1 = var_palette;
                    tmp_xrange_high_1 = BUILTIN_LEN( tmp_len_arg_1 );
                    if ( tmp_xrange_high_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 169;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_xrange_step_1 = const_int_pos_3;
                    tmp_iter_arg_2 = BUILTIN_XRANGE3( tmp_xrange_low_1, tmp_xrange_high_1, tmp_xrange_step_1 );
                    Py_DECREF( tmp_xrange_high_1 );
                    if ( tmp_iter_arg_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 169;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_15 = MAKE_ITERATOR( tmp_iter_arg_2 );
                    Py_DECREF( tmp_iter_arg_2 );
                    if ( tmp_assign_source_15 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 169;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( tmp_for_loop_2__for_iterator == NULL );
                    tmp_for_loop_2__for_iterator = tmp_assign_source_15;
                }
                // Tried code:
                loop_start_2:;
                {
                    PyObject *tmp_next_source_2;
                    PyObject *tmp_assign_source_16;
                    CHECK_OBJECT( tmp_for_loop_2__for_iterator );
                    tmp_next_source_2 = tmp_for_loop_2__for_iterator;
                    tmp_assign_source_16 = ITERATOR_NEXT( tmp_next_source_2 );
                    if ( tmp_assign_source_16 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_2;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "ooooooooooo";
                            exception_lineno = 169;
                            goto try_except_handler_3;
                        }
                    }

                    {
                        PyObject *old = tmp_for_loop_2__iter_value;
                        tmp_for_loop_2__iter_value = tmp_assign_source_16;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_17;
                    CHECK_OBJECT( tmp_for_loop_2__iter_value );
                    tmp_assign_source_17 = tmp_for_loop_2__iter_value;
                    {
                        PyObject *old = var_i;
                        var_i = tmp_assign_source_17;
                        Py_INCREF( var_i );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_called_name_6;
                    PyObject *tmp_source_name_10;
                    PyObject *tmp_call_result_3;
                    PyObject *tmp_args_element_name_9;
                    PyObject *tmp_dircall_arg1_1;
                    PyObject *tmp_mvar_value_9;
                    PyObject *tmp_dircall_arg2_1;
                    PyObject *tmp_subscribed_name_1;
                    PyObject *tmp_subscript_name_1;
                    PyObject *tmp_start_name_1;
                    PyObject *tmp_stop_name_1;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_step_name_1;
                    CHECK_OBJECT( var_colortable );
                    tmp_source_name_10 = var_colortable;
                    tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_append );
                    if ( tmp_called_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 170;
                        type_description_1 = "ooooooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_rgb );

                    if (unlikely( tmp_mvar_value_9 == NULL ))
                    {
                        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rgb );
                    }

                    if ( tmp_mvar_value_9 == NULL )
                    {
                        Py_DECREF( tmp_called_name_6 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rgb" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 170;
                        type_description_1 = "ooooooooooo";
                        goto try_except_handler_3;
                    }

                    tmp_dircall_arg1_1 = tmp_mvar_value_9;
                    CHECK_OBJECT( var_palette );
                    tmp_subscribed_name_1 = var_palette;
                    CHECK_OBJECT( var_i );
                    tmp_start_name_1 = var_i;
                    CHECK_OBJECT( var_i );
                    tmp_left_name_1 = var_i;
                    tmp_right_name_1 = const_int_pos_3;
                    tmp_stop_name_1 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
                    if ( tmp_stop_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_6 );

                        exception_lineno = 170;
                        type_description_1 = "ooooooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_step_name_1 = Py_None;
                    tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
                    Py_DECREF( tmp_stop_name_1 );
                    assert( !(tmp_subscript_name_1 == NULL) );
                    tmp_dircall_arg2_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                    Py_DECREF( tmp_subscript_name_1 );
                    if ( tmp_dircall_arg2_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_6 );

                        exception_lineno = 170;
                        type_description_1 = "ooooooooooo";
                        goto try_except_handler_3;
                    }
                    Py_INCREF( tmp_dircall_arg1_1 );

                    {
                        PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
                        tmp_args_element_name_9 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
                    }
                    if ( tmp_args_element_name_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_6 );

                        exception_lineno = 170;
                        type_description_1 = "ooooooooooo";
                        goto try_except_handler_3;
                    }
                    frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 170;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_9 };
                        tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
                    }

                    Py_DECREF( tmp_called_name_6 );
                    Py_DECREF( tmp_args_element_name_9 );
                    if ( tmp_call_result_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 170;
                        type_description_1 = "ooooooooooo";
                        goto try_except_handler_3;
                    }
                    Py_DECREF( tmp_call_result_3 );
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 169;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_3;
                }
                goto loop_start_2;
                loop_end_2:;
                goto try_end_2;
                // Exception handler code:
                try_except_handler_3:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_for_loop_2__iter_value );
                tmp_for_loop_2__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
                Py_DECREF( tmp_for_loop_2__for_iterator );
                tmp_for_loop_2__for_iterator = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto frame_exception_exit_1;
                // End of try:
                try_end_2:;
                Py_XDECREF( tmp_for_loop_2__iter_value );
                tmp_for_loop_2__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
                Py_DECREF( tmp_for_loop_2__for_iterator );
                tmp_for_loop_2__for_iterator = NULL;

                goto branch_end_6;
                branch_no_6:;
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_compexpr_left_4;
                    PyObject *tmp_compexpr_right_4;
                    PyObject *tmp_source_name_11;
                    if ( par_im == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 171;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_11 = par_im;
                    tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_mode );
                    if ( tmp_compexpr_left_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 171;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_4 = const_str_plain_RGB;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                    Py_DECREF( tmp_compexpr_left_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 171;
                        type_description_1 = "ooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_7;
                    }
                    else
                    {
                        goto branch_no_7;
                    }
                    branch_yes_7:;
                    {
                        PyObject *tmp_assign_source_18;
                        PyObject *tmp_called_instance_4;
                        if ( par_im == NULL )
                        {

                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 172;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_instance_4 = par_im;
                        frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 172;
                        tmp_assign_source_18 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_tobytes, &PyTuple_GET_ITEM( const_tuple_str_plain_raw_str_plain_BGRX_tuple, 0 ) );

                        if ( tmp_assign_source_18 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 172;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        {
                            PyObject *old = var_data;
                            assert( old != NULL );
                            var_data = tmp_assign_source_18;
                            Py_DECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_19;
                        PyObject *tmp_source_name_12;
                        PyObject *tmp_mvar_value_10;
                        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage );

                        if (unlikely( tmp_mvar_value_10 == NULL ))
                        {
                            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QImage );
                        }

                        if ( tmp_mvar_value_10 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QImage" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 173;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_source_name_12 = tmp_mvar_value_10;
                        tmp_assign_source_19 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_Format_RGB32 );
                        if ( tmp_assign_source_19 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 173;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        assert( var_format == NULL );
                        var_format = tmp_assign_source_19;
                    }
                    goto branch_end_7;
                    branch_no_7:;
                    {
                        nuitka_bool tmp_condition_result_8;
                        PyObject *tmp_compexpr_left_5;
                        PyObject *tmp_compexpr_right_5;
                        PyObject *tmp_source_name_13;
                        if ( par_im == NULL )
                        {

                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 174;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_source_name_13 = par_im;
                        tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_mode );
                        if ( tmp_compexpr_left_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 174;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_compexpr_right_5 = const_str_plain_RGBA;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                        Py_DECREF( tmp_compexpr_left_5 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 174;
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_8;
                        }
                        else
                        {
                            goto branch_no_8;
                        }
                        branch_yes_8:;
                        // Tried code:
                        {
                            PyObject *tmp_assign_source_20;
                            PyObject *tmp_called_instance_5;
                            if ( par_im == NULL )
                            {

                                exception_type = PyExc_UnboundLocalError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 176;
                                type_description_1 = "ooooooooooo";
                                goto try_except_handler_4;
                            }

                            tmp_called_instance_5 = par_im;
                            frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 176;
                            tmp_assign_source_20 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_5, const_str_plain_tobytes, &PyTuple_GET_ITEM( const_tuple_str_plain_raw_str_plain_BGRA_tuple, 0 ) );

                            if ( tmp_assign_source_20 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 176;
                                type_description_1 = "ooooooooooo";
                                goto try_except_handler_4;
                            }
                            {
                                PyObject *old = var_data;
                                assert( old != NULL );
                                var_data = tmp_assign_source_20;
                                Py_DECREF( old );
                            }

                        }
                        goto try_end_3;
                        // Exception handler code:
                        try_except_handler_4:;
                        exception_keeper_type_3 = exception_type;
                        exception_keeper_value_3 = exception_value;
                        exception_keeper_tb_3 = exception_tb;
                        exception_keeper_lineno_3 = exception_lineno;
                        exception_type = NULL;
                        exception_value = NULL;
                        exception_tb = NULL;
                        exception_lineno = 0;

                        // Preserve existing published exception.
                        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
                        Py_XINCREF( exception_preserved_type_1 );
                        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
                        Py_XINCREF( exception_preserved_value_1 );
                        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
                        Py_XINCREF( exception_preserved_tb_1 );

                        if ( exception_keeper_tb_3 == NULL )
                        {
                            exception_keeper_tb_3 = MAKE_TRACEBACK( frame_aa8d8da8ecba6f51192b4600955be72d, exception_keeper_lineno_3 );
                        }
                        else if ( exception_keeper_lineno_3 != 0 )
                        {
                            exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_aa8d8da8ecba6f51192b4600955be72d, exception_keeper_lineno_3 );
                        }

                        NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
                        PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
                        PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
                        // Tried code:
                        {
                            nuitka_bool tmp_condition_result_9;
                            PyObject *tmp_compexpr_left_6;
                            PyObject *tmp_compexpr_right_6;
                            tmp_compexpr_left_6 = EXC_TYPE(PyThreadState_GET());
                            tmp_compexpr_right_6 = PyExc_SystemError;
                            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 177;
                                type_description_1 = "ooooooooooo";
                                goto try_except_handler_5;
                            }
                            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_9;
                            }
                            else
                            {
                                goto branch_no_9;
                            }
                            branch_yes_9:;
                            // Tried code:
                            {
                                PyObject *tmp_assign_source_21;
                                PyObject *tmp_iter_arg_3;
                                PyObject *tmp_called_instance_6;
                                if ( par_im == NULL )
                                {

                                    exception_type = PyExc_UnboundLocalError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 179;
                                    type_description_1 = "ooooooooooo";
                                    goto try_except_handler_6;
                                }

                                tmp_called_instance_6 = par_im;
                                frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 179;
                                tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_split );
                                if ( tmp_iter_arg_3 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 179;
                                    type_description_1 = "ooooooooooo";
                                    goto try_except_handler_6;
                                }
                                tmp_assign_source_21 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
                                Py_DECREF( tmp_iter_arg_3 );
                                if ( tmp_assign_source_21 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 179;
                                    type_description_1 = "ooooooooooo";
                                    goto try_except_handler_6;
                                }
                                assert( tmp_tuple_unpack_1__source_iter == NULL );
                                tmp_tuple_unpack_1__source_iter = tmp_assign_source_21;
                            }
                            // Tried code:
                            {
                                PyObject *tmp_assign_source_22;
                                PyObject *tmp_unpack_1;
                                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                                tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
                                tmp_assign_source_22 = UNPACK_NEXT( tmp_unpack_1, 0, 4 );
                                if ( tmp_assign_source_22 == NULL )
                                {
                                    if ( !ERROR_OCCURRED() )
                                    {
                                        exception_type = PyExc_StopIteration;
                                        Py_INCREF( exception_type );
                                        exception_value = NULL;
                                        exception_tb = NULL;
                                    }
                                    else
                                    {
                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                    }


                                    type_description_1 = "ooooooooooo";
                                    exception_lineno = 179;
                                    goto try_except_handler_7;
                                }
                                assert( tmp_tuple_unpack_1__element_1 == NULL );
                                tmp_tuple_unpack_1__element_1 = tmp_assign_source_22;
                            }
                            {
                                PyObject *tmp_assign_source_23;
                                PyObject *tmp_unpack_2;
                                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                                tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
                                tmp_assign_source_23 = UNPACK_NEXT( tmp_unpack_2, 1, 4 );
                                if ( tmp_assign_source_23 == NULL )
                                {
                                    if ( !ERROR_OCCURRED() )
                                    {
                                        exception_type = PyExc_StopIteration;
                                        Py_INCREF( exception_type );
                                        exception_value = NULL;
                                        exception_tb = NULL;
                                    }
                                    else
                                    {
                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                    }


                                    type_description_1 = "ooooooooooo";
                                    exception_lineno = 179;
                                    goto try_except_handler_7;
                                }
                                assert( tmp_tuple_unpack_1__element_2 == NULL );
                                tmp_tuple_unpack_1__element_2 = tmp_assign_source_23;
                            }
                            {
                                PyObject *tmp_assign_source_24;
                                PyObject *tmp_unpack_3;
                                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                                tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
                                tmp_assign_source_24 = UNPACK_NEXT( tmp_unpack_3, 2, 4 );
                                if ( tmp_assign_source_24 == NULL )
                                {
                                    if ( !ERROR_OCCURRED() )
                                    {
                                        exception_type = PyExc_StopIteration;
                                        Py_INCREF( exception_type );
                                        exception_value = NULL;
                                        exception_tb = NULL;
                                    }
                                    else
                                    {
                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                    }


                                    type_description_1 = "ooooooooooo";
                                    exception_lineno = 179;
                                    goto try_except_handler_7;
                                }
                                assert( tmp_tuple_unpack_1__element_3 == NULL );
                                tmp_tuple_unpack_1__element_3 = tmp_assign_source_24;
                            }
                            {
                                PyObject *tmp_assign_source_25;
                                PyObject *tmp_unpack_4;
                                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                                tmp_unpack_4 = tmp_tuple_unpack_1__source_iter;
                                tmp_assign_source_25 = UNPACK_NEXT( tmp_unpack_4, 3, 4 );
                                if ( tmp_assign_source_25 == NULL )
                                {
                                    if ( !ERROR_OCCURRED() )
                                    {
                                        exception_type = PyExc_StopIteration;
                                        Py_INCREF( exception_type );
                                        exception_value = NULL;
                                        exception_tb = NULL;
                                    }
                                    else
                                    {
                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                    }


                                    type_description_1 = "ooooooooooo";
                                    exception_lineno = 179;
                                    goto try_except_handler_7;
                                }
                                assert( tmp_tuple_unpack_1__element_4 == NULL );
                                tmp_tuple_unpack_1__element_4 = tmp_assign_source_25;
                            }
                            {
                                PyObject *tmp_iterator_name_1;
                                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                                tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
                                // Check if iterator has left-over elements.
                                CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

                                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

                                if (likely( tmp_iterator_attempt == NULL ))
                                {
                                    PyObject *error = GET_ERROR_OCCURRED();

                                    if ( error != NULL )
                                    {
                                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                                        {
                                            CLEAR_ERROR_OCCURRED();
                                        }
                                        else
                                        {
                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                            type_description_1 = "ooooooooooo";
                                            exception_lineno = 179;
                                            goto try_except_handler_7;
                                        }
                                    }
                                }
                                else
                                {
                                    Py_DECREF( tmp_iterator_attempt );

                                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 4)" );
#endif
                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                    type_description_1 = "ooooooooooo";
                                    exception_lineno = 179;
                                    goto try_except_handler_7;
                                }
                            }
                            goto try_end_4;
                            // Exception handler code:
                            try_except_handler_7:;
                            exception_keeper_type_4 = exception_type;
                            exception_keeper_value_4 = exception_value;
                            exception_keeper_tb_4 = exception_tb;
                            exception_keeper_lineno_4 = exception_lineno;
                            exception_type = NULL;
                            exception_value = NULL;
                            exception_tb = NULL;
                            exception_lineno = 0;

                            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
                            Py_DECREF( tmp_tuple_unpack_1__source_iter );
                            tmp_tuple_unpack_1__source_iter = NULL;

                            // Re-raise.
                            exception_type = exception_keeper_type_4;
                            exception_value = exception_keeper_value_4;
                            exception_tb = exception_keeper_tb_4;
                            exception_lineno = exception_keeper_lineno_4;

                            goto try_except_handler_6;
                            // End of try:
                            try_end_4:;
                            goto try_end_5;
                            // Exception handler code:
                            try_except_handler_6:;
                            exception_keeper_type_5 = exception_type;
                            exception_keeper_value_5 = exception_value;
                            exception_keeper_tb_5 = exception_tb;
                            exception_keeper_lineno_5 = exception_lineno;
                            exception_type = NULL;
                            exception_value = NULL;
                            exception_tb = NULL;
                            exception_lineno = 0;

                            Py_XDECREF( tmp_tuple_unpack_1__element_1 );
                            tmp_tuple_unpack_1__element_1 = NULL;

                            Py_XDECREF( tmp_tuple_unpack_1__element_2 );
                            tmp_tuple_unpack_1__element_2 = NULL;

                            Py_XDECREF( tmp_tuple_unpack_1__element_3 );
                            tmp_tuple_unpack_1__element_3 = NULL;

                            Py_XDECREF( tmp_tuple_unpack_1__element_4 );
                            tmp_tuple_unpack_1__element_4 = NULL;

                            // Re-raise.
                            exception_type = exception_keeper_type_5;
                            exception_value = exception_keeper_value_5;
                            exception_tb = exception_keeper_tb_5;
                            exception_lineno = exception_keeper_lineno_5;

                            goto try_except_handler_5;
                            // End of try:
                            try_end_5:;
                            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
                            Py_DECREF( tmp_tuple_unpack_1__source_iter );
                            tmp_tuple_unpack_1__source_iter = NULL;

                            {
                                PyObject *tmp_assign_source_26;
                                CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
                                tmp_assign_source_26 = tmp_tuple_unpack_1__element_1;
                                assert( var_r == NULL );
                                Py_INCREF( tmp_assign_source_26 );
                                var_r = tmp_assign_source_26;
                            }
                            Py_XDECREF( tmp_tuple_unpack_1__element_1 );
                            tmp_tuple_unpack_1__element_1 = NULL;

                            {
                                PyObject *tmp_assign_source_27;
                                CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
                                tmp_assign_source_27 = tmp_tuple_unpack_1__element_2;
                                assert( var_g == NULL );
                                Py_INCREF( tmp_assign_source_27 );
                                var_g = tmp_assign_source_27;
                            }
                            Py_XDECREF( tmp_tuple_unpack_1__element_2 );
                            tmp_tuple_unpack_1__element_2 = NULL;

                            {
                                PyObject *tmp_assign_source_28;
                                CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
                                tmp_assign_source_28 = tmp_tuple_unpack_1__element_3;
                                assert( var_b == NULL );
                                Py_INCREF( tmp_assign_source_28 );
                                var_b = tmp_assign_source_28;
                            }
                            Py_XDECREF( tmp_tuple_unpack_1__element_3 );
                            tmp_tuple_unpack_1__element_3 = NULL;

                            {
                                PyObject *tmp_assign_source_29;
                                CHECK_OBJECT( tmp_tuple_unpack_1__element_4 );
                                tmp_assign_source_29 = tmp_tuple_unpack_1__element_4;
                                assert( var_a == NULL );
                                Py_INCREF( tmp_assign_source_29 );
                                var_a = tmp_assign_source_29;
                            }
                            Py_XDECREF( tmp_tuple_unpack_1__element_4 );
                            tmp_tuple_unpack_1__element_4 = NULL;

                            {
                                PyObject *tmp_assign_source_30;
                                PyObject *tmp_called_instance_7;
                                PyObject *tmp_mvar_value_11;
                                PyObject *tmp_args_element_name_10;
                                PyObject *tmp_args_element_name_11;
                                PyObject *tmp_tuple_element_1;
                                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_Image );

                                if (unlikely( tmp_mvar_value_11 == NULL ))
                                {
                                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
                                }

                                if ( tmp_mvar_value_11 == NULL )
                                {

                                    exception_type = PyExc_NameError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 180;
                                    type_description_1 = "ooooooooooo";
                                    goto try_except_handler_5;
                                }

                                tmp_called_instance_7 = tmp_mvar_value_11;
                                tmp_args_element_name_10 = const_str_plain_RGBA;
                                CHECK_OBJECT( var_b );
                                tmp_tuple_element_1 = var_b;
                                tmp_args_element_name_11 = PyTuple_New( 4 );
                                Py_INCREF( tmp_tuple_element_1 );
                                PyTuple_SET_ITEM( tmp_args_element_name_11, 0, tmp_tuple_element_1 );
                                CHECK_OBJECT( var_g );
                                tmp_tuple_element_1 = var_g;
                                Py_INCREF( tmp_tuple_element_1 );
                                PyTuple_SET_ITEM( tmp_args_element_name_11, 1, tmp_tuple_element_1 );
                                CHECK_OBJECT( var_r );
                                tmp_tuple_element_1 = var_r;
                                Py_INCREF( tmp_tuple_element_1 );
                                PyTuple_SET_ITEM( tmp_args_element_name_11, 2, tmp_tuple_element_1 );
                                CHECK_OBJECT( var_a );
                                tmp_tuple_element_1 = var_a;
                                Py_INCREF( tmp_tuple_element_1 );
                                PyTuple_SET_ITEM( tmp_args_element_name_11, 3, tmp_tuple_element_1 );
                                frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 180;
                                {
                                    PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11 };
                                    tmp_assign_source_30 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_7, const_str_plain_merge, call_args );
                                }

                                Py_DECREF( tmp_args_element_name_11 );
                                if ( tmp_assign_source_30 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 180;
                                    type_description_1 = "ooooooooooo";
                                    goto try_except_handler_5;
                                }
                                {
                                    PyObject *old = par_im;
                                    par_im = tmp_assign_source_30;
                                    Py_XDECREF( old );
                                }

                            }
                            goto branch_end_9;
                            branch_no_9:;
                            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            if (unlikely( tmp_result == false ))
                            {
                                exception_lineno = 175;
                            }

                            if (exception_tb && exception_tb->tb_frame == &frame_aa8d8da8ecba6f51192b4600955be72d->m_frame) frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = exception_tb->tb_lineno;
                            type_description_1 = "ooooooooooo";
                            goto try_except_handler_5;
                            branch_end_9:;
                        }
                        goto try_end_6;
                        // Exception handler code:
                        try_except_handler_5:;
                        exception_keeper_type_6 = exception_type;
                        exception_keeper_value_6 = exception_value;
                        exception_keeper_tb_6 = exception_tb;
                        exception_keeper_lineno_6 = exception_lineno;
                        exception_type = NULL;
                        exception_value = NULL;
                        exception_tb = NULL;
                        exception_lineno = 0;

                        // Restore previous exception.
                        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
                        // Re-raise.
                        exception_type = exception_keeper_type_6;
                        exception_value = exception_keeper_value_6;
                        exception_tb = exception_keeper_tb_6;
                        exception_lineno = exception_keeper_lineno_6;

                        goto frame_exception_exit_1;
                        // End of try:
                        try_end_6:;
                        // Restore previous exception.
                        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
                        goto try_end_3;
                        // exception handler codes exits in all cases
                        NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_6__toqclass_helper );
                        return NULL;
                        // End of try:
                        try_end_3:;
                        {
                            PyObject *tmp_assign_source_31;
                            PyObject *tmp_source_name_14;
                            PyObject *tmp_mvar_value_12;
                            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage );

                            if (unlikely( tmp_mvar_value_12 == NULL ))
                            {
                                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QImage );
                            }

                            if ( tmp_mvar_value_12 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QImage" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 181;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }

                            tmp_source_name_14 = tmp_mvar_value_12;
                            tmp_assign_source_31 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_Format_ARGB32 );
                            if ( tmp_assign_source_31 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 181;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            assert( var_format == NULL );
                            var_format = tmp_assign_source_31;
                        }
                        goto branch_end_8;
                        branch_no_8:;
                        {
                            PyObject *tmp_raise_type_1;
                            PyObject *tmp_make_exception_arg_1;
                            PyObject *tmp_left_name_2;
                            PyObject *tmp_right_name_2;
                            PyObject *tmp_source_name_15;
                            tmp_left_name_2 = const_str_digest_f9bfb6fba4ce859d971e49ded4f308c4;
                            if ( par_im == NULL )
                            {

                                exception_type = PyExc_UnboundLocalError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 183;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }

                            tmp_source_name_15 = par_im;
                            tmp_right_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_mode );
                            if ( tmp_right_name_2 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 183;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                            Py_DECREF( tmp_right_name_2 );
                            if ( tmp_make_exception_arg_1 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 183;
                                type_description_1 = "ooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 183;
                            {
                                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                            }

                            Py_DECREF( tmp_make_exception_arg_1 );
                            assert( !(tmp_raise_type_1 == NULL) );
                            exception_type = tmp_raise_type_1;
                            exception_lineno = 183;
                            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "ooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        branch_end_8:;
                    }
                    branch_end_7:;
                }
                branch_end_6:;
            }
            branch_end_5:;
        }
        branch_end_4:;
    }
    {
        PyObject *tmp_assign_source_32;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_instance_8;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_source_name_16;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_source_name_17;
        if ( var_data == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_or_left_value_1 = var_data;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_align8to32 );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_align8to32 );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "align8to32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_13;
        if ( par_im == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_8 = par_im;
        frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 185;
        tmp_args_element_name_12 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_tobytes );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        if ( par_im == NULL )
        {
            Py_DECREF( tmp_args_element_name_12 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_16 = par_im;
        tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_size );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_12 );

            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_2 = const_int_0;
        tmp_args_element_name_13 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_12 );

            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        if ( par_im == NULL )
        {
            Py_DECREF( tmp_args_element_name_12 );
            Py_DECREF( tmp_args_element_name_13 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_17 = par_im;
        tmp_args_element_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_mode );
        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_12 );
            Py_DECREF( tmp_args_element_name_13 );

            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_aa8d8da8ecba6f51192b4600955be72d->m_frame.f_lineno = 185;
        {
            PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
            tmp_or_right_value_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_args_element_name_12 );
        Py_DECREF( tmp_args_element_name_13 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_or_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_32 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_assign_source_32 = tmp_or_left_value_1;
        or_end_1:;
        assert( var___data == NULL );
        var___data = tmp_assign_source_32;
    }
    {
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        tmp_dict_key_1 = const_str_plain_data;
        CHECK_OBJECT( var___data );
        tmp_dict_value_1 = var___data;
        tmp_return_value = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_im;
        if ( par_im == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "im" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 187;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_2 = par_im;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_format;
        if ( var_format == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "format" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 187;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_3 = var_format;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_colortable;
        if ( var_colortable == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "colortable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 187;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_4 = var_colortable;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa8d8da8ecba6f51192b4600955be72d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa8d8da8ecba6f51192b4600955be72d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa8d8da8ecba6f51192b4600955be72d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_aa8d8da8ecba6f51192b4600955be72d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_aa8d8da8ecba6f51192b4600955be72d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_aa8d8da8ecba6f51192b4600955be72d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_aa8d8da8ecba6f51192b4600955be72d,
        type_description_1,
        par_im,
        var_data,
        var_colortable,
        var_format,
        var_i,
        var_palette,
        var_r,
        var_g,
        var_b,
        var_a,
        var___data
    );


    // Release cached frame.
    if ( frame_aa8d8da8ecba6f51192b4600955be72d == cache_frame_aa8d8da8ecba6f51192b4600955be72d )
    {
        Py_DECREF( frame_aa8d8da8ecba6f51192b4600955be72d );
    }
    cache_frame_aa8d8da8ecba6f51192b4600955be72d = NULL;

    assertFrameObject( frame_aa8d8da8ecba6f51192b4600955be72d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_6__toqclass_helper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_im );
    par_im = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    Py_XDECREF( var_colortable );
    var_colortable = NULL;

    Py_XDECREF( var_format );
    var_format = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_palette );
    var_palette = NULL;

    Py_XDECREF( var_r );
    var_r = NULL;

    Py_XDECREF( var_g );
    var_g = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    CHECK_OBJECT( (PyObject *)var___data );
    Py_DECREF( var___data );
    var___data = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_im );
    par_im = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    Py_XDECREF( var_colortable );
    var_colortable = NULL;

    Py_XDECREF( var_format );
    var_format = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_palette );
    var_palette = NULL;

    Py_XDECREF( var_r );
    var_r = NULL;

    Py_XDECREF( var_g );
    var_g = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    Py_XDECREF( var___data );
    var___data = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_6__toqclass_helper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageQt$$$function_7___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_im = python_pars[ 1 ];
    PyObject *var_im_data = NULL;
    struct Nuitka_FrameObject *frame_8f2c54b91515721f2199c44813e24fa5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_8f2c54b91515721f2199c44813e24fa5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8f2c54b91515721f2199c44813e24fa5, codeobj_8f2c54b91515721f2199c44813e24fa5, module_PIL$ImageQt, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8f2c54b91515721f2199c44813e24fa5 = cache_frame_8f2c54b91515721f2199c44813e24fa5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8f2c54b91515721f2199c44813e24fa5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8f2c54b91515721f2199c44813e24fa5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain__toqclass_helper );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__toqclass_helper );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_toqclass_helper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 202;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_im );
        tmp_args_element_name_1 = par_im;
        frame_8f2c54b91515721f2199c44813e24fa5->m_frame.f_lineno = 202;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_im_data == NULL );
        var_im_data = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_im_data );
        tmp_subscribed_name_1 = var_im_data;
        tmp_subscript_name_1 = const_str_plain_data;
        tmp_assattr_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__ImageQt__data, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_source_name_4;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_subscript_name_6;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QImage );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QImage" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 208;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_2;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_args_element_name_2 = par_self;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__ImageQt__data );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 209;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_im_data );
        tmp_subscribed_name_3 = var_im_data;
        tmp_subscript_name_2 = const_str_plain_im;
        tmp_source_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_2 );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 209;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_size );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 209;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_3 = const_int_0;
        tmp_args_element_name_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_3, 0 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 209;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_im_data );
        tmp_subscribed_name_5 = var_im_data;
        tmp_subscript_name_4 = const_str_plain_im;
        tmp_source_name_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_4 );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_4 );

            exception_lineno = 210;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_subscribed_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_size );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_subscribed_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_4 );

            exception_lineno = 210;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_5 = const_int_pos_1;
        tmp_args_element_name_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_5, 1 );
        Py_DECREF( tmp_subscribed_name_4 );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_4 );

            exception_lineno = 210;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_im_data );
        tmp_subscribed_name_6 = var_im_data;
        tmp_subscript_name_6 = const_str_plain_format;
        tmp_args_element_name_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_4 );
            Py_DECREF( tmp_args_element_name_5 );

            exception_lineno = 210;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_8f2c54b91515721f2199c44813e24fa5->m_frame.f_lineno = 208;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        Py_DECREF( tmp_args_element_name_5 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_subscribed_name_7;
        PyObject *tmp_subscript_name_7;
        PyObject *tmp_subscript_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_im_data );
        tmp_subscribed_name_7 = var_im_data;
        tmp_subscript_name_7 = const_str_plain_colortable;
        tmp_subscript_result_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
        if ( tmp_subscript_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_subscript_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscript_result_1 );

            exception_lineno = 211;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_subscript_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_5;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_subscribed_name_8;
            PyObject *tmp_subscript_name_8;
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_setColorTable );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_im_data );
            tmp_subscribed_name_8 = var_im_data;
            tmp_subscript_name_8 = const_str_plain_colortable;
            tmp_args_element_name_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_8 );
            if ( tmp_args_element_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 212;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_8f2c54b91515721f2199c44813e24fa5->m_frame.f_lineno = 212;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f2c54b91515721f2199c44813e24fa5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f2c54b91515721f2199c44813e24fa5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8f2c54b91515721f2199c44813e24fa5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8f2c54b91515721f2199c44813e24fa5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8f2c54b91515721f2199c44813e24fa5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8f2c54b91515721f2199c44813e24fa5,
        type_description_1,
        par_self,
        par_im,
        var_im_data
    );


    // Release cached frame.
    if ( frame_8f2c54b91515721f2199c44813e24fa5 == cache_frame_8f2c54b91515721f2199c44813e24fa5 )
    {
        Py_DECREF( frame_8f2c54b91515721f2199c44813e24fa5 );
    }
    cache_frame_8f2c54b91515721f2199c44813e24fa5 = NULL;

    assertFrameObject( frame_8f2c54b91515721f2199c44813e24fa5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_7___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    CHECK_OBJECT( (PyObject *)var_im_data );
    Py_DECREF( var_im_data );
    var_im_data = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    Py_XDECREF( var_im_data );
    var_im_data = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_7___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageQt$$$function_8_toqimage( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_im = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_558e4da1872eab7221eac3f845dca723;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_558e4da1872eab7221eac3f845dca723 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_558e4da1872eab7221eac3f845dca723, codeobj_558e4da1872eab7221eac3f845dca723, module_PIL$ImageQt, sizeof(void *) );
    frame_558e4da1872eab7221eac3f845dca723 = cache_frame_558e4da1872eab7221eac3f845dca723;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_558e4da1872eab7221eac3f845dca723 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_558e4da1872eab7221eac3f845dca723 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_ImageQt );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ImageQt );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ImageQt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_im );
        tmp_args_element_name_1 = par_im;
        frame_558e4da1872eab7221eac3f845dca723->m_frame.f_lineno = 216;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_558e4da1872eab7221eac3f845dca723 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_558e4da1872eab7221eac3f845dca723 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_558e4da1872eab7221eac3f845dca723 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_558e4da1872eab7221eac3f845dca723, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_558e4da1872eab7221eac3f845dca723->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_558e4da1872eab7221eac3f845dca723, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_558e4da1872eab7221eac3f845dca723,
        type_description_1,
        par_im
    );


    // Release cached frame.
    if ( frame_558e4da1872eab7221eac3f845dca723 == cache_frame_558e4da1872eab7221eac3f845dca723 )
    {
        Py_DECREF( frame_558e4da1872eab7221eac3f845dca723 );
    }
    cache_frame_558e4da1872eab7221eac3f845dca723 = NULL;

    assertFrameObject( frame_558e4da1872eab7221eac3f845dca723 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_8_toqimage );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_8_toqimage );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageQt$$$function_9_toqpixmap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_im = python_pars[ 0 ];
    PyObject *var_qimage = NULL;
    struct Nuitka_FrameObject *frame_dfd9f4c88e2a6575fbf33c5da893641b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_dfd9f4c88e2a6575fbf33c5da893641b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dfd9f4c88e2a6575fbf33c5da893641b, codeobj_dfd9f4c88e2a6575fbf33c5da893641b, module_PIL$ImageQt, sizeof(void *)+sizeof(void *) );
    frame_dfd9f4c88e2a6575fbf33c5da893641b = cache_frame_dfd9f4c88e2a6575fbf33c5da893641b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dfd9f4c88e2a6575fbf33c5da893641b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dfd9f4c88e2a6575fbf33c5da893641b ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_im );
        tmp_source_name_1 = par_im;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_mode );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_RGB;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_im );
            tmp_called_instance_1 = par_im;
            frame_dfd9f4c88e2a6575fbf33c5da893641b->m_frame.f_lineno = 226;
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_convert, &PyTuple_GET_ITEM( const_tuple_str_plain_RGBA_tuple, 0 ) );

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 226;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_im;
                assert( old != NULL );
                par_im = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_toqimage );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_toqimage );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "toqimage" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 228;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_im );
        tmp_args_element_name_1 = par_im;
        frame_dfd9f4c88e2a6575fbf33c5da893641b->m_frame.f_lineno = 228;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_qimage == NULL );
        var_qimage = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QPixmap );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QPixmap );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QPixmap" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 229;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        CHECK_OBJECT( var_qimage );
        tmp_args_element_name_2 = var_qimage;
        frame_dfd9f4c88e2a6575fbf33c5da893641b->m_frame.f_lineno = 229;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_fromImage, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 229;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dfd9f4c88e2a6575fbf33c5da893641b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dfd9f4c88e2a6575fbf33c5da893641b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dfd9f4c88e2a6575fbf33c5da893641b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dfd9f4c88e2a6575fbf33c5da893641b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dfd9f4c88e2a6575fbf33c5da893641b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dfd9f4c88e2a6575fbf33c5da893641b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dfd9f4c88e2a6575fbf33c5da893641b,
        type_description_1,
        par_im,
        var_qimage
    );


    // Release cached frame.
    if ( frame_dfd9f4c88e2a6575fbf33c5da893641b == cache_frame_dfd9f4c88e2a6575fbf33c5da893641b )
    {
        Py_DECREF( frame_dfd9f4c88e2a6575fbf33c5da893641b );
    }
    cache_frame_dfd9f4c88e2a6575fbf33c5da893641b = NULL;

    assertFrameObject( frame_dfd9f4c88e2a6575fbf33c5da893641b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_9_toqpixmap );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    CHECK_OBJECT( (PyObject *)var_qimage );
    Py_DECREF( var_qimage );
    var_qimage = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_im );
    par_im = NULL;

    Py_XDECREF( var_qimage );
    var_qimage = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt$$$function_9_toqpixmap );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageQt$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_742a30896665ce34b6dfa8e142b690ac,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageQt,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_2_rgb( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageQt$$$function_2_rgb,
        const_str_plain_rgb,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7dccdce203e0aff37ce07367cf12cdbf,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageQt,
        const_str_digest_5d4e096d92c52b0a9cd8cd0f2047cd77,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_3_fromqimage(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageQt$$$function_3_fromqimage,
        const_str_plain_fromqimage,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_55e248f7349a3005d1457163d79da71a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageQt,
        const_str_digest_7a7fadd42583da786441c79549bd3d64,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_4_fromqpixmap(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageQt$$$function_4_fromqpixmap,
        const_str_plain_fromqpixmap,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_42c90e91907a455baa9f7b66c51e22f9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageQt,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_5_align8to32(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageQt$$$function_5_align8to32,
        const_str_plain_align8to32,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6b294a6c221fdbc575de8d51adf92460,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageQt,
        const_str_digest_78c6c34928177e40925be2026c51a3f2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_6__toqclass_helper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageQt$$$function_6__toqclass_helper,
        const_str_plain__toqclass_helper,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_aa8d8da8ecba6f51192b4600955be72d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageQt,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_7___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageQt$$$function_7___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_1776dd8497d285ed28d588470b84f38f,
#endif
        codeobj_8f2c54b91515721f2199c44813e24fa5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageQt,
        const_str_digest_fa2a7634e136862a6999ab8f4c7c2497,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_8_toqimage(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageQt$$$function_8_toqimage,
        const_str_plain_toqimage,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_558e4da1872eab7221eac3f845dca723,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageQt,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageQt$$$function_9_toqpixmap(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageQt$$$function_9_toqpixmap,
        const_str_plain_toqpixmap,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_dfd9f4c88e2a6575fbf33c5da893641b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageQt,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_PIL$ImageQt =
{
    PyModuleDef_HEAD_INIT,
    "PIL.ImageQt",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(PIL$ImageQt)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(PIL$ImageQt)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_PIL$ImageQt );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("PIL.ImageQt: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.ImageQt: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.ImageQt: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initPIL$ImageQt" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_PIL$ImageQt = Py_InitModule4(
        "PIL.ImageQt",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_PIL$ImageQt = PyModule_Create( &mdef_PIL$ImageQt );
#endif

    moduledict_PIL$ImageQt = MODULE_DICT( module_PIL$ImageQt );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_PIL$ImageQt,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_PIL$ImageQt,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$ImageQt,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$ImageQt,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_PIL$ImageQt );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_3fc503d1a74f09528d49ecf123c3bbf8, module_PIL$ImageQt );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    nuitka_bool tmp_for_loop_1__break_indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    PyObject *tmp_import_from_5__module = NULL;
    PyObject *tmp_import_from_6__module = NULL;
    PyObject *tmp_import_from_7__module = NULL;
    PyObject *tmp_import_from_8__module = NULL;
    PyObject *tmp_import_from_9__module = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_65cbf88a980d87c3d3a2f1bba32e8950;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_PIL$ImageQt_192 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_fd43ac5b458178f026e6bce0e6bc2a6c_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 = NULL;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_65cbf88a980d87c3d3a2f1bba32e8950 = MAKE_MODULE_FRAME( codeobj_65cbf88a980d87c3d3a2f1bba32e8950, module_PIL$ImageQt );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_65cbf88a980d87c3d3a2f1bba32e8950 );
    assert( Py_REFCNT( frame_65cbf88a980d87c3d3a2f1bba32e8950 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_empty;
        tmp_globals_name_1 = (PyObject *)moduledict_PIL$ImageQt;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Image_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 19;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_4 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_PIL$ImageQt,
                const_str_plain_Image,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Image );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_Image, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain__util;
        tmp_globals_name_2 = (PyObject *)moduledict_PIL$ImageQt;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_isPath_str_plain_py3_tuple;
        tmp_level_name_2 = const_int_pos_1;
        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 20;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_6 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_PIL$ImageQt,
                const_str_plain_isPath,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_isPath );
        }

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_isPath, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_3 ) )
        {
           tmp_assign_source_7 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_3,
                (PyObject *)moduledict_PIL$ImageQt,
                const_str_plain_py3,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_py3 );
        }

        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_py3, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_io;
        tmp_globals_name_3 = (PyObject *)moduledict_PIL$ImageQt;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_BytesIO_tuple;
        tmp_level_name_3 = const_int_0;
        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 21;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_BytesIO );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_BytesIO, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_sys;
        tmp_globals_name_4 = (PyObject *)moduledict_PIL$ImageQt;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 22;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        assert( !(tmp_assign_source_9 == NULL) );
        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_warnings;
        tmp_globals_name_5 = (PyObject *)moduledict_PIL$ImageQt;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 23;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_warnings, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = DEEP_COPY( const_list_0538c06cd28541d8072070a86c8cfda8_list );
        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_versions, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = const_str_digest_9f3bbfc48b4b17b32ffa583a21edd739;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_WARNING_TEXT, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_versions );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qt_versions );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sort );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_key;
        tmp_dict_value_1 = MAKE_FUNCTION_PIL$ImageQt$$$function_1_lambda(  );



        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_reverse;
        tmp_dict_value_2 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 38;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_assign_source_13;
        tmp_assign_source_13 = NUITKA_BOOL_FALSE;
        tmp_for_loop_1__break_indicator = tmp_assign_source_13;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_versions );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qt_versions );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "qt_versions" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;

            goto try_except_handler_2;
        }

        tmp_iter_arg_1 = tmp_mvar_value_4;
        tmp_assign_source_14 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;

            goto try_except_handler_2;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_14;
    }
    // Tried code:
    loop_start_1:;
    // Tried code:
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_value_name_1;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_value_name_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_15 = ITERATOR_NEXT( tmp_value_name_1 );
        if ( tmp_assign_source_15 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 40;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_15;
            Py_XDECREF( old );
        }

    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = exception_keeper_type_2;
        tmp_compexpr_right_1 = PyExc_StopIteration;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            Py_DECREF( exception_keeper_type_2 );
            Py_XDECREF( exception_keeper_value_2 );
            Py_XDECREF( exception_keeper_tb_2 );

            exception_lineno = 40;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_assign_source_16;
            tmp_assign_source_16 = NUITKA_BOOL_TRUE;
            tmp_for_loop_1__break_indicator = tmp_assign_source_16;
        }
        Py_DECREF( exception_keeper_type_2 );
        Py_XDECREF( exception_keeper_value_2 );
        Py_XDECREF( exception_keeper_tb_2 );
        goto loop_end_1;
        goto branch_end_1;
        branch_no_1:;
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        branch_end_1:;
    }
    // End of try:
    try_end_2:;
    // Tried code:
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_17 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;

            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_17;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_18 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 40;
            goto try_except_handler_6;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_18;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_19 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 40;
            goto try_except_handler_6;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_19;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 40;
                    goto try_except_handler_6;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            goto try_except_handler_6;
        }
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_5;
    // End of try:
    try_end_3:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_3;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_20;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_20 = tmp_tuple_unpack_1__element_1;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_version, tmp_assign_source_20 );
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_21;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_21 = tmp_tuple_unpack_1__element_2;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_module, tmp_assign_source_21 );
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_module );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qt_module );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "qt_module" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;

            goto try_except_handler_7;
        }

        tmp_compexpr_left_2 = tmp_mvar_value_5;
        tmp_compexpr_right_2 = const_str_plain_PyQt5;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_7;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_name_name_6;
            PyObject *tmp_globals_name_6;
            PyObject *tmp_locals_name_6;
            PyObject *tmp_fromlist_name_6;
            PyObject *tmp_level_name_6;
            tmp_name_name_6 = const_str_digest_274dd8b0a28882b307cec02c0df12901;
            tmp_globals_name_6 = (PyObject *)moduledict_PIL$ImageQt;
            tmp_locals_name_6 = Py_None;
            tmp_fromlist_name_6 = const_tuple_str_plain_QImage_str_plain_qRgba_str_plain_QPixmap_tuple;
            tmp_level_name_6 = const_int_0;
            frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 43;
            tmp_assign_source_22 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_7;
            }
            {
                PyObject *old = tmp_import_from_2__module;
                tmp_import_from_2__module = tmp_assign_source_22;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_import_name_from_5;
            CHECK_OBJECT( tmp_import_from_2__module );
            tmp_import_name_from_5 = tmp_import_from_2__module;
            tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_QImage );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_8;
            }
            UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage, tmp_assign_source_23 );
        }
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_import_name_from_6;
            CHECK_OBJECT( tmp_import_from_2__module );
            tmp_import_name_from_6 = tmp_import_from_2__module;
            tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_qRgba );
            if ( tmp_assign_source_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_8;
            }
            UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qRgba, tmp_assign_source_24 );
        }
        {
            PyObject *tmp_assign_source_25;
            PyObject *tmp_import_name_from_7;
            CHECK_OBJECT( tmp_import_from_2__module );
            tmp_import_name_from_7 = tmp_import_from_2__module;
            tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_QPixmap );
            if ( tmp_assign_source_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_8;
            }
            UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QPixmap, tmp_assign_source_25 );
        }
        goto try_end_5;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
        Py_DECREF( tmp_import_from_2__module );
        tmp_import_from_2__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_7;
        // End of try:
        try_end_5:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
        Py_DECREF( tmp_import_from_2__module );
        tmp_import_from_2__module = NULL;

        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_name_name_7;
            PyObject *tmp_globals_name_7;
            PyObject *tmp_locals_name_7;
            PyObject *tmp_fromlist_name_7;
            PyObject *tmp_level_name_7;
            tmp_name_name_7 = const_str_digest_98de5bc41c0485ecbec0e675e0740b32;
            tmp_globals_name_7 = (PyObject *)moduledict_PIL$ImageQt;
            tmp_locals_name_7 = Py_None;
            tmp_fromlist_name_7 = const_tuple_str_plain_QBuffer_str_plain_QIODevice_tuple;
            tmp_level_name_7 = const_int_0;
            frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 44;
            tmp_assign_source_26 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
            if ( tmp_assign_source_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 44;

                goto try_except_handler_7;
            }
            {
                PyObject *old = tmp_import_from_3__module;
                tmp_import_from_3__module = tmp_assign_source_26;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_import_name_from_8;
            CHECK_OBJECT( tmp_import_from_3__module );
            tmp_import_name_from_8 = tmp_import_from_3__module;
            tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_QBuffer );
            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 44;

                goto try_except_handler_9;
            }
            UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QBuffer, tmp_assign_source_27 );
        }
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_import_name_from_9;
            CHECK_OBJECT( tmp_import_from_3__module );
            tmp_import_name_from_9 = tmp_import_from_3__module;
            tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_QIODevice );
            if ( tmp_assign_source_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 44;

                goto try_except_handler_9;
            }
            UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QIODevice, tmp_assign_source_28 );
        }
        goto try_end_6;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
        Py_DECREF( tmp_import_from_3__module );
        tmp_import_from_3__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        try_end_6:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
        Py_DECREF( tmp_import_from_3__module );
        tmp_import_from_3__module = NULL;

        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_mvar_value_6;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_module );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qt_module );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "qt_module" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 45;

                goto try_except_handler_7;
            }

            tmp_compexpr_left_3 = tmp_mvar_value_6;
            tmp_compexpr_right_3 = const_str_plain_PySide2;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;

                goto try_except_handler_7;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_29;
                PyObject *tmp_name_name_8;
                PyObject *tmp_globals_name_8;
                PyObject *tmp_locals_name_8;
                PyObject *tmp_fromlist_name_8;
                PyObject *tmp_level_name_8;
                tmp_name_name_8 = const_str_digest_ad6c57ee9646a00a202f40e2d20b8b2e;
                tmp_globals_name_8 = (PyObject *)moduledict_PIL$ImageQt;
                tmp_locals_name_8 = Py_None;
                tmp_fromlist_name_8 = const_tuple_str_plain_QImage_str_plain_qRgba_str_plain_QPixmap_tuple;
                tmp_level_name_8 = const_int_0;
                frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 46;
                tmp_assign_source_29 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
                if ( tmp_assign_source_29 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 46;

                    goto try_except_handler_7;
                }
                {
                    PyObject *old = tmp_import_from_4__module;
                    tmp_import_from_4__module = tmp_assign_source_29;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_30;
                PyObject *tmp_import_name_from_10;
                CHECK_OBJECT( tmp_import_from_4__module );
                tmp_import_name_from_10 = tmp_import_from_4__module;
                tmp_assign_source_30 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_QImage );
                if ( tmp_assign_source_30 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 46;

                    goto try_except_handler_10;
                }
                UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage, tmp_assign_source_30 );
            }
            {
                PyObject *tmp_assign_source_31;
                PyObject *tmp_import_name_from_11;
                CHECK_OBJECT( tmp_import_from_4__module );
                tmp_import_name_from_11 = tmp_import_from_4__module;
                tmp_assign_source_31 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_qRgba );
                if ( tmp_assign_source_31 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 46;

                    goto try_except_handler_10;
                }
                UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qRgba, tmp_assign_source_31 );
            }
            {
                PyObject *tmp_assign_source_32;
                PyObject *tmp_import_name_from_12;
                CHECK_OBJECT( tmp_import_from_4__module );
                tmp_import_name_from_12 = tmp_import_from_4__module;
                tmp_assign_source_32 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_QPixmap );
                if ( tmp_assign_source_32 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 46;

                    goto try_except_handler_10;
                }
                UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QPixmap, tmp_assign_source_32 );
            }
            goto try_end_7;
            // Exception handler code:
            try_except_handler_10:;
            exception_keeper_type_7 = exception_type;
            exception_keeper_value_7 = exception_value;
            exception_keeper_tb_7 = exception_tb;
            exception_keeper_lineno_7 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
            Py_DECREF( tmp_import_from_4__module );
            tmp_import_from_4__module = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_7;
            exception_value = exception_keeper_value_7;
            exception_tb = exception_keeper_tb_7;
            exception_lineno = exception_keeper_lineno_7;

            goto try_except_handler_7;
            // End of try:
            try_end_7:;
            CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
            Py_DECREF( tmp_import_from_4__module );
            tmp_import_from_4__module = NULL;

            {
                PyObject *tmp_assign_source_33;
                PyObject *tmp_name_name_9;
                PyObject *tmp_globals_name_9;
                PyObject *tmp_locals_name_9;
                PyObject *tmp_fromlist_name_9;
                PyObject *tmp_level_name_9;
                tmp_name_name_9 = const_str_digest_c0dc88c870b231b33b027e9493c434a1;
                tmp_globals_name_9 = (PyObject *)moduledict_PIL$ImageQt;
                tmp_locals_name_9 = Py_None;
                tmp_fromlist_name_9 = const_tuple_str_plain_QBuffer_str_plain_QIODevice_tuple;
                tmp_level_name_9 = const_int_0;
                frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 47;
                tmp_assign_source_33 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
                if ( tmp_assign_source_33 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 47;

                    goto try_except_handler_7;
                }
                {
                    PyObject *old = tmp_import_from_5__module;
                    tmp_import_from_5__module = tmp_assign_source_33;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_34;
                PyObject *tmp_import_name_from_13;
                CHECK_OBJECT( tmp_import_from_5__module );
                tmp_import_name_from_13 = tmp_import_from_5__module;
                tmp_assign_source_34 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_QBuffer );
                if ( tmp_assign_source_34 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 47;

                    goto try_except_handler_11;
                }
                UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QBuffer, tmp_assign_source_34 );
            }
            {
                PyObject *tmp_assign_source_35;
                PyObject *tmp_import_name_from_14;
                CHECK_OBJECT( tmp_import_from_5__module );
                tmp_import_name_from_14 = tmp_import_from_5__module;
                tmp_assign_source_35 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_QIODevice );
                if ( tmp_assign_source_35 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 47;

                    goto try_except_handler_11;
                }
                UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QIODevice, tmp_assign_source_35 );
            }
            goto try_end_8;
            // Exception handler code:
            try_except_handler_11:;
            exception_keeper_type_8 = exception_type;
            exception_keeper_value_8 = exception_value;
            exception_keeper_tb_8 = exception_tb;
            exception_keeper_lineno_8 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
            Py_DECREF( tmp_import_from_5__module );
            tmp_import_from_5__module = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_8;
            exception_value = exception_keeper_value_8;
            exception_tb = exception_keeper_tb_8;
            exception_lineno = exception_keeper_lineno_8;

            goto try_except_handler_7;
            // End of try:
            try_end_8:;
            CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
            Py_DECREF( tmp_import_from_5__module );
            tmp_import_from_5__module = NULL;

            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                PyObject *tmp_mvar_value_7;
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_module );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qt_module );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "qt_module" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 48;

                    goto try_except_handler_7;
                }

                tmp_compexpr_left_4 = tmp_mvar_value_7;
                tmp_compexpr_right_4 = const_str_plain_PyQt4;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 48;

                    goto try_except_handler_7;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_36;
                    PyObject *tmp_name_name_10;
                    PyObject *tmp_globals_name_10;
                    PyObject *tmp_locals_name_10;
                    PyObject *tmp_fromlist_name_10;
                    PyObject *tmp_level_name_10;
                    tmp_name_name_10 = const_str_digest_91d3fabe5e3af50ef0546b25193cf397;
                    tmp_globals_name_10 = (PyObject *)moduledict_PIL$ImageQt;
                    tmp_locals_name_10 = Py_None;
                    tmp_fromlist_name_10 = const_tuple_str_plain_QImage_str_plain_qRgba_str_plain_QPixmap_tuple;
                    tmp_level_name_10 = const_int_0;
                    frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 49;
                    tmp_assign_source_36 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
                    if ( tmp_assign_source_36 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 49;

                        goto try_except_handler_7;
                    }
                    {
                        PyObject *old = tmp_import_from_6__module;
                        tmp_import_from_6__module = tmp_assign_source_36;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_37;
                    PyObject *tmp_import_name_from_15;
                    CHECK_OBJECT( tmp_import_from_6__module );
                    tmp_import_name_from_15 = tmp_import_from_6__module;
                    tmp_assign_source_37 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_QImage );
                    if ( tmp_assign_source_37 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 49;

                        goto try_except_handler_12;
                    }
                    UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage, tmp_assign_source_37 );
                }
                {
                    PyObject *tmp_assign_source_38;
                    PyObject *tmp_import_name_from_16;
                    CHECK_OBJECT( tmp_import_from_6__module );
                    tmp_import_name_from_16 = tmp_import_from_6__module;
                    tmp_assign_source_38 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_qRgba );
                    if ( tmp_assign_source_38 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 49;

                        goto try_except_handler_12;
                    }
                    UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qRgba, tmp_assign_source_38 );
                }
                {
                    PyObject *tmp_assign_source_39;
                    PyObject *tmp_import_name_from_17;
                    CHECK_OBJECT( tmp_import_from_6__module );
                    tmp_import_name_from_17 = tmp_import_from_6__module;
                    tmp_assign_source_39 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_QPixmap );
                    if ( tmp_assign_source_39 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 49;

                        goto try_except_handler_12;
                    }
                    UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QPixmap, tmp_assign_source_39 );
                }
                goto try_end_9;
                // Exception handler code:
                try_except_handler_12:;
                exception_keeper_type_9 = exception_type;
                exception_keeper_value_9 = exception_value;
                exception_keeper_tb_9 = exception_tb;
                exception_keeper_lineno_9 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
                Py_DECREF( tmp_import_from_6__module );
                tmp_import_from_6__module = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_9;
                exception_value = exception_keeper_value_9;
                exception_tb = exception_keeper_tb_9;
                exception_lineno = exception_keeper_lineno_9;

                goto try_except_handler_7;
                // End of try:
                try_end_9:;
                CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
                Py_DECREF( tmp_import_from_6__module );
                tmp_import_from_6__module = NULL;

                {
                    PyObject *tmp_assign_source_40;
                    PyObject *tmp_name_name_11;
                    PyObject *tmp_globals_name_11;
                    PyObject *tmp_locals_name_11;
                    PyObject *tmp_fromlist_name_11;
                    PyObject *tmp_level_name_11;
                    tmp_name_name_11 = const_str_digest_155e15f871b1d690f132957bc9eefa2e;
                    tmp_globals_name_11 = (PyObject *)moduledict_PIL$ImageQt;
                    tmp_locals_name_11 = Py_None;
                    tmp_fromlist_name_11 = const_tuple_str_plain_QBuffer_str_plain_QIODevice_tuple;
                    tmp_level_name_11 = const_int_0;
                    frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 50;
                    tmp_assign_source_40 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
                    if ( tmp_assign_source_40 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 50;

                        goto try_except_handler_7;
                    }
                    {
                        PyObject *old = tmp_import_from_7__module;
                        tmp_import_from_7__module = tmp_assign_source_40;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_41;
                    PyObject *tmp_import_name_from_18;
                    CHECK_OBJECT( tmp_import_from_7__module );
                    tmp_import_name_from_18 = tmp_import_from_7__module;
                    tmp_assign_source_41 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_QBuffer );
                    if ( tmp_assign_source_41 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 50;

                        goto try_except_handler_13;
                    }
                    UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QBuffer, tmp_assign_source_41 );
                }
                {
                    PyObject *tmp_assign_source_42;
                    PyObject *tmp_import_name_from_19;
                    CHECK_OBJECT( tmp_import_from_7__module );
                    tmp_import_name_from_19 = tmp_import_from_7__module;
                    tmp_assign_source_42 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_QIODevice );
                    if ( tmp_assign_source_42 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 50;

                        goto try_except_handler_13;
                    }
                    UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QIODevice, tmp_assign_source_42 );
                }
                goto try_end_10;
                // Exception handler code:
                try_except_handler_13:;
                exception_keeper_type_10 = exception_type;
                exception_keeper_value_10 = exception_value;
                exception_keeper_tb_10 = exception_tb;
                exception_keeper_lineno_10 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_import_from_7__module );
                Py_DECREF( tmp_import_from_7__module );
                tmp_import_from_7__module = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_10;
                exception_value = exception_keeper_value_10;
                exception_tb = exception_keeper_tb_10;
                exception_lineno = exception_keeper_lineno_10;

                goto try_except_handler_7;
                // End of try:
                try_end_10:;
                CHECK_OBJECT( (PyObject *)tmp_import_from_7__module );
                Py_DECREF( tmp_import_from_7__module );
                tmp_import_from_7__module = NULL;

                {
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_source_name_2;
                    PyObject *tmp_mvar_value_8;
                    PyObject *tmp_call_result_2;
                    PyObject *tmp_args_element_name_1;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_source_name_3;
                    PyObject *tmp_mvar_value_9;
                    PyObject *tmp_args_element_name_2;
                    PyObject *tmp_mvar_value_10;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_mvar_value_11;
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_warnings );

                    if (unlikely( tmp_mvar_value_8 == NULL ))
                    {
                        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warnings );
                    }

                    if ( tmp_mvar_value_8 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warnings" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 52;

                        goto try_except_handler_7;
                    }

                    tmp_source_name_2 = tmp_mvar_value_8;
                    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_warn );
                    if ( tmp_called_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 52;

                        goto try_except_handler_7;
                    }
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_WARNING_TEXT );

                    if (unlikely( tmp_mvar_value_9 == NULL ))
                    {
                        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WARNING_TEXT );
                    }

                    if ( tmp_mvar_value_9 == NULL )
                    {
                        Py_DECREF( tmp_called_name_2 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WARNING_TEXT" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 52;

                        goto try_except_handler_7;
                    }

                    tmp_source_name_3 = tmp_mvar_value_9;
                    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_format );
                    if ( tmp_called_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 52;

                        goto try_except_handler_7;
                    }
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_module );

                    if (unlikely( tmp_mvar_value_10 == NULL ))
                    {
                        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qt_module );
                    }

                    if ( tmp_mvar_value_10 == NULL )
                    {
                        Py_DECREF( tmp_called_name_2 );
                        Py_DECREF( tmp_called_name_3 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "qt_module" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 52;

                        goto try_except_handler_7;
                    }

                    tmp_args_element_name_2 = tmp_mvar_value_10;
                    frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 52;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_2 };
                        tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                    }

                    Py_DECREF( tmp_called_name_3 );
                    if ( tmp_args_element_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 52;

                        goto try_except_handler_7;
                    }
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );

                    if (unlikely( tmp_mvar_value_11 == NULL ))
                    {
                        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );
                    }

                    if ( tmp_mvar_value_11 == NULL )
                    {
                        Py_DECREF( tmp_called_name_2 );
                        Py_DECREF( tmp_args_element_name_1 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DeprecationWarning" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 52;

                        goto try_except_handler_7;
                    }

                    tmp_args_element_name_3 = tmp_mvar_value_11;
                    frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 52;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_3 };
                        tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
                    }

                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_args_element_name_1 );
                    if ( tmp_call_result_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 52;

                        goto try_except_handler_7;
                    }
                    Py_DECREF( tmp_call_result_2 );
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    PyObject *tmp_mvar_value_12;
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_module );

                    if (unlikely( tmp_mvar_value_12 == NULL ))
                    {
                        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qt_module );
                    }

                    if ( tmp_mvar_value_12 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "qt_module" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 53;

                        goto try_except_handler_7;
                    }

                    tmp_compexpr_left_5 = tmp_mvar_value_12;
                    tmp_compexpr_right_5 = const_str_plain_PySide;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 53;

                        goto try_except_handler_7;
                    }
                    tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_assign_source_43;
                        PyObject *tmp_name_name_12;
                        PyObject *tmp_globals_name_12;
                        PyObject *tmp_locals_name_12;
                        PyObject *tmp_fromlist_name_12;
                        PyObject *tmp_level_name_12;
                        tmp_name_name_12 = const_str_digest_ac630a4d3871c74696d88f29085358f6;
                        tmp_globals_name_12 = (PyObject *)moduledict_PIL$ImageQt;
                        tmp_locals_name_12 = Py_None;
                        tmp_fromlist_name_12 = const_tuple_str_plain_QImage_str_plain_qRgba_str_plain_QPixmap_tuple;
                        tmp_level_name_12 = const_int_0;
                        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 54;
                        tmp_assign_source_43 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
                        if ( tmp_assign_source_43 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 54;

                            goto try_except_handler_7;
                        }
                        {
                            PyObject *old = tmp_import_from_8__module;
                            tmp_import_from_8__module = tmp_assign_source_43;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_44;
                        PyObject *tmp_import_name_from_20;
                        CHECK_OBJECT( tmp_import_from_8__module );
                        tmp_import_name_from_20 = tmp_import_from_8__module;
                        tmp_assign_source_44 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_QImage );
                        if ( tmp_assign_source_44 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 54;

                            goto try_except_handler_14;
                        }
                        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage, tmp_assign_source_44 );
                    }
                    {
                        PyObject *tmp_assign_source_45;
                        PyObject *tmp_import_name_from_21;
                        CHECK_OBJECT( tmp_import_from_8__module );
                        tmp_import_name_from_21 = tmp_import_from_8__module;
                        tmp_assign_source_45 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_qRgba );
                        if ( tmp_assign_source_45 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 54;

                            goto try_except_handler_14;
                        }
                        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qRgba, tmp_assign_source_45 );
                    }
                    {
                        PyObject *tmp_assign_source_46;
                        PyObject *tmp_import_name_from_22;
                        CHECK_OBJECT( tmp_import_from_8__module );
                        tmp_import_name_from_22 = tmp_import_from_8__module;
                        tmp_assign_source_46 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_QPixmap );
                        if ( tmp_assign_source_46 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 54;

                            goto try_except_handler_14;
                        }
                        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QPixmap, tmp_assign_source_46 );
                    }
                    goto try_end_11;
                    // Exception handler code:
                    try_except_handler_14:;
                    exception_keeper_type_11 = exception_type;
                    exception_keeper_value_11 = exception_value;
                    exception_keeper_tb_11 = exception_tb;
                    exception_keeper_lineno_11 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_import_from_8__module );
                    Py_DECREF( tmp_import_from_8__module );
                    tmp_import_from_8__module = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_11;
                    exception_value = exception_keeper_value_11;
                    exception_tb = exception_keeper_tb_11;
                    exception_lineno = exception_keeper_lineno_11;

                    goto try_except_handler_7;
                    // End of try:
                    try_end_11:;
                    CHECK_OBJECT( (PyObject *)tmp_import_from_8__module );
                    Py_DECREF( tmp_import_from_8__module );
                    tmp_import_from_8__module = NULL;

                    {
                        PyObject *tmp_assign_source_47;
                        PyObject *tmp_name_name_13;
                        PyObject *tmp_globals_name_13;
                        PyObject *tmp_locals_name_13;
                        PyObject *tmp_fromlist_name_13;
                        PyObject *tmp_level_name_13;
                        tmp_name_name_13 = const_str_digest_47e034e36982872ed6479ef51bdba8d8;
                        tmp_globals_name_13 = (PyObject *)moduledict_PIL$ImageQt;
                        tmp_locals_name_13 = Py_None;
                        tmp_fromlist_name_13 = const_tuple_str_plain_QBuffer_str_plain_QIODevice_tuple;
                        tmp_level_name_13 = const_int_0;
                        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 55;
                        tmp_assign_source_47 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
                        if ( tmp_assign_source_47 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 55;

                            goto try_except_handler_7;
                        }
                        {
                            PyObject *old = tmp_import_from_9__module;
                            tmp_import_from_9__module = tmp_assign_source_47;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_48;
                        PyObject *tmp_import_name_from_23;
                        CHECK_OBJECT( tmp_import_from_9__module );
                        tmp_import_name_from_23 = tmp_import_from_9__module;
                        tmp_assign_source_48 = IMPORT_NAME( tmp_import_name_from_23, const_str_plain_QBuffer );
                        if ( tmp_assign_source_48 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 55;

                            goto try_except_handler_15;
                        }
                        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QBuffer, tmp_assign_source_48 );
                    }
                    {
                        PyObject *tmp_assign_source_49;
                        PyObject *tmp_import_name_from_24;
                        CHECK_OBJECT( tmp_import_from_9__module );
                        tmp_import_name_from_24 = tmp_import_from_9__module;
                        tmp_assign_source_49 = IMPORT_NAME( tmp_import_name_from_24, const_str_plain_QIODevice );
                        if ( tmp_assign_source_49 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 55;

                            goto try_except_handler_15;
                        }
                        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QIODevice, tmp_assign_source_49 );
                    }
                    goto try_end_12;
                    // Exception handler code:
                    try_except_handler_15:;
                    exception_keeper_type_12 = exception_type;
                    exception_keeper_value_12 = exception_value;
                    exception_keeper_tb_12 = exception_tb;
                    exception_keeper_lineno_12 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_import_from_9__module );
                    Py_DECREF( tmp_import_from_9__module );
                    tmp_import_from_9__module = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_12;
                    exception_value = exception_keeper_value_12;
                    exception_tb = exception_keeper_tb_12;
                    exception_lineno = exception_keeper_lineno_12;

                    goto try_except_handler_7;
                    // End of try:
                    try_end_12:;
                    CHECK_OBJECT( (PyObject *)tmp_import_from_9__module );
                    Py_DECREF( tmp_import_from_9__module );
                    tmp_import_from_9__module = NULL;

                    {
                        PyObject *tmp_called_name_4;
                        PyObject *tmp_source_name_4;
                        PyObject *tmp_mvar_value_13;
                        PyObject *tmp_call_result_3;
                        PyObject *tmp_args_element_name_4;
                        PyObject *tmp_called_name_5;
                        PyObject *tmp_source_name_5;
                        PyObject *tmp_mvar_value_14;
                        PyObject *tmp_args_element_name_5;
                        PyObject *tmp_mvar_value_15;
                        PyObject *tmp_args_element_name_6;
                        PyObject *tmp_mvar_value_16;
                        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_warnings );

                        if (unlikely( tmp_mvar_value_13 == NULL ))
                        {
                            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warnings );
                        }

                        if ( tmp_mvar_value_13 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warnings" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 57;

                            goto try_except_handler_7;
                        }

                        tmp_source_name_4 = tmp_mvar_value_13;
                        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_warn );
                        if ( tmp_called_name_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 57;

                            goto try_except_handler_7;
                        }
                        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_WARNING_TEXT );

                        if (unlikely( tmp_mvar_value_14 == NULL ))
                        {
                            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WARNING_TEXT );
                        }

                        if ( tmp_mvar_value_14 == NULL )
                        {
                            Py_DECREF( tmp_called_name_4 );
                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WARNING_TEXT" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 57;

                            goto try_except_handler_7;
                        }

                        tmp_source_name_5 = tmp_mvar_value_14;
                        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_format );
                        if ( tmp_called_name_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_called_name_4 );

                            exception_lineno = 57;

                            goto try_except_handler_7;
                        }
                        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_module );

                        if (unlikely( tmp_mvar_value_15 == NULL ))
                        {
                            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qt_module );
                        }

                        if ( tmp_mvar_value_15 == NULL )
                        {
                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_called_name_5 );
                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "qt_module" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 57;

                            goto try_except_handler_7;
                        }

                        tmp_args_element_name_5 = tmp_mvar_value_15;
                        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 57;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_5 };
                            tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
                        }

                        Py_DECREF( tmp_called_name_5 );
                        if ( tmp_args_element_name_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_called_name_4 );

                            exception_lineno = 57;

                            goto try_except_handler_7;
                        }
                        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );

                        if (unlikely( tmp_mvar_value_16 == NULL ))
                        {
                            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );
                        }

                        if ( tmp_mvar_value_16 == NULL )
                        {
                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_args_element_name_4 );
                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DeprecationWarning" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 57;

                            goto try_except_handler_7;
                        }

                        tmp_args_element_name_6 = tmp_mvar_value_16;
                        frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 57;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_6 };
                            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
                        }

                        Py_DECREF( tmp_called_name_4 );
                        Py_DECREF( tmp_args_element_name_4 );
                        if ( tmp_call_result_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 57;

                            goto try_except_handler_7;
                        }
                        Py_DECREF( tmp_call_result_3 );
                    }
                    branch_no_5:;
                }
                branch_end_4:;
            }
            branch_end_3:;
        }
        branch_end_2:;
    }
    goto try_end_13;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_13 == NULL )
    {
        exception_keeper_tb_13 = MAKE_TRACEBACK( frame_65cbf88a980d87c3d3a2f1bba32e8950, exception_keeper_lineno_13 );
    }
    else if ( exception_keeper_lineno_13 != 0 )
    {
        exception_keeper_tb_13 = ADD_TRACEBACK( exception_keeper_tb_13, frame_65cbf88a980d87c3d3a2f1bba32e8950, exception_keeper_lineno_13 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_13, &exception_keeper_value_13, &exception_keeper_tb_13 );
    PyException_SetTraceback( exception_keeper_value_13, (PyObject *)exception_keeper_tb_13 );
    PUBLISH_EXCEPTION( &exception_keeper_type_13, &exception_keeper_value_13, &exception_keeper_tb_13 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        tmp_compexpr_left_6 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_6 = const_tuple_type_ImportError_type_RuntimeError_tuple;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_16;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        goto try_continue_handler_16;
        goto branch_end_6;
        branch_no_6:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 41;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame) frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_16;
        branch_end_6:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageQt );
    return MOD_RETURN_VALUE( NULL );
    // Exception handler code:
    try_except_handler_16:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto try_except_handler_3;
    // try continue handler code:
    try_continue_handler_16:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto loop_start_1;
    // End of try:
    // End of try:
    try_end_13:;
    {
        PyObject *tmp_assign_source_50;
        tmp_assign_source_50 = Py_True;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_is_installed, tmp_assign_source_50 );
    }
    goto loop_end_1;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 40;

        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_14;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto try_except_handler_2;
    // End of try:
    try_end_14:;
    goto try_end_15;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_16 = exception_type;
    exception_keeper_value_16 = exception_value;
    exception_keeper_tb_16 = exception_tb;
    exception_keeper_lineno_16 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_16;
    exception_value = exception_keeper_value_16;
    exception_tb = exception_keeper_tb_16;
    exception_lineno = exception_keeper_lineno_16;

    goto frame_exception_exit_1;
    // End of try:
    try_end_15:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        nuitka_bool tmp_condition_result_7;
        nuitka_bool tmp_compexpr_left_7;
        nuitka_bool tmp_compexpr_right_7;
        assert( tmp_for_loop_1__break_indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_7 = tmp_for_loop_1__break_indicator;
        tmp_compexpr_right_7 = NUITKA_BOOL_TRUE;
        tmp_condition_result_7 = ( tmp_compexpr_left_7 == tmp_compexpr_right_7 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_51;
            tmp_assign_source_51 = Py_False;
            UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_is_installed, tmp_assign_source_51 );
        }
        {
            PyObject *tmp_assign_source_52;
            tmp_assign_source_52 = Py_None;
            UPDATE_STRING_DICT0( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_version, tmp_assign_source_52 );
        }
        branch_no_7:;
    }
    {
        PyObject *tmp_assign_source_53;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_int_pos_255_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_53 = MAKE_FUNCTION_PIL$ImageQt$$$function_2_rgb( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_rgb, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        tmp_assign_source_54 = MAKE_FUNCTION_PIL$ImageQt$$$function_3_fromqimage(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_fromqimage, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        tmp_assign_source_55 = MAKE_FUNCTION_PIL$ImageQt$$$function_4_fromqpixmap(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_fromqpixmap, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        tmp_assign_source_56 = MAKE_FUNCTION_PIL$ImageQt$$$function_5_align8to32(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_align8to32, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        tmp_assign_source_57 = MAKE_FUNCTION_PIL$ImageQt$$$function_6__toqclass_helper(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain__toqclass_helper, tmp_assign_source_57 );
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_mvar_value_17;
        int tmp_truth_name_1;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_qt_is_installed );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_qt_is_installed );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "qt_is_installed" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 191;

            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_mvar_value_17 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        // Tried code:
        {
            PyObject *tmp_assign_source_58;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_mvar_value_18;
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_QImage );

            if (unlikely( tmp_mvar_value_18 == NULL ))
            {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QImage );
            }

            if ( tmp_mvar_value_18 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QImage" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 192;

                goto try_except_handler_17;
            }

            tmp_tuple_element_1 = tmp_mvar_value_18;
            tmp_assign_source_58 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_assign_source_58, 0, tmp_tuple_element_1 );
            assert( tmp_class_creation_1__bases_orig == NULL );
            tmp_class_creation_1__bases_orig = tmp_assign_source_58;
        }
        {
            PyObject *tmp_assign_source_59;
            PyObject *tmp_dircall_arg1_1;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
            Py_INCREF( tmp_dircall_arg1_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
                tmp_assign_source_59 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_59 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_17;
            }
            assert( tmp_class_creation_1__bases == NULL );
            tmp_class_creation_1__bases = tmp_assign_source_59;
        }
        {
            PyObject *tmp_assign_source_60;
            tmp_assign_source_60 = PyDict_New();
            assert( tmp_class_creation_1__class_decl_dict == NULL );
            tmp_class_creation_1__class_decl_dict = tmp_assign_source_60;
        }
        {
            PyObject *tmp_assign_source_61;
            PyObject *tmp_metaclass_name_1;
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_key_name_1;
            PyObject *tmp_dict_name_1;
            PyObject *tmp_dict_name_2;
            PyObject *tmp_key_name_2;
            nuitka_bool tmp_condition_result_10;
            int tmp_truth_name_2;
            PyObject *tmp_type_arg_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_bases_name_1;
            tmp_key_name_1 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_17;
            }
            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_1;
            }
            else
            {
                goto condexpr_false_1;
            }
            condexpr_true_1:;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
            tmp_key_name_2 = const_str_plain_metaclass;
            tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
            if ( tmp_metaclass_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_17;
            }
            goto condexpr_end_1;
            condexpr_false_1:;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_17;
            }
            tmp_condition_result_10 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_2;
            }
            else
            {
                goto condexpr_false_2;
            }
            condexpr_true_2:;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_subscribed_name_1 = tmp_class_creation_1__bases;
            tmp_subscript_name_1 = const_int_0;
            tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_type_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_17;
            }
            tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
            Py_DECREF( tmp_type_arg_1 );
            if ( tmp_metaclass_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_17;
            }
            goto condexpr_end_2;
            condexpr_false_2:;
            tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_1 );
            condexpr_end_2:;
            condexpr_end_1:;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_bases_name_1 = tmp_class_creation_1__bases;
            tmp_assign_source_61 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
            Py_DECREF( tmp_metaclass_name_1 );
            if ( tmp_assign_source_61 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_17;
            }
            assert( tmp_class_creation_1__metaclass == NULL );
            tmp_class_creation_1__metaclass = tmp_assign_source_61;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_key_name_3;
            PyObject *tmp_dict_name_3;
            tmp_key_name_3 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_17;
            }
            tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_17;
            }
            branch_no_9:;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_6 = tmp_class_creation_1__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_6, const_str_plain___prepare__ );
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_10;
            }
            else
            {
                goto branch_no_10;
            }
            branch_yes_10:;
            {
                PyObject *tmp_assign_source_62;
                PyObject *tmp_called_name_6;
                PyObject *tmp_source_name_7;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_kw_name_2;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_source_name_7 = tmp_class_creation_1__metaclass;
                tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain___prepare__ );
                if ( tmp_called_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;

                    goto try_except_handler_17;
                }
                tmp_tuple_element_2 = const_str_plain_ImageQt;
                tmp_args_name_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_tuple_element_2 = tmp_class_creation_1__bases;
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
                frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 192;
                tmp_assign_source_62 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_1, tmp_kw_name_2 );
                Py_DECREF( tmp_called_name_6 );
                Py_DECREF( tmp_args_name_1 );
                if ( tmp_assign_source_62 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;

                    goto try_except_handler_17;
                }
                assert( tmp_class_creation_1__prepared == NULL );
                tmp_class_creation_1__prepared = tmp_assign_source_62;
            }
            {
                nuitka_bool tmp_condition_result_13;
                PyObject *tmp_operand_name_1;
                PyObject *tmp_source_name_8;
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_source_name_8 = tmp_class_creation_1__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_8, const_str_plain___getitem__ );
                tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;

                    goto try_except_handler_17;
                }
                tmp_condition_result_13 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_11;
                }
                else
                {
                    goto branch_no_11;
                }
                branch_yes_11:;
                {
                    PyObject *tmp_raise_type_1;
                    PyObject *tmp_raise_value_1;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_tuple_element_3;
                    PyObject *tmp_getattr_target_1;
                    PyObject *tmp_getattr_attr_1;
                    PyObject *tmp_getattr_default_1;
                    PyObject *tmp_source_name_9;
                    PyObject *tmp_type_arg_2;
                    tmp_raise_type_1 = PyExc_TypeError;
                    tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_class_creation_1__metaclass );
                    tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                    tmp_getattr_attr_1 = const_str_plain___name__;
                    tmp_getattr_default_1 = const_str_angle_metaclass;
                    tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 192;

                        goto try_except_handler_17;
                    }
                    tmp_right_name_1 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                    CHECK_OBJECT( tmp_class_creation_1__prepared );
                    tmp_type_arg_2 = tmp_class_creation_1__prepared;
                    tmp_source_name_9 = BUILTIN_TYPE1( tmp_type_arg_2 );
                    assert( !(tmp_source_name_9 == NULL) );
                    tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_9 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_1 );

                        exception_lineno = 192;

                        goto try_except_handler_17;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                    tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                    Py_DECREF( tmp_right_name_1 );
                    if ( tmp_raise_value_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 192;

                        goto try_except_handler_17;
                    }
                    exception_type = tmp_raise_type_1;
                    Py_INCREF( tmp_raise_type_1 );
                    exception_value = tmp_raise_value_1;
                    exception_lineno = 192;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                    goto try_except_handler_17;
                }
                branch_no_11:;
            }
            goto branch_end_10;
            branch_no_10:;
            {
                PyObject *tmp_assign_source_63;
                tmp_assign_source_63 = PyDict_New();
                assert( tmp_class_creation_1__prepared == NULL );
                tmp_class_creation_1__prepared = tmp_assign_source_63;
            }
            branch_end_10:;
        }
        {
            PyObject *tmp_assign_source_64;
            {
                PyObject *tmp_set_locals_1;
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_set_locals_1 = tmp_class_creation_1__prepared;
                locals_PIL$ImageQt_192 = tmp_set_locals_1;
                Py_INCREF( tmp_set_locals_1 );
            }
            // Tried code:
            // Tried code:
            tmp_dictset_value = const_str_digest_3fc503d1a74f09528d49ecf123c3bbf8;
            tmp_res = PyObject_SetItem( locals_PIL$ImageQt_192, const_str_plain___module__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_19;
            }
            tmp_dictset_value = const_str_plain_ImageQt;
            tmp_res = PyObject_SetItem( locals_PIL$ImageQt_192, const_str_plain___qualname__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;

                goto try_except_handler_19;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_fd43ac5b458178f026e6bce0e6bc2a6c_2, codeobj_fd43ac5b458178f026e6bce0e6bc2a6c, module_PIL$ImageQt, sizeof(void *) );
            frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 = cache_frame_fd43ac5b458178f026e6bce0e6bc2a6c_2;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 ) == 2 ); // Frame stack

            // Framed code:
            tmp_dictset_value = MAKE_FUNCTION_PIL$ImageQt$$$function_7___init__(  );



            tmp_res = PyObject_SetItem( locals_PIL$ImageQt_192, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 194;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

#if 0
            RESTORE_FRAME_EXCEPTION( frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_fd43ac5b458178f026e6bce0e6bc2a6c_2, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_fd43ac5b458178f026e6bce0e6bc2a6c_2->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_fd43ac5b458178f026e6bce0e6bc2a6c_2, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_fd43ac5b458178f026e6bce0e6bc2a6c_2,
                type_description_2,
                outline_0_var___class__
            );


            // Release cached frame.
            if ( frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 == cache_frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 )
            {
                Py_DECREF( frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 );
            }
            cache_frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 = NULL;

            assertFrameObject( frame_fd43ac5b458178f026e6bce0e6bc2a6c_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;

            goto try_except_handler_19;
            skip_nested_handling_1:;
            {
                nuitka_bool tmp_condition_result_14;
                PyObject *tmp_compexpr_left_8;
                PyObject *tmp_compexpr_right_8;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_compexpr_left_8 = tmp_class_creation_1__bases;
                CHECK_OBJECT( tmp_class_creation_1__bases_orig );
                tmp_compexpr_right_8 = tmp_class_creation_1__bases_orig;
                tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;

                    goto try_except_handler_19;
                }
                tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_12;
                }
                else
                {
                    goto branch_no_12;
                }
                branch_yes_12:;
                CHECK_OBJECT( tmp_class_creation_1__bases_orig );
                tmp_dictset_value = tmp_class_creation_1__bases_orig;
                tmp_res = PyObject_SetItem( locals_PIL$ImageQt_192, const_str_plain___orig_bases__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;

                    goto try_except_handler_19;
                }
                branch_no_12:;
            }
            {
                PyObject *tmp_assign_source_65;
                PyObject *tmp_called_name_7;
                PyObject *tmp_args_name_2;
                PyObject *tmp_tuple_element_4;
                PyObject *tmp_kw_name_3;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_called_name_7 = tmp_class_creation_1__metaclass;
                tmp_tuple_element_4 = const_str_plain_ImageQt;
                tmp_args_name_2 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_4 );
                PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_tuple_element_4 = tmp_class_creation_1__bases;
                Py_INCREF( tmp_tuple_element_4 );
                PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
                tmp_tuple_element_4 = locals_PIL$ImageQt_192;
                Py_INCREF( tmp_tuple_element_4 );
                PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_kw_name_3 = tmp_class_creation_1__class_decl_dict;
                frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame.f_lineno = 192;
                tmp_assign_source_65 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_2, tmp_kw_name_3 );
                Py_DECREF( tmp_args_name_2 );
                if ( tmp_assign_source_65 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;

                    goto try_except_handler_19;
                }
                assert( outline_0_var___class__ == NULL );
                outline_0_var___class__ = tmp_assign_source_65;
            }
            CHECK_OBJECT( outline_0_var___class__ );
            tmp_assign_source_64 = outline_0_var___class__;
            Py_INCREF( tmp_assign_source_64 );
            goto try_return_handler_19;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( PIL$ImageQt );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_19:;
            Py_DECREF( locals_PIL$ImageQt_192 );
            locals_PIL$ImageQt_192 = NULL;
            goto try_return_handler_18;
            // Exception handler code:
            try_except_handler_19:;
            exception_keeper_type_17 = exception_type;
            exception_keeper_value_17 = exception_value;
            exception_keeper_tb_17 = exception_tb;
            exception_keeper_lineno_17 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_DECREF( locals_PIL$ImageQt_192 );
            locals_PIL$ImageQt_192 = NULL;
            // Re-raise.
            exception_type = exception_keeper_type_17;
            exception_value = exception_keeper_value_17;
            exception_tb = exception_keeper_tb_17;
            exception_lineno = exception_keeper_lineno_17;

            goto try_except_handler_18;
            // End of try:
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( PIL$ImageQt );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_18:;
            CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
            Py_DECREF( outline_0_var___class__ );
            outline_0_var___class__ = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_18:;
            exception_keeper_type_18 = exception_type;
            exception_keeper_value_18 = exception_value;
            exception_keeper_tb_18 = exception_tb;
            exception_keeper_lineno_18 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Re-raise.
            exception_type = exception_keeper_type_18;
            exception_value = exception_keeper_value_18;
            exception_tb = exception_keeper_tb_18;
            exception_lineno = exception_keeper_lineno_18;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( PIL$ImageQt );
            return MOD_RETURN_VALUE( NULL );
            outline_exception_1:;
            exception_lineno = 192;
            goto try_except_handler_17;
            outline_result_1:;
            UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_ImageQt, tmp_assign_source_64 );
        }
        goto try_end_16;
        // Exception handler code:
        try_except_handler_17:;
        exception_keeper_type_19 = exception_type;
        exception_keeper_value_19 = exception_value;
        exception_keeper_tb_19 = exception_tb;
        exception_keeper_lineno_19 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_class_creation_1__bases_orig );
        tmp_class_creation_1__bases_orig = NULL;

        Py_XDECREF( tmp_class_creation_1__bases );
        tmp_class_creation_1__bases = NULL;

        Py_XDECREF( tmp_class_creation_1__class_decl_dict );
        tmp_class_creation_1__class_decl_dict = NULL;

        Py_XDECREF( tmp_class_creation_1__metaclass );
        tmp_class_creation_1__metaclass = NULL;

        Py_XDECREF( tmp_class_creation_1__prepared );
        tmp_class_creation_1__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_19;
        exception_value = exception_keeper_value_19;
        exception_tb = exception_keeper_tb_19;
        exception_lineno = exception_keeper_lineno_19;

        goto frame_exception_exit_1;
        // End of try:
        try_end_16:;
        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
        Py_DECREF( tmp_class_creation_1__bases_orig );
        tmp_class_creation_1__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
        Py_DECREF( tmp_class_creation_1__bases );
        tmp_class_creation_1__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
        Py_DECREF( tmp_class_creation_1__class_decl_dict );
        tmp_class_creation_1__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
        Py_DECREF( tmp_class_creation_1__metaclass );
        tmp_class_creation_1__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
        Py_DECREF( tmp_class_creation_1__prepared );
        tmp_class_creation_1__prepared = NULL;

        branch_no_8:;
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_65cbf88a980d87c3d3a2f1bba32e8950 );
#endif
    popFrameStack();

    assertFrameObject( frame_65cbf88a980d87c3d3a2f1bba32e8950 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_65cbf88a980d87c3d3a2f1bba32e8950 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_65cbf88a980d87c3d3a2f1bba32e8950, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_65cbf88a980d87c3d3a2f1bba32e8950->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_65cbf88a980d87c3d3a2f1bba32e8950, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;
    {
        PyObject *tmp_assign_source_66;
        tmp_assign_source_66 = MAKE_FUNCTION_PIL$ImageQt$$$function_8_toqimage(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_toqimage, tmp_assign_source_66 );
    }
    {
        PyObject *tmp_assign_source_67;
        tmp_assign_source_67 = MAKE_FUNCTION_PIL$ImageQt$$$function_9_toqpixmap(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageQt, (Nuitka_StringObject *)const_str_plain_toqpixmap, tmp_assign_source_67 );
    }

    return MOD_RETURN_VALUE( module_PIL$ImageQt );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
