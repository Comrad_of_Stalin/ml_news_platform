/* Generated code for Python module 'pymorphy2.opencorpora_dict.parse'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_pymorphy2$opencorpora_dict$parse" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pymorphy2$opencorpora_dict$parse;
PyDictObject *moduledict_pymorphy2$opencorpora_dict$parse;

/* The declarations of module constants used, if any. */
static PyObject *const_str_plain_getprevious;
extern PyObject *const_str_plain_tag;
static PyObject *const_tuple_str_plain_revision_tuple;
static PyObject *const_str_digest_3bba681c4b8c1f41ee79e5e1e4540d1d;
static PyObject *const_str_digest_d23049c30e01960e937fa4f898226536;
extern PyObject *const_tuple_str_plain_version_tuple;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_links;
extern PyObject *const_str_plain_getLogger;
extern PyObject *const_str_plain_from;
static PyObject *const_tuple_str_plain_id_tuple;
static PyObject *const_str_digest_f82c239c212e4d830c30a94140099e1f;
extern PyObject *const_str_plain___file__;
extern PyObject *const_int_pos_50000;
extern PyObject *const_str_plain_division;
extern PyObject *const_str_plain_link;
extern PyObject *const_str_plain_grammeme;
extern PyObject *const_str_plain_grammemes;
extern PyObject *const_str_plain_id;
static PyObject *const_str_digest_096e06c9ce7af0f307ff3d8f2f04028b;
extern PyObject *const_str_plain_end;
static PyObject *const_tuple_6139be65ada13d027967d7d96981feef_tuple;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_8d18621b6c1c50010f0e396f8ff9124a;
extern PyObject *const_str_plain_strip;
static PyObject *const_str_digest_63db0cd627409470a480e5b7e0ad1149;
static PyObject *const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_revision;
extern PyObject *const_str_plain_start;
static PyObject *const_tuple_str_plain_parent_tuple;
extern PyObject *const_tuple_str_plain_v_tuple;
static PyObject *const_str_plain_form_elem;
extern PyObject *const_str_plain_absolute_import;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_description;
extern PyObject *const_str_plain_lower;
extern PyObject *const_tuple_str_plain_l_tuple;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_lexemes;
static PyObject *const_tuple_str_plain_from_tuple;
extern PyObject *const_str_chr_44;
static PyObject *const_str_digest_6af5c9035db191544f6324ea218e6100;
static PyObject *const_tuple_str_plain_alias_tuple;
extern PyObject *const_str_plain_t;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_g;
extern PyObject *const_str_plain_lex_id;
static PyObject *const_str_plain_getparent;
extern PyObject *const_str_plain_collections;
extern PyObject *const_str_plain_parent;
extern PyObject *const_tuple_str_plain_description_tuple;
extern PyObject *const_str_plain_ev;
extern PyObject *const_str_plain_l;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_space;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_form;
static PyObject *const_str_digest_22458dfdf60aff9ff5147019bb64ce31;
static PyObject *const_tuple_str_digest_096e06c9ce7af0f307ff3d8f2f04028b_tuple;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_lexeme;
static PyObject *const_tuple_str_plain_elem_tuple;
static PyObject *const_str_plain_base_grammemes;
static PyObject *const_str_plain_ParsedDictionary;
extern PyObject *const_str_plain_f;
static PyObject *const_dict_31447d5217e4331fe3befd1998fb9495;
extern PyObject *const_int_0;
static PyObject *const_str_digest_ffc3563b9dd49f79271721271071fff5;
extern PyObject *const_tuple_str_plain_iterparse_tuple;
static PyObject *const_str_digest_d5d300aa65b095a98d597e98b262766c;
static PyObject *const_tuple_str_digest_3bba681c4b8c1f41ee79e5e1e4540d1d_tuple;
static PyObject *const_str_plain_lemma;
static PyObject *const_str_plain_xml_clear_elem;
extern PyObject *const_str_plain_text;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_parse_opencorpora_xml;
static PyObject *const_str_digest_dfb31e364b9832e04784d4cdbf5fd05b;
extern PyObject *const_str_plain_namedtuple;
extern PyObject *const_str_plain_type;
extern PyObject *const_tuple_str_plain_g_tuple;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_v;
extern PyObject *const_str_plain_to;
extern PyObject *const_str_digest_04d67503345d6d64752f0c9fc2a764c3;
extern PyObject *const_str_plain_debug;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_filename;
extern PyObject *const_str_plain_events;
static PyObject *const_tuple_str_plain_to_tuple;
extern PyObject *const_tuple_str_plain_start_str_plain_end_tuple;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_findall;
static PyObject *const_str_plain__word_forms_from_xml_elem;
extern PyObject *const_tuple_str_plain_t_tuple;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_g_tuple;
extern PyObject *const_str_plain_logger;
extern PyObject *const_str_plain_alias;
extern PyObject *const_str_plain_elem;
static PyObject *const_str_plain_link_tuple;
static PyObject *const_str_plain_word_forms;
extern PyObject *const_str_plain_clear;
static PyObject *const_str_digest_77cc7b8b9f388a9e6b0502b0c14c22aa;
extern PyObject *const_tuple_str_plain_name_tuple;
static PyObject *const_tuple_str_digest_6af5c9035db191544f6324ea218e6100_tuple;
extern PyObject *const_str_digest_b8b8773fbd08a10b8c2607df28400381;
extern PyObject *const_str_plain_iterparse;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_dictionary;
static PyObject *const_str_plain__grammemes_from_elem;
extern PyObject *const_tuple_str_plain_f_tuple;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_a7262064a8ae75fcf8de740d8c04b96d;
extern PyObject *const_str_plain_version;
static PyObject *const_tuple_c8a95e2fa80a3fa9e52aa9ec05e6f60c_tuple;
extern PyObject *const_str_plain_info;
extern PyObject *const_str_plain_logging;
static PyObject *const_str_plain_base_info;
extern PyObject *const_tuple_str_plain_type_tuple;
extern PyObject *const_str_plain_find;
extern PyObject *const_tuple_none_none_tuple;
static PyObject *const_str_plain__lexemes_len;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_getprevious = UNSTREAM_STRING_ASCII( &constant_bin[ 5052692 ], 11, 1 );
    const_tuple_str_plain_revision_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_revision_tuple, 0, const_str_plain_revision ); Py_INCREF( const_str_plain_revision );
    const_str_digest_3bba681c4b8c1f41ee79e5e1e4540d1d = UNSTREAM_STRING_ASCII( &constant_bin[ 5052703 ], 21, 0 );
    const_str_digest_d23049c30e01960e937fa4f898226536 = UNSTREAM_STRING_ASCII( &constant_bin[ 5052724 ], 39, 0 );
    const_tuple_str_plain_id_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_id_tuple, 0, const_str_plain_id ); Py_INCREF( const_str_plain_id );
    const_str_digest_f82c239c212e4d830c30a94140099e1f = UNSTREAM_STRING_ASCII( &constant_bin[ 5052763 ], 95, 0 );
    const_str_digest_096e06c9ce7af0f307ff3d8f2f04028b = UNSTREAM_STRING_ASCII( &constant_bin[ 5052770 ], 32, 0 );
    const_tuple_6139be65ada13d027967d7d96981feef_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_6139be65ada13d027967d7d96981feef_tuple, 0, const_str_plain_elem ); Py_INCREF( const_str_plain_elem );
    PyTuple_SET_ITEM( const_tuple_6139be65ada13d027967d7d96981feef_tuple, 1, const_str_plain_lexeme ); Py_INCREF( const_str_plain_lexeme );
    PyTuple_SET_ITEM( const_tuple_6139be65ada13d027967d7d96981feef_tuple, 2, const_str_plain_lex_id ); Py_INCREF( const_str_plain_lex_id );
    const_str_plain_base_info = UNSTREAM_STRING_ASCII( &constant_bin[ 5052858 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_6139be65ada13d027967d7d96981feef_tuple, 3, const_str_plain_base_info ); Py_INCREF( const_str_plain_base_info );
    const_str_plain_base_grammemes = UNSTREAM_STRING_ASCII( &constant_bin[ 5052867 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_6139be65ada13d027967d7d96981feef_tuple, 4, const_str_plain_base_grammemes ); Py_INCREF( const_str_plain_base_grammemes );
    const_str_plain_form_elem = UNSTREAM_STRING_ASCII( &constant_bin[ 5052881 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_6139be65ada13d027967d7d96981feef_tuple, 5, const_str_plain_form_elem ); Py_INCREF( const_str_plain_form_elem );
    PyTuple_SET_ITEM( const_tuple_6139be65ada13d027967d7d96981feef_tuple, 6, const_str_plain_grammemes ); Py_INCREF( const_str_plain_grammemes );
    PyTuple_SET_ITEM( const_tuple_6139be65ada13d027967d7d96981feef_tuple, 7, const_str_plain_form ); Py_INCREF( const_str_plain_form );
    const_str_digest_8d18621b6c1c50010f0e396f8ff9124a = UNSTREAM_STRING_ASCII( &constant_bin[ 5052890 ], 71, 0 );
    const_str_digest_63db0cd627409470a480e5b7e0ad1149 = UNSTREAM_STRING_ASCII( &constant_bin[ 5052961 ], 40, 0 );
    const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple = PyTuple_New( 17 );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 0, const_str_plain_filename ); Py_INCREF( const_str_plain_filename );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 1, const_str_plain_links ); Py_INCREF( const_str_plain_links );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 2, const_str_plain_lexemes ); Py_INCREF( const_str_plain_lexemes );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 3, const_str_plain_grammemes ); Py_INCREF( const_str_plain_grammemes );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 4, const_str_plain_version ); Py_INCREF( const_str_plain_version );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 5, const_str_plain_revision ); Py_INCREF( const_str_plain_revision );
    const_str_plain__lexemes_len = UNSTREAM_STRING_ASCII( &constant_bin[ 5053001 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 6, const_str_plain__lexemes_len ); Py_INCREF( const_str_plain__lexemes_len );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 7, const_str_plain_ev ); Py_INCREF( const_str_plain_ev );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 8, const_str_plain_elem ); Py_INCREF( const_str_plain_elem );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 9, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 10, const_str_plain_parent ); Py_INCREF( const_str_plain_parent );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 11, const_str_plain_alias ); Py_INCREF( const_str_plain_alias );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 12, const_str_plain_description ); Py_INCREF( const_str_plain_description );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 13, const_str_plain_grammeme ); Py_INCREF( const_str_plain_grammeme );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 14, const_str_plain_lex_id ); Py_INCREF( const_str_plain_lex_id );
    const_str_plain_word_forms = UNSTREAM_STRING_ASCII( &constant_bin[ 5053013 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 15, const_str_plain_word_forms ); Py_INCREF( const_str_plain_word_forms );
    const_str_plain_link_tuple = UNSTREAM_STRING_ASCII( &constant_bin[ 5053023 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 16, const_str_plain_link_tuple ); Py_INCREF( const_str_plain_link_tuple );
    const_tuple_str_plain_parent_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_parent_tuple, 0, const_str_plain_parent ); Py_INCREF( const_str_plain_parent );
    const_tuple_str_plain_from_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_from_tuple, 0, const_str_plain_from ); Py_INCREF( const_str_plain_from );
    const_str_digest_6af5c9035db191544f6324ea218e6100 = UNSTREAM_STRING_ASCII( &constant_bin[ 5053033 ], 20, 0 );
    const_tuple_str_plain_alias_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_alias_tuple, 0, const_str_plain_alias ); Py_INCREF( const_str_plain_alias );
    const_str_plain_getparent = UNSTREAM_STRING_ASCII( &constant_bin[ 5053053 ], 9, 1 );
    const_str_digest_22458dfdf60aff9ff5147019bb64ce31 = UNSTREAM_STRING_ASCII( &constant_bin[ 5053062 ], 10, 0 );
    const_tuple_str_digest_096e06c9ce7af0f307ff3d8f2f04028b_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_096e06c9ce7af0f307ff3d8f2f04028b_tuple, 0, const_str_digest_096e06c9ce7af0f307ff3d8f2f04028b ); Py_INCREF( const_str_digest_096e06c9ce7af0f307ff3d8f2f04028b );
    const_tuple_str_plain_elem_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_elem_tuple, 0, const_str_plain_elem ); Py_INCREF( const_str_plain_elem );
    const_str_plain_ParsedDictionary = UNSTREAM_STRING_ASCII( &constant_bin[ 5053072 ], 16, 1 );
    const_dict_31447d5217e4331fe3befd1998fb9495 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_31447d5217e4331fe3befd1998fb9495, const_str_plain_events, const_tuple_str_plain_start_str_plain_end_tuple );
    assert( PyDict_Size( const_dict_31447d5217e4331fe3befd1998fb9495 ) == 1 );
    const_str_digest_ffc3563b9dd49f79271721271071fff5 = UNSTREAM_STRING_ASCII( &constant_bin[ 5053088 ], 17, 0 );
    const_str_digest_d5d300aa65b095a98d597e98b262766c = UNSTREAM_STRING_ASCII( &constant_bin[ 5053105 ], 35, 0 );
    const_tuple_str_digest_3bba681c4b8c1f41ee79e5e1e4540d1d_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_3bba681c4b8c1f41ee79e5e1e4540d1d_tuple, 0, const_str_digest_3bba681c4b8c1f41ee79e5e1e4540d1d ); Py_INCREF( const_str_digest_3bba681c4b8c1f41ee79e5e1e4540d1d );
    const_str_plain_lemma = UNSTREAM_STRING_ASCII( &constant_bin[ 5052715 ], 5, 1 );
    const_str_plain_xml_clear_elem = UNSTREAM_STRING_ASCII( &constant_bin[ 5053140 ], 14, 1 );
    const_str_digest_dfb31e364b9832e04784d4cdbf5fd05b = UNSTREAM_STRING_ASCII( &constant_bin[ 5053154 ], 82, 0 );
    const_tuple_str_plain_to_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_to_tuple, 0, const_str_plain_to ); Py_INCREF( const_str_plain_to );
    const_str_plain__word_forms_from_xml_elem = UNSTREAM_STRING_ASCII( &constant_bin[ 5053236 ], 25, 1 );
    const_str_digest_77cc7b8b9f388a9e6b0502b0c14c22aa = UNSTREAM_STRING_ASCII( &constant_bin[ 5053261 ], 21, 0 );
    const_tuple_str_digest_6af5c9035db191544f6324ea218e6100_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_6af5c9035db191544f6324ea218e6100_tuple, 0, const_str_digest_6af5c9035db191544f6324ea218e6100 ); Py_INCREF( const_str_digest_6af5c9035db191544f6324ea218e6100 );
    const_str_plain__grammemes_from_elem = UNSTREAM_STRING_ASCII( &constant_bin[ 5052724 ], 20, 1 );
    const_str_digest_a7262064a8ae75fcf8de740d8c04b96d = UNSTREAM_STRING_ASCII( &constant_bin[ 5053282 ], 41, 0 );
    const_tuple_c8a95e2fa80a3fa9e52aa9ec05e6f60c_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_c8a95e2fa80a3fa9e52aa9ec05e6f60c_tuple, 0, const_str_plain_ParsedDictionary ); Py_INCREF( const_str_plain_ParsedDictionary );
    PyTuple_SET_ITEM( const_tuple_c8a95e2fa80a3fa9e52aa9ec05e6f60c_tuple, 1, const_str_digest_63db0cd627409470a480e5b7e0ad1149 ); Py_INCREF( const_str_digest_63db0cd627409470a480e5b7e0ad1149 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pymorphy2$opencorpora_dict$parse( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_9bf203e631b846eb0b5f407c49516f4f;
static PyCodeObject *codeobj_72f718beb2fbc5300782c41934593b24;
static PyCodeObject *codeobj_102f62da71b77e0152e89b2fc8752154;
static PyCodeObject *codeobj_718ae49ea6cda6528f3dea607d343cd5;
static PyCodeObject *codeobj_195a4eaed54dd53ec6103e22fe764cbb;
static PyCodeObject *codeobj_1084451cde5ccd6aef9f0350e39959f2;
static PyCodeObject *codeobj_3a996733c5203aba96c3c5506e1a837a;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_d5d300aa65b095a98d597e98b262766c );
    codeobj_9bf203e631b846eb0b5f407c49516f4f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 93, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_g_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_72f718beb2fbc5300782c41934593b24 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_a7262064a8ae75fcf8de740d8c04b96d, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_102f62da71b77e0152e89b2fc8752154 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__grammemes_from_elem, 92, const_tuple_str_plain_elem_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_718ae49ea6cda6528f3dea607d343cd5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__word_forms_from_xml_elem, 96, const_tuple_6139be65ada13d027967d7d96981feef_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_195a4eaed54dd53ec6103e22fe764cbb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_parse_opencorpora_xml, 34, const_tuple_76e5f0a2cec93b6667cbe565fbbeffe4_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1084451cde5ccd6aef9f0350e39959f2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_xml_clear_elem, 14, const_tuple_str_plain_elem_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3a996733c5203aba96c3c5506e1a837a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_xml_clear_elem, 25, const_tuple_str_plain_elem_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem$$$genexpr_1_genexpr_maker( void );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_1_xml_clear_elem(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_2_xml_clear_elem(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_3_parse_opencorpora_xml(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_5__word_forms_from_xml_elem(  );


// The module function definitions.
static PyObject *impl_pymorphy2$opencorpora_dict$parse$$$function_1_xml_clear_elem( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_elem = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_1084451cde5ccd6aef9f0350e39959f2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_1084451cde5ccd6aef9f0350e39959f2 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1084451cde5ccd6aef9f0350e39959f2, codeobj_1084451cde5ccd6aef9f0350e39959f2, module_pymorphy2$opencorpora_dict$parse, sizeof(void *) );
    frame_1084451cde5ccd6aef9f0350e39959f2 = cache_frame_1084451cde5ccd6aef9f0350e39959f2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1084451cde5ccd6aef9f0350e39959f2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1084451cde5ccd6aef9f0350e39959f2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_elem );
        tmp_called_instance_1 = par_elem;
        frame_1084451cde5ccd6aef9f0350e39959f2->m_frame.f_lineno = 15;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_clear );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_elem );
        tmp_called_instance_2 = par_elem;
        frame_1084451cde5ccd6aef9f0350e39959f2->m_frame.f_lineno = 16;
        tmp_compexpr_left_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_getprevious );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_delsubscr_target_1;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_delsubscr_subscript_1;
        CHECK_OBJECT( par_elem );
        tmp_called_instance_3 = par_elem;
        frame_1084451cde5ccd6aef9f0350e39959f2->m_frame.f_lineno = 17;
        tmp_delsubscr_target_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_getparent );
        if ( tmp_delsubscr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_delsubscr_subscript_1 = const_int_0;
        tmp_result = DEL_SUBSCRIPT( tmp_delsubscr_target_1, tmp_delsubscr_subscript_1 );
        Py_DECREF( tmp_delsubscr_target_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 16;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1084451cde5ccd6aef9f0350e39959f2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1084451cde5ccd6aef9f0350e39959f2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1084451cde5ccd6aef9f0350e39959f2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1084451cde5ccd6aef9f0350e39959f2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1084451cde5ccd6aef9f0350e39959f2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1084451cde5ccd6aef9f0350e39959f2,
        type_description_1,
        par_elem
    );


    // Release cached frame.
    if ( frame_1084451cde5ccd6aef9f0350e39959f2 == cache_frame_1084451cde5ccd6aef9f0350e39959f2 )
    {
        Py_DECREF( frame_1084451cde5ccd6aef9f0350e39959f2 );
    }
    cache_frame_1084451cde5ccd6aef9f0350e39959f2 = NULL;

    assertFrameObject( frame_1084451cde5ccd6aef9f0350e39959f2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_1_xml_clear_elem );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_elem );
    Py_DECREF( par_elem );
    par_elem = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_elem );
    Py_DECREF( par_elem );
    par_elem = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_1_xml_clear_elem );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$parse$$$function_2_xml_clear_elem( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_elem = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3a996733c5203aba96c3c5506e1a837a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3a996733c5203aba96c3c5506e1a837a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3a996733c5203aba96c3c5506e1a837a, codeobj_3a996733c5203aba96c3c5506e1a837a, module_pymorphy2$opencorpora_dict$parse, sizeof(void *) );
    frame_3a996733c5203aba96c3c5506e1a837a = cache_frame_3a996733c5203aba96c3c5506e1a837a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3a996733c5203aba96c3c5506e1a837a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3a996733c5203aba96c3c5506e1a837a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_elem );
        tmp_called_instance_1 = par_elem;
        frame_3a996733c5203aba96c3c5506e1a837a->m_frame.f_lineno = 26;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_clear );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3a996733c5203aba96c3c5506e1a837a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3a996733c5203aba96c3c5506e1a837a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3a996733c5203aba96c3c5506e1a837a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3a996733c5203aba96c3c5506e1a837a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3a996733c5203aba96c3c5506e1a837a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3a996733c5203aba96c3c5506e1a837a,
        type_description_1,
        par_elem
    );


    // Release cached frame.
    if ( frame_3a996733c5203aba96c3c5506e1a837a == cache_frame_3a996733c5203aba96c3c5506e1a837a )
    {
        Py_DECREF( frame_3a996733c5203aba96c3c5506e1a837a );
    }
    cache_frame_3a996733c5203aba96c3c5506e1a837a = NULL;

    assertFrameObject( frame_3a996733c5203aba96c3c5506e1a837a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_2_xml_clear_elem );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_elem );
    Py_DECREF( par_elem );
    par_elem = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_elem );
    Py_DECREF( par_elem );
    par_elem = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_2_xml_clear_elem );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$parse$$$function_3_parse_opencorpora_xml( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_filename = python_pars[ 0 ];
    PyObject *var_links = NULL;
    PyObject *var_lexemes = NULL;
    PyObject *var_grammemes = NULL;
    PyObject *var_version = NULL;
    PyObject *var_revision = NULL;
    PyObject *var__lexemes_len = NULL;
    PyObject *var_ev = NULL;
    PyObject *var_elem = NULL;
    PyObject *var_name = NULL;
    PyObject *var_parent = NULL;
    PyObject *var_alias = NULL;
    PyObject *var_description = NULL;
    PyObject *var_grammeme = NULL;
    PyObject *var_lex_id = NULL;
    PyObject *var_word_forms = NULL;
    PyObject *var_link_tuple = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    struct Nuitka_FrameObject *frame_195a4eaed54dd53ec6103e22fe764cbb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    int tmp_res;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_195a4eaed54dd53ec6103e22fe764cbb = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_links == NULL );
        var_links = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyDict_New();
        assert( var_lexemes == NULL );
        var_lexemes = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = PyList_New( 0 );
        assert( var_grammemes == NULL );
        var_grammemes = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        tmp_iter_arg_1 = const_tuple_none_none_tuple;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        assert( !(tmp_assign_source_4 == NULL) );
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_195a4eaed54dd53ec6103e22fe764cbb, codeobj_195a4eaed54dd53ec6103e22fe764cbb, module_pymorphy2$opencorpora_dict$parse, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_195a4eaed54dd53ec6103e22fe764cbb = cache_frame_195a4eaed54dd53ec6103e22fe764cbb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_195a4eaed54dd53ec6103e22fe764cbb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_195a4eaed54dd53ec6103e22fe764cbb ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooooo";
            exception_lineno = 42;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooooo";
            exception_lineno = 42;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        assert( var_version == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_version = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        assert( var_revision == NULL );
        Py_INCREF( tmp_assign_source_8 );
        var_revision = tmp_assign_source_8;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = const_int_0;
        assert( var__lexemes_len == NULL );
        Py_INCREF( tmp_assign_source_9 );
        var__lexemes_len = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_iterparse );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterparse );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterparse" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_filename );
        tmp_tuple_element_1 = par_filename;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_31447d5217e4331fe3befd1998fb9495 );
        frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 45;
        tmp_iter_arg_2 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_10;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_11 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooooooo";
                exception_lineno = 45;
                goto try_except_handler_4;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_iter_arg_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_3 = tmp_for_loop_1__iter_value;
        tmp_assign_source_12 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__source_iter;
            tmp_tuple_unpack_2__source_iter = tmp_assign_source_12;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_13 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooooo";
            exception_lineno = 45;
            goto try_except_handler_6;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_1;
            tmp_tuple_unpack_2__element_1 = tmp_assign_source_13;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_14 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_14 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooooo";
            exception_lineno = 45;
            goto try_except_handler_6;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_2;
            tmp_tuple_unpack_2__element_2 = tmp_assign_source_14;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 45;
                    goto try_except_handler_6;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooooooooo";
            exception_lineno = 45;
            goto try_except_handler_6;
        }
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_5;
    // End of try:
    try_end_3:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_4;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_15;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_15 = tmp_tuple_unpack_2__element_1;
        {
            PyObject *old = var_ev;
            var_ev = tmp_assign_source_15;
            Py_INCREF( var_ev );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_16;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_16 = tmp_tuple_unpack_2__element_2;
        {
            PyObject *old = var_elem;
            var_elem = tmp_assign_source_16;
            Py_INCREF( var_elem );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_ev );
        tmp_compexpr_left_1 = var_ev;
        tmp_compexpr_right_1 = const_str_plain_start;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( var_elem );
            tmp_source_name_1 = var_elem;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_tag );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 48;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            tmp_compexpr_right_2 = const_str_plain_dictionary;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 48;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_17;
                PyObject *tmp_called_instance_1;
                CHECK_OBJECT( var_elem );
                tmp_called_instance_1 = var_elem;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 49;
                tmp_assign_source_17 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_version_tuple, 0 ) );

                if ( tmp_assign_source_17 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 49;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                {
                    PyObject *old = var_version;
                    var_version = tmp_assign_source_17;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_18;
                PyObject *tmp_called_instance_2;
                CHECK_OBJECT( var_elem );
                tmp_called_instance_2 = var_elem;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 50;
                tmp_assign_source_18 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_revision_tuple, 0 ) );

                if ( tmp_assign_source_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 50;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                {
                    PyObject *old = var_revision;
                    var_revision = tmp_assign_source_18;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_instance_3;
                PyObject *tmp_mvar_value_2;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_args_element_name_3;
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_logger );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logger );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logger" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 51;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }

                tmp_called_instance_3 = tmp_mvar_value_2;
                tmp_args_element_name_1 = const_str_digest_77cc7b8b9f388a9e6b0502b0c14c22aa;
                CHECK_OBJECT( var_version );
                tmp_args_element_name_2 = var_version;
                CHECK_OBJECT( var_revision );
                tmp_args_element_name_3 = var_revision;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 51;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                    tmp_call_result_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_3, const_str_plain_info, call_args );
                }

                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 51;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            {
                PyObject *tmp_called_name_2;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_4;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_xml_clear_elem );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_xml_clear_elem );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "xml_clear_elem" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 52;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }

                tmp_called_name_2 = tmp_mvar_value_3;
                CHECK_OBJECT( var_elem );
                tmp_args_element_name_4 = var_elem;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 52;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                }

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 52;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_2:;
        }
        goto loop_start_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_elem );
        tmp_source_name_2 = var_elem;
        tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_tag );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_compexpr_right_3 = const_str_plain_grammeme;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_source_name_3;
            PyObject *tmp_called_instance_4;
            CHECK_OBJECT( var_elem );
            tmp_called_instance_4 = var_elem;
            frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 56;
            tmp_source_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_find, &PyTuple_GET_ITEM( const_tuple_str_plain_name_tuple, 0 ) );

            if ( tmp_source_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 56;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            tmp_assign_source_19 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_text );
            Py_DECREF( tmp_source_name_3 );
            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 56;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_name;
                var_name = tmp_assign_source_19;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_called_instance_5;
            CHECK_OBJECT( var_elem );
            tmp_called_instance_5 = var_elem;
            frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 57;
            tmp_assign_source_20 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_parent_tuple, 0 ) );

            if ( tmp_assign_source_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 57;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_parent;
                var_parent = tmp_assign_source_20;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_source_name_4;
            PyObject *tmp_called_instance_6;
            CHECK_OBJECT( var_elem );
            tmp_called_instance_6 = var_elem;
            frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 58;
            tmp_source_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_find, &PyTuple_GET_ITEM( const_tuple_str_plain_alias_tuple, 0 ) );

            if ( tmp_source_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 58;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            tmp_assign_source_21 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_text );
            Py_DECREF( tmp_source_name_4 );
            if ( tmp_assign_source_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 58;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_alias;
                var_alias = tmp_assign_source_21;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_source_name_5;
            PyObject *tmp_called_instance_7;
            CHECK_OBJECT( var_elem );
            tmp_called_instance_7 = var_elem;
            frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 59;
            tmp_source_name_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_find, &PyTuple_GET_ITEM( const_tuple_str_plain_description_tuple, 0 ) );

            if ( tmp_source_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            tmp_assign_source_22 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_text );
            Py_DECREF( tmp_source_name_5 );
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_description;
                var_description = tmp_assign_source_22;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_tuple_element_2;
            CHECK_OBJECT( var_name );
            tmp_tuple_element_2 = var_name;
            tmp_assign_source_23 = PyTuple_New( 4 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_assign_source_23, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( var_parent );
            tmp_tuple_element_2 = var_parent;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_assign_source_23, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( var_alias );
            tmp_tuple_element_2 = var_alias;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_assign_source_23, 2, tmp_tuple_element_2 );
            CHECK_OBJECT( var_description );
            tmp_tuple_element_2 = var_description;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_assign_source_23, 3, tmp_tuple_element_2 );
            {
                PyObject *old = var_grammeme;
                var_grammeme = tmp_assign_source_23;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_instance_8;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( var_grammemes );
            tmp_called_instance_8 = var_grammemes;
            CHECK_OBJECT( var_grammeme );
            tmp_args_element_name_5 = var_grammeme;
            frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 62;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 62;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_6;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_xml_clear_elem );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_xml_clear_elem );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "xml_clear_elem" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 63;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_3 = tmp_mvar_value_4;
            CHECK_OBJECT( var_elem );
            tmp_args_element_name_6 = var_elem;
            frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 63;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( var_elem );
        tmp_source_name_6 = var_elem;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_tag );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_compexpr_right_4 = const_str_plain_lemma;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            CHECK_OBJECT( var_lexemes );
            tmp_operand_name_1 = var_lexemes;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_called_instance_9;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_call_result_5;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_logger );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logger );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logger" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 67;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }

                tmp_called_instance_9 = tmp_mvar_value_5;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 67;
                tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_info, &PyTuple_GET_ITEM( const_tuple_str_digest_3bba681c4b8c1f41ee79e5e1e4540d1d_tuple, 0 ) );

                if ( tmp_call_result_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 67;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                Py_DECREF( tmp_call_result_5 );
            }
            branch_no_5:;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_iter_arg_4;
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_7;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain__word_forms_from_xml_elem );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__word_forms_from_xml_elem );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_word_forms_from_xml_elem" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 69;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_7;
            }

            tmp_called_name_4 = tmp_mvar_value_6;
            CHECK_OBJECT( var_elem );
            tmp_args_element_name_7 = var_elem;
            frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 69;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_iter_arg_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            if ( tmp_iter_arg_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_7;
            }
            tmp_assign_source_24 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
            Py_DECREF( tmp_iter_arg_4 );
            if ( tmp_assign_source_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_7;
            }
            {
                PyObject *old = tmp_tuple_unpack_3__source_iter;
                tmp_tuple_unpack_3__source_iter = tmp_assign_source_24;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_25;
            PyObject *tmp_unpack_5;
            CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
            tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
            tmp_assign_source_25 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
            if ( tmp_assign_source_25 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooooo";
                exception_lineno = 69;
                goto try_except_handler_8;
            }
            {
                PyObject *old = tmp_tuple_unpack_3__element_1;
                tmp_tuple_unpack_3__element_1 = tmp_assign_source_25;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_unpack_6;
            CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
            tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
            tmp_assign_source_26 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
            if ( tmp_assign_source_26 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooooo";
                exception_lineno = 69;
                goto try_except_handler_8;
            }
            {
                PyObject *old = tmp_tuple_unpack_3__element_2;
                tmp_tuple_unpack_3__element_2 = tmp_assign_source_26;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_2;
            CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
            tmp_iterator_name_2 = tmp_tuple_unpack_3__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 69;
                        goto try_except_handler_8;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooooooooo";
                exception_lineno = 69;
                goto try_except_handler_8;
            }
        }
        goto try_end_5;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
        Py_DECREF( tmp_tuple_unpack_3__source_iter );
        tmp_tuple_unpack_3__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_7;
        // End of try:
        try_end_5:;
        goto try_end_6;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_3__element_1 );
        tmp_tuple_unpack_3__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_3__element_2 );
        tmp_tuple_unpack_3__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_4;
        // End of try:
        try_end_6:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
        Py_DECREF( tmp_tuple_unpack_3__source_iter );
        tmp_tuple_unpack_3__source_iter = NULL;

        {
            PyObject *tmp_assign_source_27;
            CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
            tmp_assign_source_27 = tmp_tuple_unpack_3__element_1;
            {
                PyObject *old = var_lex_id;
                var_lex_id = tmp_assign_source_27;
                Py_INCREF( var_lex_id );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_3__element_1 );
        tmp_tuple_unpack_3__element_1 = NULL;

        {
            PyObject *tmp_assign_source_28;
            CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
            tmp_assign_source_28 = tmp_tuple_unpack_3__element_2;
            {
                PyObject *old = var_word_forms;
                var_word_forms = tmp_assign_source_28;
                Py_INCREF( var_word_forms );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_3__element_2 );
        tmp_tuple_unpack_3__element_2 = NULL;

        CHECK_OBJECT( var_word_forms );
        tmp_dictset_value = var_word_forms;
        CHECK_OBJECT( var_lexemes );
        tmp_dictset_dict = var_lexemes;
        CHECK_OBJECT( var_lex_id );
        tmp_dictset_key = var_lex_id;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_call_result_6;
            PyObject *tmp_args_element_name_8;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_xml_clear_elem );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_xml_clear_elem );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "xml_clear_elem" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 71;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_5 = tmp_mvar_value_7;
            CHECK_OBJECT( var_elem );
            tmp_args_element_name_8 = var_elem;
            frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 71;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            if ( tmp_call_result_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_6 );
        }
        goto branch_end_4;
        branch_no_4:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( var_elem );
            tmp_source_name_7 = var_elem;
            tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_tag );
            if ( tmp_compexpr_left_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 73;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            tmp_compexpr_right_5 = const_str_plain_link;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            Py_DECREF( tmp_compexpr_left_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 73;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_operand_name_2;
                CHECK_OBJECT( var_links );
                tmp_operand_name_2 = var_links;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                assert( !(tmp_res == -1) );
                tmp_condition_result_7 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    PyObject *tmp_called_instance_10;
                    PyObject *tmp_mvar_value_8;
                    PyObject *tmp_call_result_7;
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_logger );

                    if (unlikely( tmp_mvar_value_8 == NULL ))
                    {
                        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logger );
                    }

                    if ( tmp_mvar_value_8 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logger" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 75;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_4;
                    }

                    tmp_called_instance_10 = tmp_mvar_value_8;
                    frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 75;
                    tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_info, &PyTuple_GET_ITEM( const_tuple_str_digest_6af5c9035db191544f6324ea218e6100_tuple, 0 ) );

                    if ( tmp_call_result_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 75;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_4;
                    }
                    Py_DECREF( tmp_call_result_7 );
                }
                branch_no_7:;
            }
            {
                PyObject *tmp_assign_source_29;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_called_instance_11;
                PyObject *tmp_called_instance_12;
                PyObject *tmp_called_instance_13;
                CHECK_OBJECT( var_elem );
                tmp_called_instance_11 = var_elem;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 78;
                tmp_tuple_element_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_11, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_from_tuple, 0 ) );

                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 78;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                tmp_assign_source_29 = PyTuple_New( 3 );
                PyTuple_SET_ITEM( tmp_assign_source_29, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( var_elem );
                tmp_called_instance_12 = var_elem;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 79;
                tmp_tuple_element_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_12, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_to_tuple, 0 ) );

                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_assign_source_29 );

                    exception_lineno = 79;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_assign_source_29, 1, tmp_tuple_element_3 );
                CHECK_OBJECT( var_elem );
                tmp_called_instance_13 = var_elem;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 80;
                tmp_tuple_element_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_13, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_type_tuple, 0 ) );

                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_assign_source_29 );

                    exception_lineno = 80;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_assign_source_29, 2, tmp_tuple_element_3 );
                {
                    PyObject *old = var_link_tuple;
                    var_link_tuple = tmp_assign_source_29;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_instance_14;
                PyObject *tmp_call_result_8;
                PyObject *tmp_args_element_name_9;
                CHECK_OBJECT( var_links );
                tmp_called_instance_14 = var_links;
                CHECK_OBJECT( var_link_tuple );
                tmp_args_element_name_9 = var_link_tuple;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 82;
                {
                    PyObject *call_args[] = { tmp_args_element_name_9 };
                    tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_14, const_str_plain_append, call_args );
                }

                if ( tmp_call_result_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 82;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                Py_DECREF( tmp_call_result_8 );
            }
            {
                PyObject *tmp_called_name_6;
                PyObject *tmp_mvar_value_9;
                PyObject *tmp_call_result_9;
                PyObject *tmp_args_element_name_10;
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_xml_clear_elem );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_xml_clear_elem );
                }

                if ( tmp_mvar_value_9 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "xml_clear_elem" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 83;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }

                tmp_called_name_6 = tmp_mvar_value_9;
                CHECK_OBJECT( var_elem );
                tmp_args_element_name_10 = var_elem;
                frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 83;
                {
                    PyObject *call_args[] = { tmp_args_element_name_10 };
                    tmp_call_result_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
                }

                if ( tmp_call_result_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 83;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_4;
                }
                Py_DECREF( tmp_call_result_9 );
            }
            branch_no_6:;
        }
        branch_end_4:;
    }
    {
        nuitka_bool tmp_condition_result_8;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_operand_name_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_len_arg_2;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( var_lexemes );
        tmp_len_arg_1 = var_lexemes;
        tmp_compexpr_left_6 = BUILTIN_LEN( tmp_len_arg_1 );
        assert( !(tmp_compexpr_left_6 == NULL) );
        if ( var__lexemes_len == NULL )
        {
            Py_DECREF( tmp_compexpr_left_6 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "_lexemes_len" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }

        tmp_compexpr_right_6 = var__lexemes_len;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        Py_DECREF( tmp_compexpr_left_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_lexemes );
        tmp_len_arg_2 = var_lexemes;
        tmp_left_name_1 = BUILTIN_LEN( tmp_len_arg_2 );
        assert( !(tmp_left_name_1 == NULL) );
        tmp_right_name_1 = const_int_pos_50000;
        tmp_operand_name_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_operand_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        Py_DECREF( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "ooooooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_8 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_8 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            PyObject *tmp_called_name_7;
            PyObject *tmp_source_name_8;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_call_result_10;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_len_arg_3;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_logger );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logger );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logger" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 86;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }

            tmp_source_name_8 = tmp_mvar_value_10;
            tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_debug );
            if ( tmp_called_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            tmp_left_name_2 = const_str_digest_ffc3563b9dd49f79271721271071fff5;
            CHECK_OBJECT( var_lexemes );
            tmp_len_arg_3 = var_lexemes;
            tmp_right_name_2 = BUILTIN_LEN( tmp_len_arg_3 );
            assert( !(tmp_right_name_2 == NULL) );
            tmp_args_element_name_11 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_args_element_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_7 );

                exception_lineno = 86;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 86;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_call_result_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_element_name_11 );
            if ( tmp_call_result_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_10 );
        }
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_len_arg_4;
            CHECK_OBJECT( var_lexemes );
            tmp_len_arg_4 = var_lexemes;
            tmp_assign_source_30 = BUILTIN_LEN( tmp_len_arg_4 );
            assert( !(tmp_assign_source_30 == NULL) );
            {
                PyObject *old = var__lexemes_len;
                var__lexemes_len = tmp_assign_source_30;
                Py_XDECREF( old );
            }

        }
        branch_no_8:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 45;
        type_description_1 = "ooooooooooooooooo";
        goto try_except_handler_4;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_7;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_args_element_name_16;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_ParsedDictionary );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ParsedDictionary );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ParsedDictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_11;
        CHECK_OBJECT( var_lexemes );
        tmp_args_element_name_12 = var_lexemes;
        CHECK_OBJECT( var_links );
        tmp_args_element_name_13 = var_links;
        CHECK_OBJECT( var_grammemes );
        tmp_args_element_name_14 = var_grammemes;
        if ( var_version == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "version" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_15 = var_version;
        if ( var_revision == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "revision" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_16 = var_revision;
        frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame.f_lineno = 89;
        {
            PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14, tmp_args_element_name_15, tmp_args_element_name_16 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_8, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_195a4eaed54dd53ec6103e22fe764cbb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_195a4eaed54dd53ec6103e22fe764cbb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_195a4eaed54dd53ec6103e22fe764cbb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_195a4eaed54dd53ec6103e22fe764cbb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_195a4eaed54dd53ec6103e22fe764cbb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_195a4eaed54dd53ec6103e22fe764cbb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_195a4eaed54dd53ec6103e22fe764cbb,
        type_description_1,
        par_filename,
        var_links,
        var_lexemes,
        var_grammemes,
        var_version,
        var_revision,
        var__lexemes_len,
        var_ev,
        var_elem,
        var_name,
        var_parent,
        var_alias,
        var_description,
        var_grammeme,
        var_lex_id,
        var_word_forms,
        var_link_tuple
    );


    // Release cached frame.
    if ( frame_195a4eaed54dd53ec6103e22fe764cbb == cache_frame_195a4eaed54dd53ec6103e22fe764cbb )
    {
        Py_DECREF( frame_195a4eaed54dd53ec6103e22fe764cbb );
    }
    cache_frame_195a4eaed54dd53ec6103e22fe764cbb = NULL;

    assertFrameObject( frame_195a4eaed54dd53ec6103e22fe764cbb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_3_parse_opencorpora_xml );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)var_links );
    Py_DECREF( var_links );
    var_links = NULL;

    CHECK_OBJECT( (PyObject *)var_lexemes );
    Py_DECREF( var_lexemes );
    var_lexemes = NULL;

    CHECK_OBJECT( (PyObject *)var_grammemes );
    Py_DECREF( var_grammemes );
    var_grammemes = NULL;

    Py_XDECREF( var_version );
    var_version = NULL;

    Py_XDECREF( var_revision );
    var_revision = NULL;

    Py_XDECREF( var__lexemes_len );
    var__lexemes_len = NULL;

    Py_XDECREF( var_ev );
    var_ev = NULL;

    Py_XDECREF( var_elem );
    var_elem = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_parent );
    var_parent = NULL;

    Py_XDECREF( var_alias );
    var_alias = NULL;

    Py_XDECREF( var_description );
    var_description = NULL;

    Py_XDECREF( var_grammeme );
    var_grammeme = NULL;

    Py_XDECREF( var_lex_id );
    var_lex_id = NULL;

    Py_XDECREF( var_word_forms );
    var_word_forms = NULL;

    Py_XDECREF( var_link_tuple );
    var_link_tuple = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)var_links );
    Py_DECREF( var_links );
    var_links = NULL;

    CHECK_OBJECT( (PyObject *)var_lexemes );
    Py_DECREF( var_lexemes );
    var_lexemes = NULL;

    CHECK_OBJECT( (PyObject *)var_grammemes );
    Py_DECREF( var_grammemes );
    var_grammemes = NULL;

    Py_XDECREF( var_version );
    var_version = NULL;

    Py_XDECREF( var_revision );
    var_revision = NULL;

    Py_XDECREF( var__lexemes_len );
    var__lexemes_len = NULL;

    Py_XDECREF( var_ev );
    var_ev = NULL;

    Py_XDECREF( var_elem );
    var_elem = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_parent );
    var_parent = NULL;

    Py_XDECREF( var_alias );
    var_alias = NULL;

    Py_XDECREF( var_description );
    var_description = NULL;

    Py_XDECREF( var_grammeme );
    var_grammeme = NULL;

    Py_XDECREF( var_lex_id );
    var_lex_id = NULL;

    Py_XDECREF( var_word_forms );
    var_word_forms = NULL;

    Py_XDECREF( var_link_tuple );
    var_link_tuple = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_3_parse_opencorpora_xml );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_elem = python_pars[ 0 ];
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_102f62da71b77e0152e89b2fc8752154;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_102f62da71b77e0152e89b2fc8752154 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_102f62da71b77e0152e89b2fc8752154, codeobj_102f62da71b77e0152e89b2fc8752154, module_pymorphy2$opencorpora_dict$parse, sizeof(void *) );
    frame_102f62da71b77e0152e89b2fc8752154 = cache_frame_102f62da71b77e0152e89b2fc8752154;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_102f62da71b77e0152e89b2fc8752154 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_102f62da71b77e0152e89b2fc8752154 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        tmp_source_name_1 = const_str_chr_44;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        assert( !(tmp_called_name_1 == NULL) );
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_elem );
            tmp_called_instance_1 = par_elem;
            frame_102f62da71b77e0152e89b2fc8752154->m_frame.f_lineno = 93;
            tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_findall, &PyTuple_GET_ITEM( const_tuple_str_plain_g_tuple, 0 ) );

            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_1;
        }
        // Tried code:
        tmp_args_element_name_1 = pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem );
        return NULL;
        outline_result_1:;
        frame_102f62da71b77e0152e89b2fc8752154->m_frame.f_lineno = 93;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_102f62da71b77e0152e89b2fc8752154 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_102f62da71b77e0152e89b2fc8752154 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_102f62da71b77e0152e89b2fc8752154 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_102f62da71b77e0152e89b2fc8752154, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_102f62da71b77e0152e89b2fc8752154->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_102f62da71b77e0152e89b2fc8752154, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_102f62da71b77e0152e89b2fc8752154,
        type_description_1,
        par_elem
    );


    // Release cached frame.
    if ( frame_102f62da71b77e0152e89b2fc8752154 == cache_frame_102f62da71b77e0152e89b2fc8752154 )
    {
        Py_DECREF( frame_102f62da71b77e0152e89b2fc8752154 );
    }
    cache_frame_102f62da71b77e0152e89b2fc8752154 = NULL;

    assertFrameObject( frame_102f62da71b77e0152e89b2fc8752154 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_elem );
    Py_DECREF( par_elem );
    par_elem = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_elem );
    Py_DECREF( par_elem );
    par_elem = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem$$$genexpr_1_genexpr_locals {
    PyObject *var_g;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem$$$genexpr_1_genexpr_locals *generator_heap = (struct pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_g = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_9bf203e631b846eb0b5f407c49516f4f, module_pymorphy2$opencorpora_dict$parse, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 93;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_g;
            generator_heap->var_g = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_g );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_instance_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_g );
        tmp_called_instance_1 = generator_heap->var_g;
        generator->m_frame->m_frame.f_lineno = 93;
        tmp_expression_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_v_tuple, 0 ) );

        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 93;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_instance_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_instance_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 93;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 93;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_g
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_g );
    generator_heap->var_g = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_g );
    generator_heap->var_g = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem$$$genexpr_1_genexpr_context,
        module_pymorphy2$opencorpora_dict$parse,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_d23049c30e01960e937fa4f898226536,
#endif
        codeobj_9bf203e631b846eb0b5f407c49516f4f,
        1,
        sizeof(struct pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_pymorphy2$opencorpora_dict$parse$$$function_5__word_forms_from_xml_elem( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_elem = python_pars[ 0 ];
    PyObject *var_lexeme = NULL;
    PyObject *var_lex_id = NULL;
    PyObject *var_base_info = NULL;
    PyObject *var_base_grammemes = NULL;
    PyObject *var_form_elem = NULL;
    PyObject *var_grammemes = NULL;
    PyObject *var_form = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_718ae49ea6cda6528f3dea607d343cd5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_718ae49ea6cda6528f3dea607d343cd5 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_lexeme == NULL );
        var_lexeme = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_718ae49ea6cda6528f3dea607d343cd5, codeobj_718ae49ea6cda6528f3dea607d343cd5, module_pymorphy2$opencorpora_dict$parse, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_718ae49ea6cda6528f3dea607d343cd5 = cache_frame_718ae49ea6cda6528f3dea607d343cd5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_718ae49ea6cda6528f3dea607d343cd5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_718ae49ea6cda6528f3dea607d343cd5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_elem );
        tmp_called_instance_1 = par_elem;
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 101;
        tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_id_tuple, 0 ) );

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_lex_id == NULL );
        var_lex_id = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( par_elem );
        tmp_len_arg_1 = par_elem;
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_tuple_element_1;
            CHECK_OBJECT( var_lex_id );
            tmp_tuple_element_1 = var_lex_id;
            tmp_return_value = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( var_lexeme );
            tmp_tuple_element_1 = var_lexeme;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_elem );
        tmp_called_instance_2 = par_elem;
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 106;
        tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_findall, &PyTuple_GET_ITEM( const_tuple_str_plain_l_tuple, 0 ) );

        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_base_info == NULL );
        var_base_info = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_2;
        CHECK_OBJECT( var_base_info );
        tmp_len_arg_2 = var_base_info;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_2 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 108;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain__grammemes_from_elem );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__grammemes_from_elem );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_grammemes_from_elem" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 109;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_base_info );
        tmp_subscribed_name_1 = var_base_info;
        tmp_subscript_name_1 = const_int_0;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 109;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_base_grammemes == NULL );
        var_base_grammemes = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( par_elem );
        tmp_called_instance_3 = par_elem;
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 111;
        tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_findall, &PyTuple_GET_ITEM( const_tuple_str_plain_f_tuple, 0 ) );

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_5;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 111;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_7 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_form_elem;
            var_form_elem = tmp_assign_source_7;
            Py_INCREF( var_form_elem );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain__grammemes_from_elem );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__grammemes_from_elem );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_grammemes_from_elem" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( var_form_elem );
        tmp_args_element_name_2 = var_form_elem;
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 112;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_grammemes;
            var_grammemes = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_called_instance_5;
        CHECK_OBJECT( var_form_elem );
        tmp_called_instance_5 = var_form_elem;
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 113;
        tmp_called_instance_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_t_tuple, 0 ) );

        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 113;
        tmp_assign_source_9 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_lower );
        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_form;
            var_form = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_list_element_1;
        CHECK_OBJECT( var_lexeme );
        tmp_source_name_1 = var_lexeme;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_form );
        tmp_tuple_element_2 = var_form;
        tmp_args_element_name_3 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_element_name_3, 0, tmp_tuple_element_2 );
        tmp_called_instance_7 = const_str_space;
        CHECK_OBJECT( var_base_grammemes );
        tmp_list_element_1 = var_base_grammemes;
        tmp_args_element_name_4 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_4, 0, tmp_list_element_1 );
        CHECK_OBJECT( var_grammemes );
        tmp_list_element_1 = var_grammemes;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_4, 1, tmp_list_element_1 );
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 115;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_called_instance_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_join, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_called_instance_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 115;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 115;
        tmp_tuple_element_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_strip );
        Py_DECREF( tmp_called_instance_6 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 115;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        PyTuple_SET_ITEM( tmp_args_element_name_3, 1, tmp_tuple_element_2 );
        frame_718ae49ea6cda6528f3dea607d343cd5->m_frame.f_lineno = 114;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 111;
        type_description_1 = "oooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_718ae49ea6cda6528f3dea607d343cd5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_718ae49ea6cda6528f3dea607d343cd5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_718ae49ea6cda6528f3dea607d343cd5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_718ae49ea6cda6528f3dea607d343cd5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_718ae49ea6cda6528f3dea607d343cd5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_718ae49ea6cda6528f3dea607d343cd5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_718ae49ea6cda6528f3dea607d343cd5,
        type_description_1,
        par_elem,
        var_lexeme,
        var_lex_id,
        var_base_info,
        var_base_grammemes,
        var_form_elem,
        var_grammemes,
        var_form
    );


    // Release cached frame.
    if ( frame_718ae49ea6cda6528f3dea607d343cd5 == cache_frame_718ae49ea6cda6528f3dea607d343cd5 )
    {
        Py_DECREF( frame_718ae49ea6cda6528f3dea607d343cd5 );
    }
    cache_frame_718ae49ea6cda6528f3dea607d343cd5 = NULL;

    assertFrameObject( frame_718ae49ea6cda6528f3dea607d343cd5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_tuple_element_3;
        CHECK_OBJECT( var_lex_id );
        tmp_tuple_element_3 = var_lex_id;
        tmp_return_value = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_3 );
        CHECK_OBJECT( var_lexeme );
        tmp_tuple_element_3 = var_lexeme;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_3 );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_5__word_forms_from_xml_elem );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_elem );
    Py_DECREF( par_elem );
    par_elem = NULL;

    CHECK_OBJECT( (PyObject *)var_lexeme );
    Py_DECREF( var_lexeme );
    var_lexeme = NULL;

    CHECK_OBJECT( (PyObject *)var_lex_id );
    Py_DECREF( var_lex_id );
    var_lex_id = NULL;

    Py_XDECREF( var_base_info );
    var_base_info = NULL;

    Py_XDECREF( var_base_grammemes );
    var_base_grammemes = NULL;

    Py_XDECREF( var_form_elem );
    var_form_elem = NULL;

    Py_XDECREF( var_grammemes );
    var_grammemes = NULL;

    Py_XDECREF( var_form );
    var_form = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_elem );
    Py_DECREF( par_elem );
    par_elem = NULL;

    CHECK_OBJECT( (PyObject *)var_lexeme );
    Py_DECREF( var_lexeme );
    var_lexeme = NULL;

    Py_XDECREF( var_lex_id );
    var_lex_id = NULL;

    Py_XDECREF( var_base_info );
    var_base_info = NULL;

    Py_XDECREF( var_base_grammemes );
    var_base_grammemes = NULL;

    Py_XDECREF( var_form_elem );
    var_form_elem = NULL;

    Py_XDECREF( var_grammemes );
    var_grammemes = NULL;

    Py_XDECREF( var_form );
    var_form = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse$$$function_5__word_forms_from_xml_elem );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_1_xml_clear_elem(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$parse$$$function_1_xml_clear_elem,
        const_str_plain_xml_clear_elem,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1084451cde5ccd6aef9f0350e39959f2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$parse,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_2_xml_clear_elem(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$parse$$$function_2_xml_clear_elem,
        const_str_plain_xml_clear_elem,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3a996733c5203aba96c3c5506e1a837a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$parse,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_3_parse_opencorpora_xml(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$parse$$$function_3_parse_opencorpora_xml,
        const_str_plain_parse_opencorpora_xml,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_195a4eaed54dd53ec6103e22fe764cbb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$parse,
        const_str_digest_dfb31e364b9832e04784d4cdbf5fd05b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem,
        const_str_plain__grammemes_from_elem,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_102f62da71b77e0152e89b2fc8752154,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$parse,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_5__word_forms_from_xml_elem(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$parse$$$function_5__word_forms_from_xml_elem,
        const_str_plain__word_forms_from_xml_elem,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_718ae49ea6cda6528f3dea607d343cd5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$parse,
        const_str_digest_8d18621b6c1c50010f0e396f8ff9124a,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pymorphy2$opencorpora_dict$parse =
{
    PyModuleDef_HEAD_INIT,
    "pymorphy2.opencorpora_dict.parse",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(pymorphy2$opencorpora_dict$parse)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(pymorphy2$opencorpora_dict$parse)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pymorphy2$opencorpora_dict$parse );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pymorphy2.opencorpora_dict.parse: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pymorphy2.opencorpora_dict.parse: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pymorphy2.opencorpora_dict.parse: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpymorphy2$opencorpora_dict$parse" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pymorphy2$opencorpora_dict$parse = Py_InitModule4(
        "pymorphy2.opencorpora_dict.parse",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pymorphy2$opencorpora_dict$parse = PyModule_Create( &mdef_pymorphy2$opencorpora_dict$parse );
#endif

    moduledict_pymorphy2$opencorpora_dict$parse = MODULE_DICT( module_pymorphy2$opencorpora_dict$parse );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_pymorphy2$opencorpora_dict$parse,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pymorphy2$opencorpora_dict$parse,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pymorphy2$opencorpora_dict$parse,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pymorphy2$opencorpora_dict$parse,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pymorphy2$opencorpora_dict$parse );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_096e06c9ce7af0f307ff3d8f2f04028b, module_pymorphy2$opencorpora_dict$parse );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_72f718beb2fbc5300782c41934593b24;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_f82c239c212e4d830c30a94140099e1f;
        UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_72f718beb2fbc5300782c41934593b24 = MAKE_MODULE_FRAME( codeobj_72f718beb2fbc5300782c41934593b24, module_pymorphy2$opencorpora_dict$parse );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_72f718beb2fbc5300782c41934593b24 );
    assert( Py_REFCNT( frame_72f718beb2fbc5300782c41934593b24 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = 6;
        tmp_assign_source_4 = PyImport_ImportModule("__future__");
        assert( !(tmp_assign_source_4 == NULL) );
        assert( tmp_import_from_1__module == NULL );
        Py_INCREF( tmp_assign_source_4 );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_absolute_import );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_absolute_import, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_unicode_literals );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_division );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_division, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_logging;
        tmp_globals_name_1 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$parse;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = 8;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_logging, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_collections;
        tmp_globals_name_2 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$parse;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = 9;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_collections, tmp_assign_source_9 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_22458dfdf60aff9ff5147019bb64ce31;
        tmp_globals_name_3 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$parse;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_iterparse_tuple;
        tmp_level_name_3 = const_int_0;
        frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = 12;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_iterparse );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_iterparse, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_1_xml_clear_elem(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_xml_clear_elem, tmp_assign_source_11 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_72f718beb2fbc5300782c41934593b24, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_72f718beb2fbc5300782c41934593b24, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_import_name_from_5;
            PyObject *tmp_name_name_4;
            PyObject *tmp_globals_name_4;
            PyObject *tmp_locals_name_4;
            PyObject *tmp_fromlist_name_4;
            PyObject *tmp_level_name_4;
            tmp_name_name_4 = const_str_digest_b8b8773fbd08a10b8c2607df28400381;
            tmp_globals_name_4 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$parse;
            tmp_locals_name_4 = Py_None;
            tmp_fromlist_name_4 = const_tuple_str_plain_iterparse_tuple;
            tmp_level_name_4 = const_int_0;
            frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = 21;
            tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
            if ( tmp_import_name_from_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_4;
            }
            tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_iterparse );
            Py_DECREF( tmp_import_name_from_5 );
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_4;
            }
            UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_iterparse, tmp_assign_source_12 );
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_3 == NULL )
        {
            exception_keeper_tb_3 = MAKE_TRACEBACK( frame_72f718beb2fbc5300782c41934593b24, exception_keeper_lineno_3 );
        }
        else if ( exception_keeper_lineno_3 != 0 )
        {
            exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_72f718beb2fbc5300782c41934593b24, exception_keeper_lineno_3 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
        PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = PyExc_ImportError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_5;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_13;
                PyObject *tmp_import_name_from_6;
                PyObject *tmp_name_name_5;
                PyObject *tmp_globals_name_5;
                PyObject *tmp_locals_name_5;
                PyObject *tmp_fromlist_name_5;
                PyObject *tmp_level_name_5;
                tmp_name_name_5 = const_str_digest_04d67503345d6d64752f0c9fc2a764c3;
                tmp_globals_name_5 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$parse;
                tmp_locals_name_5 = Py_None;
                tmp_fromlist_name_5 = const_tuple_str_plain_iterparse_tuple;
                tmp_level_name_5 = const_int_0;
                frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = 23;
                tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
                if ( tmp_import_name_from_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 23;

                    goto try_except_handler_5;
                }
                tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_iterparse );
                Py_DECREF( tmp_import_name_from_6 );
                if ( tmp_assign_source_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 23;

                    goto try_except_handler_5;
                }
                UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_iterparse, tmp_assign_source_13 );
            }
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 20;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_72f718beb2fbc5300782c41934593b24->m_frame) frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = exception_tb->tb_lineno;

            goto try_except_handler_5;
            branch_end_2:;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_3;
        // End of try:
        try_end_4:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_3;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse );
        return MOD_RETURN_VALUE( NULL );
        // End of try:
        try_end_3:;
        {
            PyObject *tmp_assign_source_14;
            tmp_assign_source_14 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_2_xml_clear_elem(  );



            UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_xml_clear_elem, tmp_assign_source_14 );
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 11;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_72f718beb2fbc5300782c41934593b24->m_frame) frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_3;
        branch_end_1:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$parse );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_2:;
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_logging );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logging );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logging" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 29;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = 29;
        tmp_assign_source_15 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_getLogger, &PyTuple_GET_ITEM( const_tuple_str_digest_096e06c9ce7af0f307ff3d8f2f04028b_tuple, 0 ) );

        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_logger, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_collections );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_collections );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "collections" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 31;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_4;
        frame_72f718beb2fbc5300782c41934593b24->m_frame.f_lineno = 31;
        tmp_assign_source_16 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_namedtuple, &PyTuple_GET_ITEM( const_tuple_c8a95e2fa80a3fa9e52aa9ec05e6f60c_tuple, 0 ) );

        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_ParsedDictionary, tmp_assign_source_16 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_72f718beb2fbc5300782c41934593b24 );
#endif
    popFrameStack();

    assertFrameObject( frame_72f718beb2fbc5300782c41934593b24 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_72f718beb2fbc5300782c41934593b24 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_72f718beb2fbc5300782c41934593b24, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_72f718beb2fbc5300782c41934593b24->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_72f718beb2fbc5300782c41934593b24, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_3_parse_opencorpora_xml(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain_parse_opencorpora_xml, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_4__grammemes_from_elem(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain__grammemes_from_elem, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$parse$$$function_5__word_forms_from_xml_elem(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$parse, (Nuitka_StringObject *)const_str_plain__word_forms_from_xml_elem, tmp_assign_source_19 );
    }

    return MOD_RETURN_VALUE( module_pymorphy2$opencorpora_dict$parse );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
