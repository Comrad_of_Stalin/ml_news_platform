/* Generated code for Python module 'jinja2.asyncsupport'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jinja2$asyncsupport" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jinja2$asyncsupport;
PyDictObject *moduledict_jinja2$asyncsupport;

/* The declarations of module constants used, if any. */
static PyObject *const_str_plain_async_iterator;
static PyObject *const_str_digest_c162fb909647330a34e1c7480f74a6a1;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_str_plain_TemplateModule_tuple;
extern PyObject *const_str_plain_dict;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_ASYNC_FILTERS;
static PyObject *const_tuple_str_plain_async_gen_str_plain_rv_str_plain_collect_tuple;
extern PyObject *const_str_plain_vars;
extern PyObject *const_str_plain_root_render_func;
extern PyObject *const_str_plain__last_iteration;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_plain_wrap_render_func;
extern PyObject *const_str_plain_iterable;
extern PyObject *const_str_plain__environment;
static PyObject *const_str_plain_get_default_module_async;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_object;
static PyObject *const_tuple_8941e6f2368d99ee0e2007463d5ce197_tuple;
extern PyObject *const_str_plain_args;
extern PyObject *const_tuple_str_plain_value_tuple;
extern PyObject *const_tuple_172fed3f8b88a5921847d3a48ef4b7c9_tuple;
static PyObject *const_tuple_4e790e31576b423020c6860adf0e8fd3_tuple;
extern PyObject *const_tuple_str_plain_self_str_plain_ctx_tuple;
static PyObject *const_str_plain_original_generate;
static PyObject *const_str_digest_bffb2332ad84fd41c0c36e13b1e5706e;
static PyObject *const_tuple_str_plain_self_str_plain_original_default_module_tuple;
static PyObject *const_str_plain_wrap_block_reference_call;
extern PyObject *const_str_plain_rv;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain__module;
extern PyObject *const_tuple_str_plain_self_str_plain_rv_tuple;
static PyObject *const_tuple_str_plain_original_render_str_plain_render_tuple;
static PyObject *const_tuple_25751c10c19e659145b2b44e8eaa0212_tuple;
static PyObject *const_str_digest_86dc107ababe21ba1e74f24723c00d16;
extern PyObject *const_str_plain_internalcode;
extern PyObject *const_str_plain__after;
extern PyObject *const_str_plain_is_async;
static PyObject *const_tuple_str_plain_Template_tuple;
static PyObject *const_str_plain_original_invoke;
extern PyObject *const_str_plain_FILTERS;
static PyObject *const_str_plain_wrap_generate_func;
static PyObject *const_str_digest_1f42321ef9bba5d561ea6c02a7960d6b;
static PyObject *const_str_plain_make_async_loop_context;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_concat;
static PyObject *const_str_digest_b80c01c7bf5b82290397cb53c270a4f4;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_tuple_ccc0332f38bfcbbe463a0112fe57cf2a_tuple;
static PyObject *const_tuple_5a8b0942b888893bd13b4eaf4dbb453e_tuple;
extern PyObject *const_str_plain_render_async;
static PyObject *const_tuple_str_plain_event_str_plain_async_gen_str_plain_rv_tuple;
extern PyObject *const_str_plain_loop;
extern PyObject *const_str_plain_render;
extern PyObject *const_str_digest_37aca85c698d791dc4bb639deca6348b;
extern PyObject *const_str_plain_index0;
extern PyObject *const_str_plain_make_module_async;
static PyObject *const_str_digest_60fb315dc731b29d8eacffa85fd5fbcf;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_body_stream;
static PyObject *const_str_plain_patch_runtime;
static PyObject *const_str_digest_9eec2e33de8cedad6097d6f88595b09c;
extern PyObject *const_str_plain_generate;
static PyObject *const_str_plain_async_gen;
extern PyObject *const_str_plain__context;
static PyObject *const_tuple_9af200a6b8840745008d9911626e1a7c_tuple;
extern PyObject *const_str_plain_arguments;
extern PyObject *const_str_digest_38d72d37090218643439302eee0c8cc7;
static PyObject *const_tuple_730b53faf75284ef0ec40b0e377a3372_tuple;
static PyObject *const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple;
static PyObject *const_str_digest_b2d89451c7e47bfda7c99b2c93d722a7;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_item;
static PyObject *const_str_plain_wrap_default_module;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_ctx;
extern PyObject *const_tuple_type_TypeError_type_AttributeError_tuple;
static PyObject *const_str_plain_AsyncLoopContextIterator;
extern PyObject *const_str_plain_after;
static PyObject *const_tuple_str_plain_self_str_plain_original_call_str_plain_async_call_tuple;
extern PyObject *const_str_plain_environment;
extern PyObject *const_str_plain__func;
extern PyObject *const_str_plain_recurse;
extern PyObject *const_str_plain_Template;
extern PyObject *const_str_digest_50cd10b5aab52d808bd46c35f0566289;
extern PyObject *const_str_plain_shared;
extern PyObject *const_str_plain_context;
extern PyObject *const_tuple_empty;
static PyObject *const_tuple_dd8870a0791446f1214ebc44ee539f3a_tuple;
static PyObject *const_tuple_eaa6c2fea91dd7ebca5336178766e7bb_tuple;
static PyObject *const_tuple_str_plain_FILTERS_str_plain_ASYNC_FILTERS_tuple;
static PyObject *const_str_plain__convert_generator;
extern PyObject *const_str_plain_event;
extern PyObject *const_str_plain_append;
static PyObject *const_str_plain_auto_await;
extern PyObject *const_str_plain__before;
static PyObject *const_str_plain__get_default_module_async;
extern PyObject *const_str_plain_new_context;
extern PyObject *const_str_plain_locals;
static PyObject *const_str_digest_33a6af52fee903f72e0e65a535276972;
extern PyObject *const_str_plain__length;
extern PyObject *const_str_plain_BlockReference;
extern PyObject *const_str_plain_isawaitable;
extern PyObject *const_str_plain__depth;
extern PyObject *const_str_digest_e9c0aba9ba51d097a6babda6163af2fe;
extern PyObject *const_tuple_str_plain_update_wrapper_tuple;
extern PyObject *const_str_plain_False;
static PyObject *const_tuple_str_plain_concat_str_plain_internalcode_str_plain_Markup_tuple;
extern PyObject *const_str_plain_Markup;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_LoopContextBase;
extern PyObject *const_str_plain_auto_aiter;
static PyObject *const_str_digest_a1e2dbb21a29bf138fd240dc65f4d45d;
static PyObject *const_tuple_str_plain_BlockReference_str_plain_Macro_tuple;
static PyObject *const_str_digest_3fa472849f7563cdf43fdd81ca51dc6e;
extern PyObject *const_tuple_str_plain_self_str_plain_context_tuple;
extern PyObject *const_str_plain_TemplateModule;
static PyObject *const_str_plain_async_invoke;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_depth0;
extern PyObject *const_str_plain_update_wrapper;
static PyObject *const_str_digest_bea5503dcad2fbda54f7f5ef23d14ba2;
static PyObject *const_tuple_2737c610e5ed955ada0ea32e93b5dc10_tuple;
extern PyObject *const_str_plain_asyncio;
static PyObject *const_str_digest_53ced86d8a41710c1e4df15bc1e68545;
static PyObject *const_str_plain_async_call;
static PyObject *const_str_digest_361aa6e29b7b6ee9ca4424254bc2ed83;
extern PyObject *const_str_plain___slots__;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_undefined;
extern PyObject *const_tuple_none_int_0_tuple;
extern PyObject *const_str_plain_length;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_exc_info;
static PyObject *const_tuple_str_plain_ASYNC_FILTERS_tuple;
extern PyObject *const_str_plain_handle_exception;
extern PyObject *const_str_plain_autoescape;
extern PyObject *const_str_plain___anext__;
static PyObject *const_str_plain_concat_async;
extern PyObject *const_str_digest_74893e30ca8be260e576d22e2956d0c6;
static PyObject *const_str_digest_49a9c2b6c4e1e6e1700f3e3846a4ff46;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain__get_default_module;
extern PyObject *const_str_plain_patch_all;
extern PyObject *const_str_plain_get_event_loop;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_eval_ctx;
static PyObject *const_str_digest_0092c3cda9e74bac591e5be98e5ba5f3;
static PyObject *const_tuple_str_plain_iterable_str_plain_item_tuple;
static PyObject *const_str_digest_fb845e53b3d15f89e589e1d47fc1ead5;
extern PyObject *const_str_plain___module__;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain_functools;
extern PyObject *const_str_plain_sys;
extern PyObject *const_tuple_none_false_none_tuple;
extern PyObject *const_str_plain__current;
static PyObject *const_str_plain_original_render;
extern PyObject *const_str_plain_update;
extern PyObject *const_int_pos_1;
static PyObject *const_tuple_3a916ae68ea77a1da8b015681b8c7b7b_tuple;
extern PyObject *const_tuple_str_plain_FILTERS_tuple;
static PyObject *const_str_plain_original_call;
extern PyObject *const_str_plain_jinja2;
extern PyObject *const_tuple_str_plain_context_tuple;
static PyObject *const_tuple_str_plain_LoopContextBase_str_plain__last_iteration_tuple;
static PyObject *const_str_plain_AsyncLoopContext;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_str_plain_original_default_module;
extern PyObject *const_str_digest_6a1b15fc617d9b2615daf846b271eabb;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_wrap_macro_invoke;
static PyObject *const_str_digest_56383ec153fbb011ff77a421b05c140c;
extern PyObject *const_str_plain_inspect;
extern PyObject *const_str_plain__stack;
extern PyObject *const_str_plain___call__;
static PyObject *const_str_digest_3726530226521a6996a67c61864a81b0;
static PyObject *const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple;
static PyObject *const_str_digest_1558c9fe5a9ec837ccc69754c23d4d52;
extern PyObject *const_str_plain_run_until_complete;
extern PyObject *const_str_plain_generate_async;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_collect;
static PyObject *const_str_digest_3ea09c231a2793f33456c0650667083b;
static PyObject *const_str_plain_patch_template;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain__async_iterator;
static PyObject *const_str_plain_patch_filters;
extern PyObject *const_str_plain_property;
extern PyObject *const_str_plain__invoke;
extern PyObject *const_str_plain___aiter__;
extern PyObject *const_str_plain_Macro;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_async_iterator = UNSTREAM_STRING_ASCII( &constant_bin[ 1031007 ], 14, 1 );
    const_str_digest_c162fb909647330a34e1c7480f74a6a1 = UNSTREAM_STRING_ASCII( &constant_bin[ 1031021 ], 34, 0 );
    const_tuple_str_plain_TemplateModule_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_TemplateModule_tuple, 0, const_str_plain_TemplateModule ); Py_INCREF( const_str_plain_TemplateModule );
    const_tuple_str_plain_async_gen_str_plain_rv_str_plain_collect_tuple = PyTuple_New( 3 );
    const_str_plain_async_gen = UNSTREAM_STRING_ASCII( &constant_bin[ 971109 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_async_gen_str_plain_rv_str_plain_collect_tuple, 0, const_str_plain_async_gen ); Py_INCREF( const_str_plain_async_gen );
    PyTuple_SET_ITEM( const_tuple_str_plain_async_gen_str_plain_rv_str_plain_collect_tuple, 1, const_str_plain_rv ); Py_INCREF( const_str_plain_rv );
    PyTuple_SET_ITEM( const_tuple_str_plain_async_gen_str_plain_rv_str_plain_collect_tuple, 2, const_str_plain_collect ); Py_INCREF( const_str_plain_collect );
    const_str_plain_wrap_render_func = UNSTREAM_STRING_ASCII( &constant_bin[ 1031055 ], 16, 1 );
    const_str_plain_get_default_module_async = UNSTREAM_STRING_ASCII( &constant_bin[ 1031071 ], 24, 1 );
    const_tuple_8941e6f2368d99ee0e2007463d5ce197_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_8941e6f2368d99ee0e2007463d5ce197_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_8941e6f2368d99ee0e2007463d5ce197_tuple, 1, const_str_plain_loop ); Py_INCREF( const_str_plain_loop );
    PyTuple_SET_ITEM( const_tuple_8941e6f2368d99ee0e2007463d5ce197_tuple, 2, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_8941e6f2368d99ee0e2007463d5ce197_tuple, 3, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_8941e6f2368d99ee0e2007463d5ce197_tuple, 4, const_str_plain_async_gen ); Py_INCREF( const_str_plain_async_gen );
    const_tuple_4e790e31576b423020c6860adf0e8fd3_tuple = PyTuple_New( 3 );
    const_str_plain_original_invoke = UNSTREAM_STRING_ASCII( &constant_bin[ 1031095 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_4e790e31576b423020c6860adf0e8fd3_tuple, 0, const_str_plain_original_invoke ); Py_INCREF( const_str_plain_original_invoke );
    const_str_plain_async_invoke = UNSTREAM_STRING_ASCII( &constant_bin[ 1031110 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_4e790e31576b423020c6860adf0e8fd3_tuple, 1, const_str_plain_async_invoke ); Py_INCREF( const_str_plain_async_invoke );
    PyTuple_SET_ITEM( const_tuple_4e790e31576b423020c6860adf0e8fd3_tuple, 2, const_str_plain__invoke ); Py_INCREF( const_str_plain__invoke );
    const_str_plain_original_generate = UNSTREAM_STRING_ASCII( &constant_bin[ 1031122 ], 17, 1 );
    const_str_digest_bffb2332ad84fd41c0c36e13b1e5706e = UNSTREAM_STRING_ASCII( &constant_bin[ 1031139 ], 250, 0 );
    const_tuple_str_plain_self_str_plain_original_default_module_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_original_default_module_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_original_default_module = UNSTREAM_STRING_ASCII( &constant_bin[ 1031389 ], 23, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_original_default_module_tuple, 1, const_str_plain_original_default_module ); Py_INCREF( const_str_plain_original_default_module );
    const_str_plain_wrap_block_reference_call = UNSTREAM_STRING_ASCII( &constant_bin[ 1031412 ], 25, 1 );
    const_tuple_str_plain_original_render_str_plain_render_tuple = PyTuple_New( 2 );
    const_str_plain_original_render = UNSTREAM_STRING_ASCII( &constant_bin[ 1031437 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_original_render_str_plain_render_tuple, 0, const_str_plain_original_render ); Py_INCREF( const_str_plain_original_render );
    PyTuple_SET_ITEM( const_tuple_str_plain_original_render_str_plain_render_tuple, 1, const_str_plain_render ); Py_INCREF( const_str_plain_render );
    const_tuple_25751c10c19e659145b2b44e8eaa0212_tuple = PyTuple_New( 3 );
    const_str_plain_original_call = UNSTREAM_STRING_ASCII( &constant_bin[ 1031452 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_25751c10c19e659145b2b44e8eaa0212_tuple, 0, const_str_plain_original_call ); Py_INCREF( const_str_plain_original_call );
    const_str_plain_async_call = UNSTREAM_STRING_ASCII( &constant_bin[ 1031465 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_25751c10c19e659145b2b44e8eaa0212_tuple, 1, const_str_plain_async_call ); Py_INCREF( const_str_plain_async_call );
    PyTuple_SET_ITEM( const_tuple_25751c10c19e659145b2b44e8eaa0212_tuple, 2, const_str_plain___call__ ); Py_INCREF( const_str_plain___call__ );
    const_str_digest_86dc107ababe21ba1e74f24723c00d16 = UNSTREAM_STRING_ASCII( &constant_bin[ 1031475 ], 23, 0 );
    const_tuple_str_plain_Template_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Template_tuple, 0, const_str_plain_Template ); Py_INCREF( const_str_plain_Template );
    const_str_plain_wrap_generate_func = UNSTREAM_STRING_ASCII( &constant_bin[ 1031498 ], 18, 1 );
    const_str_digest_1f42321ef9bba5d561ea6c02a7960d6b = UNSTREAM_STRING_ASCII( &constant_bin[ 1031516 ], 39, 0 );
    const_str_plain_make_async_loop_context = UNSTREAM_STRING_ASCII( &constant_bin[ 1031555 ], 23, 1 );
    const_str_digest_b80c01c7bf5b82290397cb53c270a4f4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1031578 ], 25, 0 );
    const_tuple_5a8b0942b888893bd13b4eaf4dbb453e_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_5a8b0942b888893bd13b4eaf4dbb453e_tuple, 0, const_str_plain_original_default_module ); Py_INCREF( const_str_plain_original_default_module );
    PyTuple_SET_ITEM( const_tuple_5a8b0942b888893bd13b4eaf4dbb453e_tuple, 1, const_str_plain__get_default_module ); Py_INCREF( const_str_plain__get_default_module );
    const_tuple_str_plain_event_str_plain_async_gen_str_plain_rv_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_async_gen_str_plain_rv_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_async_gen_str_plain_rv_tuple, 1, const_str_plain_async_gen ); Py_INCREF( const_str_plain_async_gen );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_async_gen_str_plain_rv_tuple, 2, const_str_plain_rv ); Py_INCREF( const_str_plain_rv );
    const_str_digest_60fb315dc731b29d8eacffa85fd5fbcf = UNSTREAM_STRING_ASCII( &constant_bin[ 1031603 ], 34, 0 );
    const_str_plain_patch_runtime = UNSTREAM_STRING_ASCII( &constant_bin[ 1031637 ], 13, 1 );
    const_str_digest_9eec2e33de8cedad6097d6f88595b09c = UNSTREAM_STRING_ASCII( &constant_bin[ 1031650 ], 56, 0 );
    const_tuple_9af200a6b8840745008d9911626e1a7c_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_9af200a6b8840745008d9911626e1a7c_tuple, 0, const_str_plain_iterable ); Py_INCREF( const_str_plain_iterable );
    PyTuple_SET_ITEM( const_tuple_9af200a6b8840745008d9911626e1a7c_tuple, 1, const_str_plain_undefined ); Py_INCREF( const_str_plain_undefined );
    PyTuple_SET_ITEM( const_tuple_9af200a6b8840745008d9911626e1a7c_tuple, 2, const_str_plain_recurse ); Py_INCREF( const_str_plain_recurse );
    PyTuple_SET_ITEM( const_tuple_9af200a6b8840745008d9911626e1a7c_tuple, 3, const_str_plain_depth0 ); Py_INCREF( const_str_plain_depth0 );
    PyTuple_SET_ITEM( const_tuple_9af200a6b8840745008d9911626e1a7c_tuple, 4, const_str_plain_length ); Py_INCREF( const_str_plain_length );
    PyTuple_SET_ITEM( const_tuple_9af200a6b8840745008d9911626e1a7c_tuple, 5, const_str_plain_async_iterator ); Py_INCREF( const_str_plain_async_iterator );
    PyTuple_SET_ITEM( const_tuple_9af200a6b8840745008d9911626e1a7c_tuple, 6, const_str_plain_after ); Py_INCREF( const_str_plain_after );
    const_tuple_730b53faf75284ef0ec40b0e377a3372_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_730b53faf75284ef0ec40b0e377a3372_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_730b53faf75284ef0ec40b0e377a3372_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_730b53faf75284ef0ec40b0e377a3372_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_730b53faf75284ef0ec40b0e377a3372_tuple, 3, const_str_plain_original_generate ); Py_INCREF( const_str_plain_original_generate );
    const_str_plain__convert_generator = UNSTREAM_STRING_ASCII( &constant_bin[ 1031706 ], 18, 1 );
    PyTuple_SET_ITEM( const_tuple_730b53faf75284ef0ec40b0e377a3372_tuple, 4, const_str_plain__convert_generator ); Py_INCREF( const_str_plain__convert_generator );
    const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple, 1, const_str_plain_async_iterator ); Py_INCREF( const_str_plain_async_iterator );
    PyTuple_SET_ITEM( const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple, 2, const_str_plain_undefined ); Py_INCREF( const_str_plain_undefined );
    PyTuple_SET_ITEM( const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple, 3, const_str_plain_after ); Py_INCREF( const_str_plain_after );
    PyTuple_SET_ITEM( const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple, 4, const_str_plain_length ); Py_INCREF( const_str_plain_length );
    PyTuple_SET_ITEM( const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple, 5, const_str_plain_recurse ); Py_INCREF( const_str_plain_recurse );
    PyTuple_SET_ITEM( const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple, 6, const_str_plain_depth0 ); Py_INCREF( const_str_plain_depth0 );
    const_str_digest_b2d89451c7e47bfda7c99b2c93d722a7 = UNSTREAM_STRING_ASCII( &constant_bin[ 1031724 ], 46, 0 );
    const_str_plain_wrap_default_module = UNSTREAM_STRING_ASCII( &constant_bin[ 1031770 ], 19, 1 );
    const_str_plain_AsyncLoopContextIterator = UNSTREAM_STRING_ASCII( &constant_bin[ 1031603 ], 24, 1 );
    const_tuple_str_plain_self_str_plain_original_call_str_plain_async_call_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_original_call_str_plain_async_call_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_original_call_str_plain_async_call_tuple, 1, const_str_plain_original_call ); Py_INCREF( const_str_plain_original_call );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_original_call_str_plain_async_call_tuple, 2, const_str_plain_async_call ); Py_INCREF( const_str_plain_async_call );
    const_tuple_dd8870a0791446f1214ebc44ee539f3a_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_dd8870a0791446f1214ebc44ee539f3a_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_dd8870a0791446f1214ebc44ee539f3a_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_dd8870a0791446f1214ebc44ee539f3a_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_dd8870a0791446f1214ebc44ee539f3a_tuple, 3, const_str_plain_loop ); Py_INCREF( const_str_plain_loop );
    PyTuple_SET_ITEM( const_tuple_dd8870a0791446f1214ebc44ee539f3a_tuple, 4, const_str_plain_original_render ); Py_INCREF( const_str_plain_original_render );
    const_tuple_eaa6c2fea91dd7ebca5336178766e7bb_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_eaa6c2fea91dd7ebca5336178766e7bb_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_eaa6c2fea91dd7ebca5336178766e7bb_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_eaa6c2fea91dd7ebca5336178766e7bb_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_eaa6c2fea91dd7ebca5336178766e7bb_tuple, 3, const_str_plain_vars ); Py_INCREF( const_str_plain_vars );
    PyTuple_SET_ITEM( const_tuple_eaa6c2fea91dd7ebca5336178766e7bb_tuple, 4, const_str_plain_ctx ); Py_INCREF( const_str_plain_ctx );
    PyTuple_SET_ITEM( const_tuple_eaa6c2fea91dd7ebca5336178766e7bb_tuple, 5, const_str_plain_exc_info ); Py_INCREF( const_str_plain_exc_info );
    const_tuple_str_plain_FILTERS_str_plain_ASYNC_FILTERS_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_FILTERS_str_plain_ASYNC_FILTERS_tuple, 0, const_str_plain_FILTERS ); Py_INCREF( const_str_plain_FILTERS );
    PyTuple_SET_ITEM( const_tuple_str_plain_FILTERS_str_plain_ASYNC_FILTERS_tuple, 1, const_str_plain_ASYNC_FILTERS ); Py_INCREF( const_str_plain_ASYNC_FILTERS );
    const_str_plain_auto_await = UNSTREAM_STRING_ASCII( &constant_bin[ 1031789 ], 10, 1 );
    const_str_plain__get_default_module_async = UNSTREAM_STRING_ASCII( &constant_bin[ 1031799 ], 25, 1 );
    const_str_digest_33a6af52fee903f72e0e65a535276972 = UNSTREAM_STRING_ASCII( &constant_bin[ 1031824 ], 28, 0 );
    const_tuple_str_plain_concat_str_plain_internalcode_str_plain_Markup_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_concat_str_plain_internalcode_str_plain_Markup_tuple, 0, const_str_plain_concat ); Py_INCREF( const_str_plain_concat );
    PyTuple_SET_ITEM( const_tuple_str_plain_concat_str_plain_internalcode_str_plain_Markup_tuple, 1, const_str_plain_internalcode ); Py_INCREF( const_str_plain_internalcode );
    PyTuple_SET_ITEM( const_tuple_str_plain_concat_str_plain_internalcode_str_plain_Markup_tuple, 2, const_str_plain_Markup ); Py_INCREF( const_str_plain_Markup );
    const_str_digest_a1e2dbb21a29bf138fd240dc65f4d45d = UNSTREAM_STRING_ASCII( &constant_bin[ 1031852 ], 33, 0 );
    const_tuple_str_plain_BlockReference_str_plain_Macro_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_BlockReference_str_plain_Macro_tuple, 0, const_str_plain_BlockReference ); Py_INCREF( const_str_plain_BlockReference );
    PyTuple_SET_ITEM( const_tuple_str_plain_BlockReference_str_plain_Macro_tuple, 1, const_str_plain_Macro ); Py_INCREF( const_str_plain_Macro );
    const_str_digest_3fa472849f7563cdf43fdd81ca51dc6e = UNSTREAM_STRING_ASCII( &constant_bin[ 1031885 ], 29, 0 );
    const_str_digest_bea5503dcad2fbda54f7f5ef23d14ba2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1031914 ], 32, 0 );
    const_tuple_2737c610e5ed955ada0ea32e93b5dc10_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_2737c610e5ed955ada0ea32e93b5dc10_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_2737c610e5ed955ada0ea32e93b5dc10_tuple, 1, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    PyTuple_SET_ITEM( const_tuple_2737c610e5ed955ada0ea32e93b5dc10_tuple, 2, const_str_plain_autoescape ); Py_INCREF( const_str_plain_autoescape );
    PyTuple_SET_ITEM( const_tuple_2737c610e5ed955ada0ea32e93b5dc10_tuple, 3, const_str_plain_original_invoke ); Py_INCREF( const_str_plain_original_invoke );
    PyTuple_SET_ITEM( const_tuple_2737c610e5ed955ada0ea32e93b5dc10_tuple, 4, const_str_plain_async_invoke ); Py_INCREF( const_str_plain_async_invoke );
    const_str_digest_53ced86d8a41710c1e4df15bc1e68545 = UNSTREAM_STRING_ASCII( &constant_bin[ 1031946 ], 36, 0 );
    const_str_digest_361aa6e29b7b6ee9ca4424254bc2ed83 = UNSTREAM_STRING_ASCII( &constant_bin[ 1031982 ], 72, 0 );
    const_tuple_str_plain_ASYNC_FILTERS_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ASYNC_FILTERS_tuple, 0, const_str_plain_ASYNC_FILTERS ); Py_INCREF( const_str_plain_ASYNC_FILTERS );
    const_str_plain_concat_async = UNSTREAM_STRING_ASCII( &constant_bin[ 1031885 ], 12, 1 );
    const_str_digest_49a9c2b6c4e1e6e1700f3e3846a4ff46 = UNSTREAM_STRING_ASCII( &constant_bin[ 1032054 ], 43, 0 );
    const_str_digest_0092c3cda9e74bac591e5be98e5ba5f3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1032097 ], 22, 0 );
    const_tuple_str_plain_iterable_str_plain_item_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_iterable_str_plain_item_tuple, 0, const_str_plain_iterable ); Py_INCREF( const_str_plain_iterable );
    PyTuple_SET_ITEM( const_tuple_str_plain_iterable_str_plain_item_tuple, 1, const_str_plain_item ); Py_INCREF( const_str_plain_item );
    const_str_digest_fb845e53b3d15f89e589e1d47fc1ead5 = UNSTREAM_STRING_ASCII( &constant_bin[ 1032119 ], 54, 0 );
    const_tuple_3a916ae68ea77a1da8b015681b8c7b7b_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_3a916ae68ea77a1da8b015681b8c7b7b_tuple, 0, const_str_plain_original_generate ); Py_INCREF( const_str_plain_original_generate );
    PyTuple_SET_ITEM( const_tuple_3a916ae68ea77a1da8b015681b8c7b7b_tuple, 1, const_str_plain__convert_generator ); Py_INCREF( const_str_plain__convert_generator );
    PyTuple_SET_ITEM( const_tuple_3a916ae68ea77a1da8b015681b8c7b7b_tuple, 2, const_str_plain_generate ); Py_INCREF( const_str_plain_generate );
    const_tuple_str_plain_LoopContextBase_str_plain__last_iteration_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_LoopContextBase_str_plain__last_iteration_tuple, 0, const_str_plain_LoopContextBase ); Py_INCREF( const_str_plain_LoopContextBase );
    PyTuple_SET_ITEM( const_tuple_str_plain_LoopContextBase_str_plain__last_iteration_tuple, 1, const_str_plain__last_iteration ); Py_INCREF( const_str_plain__last_iteration );
    const_str_plain_AsyncLoopContext = UNSTREAM_STRING_ASCII( &constant_bin[ 1031475 ], 16, 1 );
    const_str_plain_wrap_macro_invoke = UNSTREAM_STRING_ASCII( &constant_bin[ 1031021 ], 17, 1 );
    const_str_digest_56383ec153fbb011ff77a421b05c140c = UNSTREAM_STRING_ASCII( &constant_bin[ 1032173 ], 26, 0 );
    const_str_digest_3726530226521a6996a67c61864a81b0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1032199 ], 34, 0 );
    const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple, 1, const_str_plain_vars ); Py_INCREF( const_str_plain_vars );
    PyTuple_SET_ITEM( const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple, 2, const_str_plain_shared ); Py_INCREF( const_str_plain_shared );
    PyTuple_SET_ITEM( const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple, 3, const_str_plain_locals ); Py_INCREF( const_str_plain_locals );
    PyTuple_SET_ITEM( const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple, 4, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple, 5, const_str_plain_body_stream ); Py_INCREF( const_str_plain_body_stream );
    PyTuple_SET_ITEM( const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple, 6, const_str_plain_item ); Py_INCREF( const_str_plain_item );
    const_str_digest_1558c9fe5a9ec837ccc69754c23d4d52 = UNSTREAM_STRING_ASCII( &constant_bin[ 1032233 ], 48, 0 );
    const_str_digest_3ea09c231a2793f33456c0650667083b = UNSTREAM_STRING_ASCII( &constant_bin[ 1032281 ], 45, 0 );
    const_str_plain_patch_template = UNSTREAM_STRING_ASCII( &constant_bin[ 1032326 ], 14, 1 );
    const_str_plain__async_iterator = UNSTREAM_STRING_ASCII( &constant_bin[ 1032340 ], 15, 1 );
    const_str_plain_patch_filters = UNSTREAM_STRING_ASCII( &constant_bin[ 1032355 ], 13, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jinja2$asyncsupport( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_a8d131fd71ee5a659a302f38cea47f36;
static PyCodeObject *codeobj_5df0c0789b1207f0f1ac5698b782d124;
static PyCodeObject *codeobj_681557c3bab872442dbaf044cf952344;
static PyCodeObject *codeobj_9665664a0d99957e317710449ffbcb27;
static PyCodeObject *codeobj_a1764b5ae8e154059565156befa51c32;
static PyCodeObject *codeobj_1122cdfeeadebd59dd5fd2047c35efaf;
static PyCodeObject *codeobj_ea897d3dddf300ea230b292301e5630c;
static PyCodeObject *codeobj_be823818de5369c844e2df5ef6e22965;
static PyCodeObject *codeobj_be7319c0e8e97b9ff48abee03cc84f34;
static PyCodeObject *codeobj_9a2a22d4803e75d9e1e3b0d5139bd12f;
static PyCodeObject *codeobj_5ae50fc21a721b02bd5d43f51df2ad7f;
static PyCodeObject *codeobj_f75bb9b4aa760f68208e3028ea5e10e5;
static PyCodeObject *codeobj_07e4033d9f21261c918d73ab2d6aa8a8;
static PyCodeObject *codeobj_58492195bad6f50ecacb5696bc24058d;
static PyCodeObject *codeobj_a5565103ad3eef4b0544670c66e722f4;
static PyCodeObject *codeobj_23294641a30c5a7bda6b5f6f2ebb7fc3;
static PyCodeObject *codeobj_5afbf7df3f782aebd2b7ade122e30df6;
static PyCodeObject *codeobj_8c0c03b7cd55adf56b63556d65b5edca;
static PyCodeObject *codeobj_1b9ac494d333528d8159b0459ddf1c9d;
static PyCodeObject *codeobj_023fbd84bceab111ad9a9f207a1ba2dd;
static PyCodeObject *codeobj_e3e90d4cdae6538e1286fa2ddcca2c7d;
static PyCodeObject *codeobj_8aae607047f59b13ee8f15a1f9a3b603;
static PyCodeObject *codeobj_140f9e161b20f953b6d1f3bed12c0f47;
static PyCodeObject *codeobj_8bb0f2bbea2cda8feebec760dde8a8b2;
static PyCodeObject *codeobj_e1cd052a6439bdb80665802b33a17a24;
static PyCodeObject *codeobj_e420348637aa31a4160898b1c19f16b3;
static PyCodeObject *codeobj_1648508166d5a9568d9532ae759f5032;
static PyCodeObject *codeobj_4b55f3a546fa26d3e73b5679f1b183c7;
static PyCodeObject *codeobj_9b377e527174a28f7b8047af594a6948;
static PyCodeObject *codeobj_0a31ff8c915747d8083b899f002c6e19;
static PyCodeObject *codeobj_64d9569afd0b28fbd639d1f4b39ba5f0;
static PyCodeObject *codeobj_18a1cc25b18ba20a6501b40b66a3172d;
static PyCodeObject *codeobj_9e4b8e20628473adeb29bdd286ae35e5;
static PyCodeObject *codeobj_bf1c1ebb777926dd7a299ddd72c77a34;
static PyCodeObject *codeobj_58940d299f4d03c2a228c95099823d78;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_0092c3cda9e74bac591e5be98e5ba5f3 );
    codeobj_a8d131fd71ee5a659a302f38cea47f36 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_33a6af52fee903f72e0e65a535276972, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_5df0c0789b1207f0f1ac5698b782d124 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AsyncLoopContext, 190, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_681557c3bab872442dbaf044cf952344 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AsyncLoopContextIterator, 210, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_9665664a0d99957e317710449ffbcb27 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___aiter__, 206, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a1764b5ae8e154059565156befa51c32 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___aiter__, 216, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1122cdfeeadebd59dd5fd2047c35efaf = MAKE_CODEOBJ( module_filename_obj, const_str_plain___anext__, 219, const_tuple_str_plain_self_str_plain_ctx_tuple, 1, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ea897d3dddf300ea230b292301e5630c = MAKE_CODEOBJ( module_filename_obj, const_str_plain___call__, 90, const_tuple_str_plain_self_str_plain_original_call_str_plain_async_call_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_be823818de5369c844e2df5ef6e22965 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 192, const_tuple_9dfbe33558311972ee59a2f62af1d1fe_tuple, 7, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_be7319c0e8e97b9ff48abee03cc84f34 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 213, const_tuple_str_plain_self_str_plain_context_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9a2a22d4803e75d9e1e3b0d5139bd12f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__convert_generator, 44, const_tuple_8941e6f2368d99ee0e2007463d5ce197_tuple, 4, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5ae50fc21a721b02bd5d43f51df2ad7f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_default_module, 124, const_tuple_str_plain_self_str_plain_original_default_module_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_f75bb9b4aa760f68208e3028ea5e10e5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__invoke, 107, const_tuple_2737c610e5ed955ada0ea32e93b5dc10_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_07e4033d9f21261c918d73ab2d6aa8a8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_async_call, 83, const_tuple_str_plain_self_str_plain_rv_tuple, 1, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_58492195bad6f50ecacb5696bc24058d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_async_invoke, 100, const_tuple_172fed3f8b88a5921847d3a48ef4b7c9_tuple, 3, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a5565103ad3eef4b0544670c66e722f4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_auto_aiter, 181, const_tuple_str_plain_iterable_str_plain_item_tuple, 1, 0, CO_ASYNC_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_23294641a30c5a7bda6b5f6f2ebb7fc3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_auto_await, 175, const_tuple_str_plain_value_tuple, 1, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5afbf7df3f782aebd2b7ade122e30df6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_collect, 24, const_tuple_str_plain_event_str_plain_async_gen_str_plain_rv_tuple, 0, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_8c0c03b7cd55adf56b63556d65b5edca = MAKE_CODEOBJ( module_filename_obj, const_str_plain_concat_async, 22, const_tuple_str_plain_async_gen_str_plain_rv_str_plain_collect_tuple, 1, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1b9ac494d333528d8159b0459ddf1c9d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_generate, 51, const_tuple_730b53faf75284ef0ec40b0e377a3372_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
    codeobj_023fbd84bceab111ad9a9f207a1ba2dd = MAKE_CODEOBJ( module_filename_obj, const_str_plain_generate_async, 31, const_tuple_ccc0332f38bfcbbe463a0112fe57cf2a_tuple, 1, 0, CO_ASYNC_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_e3e90d4cdae6538e1286fa2ddcca2c7d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_default_module_async, 115, const_tuple_str_plain_self_str_plain_rv_tuple, 1, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8aae607047f59b13ee8f15a1f9a3b603 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_length, 199, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_140f9e161b20f953b6d1f3bed12c0f47 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_async_loop_context, 233, const_tuple_9af200a6b8840745008d9911626e1a7c_tuple, 4, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8bb0f2bbea2cda8feebec760dde8a8b2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_module_async, 133, const_tuple_91b423b9f2a89b6935af35326b7d9164_tuple, 4, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e1cd052a6439bdb80665802b33a17a24 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_patch_all, 169, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e420348637aa31a4160898b1c19f16b3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_patch_filters, 163, const_tuple_str_plain_FILTERS_str_plain_ASYNC_FILTERS_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1648508166d5a9568d9532ae759f5032 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_patch_runtime, 156, const_tuple_str_plain_BlockReference_str_plain_Macro_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4b55f3a546fa26d3e73b5679f1b183c7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_patch_template, 141, const_tuple_str_plain_Template_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9b377e527174a28f7b8047af594a6948 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_render, 74, const_tuple_dd8870a0791446f1214ebc44ee539f3a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
    codeobj_0a31ff8c915747d8083b899f002c6e19 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_render_async, 58, const_tuple_eaa6c2fea91dd7ebca5336178766e7bb_tuple, 1, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_64d9569afd0b28fbd639d1f4b39ba5f0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrap_block_reference_call, 82, const_tuple_25751c10c19e659145b2b44e8eaa0212_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_18a1cc25b18ba20a6501b40b66a3172d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrap_default_module, 123, const_tuple_5a8b0942b888893bd13b4eaf4dbb453e_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9e4b8e20628473adeb29bdd286ae35e5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrap_generate_func, 43, const_tuple_3a916ae68ea77a1da8b015681b8c7b7b_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bf1c1ebb777926dd7a299ddd72c77a34 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrap_macro_invoke, 99, const_tuple_4e790e31576b423020c6860adf0e8fd3_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_58940d299f4d03c2a228c95099823d78 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrap_render_func, 73, const_tuple_str_plain_original_render_str_plain_render_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async_maker( void );


static PyObject *jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect_maker( void );


static PyObject *jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async_maker( void );


static PyObject *jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator_maker( void );


static PyObject *jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async_maker( void );


static PyObject *jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call_maker( void );


static PyObject *jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke_maker( void );


static PyObject *jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async_maker( void );


static PyObject *jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async_maker( void );


static PyObject *jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await_maker( void );


static PyObject *jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter_maker( void );


static PyObject *jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext___maker( void );


static PyObject *jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_7_complex_call_helper_pos_star_list_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_4_complex_call_helper_star_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_10_make_module_async( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_11_patch_template(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_12_patch_runtime(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_13_patch_filters(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_14_patch_all(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_15_auto_await(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_16_auto_aiter(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_17___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_18_length(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_19___aiter__(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_1_concat_async(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_20___init__(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_21___aiter__(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_22___anext__(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_23_make_async_loop_context( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_2_generate_async(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_3_wrap_generate_func(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_2_generate(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_4_render_async(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_5_wrap_render_func(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_5_wrap_render_func$$$function_1_render(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_6_wrap_block_reference_call(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_2___call__(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_7_wrap_macro_invoke(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_2__invoke(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_8_get_default_module_async(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_9_wrap_default_module(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_9_wrap_default_module$$$function_1__get_default_module(  );


// The module function definitions.
static PyObject *impl_jinja2$asyncsupport$$$function_1_concat_async( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_async_gen = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_async_gen;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_1_concat_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_async_gen );
    Py_DECREF( par_async_gen );
    par_async_gen = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_async_gen );
    Py_DECREF( par_async_gen );
    par_async_gen = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_1_concat_async );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async_locals {
    struct Nuitka_CellObject *var_rv;
    PyObject *var_collect;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
};

static PyObject *jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async_locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_rv = PyCell_EMPTY();
    coroutine_heap->var_collect = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( PyCell_GET( coroutine_heap->var_rv ) == NULL );
        PyCell_SET( coroutine_heap->var_rv, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = coroutine->m_closure[0];
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] = coroutine_heap->var_rv;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] );


        assert( coroutine_heap->var_collect == NULL );
        coroutine_heap->var_collect = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_8c0c03b7cd55adf56b63556d65b5edca, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_await_result_1;
        coroutine->m_frame->m_frame.f_lineno = 27;
        CHECK_OBJECT( coroutine_heap->var_collect );
        tmp_called_name_1 = coroutine_heap->var_collect;
        coroutine->m_frame->m_frame.f_lineno = 27;
        tmp_expression_name_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 27;
            coroutine_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 27;
            coroutine_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 27;
            coroutine_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        tmp_await_result_1 = yield_return_value;
        if ( tmp_await_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 27;
            coroutine_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_await_result_1 );
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_concat );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_concat );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "concat" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 28;
            coroutine_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( PyCell_GET( coroutine_heap->var_rv ) );
        tmp_args_element_name_1 = PyCell_GET( coroutine_heap->var_rv );
        coroutine->m_frame->m_frame.f_lineno = 28;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 28;
            coroutine_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine_heap->var_rv,
            coroutine_heap->var_collect
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_rv );
    Py_DECREF( coroutine_heap->var_rv );
    coroutine_heap->var_rv = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_collect );
    Py_DECREF( coroutine_heap->var_collect );
    coroutine_heap->var_collect = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_rv );
    Py_DECREF( coroutine_heap->var_rv );
    coroutine_heap->var_rv = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_collect );
    Py_DECREF( coroutine_heap->var_collect );
    coroutine_heap->var_collect = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async_context,
        module_jinja2$asyncsupport,
        const_str_plain_concat_async,
        NULL,
        codeobj_8c0c03b7cd55adf56b63556d65b5edca,
        1,
        sizeof(struct jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = self->m_closure[0];
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = self->m_closure[1];
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );


    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect_locals {
    PyObject *var_event;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect_locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_event = NULL;
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_5afbf7df3f782aebd2b7ade122e30df6, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_value_name_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "async_gen" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 25;
            coroutine_heap->type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_value_name_1 = PyCell_GET( coroutine->m_closure[0] );
        tmp_expression_name_1 = ASYNC_MAKE_ITERATOR( tmp_value_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 25;
            coroutine_heap->type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_value_name_1, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_value_name_1, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 25;
            coroutine_heap->type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = yield_return_value;
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 25;
            coroutine_heap->type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->tmp_for_loop_1__for_iterator == NULL );
        coroutine_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_value_name_2;
        CHECK_OBJECT( coroutine_heap->tmp_for_loop_1__for_iterator );
        tmp_value_name_2 = coroutine_heap->tmp_for_loop_1__for_iterator;
        tmp_expression_name_2 = ASYNC_ITERATOR_NEXT( tmp_value_name_2 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 25;
            coroutine_heap->type_description_1 = "occ";
            goto try_except_handler_3;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_value_name_2, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 2;
        coroutine->m_yieldfrom = tmp_expression_name_2;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_2:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_value_name_2, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 25;
            coroutine_heap->type_description_1 = "occ";
            goto try_except_handler_3;
        }
        tmp_assign_source_2 = yield_return_value;
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 25;
            coroutine_heap->type_description_1 = "occ";
            goto try_except_handler_3;
        }
        {
            PyObject *old = coroutine_heap->tmp_for_loop_1__iter_value;
            coroutine_heap->tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = coroutine_heap->exception_keeper_type_1;
        tmp_compexpr_right_1 = PyExc_StopAsyncIteration;
        coroutine_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );

            Py_DECREF( coroutine_heap->exception_keeper_type_1 );
            Py_XDECREF( coroutine_heap->exception_keeper_value_1 );
            Py_XDECREF( coroutine_heap->exception_keeper_tb_1 );

            coroutine_heap->exception_lineno = 25;
            coroutine_heap->type_description_1 = "occ";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        Py_DECREF( coroutine_heap->exception_keeper_type_1 );
        Py_XDECREF( coroutine_heap->exception_keeper_value_1 );
        Py_XDECREF( coroutine_heap->exception_keeper_tb_1 );
        goto loop_end_1;
        goto branch_end_1;
        branch_no_1:;
        // Re-raise.
        coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
        coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
        coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
        coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

        goto try_except_handler_2;
        branch_end_1:;
    }
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( coroutine_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = coroutine_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = coroutine_heap->var_event;
            coroutine_heap->var_event = tmp_assign_source_3;
            Py_INCREF( coroutine_heap->var_event );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "rv" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 26;
            coroutine_heap->type_description_1 = "occ";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = PyCell_GET( coroutine->m_closure[1] );
        CHECK_OBJECT( coroutine_heap->var_event );
        tmp_args_element_name_1 = coroutine_heap->var_event;
        coroutine->m_frame->m_frame.f_lineno = 26;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 26;
            coroutine_heap->type_description_1 = "occ";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


        coroutine_heap->exception_lineno = 25;
        coroutine_heap->type_description_1 = "occ";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->tmp_for_loop_1__iter_value );
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( coroutine_heap->tmp_for_loop_1__for_iterator );
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine_heap->var_event,
            coroutine->m_closure[0],
            coroutine->m_closure[1]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( coroutine_heap->tmp_for_loop_1__iter_value );
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( coroutine_heap->tmp_for_loop_1__for_iterator );
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;

    coroutine_heap->tmp_return_value = Py_None;
    Py_INCREF( coroutine_heap->tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( coroutine_heap->var_event );
    coroutine_heap->var_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_3 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_3 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_3 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_3 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_event );
    coroutine_heap->var_event = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_3;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_3;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_3;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect_context,
        module_jinja2$asyncsupport,
        const_str_plain_collect,
        const_str_digest_3fa472849f7563cdf43fdd81ca51dc6e,
        codeobj_5afbf7df3f782aebd2b7ade122e30df6,
        2,
        sizeof(struct jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect$$$coroutine_1_collect_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_2_generate_async( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_args = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async_maker();

    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[0] = par_args;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[1] = par_kwargs;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[2] = par_self;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[2] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_2_generate_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_2_generate_async );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async_locals {
    PyObject *var_vars;
    PyObject *var_event;
    PyObject *var_exc_info;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    nuitka_bool tmp_try_except_1__unhandled_indicator;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    int exception_keeper_lineno_6;
};

static PyObject *jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async_context( struct Nuitka_AsyncgenObject *asyncgen, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)asyncgen );
    assert( Nuitka_Asyncgen_Check( (PyObject *)asyncgen ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async_locals *asyncgen_heap = (struct jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async_locals *)asyncgen->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(asyncgen->m_yield_return_index) {
    case 4: goto yield_return_4;
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    asyncgen_heap->var_vars = NULL;
    asyncgen_heap->var_event = NULL;
    asyncgen_heap->var_exc_info = NULL;
    asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;
    asyncgen_heap->tmp_for_loop_1__iter_value = NULL;
    asyncgen_heap->tmp_try_except_1__unhandled_indicator = NUITKA_BOOL_UNASSIGNED;
    asyncgen_heap->type_description_1 = NULL;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    // Actual asyngen body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_023fbd84bceab111ad9a9f207a1ba2dd, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    asyncgen->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( asyncgen->m_frame );
    assert( Py_REFCNT( asyncgen->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    asyncgen->m_frame->m_frame.f_gen = (PyObject *)asyncgen;
#endif

    Py_CLEAR( asyncgen->m_frame->m_frame.f_back );

    asyncgen->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( asyncgen->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &asyncgen->m_frame->m_frame;
    Py_INCREF( asyncgen->m_frame );

    Nuitka_Frame_MarkAsExecuting( asyncgen->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        asyncgen->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( asyncgen->m_frame->m_frame.f_exc_type == Py_None ) asyncgen->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_type );
    asyncgen->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_value );
    asyncgen->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_traceback );
#else
        asyncgen->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( asyncgen->m_exc_state.exc_type == Py_None ) asyncgen->m_exc_state.exc_type = NULL;
        Py_XINCREF( asyncgen->m_exc_state.exc_type );
        asyncgen->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( asyncgen->m_exc_state.exc_value );
        asyncgen->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( asyncgen->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        tmp_dircall_arg1_1 = (PyObject *)&PyDict_Type;
        if ( PyCell_GET( asyncgen->m_closure[0] ) == NULL )
        {

            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "args" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 32;
            asyncgen_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg2_1 = PyCell_GET( asyncgen->m_closure[0] );
        if ( PyCell_GET( asyncgen->m_closure[1] ) == NULL )
        {

            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 32;
            asyncgen_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg3_1 = PyCell_GET( asyncgen->m_closure[1] );
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_1 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 32;
            asyncgen_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        assert( asyncgen_heap->var_vars == NULL );
        asyncgen_heap->var_vars = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_assign_source_2;
        tmp_assign_source_2 = NUITKA_BOOL_TRUE;
        asyncgen_heap->tmp_try_except_1__unhandled_indicator = tmp_assign_source_2;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_value_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_2;
        if ( PyCell_GET( asyncgen->m_closure[2] ) == NULL )
        {

            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_3;
        }

        tmp_source_name_1 = PyCell_GET( asyncgen->m_closure[2] );
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_root_render_func );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_3;
        }
        if ( PyCell_GET( asyncgen->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_3;
        }

        tmp_called_instance_1 = PyCell_GET( asyncgen->m_closure[2] );
        CHECK_OBJECT( asyncgen_heap->var_vars );
        tmp_args_element_name_2 = asyncgen_heap->var_vars;
        asyncgen->m_frame->m_frame.f_lineno = 34;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_new_context, call_args );
        }

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_3;
        }
        asyncgen->m_frame->m_frame.f_lineno = 34;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_value_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_value_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_3;
        }
        tmp_expression_name_1 = ASYNC_MAKE_ITERATOR( tmp_value_name_1 );
        Py_DECREF( tmp_value_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_3;
        }
        Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_value_name_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), NULL );
        asyncgen->m_yield_return_index = 1;
        asyncgen->m_yieldfrom = tmp_expression_name_1;
        asyncgen->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_value_name_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), NULL );
        asyncgen->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_3 = yield_return_value;
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_3;
        }
        assert( asyncgen_heap->tmp_for_loop_1__for_iterator == NULL );
        asyncgen_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_value_name_2;
        CHECK_OBJECT( asyncgen_heap->tmp_for_loop_1__for_iterator );
        tmp_value_name_2 = asyncgen_heap->tmp_for_loop_1__for_iterator;
        tmp_expression_name_2 = ASYNC_ITERATOR_NEXT( tmp_value_name_2 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_5;
        }
        Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_value_name_2, sizeof(PyObject *), NULL );
        asyncgen->m_yield_return_index = 2;
        asyncgen->m_yieldfrom = tmp_expression_name_2;
        asyncgen->m_awaiting = true;
        return NULL;

        yield_return_2:
        Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_value_name_2, sizeof(PyObject *), NULL );
        asyncgen->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_5;
        }
        tmp_assign_source_4 = yield_return_value;
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = asyncgen_heap->tmp_for_loop_1__iter_value;
            asyncgen_heap->tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_5:;
    asyncgen_heap->exception_keeper_type_1 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_1 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_1 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_1 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = asyncgen_heap->exception_keeper_type_1;
        tmp_compexpr_right_1 = PyExc_StopAsyncIteration;
        asyncgen_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( asyncgen_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );

            Py_DECREF( asyncgen_heap->exception_keeper_type_1 );
            Py_XDECREF( asyncgen_heap->exception_keeper_value_1 );
            Py_XDECREF( asyncgen_heap->exception_keeper_tb_1 );

            asyncgen_heap->exception_lineno = 34;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( asyncgen_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        Py_DECREF( asyncgen_heap->exception_keeper_type_1 );
        Py_XDECREF( asyncgen_heap->exception_keeper_value_1 );
        Py_XDECREF( asyncgen_heap->exception_keeper_tb_1 );
        goto loop_end_1;
        goto branch_end_1;
        branch_no_1:;
        // Re-raise.
        asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_1;
        asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_1;
        asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_1;
        asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_1;

        goto try_except_handler_4;
        branch_end_1:;
    }
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( asyncgen_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = asyncgen_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = asyncgen_heap->var_event;
            asyncgen_heap->var_event = tmp_assign_source_5;
            Py_INCREF( asyncgen_heap->var_event );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_3;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( asyncgen_heap->var_event );
        tmp_expression_name_3 = asyncgen_heap->var_event;
        Py_INCREF( tmp_expression_name_3 );
        asyncgen->m_yield_return_index = 3;
        return tmp_expression_name_3;
        yield_return_3:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 35;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_4;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


        asyncgen_heap->exception_lineno = 34;
        asyncgen_heap->type_description_1 = "cccooo";
        goto try_except_handler_4;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    asyncgen_heap->exception_keeper_type_2 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_2 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_2 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_2 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    Py_XDECREF( asyncgen_heap->tmp_for_loop_1__iter_value );
    asyncgen_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( asyncgen_heap->tmp_for_loop_1__for_iterator );
    asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_2;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_2;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_2;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_2:;
    Py_XDECREF( asyncgen_heap->tmp_for_loop_1__iter_value );
    asyncgen_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( asyncgen_heap->tmp_for_loop_1__for_iterator );
    asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;

    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    asyncgen_heap->exception_keeper_type_3 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_3 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_3 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_3 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    {
        nuitka_bool tmp_assign_source_6;
        tmp_assign_source_6 = NUITKA_BOOL_FALSE;
        asyncgen_heap->tmp_try_except_1__unhandled_indicator = tmp_assign_source_6;
    }
    // Preserve existing published exception.
    asyncgen_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( asyncgen_heap->exception_preserved_type_1 );
    asyncgen_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( asyncgen_heap->exception_preserved_value_1 );
    asyncgen_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( asyncgen_heap->exception_preserved_tb_1 );

    if ( asyncgen_heap->exception_keeper_tb_3 == NULL )
    {
        asyncgen_heap->exception_keeper_tb_3 = MAKE_TRACEBACK( asyncgen->m_frame, asyncgen_heap->exception_keeper_lineno_3 );
    }
    else if ( asyncgen_heap->exception_keeper_lineno_3 != 0 )
    {
        asyncgen_heap->exception_keeper_tb_3 = ADD_TRACEBACK( asyncgen_heap->exception_keeper_tb_3, asyncgen->m_frame, asyncgen_heap->exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &asyncgen_heap->exception_keeper_type_3, &asyncgen_heap->exception_keeper_value_3, &asyncgen_heap->exception_keeper_tb_3 );
    PyException_SetTraceback( asyncgen_heap->exception_keeper_value_3, (PyObject *)asyncgen_heap->exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &asyncgen_heap->exception_keeper_type_3, &asyncgen_heap->exception_keeper_value_3, &asyncgen_heap->exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_Exception;
        asyncgen_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( asyncgen_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 36;
            asyncgen_heap->type_description_1 = "cccooo";
            goto try_except_handler_6;
        }
        tmp_condition_result_2 = ( asyncgen_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                asyncgen_heap->exception_type = PyExc_NameError;
                Py_INCREF( asyncgen_heap->exception_type );
                asyncgen_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                asyncgen_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
                CHAIN_EXCEPTION( asyncgen_heap->exception_value );

                asyncgen_heap->exception_lineno = 37;
                asyncgen_heap->type_description_1 = "cccooo";
                goto try_except_handler_6;
            }

            tmp_called_instance_2 = tmp_mvar_value_1;
            asyncgen->m_frame->m_frame.f_lineno = 37;
            tmp_assign_source_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_exc_info );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 37;
                asyncgen_heap->type_description_1 = "cccooo";
                goto try_except_handler_6;
            }
            assert( asyncgen_heap->var_exc_info == NULL );
            asyncgen_heap->var_exc_info = tmp_assign_source_7;
        }
        goto branch_end_2;
        branch_no_2:;
        asyncgen_heap->tmp_result = RERAISE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
        if (unlikely( asyncgen_heap->tmp_result == false ))
        {
            asyncgen_heap->exception_lineno = 33;
        }

        if (asyncgen_heap->exception_tb && asyncgen_heap->exception_tb->tb_frame == &asyncgen->m_frame->m_frame) asyncgen->m_frame->m_frame.f_lineno = asyncgen_heap->exception_tb->tb_lineno;
        asyncgen_heap->type_description_1 = "cccooo";
        goto try_except_handler_6;
        branch_end_2:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    asyncgen_heap->exception_keeper_type_4 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_4 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_4 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_4 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( asyncgen_heap->exception_preserved_type_1, asyncgen_heap->exception_preserved_value_1, asyncgen_heap->exception_preserved_tb_1 );
    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_4;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_4;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_4;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_4;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( asyncgen_heap->exception_preserved_type_1, asyncgen_heap->exception_preserved_value_1, asyncgen_heap->exception_preserved_tb_1 );
    goto try_end_3;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async );
    return NULL;
    // End of try:
    try_end_3:;
    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( asyncgen_heap->tmp_try_except_1__unhandled_indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = asyncgen_heap->tmp_try_except_1__unhandled_indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        goto try_return_handler_2;
        branch_no_3:;
    }
    goto try_end_5;
    // Return handler code:
    try_return_handler_2:;
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    asyncgen_heap->exception_keeper_type_5 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_5 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_5 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_5 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_5;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_5;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_5;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    {
        PyObject *tmp_expression_name_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_2;
        if ( PyCell_GET( asyncgen->m_closure[2] ) == NULL )
        {

            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 40;
            asyncgen_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = PyCell_GET( asyncgen->m_closure[2] );
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_environment );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 40;
            asyncgen_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_handle_exception );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 40;
            asyncgen_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        if ( asyncgen_heap->var_exc_info == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            asyncgen_heap->exception_type = PyExc_UnboundLocalError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "exc_info" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 40;
            asyncgen_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = asyncgen_heap->var_exc_info;
        tmp_args_element_name_4 = Py_True;
        asyncgen->m_frame->m_frame.f_lineno = 40;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_expression_name_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        if ( tmp_expression_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 40;
            asyncgen_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), NULL );
        asyncgen->m_yield_return_index = 4;
        return tmp_expression_name_4;
        yield_return_4:
        Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 40;
            asyncgen_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        tmp_yield_result_2 = yield_return_value;
    }

    Nuitka_Frame_MarkAsNotExecuting( asyncgen->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( asyncgen->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( asyncgen->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( asyncgen_heap->exception_type ) )
    {
        if ( asyncgen_heap->exception_tb == NULL )
        {
            asyncgen_heap->exception_tb = MAKE_TRACEBACK( asyncgen->m_frame, asyncgen_heap->exception_lineno );
        }
        else if ( asyncgen_heap->exception_tb->tb_frame != &asyncgen->m_frame->m_frame )
        {
            asyncgen_heap->exception_tb = ADD_TRACEBACK( asyncgen_heap->exception_tb, asyncgen->m_frame, asyncgen_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)asyncgen->m_frame,
            asyncgen_heap->type_description_1,
            asyncgen->m_closure[2],
            asyncgen->m_closure[0],
            asyncgen->m_closure[1],
            asyncgen_heap->var_vars,
            asyncgen_heap->var_event,
            asyncgen_heap->var_exc_info
        );


        // Release cached frame.
        if ( asyncgen->m_frame == cache_m_frame )
        {
            Py_DECREF( asyncgen->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( asyncgen->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( asyncgen->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)asyncgen_heap->var_vars );
    Py_DECREF( asyncgen_heap->var_vars );
    asyncgen_heap->var_vars = NULL;

    Py_XDECREF( asyncgen_heap->var_event );
    asyncgen_heap->var_event = NULL;

    Py_XDECREF( asyncgen_heap->var_exc_info );
    asyncgen_heap->var_exc_info = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    asyncgen_heap->exception_keeper_type_6 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_6 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_6 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_6 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    Py_XDECREF( asyncgen_heap->var_vars );
    asyncgen_heap->var_vars = NULL;

    Py_XDECREF( asyncgen_heap->var_event );
    asyncgen_heap->var_event = NULL;

    Py_XDECREF( asyncgen_heap->var_exc_info );
    asyncgen_heap->var_exc_info = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_6;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_6;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_6;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async );

    function_exception_exit:
    assert( asyncgen_heap->exception_type );
    RESTORE_ERROR_OCCURRED( asyncgen_heap->exception_type, asyncgen_heap->exception_value, asyncgen_heap->exception_tb );
    return NULL;
    function_return_exit:;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async_maker( void )
{
    return Nuitka_Asyncgen_New(
        jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async_context,
        module_jinja2$asyncsupport,
        const_str_plain_generate_async,
        NULL,
        codeobj_023fbd84bceab111ad9a9f207a1ba2dd,
        3,
        sizeof(struct jinja2$asyncsupport$$$function_2_generate_async$$$asyncgen_1_generate_async_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_3_wrap_generate_func( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_original_generate = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *var__convert_generator = PyCell_EMPTY();
    PyObject *var_generate = NULL;
    struct Nuitka_FrameObject *frame_9e4b8e20628473adeb29bdd286ae35e5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9e4b8e20628473adeb29bdd286ae35e5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator(  );



        assert( PyCell_GET( var__convert_generator ) == NULL );
        PyCell_SET( var__convert_generator, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_2_generate(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = var__convert_generator;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] = par_original_generate;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] );


        assert( var_generate == NULL );
        var_generate = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9e4b8e20628473adeb29bdd286ae35e5, codeobj_9e4b8e20628473adeb29bdd286ae35e5, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9e4b8e20628473adeb29bdd286ae35e5 = cache_frame_9e4b8e20628473adeb29bdd286ae35e5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9e4b8e20628473adeb29bdd286ae35e5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9e4b8e20628473adeb29bdd286ae35e5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_update_wrapper );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_update_wrapper );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "update_wrapper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_generate );
        tmp_args_element_name_1 = var_generate;
        CHECK_OBJECT( PyCell_GET( par_original_generate ) );
        tmp_args_element_name_2 = PyCell_GET( par_original_generate );
        frame_9e4b8e20628473adeb29bdd286ae35e5->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9e4b8e20628473adeb29bdd286ae35e5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9e4b8e20628473adeb29bdd286ae35e5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9e4b8e20628473adeb29bdd286ae35e5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9e4b8e20628473adeb29bdd286ae35e5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9e4b8e20628473adeb29bdd286ae35e5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9e4b8e20628473adeb29bdd286ae35e5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9e4b8e20628473adeb29bdd286ae35e5,
        type_description_1,
        par_original_generate,
        var__convert_generator,
        var_generate
    );


    // Release cached frame.
    if ( frame_9e4b8e20628473adeb29bdd286ae35e5 == cache_frame_9e4b8e20628473adeb29bdd286ae35e5 )
    {
        Py_DECREF( frame_9e4b8e20628473adeb29bdd286ae35e5 );
    }
    cache_frame_9e4b8e20628473adeb29bdd286ae35e5 = NULL;

    assertFrameObject( frame_9e4b8e20628473adeb29bdd286ae35e5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_3_wrap_generate_func );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_original_generate );
    Py_DECREF( par_original_generate );
    par_original_generate = NULL;

    CHECK_OBJECT( (PyObject *)var__convert_generator );
    Py_DECREF( var__convert_generator );
    var__convert_generator = NULL;

    CHECK_OBJECT( (PyObject *)var_generate );
    Py_DECREF( var_generate );
    var_generate = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_original_generate );
    Py_DECREF( par_original_generate );
    par_original_generate = NULL;

    CHECK_OBJECT( (PyObject *)var__convert_generator );
    Py_DECREF( var__convert_generator );
    var__convert_generator = NULL;

    CHECK_OBJECT( (PyObject *)var_generate );
    Py_DECREF( var_generate );
    var_generate = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_3_wrap_generate_func );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_loop = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_args = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_args;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = par_kwargs;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[2] = par_loop;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[3] = par_self;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[3] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_loop );
    Py_DECREF( par_loop );
    par_loop = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_loop );
    Py_DECREF( par_loop );
    par_loop = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator_locals {
    PyObject *var_async_gen;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator_locals *generator_heap = (struct jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_async_gen = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_9a2a22d4803e75d9e1e3b0d5139bd12f, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        if ( PyCell_GET( generator->m_closure[3] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 45;
            generator_heap->type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( generator->m_closure[3] );
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_generate_async );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 45;
            generator_heap->type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_dircall_arg1_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "args" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 45;
            generator_heap->type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg2_1 = PyCell_GET( generator->m_closure[0] );
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_dircall_arg1_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 45;
            generator_heap->type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg3_1 = PyCell_GET( generator->m_closure[1] );
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_1 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 45;
            generator_heap->type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->var_async_gen == NULL );
        generator_heap->var_async_gen = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        if ( PyCell_GET( generator->m_closure[2] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "loop" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 48;
            generator_heap->type_description_1 = "cccco";
            goto try_except_handler_2;
        }

        tmp_source_name_2 = PyCell_GET( generator->m_closure[2] );
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_run_until_complete );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 48;
            generator_heap->type_description_1 = "cccco";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( generator_heap->var_async_gen );
        tmp_called_instance_1 = generator_heap->var_async_gen;
        generator->m_frame->m_frame.f_lineno = 48;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain___anext__ );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            generator_heap->exception_lineno = 48;
            generator_heap->type_description_1 = "cccco";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_expression_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 48;
            generator_heap->type_description_1 = "cccco";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 48;
            generator_heap->type_description_1 = "cccco";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 47;
        generator_heap->type_description_1 = "cccco";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Preserve existing published exception.
    generator_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_type_1 );
    generator_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_value_1 );
    generator_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_tb_1 );

    if ( generator_heap->exception_keeper_tb_1 == NULL )
    {
        generator_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }
    else if ( generator_heap->exception_keeper_lineno_1 != 0 )
    {
        generator_heap->exception_keeper_tb_1 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_1, generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    PyException_SetTraceback( generator_heap->exception_keeper_value_1, (PyObject *)generator_heap->exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_StopAsyncIteration;
        generator_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 49;
            generator_heap->type_description_1 = "cccco";
            goto try_except_handler_3;
        }
        tmp_operand_name_1 = ( generator_heap->tmp_res != 0 ) ? Py_True : Py_False;
        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 49;
            generator_heap->type_description_1 = "cccco";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
        if (unlikely( generator_heap->tmp_result == false ))
        {
            generator_heap->exception_lineno = 46;
        }

        if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
        generator_heap->type_description_1 = "cccco";
        goto try_except_handler_3;
        branch_no_1:;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator );
    return NULL;
    // End of try:
    try_end_2:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[3],
            generator->m_closure[2],
            generator->m_closure[0],
            generator->m_closure[1],
            generator_heap->var_async_gen
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_async_gen );
    generator_heap->var_async_gen = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)generator_heap->var_async_gen );
    Py_DECREF( generator_heap->var_async_gen );
    generator_heap->var_async_gen = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator_maker( void )
{
    return Nuitka_Generator_New(
        jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator_context,
        module_jinja2$asyncsupport,
        const_str_plain__convert_generator,
#if PYTHON_VERSION >= 350
        const_str_digest_b2d89451c7e47bfda7c99b2c93d722a7,
#endif
        codeobj_9a2a22d4803e75d9e1e3b0d5139bd12f,
        4,
        sizeof(struct jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator$$$genobj_1__convert_generator_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_2_generate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_1b9ac494d333528d8159b0459ddf1c9d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_1b9ac494d333528d8159b0459ddf1c9d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1b9ac494d333528d8159b0459ddf1c9d, codeobj_1b9ac494d333528d8159b0459ddf1c9d, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1b9ac494d333528d8159b0459ddf1c9d = cache_frame_1b9ac494d333528d8159b0459ddf1c9d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1b9ac494d333528d8159b0459ddf1c9d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1b9ac494d333528d8159b0459ddf1c9d ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_environment );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_async );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_dircall_arg1_1;
            PyObject *tmp_dircall_arg2_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_dircall_arg3_1;
            PyObject *tmp_dircall_arg4_1;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "original_generate" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 53;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }

            tmp_dircall_arg1_1 = PyCell_GET( self->m_closure[1] );
            CHECK_OBJECT( par_self );
            tmp_tuple_element_1 = par_self;
            tmp_dircall_arg2_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( par_args );
            tmp_dircall_arg3_1 = par_args;
            CHECK_OBJECT( par_kwargs );
            tmp_dircall_arg4_1 = par_kwargs;
            Py_INCREF( tmp_dircall_arg1_1 );
            Py_INCREF( tmp_dircall_arg3_1 );
            Py_INCREF( tmp_dircall_arg4_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1, tmp_dircall_arg4_1};
                tmp_return_value = impl___internal__$$$function_7_complex_call_helper_pos_star_list_star_dict( dir_call_args );
            }
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "_convert_generator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_asyncio );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncio );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncio" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_1b9ac494d333528d8159b0459ddf1c9d->m_frame.f_lineno = 54;
        tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_event_loop );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_args );
        tmp_args_element_name_3 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_args_element_name_4 = par_kwargs;
        frame_1b9ac494d333528d8159b0459ddf1c9d->m_frame.f_lineno = 54;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b9ac494d333528d8159b0459ddf1c9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b9ac494d333528d8159b0459ddf1c9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b9ac494d333528d8159b0459ddf1c9d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1b9ac494d333528d8159b0459ddf1c9d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1b9ac494d333528d8159b0459ddf1c9d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1b9ac494d333528d8159b0459ddf1c9d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1b9ac494d333528d8159b0459ddf1c9d,
        type_description_1,
        par_self,
        par_args,
        par_kwargs,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_1b9ac494d333528d8159b0459ddf1c9d == cache_frame_1b9ac494d333528d8159b0459ddf1c9d )
    {
        Py_DECREF( frame_1b9ac494d333528d8159b0459ddf1c9d );
    }
    cache_frame_1b9ac494d333528d8159b0459ddf1c9d = NULL;

    assertFrameObject( frame_1b9ac494d333528d8159b0459ddf1c9d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_2_generate );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_2_generate );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_4_render_async( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_args = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_args;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_kwargs;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] = par_self;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_4_render_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_4_render_async );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async_locals {
    PyObject *var_vars;
    PyObject *var_ctx;
    PyObject *var_exc_info;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    PyObject *tmp_return_value;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async_locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_vars = NULL;
    coroutine_heap->var_ctx = NULL;
    coroutine_heap->var_exc_info = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_0a31ff8c915747d8083b899f002c6e19, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 59;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = PyCell_GET( coroutine->m_closure[2] );
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_environment );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 59;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_async );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 59;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        coroutine_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 59;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( coroutine_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_9eec2e33de8cedad6097d6f88595b09c;
            coroutine->m_frame->m_frame.f_lineno = 60;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            coroutine_heap->exception_type = tmp_raise_type_1;
            coroutine_heap->exception_lineno = 60;
            RAISE_EXCEPTION_WITH_TYPE( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        tmp_dircall_arg1_1 = (PyObject *)&PyDict_Type;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "args" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 63;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg2_1 = PyCell_GET( coroutine->m_closure[0] );
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 63;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg3_1 = PyCell_GET( coroutine->m_closure[1] );
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_1 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 63;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->var_vars == NULL );
        coroutine_heap->var_vars = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 64;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( coroutine->m_closure[2] );
        CHECK_OBJECT( coroutine_heap->var_vars );
        tmp_args_element_name_1 = coroutine_heap->var_vars;
        coroutine->m_frame->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_new_context, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 64;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->var_ctx == NULL );
        coroutine_heap->var_ctx = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_3;
        coroutine->m_frame->m_frame.f_lineno = 67;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_concat_async );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_concat_async );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "concat_async" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 67;
            coroutine_heap->type_description_1 = "cccooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 67;
            coroutine_heap->type_description_1 = "cccooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_2 = PyCell_GET( coroutine->m_closure[2] );
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_args_element_name_3 = coroutine_heap->var_ctx;
        coroutine->m_frame->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_root_render_func, call_args );
        }

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 67;
            coroutine_heap->type_description_1 = "cccooo";
            goto try_except_handler_2;
        }
        coroutine->m_frame->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_expression_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 67;
            coroutine_heap->type_description_1 = "cccooo";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 67;
            coroutine_heap->type_description_1 = "cccooo";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 67;
            coroutine_heap->type_description_1 = "cccooo";
            goto try_except_handler_2;
        }
        coroutine_heap->tmp_return_value = yield_return_value;
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 67;
            coroutine_heap->type_description_1 = "cccooo";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Preserve existing published exception.
    coroutine_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_type_1 );
    coroutine_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_value_1 );
    coroutine_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_tb_1 );

    if ( coroutine_heap->exception_keeper_tb_1 == NULL )
    {
        coroutine_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_keeper_lineno_1 );
    }
    else if ( coroutine_heap->exception_keeper_lineno_1 != 0 )
    {
        coroutine_heap->exception_keeper_tb_1 = ADD_TRACEBACK( coroutine_heap->exception_keeper_tb_1, coroutine->m_frame, coroutine_heap->exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &coroutine_heap->exception_keeper_type_1, &coroutine_heap->exception_keeper_value_1, &coroutine_heap->exception_keeper_tb_1 );
    PyException_SetTraceback( coroutine_heap->exception_keeper_value_1, (PyObject *)coroutine_heap->exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &coroutine_heap->exception_keeper_type_1, &coroutine_heap->exception_keeper_value_1, &coroutine_heap->exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_Exception;
        coroutine_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 68;
            coroutine_heap->type_description_1 = "cccooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_2 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 69;
                coroutine_heap->type_description_1 = "cccooo";
                goto try_except_handler_3;
            }

            tmp_called_instance_3 = tmp_mvar_value_2;
            coroutine->m_frame->m_frame.f_lineno = 69;
            tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_exc_info );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 69;
                coroutine_heap->type_description_1 = "cccooo";
                goto try_except_handler_3;
            }
            assert( coroutine_heap->var_exc_info == NULL );
            coroutine_heap->var_exc_info = tmp_assign_source_3;
        }
        goto branch_end_2;
        branch_no_2:;
        coroutine_heap->tmp_result = RERAISE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
        if (unlikely( coroutine_heap->tmp_result == false ))
        {
            coroutine_heap->exception_lineno = 66;
        }

        if (coroutine_heap->exception_tb && coroutine_heap->exception_tb->tb_frame == &coroutine->m_frame->m_frame) coroutine->m_frame->m_frame.f_lineno = coroutine_heap->exception_tb->tb_lineno;
        coroutine_heap->type_description_1 = "cccooo";
        goto try_except_handler_3;
        branch_end_2:;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_1, coroutine_heap->exception_preserved_value_1, coroutine_heap->exception_preserved_tb_1 );
    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_1, coroutine_heap->exception_preserved_value_1, coroutine_heap->exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async );
    return NULL;
    // End of try:
    try_end_2:;
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 70;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = PyCell_GET( coroutine->m_closure[2] );
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_environment );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 70;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_handle_exception );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 70;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        if ( coroutine_heap->var_exc_info == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            coroutine_heap->exception_type = PyExc_UnboundLocalError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "exc_info" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 70;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_4 = coroutine_heap->var_exc_info;
        tmp_args_element_name_5 = Py_True;
        coroutine->m_frame->m_frame.f_lineno = 70;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 70;
            coroutine_heap->type_description_1 = "cccooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[2],
            coroutine->m_closure[0],
            coroutine->m_closure[1],
            coroutine_heap->var_vars,
            coroutine_heap->var_ctx,
            coroutine_heap->var_exc_info
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_vars );
    Py_DECREF( coroutine_heap->var_vars );
    coroutine_heap->var_vars = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_ctx );
    Py_DECREF( coroutine_heap->var_ctx );
    coroutine_heap->var_ctx = NULL;

    Py_XDECREF( coroutine_heap->var_exc_info );
    coroutine_heap->var_exc_info = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_3 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_3 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_3 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_3 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_vars );
    coroutine_heap->var_vars = NULL;

    Py_XDECREF( coroutine_heap->var_ctx );
    coroutine_heap->var_ctx = NULL;

    Py_XDECREF( coroutine_heap->var_exc_info );
    coroutine_heap->var_exc_info = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_3;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_3;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_3;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async_context,
        module_jinja2$asyncsupport,
        const_str_plain_render_async,
        NULL,
        codeobj_0a31ff8c915747d8083b899f002c6e19,
        3,
        sizeof(struct jinja2$asyncsupport$$$function_4_render_async$$$coroutine_1_render_async_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_5_wrap_render_func( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_original_render = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_render = NULL;
    struct Nuitka_FrameObject *frame_58940d299f4d03c2a228c95099823d78;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_58940d299f4d03c2a228c95099823d78 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_5_wrap_render_func$$$function_1_render(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_original_render;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_render == NULL );
        var_render = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_58940d299f4d03c2a228c95099823d78, codeobj_58940d299f4d03c2a228c95099823d78, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    frame_58940d299f4d03c2a228c95099823d78 = cache_frame_58940d299f4d03c2a228c95099823d78;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_58940d299f4d03c2a228c95099823d78 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_58940d299f4d03c2a228c95099823d78 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_update_wrapper );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_update_wrapper );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "update_wrapper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_render );
        tmp_args_element_name_1 = var_render;
        CHECK_OBJECT( PyCell_GET( par_original_render ) );
        tmp_args_element_name_2 = PyCell_GET( par_original_render );
        frame_58940d299f4d03c2a228c95099823d78->m_frame.f_lineno = 79;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_58940d299f4d03c2a228c95099823d78 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_58940d299f4d03c2a228c95099823d78 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_58940d299f4d03c2a228c95099823d78 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_58940d299f4d03c2a228c95099823d78, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_58940d299f4d03c2a228c95099823d78->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_58940d299f4d03c2a228c95099823d78, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_58940d299f4d03c2a228c95099823d78,
        type_description_1,
        par_original_render,
        var_render
    );


    // Release cached frame.
    if ( frame_58940d299f4d03c2a228c95099823d78 == cache_frame_58940d299f4d03c2a228c95099823d78 )
    {
        Py_DECREF( frame_58940d299f4d03c2a228c95099823d78 );
    }
    cache_frame_58940d299f4d03c2a228c95099823d78 = NULL;

    assertFrameObject( frame_58940d299f4d03c2a228c95099823d78 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_5_wrap_render_func );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_original_render );
    Py_DECREF( par_original_render );
    par_original_render = NULL;

    CHECK_OBJECT( (PyObject *)var_render );
    Py_DECREF( var_render );
    var_render = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_original_render );
    Py_DECREF( par_original_render );
    par_original_render = NULL;

    CHECK_OBJECT( (PyObject *)var_render );
    Py_DECREF( var_render );
    var_render = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_5_wrap_render_func );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_5_wrap_render_func$$$function_1_render( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_loop = NULL;
    struct Nuitka_FrameObject *frame_9b377e527174a28f7b8047af594a6948;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_9b377e527174a28f7b8047af594a6948 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9b377e527174a28f7b8047af594a6948, codeobj_9b377e527174a28f7b8047af594a6948, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9b377e527174a28f7b8047af594a6948 = cache_frame_9b377e527174a28f7b8047af594a6948;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9b377e527174a28f7b8047af594a6948 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9b377e527174a28f7b8047af594a6948 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_environment );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_async );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_dircall_arg1_1;
            PyObject *tmp_dircall_arg2_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_dircall_arg3_1;
            PyObject *tmp_dircall_arg4_1;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "original_render" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 76;
                type_description_1 = "ooooc";
                goto frame_exception_exit_1;
            }

            tmp_dircall_arg1_1 = PyCell_GET( self->m_closure[0] );
            CHECK_OBJECT( par_self );
            tmp_tuple_element_1 = par_self;
            tmp_dircall_arg2_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( par_args );
            tmp_dircall_arg3_1 = par_args;
            CHECK_OBJECT( par_kwargs );
            tmp_dircall_arg4_1 = par_kwargs;
            Py_INCREF( tmp_dircall_arg1_1 );
            Py_INCREF( tmp_dircall_arg3_1 );
            Py_INCREF( tmp_dircall_arg4_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1, tmp_dircall_arg4_1};
                tmp_return_value = impl___internal__$$$function_7_complex_call_helper_pos_star_list_star_dict( dir_call_args );
            }
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_1 = "ooooc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_asyncio );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncio );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncio" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_9b377e527174a28f7b8047af594a6948->m_frame.f_lineno = 77;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_event_loop );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        assert( var_loop == NULL );
        var_loop = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_dircall_arg1_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_dircall_arg2_2;
        PyObject *tmp_dircall_arg3_2;
        CHECK_OBJECT( var_loop );
        tmp_source_name_3 = var_loop;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_run_until_complete );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_dircall_arg1_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_render_async );
        if ( tmp_dircall_arg1_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 78;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_2 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_2 = par_kwargs;
        Py_INCREF( tmp_dircall_arg2_2 );
        Py_INCREF( tmp_dircall_arg3_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2, tmp_dircall_arg3_2};
            tmp_args_element_name_1 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 78;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        frame_9b377e527174a28f7b8047af594a6948->m_frame.f_lineno = 78;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b377e527174a28f7b8047af594a6948 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b377e527174a28f7b8047af594a6948 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b377e527174a28f7b8047af594a6948 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9b377e527174a28f7b8047af594a6948, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9b377e527174a28f7b8047af594a6948->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9b377e527174a28f7b8047af594a6948, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9b377e527174a28f7b8047af594a6948,
        type_description_1,
        par_self,
        par_args,
        par_kwargs,
        var_loop,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_9b377e527174a28f7b8047af594a6948 == cache_frame_9b377e527174a28f7b8047af594a6948 )
    {
        Py_DECREF( frame_9b377e527174a28f7b8047af594a6948 );
    }
    cache_frame_9b377e527174a28f7b8047af594a6948 = NULL;

    assertFrameObject( frame_9b377e527174a28f7b8047af594a6948 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_5_wrap_render_func$$$function_1_render );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_loop );
    var_loop = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_loop );
    var_loop = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_5_wrap_render_func$$$function_1_render );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_6_wrap_block_reference_call( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_original_call = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *var_async_call = PyCell_EMPTY();
    PyObject *var___call__ = NULL;
    struct Nuitka_FrameObject *frame_64d9569afd0b28fbd639d1f4b39ba5f0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_64d9569afd0b28fbd639d1f4b39ba5f0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_64d9569afd0b28fbd639d1f4b39ba5f0, codeobj_64d9569afd0b28fbd639d1f4b39ba5f0, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_64d9569afd0b28fbd639d1f4b39ba5f0 = cache_frame_64d9569afd0b28fbd639d1f4b39ba5f0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_64d9569afd0b28fbd639d1f4b39ba5f0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_64d9569afd0b28fbd639d1f4b39ba5f0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_internalcode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_internalcode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "internalcode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_args_element_name_1 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call(  );



        frame_64d9569afd0b28fbd639d1f4b39ba5f0->m_frame.f_lineno = 83;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_async_call ) == NULL );
        PyCell_SET( var_async_call, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_internalcode );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_internalcode );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "internalcode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 90;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_args_element_name_2 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_2___call__(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] = var_async_call;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] = par_original_call;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] );


        frame_64d9569afd0b28fbd639d1f4b39ba5f0->m_frame.f_lineno = 90;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        assert( var___call__ == NULL );
        var___call__ = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_update_wrapper );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_update_wrapper );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "update_wrapper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        CHECK_OBJECT( var___call__ );
        tmp_args_element_name_3 = var___call__;
        CHECK_OBJECT( PyCell_GET( par_original_call ) );
        tmp_args_element_name_4 = PyCell_GET( par_original_call );
        frame_64d9569afd0b28fbd639d1f4b39ba5f0->m_frame.f_lineno = 96;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_64d9569afd0b28fbd639d1f4b39ba5f0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_64d9569afd0b28fbd639d1f4b39ba5f0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_64d9569afd0b28fbd639d1f4b39ba5f0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_64d9569afd0b28fbd639d1f4b39ba5f0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_64d9569afd0b28fbd639d1f4b39ba5f0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_64d9569afd0b28fbd639d1f4b39ba5f0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_64d9569afd0b28fbd639d1f4b39ba5f0,
        type_description_1,
        par_original_call,
        var_async_call,
        var___call__
    );


    // Release cached frame.
    if ( frame_64d9569afd0b28fbd639d1f4b39ba5f0 == cache_frame_64d9569afd0b28fbd639d1f4b39ba5f0 )
    {
        Py_DECREF( frame_64d9569afd0b28fbd639d1f4b39ba5f0 );
    }
    cache_frame_64d9569afd0b28fbd639d1f4b39ba5f0 = NULL;

    assertFrameObject( frame_64d9569afd0b28fbd639d1f4b39ba5f0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_6_wrap_block_reference_call );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_original_call );
    Py_DECREF( par_original_call );
    par_original_call = NULL;

    CHECK_OBJECT( (PyObject *)var_async_call );
    Py_DECREF( var_async_call );
    var_async_call = NULL;

    CHECK_OBJECT( (PyObject *)var___call__ );
    Py_DECREF( var___call__ );
    var___call__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_original_call );
    Py_DECREF( par_original_call );
    par_original_call = NULL;

    CHECK_OBJECT( (PyObject *)var_async_call );
    Py_DECREF( var_async_call );
    var_async_call = NULL;

    Py_XDECREF( var___call__ );
    var___call__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_6_wrap_block_reference_call );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_self;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call_locals {
    PyObject *var_rv;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
};

static PyObject *jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call_locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_rv = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_07e4033d9f21261c918d73ab2d6aa8a8, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        coroutine->m_frame->m_frame.f_lineno = 85;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_concat_async );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_concat_async );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "concat_async" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( coroutine->m_closure[0] );
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__stack );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_subscribed_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = PyCell_GET( coroutine->m_closure[0] );
        tmp_subscript_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__depth );
        if ( tmp_subscript_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_subscribed_name_1 );

            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = PyCell_GET( coroutine->m_closure[0] );
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__context );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        coroutine->m_frame->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        coroutine->m_frame->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_expression_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = yield_return_value;
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 85;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->var_rv == NULL );
        coroutine_heap->var_rv = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_source_name_6;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 86;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = PyCell_GET( coroutine->m_closure[0] );
        tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__context );
        if ( tmp_source_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 86;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_eval_ctx );
        Py_DECREF( tmp_source_name_5 );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 86;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_autoescape );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 86;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            coroutine_heap->exception_lineno = 86;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_Markup );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Markup );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Markup" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 87;
                coroutine_heap->type_description_1 = "co";
                goto frame_exception_exit_1;
            }

            tmp_called_name_3 = tmp_mvar_value_2;
            CHECK_OBJECT( coroutine_heap->var_rv );
            tmp_args_element_name_3 = coroutine_heap->var_rv;
            coroutine->m_frame->m_frame.f_lineno = 87;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 87;
                coroutine_heap->type_description_1 = "co";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = coroutine_heap->var_rv;
                assert( old != NULL );
                coroutine_heap->var_rv = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine_heap->var_rv
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( coroutine_heap->var_rv );
    coroutine_heap->tmp_return_value = coroutine_heap->var_rv;
    Py_INCREF( coroutine_heap->tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_rv );
    Py_DECREF( coroutine_heap->var_rv );
    coroutine_heap->var_rv = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_rv );
    coroutine_heap->var_rv = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call_context,
        module_jinja2$asyncsupport,
        const_str_plain_async_call,
        const_str_digest_3ea09c231a2793f33456c0650667083b,
        codeobj_07e4033d9f21261c918d73ab2d6aa8a8,
        1,
        sizeof(struct jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call$$$coroutine_1_async_call_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_2___call__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ea897d3dddf300ea230b292301e5630c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_ea897d3dddf300ea230b292301e5630c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ea897d3dddf300ea230b292301e5630c, codeobj_ea897d3dddf300ea230b292301e5630c, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ea897d3dddf300ea230b292301e5630c = cache_frame_ea897d3dddf300ea230b292301e5630c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ea897d3dddf300ea230b292301e5630c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ea897d3dddf300ea230b292301e5630c ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__context );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_environment );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_async );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "original_call" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 93;
                type_description_1 = "occ";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = PyCell_GET( self->m_closure[1] );
            CHECK_OBJECT( par_self );
            tmp_args_element_name_1 = par_self;
            frame_ea897d3dddf300ea230b292301e5630c->m_frame.f_lineno = 93;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "occ";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "async_call" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_args_element_name_2 = par_self;
        frame_ea897d3dddf300ea230b292301e5630c->m_frame.f_lineno = 94;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ea897d3dddf300ea230b292301e5630c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ea897d3dddf300ea230b292301e5630c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ea897d3dddf300ea230b292301e5630c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ea897d3dddf300ea230b292301e5630c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ea897d3dddf300ea230b292301e5630c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ea897d3dddf300ea230b292301e5630c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ea897d3dddf300ea230b292301e5630c,
        type_description_1,
        par_self,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_ea897d3dddf300ea230b292301e5630c == cache_frame_ea897d3dddf300ea230b292301e5630c )
    {
        Py_DECREF( frame_ea897d3dddf300ea230b292301e5630c );
    }
    cache_frame_ea897d3dddf300ea230b292301e5630c = NULL;

    assertFrameObject( frame_ea897d3dddf300ea230b292301e5630c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_2___call__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_2___call__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_7_wrap_macro_invoke( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_original_invoke = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *var_async_invoke = PyCell_EMPTY();
    PyObject *var__invoke = NULL;
    struct Nuitka_FrameObject *frame_bf1c1ebb777926dd7a299ddd72c77a34;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_bf1c1ebb777926dd7a299ddd72c77a34 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bf1c1ebb777926dd7a299ddd72c77a34, codeobj_bf1c1ebb777926dd7a299ddd72c77a34, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bf1c1ebb777926dd7a299ddd72c77a34 = cache_frame_bf1c1ebb777926dd7a299ddd72c77a34;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bf1c1ebb777926dd7a299ddd72c77a34 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bf1c1ebb777926dd7a299ddd72c77a34 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_internalcode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_internalcode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "internalcode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_args_element_name_1 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke(  );



        frame_bf1c1ebb777926dd7a299ddd72c77a34->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_async_invoke ) == NULL );
        PyCell_SET( var_async_invoke, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_internalcode );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_internalcode );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "internalcode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_args_element_name_2 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_2__invoke(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] = var_async_invoke;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] = par_original_invoke;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] );


        frame_bf1c1ebb777926dd7a299ddd72c77a34->m_frame.f_lineno = 107;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        assert( var__invoke == NULL );
        var__invoke = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_update_wrapper );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_update_wrapper );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "update_wrapper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        CHECK_OBJECT( var__invoke );
        tmp_args_element_name_3 = var__invoke;
        CHECK_OBJECT( PyCell_GET( par_original_invoke ) );
        tmp_args_element_name_4 = PyCell_GET( par_original_invoke );
        frame_bf1c1ebb777926dd7a299ddd72c77a34->m_frame.f_lineno = 112;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf1c1ebb777926dd7a299ddd72c77a34 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf1c1ebb777926dd7a299ddd72c77a34 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf1c1ebb777926dd7a299ddd72c77a34 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bf1c1ebb777926dd7a299ddd72c77a34, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bf1c1ebb777926dd7a299ddd72c77a34->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bf1c1ebb777926dd7a299ddd72c77a34, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bf1c1ebb777926dd7a299ddd72c77a34,
        type_description_1,
        par_original_invoke,
        var_async_invoke,
        var__invoke
    );


    // Release cached frame.
    if ( frame_bf1c1ebb777926dd7a299ddd72c77a34 == cache_frame_bf1c1ebb777926dd7a299ddd72c77a34 )
    {
        Py_DECREF( frame_bf1c1ebb777926dd7a299ddd72c77a34 );
    }
    cache_frame_bf1c1ebb777926dd7a299ddd72c77a34 = NULL;

    assertFrameObject( frame_bf1c1ebb777926dd7a299ddd72c77a34 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_7_wrap_macro_invoke );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_original_invoke );
    Py_DECREF( par_original_invoke );
    par_original_invoke = NULL;

    CHECK_OBJECT( (PyObject *)var_async_invoke );
    Py_DECREF( var_async_invoke );
    var_async_invoke = NULL;

    CHECK_OBJECT( (PyObject *)var__invoke );
    Py_DECREF( var__invoke );
    var__invoke = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_original_invoke );
    Py_DECREF( par_original_invoke );
    par_original_invoke = NULL;

    CHECK_OBJECT( (PyObject *)var_async_invoke );
    Py_DECREF( var_async_invoke );
    var_async_invoke = NULL;

    Py_XDECREF( var__invoke );
    var__invoke = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_7_wrap_macro_invoke );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_arguments = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_autoescape = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_arguments;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_autoescape;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] = par_self;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)par_autoescape );
    Py_DECREF( par_autoescape );
    par_autoescape = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)par_autoescape );
    Py_DECREF( par_autoescape );
    par_autoescape = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke_locals {
    PyObject *var_rv;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
};

static PyObject *jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke_locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_rv = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_58492195bad6f50ecacb5696bc24058d, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        coroutine->m_frame->m_frame.f_lineno = 102;
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 102;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( coroutine->m_closure[2] );
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__func );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 102;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_dircall_arg1_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "arguments" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 102;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg2_1 = PyCell_GET( coroutine->m_closure[0] );
        Py_INCREF( tmp_dircall_arg2_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
            tmp_expression_name_2 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
        }
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 102;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 102;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_dircall_arg1_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_dircall_arg2_1, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_dircall_arg1_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_dircall_arg2_1, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 102;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = yield_return_value;
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 102;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->var_rv == NULL );
        coroutine_heap->var_rv = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "autoescape" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 103;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( PyCell_GET( coroutine->m_closure[1] ) );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 103;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_Markup );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Markup );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Markup" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 104;
                coroutine_heap->type_description_1 = "ccco";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( coroutine_heap->var_rv );
            tmp_args_element_name_1 = coroutine_heap->var_rv;
            coroutine->m_frame->m_frame.f_lineno = 104;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 104;
                coroutine_heap->type_description_1 = "ccco";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = coroutine_heap->var_rv;
                assert( old != NULL );
                coroutine_heap->var_rv = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[2],
            coroutine->m_closure[0],
            coroutine->m_closure[1],
            coroutine_heap->var_rv
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( coroutine_heap->var_rv );
    coroutine_heap->tmp_return_value = coroutine_heap->var_rv;
    Py_INCREF( coroutine_heap->tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_rv );
    Py_DECREF( coroutine_heap->var_rv );
    coroutine_heap->var_rv = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_rv );
    coroutine_heap->var_rv = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke_context,
        module_jinja2$asyncsupport,
        const_str_plain_async_invoke,
        const_str_digest_1f42321ef9bba5d561ea6c02a7960d6b,
        codeobj_58492195bad6f50ecacb5696bc24058d,
        3,
        sizeof(struct jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke$$$coroutine_1_async_invoke_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_2__invoke( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_arguments = python_pars[ 1 ];
    PyObject *par_autoescape = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_f75bb9b4aa760f68208e3028ea5e10e5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f75bb9b4aa760f68208e3028ea5e10e5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f75bb9b4aa760f68208e3028ea5e10e5, codeobj_f75bb9b4aa760f68208e3028ea5e10e5, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f75bb9b4aa760f68208e3028ea5e10e5 = cache_frame_f75bb9b4aa760f68208e3028ea5e10e5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f75bb9b4aa760f68208e3028ea5e10e5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f75bb9b4aa760f68208e3028ea5e10e5 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__environment );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_async );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "original_invoke" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 110;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = PyCell_GET( self->m_closure[1] );
            CHECK_OBJECT( par_self );
            tmp_args_element_name_1 = par_self;
            CHECK_OBJECT( par_arguments );
            tmp_args_element_name_2 = par_arguments;
            CHECK_OBJECT( par_autoescape );
            tmp_args_element_name_3 = par_autoescape;
            frame_f75bb9b4aa760f68208e3028ea5e10e5->m_frame.f_lineno = 110;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 110;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "async_invoke" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_args_element_name_4 = par_self;
        CHECK_OBJECT( par_arguments );
        tmp_args_element_name_5 = par_arguments;
        CHECK_OBJECT( par_autoescape );
        tmp_args_element_name_6 = par_autoescape;
        frame_f75bb9b4aa760f68208e3028ea5e10e5->m_frame.f_lineno = 111;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f75bb9b4aa760f68208e3028ea5e10e5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f75bb9b4aa760f68208e3028ea5e10e5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f75bb9b4aa760f68208e3028ea5e10e5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f75bb9b4aa760f68208e3028ea5e10e5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f75bb9b4aa760f68208e3028ea5e10e5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f75bb9b4aa760f68208e3028ea5e10e5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f75bb9b4aa760f68208e3028ea5e10e5,
        type_description_1,
        par_self,
        par_arguments,
        par_autoescape,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_f75bb9b4aa760f68208e3028ea5e10e5 == cache_frame_f75bb9b4aa760f68208e3028ea5e10e5 )
    {
        Py_DECREF( frame_f75bb9b4aa760f68208e3028ea5e10e5 );
    }
    cache_frame_f75bb9b4aa760f68208e3028ea5e10e5 = NULL;

    assertFrameObject( frame_f75bb9b4aa760f68208e3028ea5e10e5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_2__invoke );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)par_autoescape );
    Py_DECREF( par_autoescape );
    par_autoescape = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)par_autoescape );
    Py_DECREF( par_autoescape );
    par_autoescape = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_2__invoke );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_8_get_default_module_async( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_self;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_8_get_default_module_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_8_get_default_module_async );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async_locals {
    PyObject *var_rv;
    PyObject *tmp_assign_unpack_1__assign_source;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_return_value;
    char yield_tmps[1024];
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async_locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_rv = NULL;
    coroutine_heap->tmp_assign_unpack_1__assign_source = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_e3e90d4cdae6538e1286fa2ddcca2c7d, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 117;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( coroutine->m_closure[0] );
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__module );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 117;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_source_name_2;
            if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 118;
                coroutine_heap->type_description_1 = "co";
                goto frame_exception_exit_1;
            }

            tmp_source_name_2 = PyCell_GET( coroutine->m_closure[0] );
            coroutine_heap->tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__module );
            if ( coroutine_heap->tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 118;
                coroutine_heap->type_description_1 = "co";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_instance_1;
        coroutine->m_frame->m_frame.f_lineno = 119;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 119;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = PyCell_GET( coroutine->m_closure[0] );
        coroutine->m_frame->m_frame.f_lineno = 119;
        tmp_expression_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_make_module_async );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 119;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 119;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 119;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = yield_return_value;
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 119;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_2;
        }
        assert( coroutine_heap->tmp_assign_unpack_1__assign_source == NULL );
        coroutine_heap->tmp_assign_unpack_1__assign_source = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( coroutine_heap->tmp_assign_unpack_1__assign_source );
        tmp_assattr_name_1 = coroutine_heap->tmp_assign_unpack_1__assign_source;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 119;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_2;
        }

        tmp_assattr_target_1 = PyCell_GET( coroutine->m_closure[0] );
        coroutine_heap->tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__module, tmp_assattr_name_1 );
        if ( coroutine_heap->tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 119;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_2;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->tmp_assign_unpack_1__assign_source );
    coroutine_heap->tmp_assign_unpack_1__assign_source = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine_heap->var_rv
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( coroutine_heap->tmp_assign_unpack_1__assign_source );
        tmp_assign_source_2 = coroutine_heap->tmp_assign_unpack_1__assign_source;
        assert( coroutine_heap->var_rv == NULL );
        Py_INCREF( tmp_assign_source_2 );
        coroutine_heap->var_rv = tmp_assign_source_2;
    }
    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_assign_unpack_1__assign_source );
    Py_DECREF( coroutine_heap->tmp_assign_unpack_1__assign_source );
    coroutine_heap->tmp_assign_unpack_1__assign_source = NULL;

    CHECK_OBJECT( coroutine_heap->var_rv );
    coroutine_heap->tmp_return_value = coroutine_heap->var_rv;
    Py_INCREF( coroutine_heap->tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( coroutine_heap->var_rv );
    coroutine_heap->var_rv = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async_context,
        module_jinja2$asyncsupport,
        const_str_plain_get_default_module_async,
        NULL,
        codeobj_e3e90d4cdae6538e1286fa2ddcca2c7d,
        1,
        sizeof(struct jinja2$asyncsupport$$$function_8_get_default_module_async$$$coroutine_1_get_default_module_async_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_9_wrap_default_module( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_original_default_module = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var__get_default_module = NULL;
    struct Nuitka_FrameObject *frame_18a1cc25b18ba20a6501b40b66a3172d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_18a1cc25b18ba20a6501b40b66a3172d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_18a1cc25b18ba20a6501b40b66a3172d, codeobj_18a1cc25b18ba20a6501b40b66a3172d, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    frame_18a1cc25b18ba20a6501b40b66a3172d = cache_frame_18a1cc25b18ba20a6501b40b66a3172d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_18a1cc25b18ba20a6501b40b66a3172d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_18a1cc25b18ba20a6501b40b66a3172d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_internalcode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_internalcode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "internalcode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_args_element_name_1 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_9_wrap_default_module$$$function_1__get_default_module(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] = par_original_default_module;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] );


        frame_18a1cc25b18ba20a6501b40b66a3172d->m_frame.f_lineno = 124;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        assert( var__get_default_module == NULL );
        var__get_default_module = tmp_assign_source_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18a1cc25b18ba20a6501b40b66a3172d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18a1cc25b18ba20a6501b40b66a3172d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_18a1cc25b18ba20a6501b40b66a3172d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_18a1cc25b18ba20a6501b40b66a3172d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_18a1cc25b18ba20a6501b40b66a3172d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_18a1cc25b18ba20a6501b40b66a3172d,
        type_description_1,
        par_original_default_module,
        var__get_default_module
    );


    // Release cached frame.
    if ( frame_18a1cc25b18ba20a6501b40b66a3172d == cache_frame_18a1cc25b18ba20a6501b40b66a3172d )
    {
        Py_DECREF( frame_18a1cc25b18ba20a6501b40b66a3172d );
    }
    cache_frame_18a1cc25b18ba20a6501b40b66a3172d = NULL;

    assertFrameObject( frame_18a1cc25b18ba20a6501b40b66a3172d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var__get_default_module );
    tmp_return_value = var__get_default_module;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_9_wrap_default_module );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_original_default_module );
    Py_DECREF( par_original_default_module );
    par_original_default_module = NULL;

    CHECK_OBJECT( (PyObject *)var__get_default_module );
    Py_DECREF( var__get_default_module );
    var__get_default_module = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_original_default_module );
    Py_DECREF( par_original_default_module );
    par_original_default_module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_9_wrap_default_module );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_9_wrap_default_module$$$function_1__get_default_module( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5ae50fc21a721b02bd5d43f51df2ad7f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_5ae50fc21a721b02bd5d43f51df2ad7f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5ae50fc21a721b02bd5d43f51df2ad7f, codeobj_5ae50fc21a721b02bd5d43f51df2ad7f, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    frame_5ae50fc21a721b02bd5d43f51df2ad7f = cache_frame_5ae50fc21a721b02bd5d43f51df2ad7f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5ae50fc21a721b02bd5d43f51df2ad7f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5ae50fc21a721b02bd5d43f51df2ad7f ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_environment );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_async );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 126;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_fb845e53b3d15f89e589e1d47fc1ead5;
            frame_5ae50fc21a721b02bd5d43f51df2ad7f->m_frame.f_lineno = 127;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 127;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "original_default_module" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        frame_5ae50fc21a721b02bd5d43f51df2ad7f->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ae50fc21a721b02bd5d43f51df2ad7f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ae50fc21a721b02bd5d43f51df2ad7f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ae50fc21a721b02bd5d43f51df2ad7f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5ae50fc21a721b02bd5d43f51df2ad7f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5ae50fc21a721b02bd5d43f51df2ad7f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5ae50fc21a721b02bd5d43f51df2ad7f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5ae50fc21a721b02bd5d43f51df2ad7f,
        type_description_1,
        par_self,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_5ae50fc21a721b02bd5d43f51df2ad7f == cache_frame_5ae50fc21a721b02bd5d43f51df2ad7f )
    {
        Py_DECREF( frame_5ae50fc21a721b02bd5d43f51df2ad7f );
    }
    cache_frame_5ae50fc21a721b02bd5d43f51df2ad7f = NULL;

    assertFrameObject( frame_5ae50fc21a721b02bd5d43f51df2ad7f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_9_wrap_default_module$$$function_1__get_default_module );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_9_wrap_default_module$$$function_1__get_default_module );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_10_make_module_async( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_vars = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_shared = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_locals = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_locals;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_self;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] = par_shared;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] = par_vars;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_10_make_module_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_vars );
    Py_DECREF( par_vars );
    par_vars = NULL;

    CHECK_OBJECT( (PyObject *)par_shared );
    Py_DECREF( par_shared );
    par_shared = NULL;

    CHECK_OBJECT( (PyObject *)par_locals );
    Py_DECREF( par_locals );
    par_locals = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_vars );
    Py_DECREF( par_vars );
    par_vars = NULL;

    CHECK_OBJECT( (PyObject *)par_shared );
    Py_DECREF( par_shared );
    par_shared = NULL;

    CHECK_OBJECT( (PyObject *)par_locals );
    Py_DECREF( par_locals );
    par_locals = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_10_make_module_async );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async_locals {
    PyObject *var_context;
    PyObject *var_body_stream;
    PyObject *var_item;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async_locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_context = NULL;
    coroutine_heap->var_body_stream = NULL;
    coroutine_heap->var_item = NULL;
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_8bb0f2bbea2cda8feebec760dde8a8b2, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 134;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( coroutine->m_closure[1] );
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_new_context );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 134;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[3] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "vars" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 134;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[3] );
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "shared" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 134;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( coroutine->m_closure[2] );
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "locals" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 134;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = PyCell_GET( coroutine->m_closure[0] );
        coroutine->m_frame->m_frame.f_lineno = 134;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 134;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->var_context == NULL );
        coroutine_heap->var_context = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( coroutine_heap->var_body_stream == NULL );
        coroutine_heap->var_body_stream = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_value_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_4;
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 136;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( coroutine->m_closure[1] );
        CHECK_OBJECT( coroutine_heap->var_context );
        tmp_args_element_name_4 = coroutine_heap->var_context;
        coroutine->m_frame->m_frame.f_lineno = 136;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_value_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_root_render_func, call_args );
        }

        if ( tmp_value_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 136;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        tmp_expression_name_1 = ASYNC_MAKE_ITERATOR( tmp_value_name_1 );
        Py_DECREF( tmp_value_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 136;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_value_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_value_name_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 136;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = yield_return_value;
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 136;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->tmp_for_loop_1__for_iterator == NULL );
        coroutine_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_value_name_2;
        CHECK_OBJECT( coroutine_heap->tmp_for_loop_1__for_iterator );
        tmp_value_name_2 = coroutine_heap->tmp_for_loop_1__for_iterator;
        tmp_expression_name_2 = ASYNC_ITERATOR_NEXT( tmp_value_name_2 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 136;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_3;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_value_name_2, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 2;
        coroutine->m_yieldfrom = tmp_expression_name_2;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_2:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_value_name_2, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 136;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_4 = yield_return_value;
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 136;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = coroutine_heap->tmp_for_loop_1__iter_value;
            coroutine_heap->tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = coroutine_heap->exception_keeper_type_1;
        tmp_compexpr_right_1 = PyExc_StopAsyncIteration;
        coroutine_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );

            Py_DECREF( coroutine_heap->exception_keeper_type_1 );
            Py_XDECREF( coroutine_heap->exception_keeper_value_1 );
            Py_XDECREF( coroutine_heap->exception_keeper_tb_1 );

            coroutine_heap->exception_lineno = 136;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        Py_DECREF( coroutine_heap->exception_keeper_type_1 );
        Py_XDECREF( coroutine_heap->exception_keeper_value_1 );
        Py_XDECREF( coroutine_heap->exception_keeper_tb_1 );
        goto loop_end_1;
        goto branch_end_1;
        branch_no_1:;
        // Re-raise.
        coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
        coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
        coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
        coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

        goto try_except_handler_2;
        branch_end_1:;
    }
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( coroutine_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = coroutine_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = coroutine_heap->var_item;
            coroutine_heap->var_item = tmp_assign_source_5;
            Py_INCREF( coroutine_heap->var_item );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( coroutine_heap->var_body_stream );
        tmp_called_instance_2 = coroutine_heap->var_body_stream;
        CHECK_OBJECT( coroutine_heap->var_item );
        tmp_args_element_name_5 = coroutine_heap->var_item;
        coroutine->m_frame->m_frame.f_lineno = 137;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 137;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


        coroutine_heap->exception_lineno = 136;
        coroutine_heap->type_description_1 = "ccccooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->tmp_for_loop_1__iter_value );
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( coroutine_heap->tmp_for_loop_1__for_iterator );
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    Py_XDECREF( coroutine_heap->tmp_for_loop_1__iter_value );
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( coroutine_heap->tmp_for_loop_1__for_iterator );
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_TemplateModule );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TemplateModule );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TemplateModule" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 138;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 138;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_6 = PyCell_GET( coroutine->m_closure[1] );
        CHECK_OBJECT( coroutine_heap->var_context );
        tmp_args_element_name_7 = coroutine_heap->var_context;
        CHECK_OBJECT( coroutine_heap->var_body_stream );
        tmp_args_element_name_8 = coroutine_heap->var_body_stream;
        coroutine->m_frame->m_frame.f_lineno = 138;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 138;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[1],
            coroutine->m_closure[3],
            coroutine->m_closure[2],
            coroutine->m_closure[0],
            coroutine_heap->var_context,
            coroutine_heap->var_body_stream,
            coroutine_heap->var_item
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_context );
    Py_DECREF( coroutine_heap->var_context );
    coroutine_heap->var_context = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_body_stream );
    Py_DECREF( coroutine_heap->var_body_stream );
    coroutine_heap->var_body_stream = NULL;

    Py_XDECREF( coroutine_heap->var_item );
    coroutine_heap->var_item = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_3 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_3 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_3 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_3 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_context );
    coroutine_heap->var_context = NULL;

    Py_XDECREF( coroutine_heap->var_body_stream );
    coroutine_heap->var_body_stream = NULL;

    Py_XDECREF( coroutine_heap->var_item );
    coroutine_heap->var_item = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_3;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_3;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_3;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async_context,
        module_jinja2$asyncsupport,
        const_str_plain_make_module_async,
        NULL,
        codeobj_8bb0f2bbea2cda8feebec760dde8a8b2,
        4,
        sizeof(struct jinja2$asyncsupport$$$function_10_make_module_async$$$coroutine_1_make_module_async_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_11_patch_template( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_Template = NULL;
    struct Nuitka_FrameObject *frame_4b55f3a546fa26d3e73b5679f1b183c7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_4b55f3a546fa26d3e73b5679f1b183c7 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4b55f3a546fa26d3e73b5679f1b183c7, codeobj_4b55f3a546fa26d3e73b5679f1b183c7, module_jinja2$asyncsupport, sizeof(void *) );
    frame_4b55f3a546fa26d3e73b5679f1b183c7 = cache_frame_4b55f3a546fa26d3e73b5679f1b183c7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4b55f3a546fa26d3e73b5679f1b183c7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4b55f3a546fa26d3e73b5679f1b183c7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_jinja2;
        tmp_globals_name_1 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Template_tuple;
        tmp_level_name_1 = const_int_0;
        frame_4b55f3a546fa26d3e73b5679f1b183c7->m_frame.f_lineno = 142;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Template );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        assert( var_Template == NULL );
        var_Template = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_generate_func );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wrap_generate_func );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wrap_generate_func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_Template );
        tmp_source_name_1 = var_Template;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_generate );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_4b55f3a546fa26d3e73b5679f1b183c7->m_frame.f_lineno = 143;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_Template );
        tmp_assattr_target_1 = var_Template;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_generate, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_update_wrapper );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_update_wrapper );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "update_wrapper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 144;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_generate_async );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_generate_async );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "generate_async" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 145;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_3;
        CHECK_OBJECT( var_Template );
        tmp_source_name_2 = var_Template;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_generate_async );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_4b55f3a546fa26d3e73b5679f1b183c7->m_frame.f_lineno = 144;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assattr_name_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_Template );
        tmp_assattr_target_2 = var_Template;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_generate_async, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_3;
        PyObject *tmp_assattr_target_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_update_wrapper );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_update_wrapper );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "update_wrapper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_render_async );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_render_async );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "render_async" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_4 = tmp_mvar_value_5;
        CHECK_OBJECT( var_Template );
        tmp_source_name_3 = var_Template;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_render_async );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_4b55f3a546fa26d3e73b5679f1b183c7->m_frame.f_lineno = 146;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_assattr_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_Template );
        tmp_assattr_target_3 = var_Template;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_render_async, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_source_name_4;
        PyObject *tmp_assattr_target_4;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_render_func );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wrap_render_func );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wrap_render_func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_6;
        CHECK_OBJECT( var_Template );
        tmp_source_name_4 = var_Template;
        tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_render );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_4b55f3a546fa26d3e73b5679f1b183c7->m_frame.f_lineno = 148;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_assattr_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_assattr_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_Template );
        tmp_assattr_target_4 = var_Template;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_render, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_source_name_5;
        PyObject *tmp_assattr_target_5;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_default_module );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wrap_default_module );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wrap_default_module" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_7;
        CHECK_OBJECT( var_Template );
        tmp_source_name_5 = var_Template;
        tmp_args_element_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__get_default_module );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_4b55f3a546fa26d3e73b5679f1b183c7->m_frame.f_lineno = 149;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_assattr_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assattr_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_Template );
        tmp_assattr_target_5 = var_Template;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__get_default_module, tmp_assattr_name_5 );
        Py_DECREF( tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_assattr_target_6;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_get_default_module_async );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_default_module_async );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_default_module_async" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 151;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_assattr_name_6 = tmp_mvar_value_8;
        CHECK_OBJECT( var_Template );
        tmp_assattr_target_6 = var_Template;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__get_default_module_async, tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_source_name_6;
        PyObject *tmp_assattr_target_7;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_update_wrapper );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_update_wrapper );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "update_wrapper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 152;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_9;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_make_module_async );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_make_module_async );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "make_module_async" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 153;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_8 = tmp_mvar_value_10;
        CHECK_OBJECT( var_Template );
        tmp_source_name_6 = var_Template;
        tmp_args_element_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_make_module_async );
        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_4b55f3a546fa26d3e73b5679f1b183c7->m_frame.f_lineno = 152;
        {
            PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9 };
            tmp_assattr_name_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_assattr_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_Template );
        tmp_assattr_target_7 = var_Template;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_make_module_async, tmp_assattr_name_7 );
        Py_DECREF( tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4b55f3a546fa26d3e73b5679f1b183c7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4b55f3a546fa26d3e73b5679f1b183c7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4b55f3a546fa26d3e73b5679f1b183c7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4b55f3a546fa26d3e73b5679f1b183c7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4b55f3a546fa26d3e73b5679f1b183c7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4b55f3a546fa26d3e73b5679f1b183c7,
        type_description_1,
        var_Template
    );


    // Release cached frame.
    if ( frame_4b55f3a546fa26d3e73b5679f1b183c7 == cache_frame_4b55f3a546fa26d3e73b5679f1b183c7 )
    {
        Py_DECREF( frame_4b55f3a546fa26d3e73b5679f1b183c7 );
    }
    cache_frame_4b55f3a546fa26d3e73b5679f1b183c7 = NULL;

    assertFrameObject( frame_4b55f3a546fa26d3e73b5679f1b183c7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_11_patch_template );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_Template );
    Py_DECREF( var_Template );
    var_Template = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_Template );
    var_Template = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_11_patch_template );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_12_patch_runtime( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_BlockReference = NULL;
    PyObject *var_Macro = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_1648508166d5a9568d9532ae759f5032;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_1648508166d5a9568d9532ae759f5032 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1648508166d5a9568d9532ae759f5032, codeobj_1648508166d5a9568d9532ae759f5032, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    frame_1648508166d5a9568d9532ae759f5032 = cache_frame_1648508166d5a9568d9532ae759f5032;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1648508166d5a9568d9532ae759f5032 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1648508166d5a9568d9532ae759f5032 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_38d72d37090218643439302eee0c8cc7;
        tmp_globals_name_1 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_BlockReference_str_plain_Macro_tuple;
        tmp_level_name_1 = const_int_0;
        frame_1648508166d5a9568d9532ae759f5032->m_frame.f_lineno = 157;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_BlockReference );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( var_BlockReference == NULL );
        var_BlockReference = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_3 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Macro );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( var_Macro == NULL );
        var_Macro = tmp_assign_source_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_block_reference_call );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wrap_block_reference_call );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wrap_block_reference_call" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 158;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_BlockReference );
        tmp_source_name_1 = var_BlockReference;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___call__ );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_1648508166d5a9568d9532ae759f5032->m_frame.f_lineno = 158;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_BlockReference );
        tmp_assattr_target_1 = var_BlockReference;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain___call__, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_macro_invoke );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wrap_macro_invoke );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wrap_macro_invoke" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( var_Macro );
        tmp_source_name_2 = var_Macro;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__invoke );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_1648508166d5a9568d9532ae759f5032->m_frame.f_lineno = 160;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assattr_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_Macro );
        tmp_assattr_target_2 = var_Macro;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__invoke, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1648508166d5a9568d9532ae759f5032 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1648508166d5a9568d9532ae759f5032 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1648508166d5a9568d9532ae759f5032, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1648508166d5a9568d9532ae759f5032->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1648508166d5a9568d9532ae759f5032, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1648508166d5a9568d9532ae759f5032,
        type_description_1,
        var_BlockReference,
        var_Macro
    );


    // Release cached frame.
    if ( frame_1648508166d5a9568d9532ae759f5032 == cache_frame_1648508166d5a9568d9532ae759f5032 )
    {
        Py_DECREF( frame_1648508166d5a9568d9532ae759f5032 );
    }
    cache_frame_1648508166d5a9568d9532ae759f5032 = NULL;

    assertFrameObject( frame_1648508166d5a9568d9532ae759f5032 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_12_patch_runtime );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_BlockReference );
    Py_DECREF( var_BlockReference );
    var_BlockReference = NULL;

    CHECK_OBJECT( (PyObject *)var_Macro );
    Py_DECREF( var_Macro );
    var_Macro = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_BlockReference );
    var_BlockReference = NULL;

    Py_XDECREF( var_Macro );
    var_Macro = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_12_patch_runtime );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_13_patch_filters( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_FILTERS = NULL;
    PyObject *var_ASYNC_FILTERS = NULL;
    struct Nuitka_FrameObject *frame_e420348637aa31a4160898b1c19f16b3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e420348637aa31a4160898b1c19f16b3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e420348637aa31a4160898b1c19f16b3, codeobj_e420348637aa31a4160898b1c19f16b3, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    frame_e420348637aa31a4160898b1c19f16b3 = cache_frame_e420348637aa31a4160898b1c19f16b3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e420348637aa31a4160898b1c19f16b3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e420348637aa31a4160898b1c19f16b3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_74893e30ca8be260e576d22e2956d0c6;
        tmp_globals_name_1 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_FILTERS_tuple;
        tmp_level_name_1 = const_int_0;
        frame_e420348637aa31a4160898b1c19f16b3->m_frame.f_lineno = 164;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_FILTERS );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_FILTERS == NULL );
        var_FILTERS = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_e9c0aba9ba51d097a6babda6163af2fe;
        tmp_globals_name_2 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_ASYNC_FILTERS_tuple;
        tmp_level_name_2 = const_int_0;
        frame_e420348637aa31a4160898b1c19f16b3->m_frame.f_lineno = 165;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_ASYNC_FILTERS );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_ASYNC_FILTERS == NULL );
        var_ASYNC_FILTERS = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_FILTERS );
        tmp_called_instance_1 = var_FILTERS;
        CHECK_OBJECT( var_ASYNC_FILTERS );
        tmp_args_element_name_1 = var_ASYNC_FILTERS;
        frame_e420348637aa31a4160898b1c19f16b3->m_frame.f_lineno = 166;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_update, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e420348637aa31a4160898b1c19f16b3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e420348637aa31a4160898b1c19f16b3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e420348637aa31a4160898b1c19f16b3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e420348637aa31a4160898b1c19f16b3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e420348637aa31a4160898b1c19f16b3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e420348637aa31a4160898b1c19f16b3,
        type_description_1,
        var_FILTERS,
        var_ASYNC_FILTERS
    );


    // Release cached frame.
    if ( frame_e420348637aa31a4160898b1c19f16b3 == cache_frame_e420348637aa31a4160898b1c19f16b3 )
    {
        Py_DECREF( frame_e420348637aa31a4160898b1c19f16b3 );
    }
    cache_frame_e420348637aa31a4160898b1c19f16b3 = NULL;

    assertFrameObject( frame_e420348637aa31a4160898b1c19f16b3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_13_patch_filters );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_FILTERS );
    Py_DECREF( var_FILTERS );
    var_FILTERS = NULL;

    CHECK_OBJECT( (PyObject *)var_ASYNC_FILTERS );
    Py_DECREF( var_ASYNC_FILTERS );
    var_ASYNC_FILTERS = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_FILTERS );
    var_FILTERS = NULL;

    Py_XDECREF( var_ASYNC_FILTERS );
    var_ASYNC_FILTERS = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_13_patch_filters );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_14_patch_all( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_e1cd052a6439bdb80665802b33a17a24;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e1cd052a6439bdb80665802b33a17a24 = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_e1cd052a6439bdb80665802b33a17a24, codeobj_e1cd052a6439bdb80665802b33a17a24, module_jinja2$asyncsupport, 0 );
    frame_e1cd052a6439bdb80665802b33a17a24 = cache_frame_e1cd052a6439bdb80665802b33a17a24;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e1cd052a6439bdb80665802b33a17a24 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e1cd052a6439bdb80665802b33a17a24 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_patch_template );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_patch_template );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "patch_template" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 170;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_e1cd052a6439bdb80665802b33a17a24->m_frame.f_lineno = 170;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_patch_runtime );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_patch_runtime );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "patch_runtime" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        frame_e1cd052a6439bdb80665802b33a17a24->m_frame.f_lineno = 171;
        tmp_call_result_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_patch_filters );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_patch_filters );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "patch_filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        frame_e1cd052a6439bdb80665802b33a17a24->m_frame.f_lineno = 172;
        tmp_call_result_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e1cd052a6439bdb80665802b33a17a24 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e1cd052a6439bdb80665802b33a17a24 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e1cd052a6439bdb80665802b33a17a24, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e1cd052a6439bdb80665802b33a17a24->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e1cd052a6439bdb80665802b33a17a24, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e1cd052a6439bdb80665802b33a17a24,
        type_description_1
    );


    // Release cached frame.
    if ( frame_e1cd052a6439bdb80665802b33a17a24 == cache_frame_e1cd052a6439bdb80665802b33a17a24 )
    {
        Py_DECREF( frame_e1cd052a6439bdb80665802b33a17a24 );
    }
    cache_frame_e1cd052a6439bdb80665802b33a17a24 = NULL;

    assertFrameObject( frame_e1cd052a6439bdb80665802b33a17a24 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_14_patch_all );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_15_auto_await( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_value = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_value;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_15_auto_await );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_15_auto_await );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await_locals {
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_return_value;
    char yield_tmps[1024];
};

static PyObject *jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await_locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_23294641a30c5a7bda6b5f6f2ebb7fc3, module_jinja2$asyncsupport, sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_inspect );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_inspect );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "inspect" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 176;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_isawaitable );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 176;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 176;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[0] );
        coroutine->m_frame->m_frame.f_lineno = 176;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 176;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_call_result_1 );

            coroutine_heap->exception_lineno = 176;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_expression_name_1;
            PyObject *tmp_expression_name_2;
            coroutine->m_frame->m_frame.f_lineno = 177;
            if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 177;
                coroutine_heap->type_description_1 = "c";
                goto frame_exception_exit_1;
            }

            tmp_expression_name_2 = PyCell_GET( coroutine->m_closure[0] );
            tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 177;
                coroutine_heap->type_description_1 = "c";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_expression_name_2, sizeof(PyObject *), NULL );
            coroutine->m_yield_return_index = 1;
            coroutine->m_yieldfrom = tmp_expression_name_1;
            coroutine->m_awaiting = true;
            return NULL;

            yield_return_1:
            Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_expression_name_2, sizeof(PyObject *), NULL );
            coroutine->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 177;
                coroutine_heap->type_description_1 = "c";
                goto frame_exception_exit_1;
            }
            coroutine_heap->tmp_return_value = yield_return_value;
            if ( coroutine_heap->tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 177;
                coroutine_heap->type_description_1 = "c";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
    {

        coroutine_heap->exception_type = PyExc_NameError;
        Py_INCREF( coroutine_heap->exception_type );
        coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
        coroutine_heap->exception_tb = NULL;
        NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
        CHAIN_EXCEPTION( coroutine_heap->exception_value );

        coroutine_heap->exception_lineno = 178;
        coroutine_heap->type_description_1 = "c";
        goto frame_exception_exit_1;
    }

    coroutine_heap->tmp_return_value = PyCell_GET( coroutine->m_closure[0] );
    Py_INCREF( coroutine_heap->tmp_return_value );
    goto frame_return_exit_1;

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await_context,
        module_jinja2$asyncsupport,
        const_str_plain_auto_await,
        NULL,
        codeobj_23294641a30c5a7bda6b5f6f2ebb7fc3,
        1,
        sizeof(struct jinja2$asyncsupport$$$function_15_auto_await$$$coroutine_1_auto_await_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_16_auto_aiter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_iterable = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter_maker();

    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[0] = par_iterable;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_16_auto_aiter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_iterable );
    Py_DECREF( par_iterable );
    par_iterable = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_iterable );
    Py_DECREF( par_iterable );
    par_iterable = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_16_auto_aiter );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter_locals {
    PyObject *var_item;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_for_loop_2__for_iterator;
    PyObject *tmp_for_loop_2__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter_context( struct Nuitka_AsyncgenObject *asyncgen, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)asyncgen );
    assert( Nuitka_Asyncgen_Check( (PyObject *)asyncgen ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter_locals *asyncgen_heap = (struct jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter_locals *)asyncgen->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(asyncgen->m_yield_return_index) {
    case 4: goto yield_return_4;
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    asyncgen_heap->var_item = NULL;
    asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;
    asyncgen_heap->tmp_for_loop_1__iter_value = NULL;
    asyncgen_heap->tmp_for_loop_2__for_iterator = NULL;
    asyncgen_heap->tmp_for_loop_2__iter_value = NULL;
    asyncgen_heap->type_description_1 = NULL;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    // Actual asyngen body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_a5565103ad3eef4b0544670c66e722f4, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    asyncgen->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( asyncgen->m_frame );
    assert( Py_REFCNT( asyncgen->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    asyncgen->m_frame->m_frame.f_gen = (PyObject *)asyncgen;
#endif

    Py_CLEAR( asyncgen->m_frame->m_frame.f_back );

    asyncgen->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( asyncgen->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &asyncgen->m_frame->m_frame;
    Py_INCREF( asyncgen->m_frame );

    Nuitka_Frame_MarkAsExecuting( asyncgen->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        asyncgen->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( asyncgen->m_frame->m_frame.f_exc_type == Py_None ) asyncgen->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_type );
    asyncgen->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_value );
    asyncgen->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_traceback );
#else
        asyncgen->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( asyncgen->m_exc_state.exc_type == Py_None ) asyncgen->m_exc_state.exc_type = NULL;
        Py_XINCREF( asyncgen->m_exc_state.exc_type );
        asyncgen->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( asyncgen->m_exc_state.exc_value );
        asyncgen->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( asyncgen->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_name_1;
        if ( PyCell_GET( asyncgen->m_closure[0] ) == NULL )
        {

            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "iterable" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 182;
            asyncgen_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( asyncgen->m_closure[0] );
        tmp_attribute_name_1 = const_str_plain___aiter__;
        asyncgen_heap->tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( asyncgen_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 182;
            asyncgen_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( asyncgen_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_expression_name_1;
            PyObject *tmp_value_name_1;
            if ( PyCell_GET( asyncgen->m_closure[0] ) == NULL )
            {

                asyncgen_heap->exception_type = PyExc_NameError;
                Py_INCREF( asyncgen_heap->exception_type );
                asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "iterable" );
                asyncgen_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
                CHAIN_EXCEPTION( asyncgen_heap->exception_value );

                asyncgen_heap->exception_lineno = 183;
                asyncgen_heap->type_description_1 = "co";
                goto frame_exception_exit_1;
            }

            tmp_value_name_1 = PyCell_GET( asyncgen->m_closure[0] );
            tmp_expression_name_1 = ASYNC_MAKE_ITERATOR( tmp_value_name_1 );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 183;
                asyncgen_heap->type_description_1 = "co";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_value_name_1, sizeof(PyObject *), NULL );
            asyncgen->m_yield_return_index = 1;
            asyncgen->m_yieldfrom = tmp_expression_name_1;
            asyncgen->m_awaiting = true;
            return NULL;

            yield_return_1:
            Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_value_name_1, sizeof(PyObject *), NULL );
            asyncgen->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 183;
                asyncgen_heap->type_description_1 = "co";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = yield_return_value;
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 183;
                asyncgen_heap->type_description_1 = "co";
                goto frame_exception_exit_1;
            }
            assert( asyncgen_heap->tmp_for_loop_1__for_iterator == NULL );
            asyncgen_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_1;
        }
        // Tried code:
        loop_start_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_expression_name_2;
            PyObject *tmp_value_name_2;
            CHECK_OBJECT( asyncgen_heap->tmp_for_loop_1__for_iterator );
            tmp_value_name_2 = asyncgen_heap->tmp_for_loop_1__for_iterator;
            tmp_expression_name_2 = ASYNC_ITERATOR_NEXT( tmp_value_name_2 );
            if ( tmp_expression_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 183;
                asyncgen_heap->type_description_1 = "co";
                goto try_except_handler_3;
            }
            Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_value_name_2, sizeof(PyObject *), NULL );
            asyncgen->m_yield_return_index = 2;
            asyncgen->m_yieldfrom = tmp_expression_name_2;
            asyncgen->m_awaiting = true;
            return NULL;

            yield_return_2:
            Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_value_name_2, sizeof(PyObject *), NULL );
            asyncgen->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 183;
                asyncgen_heap->type_description_1 = "co";
                goto try_except_handler_3;
            }
            tmp_assign_source_2 = yield_return_value;
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 183;
                asyncgen_heap->type_description_1 = "co";
                goto try_except_handler_3;
            }
            {
                PyObject *old = asyncgen_heap->tmp_for_loop_1__iter_value;
                asyncgen_heap->tmp_for_loop_1__iter_value = tmp_assign_source_2;
                Py_XDECREF( old );
            }

        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        asyncgen_heap->exception_keeper_type_1 = asyncgen_heap->exception_type;
        asyncgen_heap->exception_keeper_value_1 = asyncgen_heap->exception_value;
        asyncgen_heap->exception_keeper_tb_1 = asyncgen_heap->exception_tb;
        asyncgen_heap->exception_keeper_lineno_1 = asyncgen_heap->exception_lineno;
        asyncgen_heap->exception_type = NULL;
        asyncgen_heap->exception_value = NULL;
        asyncgen_heap->exception_tb = NULL;
        asyncgen_heap->exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = asyncgen_heap->exception_keeper_type_1;
            tmp_compexpr_right_1 = PyExc_StopAsyncIteration;
            asyncgen_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( asyncgen_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );

                Py_DECREF( asyncgen_heap->exception_keeper_type_1 );
                Py_XDECREF( asyncgen_heap->exception_keeper_value_1 );
                Py_XDECREF( asyncgen_heap->exception_keeper_tb_1 );

                asyncgen_heap->exception_lineno = 183;
                asyncgen_heap->type_description_1 = "co";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = ( asyncgen_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            Py_DECREF( asyncgen_heap->exception_keeper_type_1 );
            Py_XDECREF( asyncgen_heap->exception_keeper_value_1 );
            Py_XDECREF( asyncgen_heap->exception_keeper_tb_1 );
            goto loop_end_1;
            goto branch_end_2;
            branch_no_2:;
            // Re-raise.
            asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_1;
            asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_1;
            asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_1;
            asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_1;

            goto try_except_handler_2;
            branch_end_2:;
        }
        // End of try:
        try_end_1:;
        {
            PyObject *tmp_assign_source_3;
            CHECK_OBJECT( asyncgen_heap->tmp_for_loop_1__iter_value );
            tmp_assign_source_3 = asyncgen_heap->tmp_for_loop_1__iter_value;
            {
                PyObject *old = asyncgen_heap->var_item;
                asyncgen_heap->var_item = tmp_assign_source_3;
                Py_INCREF( asyncgen_heap->var_item );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_expression_name_3;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
            CHECK_OBJECT( asyncgen_heap->var_item );
            tmp_expression_name_3 = asyncgen_heap->var_item;
            Py_INCREF( tmp_expression_name_3 );
            Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), NULL );
            asyncgen->m_yield_return_index = 3;
            return tmp_expression_name_3;
            yield_return_3:
            Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 184;
                asyncgen_heap->type_description_1 = "co";
                goto try_except_handler_2;
            }
            tmp_yield_result_1 = yield_return_value;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 183;
            asyncgen_heap->type_description_1 = "co";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        asyncgen_heap->exception_keeper_type_2 = asyncgen_heap->exception_type;
        asyncgen_heap->exception_keeper_value_2 = asyncgen_heap->exception_value;
        asyncgen_heap->exception_keeper_tb_2 = asyncgen_heap->exception_tb;
        asyncgen_heap->exception_keeper_lineno_2 = asyncgen_heap->exception_lineno;
        asyncgen_heap->exception_type = NULL;
        asyncgen_heap->exception_value = NULL;
        asyncgen_heap->exception_tb = NULL;
        asyncgen_heap->exception_lineno = 0;

        Py_XDECREF( asyncgen_heap->tmp_for_loop_1__iter_value );
        asyncgen_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( asyncgen_heap->tmp_for_loop_1__for_iterator );
        asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_2;
        asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_2;
        asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_2;
        asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        Py_XDECREF( asyncgen_heap->tmp_for_loop_1__iter_value );
        asyncgen_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( asyncgen_heap->tmp_for_loop_1__for_iterator );
        asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;

        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        if ( PyCell_GET( asyncgen->m_closure[0] ) == NULL )
        {

            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "iterable" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 186;
            asyncgen_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_iter_arg_1 = PyCell_GET( asyncgen->m_closure[0] );
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 186;
            asyncgen_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        assert( asyncgen_heap->tmp_for_loop_2__for_iterator == NULL );
        asyncgen_heap->tmp_for_loop_2__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( asyncgen_heap->tmp_for_loop_2__for_iterator );
        tmp_next_source_1 = asyncgen_heap->tmp_for_loop_2__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
                asyncgen_heap->type_description_1 = "co";
                asyncgen_heap->exception_lineno = 186;
                goto try_except_handler_4;
            }
        }

        {
            PyObject *old = asyncgen_heap->tmp_for_loop_2__iter_value;
            asyncgen_heap->tmp_for_loop_2__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( asyncgen_heap->tmp_for_loop_2__iter_value );
        tmp_assign_source_6 = asyncgen_heap->tmp_for_loop_2__iter_value;
        {
            PyObject *old = asyncgen_heap->var_item;
            asyncgen_heap->var_item = tmp_assign_source_6;
            Py_INCREF( asyncgen_heap->var_item );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_4;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_2;
        CHECK_OBJECT( asyncgen_heap->var_item );
        tmp_expression_name_4 = asyncgen_heap->var_item;
        Py_INCREF( tmp_expression_name_4 );
        asyncgen->m_yield_return_index = 4;
        return tmp_expression_name_4;
        yield_return_4:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 187;
            asyncgen_heap->type_description_1 = "co";
            goto try_except_handler_4;
        }
        tmp_yield_result_2 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


        asyncgen_heap->exception_lineno = 186;
        asyncgen_heap->type_description_1 = "co";
        goto try_except_handler_4;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    asyncgen_heap->exception_keeper_type_3 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_3 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_3 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_3 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    Py_XDECREF( asyncgen_heap->tmp_for_loop_2__iter_value );
    asyncgen_heap->tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_2__for_iterator );
    Py_DECREF( asyncgen_heap->tmp_for_loop_2__for_iterator );
    asyncgen_heap->tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_3;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_3;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_3;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( asyncgen->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( asyncgen->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( asyncgen->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( asyncgen_heap->exception_type ) )
    {
        if ( asyncgen_heap->exception_tb == NULL )
        {
            asyncgen_heap->exception_tb = MAKE_TRACEBACK( asyncgen->m_frame, asyncgen_heap->exception_lineno );
        }
        else if ( asyncgen_heap->exception_tb->tb_frame != &asyncgen->m_frame->m_frame )
        {
            asyncgen_heap->exception_tb = ADD_TRACEBACK( asyncgen_heap->exception_tb, asyncgen->m_frame, asyncgen_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)asyncgen->m_frame,
            asyncgen_heap->type_description_1,
            asyncgen->m_closure[0],
            asyncgen_heap->var_item
        );


        // Release cached frame.
        if ( asyncgen->m_frame == cache_m_frame )
        {
            Py_DECREF( asyncgen->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( asyncgen->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( asyncgen->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( asyncgen_heap->tmp_for_loop_2__iter_value );
    asyncgen_heap->tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_2__for_iterator );
    Py_DECREF( asyncgen_heap->tmp_for_loop_2__for_iterator );
    asyncgen_heap->tmp_for_loop_2__for_iterator = NULL;

    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( asyncgen_heap->var_item );
    asyncgen_heap->var_item = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    asyncgen_heap->exception_keeper_type_4 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_4 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_4 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_4 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    Py_XDECREF( asyncgen_heap->var_item );
    asyncgen_heap->var_item = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_4;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_4;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_4;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter );

    function_exception_exit:
    assert( asyncgen_heap->exception_type );
    RESTORE_ERROR_OCCURRED( asyncgen_heap->exception_type, asyncgen_heap->exception_value, asyncgen_heap->exception_tb );
    return NULL;
    function_return_exit:;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter_maker( void )
{
    return Nuitka_Asyncgen_New(
        jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter_context,
        module_jinja2$asyncsupport,
        const_str_plain_auto_aiter,
        NULL,
        codeobj_a5565103ad3eef4b0544670c66e722f4,
        1,
        sizeof(struct jinja2$asyncsupport$$$function_16_auto_aiter$$$asyncgen_1_auto_aiter_locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_17___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_async_iterator = python_pars[ 1 ];
    PyObject *par_undefined = python_pars[ 2 ];
    PyObject *par_after = python_pars[ 3 ];
    PyObject *par_length = python_pars[ 4 ];
    PyObject *par_recurse = python_pars[ 5 ];
    PyObject *par_depth0 = python_pars[ 6 ];
    struct Nuitka_FrameObject *frame_be823818de5369c844e2df5ef6e22965;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_be823818de5369c844e2df5ef6e22965 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_be823818de5369c844e2df5ef6e22965, codeobj_be823818de5369c844e2df5ef6e22965, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_be823818de5369c844e2df5ef6e22965 = cache_frame_be823818de5369c844e2df5ef6e22965;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_be823818de5369c844e2df5ef6e22965 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_be823818de5369c844e2df5ef6e22965 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_LoopContextBase );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LoopContextBase );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LoopContextBase" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 194;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        CHECK_OBJECT( par_undefined );
        tmp_args_element_name_2 = par_undefined;
        CHECK_OBJECT( par_recurse );
        tmp_args_element_name_3 = par_recurse;
        CHECK_OBJECT( par_depth0 );
        tmp_args_element_name_4 = par_depth0;
        frame_be823818de5369c844e2df5ef6e22965->m_frame.f_lineno = 194;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS4( tmp_called_instance_1, const_str_plain___init__, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_async_iterator );
        tmp_assattr_name_1 = par_async_iterator;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__async_iterator, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 195;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_after );
        tmp_assattr_name_2 = par_after;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__after, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_length );
        tmp_assattr_name_3 = par_length;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__length, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be823818de5369c844e2df5ef6e22965 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be823818de5369c844e2df5ef6e22965 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_be823818de5369c844e2df5ef6e22965, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_be823818de5369c844e2df5ef6e22965->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_be823818de5369c844e2df5ef6e22965, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_be823818de5369c844e2df5ef6e22965,
        type_description_1,
        par_self,
        par_async_iterator,
        par_undefined,
        par_after,
        par_length,
        par_recurse,
        par_depth0
    );


    // Release cached frame.
    if ( frame_be823818de5369c844e2df5ef6e22965 == cache_frame_be823818de5369c844e2df5ef6e22965 )
    {
        Py_DECREF( frame_be823818de5369c844e2df5ef6e22965 );
    }
    cache_frame_be823818de5369c844e2df5ef6e22965 = NULL;

    assertFrameObject( frame_be823818de5369c844e2df5ef6e22965 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_17___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_async_iterator );
    Py_DECREF( par_async_iterator );
    par_async_iterator = NULL;

    CHECK_OBJECT( (PyObject *)par_undefined );
    Py_DECREF( par_undefined );
    par_undefined = NULL;

    CHECK_OBJECT( (PyObject *)par_after );
    Py_DECREF( par_after );
    par_after = NULL;

    CHECK_OBJECT( (PyObject *)par_length );
    Py_DECREF( par_length );
    par_length = NULL;

    CHECK_OBJECT( (PyObject *)par_recurse );
    Py_DECREF( par_recurse );
    par_recurse = NULL;

    CHECK_OBJECT( (PyObject *)par_depth0 );
    Py_DECREF( par_depth0 );
    par_depth0 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_async_iterator );
    Py_DECREF( par_async_iterator );
    par_async_iterator = NULL;

    CHECK_OBJECT( (PyObject *)par_undefined );
    Py_DECREF( par_undefined );
    par_undefined = NULL;

    CHECK_OBJECT( (PyObject *)par_after );
    Py_DECREF( par_after );
    par_after = NULL;

    CHECK_OBJECT( (PyObject *)par_length );
    Py_DECREF( par_length );
    par_length = NULL;

    CHECK_OBJECT( (PyObject *)par_recurse );
    Py_DECREF( par_recurse );
    par_recurse = NULL;

    CHECK_OBJECT( (PyObject *)par_depth0 );
    Py_DECREF( par_depth0 );
    par_depth0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_17___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_18_length( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_8aae607047f59b13ee8f15a1f9a3b603;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_8aae607047f59b13ee8f15a1f9a3b603 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8aae607047f59b13ee8f15a1f9a3b603, codeobj_8aae607047f59b13ee8f15a1f9a3b603, module_jinja2$asyncsupport, sizeof(void *) );
    frame_8aae607047f59b13ee8f15a1f9a3b603 = cache_frame_8aae607047f59b13ee8f15a1f9a3b603;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8aae607047f59b13ee8f15a1f9a3b603 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8aae607047f59b13ee8f15a1f9a3b603 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__length );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_361aa6e29b7b6ee9ca4424254bc2ed83;
            frame_8aae607047f59b13ee8f15a1f9a3b603->m_frame.f_lineno = 202;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 202;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__length );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8aae607047f59b13ee8f15a1f9a3b603 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8aae607047f59b13ee8f15a1f9a3b603 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8aae607047f59b13ee8f15a1f9a3b603 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8aae607047f59b13ee8f15a1f9a3b603, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8aae607047f59b13ee8f15a1f9a3b603->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8aae607047f59b13ee8f15a1f9a3b603, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8aae607047f59b13ee8f15a1f9a3b603,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_8aae607047f59b13ee8f15a1f9a3b603 == cache_frame_8aae607047f59b13ee8f15a1f9a3b603 )
    {
        Py_DECREF( frame_8aae607047f59b13ee8f15a1f9a3b603 );
    }
    cache_frame_8aae607047f59b13ee8f15a1f9a3b603 = NULL;

    assertFrameObject( frame_8aae607047f59b13ee8f15a1f9a3b603 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_18_length );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_18_length );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_19___aiter__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9665664a0d99957e317710449ffbcb27;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9665664a0d99957e317710449ffbcb27 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9665664a0d99957e317710449ffbcb27, codeobj_9665664a0d99957e317710449ffbcb27, module_jinja2$asyncsupport, sizeof(void *) );
    frame_9665664a0d99957e317710449ffbcb27 = cache_frame_9665664a0d99957e317710449ffbcb27;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9665664a0d99957e317710449ffbcb27 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9665664a0d99957e317710449ffbcb27 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_AsyncLoopContextIterator );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AsyncLoopContextIterator );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AsyncLoopContextIterator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 207;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        frame_9665664a0d99957e317710449ffbcb27->m_frame.f_lineno = 207;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9665664a0d99957e317710449ffbcb27 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9665664a0d99957e317710449ffbcb27 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9665664a0d99957e317710449ffbcb27 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9665664a0d99957e317710449ffbcb27, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9665664a0d99957e317710449ffbcb27->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9665664a0d99957e317710449ffbcb27, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9665664a0d99957e317710449ffbcb27,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_9665664a0d99957e317710449ffbcb27 == cache_frame_9665664a0d99957e317710449ffbcb27 )
    {
        Py_DECREF( frame_9665664a0d99957e317710449ffbcb27 );
    }
    cache_frame_9665664a0d99957e317710449ffbcb27 = NULL;

    assertFrameObject( frame_9665664a0d99957e317710449ffbcb27 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_19___aiter__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_19___aiter__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_20___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_context = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_be7319c0e8e97b9ff48abee03cc84f34;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_be7319c0e8e97b9ff48abee03cc84f34 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_be7319c0e8e97b9ff48abee03cc84f34, codeobj_be7319c0e8e97b9ff48abee03cc84f34, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    frame_be7319c0e8e97b9ff48abee03cc84f34 = cache_frame_be7319c0e8e97b9ff48abee03cc84f34;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_be7319c0e8e97b9ff48abee03cc84f34 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_be7319c0e8e97b9ff48abee03cc84f34 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_context );
        tmp_assattr_name_1 = par_context;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_context, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 214;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be7319c0e8e97b9ff48abee03cc84f34 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be7319c0e8e97b9ff48abee03cc84f34 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_be7319c0e8e97b9ff48abee03cc84f34, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_be7319c0e8e97b9ff48abee03cc84f34->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_be7319c0e8e97b9ff48abee03cc84f34, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_be7319c0e8e97b9ff48abee03cc84f34,
        type_description_1,
        par_self,
        par_context
    );


    // Release cached frame.
    if ( frame_be7319c0e8e97b9ff48abee03cc84f34 == cache_frame_be7319c0e8e97b9ff48abee03cc84f34 )
    {
        Py_DECREF( frame_be7319c0e8e97b9ff48abee03cc84f34 );
    }
    cache_frame_be7319c0e8e97b9ff48abee03cc84f34 = NULL;

    assertFrameObject( frame_be7319c0e8e97b9ff48abee03cc84f34 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_20___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_20___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_21___aiter__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_self );
    tmp_return_value = par_self;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_21___aiter__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_21___aiter__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncsupport$$$function_22___anext__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext___maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_self;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_22___anext__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_22___anext__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext___locals {
    PyObject *var_ctx;
    PyObject *tmp_inplace_assign_attr_1__end;
    PyObject *tmp_inplace_assign_attr_1__start;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
};

static PyObject *jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext___context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext___locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext___locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_ctx = NULL;
    coroutine_heap->tmp_inplace_assign_attr_1__end = NULL;
    coroutine_heap->tmp_inplace_assign_attr_1__start = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_1122cdfeeadebd59dd5fd2047c35efaf, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 220;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( coroutine->m_closure[0] );
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_context );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 220;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->var_ctx == NULL );
        coroutine_heap->var_ctx = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_source_name_2 = coroutine_heap->var_ctx;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_index0 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 221;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->tmp_inplace_assign_attr_1__start == NULL );
        coroutine_heap->tmp_inplace_assign_attr_1__start = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( coroutine_heap->tmp_inplace_assign_attr_1__start );
        tmp_left_name_1 = coroutine_heap->tmp_inplace_assign_attr_1__start;
        tmp_right_name_1 = const_int_pos_1;
        tmp_assign_source_3 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 221;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_2;
        }
        assert( coroutine_heap->tmp_inplace_assign_attr_1__end == NULL );
        coroutine_heap->tmp_inplace_assign_attr_1__end = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( coroutine_heap->tmp_inplace_assign_attr_1__end );
        tmp_assattr_name_1 = coroutine_heap->tmp_inplace_assign_attr_1__end;
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_assattr_target_1 = coroutine_heap->var_ctx;
        coroutine_heap->tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_index0, tmp_assattr_name_1 );
        if ( coroutine_heap->tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 221;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_inplace_assign_attr_1__end );
    Py_DECREF( coroutine_heap->tmp_inplace_assign_attr_1__end );
    coroutine_heap->tmp_inplace_assign_attr_1__end = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_inplace_assign_attr_1__start );
    Py_DECREF( coroutine_heap->tmp_inplace_assign_attr_1__start );
    coroutine_heap->tmp_inplace_assign_attr_1__start = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_inplace_assign_attr_1__end );
    Py_DECREF( coroutine_heap->tmp_inplace_assign_attr_1__end );
    coroutine_heap->tmp_inplace_assign_attr_1__end = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_inplace_assign_attr_1__start );
    Py_DECREF( coroutine_heap->tmp_inplace_assign_attr_1__start );
    coroutine_heap->tmp_inplace_assign_attr_1__start = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_source_name_3 = coroutine_heap->var_ctx;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__after );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 222;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain__last_iteration );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__last_iteration );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_compexpr_left_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_last_iteration" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 222;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_1;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            coroutine->m_frame->m_frame.f_lineno = 223;
            tmp_raise_type_1 = CALL_FUNCTION_NO_ARGS( PyExc_StopAsyncIteration );
            assert( !(tmp_raise_type_1 == NULL) );
            coroutine_heap->exception_type = tmp_raise_type_1;
            coroutine_heap->exception_lineno = 223;
            RAISE_EXCEPTION_WITH_TYPE( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_source_name_4 = coroutine_heap->var_ctx;
        tmp_assattr_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__current );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 224;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_assattr_target_2 = coroutine_heap->var_ctx;
        coroutine_heap->tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__before, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( coroutine_heap->tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 224;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_source_name_5 = coroutine_heap->var_ctx;
        tmp_assattr_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__after );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 225;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_assattr_target_3 = coroutine_heap->var_ctx;
        coroutine_heap->tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__current, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( coroutine_heap->tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 225;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_assattr_target_4;
        coroutine->m_frame->m_frame.f_lineno = 227;
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_source_name_6 = coroutine_heap->var_ctx;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__async_iterator );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 227;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_4;
        }
        coroutine->m_frame->m_frame.f_lineno = 227;
        tmp_expression_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain___anext__ );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 227;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_4;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 227;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_4;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_source_name_6, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_source_name_6, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 227;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_4;
        }
        tmp_assattr_name_4 = yield_return_value;
        if ( tmp_assattr_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 227;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_assattr_target_4 = coroutine_heap->var_ctx;
        coroutine_heap->tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__after, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( coroutine_heap->tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 227;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_4;
        }
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    coroutine_heap->exception_keeper_type_3 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_3 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_3 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_3 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Preserve existing published exception.
    coroutine_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_type_1 );
    coroutine_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_value_1 );
    coroutine_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_tb_1 );

    if ( coroutine_heap->exception_keeper_tb_3 == NULL )
    {
        coroutine_heap->exception_keeper_tb_3 = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_keeper_lineno_3 );
    }
    else if ( coroutine_heap->exception_keeper_lineno_3 != 0 )
    {
        coroutine_heap->exception_keeper_tb_3 = ADD_TRACEBACK( coroutine_heap->exception_keeper_tb_3, coroutine->m_frame, coroutine_heap->exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &coroutine_heap->exception_keeper_type_3, &coroutine_heap->exception_keeper_value_3, &coroutine_heap->exception_keeper_tb_3 );
    PyException_SetTraceback( coroutine_heap->exception_keeper_value_3, (PyObject *)coroutine_heap->exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &coroutine_heap->exception_keeper_type_3, &coroutine_heap->exception_keeper_value_3, &coroutine_heap->exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_StopAsyncIteration;
        coroutine_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 228;
            coroutine_heap->type_description_1 = "co";
            goto try_except_handler_5;
        }
        tmp_condition_result_2 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assattr_name_5;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_assattr_target_5;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain__last_iteration );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__last_iteration );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_last_iteration" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 229;
                coroutine_heap->type_description_1 = "co";
                goto try_except_handler_5;
            }

            tmp_assattr_name_5 = tmp_mvar_value_2;
            CHECK_OBJECT( coroutine_heap->var_ctx );
            tmp_assattr_target_5 = coroutine_heap->var_ctx;
            coroutine_heap->tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__after, tmp_assattr_name_5 );
            if ( coroutine_heap->tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 229;
                coroutine_heap->type_description_1 = "co";
                goto try_except_handler_5;
            }
        }
        goto branch_end_2;
        branch_no_2:;
        coroutine_heap->tmp_result = RERAISE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
        if (unlikely( coroutine_heap->tmp_result == false ))
        {
            coroutine_heap->exception_lineno = 226;
        }

        if (coroutine_heap->exception_tb && coroutine_heap->exception_tb->tb_frame == &coroutine->m_frame->m_frame) coroutine->m_frame->m_frame.f_lineno = coroutine_heap->exception_tb->tb_lineno;
        coroutine_heap->type_description_1 = "co";
        goto try_except_handler_5;
        branch_end_2:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    coroutine_heap->exception_keeper_type_4 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_4 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_4 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_4 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_1, coroutine_heap->exception_preserved_value_1, coroutine_heap->exception_preserved_tb_1 );
    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_4;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_4;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_4;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_1, coroutine_heap->exception_preserved_value_1, coroutine_heap->exception_preserved_tb_1 );
    goto try_end_3;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext__ );
    return NULL;
    // End of try:
    try_end_3:;
    {
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_source_name_7 = coroutine_heap->var_ctx;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__current );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 230;
            coroutine_heap->type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        coroutine_heap->tmp_return_value = PyTuple_New( 2 );
        PyTuple_SET_ITEM( coroutine_heap->tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( coroutine_heap->var_ctx );
        tmp_tuple_element_1 = coroutine_heap->var_ctx;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( coroutine_heap->tmp_return_value, 1, tmp_tuple_element_1 );
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine_heap->var_ctx
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_ctx );
    Py_DECREF( coroutine_heap->var_ctx );
    coroutine_heap->var_ctx = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_5 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_5 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_5 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_5 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_ctx );
    coroutine_heap->var_ctx = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_5;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_5;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_5;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext__ );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext___maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext___context,
        module_jinja2$asyncsupport,
        const_str_plain___anext__,
        const_str_digest_3726530226521a6996a67c61864a81b0,
        codeobj_1122cdfeeadebd59dd5fd2047c35efaf,
        1,
        sizeof(struct jinja2$asyncsupport$$$function_22___anext__$$$coroutine_1___anext___locals)
    );
}


static PyObject *impl_jinja2$asyncsupport$$$function_23_make_async_loop_context( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_iterable = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_undefined = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_recurse = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_depth0 = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_depth0;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_iterable;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] = par_recurse;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] = par_undefined;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_23_make_async_loop_context );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_iterable );
    Py_DECREF( par_iterable );
    par_iterable = NULL;

    CHECK_OBJECT( (PyObject *)par_undefined );
    Py_DECREF( par_undefined );
    par_undefined = NULL;

    CHECK_OBJECT( (PyObject *)par_recurse );
    Py_DECREF( par_recurse );
    par_recurse = NULL;

    CHECK_OBJECT( (PyObject *)par_depth0 );
    Py_DECREF( par_depth0 );
    par_depth0 = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_iterable );
    Py_DECREF( par_iterable );
    par_iterable = NULL;

    CHECK_OBJECT( (PyObject *)par_undefined );
    Py_DECREF( par_undefined );
    par_undefined = NULL;

    CHECK_OBJECT( (PyObject *)par_recurse );
    Py_DECREF( par_recurse );
    par_recurse = NULL;

    CHECK_OBJECT( (PyObject *)par_depth0 );
    Py_DECREF( par_depth0 );
    par_depth0 = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_23_make_async_loop_context );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context_locals {
    PyObject *var_length;
    PyObject *var_async_iterator;
    PyObject *var_after;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
};

static PyObject *jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context_locals *coroutine_heap = (struct jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_length = NULL;
    coroutine_heap->var_async_iterator = NULL;
    coroutine_heap->var_after = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_140f9e161b20f953b6d1f3bed12c0f47, module_jinja2$asyncsupport, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_len_arg_1;
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "iterable" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 243;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }

        tmp_len_arg_1 = PyCell_GET( coroutine->m_closure[1] );
        tmp_assign_source_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 243;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }
        assert( coroutine_heap->var_length == NULL );
        coroutine_heap->var_length = tmp_assign_source_1;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Preserve existing published exception.
    coroutine_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_type_1 );
    coroutine_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_value_1 );
    coroutine_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_tb_1 );

    if ( coroutine_heap->exception_keeper_tb_1 == NULL )
    {
        coroutine_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_keeper_lineno_1 );
    }
    else if ( coroutine_heap->exception_keeper_lineno_1 != 0 )
    {
        coroutine_heap->exception_keeper_tb_1 = ADD_TRACEBACK( coroutine_heap->exception_keeper_tb_1, coroutine->m_frame, coroutine_heap->exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &coroutine_heap->exception_keeper_type_1, &coroutine_heap->exception_keeper_value_1, &coroutine_heap->exception_keeper_tb_1 );
    PyException_SetTraceback( coroutine_heap->exception_keeper_value_1, (PyObject *)coroutine_heap->exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &coroutine_heap->exception_keeper_type_1, &coroutine_heap->exception_keeper_value_1, &coroutine_heap->exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = const_tuple_type_TypeError_type_AttributeError_tuple;
        coroutine_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 244;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_attribute_name_1;
            if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "iterable" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 245;
                coroutine_heap->type_description_1 = "ccccooo";
                goto try_except_handler_3;
            }

            tmp_source_name_1 = PyCell_GET( coroutine->m_closure[1] );
            tmp_attribute_name_1 = const_str_plain___aiter__;
            coroutine_heap->tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
            if ( coroutine_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 245;
                coroutine_heap->type_description_1 = "ccccooo";
                goto try_except_handler_3;
            }
            tmp_operand_name_1 = ( coroutine_heap->tmp_res != 0 ) ? Py_True : Py_False;
            coroutine_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( coroutine_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 245;
                coroutine_heap->type_description_1 = "ccccooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( coroutine_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_tuple_arg_1;
                if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
                {

                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "iterable" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 246;
                    coroutine_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_3;
                }

                tmp_tuple_arg_1 = PyCell_GET( coroutine->m_closure[1] );
                tmp_assign_source_2 = PySequence_Tuple( tmp_tuple_arg_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 246;
                    coroutine_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = PyCell_GET( coroutine->m_closure[1] );
                    PyCell_SET( coroutine->m_closure[1], tmp_assign_source_2 );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_len_arg_2;
                CHECK_OBJECT( PyCell_GET( coroutine->m_closure[1] ) );
                tmp_len_arg_2 = PyCell_GET( coroutine->m_closure[1] );
                tmp_assign_source_3 = BUILTIN_LEN( tmp_len_arg_2 );
                assert( !(tmp_assign_source_3 == NULL) );
                assert( coroutine_heap->var_length == NULL );
                coroutine_heap->var_length = tmp_assign_source_3;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_assign_source_4;
                tmp_assign_source_4 = Py_None;
                assert( coroutine_heap->var_length == NULL );
                Py_INCREF( tmp_assign_source_4 );
                coroutine_heap->var_length = tmp_assign_source_4;
            }
            branch_end_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        coroutine_heap->tmp_result = RERAISE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
        if (unlikely( coroutine_heap->tmp_result == false ))
        {
            coroutine_heap->exception_lineno = 242;
        }

        if (coroutine_heap->exception_tb && coroutine_heap->exception_tb->tb_frame == &coroutine->m_frame->m_frame) coroutine->m_frame->m_frame.f_lineno = coroutine_heap->exception_tb->tb_lineno;
        coroutine_heap->type_description_1 = "ccccooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_1, coroutine_heap->exception_preserved_value_1, coroutine_heap->exception_preserved_tb_1 );
    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_1, coroutine_heap->exception_preserved_value_1, coroutine_heap->exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context );
    return NULL;
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_auto_aiter );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_aiter );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_aiter" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 250;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "iterable" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 250;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[1] );
        coroutine->m_frame->m_frame.f_lineno = 250;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 250;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->var_async_iterator == NULL );
        coroutine_heap->var_async_iterator = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_instance_1;
        coroutine->m_frame->m_frame.f_lineno = 252;
        CHECK_OBJECT( coroutine_heap->var_async_iterator );
        tmp_called_instance_1 = coroutine_heap->var_async_iterator;
        coroutine->m_frame->m_frame.f_lineno = 252;
        tmp_expression_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain___anext__ );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 252;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_4;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 252;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_4;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 252;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_4;
        }
        tmp_assign_source_6 = yield_return_value;
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 252;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_4;
        }
        assert( coroutine_heap->var_after == NULL );
        coroutine_heap->var_after = tmp_assign_source_6;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    coroutine_heap->exception_keeper_type_3 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_3 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_3 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_3 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Preserve existing published exception.
    coroutine_heap->exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_type_2 );
    coroutine_heap->exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_value_2 );
    coroutine_heap->exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_tb_2 );

    if ( coroutine_heap->exception_keeper_tb_3 == NULL )
    {
        coroutine_heap->exception_keeper_tb_3 = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_keeper_lineno_3 );
    }
    else if ( coroutine_heap->exception_keeper_lineno_3 != 0 )
    {
        coroutine_heap->exception_keeper_tb_3 = ADD_TRACEBACK( coroutine_heap->exception_keeper_tb_3, coroutine->m_frame, coroutine_heap->exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &coroutine_heap->exception_keeper_type_3, &coroutine_heap->exception_keeper_value_3, &coroutine_heap->exception_keeper_tb_3 );
    PyException_SetTraceback( coroutine_heap->exception_keeper_value_3, (PyObject *)coroutine_heap->exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &coroutine_heap->exception_keeper_type_3, &coroutine_heap->exception_keeper_value_3, &coroutine_heap->exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_StopAsyncIteration;
        coroutine_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 253;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_3 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain__last_iteration );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__last_iteration );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_last_iteration" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 254;
                coroutine_heap->type_description_1 = "ccccooo";
                goto try_except_handler_5;
            }

            tmp_assign_source_7 = tmp_mvar_value_2;
            assert( coroutine_heap->var_after == NULL );
            Py_INCREF( tmp_assign_source_7 );
            coroutine_heap->var_after = tmp_assign_source_7;
        }
        goto branch_end_3;
        branch_no_3:;
        coroutine_heap->tmp_result = RERAISE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
        if (unlikely( coroutine_heap->tmp_result == false ))
        {
            coroutine_heap->exception_lineno = 251;
        }

        if (coroutine_heap->exception_tb && coroutine_heap->exception_tb->tb_frame == &coroutine->m_frame->m_frame) coroutine->m_frame->m_frame.f_lineno = coroutine_heap->exception_tb->tb_lineno;
        coroutine_heap->type_description_1 = "ccccooo";
        goto try_except_handler_5;
        branch_end_3:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    coroutine_heap->exception_keeper_type_4 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_4 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_4 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_4 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_2, coroutine_heap->exception_preserved_value_2, coroutine_heap->exception_preserved_tb_2 );
    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_4;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_4;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_4;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_2, coroutine_heap->exception_preserved_value_2, coroutine_heap->exception_preserved_tb_2 );
    goto try_end_3;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context );
    return NULL;
    // End of try:
    try_end_3:;
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_AsyncLoopContext );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AsyncLoopContext );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AsyncLoopContext" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 255;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        CHECK_OBJECT( coroutine_heap->var_async_iterator );
        tmp_args_element_name_2 = coroutine_heap->var_async_iterator;
        if ( PyCell_GET( coroutine->m_closure[3] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "undefined" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 255;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = PyCell_GET( coroutine->m_closure[3] );
        if ( coroutine_heap->var_after == NULL )
        {

            coroutine_heap->exception_type = PyExc_UnboundLocalError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "after" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 255;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_4 = coroutine_heap->var_after;
        if ( coroutine_heap->var_length == NULL )
        {

            coroutine_heap->exception_type = PyExc_UnboundLocalError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "length" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 255;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = coroutine_heap->var_length;
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "recurse" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 255;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_6 = PyCell_GET( coroutine->m_closure[2] );
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "depth0" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 256;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_7 = PyCell_GET( coroutine->m_closure[0] );
        coroutine->m_frame->m_frame.f_lineno = 255;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_2, call_args );
        }

        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 255;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[1],
            coroutine->m_closure[3],
            coroutine->m_closure[2],
            coroutine->m_closure[0],
            coroutine_heap->var_length,
            coroutine_heap->var_async_iterator,
            coroutine_heap->var_after
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( coroutine_heap->var_length );
    coroutine_heap->var_length = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_async_iterator );
    Py_DECREF( coroutine_heap->var_async_iterator );
    coroutine_heap->var_async_iterator = NULL;

    Py_XDECREF( coroutine_heap->var_after );
    coroutine_heap->var_after = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_5 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_5 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_5 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_5 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_length );
    coroutine_heap->var_length = NULL;

    Py_XDECREF( coroutine_heap->var_async_iterator );
    coroutine_heap->var_async_iterator = NULL;

    Py_XDECREF( coroutine_heap->var_after );
    coroutine_heap->var_after = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_5;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_5;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_5;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context_context,
        module_jinja2$asyncsupport,
        const_str_plain_make_async_loop_context,
        NULL,
        codeobj_140f9e161b20f953b6d1f3bed12c0f47,
        4,
        sizeof(struct jinja2$asyncsupport$$$function_23_make_async_loop_context$$$coroutine_1_make_async_loop_context_locals)
    );
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_10_make_module_async( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_10_make_module_async,
        const_str_plain_make_module_async,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8bb0f2bbea2cda8feebec760dde8a8b2,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_11_patch_template(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_11_patch_template,
        const_str_plain_patch_template,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4b55f3a546fa26d3e73b5679f1b183c7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_12_patch_runtime(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_12_patch_runtime,
        const_str_plain_patch_runtime,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1648508166d5a9568d9532ae759f5032,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_13_patch_filters(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_13_patch_filters,
        const_str_plain_patch_filters,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e420348637aa31a4160898b1c19f16b3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_14_patch_all(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_14_patch_all,
        const_str_plain_patch_all,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e1cd052a6439bdb80665802b33a17a24,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_15_auto_await(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_15_auto_await,
        const_str_plain_auto_await,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_23294641a30c5a7bda6b5f6f2ebb7fc3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_16_auto_aiter(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_16_auto_aiter,
        const_str_plain_auto_aiter,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a5565103ad3eef4b0544670c66e722f4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_17___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_17___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_b80c01c7bf5b82290397cb53c270a4f4,
#endif
        codeobj_be823818de5369c844e2df5ef6e22965,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_18_length(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_18_length,
        const_str_plain_length,
#if PYTHON_VERSION >= 300
        const_str_digest_86dc107ababe21ba1e74f24723c00d16,
#endif
        codeobj_8aae607047f59b13ee8f15a1f9a3b603,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_19___aiter__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_19___aiter__,
        const_str_plain___aiter__,
#if PYTHON_VERSION >= 300
        const_str_digest_56383ec153fbb011ff77a421b05c140c,
#endif
        codeobj_9665664a0d99957e317710449ffbcb27,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_1_concat_async(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_1_concat_async,
        const_str_plain_concat_async,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8c0c03b7cd55adf56b63556d65b5edca,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_1_concat_async$$$coroutine_1_concat_async$$$function_1_collect,
        const_str_plain_collect,
#if PYTHON_VERSION >= 300
        const_str_digest_3fa472849f7563cdf43fdd81ca51dc6e,
#endif
        codeobj_5afbf7df3f782aebd2b7ade122e30df6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_20___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_20___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_a1e2dbb21a29bf138fd240dc65f4d45d,
#endif
        codeobj_be7319c0e8e97b9ff48abee03cc84f34,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_21___aiter__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_21___aiter__,
        const_str_plain___aiter__,
#if PYTHON_VERSION >= 300
        const_str_digest_60fb315dc731b29d8eacffa85fd5fbcf,
#endif
        codeobj_a1764b5ae8e154059565156befa51c32,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_22___anext__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_22___anext__,
        const_str_plain___anext__,
#if PYTHON_VERSION >= 300
        const_str_digest_3726530226521a6996a67c61864a81b0,
#endif
        codeobj_1122cdfeeadebd59dd5fd2047c35efaf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_23_make_async_loop_context( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_23_make_async_loop_context,
        const_str_plain_make_async_loop_context,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_140f9e161b20f953b6d1f3bed12c0f47,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_2_generate_async(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_2_generate_async,
        const_str_plain_generate_async,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_023fbd84bceab111ad9a9f207a1ba2dd,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_3_wrap_generate_func(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_3_wrap_generate_func,
        const_str_plain_wrap_generate_func,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9e4b8e20628473adeb29bdd286ae35e5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_1__convert_generator,
        const_str_plain__convert_generator,
#if PYTHON_VERSION >= 300
        const_str_digest_b2d89451c7e47bfda7c99b2c93d722a7,
#endif
        codeobj_9a2a22d4803e75d9e1e3b0d5139bd12f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_2_generate(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_3_wrap_generate_func$$$function_2_generate,
        const_str_plain_generate,
#if PYTHON_VERSION >= 300
        const_str_digest_53ced86d8a41710c1e4df15bc1e68545,
#endif
        codeobj_1b9ac494d333528d8159b0459ddf1c9d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_4_render_async(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_4_render_async,
        const_str_plain_render_async,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0a31ff8c915747d8083b899f002c6e19,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_5_wrap_render_func(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_5_wrap_render_func,
        const_str_plain_wrap_render_func,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_58940d299f4d03c2a228c95099823d78,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_5_wrap_render_func$$$function_1_render(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_5_wrap_render_func$$$function_1_render,
        const_str_plain_render,
#if PYTHON_VERSION >= 300
        const_str_digest_bea5503dcad2fbda54f7f5ef23d14ba2,
#endif
        codeobj_9b377e527174a28f7b8047af594a6948,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_6_wrap_block_reference_call(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_6_wrap_block_reference_call,
        const_str_plain_wrap_block_reference_call,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_64d9569afd0b28fbd639d1f4b39ba5f0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_1_async_call,
        const_str_plain_async_call,
#if PYTHON_VERSION >= 300
        const_str_digest_3ea09c231a2793f33456c0650667083b,
#endif
        codeobj_07e4033d9f21261c918d73ab2d6aa8a8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_2___call__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_6_wrap_block_reference_call$$$function_2___call__,
        const_str_plain___call__,
#if PYTHON_VERSION >= 300
        const_str_digest_49a9c2b6c4e1e6e1700f3e3846a4ff46,
#endif
        codeobj_ea897d3dddf300ea230b292301e5630c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_7_wrap_macro_invoke(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_7_wrap_macro_invoke,
        const_str_plain_wrap_macro_invoke,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bf1c1ebb777926dd7a299ddd72c77a34,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_1_async_invoke,
        const_str_plain_async_invoke,
#if PYTHON_VERSION >= 300
        const_str_digest_1f42321ef9bba5d561ea6c02a7960d6b,
#endif
        codeobj_58492195bad6f50ecacb5696bc24058d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_2__invoke(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_7_wrap_macro_invoke$$$function_2__invoke,
        const_str_plain__invoke,
#if PYTHON_VERSION >= 300
        const_str_digest_c162fb909647330a34e1c7480f74a6a1,
#endif
        codeobj_f75bb9b4aa760f68208e3028ea5e10e5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_8_get_default_module_async(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_8_get_default_module_async,
        const_str_plain_get_default_module_async,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e3e90d4cdae6538e1286fa2ddcca2c7d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_9_wrap_default_module(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_9_wrap_default_module,
        const_str_plain_wrap_default_module,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_18a1cc25b18ba20a6501b40b66a3172d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncsupport$$$function_9_wrap_default_module$$$function_1__get_default_module(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncsupport$$$function_9_wrap_default_module$$$function_1__get_default_module,
        const_str_plain__get_default_module,
#if PYTHON_VERSION >= 300
        const_str_digest_1558c9fe5a9ec837ccc69754c23d4d52,
#endif
        codeobj_5ae50fc21a721b02bd5d43f51df2ad7f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncsupport,
        NULL,
        1
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jinja2$asyncsupport =
{
    PyModuleDef_HEAD_INIT,
    "jinja2.asyncsupport",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jinja2$asyncsupport)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jinja2$asyncsupport)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jinja2$asyncsupport );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jinja2.asyncsupport: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jinja2.asyncsupport: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jinja2.asyncsupport: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjinja2$asyncsupport" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jinja2$asyncsupport = Py_InitModule4(
        "jinja2.asyncsupport",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jinja2$asyncsupport = PyModule_Create( &mdef_jinja2$asyncsupport );
#endif

    moduledict_jinja2$asyncsupport = MODULE_DICT( module_jinja2$asyncsupport );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jinja2$asyncsupport,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jinja2$asyncsupport,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jinja2$asyncsupport,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jinja2$asyncsupport,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jinja2$asyncsupport );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_37aca85c698d791dc4bb639deca6348b, module_jinja2$asyncsupport );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_a8d131fd71ee5a659a302f38cea47f36;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_jinja2$asyncsupport_190 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_5df0c0789b1207f0f1ac5698b782d124_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_5df0c0789b1207f0f1ac5698b782d124_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *locals_jinja2$asyncsupport_210 = NULL;
    struct Nuitka_FrameObject *frame_681557c3bab872442dbaf044cf952344_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_681557c3bab872442dbaf044cf952344_3 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_bffb2332ad84fd41c0c36e13b1e5706e;
        UPDATE_STRING_DICT0( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_a8d131fd71ee5a659a302f38cea47f36 = MAKE_MODULE_FRAME( codeobj_a8d131fd71ee5a659a302f38cea47f36, module_jinja2$asyncsupport );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_a8d131fd71ee5a659a302f38cea47f36 );
    assert( Py_REFCNT( frame_a8d131fd71ee5a659a302f38cea47f36 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_sys;
        tmp_globals_name_1 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 12;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        assert( !(tmp_assign_source_4 == NULL) );
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_asyncio;
        tmp_globals_name_2 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 13;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_asyncio, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_inspect;
        tmp_globals_name_3 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 14;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_inspect, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_functools;
        tmp_globals_name_4 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_update_wrapper_tuple;
        tmp_level_name_4 = const_int_0;
        frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 15;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_update_wrapper );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_update_wrapper, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_6a1b15fc617d9b2615daf846b271eabb;
        tmp_globals_name_5 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_concat_str_plain_internalcode_str_plain_Markup_tuple;
        tmp_level_name_5 = const_int_0;
        frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 17;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_8;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_concat );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_concat, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_internalcode );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_internalcode, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Markup );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_Markup, tmp_assign_source_11 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_50cd10b5aab52d808bd46c35f0566289;
        tmp_globals_name_6 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_TemplateModule_tuple;
        tmp_level_name_6 = const_int_0;
        frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 18;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_TemplateModule );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_TemplateModule, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_38d72d37090218643439302eee0c8cc7;
        tmp_globals_name_7 = (PyObject *)moduledict_jinja2$asyncsupport;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_LoopContextBase_str_plain__last_iteration_tuple;
        tmp_level_name_7 = const_int_0;
        frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 19;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_13;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_LoopContextBase );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_LoopContextBase, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain__last_iteration );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain__last_iteration, tmp_assign_source_15 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_1_concat_async(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_concat_async, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_2_generate_async(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_generate_async, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_3_wrap_generate_func(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_generate_func, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_4_render_async(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_render_async, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_5_wrap_render_func(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_render_func, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_6_wrap_block_reference_call(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_block_reference_call, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_7_wrap_macro_invoke(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_macro_invoke, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_internalcode );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_internalcode );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "internalcode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_args_element_name_1 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_8_get_default_module_async(  );



        frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 115;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_get_default_module_async, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_9_wrap_default_module(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_wrap_default_module, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_false_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_25 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_10_make_module_async( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_make_module_async, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_11_patch_template(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_patch_template, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_12_patch_runtime(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_patch_runtime, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_13_patch_filters(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_patch_filters, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_14_patch_all(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_patch_all, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_15_auto_await(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_auto_await, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_16_auto_aiter(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_auto_aiter, tmp_assign_source_31 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_LoopContextBase );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LoopContextBase );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LoopContextBase" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 190;

            goto try_except_handler_3;
        }

        tmp_tuple_element_1 = tmp_mvar_value_4;
        tmp_assign_source_32 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_32, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_33 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_33;
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_34;
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_3;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_3;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_3;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_35 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_35;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_3;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;

                goto try_except_handler_3;
            }
            tmp_tuple_element_2 = const_str_plain_AsyncLoopContext;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 190;
            tmp_assign_source_36 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;

                goto try_except_handler_3;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_36;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;

                goto try_except_handler_3;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 190;

                    goto try_except_handler_3;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 190;

                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 190;

                    goto try_except_handler_3;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 190;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_3;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_37;
            tmp_assign_source_37 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_37;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_38;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_jinja2$asyncsupport_190 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_37aca85c698d791dc4bb639deca6348b;
        tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_190, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_plain_AsyncLoopContext;
        tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_190, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;

            goto try_except_handler_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_5df0c0789b1207f0f1ac5698b782d124_2, codeobj_5df0c0789b1207f0f1ac5698b782d124, module_jinja2$asyncsupport, sizeof(void *) );
        frame_5df0c0789b1207f0f1ac5698b782d124_2 = cache_frame_5df0c0789b1207f0f1ac5698b782d124_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_5df0c0789b1207f0f1ac5698b782d124_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_5df0c0789b1207f0f1ac5698b782d124_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_none_int_0_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_jinja2$asyncsupport$$$function_17___init__( tmp_defaults_2 );



            tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_190, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_3;
            tmp_res = MAPPING_HAS_ITEM( locals_jinja2$asyncsupport_190, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 199;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_3 = PyObject_GetItem( locals_jinja2$asyncsupport_190, const_str_plain_property );

            if ( tmp_called_name_3 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 199;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 199;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_2 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_18_length(  );



            frame_5df0c0789b1207f0f1ac5698b782d124_2->m_frame.f_lineno = 199;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 199;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_called_name_4 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_3 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_18_length(  );



            frame_5df0c0789b1207f0f1ac5698b782d124_2->m_frame.f_lineno = 199;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 199;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_190, const_str_plain_length, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 199;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jinja2$asyncsupport$$$function_19___aiter__(  );



        tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_190, const_str_plain___aiter__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 206;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_5df0c0789b1207f0f1ac5698b782d124_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_5df0c0789b1207f0f1ac5698b782d124_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_5df0c0789b1207f0f1ac5698b782d124_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_5df0c0789b1207f0f1ac5698b782d124_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_5df0c0789b1207f0f1ac5698b782d124_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_5df0c0789b1207f0f1ac5698b782d124_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_5df0c0789b1207f0f1ac5698b782d124_2 == cache_frame_5df0c0789b1207f0f1ac5698b782d124_2 )
        {
            Py_DECREF( frame_5df0c0789b1207f0f1ac5698b782d124_2 );
        }
        cache_frame_5df0c0789b1207f0f1ac5698b782d124_2 = NULL;

        assertFrameObject( frame_5df0c0789b1207f0f1ac5698b782d124_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_5;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;

                goto try_except_handler_5;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_190, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;

                goto try_except_handler_5;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_39;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_5 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_AsyncLoopContext;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_jinja2$asyncsupport_190;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 190;
            tmp_assign_source_39 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;

                goto try_except_handler_5;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_39;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_38 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_38 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        Py_DECREF( locals_jinja2$asyncsupport_190 );
        locals_jinja2$asyncsupport_190 = NULL;
        goto try_return_handler_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jinja2$asyncsupport_190 );
        locals_jinja2$asyncsupport_190 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 190;
        goto try_except_handler_3;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_AsyncLoopContext, tmp_assign_source_38 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_dircall_arg1_2;
        tmp_dircall_arg1_2 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_40 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_40;
    }
    {
        PyObject *tmp_assign_source_41;
        tmp_assign_source_41 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_41;
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_6;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_6;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_6;
        }
        tmp_condition_result_9 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_6;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_6;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_5:;
        condexpr_end_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_42 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_42;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_6;
        }
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_6;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_43;
            PyObject *tmp_called_name_6;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;

                goto try_except_handler_6;
            }
            tmp_tuple_element_5 = const_str_plain_AsyncLoopContextIterator;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_5 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 210;
            tmp_assign_source_43 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_43 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;

                goto try_except_handler_6;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_43;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;

                goto try_except_handler_6;
            }
            tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_6;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_6 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 210;

                    goto try_except_handler_6;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_6 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 210;

                    goto try_except_handler_6;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_6 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 210;

                    goto try_except_handler_6;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 210;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_6;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_44;
            tmp_assign_source_44 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_44;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_45;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_jinja2$asyncsupport_210 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_37aca85c698d791dc4bb639deca6348b;
        tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_210, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_plain_AsyncLoopContextIterator;
        tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_210, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;

            goto try_except_handler_8;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_681557c3bab872442dbaf044cf952344_3, codeobj_681557c3bab872442dbaf044cf952344, module_jinja2$asyncsupport, sizeof(void *) );
        frame_681557c3bab872442dbaf044cf952344_3 = cache_frame_681557c3bab872442dbaf044cf952344_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_681557c3bab872442dbaf044cf952344_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_681557c3bab872442dbaf044cf952344_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = const_tuple_str_plain_context_tuple;
        tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_210, const_str_plain___slots__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_jinja2$asyncsupport$$$function_20___init__(  );



        tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_210, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_jinja2$asyncsupport$$$function_21___aiter__(  );



        tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_210, const_str_plain___aiter__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_jinja2$asyncsupport$$$function_22___anext__(  );



        tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_210, const_str_plain___anext__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_681557c3bab872442dbaf044cf952344_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_681557c3bab872442dbaf044cf952344_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_681557c3bab872442dbaf044cf952344_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_681557c3bab872442dbaf044cf952344_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_681557c3bab872442dbaf044cf952344_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_681557c3bab872442dbaf044cf952344_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_681557c3bab872442dbaf044cf952344_3 == cache_frame_681557c3bab872442dbaf044cf952344_3 )
        {
            Py_DECREF( frame_681557c3bab872442dbaf044cf952344_3 );
        }
        cache_frame_681557c3bab872442dbaf044cf952344_3 = NULL;

        assertFrameObject( frame_681557c3bab872442dbaf044cf952344_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_8;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            tmp_compexpr_right_2 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;

                goto try_except_handler_8;
            }
            tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_jinja2$asyncsupport_210, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;

                goto try_except_handler_8;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_7 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_7 = const_str_plain_AsyncLoopContextIterator;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_7 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_7 );
            tmp_tuple_element_7 = locals_jinja2$asyncsupport_210;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_a8d131fd71ee5a659a302f38cea47f36->m_frame.f_lineno = 210;
            tmp_assign_source_46 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;

                goto try_except_handler_8;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_46;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_45 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_45 );
        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        Py_DECREF( locals_jinja2$asyncsupport_210 );
        locals_jinja2$asyncsupport_210 = NULL;
        goto try_return_handler_7;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jinja2$asyncsupport_210 );
        locals_jinja2$asyncsupport_210 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jinja2$asyncsupport );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 210;
        goto try_except_handler_6;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_AsyncLoopContextIterator, tmp_assign_source_45 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a8d131fd71ee5a659a302f38cea47f36 );
#endif
    popFrameStack();

    assertFrameObject( frame_a8d131fd71ee5a659a302f38cea47f36 );

    goto frame_no_exception_3;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a8d131fd71ee5a659a302f38cea47f36 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a8d131fd71ee5a659a302f38cea47f36, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a8d131fd71ee5a659a302f38cea47f36->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a8d131fd71ee5a659a302f38cea47f36, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_none_int_0_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_47 = MAKE_FUNCTION_jinja2$asyncsupport$$$function_23_make_async_loop_context( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncsupport, (Nuitka_StringObject *)const_str_plain_make_async_loop_context, tmp_assign_source_47 );
    }

    return MOD_RETURN_VALUE( module_jinja2$asyncsupport );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
