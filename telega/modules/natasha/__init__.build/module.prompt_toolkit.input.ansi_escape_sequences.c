/* Generated code for Python module 'prompt_toolkit.input.ansi_escape_sequences'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$input$ansi_escape_sequences" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$input$ansi_escape_sequences;
PyDictObject *moduledict_prompt_toolkit$input$ansi_escape_sequences;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_cff27188ef75050fc3667312cd5d4bc0;
static PyObject *const_str_chr_21;
extern PyObject *const_str_plain_result;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_07a71e66c22d8f31faf91779ad8c6612;
extern PyObject *const_str_plain_ControlL;
extern PyObject *const_str_plain_ControlY;
static PyObject *const_str_digest_4e7da83bf89ff269292aba8383def151;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_BackTab;
extern PyObject *const_str_chr_30;
extern PyObject *const_str_plain_ControlJ;
static PyObject *const_str_digest_cb639d71eaae980d7ae391093b9c3b2a;
static PyObject *const_str_digest_bee2ae49567cb9050d7a4436343b7933;
static PyObject *const_str_digest_40c81036de1212c99f9ca44aa054df97;
extern PyObject *const_str_plain_F12;
static PyObject *const_str_digest_7d50c114d0c31ace70f4521595eeb145;
extern PyObject *const_str_chr_28;
extern PyObject *const_str_plain_ControlE;
static PyObject *const_str_chr_24;
extern PyObject *const_str_plain_items;
static PyObject *const_str_chr_31;
static PyObject *const_str_digest_2ca855b20812d2c2a4a4e70eb1190e23;
static PyObject *const_str_digest_062e4dd920952e7a7a4a51e9e7fddca3;
extern PyObject *const_tuple_str_plain_Keys_tuple;
extern PyObject *const_str_plain_ControlCircumflex;
static PyObject *const_str_digest_e1ab117ad38a4f32363b686a8410c9d5;
static PyObject *const_str_digest_ab6f3d643f8365fb3bac6fc96a8b7375;
extern PyObject *const_str_plain_F1;
extern PyObject *const_str_chr_0;
static PyObject *const_str_digest_8d32ec3d83390268b84dbc2c621c05bc;
extern PyObject *const_str_plain_ControlDown;
static PyObject *const_str_digest_0a4750ed52a1c31727ee0b12889ebd67;
extern PyObject *const_str_plain_REVERSE_ANSI_SEQUENCES;
extern PyObject *const_str_plain_ControlLeft;
extern PyObject *const_str_digest_76ae19717fa0d569b06c578da9221718;
static PyObject *const_str_digest_a9a7497bb05879a233f96e474d239561;
static PyObject *const_str_digest_c8f27a7f54392ae4a09ee9a266151602;
extern PyObject *const_str_plain_Left;
static PyObject *const_str_digest_32243c0fdd5ee0c73bb689eb2bde2045;
extern PyObject *const_str_plain_ShiftLeft;
static PyObject *const_str_digest_620bdf662e4481b904ab5f3e59086c27;
static PyObject *const_str_digest_73be8c3012f87688236d4b75ed957f35;
static PyObject *const_str_digest_23a95ef0f717a33ca425b2f26055a496;
extern PyObject *const_str_chr_4;
static PyObject *const_str_digest_1ef99b45a0bb33cecf0567b1674bb46c;
extern PyObject *const_str_plain_ControlV;
static PyObject *const_str_digest_13fd536cb3770b7dff6b2f1863eeb53c;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_PageDown;
static PyObject *const_str_digest_e4a4aada2698ea56536cee7a44a860f1;
extern PyObject *const_str_plain_Delete;
extern PyObject *const_str_plain_ControlG;
static PyObject *const_str_chr_22;
extern PyObject *const_str_plain_ControlBackslash;
static PyObject *const_str_digest_2cb59f6fba9e21c488534a537644382f;
extern PyObject *const_str_plain_End;
static PyObject *const_str_digest_e66b04b17e0da4b097384da7401798cd;
extern PyObject *const_str_plain_sequence;
static PyObject *const_str_digest_3f21ef25720ae804c8e773805f76314a;
static PyObject *const_str_digest_8ae5e0cc7ab1e2054da9924ef365791d;
static PyObject *const_str_digest_12cbcda11402fcdddab393c058c0f69e;
static PyObject *const_str_digest_6ab2674fce36c805aa9d8c610de646f0;
extern PyObject *const_str_plain_F9;
static PyObject *const_str_chr_127;
static PyObject *const_str_digest_31d066d6ec77a8b24d805a40b05650fd;
static PyObject *const_str_digest_f603c349ec9d7b266d70c8db9806930f;
extern PyObject *const_str_digest_d84420b7a31ae497b4474c19147bc88d;
extern PyObject *const_str_plain_ControlZ;
static PyObject *const_str_digest_56e622cd76de0e826f57104d3f211364;
extern PyObject *const_str_plain_ControlI;
static PyObject *const_str_digest_2364c66b4b73aba8a46780ea9928bbef;
extern PyObject *const_str_plain_ControlUnderscore;
extern PyObject *const_str_plain_F20;
extern PyObject *const_str_chr_13;
extern PyObject *const_str_plain_F17;
static PyObject *const_str_digest_d935c86a1d788e8e47a876915c403911;
static PyObject *const_str_digest_c1d355aa7a615f4a5c0d2e57a72621a5;
extern PyObject *const_str_plain_ControlX;
extern PyObject *const_str_plain_ScrollDown;
extern PyObject *const_str_plain_Escape;
extern PyObject *const_str_plain_ControlH;
static PyObject *const_str_digest_5de5b2adce51faf092210ff0a12b20ac;
static PyObject *const_str_plain__get_reverse_ansi_sequences;
extern PyObject *const_str_chr_8;
extern PyObject *const_str_plain_keys;
extern PyObject *const_str_plain_ControlO;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_F6;
static PyObject *const_str_digest_b97c39b71e3a68fba9de581ab9e5a6e6;
extern PyObject *const_str_plain_ControlB;
extern PyObject *const_str_chr_1;
static PyObject *const_str_chr_26;
extern PyObject *const_str_plain_ControlRight;
extern PyObject *const_str_plain_ControlT;
extern PyObject *const_str_plain_Keys;
extern PyObject *const_str_chr_7;
extern PyObject *const_str_chr_9;
extern PyObject *const_str_plain_ControlN;
static PyObject *const_str_chr_18;
extern PyObject *const_str_plain_Insert;
extern PyObject *const_str_chr_11;
extern PyObject *const_str_chr_2;
static PyObject *const_str_chr_14;
static PyObject *const_str_chr_17;
static PyObject *const_str_digest_8056f16124e7d0ff823529101047b0ea;
extern PyObject *const_str_plain_ANSI_SEQUENCES;
static PyObject *const_str_digest_dc18a06b80af74068c7974fa2e470651;
static PyObject *const_str_digest_ee4e16525301f735f93e646c6c42daec;
static PyObject *const_str_digest_8f22af8a1de8225021212383433de609;
static PyObject *const_str_digest_ba9fd5f52ba66ab48d226dc44f96fb84;
extern PyObject *const_str_plain_F3;
static PyObject *const_str_digest_01c81176a1a2b7a651333f3fc4e04bc7;
extern PyObject *const_str_plain_ControlU;
extern PyObject *const_str_plain_F23;
static PyObject *const_str_digest_03f22995d6374e9c04ae343b798f49a9;
static PyObject *const_str_digest_a893ab180602a6c3d02cc0a97b2b0fc2;
static PyObject *const_str_digest_dc41115cfc7281b49f423502e3db8b18;
static PyObject *const_str_digest_e063fd0c602e85c7b30a81421c09255a;
extern PyObject *const_str_plain_F7;
extern PyObject *const_str_plain_Right;
extern PyObject *const_str_plain___all__;
static PyObject *const_str_digest_10e19d55847a00809fd5ffd896b81aff;
extern PyObject *const_str_chr_12;
extern PyObject *const_str_plain_ControlC;
static PyObject *const_str_digest_19b8e8e1cfa33db9cfff601f637bdbda;
extern PyObject *const_str_plain_ShiftDelete;
static PyObject *const_str_digest_d641c0258f07d13a0cd660b1a5aca6db;
static PyObject *const_str_digest_95a4e7fd4e59cff6239485de1ad0e5c7;
static PyObject *const_str_digest_a7d3e33968f5a62c32c2139201127de4;
extern PyObject *const_str_plain_F19;
extern PyObject *const_str_chr_27;
static PyObject *const_str_digest_c56c5c9fd4919c9e8a403467a2a24736;
static PyObject *const_str_digest_beb89f81677925e876a6ee02d4a7bfa5;
static PyObject *const_str_digest_34068089dc6749b64bdfa3a7fca59061;
extern PyObject *const_str_digest_2059ac4e7a1d81406714c1266156f1ee;
static PyObject *const_str_digest_2767cdf3369330764ad399a482d2b929;
extern PyObject *const_str_plain_F11;
extern PyObject *const_str_plain_F24;
static PyObject *const_str_digest_058260aba72117ad57ad4403fdce6a8e;
static PyObject *const_str_digest_0189d4926f5491d9b828b71ec2427458;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_ControlP;
static PyObject *const_str_chr_19;
extern PyObject *const_str_plain_ControlK;
static PyObject *const_str_digest_432e592f9e58f4aa3e445792576b6e30;
static PyObject *const_str_digest_03598a062f93f985b73d6d0c466b12e0;
static PyObject *const_str_digest_032d49d6628734a7e58b53f5b93ed271;
static PyObject *const_str_digest_822ed9d7bc2ba5596ae8f9a852080479;
static PyObject *const_str_chr_23;
static PyObject *const_str_digest_ec10944927cf01c741f92f8dfb54ab5c;
static PyObject *const_str_chr_6;
static PyObject *const_str_digest_97a85692d546bcfcb4a5b6de8650005d;
static PyObject *const_tuple_str_plain_result_str_plain_sequence_str_plain_key_tuple;
extern PyObject *const_str_plain_ControlS;
static PyObject *const_str_digest_71ad11d69c1ea2207cf20a5df3f02d74;
static PyObject *const_str_digest_020a56b5559e369a659c41d9479d25eb;
static PyObject *const_str_chr_25;
extern PyObject *const_str_plain_ControlM;
static PyObject *const_str_digest_bb785395327ee29dbd8541c485eb65e7;
static PyObject *const_str_digest_de83c66b8e32f7c106b3da21a0e8f37e;
static PyObject *const_str_chr_3;
extern PyObject *const_str_plain_ControlDelete;
extern PyObject *const_str_plain_F8;
static PyObject *const_str_chr_15;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_0a9eb65db2809423077341b09bb9bf67;
extern PyObject *const_str_plain_F10;
static PyObject *const_str_digest_0df4cc8acea8a24f0ce17edb3f4992be;
extern PyObject *const_str_plain_F22;
static PyObject *const_str_digest_3e707169fb81bec7d62a516863d91f23;
extern PyObject *const_str_plain_ShiftDown;
extern PyObject *const_str_plain_F5;
static PyObject *const_str_digest_b929d89ddb8b76a9ddaa15631297cae1;
static PyObject *const_str_digest_5d94c1618d14c77c282d74997afa1b74;
extern PyObject *const_str_plain_Home;
extern PyObject *const_str_plain_F21;
static PyObject *const_str_digest_9510cccc23d088e9fb7184961464a33a;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_ShiftUp;
static PyObject *const_str_chr_5;
extern PyObject *const_str_plain_F14;
static PyObject *const_str_digest_a7f26ffa6da53b4707170a0ce66f1af6;
extern PyObject *const_str_plain_ControlW;
static PyObject *const_str_digest_30d7db835e6623e49486c13a0ef7bf3b;
extern PyObject *const_str_plain_ControlF;
extern PyObject *const_str_plain_ControlQ;
extern PyObject *const_str_plain_tuple;
extern PyObject *const_str_plain_PageUp;
static PyObject *const_str_digest_fd2edef1a1fff32a7aed8d7d47e6a776;
extern PyObject *const_str_plain_F4;
extern PyObject *const_str_plain_Ignore;
extern PyObject *const_str_plain_key;
extern PyObject *const_str_newline;
static PyObject *const_str_digest_9b3eaf58399f683697373ee1daa6aa28;
extern PyObject *const_str_plain_ControlSquareClose;
static PyObject *const_str_digest_72bb63f38d5716ecd080e28067560245;
static PyObject *const_str_digest_28534181036c43d70a84ccb606b5cd8f;
extern PyObject *const_str_plain_F13;
extern PyObject *const_str_plain_ControlA;
extern PyObject *const_str_plain_ShiftRight;
static PyObject *const_str_digest_8582c6351090c305eddd1c2a4cc49b2c;
static PyObject *const_list_str_plain_ANSI_SEQUENCES_str_plain_REVERSE_ANSI_SEQUENCES_list;
static PyObject *const_str_digest_49a04bf601200952b6d59882eac40d72;
static PyObject *const_str_digest_8496eda9d2db69e6bc02e9a70a857e4f;
extern PyObject *const_str_plain_ControlD;
static PyObject *const_str_digest_e9e0af174623e18f61e63518bbc6feec;
extern PyObject *const_str_plain_ControlUp;
static PyObject *const_str_digest_999606f940dae79c7c12722f054004a5;
extern PyObject *const_str_plain_ControlAt;
static PyObject *const_str_chr_20;
static PyObject *const_str_chr_16;
extern PyObject *const_str_plain_F16;
extern PyObject *const_str_plain_BracketedPaste;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_Down;
static PyObject *const_str_digest_c63467252ff92c7f8c1a3601e8401fac;
extern PyObject *const_str_plain_ScrollUp;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_Up;
static PyObject *const_str_digest_5b8f61ade02535a51406f5ee0eaf4b6d;
static PyObject *const_str_digest_81a969b9f09bca16e6c1fa6c32ff52ec;
extern PyObject *const_str_chr_29;
static PyObject *const_str_digest_855cbefdb5107659e57086dd355ba22b;
static PyObject *const_str_digest_acfa066df9d1ac14be92ac4f20bc315f;
extern PyObject *const_str_plain_ControlR;
extern PyObject *const_str_plain_F18;
static PyObject *const_str_digest_28eb87ecf432c77c3f1bc6cfad381297;
extern PyObject *const_str_plain_F15;
extern PyObject *const_str_plain_F2;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_cff27188ef75050fc3667312cd5d4bc0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663199 ], 3, 0 );
    const_str_chr_21 = UNSTREAM_STRING_ASCII( &constant_bin[ 46170 ], 1, 0 );
    const_str_digest_07a71e66c22d8f31faf91779ad8c6612 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663202 ], 6, 0 );
    const_str_digest_4e7da83bf89ff269292aba8383def151 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663208 ], 4, 0 );
    const_str_digest_cb639d71eaae980d7ae391093b9c3b2a = UNSTREAM_STRING_ASCII( &constant_bin[ 4663212 ], 6, 0 );
    const_str_digest_bee2ae49567cb9050d7a4436343b7933 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663218 ], 4, 0 );
    const_str_digest_40c81036de1212c99f9ca44aa054df97 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663222 ], 3, 0 );
    const_str_digest_7d50c114d0c31ace70f4521595eeb145 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663225 ], 7, 0 );
    const_str_chr_24 = UNSTREAM_STRING_ASCII( &constant_bin[ 1785 ], 1, 0 );
    const_str_chr_31 = UNSTREAM_STRING_ASCII( &constant_bin[ 2165 ], 1, 0 );
    const_str_digest_2ca855b20812d2c2a4a4e70eb1190e23 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663232 ], 5, 0 );
    const_str_digest_062e4dd920952e7a7a4a51e9e7fddca3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663237 ], 6, 0 );
    const_str_digest_e1ab117ad38a4f32363b686a8410c9d5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663243 ], 4, 0 );
    const_str_digest_ab6f3d643f8365fb3bac6fc96a8b7375 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663247 ], 5, 0 );
    const_str_digest_8d32ec3d83390268b84dbc2c621c05bc = UNSTREAM_STRING_ASCII( &constant_bin[ 4663252 ], 7, 0 );
    const_str_digest_0a4750ed52a1c31727ee0b12889ebd67 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663259 ], 6, 0 );
    const_str_digest_a9a7497bb05879a233f96e474d239561 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663265 ], 7, 0 );
    const_str_digest_c8f27a7f54392ae4a09ee9a266151602 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663272 ], 6, 0 );
    const_str_digest_32243c0fdd5ee0c73bb689eb2bde2045 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663278 ], 6, 0 );
    const_str_digest_620bdf662e4481b904ab5f3e59086c27 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663284 ], 6, 0 );
    const_str_digest_73be8c3012f87688236d4b75ed957f35 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663290 ], 5, 0 );
    const_str_digest_23a95ef0f717a33ca425b2f26055a496 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663295 ], 4, 0 );
    const_str_digest_1ef99b45a0bb33cecf0567b1674bb46c = UNSTREAM_STRING_ASCII( &constant_bin[ 4663299 ], 3, 0 );
    const_str_digest_13fd536cb3770b7dff6b2f1863eeb53c = UNSTREAM_STRING_ASCII( &constant_bin[ 4663302 ], 5, 0 );
    const_str_digest_e4a4aada2698ea56536cee7a44a860f1 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663307 ], 4, 0 );
    const_str_chr_22 = UNSTREAM_STRING_ASCII( &constant_bin[ 71942 ], 1, 0 );
    const_str_digest_2cb59f6fba9e21c488534a537644382f = UNSTREAM_STRING_ASCII( &constant_bin[ 4663311 ], 4, 0 );
    const_str_digest_e66b04b17e0da4b097384da7401798cd = UNSTREAM_STRING_ASCII( &constant_bin[ 4663315 ], 5, 0 );
    const_str_digest_3f21ef25720ae804c8e773805f76314a = UNSTREAM_STRING_ASCII( &constant_bin[ 4663320 ], 51, 0 );
    const_str_digest_8ae5e0cc7ab1e2054da9924ef365791d = UNSTREAM_STRING_ASCII( &constant_bin[ 4663371 ], 3, 0 );
    const_str_digest_12cbcda11402fcdddab393c058c0f69e = UNSTREAM_STRING_ASCII( &constant_bin[ 4663374 ], 4, 0 );
    const_str_digest_6ab2674fce36c805aa9d8c610de646f0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663378 ], 5, 0 );
    const_str_chr_127 = UNSTREAM_STRING_ASCII( &constant_bin[ 72467 ], 1, 0 );
    const_str_digest_31d066d6ec77a8b24d805a40b05650fd = UNSTREAM_STRING_ASCII( &constant_bin[ 4663383 ], 6, 0 );
    const_str_digest_f603c349ec9d7b266d70c8db9806930f = UNSTREAM_STRING_ASCII( &constant_bin[ 4663389 ], 6, 0 );
    const_str_digest_56e622cd76de0e826f57104d3f211364 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663395 ], 3, 0 );
    const_str_digest_2364c66b4b73aba8a46780ea9928bbef = UNSTREAM_STRING_ASCII( &constant_bin[ 4663398 ], 5, 0 );
    const_str_digest_d935c86a1d788e8e47a876915c403911 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663403 ], 7, 0 );
    const_str_digest_c1d355aa7a615f4a5c0d2e57a72621a5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663410 ], 4, 0 );
    const_str_digest_5de5b2adce51faf092210ff0a12b20ac = UNSTREAM_STRING_ASCII( &constant_bin[ 4663414 ], 3, 0 );
    const_str_plain__get_reverse_ansi_sequences = UNSTREAM_STRING_ASCII( &constant_bin[ 4663417 ], 27, 1 );
    const_str_digest_b97c39b71e3a68fba9de581ab9e5a6e6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663444 ], 5, 0 );
    const_str_chr_26 = UNSTREAM_STRING_ASCII( &constant_bin[ 71962 ], 1, 0 );
    const_str_chr_18 = UNSTREAM_STRING_ASCII( &constant_bin[ 763 ], 1, 0 );
    const_str_chr_14 = UNSTREAM_STRING_ASCII( &constant_bin[ 21 ], 1, 0 );
    const_str_chr_17 = UNSTREAM_STRING_ASCII( &constant_bin[ 380 ], 1, 0 );
    const_str_digest_8056f16124e7d0ff823529101047b0ea = UNSTREAM_STRING_ASCII( &constant_bin[ 4663449 ], 7, 0 );
    const_str_digest_dc18a06b80af74068c7974fa2e470651 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663456 ], 5, 0 );
    const_str_digest_ee4e16525301f735f93e646c6c42daec = UNSTREAM_STRING_ASCII( &constant_bin[ 4663461 ], 6, 0 );
    const_str_digest_8f22af8a1de8225021212383433de609 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663467 ], 6, 0 );
    const_str_digest_ba9fd5f52ba66ab48d226dc44f96fb84 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663473 ], 6, 0 );
    const_str_digest_01c81176a1a2b7a651333f3fc4e04bc7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663479 ], 5, 0 );
    const_str_digest_03f22995d6374e9c04ae343b798f49a9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663484 ], 7, 0 );
    const_str_digest_a893ab180602a6c3d02cc0a97b2b0fc2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663491 ], 5, 0 );
    const_str_digest_dc41115cfc7281b49f423502e3db8b18 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663496 ], 4, 0 );
    const_str_digest_e063fd0c602e85c7b30a81421c09255a = UNSTREAM_STRING_ASCII( &constant_bin[ 4663500 ], 99, 0 );
    const_str_digest_10e19d55847a00809fd5ffd896b81aff = UNSTREAM_STRING_ASCII( &constant_bin[ 4663599 ], 7, 0 );
    const_str_digest_19b8e8e1cfa33db9cfff601f637bdbda = UNSTREAM_STRING_ASCII( &constant_bin[ 4663606 ], 5, 0 );
    const_str_digest_d641c0258f07d13a0cd660b1a5aca6db = UNSTREAM_STRING_ASCII( &constant_bin[ 4663611 ], 5, 0 );
    const_str_digest_95a4e7fd4e59cff6239485de1ad0e5c7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663616 ], 3, 0 );
    const_str_digest_a7d3e33968f5a62c32c2139201127de4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663619 ], 6, 0 );
    const_str_digest_c56c5c9fd4919c9e8a403467a2a24736 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663625 ], 3, 0 );
    const_str_digest_beb89f81677925e876a6ee02d4a7bfa5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663628 ], 6, 0 );
    const_str_digest_34068089dc6749b64bdfa3a7fca59061 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663634 ], 4, 0 );
    const_str_digest_2767cdf3369330764ad399a482d2b929 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663638 ], 3, 0 );
    const_str_digest_058260aba72117ad57ad4403fdce6a8e = UNSTREAM_STRING_ASCII( &constant_bin[ 4663641 ], 5, 0 );
    const_str_digest_0189d4926f5491d9b828b71ec2427458 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663646 ], 6, 0 );
    const_str_chr_19 = UNSTREAM_STRING_ASCII( &constant_bin[ 53 ], 1, 0 );
    const_str_digest_432e592f9e58f4aa3e445792576b6e30 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663652 ], 5, 0 );
    const_str_digest_03598a062f93f985b73d6d0c466b12e0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663657 ], 45, 0 );
    const_str_digest_032d49d6628734a7e58b53f5b93ed271 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663702 ], 7, 0 );
    const_str_digest_822ed9d7bc2ba5596ae8f9a852080479 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663709 ], 6, 0 );
    const_str_chr_23 = UNSTREAM_STRING_ASCII( &constant_bin[ 71947 ], 1, 0 );
    const_str_digest_ec10944927cf01c741f92f8dfb54ab5c = UNSTREAM_STRING_ASCII( &constant_bin[ 4663715 ], 6, 0 );
    const_str_chr_6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1768 ], 1, 0 );
    const_str_digest_97a85692d546bcfcb4a5b6de8650005d = UNSTREAM_STRING_ASCII( &constant_bin[ 4663721 ], 87, 0 );
    const_tuple_str_plain_result_str_plain_sequence_str_plain_key_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_result_str_plain_sequence_str_plain_key_tuple, 0, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    PyTuple_SET_ITEM( const_tuple_str_plain_result_str_plain_sequence_str_plain_key_tuple, 1, const_str_plain_sequence ); Py_INCREF( const_str_plain_sequence );
    PyTuple_SET_ITEM( const_tuple_str_plain_result_str_plain_sequence_str_plain_key_tuple, 2, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    const_str_digest_71ad11d69c1ea2207cf20a5df3f02d74 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663808 ], 4, 0 );
    const_str_digest_020a56b5559e369a659c41d9479d25eb = UNSTREAM_STRING_ASCII( &constant_bin[ 4663812 ], 5, 0 );
    const_str_chr_25 = UNSTREAM_STRING_ASCII( &constant_bin[ 65361 ], 1, 0 );
    const_str_digest_bb785395327ee29dbd8541c485eb65e7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663817 ], 4, 0 );
    const_str_digest_de83c66b8e32f7c106b3da21a0e8f37e = UNSTREAM_STRING_ASCII( &constant_bin[ 4663821 ], 6, 0 );
    const_str_chr_3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1716 ], 1, 0 );
    const_str_chr_15 = UNSTREAM_STRING_ASCII( &constant_bin[ 191 ], 1, 0 );
    const_str_digest_0a9eb65db2809423077341b09bb9bf67 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663827 ], 3, 0 );
    const_str_digest_0df4cc8acea8a24f0ce17edb3f4992be = UNSTREAM_STRING_ASCII( &constant_bin[ 4663830 ], 5, 0 );
    const_str_digest_3e707169fb81bec7d62a516863d91f23 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663835 ], 5, 0 );
    const_str_digest_b929d89ddb8b76a9ddaa15631297cae1 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663840 ], 6, 0 );
    const_str_digest_5d94c1618d14c77c282d74997afa1b74 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663846 ], 3, 0 );
    const_str_digest_9510cccc23d088e9fb7184961464a33a = UNSTREAM_STRING_ASCII( &constant_bin[ 4663849 ], 6, 0 );
    const_str_chr_5 = UNSTREAM_STRING_ASCII( &constant_bin[ 1763 ], 1, 0 );
    const_str_digest_a7f26ffa6da53b4707170a0ce66f1af6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663855 ], 4, 0 );
    const_str_digest_30d7db835e6623e49486c13a0ef7bf3b = UNSTREAM_STRING_ASCII( &constant_bin[ 4663859 ], 3, 0 );
    const_str_digest_fd2edef1a1fff32a7aed8d7d47e6a776 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663862 ], 3, 0 );
    const_str_digest_9b3eaf58399f683697373ee1daa6aa28 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663865 ], 3, 0 );
    const_str_digest_72bb63f38d5716ecd080e28067560245 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663868 ], 5, 0 );
    const_str_digest_28534181036c43d70a84ccb606b5cd8f = UNSTREAM_STRING_ASCII( &constant_bin[ 4663873 ], 3, 0 );
    const_str_digest_8582c6351090c305eddd1c2a4cc49b2c = UNSTREAM_STRING_ASCII( &constant_bin[ 4663876 ], 4, 0 );
    const_list_str_plain_ANSI_SEQUENCES_str_plain_REVERSE_ANSI_SEQUENCES_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_ANSI_SEQUENCES_str_plain_REVERSE_ANSI_SEQUENCES_list, 0, const_str_plain_ANSI_SEQUENCES ); Py_INCREF( const_str_plain_ANSI_SEQUENCES );
    PyList_SET_ITEM( const_list_str_plain_ANSI_SEQUENCES_str_plain_REVERSE_ANSI_SEQUENCES_list, 1, const_str_plain_REVERSE_ANSI_SEQUENCES ); Py_INCREF( const_str_plain_REVERSE_ANSI_SEQUENCES );
    const_str_digest_49a04bf601200952b6d59882eac40d72 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663880 ], 4, 0 );
    const_str_digest_8496eda9d2db69e6bc02e9a70a857e4f = UNSTREAM_STRING_ASCII( &constant_bin[ 4663884 ], 4, 0 );
    const_str_digest_e9e0af174623e18f61e63518bbc6feec = UNSTREAM_STRING_ASCII( &constant_bin[ 4663888 ], 5, 0 );
    const_str_digest_999606f940dae79c7c12722f054004a5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663893 ], 5, 0 );
    const_str_chr_20 = UNSTREAM_STRING_ASCII( &constant_bin[ 57957 ], 1, 0 );
    const_str_chr_16 = UNSTREAM_STRING_ASCII( &constant_bin[ 548 ], 1, 0 );
    const_str_digest_c63467252ff92c7f8c1a3601e8401fac = UNSTREAM_STRING_ASCII( &constant_bin[ 4663898 ], 3, 0 );
    const_str_digest_5b8f61ade02535a51406f5ee0eaf4b6d = UNSTREAM_STRING_ASCII( &constant_bin[ 4663901 ], 5, 0 );
    const_str_digest_81a969b9f09bca16e6c1fa6c32ff52ec = UNSTREAM_STRING_ASCII( &constant_bin[ 4663328 ], 42, 0 );
    const_str_digest_855cbefdb5107659e57086dd355ba22b = UNSTREAM_STRING_ASCII( &constant_bin[ 4663906 ], 3, 0 );
    const_str_digest_acfa066df9d1ac14be92ac4f20bc315f = UNSTREAM_STRING_ASCII( &constant_bin[ 4663909 ], 4, 0 );
    const_str_digest_28eb87ecf432c77c3f1bc6cfad381297 = UNSTREAM_STRING_ASCII( &constant_bin[ 4663913 ], 3, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$input$ansi_escape_sequences( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_4d437a8d6dae6793ee53855afd1ebcd3;
static PyCodeObject *codeobj_59fadb902ffdf570020c7c9637fc4496;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_03598a062f93f985b73d6d0c466b12e0 );
    codeobj_4d437a8d6dae6793ee53855afd1ebcd3 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_3f21ef25720ae804c8e773805f76314a, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_59fadb902ffdf570020c7c9637fc4496 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_reverse_ansi_sequences, 181, const_tuple_str_plain_result_str_plain_sequence_str_plain_key_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_prompt_toolkit$input$ansi_escape_sequences$$$function_1__get_reverse_ansi_sequences(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$input$ansi_escape_sequences$$$function_1__get_reverse_ansi_sequences( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_result = NULL;
    PyObject *var_sequence = NULL;
    PyObject *var_key = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_59fadb902ffdf570020c7c9637fc4496;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_59fadb902ffdf570020c7c9637fc4496 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyDict_New();
        assert( var_result == NULL );
        var_result = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_59fadb902ffdf570020c7c9637fc4496, codeobj_59fadb902ffdf570020c7c9637fc4496, module_prompt_toolkit$input$ansi_escape_sequences, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_59fadb902ffdf570020c7c9637fc4496 = cache_frame_59fadb902ffdf570020c7c9637fc4496;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_59fadb902ffdf570020c7c9637fc4496 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_59fadb902ffdf570020c7c9637fc4496 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_ANSI_SEQUENCES );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ANSI_SEQUENCES );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ANSI_SEQUENCES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 188;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_59fadb902ffdf570020c7c9637fc4496->m_frame.f_lineno = 188;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                exception_lineno = 188;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooo";
            exception_lineno = 188;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooo";
            exception_lineno = 188;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooo";
                    exception_lineno = 188;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooo";
            exception_lineno = 188;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_sequence;
            var_sequence = tmp_assign_source_7;
            Py_INCREF( var_sequence );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_key;
            var_key = tmp_assign_source_8;
            Py_INCREF( var_key );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( var_key );
        tmp_isinstance_inst_1 = var_key;
        tmp_isinstance_cls_1 = (PyObject *)&PyTuple_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_key_name_1;
            PyObject *tmp_dict_name_1;
            CHECK_OBJECT( var_key );
            tmp_key_name_1 = var_key;
            CHECK_OBJECT( var_result );
            tmp_dict_name_1 = var_result;
            tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            CHECK_OBJECT( var_sequence );
            tmp_dictset_value = var_sequence;
            CHECK_OBJECT( var_result );
            tmp_dictset_dict = var_result;
            CHECK_OBJECT( var_key );
            tmp_dictset_key = var_key;
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 191;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            branch_no_2:;
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 188;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_59fadb902ffdf570020c7c9637fc4496 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_59fadb902ffdf570020c7c9637fc4496 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_59fadb902ffdf570020c7c9637fc4496, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_59fadb902ffdf570020c7c9637fc4496->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_59fadb902ffdf570020c7c9637fc4496, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_59fadb902ffdf570020c7c9637fc4496,
        type_description_1,
        var_result,
        var_sequence,
        var_key
    );


    // Release cached frame.
    if ( frame_59fadb902ffdf570020c7c9637fc4496 == cache_frame_59fadb902ffdf570020c7c9637fc4496 )
    {
        Py_DECREF( frame_59fadb902ffdf570020c7c9637fc4496 );
    }
    cache_frame_59fadb902ffdf570020c7c9637fc4496 = NULL;

    assertFrameObject( frame_59fadb902ffdf570020c7c9637fc4496 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_result );
    tmp_return_value = var_result;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$ansi_escape_sequences$$$function_1__get_reverse_ansi_sequences );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_sequence );
    var_sequence = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_sequence );
    var_sequence = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$ansi_escape_sequences$$$function_1__get_reverse_ansi_sequences );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$input$ansi_escape_sequences$$$function_1__get_reverse_ansi_sequences(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$input$ansi_escape_sequences$$$function_1__get_reverse_ansi_sequences,
        const_str_plain__get_reverse_ansi_sequences,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_59fadb902ffdf570020c7c9637fc4496,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$input$ansi_escape_sequences,
        const_str_digest_e063fd0c602e85c7b30a81421c09255a,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$input$ansi_escape_sequences =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.input.ansi_escape_sequences",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$input$ansi_escape_sequences)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$input$ansi_escape_sequences)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$input$ansi_escape_sequences );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.input.ansi_escape_sequences: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.input.ansi_escape_sequences: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.input.ansi_escape_sequences: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$input$ansi_escape_sequences" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$input$ansi_escape_sequences = Py_InitModule4(
        "prompt_toolkit.input.ansi_escape_sequences",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$input$ansi_escape_sequences = PyModule_Create( &mdef_prompt_toolkit$input$ansi_escape_sequences );
#endif

    moduledict_prompt_toolkit$input$ansi_escape_sequences = MODULE_DICT( module_prompt_toolkit$input$ansi_escape_sequences );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$input$ansi_escape_sequences,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$input$ansi_escape_sequences,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$input$ansi_escape_sequences,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$input$ansi_escape_sequences,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$input$ansi_escape_sequences );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_81a969b9f09bca16e6c1fa6c32ff52ec, module_prompt_toolkit$input$ansi_escape_sequences );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_4d437a8d6dae6793ee53855afd1ebcd3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_97a85692d546bcfcb4a5b6de8650005d;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_4d437a8d6dae6793ee53855afd1ebcd3 = MAKE_MODULE_FRAME( codeobj_4d437a8d6dae6793ee53855afd1ebcd3, module_prompt_toolkit$input$ansi_escape_sequences );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_4d437a8d6dae6793ee53855afd1ebcd3 );
    assert( Py_REFCNT( frame_4d437a8d6dae6793ee53855afd1ebcd3 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_4d437a8d6dae6793ee53855afd1ebcd3->m_frame.f_lineno = 5;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_keys;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$input$ansi_escape_sequences;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Keys_tuple;
        tmp_level_name_1 = const_int_pos_2;
        frame_4d437a8d6dae6793ee53855afd1ebcd3->m_frame.f_lineno = 6;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_5 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_prompt_toolkit$input$ansi_escape_sequences,
                const_str_plain_Keys,
                const_int_pos_2
            );
        }
        else
        {
           tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Keys );
        }

        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = LIST_COPY( const_list_str_plain_ANSI_SEQUENCES_str_plain_REVERSE_ANSI_SEQUENCES_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_source_name_13;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_source_name_15;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_source_name_16;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_source_name_17;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_source_name_18;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_source_name_19;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_source_name_20;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_source_name_21;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_dict_key_22;
        PyObject *tmp_dict_value_22;
        PyObject *tmp_source_name_22;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_source_name_23;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        PyObject *tmp_source_name_24;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        PyObject *tmp_source_name_25;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_source_name_26;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        PyObject *tmp_source_name_27;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_dict_key_28;
        PyObject *tmp_dict_value_28;
        PyObject *tmp_source_name_28;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_dict_key_29;
        PyObject *tmp_dict_value_29;
        PyObject *tmp_source_name_29;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_dict_key_30;
        PyObject *tmp_dict_value_30;
        PyObject *tmp_source_name_30;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_dict_key_31;
        PyObject *tmp_dict_value_31;
        PyObject *tmp_source_name_31;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_dict_key_32;
        PyObject *tmp_dict_value_32;
        PyObject *tmp_source_name_32;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_dict_key_33;
        PyObject *tmp_dict_value_33;
        PyObject *tmp_source_name_33;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_dict_key_34;
        PyObject *tmp_dict_value_34;
        PyObject *tmp_source_name_34;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_dict_key_35;
        PyObject *tmp_dict_value_35;
        PyObject *tmp_source_name_35;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_dict_key_36;
        PyObject *tmp_dict_value_36;
        PyObject *tmp_source_name_36;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_dict_key_37;
        PyObject *tmp_dict_value_37;
        PyObject *tmp_source_name_37;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_dict_key_38;
        PyObject *tmp_dict_value_38;
        PyObject *tmp_source_name_38;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_dict_key_39;
        PyObject *tmp_dict_value_39;
        PyObject *tmp_source_name_39;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_dict_key_40;
        PyObject *tmp_dict_value_40;
        PyObject *tmp_source_name_40;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_dict_key_41;
        PyObject *tmp_dict_value_41;
        PyObject *tmp_source_name_41;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_dict_key_42;
        PyObject *tmp_dict_value_42;
        PyObject *tmp_source_name_42;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_dict_key_43;
        PyObject *tmp_dict_value_43;
        PyObject *tmp_source_name_43;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_dict_key_44;
        PyObject *tmp_dict_value_44;
        PyObject *tmp_source_name_44;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_dict_key_45;
        PyObject *tmp_dict_value_45;
        PyObject *tmp_source_name_45;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_dict_key_46;
        PyObject *tmp_dict_value_46;
        PyObject *tmp_source_name_46;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_dict_key_47;
        PyObject *tmp_dict_value_47;
        PyObject *tmp_source_name_47;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_dict_key_48;
        PyObject *tmp_dict_value_48;
        PyObject *tmp_source_name_48;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_dict_key_49;
        PyObject *tmp_dict_value_49;
        PyObject *tmp_source_name_49;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_dict_key_50;
        PyObject *tmp_dict_value_50;
        PyObject *tmp_source_name_50;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_dict_key_51;
        PyObject *tmp_dict_value_51;
        PyObject *tmp_source_name_51;
        PyObject *tmp_mvar_value_53;
        PyObject *tmp_dict_key_52;
        PyObject *tmp_dict_value_52;
        PyObject *tmp_source_name_52;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_dict_key_53;
        PyObject *tmp_dict_value_53;
        PyObject *tmp_source_name_53;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_dict_key_54;
        PyObject *tmp_dict_value_54;
        PyObject *tmp_source_name_54;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_dict_key_55;
        PyObject *tmp_dict_value_55;
        PyObject *tmp_source_name_55;
        PyObject *tmp_mvar_value_57;
        PyObject *tmp_dict_key_56;
        PyObject *tmp_dict_value_56;
        PyObject *tmp_source_name_56;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_dict_key_57;
        PyObject *tmp_dict_value_57;
        PyObject *tmp_source_name_57;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_dict_key_58;
        PyObject *tmp_dict_value_58;
        PyObject *tmp_source_name_58;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_dict_key_59;
        PyObject *tmp_dict_value_59;
        PyObject *tmp_source_name_59;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_dict_key_60;
        PyObject *tmp_dict_value_60;
        PyObject *tmp_source_name_60;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_dict_key_61;
        PyObject *tmp_dict_value_61;
        PyObject *tmp_source_name_61;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_dict_key_62;
        PyObject *tmp_dict_value_62;
        PyObject *tmp_source_name_62;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_dict_key_63;
        PyObject *tmp_dict_value_63;
        PyObject *tmp_source_name_63;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_dict_key_64;
        PyObject *tmp_dict_value_64;
        PyObject *tmp_source_name_64;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_dict_key_65;
        PyObject *tmp_dict_value_65;
        PyObject *tmp_source_name_65;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_dict_key_66;
        PyObject *tmp_dict_value_66;
        PyObject *tmp_source_name_66;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_dict_key_67;
        PyObject *tmp_dict_value_67;
        PyObject *tmp_source_name_67;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_dict_key_68;
        PyObject *tmp_dict_value_68;
        PyObject *tmp_source_name_68;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_dict_key_69;
        PyObject *tmp_dict_value_69;
        PyObject *tmp_source_name_69;
        PyObject *tmp_mvar_value_71;
        PyObject *tmp_dict_key_70;
        PyObject *tmp_dict_value_70;
        PyObject *tmp_source_name_70;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_dict_key_71;
        PyObject *tmp_dict_value_71;
        PyObject *tmp_source_name_71;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_dict_key_72;
        PyObject *tmp_dict_value_72;
        PyObject *tmp_source_name_72;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_dict_key_73;
        PyObject *tmp_dict_value_73;
        PyObject *tmp_source_name_73;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_dict_key_74;
        PyObject *tmp_dict_value_74;
        PyObject *tmp_source_name_74;
        PyObject *tmp_mvar_value_76;
        PyObject *tmp_dict_key_75;
        PyObject *tmp_dict_value_75;
        PyObject *tmp_source_name_75;
        PyObject *tmp_mvar_value_77;
        PyObject *tmp_dict_key_76;
        PyObject *tmp_dict_value_76;
        PyObject *tmp_source_name_76;
        PyObject *tmp_mvar_value_78;
        PyObject *tmp_dict_key_77;
        PyObject *tmp_dict_value_77;
        PyObject *tmp_source_name_77;
        PyObject *tmp_mvar_value_79;
        PyObject *tmp_dict_key_78;
        PyObject *tmp_dict_value_78;
        PyObject *tmp_source_name_78;
        PyObject *tmp_mvar_value_80;
        PyObject *tmp_dict_key_79;
        PyObject *tmp_dict_value_79;
        PyObject *tmp_source_name_79;
        PyObject *tmp_mvar_value_81;
        PyObject *tmp_dict_key_80;
        PyObject *tmp_dict_value_80;
        PyObject *tmp_source_name_80;
        PyObject *tmp_mvar_value_82;
        PyObject *tmp_dict_key_81;
        PyObject *tmp_dict_value_81;
        PyObject *tmp_source_name_81;
        PyObject *tmp_mvar_value_83;
        PyObject *tmp_dict_key_82;
        PyObject *tmp_dict_value_82;
        PyObject *tmp_source_name_82;
        PyObject *tmp_mvar_value_84;
        PyObject *tmp_dict_key_83;
        PyObject *tmp_dict_value_83;
        PyObject *tmp_source_name_83;
        PyObject *tmp_mvar_value_85;
        PyObject *tmp_dict_key_84;
        PyObject *tmp_dict_value_84;
        PyObject *tmp_source_name_84;
        PyObject *tmp_mvar_value_86;
        PyObject *tmp_dict_key_85;
        PyObject *tmp_dict_value_85;
        PyObject *tmp_source_name_85;
        PyObject *tmp_mvar_value_87;
        PyObject *tmp_dict_key_86;
        PyObject *tmp_dict_value_86;
        PyObject *tmp_source_name_86;
        PyObject *tmp_mvar_value_88;
        PyObject *tmp_dict_key_87;
        PyObject *tmp_dict_value_87;
        PyObject *tmp_source_name_87;
        PyObject *tmp_mvar_value_89;
        PyObject *tmp_dict_key_88;
        PyObject *tmp_dict_value_88;
        PyObject *tmp_source_name_88;
        PyObject *tmp_mvar_value_90;
        PyObject *tmp_dict_key_89;
        PyObject *tmp_dict_value_89;
        PyObject *tmp_source_name_89;
        PyObject *tmp_mvar_value_91;
        PyObject *tmp_dict_key_90;
        PyObject *tmp_dict_value_90;
        PyObject *tmp_source_name_90;
        PyObject *tmp_mvar_value_92;
        PyObject *tmp_dict_key_91;
        PyObject *tmp_dict_value_91;
        PyObject *tmp_source_name_91;
        PyObject *tmp_mvar_value_93;
        PyObject *tmp_dict_key_92;
        PyObject *tmp_dict_value_92;
        PyObject *tmp_source_name_92;
        PyObject *tmp_mvar_value_94;
        PyObject *tmp_dict_key_93;
        PyObject *tmp_dict_value_93;
        PyObject *tmp_source_name_93;
        PyObject *tmp_mvar_value_95;
        PyObject *tmp_dict_key_94;
        PyObject *tmp_dict_value_94;
        PyObject *tmp_source_name_94;
        PyObject *tmp_mvar_value_96;
        PyObject *tmp_dict_key_95;
        PyObject *tmp_dict_value_95;
        PyObject *tmp_source_name_95;
        PyObject *tmp_mvar_value_97;
        PyObject *tmp_dict_key_96;
        PyObject *tmp_dict_value_96;
        PyObject *tmp_source_name_96;
        PyObject *tmp_mvar_value_98;
        PyObject *tmp_dict_key_97;
        PyObject *tmp_dict_value_97;
        PyObject *tmp_source_name_97;
        PyObject *tmp_mvar_value_99;
        PyObject *tmp_dict_key_98;
        PyObject *tmp_dict_value_98;
        PyObject *tmp_source_name_98;
        PyObject *tmp_mvar_value_100;
        PyObject *tmp_dict_key_99;
        PyObject *tmp_dict_value_99;
        PyObject *tmp_source_name_99;
        PyObject *tmp_mvar_value_101;
        PyObject *tmp_dict_key_100;
        PyObject *tmp_dict_value_100;
        PyObject *tmp_source_name_100;
        PyObject *tmp_mvar_value_102;
        PyObject *tmp_dict_key_101;
        PyObject *tmp_dict_value_101;
        PyObject *tmp_source_name_101;
        PyObject *tmp_mvar_value_103;
        PyObject *tmp_dict_key_102;
        PyObject *tmp_dict_value_102;
        PyObject *tmp_source_name_102;
        PyObject *tmp_mvar_value_104;
        PyObject *tmp_dict_key_103;
        PyObject *tmp_dict_value_103;
        PyObject *tmp_source_name_103;
        PyObject *tmp_mvar_value_105;
        PyObject *tmp_dict_key_104;
        PyObject *tmp_dict_value_104;
        PyObject *tmp_source_name_104;
        PyObject *tmp_mvar_value_106;
        PyObject *tmp_dict_key_105;
        PyObject *tmp_dict_value_105;
        PyObject *tmp_source_name_105;
        PyObject *tmp_mvar_value_107;
        PyObject *tmp_dict_key_106;
        PyObject *tmp_dict_value_106;
        PyObject *tmp_source_name_106;
        PyObject *tmp_mvar_value_108;
        PyObject *tmp_dict_key_107;
        PyObject *tmp_dict_value_107;
        PyObject *tmp_source_name_107;
        PyObject *tmp_mvar_value_109;
        PyObject *tmp_dict_key_108;
        PyObject *tmp_dict_value_108;
        PyObject *tmp_source_name_108;
        PyObject *tmp_mvar_value_110;
        PyObject *tmp_dict_key_109;
        PyObject *tmp_dict_value_109;
        PyObject *tmp_source_name_109;
        PyObject *tmp_mvar_value_111;
        PyObject *tmp_dict_key_110;
        PyObject *tmp_dict_value_110;
        PyObject *tmp_source_name_110;
        PyObject *tmp_mvar_value_112;
        PyObject *tmp_dict_key_111;
        PyObject *tmp_dict_value_111;
        PyObject *tmp_source_name_111;
        PyObject *tmp_mvar_value_113;
        PyObject *tmp_dict_key_112;
        PyObject *tmp_dict_value_112;
        PyObject *tmp_source_name_112;
        PyObject *tmp_mvar_value_114;
        PyObject *tmp_dict_key_113;
        PyObject *tmp_dict_value_113;
        PyObject *tmp_source_name_113;
        PyObject *tmp_mvar_value_115;
        PyObject *tmp_dict_key_114;
        PyObject *tmp_dict_value_114;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_114;
        PyObject *tmp_mvar_value_116;
        PyObject *tmp_source_name_115;
        PyObject *tmp_mvar_value_117;
        PyObject *tmp_dict_key_115;
        PyObject *tmp_dict_value_115;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_source_name_116;
        PyObject *tmp_mvar_value_118;
        PyObject *tmp_source_name_117;
        PyObject *tmp_mvar_value_119;
        PyObject *tmp_dict_key_116;
        PyObject *tmp_dict_value_116;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_source_name_118;
        PyObject *tmp_mvar_value_120;
        PyObject *tmp_source_name_119;
        PyObject *tmp_mvar_value_121;
        PyObject *tmp_dict_key_117;
        PyObject *tmp_dict_value_117;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_source_name_120;
        PyObject *tmp_mvar_value_122;
        PyObject *tmp_source_name_121;
        PyObject *tmp_mvar_value_123;
        PyObject *tmp_dict_key_118;
        PyObject *tmp_dict_value_118;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_source_name_122;
        PyObject *tmp_mvar_value_124;
        PyObject *tmp_source_name_123;
        PyObject *tmp_mvar_value_125;
        PyObject *tmp_dict_key_119;
        PyObject *tmp_dict_value_119;
        PyObject *tmp_tuple_element_6;
        PyObject *tmp_source_name_124;
        PyObject *tmp_mvar_value_126;
        PyObject *tmp_source_name_125;
        PyObject *tmp_mvar_value_127;
        PyObject *tmp_dict_key_120;
        PyObject *tmp_dict_value_120;
        PyObject *tmp_source_name_126;
        PyObject *tmp_mvar_value_128;
        PyObject *tmp_dict_key_121;
        PyObject *tmp_dict_value_121;
        PyObject *tmp_source_name_127;
        PyObject *tmp_mvar_value_129;
        tmp_dict_key_1 = const_str_chr_0;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ControlAt );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = _PyDict_NewPresized( 121 );
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_chr_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 17;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_4;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_ControlA );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_chr_2;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 18;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_5;
        tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_ControlB );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_chr_3;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 19;

            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_6;
        tmp_dict_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_ControlC );
        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_chr_4;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 20;

            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_7;
        tmp_dict_value_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_ControlD );
        if ( tmp_dict_value_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_chr_5;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 21;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_8;
        tmp_dict_value_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_ControlE );
        if ( tmp_dict_value_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_chr_6;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 22;

            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_9;
        tmp_dict_value_7 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_ControlF );
        if ( tmp_dict_value_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_chr_7;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 23;

            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_10;
        tmp_dict_value_8 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_ControlG );
        if ( tmp_dict_value_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_8, tmp_dict_value_8 );
        Py_DECREF( tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_chr_8;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 24;

            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_11;
        tmp_dict_value_9 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_ControlH );
        if ( tmp_dict_value_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_9, tmp_dict_value_9 );
        Py_DECREF( tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_chr_9;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 25;

            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_12;
        tmp_dict_value_10 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_ControlI );
        if ( tmp_dict_value_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_10, tmp_dict_value_10 );
        Py_DECREF( tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_11 = const_str_newline;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 26;

            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_13;
        tmp_dict_value_11 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_ControlJ );
        if ( tmp_dict_value_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_11, tmp_dict_value_11 );
        Py_DECREF( tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_12 = const_str_chr_11;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 27;

            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_14;
        tmp_dict_value_12 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_ControlK );
        if ( tmp_dict_value_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_12, tmp_dict_value_12 );
        Py_DECREF( tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_13 = const_str_chr_12;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 28;

            goto frame_exception_exit_1;
        }

        tmp_source_name_13 = tmp_mvar_value_15;
        tmp_dict_value_13 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_ControlL );
        if ( tmp_dict_value_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_13, tmp_dict_value_13 );
        Py_DECREF( tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_14 = const_str_chr_13;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 29;

            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_16;
        tmp_dict_value_14 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_ControlM );
        if ( tmp_dict_value_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_14, tmp_dict_value_14 );
        Py_DECREF( tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_15 = const_str_chr_14;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 30;

            goto frame_exception_exit_1;
        }

        tmp_source_name_15 = tmp_mvar_value_17;
        tmp_dict_value_15 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_ControlN );
        if ( tmp_dict_value_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_15, tmp_dict_value_15 );
        Py_DECREF( tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_16 = const_str_chr_15;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 31;

            goto frame_exception_exit_1;
        }

        tmp_source_name_16 = tmp_mvar_value_18;
        tmp_dict_value_16 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_ControlO );
        if ( tmp_dict_value_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_16, tmp_dict_value_16 );
        Py_DECREF( tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_17 = const_str_chr_16;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_19 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 32;

            goto frame_exception_exit_1;
        }

        tmp_source_name_17 = tmp_mvar_value_19;
        tmp_dict_value_17 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_ControlP );
        if ( tmp_dict_value_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_17, tmp_dict_value_17 );
        Py_DECREF( tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_18 = const_str_chr_17;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_20 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;

            goto frame_exception_exit_1;
        }

        tmp_source_name_18 = tmp_mvar_value_20;
        tmp_dict_value_18 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_ControlQ );
        if ( tmp_dict_value_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 33;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_18, tmp_dict_value_18 );
        Py_DECREF( tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_19 = const_str_chr_18;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 34;

            goto frame_exception_exit_1;
        }

        tmp_source_name_19 = tmp_mvar_value_21;
        tmp_dict_value_19 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_ControlR );
        if ( tmp_dict_value_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_19, tmp_dict_value_19 );
        Py_DECREF( tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_20 = const_str_chr_19;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 35;

            goto frame_exception_exit_1;
        }

        tmp_source_name_20 = tmp_mvar_value_22;
        tmp_dict_value_20 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_ControlS );
        if ( tmp_dict_value_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_20, tmp_dict_value_20 );
        Py_DECREF( tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_21 = const_str_chr_20;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;

            goto frame_exception_exit_1;
        }

        tmp_source_name_21 = tmp_mvar_value_23;
        tmp_dict_value_21 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_ControlT );
        if ( tmp_dict_value_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 36;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_21, tmp_dict_value_21 );
        Py_DECREF( tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_22 = const_str_chr_21;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 37;

            goto frame_exception_exit_1;
        }

        tmp_source_name_22 = tmp_mvar_value_24;
        tmp_dict_value_22 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_ControlU );
        if ( tmp_dict_value_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_22, tmp_dict_value_22 );
        Py_DECREF( tmp_dict_value_22 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_23 = const_str_chr_22;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_25 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 38;

            goto frame_exception_exit_1;
        }

        tmp_source_name_23 = tmp_mvar_value_25;
        tmp_dict_value_23 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_ControlV );
        if ( tmp_dict_value_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_23, tmp_dict_value_23 );
        Py_DECREF( tmp_dict_value_23 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_24 = const_str_chr_23;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;

            goto frame_exception_exit_1;
        }

        tmp_source_name_24 = tmp_mvar_value_26;
        tmp_dict_value_24 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_ControlW );
        if ( tmp_dict_value_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_24, tmp_dict_value_24 );
        Py_DECREF( tmp_dict_value_24 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_25 = const_str_chr_24;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;

            goto frame_exception_exit_1;
        }

        tmp_source_name_25 = tmp_mvar_value_27;
        tmp_dict_value_25 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_ControlX );
        if ( tmp_dict_value_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 40;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_25, tmp_dict_value_25 );
        Py_DECREF( tmp_dict_value_25 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_26 = const_str_chr_25;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }

        tmp_source_name_26 = tmp_mvar_value_28;
        tmp_dict_value_26 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_ControlY );
        if ( tmp_dict_value_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_26, tmp_dict_value_26 );
        Py_DECREF( tmp_dict_value_26 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_27 = const_str_chr_26;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;

            goto frame_exception_exit_1;
        }

        tmp_source_name_27 = tmp_mvar_value_29;
        tmp_dict_value_27 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_ControlZ );
        if ( tmp_dict_value_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 42;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_27, tmp_dict_value_27 );
        Py_DECREF( tmp_dict_value_27 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_28 = const_str_chr_27;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;

            goto frame_exception_exit_1;
        }

        tmp_source_name_28 = tmp_mvar_value_30;
        tmp_dict_value_28 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_Escape );
        if ( tmp_dict_value_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 44;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_28, tmp_dict_value_28 );
        Py_DECREF( tmp_dict_value_28 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_29 = const_str_chr_28;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_31 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;

            goto frame_exception_exit_1;
        }

        tmp_source_name_29 = tmp_mvar_value_31;
        tmp_dict_value_29 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_ControlBackslash );
        if ( tmp_dict_value_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 45;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_29, tmp_dict_value_29 );
        Py_DECREF( tmp_dict_value_29 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_30 = const_str_chr_29;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;

            goto frame_exception_exit_1;
        }

        tmp_source_name_30 = tmp_mvar_value_32;
        tmp_dict_value_30 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_ControlSquareClose );
        if ( tmp_dict_value_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 46;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_30, tmp_dict_value_30 );
        Py_DECREF( tmp_dict_value_30 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_31 = const_str_chr_30;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;

            goto frame_exception_exit_1;
        }

        tmp_source_name_31 = tmp_mvar_value_33;
        tmp_dict_value_31 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_ControlCircumflex );
        if ( tmp_dict_value_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 47;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_31, tmp_dict_value_31 );
        Py_DECREF( tmp_dict_value_31 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_32 = const_str_chr_31;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;

            goto frame_exception_exit_1;
        }

        tmp_source_name_32 = tmp_mvar_value_34;
        tmp_dict_value_32 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_ControlUnderscore );
        if ( tmp_dict_value_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 48;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_32, tmp_dict_value_32 );
        Py_DECREF( tmp_dict_value_32 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_33 = const_str_chr_127;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_35 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;

            goto frame_exception_exit_1;
        }

        tmp_source_name_33 = tmp_mvar_value_35;
        tmp_dict_value_33 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_ControlH );
        if ( tmp_dict_value_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 57;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_33, tmp_dict_value_33 );
        Py_DECREF( tmp_dict_value_33 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_34 = const_str_digest_76ae19717fa0d569b06c578da9221718;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_36 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 59;

            goto frame_exception_exit_1;
        }

        tmp_source_name_34 = tmp_mvar_value_36;
        tmp_dict_value_34 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain_Up );
        if ( tmp_dict_value_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 59;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_34, tmp_dict_value_34 );
        Py_DECREF( tmp_dict_value_34 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_35 = const_str_digest_2059ac4e7a1d81406714c1266156f1ee;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_37 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;

            goto frame_exception_exit_1;
        }

        tmp_source_name_35 = tmp_mvar_value_37;
        tmp_dict_value_35 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain_Down );
        if ( tmp_dict_value_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 60;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_35, tmp_dict_value_35 );
        Py_DECREF( tmp_dict_value_35 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_36 = const_str_digest_d84420b7a31ae497b4474c19147bc88d;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_38 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;

            goto frame_exception_exit_1;
        }

        tmp_source_name_36 = tmp_mvar_value_38;
        tmp_dict_value_36 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain_Right );
        if ( tmp_dict_value_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 61;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_36, tmp_dict_value_36 );
        Py_DECREF( tmp_dict_value_36 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_37 = const_str_digest_30d7db835e6623e49486c13a0ef7bf3b;
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_39 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }

        tmp_source_name_37 = tmp_mvar_value_39;
        tmp_dict_value_37 = LOOKUP_ATTRIBUTE( tmp_source_name_37, const_str_plain_Left );
        if ( tmp_dict_value_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_37, tmp_dict_value_37 );
        Py_DECREF( tmp_dict_value_37 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_38 = const_str_digest_c63467252ff92c7f8c1a3601e8401fac;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_40 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;

            goto frame_exception_exit_1;
        }

        tmp_source_name_38 = tmp_mvar_value_40;
        tmp_dict_value_38 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain_Home );
        if ( tmp_dict_value_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_38, tmp_dict_value_38 );
        Py_DECREF( tmp_dict_value_38 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_39 = const_str_digest_40c81036de1212c99f9ca44aa054df97;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_41 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }

        tmp_source_name_39 = tmp_mvar_value_41;
        tmp_dict_value_39 = LOOKUP_ATTRIBUTE( tmp_source_name_39, const_str_plain_Home );
        if ( tmp_dict_value_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_39, tmp_dict_value_39 );
        Py_DECREF( tmp_dict_value_39 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_40 = const_str_digest_855cbefdb5107659e57086dd355ba22b;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_42 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 65;

            goto frame_exception_exit_1;
        }

        tmp_source_name_40 = tmp_mvar_value_42;
        tmp_dict_value_40 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain_End );
        if ( tmp_dict_value_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 65;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_40, tmp_dict_value_40 );
        Py_DECREF( tmp_dict_value_40 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_41 = const_str_digest_0a9eb65db2809423077341b09bb9bf67;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_43 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 66;

            goto frame_exception_exit_1;
        }

        tmp_source_name_41 = tmp_mvar_value_43;
        tmp_dict_value_41 = LOOKUP_ATTRIBUTE( tmp_source_name_41, const_str_plain_End );
        if ( tmp_dict_value_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 66;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_41, tmp_dict_value_41 );
        Py_DECREF( tmp_dict_value_41 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_42 = const_str_digest_2cb59f6fba9e21c488534a537644382f;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_44 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }

        tmp_source_name_42 = tmp_mvar_value_44;
        tmp_dict_value_42 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain_Delete );
        if ( tmp_dict_value_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_42, tmp_dict_value_42 );
        Py_DECREF( tmp_dict_value_42 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_43 = const_str_digest_cb639d71eaae980d7ae391093b9c3b2a;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_45 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 68;

            goto frame_exception_exit_1;
        }

        tmp_source_name_43 = tmp_mvar_value_45;
        tmp_dict_value_43 = LOOKUP_ATTRIBUTE( tmp_source_name_43, const_str_plain_ShiftDelete );
        if ( tmp_dict_value_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 68;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_43, tmp_dict_value_43 );
        Py_DECREF( tmp_dict_value_43 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_44 = const_str_digest_ee4e16525301f735f93e646c6c42daec;
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_46 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }

        tmp_source_name_44 = tmp_mvar_value_46;
        tmp_dict_value_44 = LOOKUP_ATTRIBUTE( tmp_source_name_44, const_str_plain_ControlDelete );
        if ( tmp_dict_value_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_44, tmp_dict_value_44 );
        Py_DECREF( tmp_dict_value_44 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_45 = const_str_digest_49a04bf601200952b6d59882eac40d72;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_47 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 70;

            goto frame_exception_exit_1;
        }

        tmp_source_name_45 = tmp_mvar_value_47;
        tmp_dict_value_45 = LOOKUP_ATTRIBUTE( tmp_source_name_45, const_str_plain_Home );
        if ( tmp_dict_value_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 70;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_45, tmp_dict_value_45 );
        Py_DECREF( tmp_dict_value_45 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_46 = const_str_digest_a7f26ffa6da53b4707170a0ce66f1af6;
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_48 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;

            goto frame_exception_exit_1;
        }

        tmp_source_name_46 = tmp_mvar_value_48;
        tmp_dict_value_46 = LOOKUP_ATTRIBUTE( tmp_source_name_46, const_str_plain_End );
        if ( tmp_dict_value_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 71;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_46, tmp_dict_value_46 );
        Py_DECREF( tmp_dict_value_46 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_47 = const_str_digest_bb785395327ee29dbd8541c485eb65e7;
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_49 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;

            goto frame_exception_exit_1;
        }

        tmp_source_name_47 = tmp_mvar_value_49;
        tmp_dict_value_47 = LOOKUP_ATTRIBUTE( tmp_source_name_47, const_str_plain_PageUp );
        if ( tmp_dict_value_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_47, tmp_dict_value_47 );
        Py_DECREF( tmp_dict_value_47 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_48 = const_str_digest_e1ab117ad38a4f32363b686a8410c9d5;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_50 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;

            goto frame_exception_exit_1;
        }

        tmp_source_name_48 = tmp_mvar_value_50;
        tmp_dict_value_48 = LOOKUP_ATTRIBUTE( tmp_source_name_48, const_str_plain_PageDown );
        if ( tmp_dict_value_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 73;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_48, tmp_dict_value_48 );
        Py_DECREF( tmp_dict_value_48 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_49 = const_str_digest_8582c6351090c305eddd1c2a4cc49b2c;
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_51 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 74;

            goto frame_exception_exit_1;
        }

        tmp_source_name_49 = tmp_mvar_value_51;
        tmp_dict_value_49 = LOOKUP_ATTRIBUTE( tmp_source_name_49, const_str_plain_Home );
        if ( tmp_dict_value_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 74;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_49, tmp_dict_value_49 );
        Py_DECREF( tmp_dict_value_49 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_50 = const_str_digest_dc41115cfc7281b49f423502e3db8b18;
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_52 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 75;

            goto frame_exception_exit_1;
        }

        tmp_source_name_50 = tmp_mvar_value_52;
        tmp_dict_value_50 = LOOKUP_ATTRIBUTE( tmp_source_name_50, const_str_plain_End );
        if ( tmp_dict_value_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 75;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_50, tmp_dict_value_50 );
        Py_DECREF( tmp_dict_value_50 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_51 = const_str_digest_9b3eaf58399f683697373ee1daa6aa28;
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_53 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;

            goto frame_exception_exit_1;
        }

        tmp_source_name_51 = tmp_mvar_value_53;
        tmp_dict_value_51 = LOOKUP_ATTRIBUTE( tmp_source_name_51, const_str_plain_BackTab );
        if ( tmp_dict_value_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_51, tmp_dict_value_51 );
        Py_DECREF( tmp_dict_value_51 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_52 = const_str_digest_23a95ef0f717a33ca425b2f26055a496;
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_54 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }

        tmp_source_name_52 = tmp_mvar_value_54;
        tmp_dict_value_52 = LOOKUP_ATTRIBUTE( tmp_source_name_52, const_str_plain_Insert );
        if ( tmp_dict_value_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_52, tmp_dict_value_52 );
        Py_DECREF( tmp_dict_value_52 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_53 = const_str_digest_2767cdf3369330764ad399a482d2b929;
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_55 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;

            goto frame_exception_exit_1;
        }

        tmp_source_name_53 = tmp_mvar_value_55;
        tmp_dict_value_53 = LOOKUP_ATTRIBUTE( tmp_source_name_53, const_str_plain_F1 );
        if ( tmp_dict_value_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_53, tmp_dict_value_53 );
        Py_DECREF( tmp_dict_value_53 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_54 = const_str_digest_56e622cd76de0e826f57104d3f211364;
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_56 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;

            goto frame_exception_exit_1;
        }

        tmp_source_name_54 = tmp_mvar_value_56;
        tmp_dict_value_54 = LOOKUP_ATTRIBUTE( tmp_source_name_54, const_str_plain_F2 );
        if ( tmp_dict_value_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 80;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_54, tmp_dict_value_54 );
        Py_DECREF( tmp_dict_value_54 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_55 = const_str_digest_fd2edef1a1fff32a7aed8d7d47e6a776;
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_57 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }

        tmp_source_name_55 = tmp_mvar_value_57;
        tmp_dict_value_55 = LOOKUP_ATTRIBUTE( tmp_source_name_55, const_str_plain_F3 );
        if ( tmp_dict_value_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_55, tmp_dict_value_55 );
        Py_DECREF( tmp_dict_value_55 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_56 = const_str_digest_1ef99b45a0bb33cecf0567b1674bb46c;
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_58 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;

            goto frame_exception_exit_1;
        }

        tmp_source_name_56 = tmp_mvar_value_58;
        tmp_dict_value_56 = LOOKUP_ATTRIBUTE( tmp_source_name_56, const_str_plain_F4 );
        if ( tmp_dict_value_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_56, tmp_dict_value_56 );
        Py_DECREF( tmp_dict_value_56 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_57 = const_str_digest_12cbcda11402fcdddab393c058c0f69e;
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_59 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;

            goto frame_exception_exit_1;
        }

        tmp_source_name_57 = tmp_mvar_value_59;
        tmp_dict_value_57 = LOOKUP_ATTRIBUTE( tmp_source_name_57, const_str_plain_F1 );
        if ( tmp_dict_value_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_57, tmp_dict_value_57 );
        Py_DECREF( tmp_dict_value_57 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_58 = const_str_digest_34068089dc6749b64bdfa3a7fca59061;
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_60 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 84;

            goto frame_exception_exit_1;
        }

        tmp_source_name_58 = tmp_mvar_value_60;
        tmp_dict_value_58 = LOOKUP_ATTRIBUTE( tmp_source_name_58, const_str_plain_F2 );
        if ( tmp_dict_value_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 84;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_58, tmp_dict_value_58 );
        Py_DECREF( tmp_dict_value_58 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_59 = const_str_digest_4e7da83bf89ff269292aba8383def151;
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_61 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }

        tmp_source_name_59 = tmp_mvar_value_61;
        tmp_dict_value_59 = LOOKUP_ATTRIBUTE( tmp_source_name_59, const_str_plain_F3 );
        if ( tmp_dict_value_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_59, tmp_dict_value_59 );
        Py_DECREF( tmp_dict_value_59 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_60 = const_str_digest_8496eda9d2db69e6bc02e9a70a857e4f;
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_62 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }

        tmp_source_name_60 = tmp_mvar_value_62;
        tmp_dict_value_60 = LOOKUP_ATTRIBUTE( tmp_source_name_60, const_str_plain_F4 );
        if ( tmp_dict_value_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_60, tmp_dict_value_60 );
        Py_DECREF( tmp_dict_value_60 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_61 = const_str_digest_71ad11d69c1ea2207cf20a5df3f02d74;
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_63 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 87;

            goto frame_exception_exit_1;
        }

        tmp_source_name_61 = tmp_mvar_value_63;
        tmp_dict_value_61 = LOOKUP_ATTRIBUTE( tmp_source_name_61, const_str_plain_F5 );
        if ( tmp_dict_value_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 87;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_61, tmp_dict_value_61 );
        Py_DECREF( tmp_dict_value_61 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_62 = const_str_digest_0df4cc8acea8a24f0ce17edb3f4992be;
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_64 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 88;

            goto frame_exception_exit_1;
        }

        tmp_source_name_62 = tmp_mvar_value_64;
        tmp_dict_value_62 = LOOKUP_ATTRIBUTE( tmp_source_name_62, const_str_plain_F1 );
        if ( tmp_dict_value_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 88;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_62, tmp_dict_value_62 );
        Py_DECREF( tmp_dict_value_62 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_63 = const_str_digest_73be8c3012f87688236d4b75ed957f35;
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_65 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;

            goto frame_exception_exit_1;
        }

        tmp_source_name_63 = tmp_mvar_value_65;
        tmp_dict_value_63 = LOOKUP_ATTRIBUTE( tmp_source_name_63, const_str_plain_F2 );
        if ( tmp_dict_value_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 89;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_63, tmp_dict_value_63 );
        Py_DECREF( tmp_dict_value_63 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_64 = const_str_digest_058260aba72117ad57ad4403fdce6a8e;
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_66 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 90;

            goto frame_exception_exit_1;
        }

        tmp_source_name_64 = tmp_mvar_value_66;
        tmp_dict_value_64 = LOOKUP_ATTRIBUTE( tmp_source_name_64, const_str_plain_F3 );
        if ( tmp_dict_value_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 90;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_64, tmp_dict_value_64 );
        Py_DECREF( tmp_dict_value_64 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_65 = const_str_digest_020a56b5559e369a659c41d9479d25eb;
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_67 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;

            goto frame_exception_exit_1;
        }

        tmp_source_name_65 = tmp_mvar_value_67;
        tmp_dict_value_65 = LOOKUP_ATTRIBUTE( tmp_source_name_65, const_str_plain_F4 );
        if ( tmp_dict_value_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 91;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_65, tmp_dict_value_65 );
        Py_DECREF( tmp_dict_value_65 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_66 = const_str_digest_a893ab180602a6c3d02cc0a97b2b0fc2;
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_68 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;

            goto frame_exception_exit_1;
        }

        tmp_source_name_66 = tmp_mvar_value_68;
        tmp_dict_value_66 = LOOKUP_ATTRIBUTE( tmp_source_name_66, const_str_plain_F5 );
        if ( tmp_dict_value_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 92;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_66, tmp_dict_value_66 );
        Py_DECREF( tmp_dict_value_66 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_67 = const_str_digest_ab6f3d643f8365fb3bac6fc96a8b7375;
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_69 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 93;

            goto frame_exception_exit_1;
        }

        tmp_source_name_67 = tmp_mvar_value_69;
        tmp_dict_value_67 = LOOKUP_ATTRIBUTE( tmp_source_name_67, const_str_plain_F6 );
        if ( tmp_dict_value_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 93;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_67, tmp_dict_value_67 );
        Py_DECREF( tmp_dict_value_67 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_68 = const_str_digest_e66b04b17e0da4b097384da7401798cd;
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_70 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }

        tmp_source_name_68 = tmp_mvar_value_70;
        tmp_dict_value_68 = LOOKUP_ATTRIBUTE( tmp_source_name_68, const_str_plain_F7 );
        if ( tmp_dict_value_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_68, tmp_dict_value_68 );
        Py_DECREF( tmp_dict_value_68 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_69 = const_str_digest_6ab2674fce36c805aa9d8c610de646f0;
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_71 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 95;

            goto frame_exception_exit_1;
        }

        tmp_source_name_69 = tmp_mvar_value_71;
        tmp_dict_value_69 = LOOKUP_ATTRIBUTE( tmp_source_name_69, const_str_plain_F8 );
        if ( tmp_dict_value_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 95;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_69, tmp_dict_value_69 );
        Py_DECREF( tmp_dict_value_69 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_70 = const_str_digest_432e592f9e58f4aa3e445792576b6e30;
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_72 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;

            goto frame_exception_exit_1;
        }

        tmp_source_name_70 = tmp_mvar_value_72;
        tmp_dict_value_70 = LOOKUP_ATTRIBUTE( tmp_source_name_70, const_str_plain_F9 );
        if ( tmp_dict_value_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 96;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_70, tmp_dict_value_70 );
        Py_DECREF( tmp_dict_value_70 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_71 = const_str_digest_b97c39b71e3a68fba9de581ab9e5a6e6;
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_73 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;

            goto frame_exception_exit_1;
        }

        tmp_source_name_71 = tmp_mvar_value_73;
        tmp_dict_value_71 = LOOKUP_ATTRIBUTE( tmp_source_name_71, const_str_plain_F10 );
        if ( tmp_dict_value_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_71, tmp_dict_value_71 );
        Py_DECREF( tmp_dict_value_71 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_72 = const_str_digest_5b8f61ade02535a51406f5ee0eaf4b6d;
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_74 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 98;

            goto frame_exception_exit_1;
        }

        tmp_source_name_72 = tmp_mvar_value_74;
        tmp_dict_value_72 = LOOKUP_ATTRIBUTE( tmp_source_name_72, const_str_plain_F11 );
        if ( tmp_dict_value_72 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 98;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_72, tmp_dict_value_72 );
        Py_DECREF( tmp_dict_value_72 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_73 = const_str_digest_e9e0af174623e18f61e63518bbc6feec;
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_75 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;

            goto frame_exception_exit_1;
        }

        tmp_source_name_73 = tmp_mvar_value_75;
        tmp_dict_value_73 = LOOKUP_ATTRIBUTE( tmp_source_name_73, const_str_plain_F12 );
        if ( tmp_dict_value_73 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 99;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_73, tmp_dict_value_73 );
        Py_DECREF( tmp_dict_value_73 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_74 = const_str_digest_19b8e8e1cfa33db9cfff601f637bdbda;
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_76 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;

            goto frame_exception_exit_1;
        }

        tmp_source_name_74 = tmp_mvar_value_76;
        tmp_dict_value_74 = LOOKUP_ATTRIBUTE( tmp_source_name_74, const_str_plain_F13 );
        if ( tmp_dict_value_74 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 100;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_74, tmp_dict_value_74 );
        Py_DECREF( tmp_dict_value_74 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_75 = const_str_digest_01c81176a1a2b7a651333f3fc4e04bc7;
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_77 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;

            goto frame_exception_exit_1;
        }

        tmp_source_name_75 = tmp_mvar_value_77;
        tmp_dict_value_75 = LOOKUP_ATTRIBUTE( tmp_source_name_75, const_str_plain_F14 );
        if ( tmp_dict_value_75 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 101;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_75, tmp_dict_value_75 );
        Py_DECREF( tmp_dict_value_75 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_76 = const_str_digest_2ca855b20812d2c2a4a4e70eb1190e23;
        tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_78 == NULL ))
        {
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_78 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;

            goto frame_exception_exit_1;
        }

        tmp_source_name_76 = tmp_mvar_value_78;
        tmp_dict_value_76 = LOOKUP_ATTRIBUTE( tmp_source_name_76, const_str_plain_F15 );
        if ( tmp_dict_value_76 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 102;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_76, tmp_dict_value_76 );
        Py_DECREF( tmp_dict_value_76 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_77 = const_str_digest_72bb63f38d5716ecd080e28067560245;
        tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_79 == NULL ))
        {
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_79 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 103;

            goto frame_exception_exit_1;
        }

        tmp_source_name_77 = tmp_mvar_value_79;
        tmp_dict_value_77 = LOOKUP_ATTRIBUTE( tmp_source_name_77, const_str_plain_F16 );
        if ( tmp_dict_value_77 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 103;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_77, tmp_dict_value_77 );
        Py_DECREF( tmp_dict_value_77 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_78 = const_str_digest_dc18a06b80af74068c7974fa2e470651;
        tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_80 == NULL ))
        {
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_80 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }

        tmp_source_name_78 = tmp_mvar_value_80;
        tmp_dict_value_78 = LOOKUP_ATTRIBUTE( tmp_source_name_78, const_str_plain_F17 );
        if ( tmp_dict_value_78 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_78, tmp_dict_value_78 );
        Py_DECREF( tmp_dict_value_78 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_79 = const_str_digest_2364c66b4b73aba8a46780ea9928bbef;
        tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_81 == NULL ))
        {
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_81 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }

        tmp_source_name_79 = tmp_mvar_value_81;
        tmp_dict_value_79 = LOOKUP_ATTRIBUTE( tmp_source_name_79, const_str_plain_F18 );
        if ( tmp_dict_value_79 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_79, tmp_dict_value_79 );
        Py_DECREF( tmp_dict_value_79 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_80 = const_str_digest_3e707169fb81bec7d62a516863d91f23;
        tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_82 == NULL ))
        {
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_82 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }

        tmp_source_name_80 = tmp_mvar_value_82;
        tmp_dict_value_80 = LOOKUP_ATTRIBUTE( tmp_source_name_80, const_str_plain_F19 );
        if ( tmp_dict_value_80 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_80, tmp_dict_value_80 );
        Py_DECREF( tmp_dict_value_80 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_81 = const_str_digest_d641c0258f07d13a0cd660b1a5aca6db;
        tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_83 == NULL ))
        {
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_83 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;

            goto frame_exception_exit_1;
        }

        tmp_source_name_81 = tmp_mvar_value_83;
        tmp_dict_value_81 = LOOKUP_ATTRIBUTE( tmp_source_name_81, const_str_plain_F20 );
        if ( tmp_dict_value_81 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 107;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_81, tmp_dict_value_81 );
        Py_DECREF( tmp_dict_value_81 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_82 = const_str_digest_ec10944927cf01c741f92f8dfb54ab5c;
        tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_84 == NULL ))
        {
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_84 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;

            goto frame_exception_exit_1;
        }

        tmp_source_name_82 = tmp_mvar_value_84;
        tmp_dict_value_82 = LOOKUP_ATTRIBUTE( tmp_source_name_82, const_str_plain_F13 );
        if ( tmp_dict_value_82 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 110;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_82, tmp_dict_value_82 );
        Py_DECREF( tmp_dict_value_82 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_83 = const_str_digest_0a4750ed52a1c31727ee0b12889ebd67;
        tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_85 == NULL ))
        {
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_85 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;

            goto frame_exception_exit_1;
        }

        tmp_source_name_83 = tmp_mvar_value_85;
        tmp_dict_value_83 = LOOKUP_ATTRIBUTE( tmp_source_name_83, const_str_plain_F14 );
        if ( tmp_dict_value_83 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 111;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_83, tmp_dict_value_83 );
        Py_DECREF( tmp_dict_value_83 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_84 = const_str_digest_620bdf662e4481b904ab5f3e59086c27;
        tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_86 == NULL ))
        {
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_86 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 113;

            goto frame_exception_exit_1;
        }

        tmp_source_name_84 = tmp_mvar_value_86;
        tmp_dict_value_84 = LOOKUP_ATTRIBUTE( tmp_source_name_84, const_str_plain_F16 );
        if ( tmp_dict_value_84 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 113;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_84, tmp_dict_value_84 );
        Py_DECREF( tmp_dict_value_84 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_85 = const_str_digest_8d32ec3d83390268b84dbc2c621c05bc;
        tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_87 == NULL ))
        {
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_87 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;

            goto frame_exception_exit_1;
        }

        tmp_source_name_85 = tmp_mvar_value_87;
        tmp_dict_value_85 = LOOKUP_ATTRIBUTE( tmp_source_name_85, const_str_plain_F17 );
        if ( tmp_dict_value_85 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 114;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_85, tmp_dict_value_85 );
        Py_DECREF( tmp_dict_value_85 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_86 = const_str_digest_7d50c114d0c31ace70f4521595eeb145;
        tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_88 == NULL ))
        {
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_88 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }

        tmp_source_name_86 = tmp_mvar_value_88;
        tmp_dict_value_86 = LOOKUP_ATTRIBUTE( tmp_source_name_86, const_str_plain_F18 );
        if ( tmp_dict_value_86 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_86, tmp_dict_value_86 );
        Py_DECREF( tmp_dict_value_86 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_87 = const_str_digest_032d49d6628734a7e58b53f5b93ed271;
        tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_89 == NULL ))
        {
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_89 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 116;

            goto frame_exception_exit_1;
        }

        tmp_source_name_87 = tmp_mvar_value_89;
        tmp_dict_value_87 = LOOKUP_ATTRIBUTE( tmp_source_name_87, const_str_plain_F19 );
        if ( tmp_dict_value_87 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 116;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_87, tmp_dict_value_87 );
        Py_DECREF( tmp_dict_value_87 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_88 = const_str_digest_a9a7497bb05879a233f96e474d239561;
        tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_90 == NULL ))
        {
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_90 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;

            goto frame_exception_exit_1;
        }

        tmp_source_name_88 = tmp_mvar_value_90;
        tmp_dict_value_88 = LOOKUP_ATTRIBUTE( tmp_source_name_88, const_str_plain_F20 );
        if ( tmp_dict_value_88 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 117;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_88, tmp_dict_value_88 );
        Py_DECREF( tmp_dict_value_88 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_89 = const_str_digest_d935c86a1d788e8e47a876915c403911;
        tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_91 == NULL ))
        {
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_91 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;

            goto frame_exception_exit_1;
        }

        tmp_source_name_89 = tmp_mvar_value_91;
        tmp_dict_value_89 = LOOKUP_ATTRIBUTE( tmp_source_name_89, const_str_plain_F21 );
        if ( tmp_dict_value_89 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 118;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_89, tmp_dict_value_89 );
        Py_DECREF( tmp_dict_value_89 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_90 = const_str_digest_8056f16124e7d0ff823529101047b0ea;
        tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_92 == NULL ))
        {
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_92 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;

            goto frame_exception_exit_1;
        }

        tmp_source_name_90 = tmp_mvar_value_92;
        tmp_dict_value_90 = LOOKUP_ATTRIBUTE( tmp_source_name_90, const_str_plain_F22 );
        if ( tmp_dict_value_90 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 119;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_90, tmp_dict_value_90 );
        Py_DECREF( tmp_dict_value_90 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_91 = const_str_digest_10e19d55847a00809fd5ffd896b81aff;
        tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_93 == NULL ))
        {
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_93 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;

            goto frame_exception_exit_1;
        }

        tmp_source_name_91 = tmp_mvar_value_93;
        tmp_dict_value_91 = LOOKUP_ATTRIBUTE( tmp_source_name_91, const_str_plain_F23 );
        if ( tmp_dict_value_91 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 120;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_91, tmp_dict_value_91 );
        Py_DECREF( tmp_dict_value_91 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_92 = const_str_digest_03f22995d6374e9c04ae343b798f49a9;
        tmp_mvar_value_94 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_94 == NULL ))
        {
            tmp_mvar_value_94 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_94 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;

            goto frame_exception_exit_1;
        }

        tmp_source_name_92 = tmp_mvar_value_94;
        tmp_dict_value_92 = LOOKUP_ATTRIBUTE( tmp_source_name_92, const_str_plain_F24 );
        if ( tmp_dict_value_92 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 121;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_92, tmp_dict_value_92 );
        Py_DECREF( tmp_dict_value_92 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_93 = const_str_digest_8f22af8a1de8225021212383433de609;
        tmp_mvar_value_95 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_95 == NULL ))
        {
            tmp_mvar_value_95 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_95 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;

            goto frame_exception_exit_1;
        }

        tmp_source_name_93 = tmp_mvar_value_95;
        tmp_dict_value_93 = LOOKUP_ATTRIBUTE( tmp_source_name_93, const_str_plain_ControlUp );
        if ( tmp_dict_value_93 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 123;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_93, tmp_dict_value_93 );
        Py_DECREF( tmp_dict_value_93 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_94 = const_str_digest_b929d89ddb8b76a9ddaa15631297cae1;
        tmp_mvar_value_96 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_96 == NULL ))
        {
            tmp_mvar_value_96 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_96 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }

        tmp_source_name_94 = tmp_mvar_value_96;
        tmp_dict_value_94 = LOOKUP_ATTRIBUTE( tmp_source_name_94, const_str_plain_ControlDown );
        if ( tmp_dict_value_94 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_94, tmp_dict_value_94 );
        Py_DECREF( tmp_dict_value_94 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_95 = const_str_digest_beb89f81677925e876a6ee02d4a7bfa5;
        tmp_mvar_value_97 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_97 == NULL ))
        {
            tmp_mvar_value_97 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_97 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;

            goto frame_exception_exit_1;
        }

        tmp_source_name_95 = tmp_mvar_value_97;
        tmp_dict_value_95 = LOOKUP_ATTRIBUTE( tmp_source_name_95, const_str_plain_ControlRight );
        if ( tmp_dict_value_95 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 125;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_95, tmp_dict_value_95 );
        Py_DECREF( tmp_dict_value_95 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_96 = const_str_digest_f603c349ec9d7b266d70c8db9806930f;
        tmp_mvar_value_98 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_98 == NULL ))
        {
            tmp_mvar_value_98 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_98 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 126;

            goto frame_exception_exit_1;
        }

        tmp_source_name_96 = tmp_mvar_value_98;
        tmp_dict_value_96 = LOOKUP_ATTRIBUTE( tmp_source_name_96, const_str_plain_ControlLeft );
        if ( tmp_dict_value_96 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 126;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_96, tmp_dict_value_96 );
        Py_DECREF( tmp_dict_value_96 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_97 = const_str_digest_062e4dd920952e7a7a4a51e9e7fddca3;
        tmp_mvar_value_99 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_99 == NULL ))
        {
            tmp_mvar_value_99 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_99 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;

            goto frame_exception_exit_1;
        }

        tmp_source_name_97 = tmp_mvar_value_99;
        tmp_dict_value_97 = LOOKUP_ATTRIBUTE( tmp_source_name_97, const_str_plain_ShiftUp );
        if ( tmp_dict_value_97 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 128;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_97, tmp_dict_value_97 );
        Py_DECREF( tmp_dict_value_97 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_98 = const_str_digest_31d066d6ec77a8b24d805a40b05650fd;
        tmp_mvar_value_100 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_100 == NULL ))
        {
            tmp_mvar_value_100 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_100 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }

        tmp_source_name_98 = tmp_mvar_value_100;
        tmp_dict_value_98 = LOOKUP_ATTRIBUTE( tmp_source_name_98, const_str_plain_ShiftDown );
        if ( tmp_dict_value_98 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_98, tmp_dict_value_98 );
        Py_DECREF( tmp_dict_value_98 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_99 = const_str_digest_822ed9d7bc2ba5596ae8f9a852080479;
        tmp_mvar_value_101 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_101 == NULL ))
        {
            tmp_mvar_value_101 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_101 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;

            goto frame_exception_exit_1;
        }

        tmp_source_name_99 = tmp_mvar_value_101;
        tmp_dict_value_99 = LOOKUP_ATTRIBUTE( tmp_source_name_99, const_str_plain_ShiftRight );
        if ( tmp_dict_value_99 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 130;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_99, tmp_dict_value_99 );
        Py_DECREF( tmp_dict_value_99 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_100 = const_str_digest_de83c66b8e32f7c106b3da21a0e8f37e;
        tmp_mvar_value_102 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_102 == NULL ))
        {
            tmp_mvar_value_102 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_102 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;

            goto frame_exception_exit_1;
        }

        tmp_source_name_100 = tmp_mvar_value_102;
        tmp_dict_value_100 = LOOKUP_ATTRIBUTE( tmp_source_name_100, const_str_plain_ShiftLeft );
        if ( tmp_dict_value_100 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 131;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_100, tmp_dict_value_100 );
        Py_DECREF( tmp_dict_value_100 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_101 = const_str_digest_8ae5e0cc7ab1e2054da9924ef365791d;
        tmp_mvar_value_103 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_103 == NULL ))
        {
            tmp_mvar_value_103 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_103 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;

            goto frame_exception_exit_1;
        }

        tmp_source_name_101 = tmp_mvar_value_103;
        tmp_dict_value_101 = LOOKUP_ATTRIBUTE( tmp_source_name_101, const_str_plain_Up );
        if ( tmp_dict_value_101 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 136;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_101, tmp_dict_value_101 );
        Py_DECREF( tmp_dict_value_101 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_102 = const_str_digest_5d94c1618d14c77c282d74997afa1b74;
        tmp_mvar_value_104 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_104 == NULL ))
        {
            tmp_mvar_value_104 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_104 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 137;

            goto frame_exception_exit_1;
        }

        tmp_source_name_102 = tmp_mvar_value_104;
        tmp_dict_value_102 = LOOKUP_ATTRIBUTE( tmp_source_name_102, const_str_plain_Down );
        if ( tmp_dict_value_102 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 137;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_102, tmp_dict_value_102 );
        Py_DECREF( tmp_dict_value_102 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_103 = const_str_digest_cff27188ef75050fc3667312cd5d4bc0;
        tmp_mvar_value_105 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_105 == NULL ))
        {
            tmp_mvar_value_105 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_105 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;

            goto frame_exception_exit_1;
        }

        tmp_source_name_103 = tmp_mvar_value_105;
        tmp_dict_value_103 = LOOKUP_ATTRIBUTE( tmp_source_name_103, const_str_plain_Right );
        if ( tmp_dict_value_103 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 138;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_103, tmp_dict_value_103 );
        Py_DECREF( tmp_dict_value_103 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_104 = const_str_digest_28eb87ecf432c77c3f1bc6cfad381297;
        tmp_mvar_value_106 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_106 == NULL ))
        {
            tmp_mvar_value_106 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_106 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;

            goto frame_exception_exit_1;
        }

        tmp_source_name_104 = tmp_mvar_value_106;
        tmp_dict_value_104 = LOOKUP_ATTRIBUTE( tmp_source_name_104, const_str_plain_Left );
        if ( tmp_dict_value_104 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 139;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_104, tmp_dict_value_104 );
        Py_DECREF( tmp_dict_value_104 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_105 = const_str_digest_bee2ae49567cb9050d7a4436343b7933;
        tmp_mvar_value_107 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_107 == NULL ))
        {
            tmp_mvar_value_107 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_107 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 141;

            goto frame_exception_exit_1;
        }

        tmp_source_name_105 = tmp_mvar_value_107;
        tmp_dict_value_105 = LOOKUP_ATTRIBUTE( tmp_source_name_105, const_str_plain_ControlUp );
        if ( tmp_dict_value_105 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 141;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_105, tmp_dict_value_105 );
        Py_DECREF( tmp_dict_value_105 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_106 = const_str_digest_c1d355aa7a615f4a5c0d2e57a72621a5;
        tmp_mvar_value_108 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_108 == NULL ))
        {
            tmp_mvar_value_108 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_108 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }

        tmp_source_name_106 = tmp_mvar_value_108;
        tmp_dict_value_106 = LOOKUP_ATTRIBUTE( tmp_source_name_106, const_str_plain_ControlDown );
        if ( tmp_dict_value_106 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_106, tmp_dict_value_106 );
        Py_DECREF( tmp_dict_value_106 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_107 = const_str_digest_e4a4aada2698ea56536cee7a44a860f1;
        tmp_mvar_value_109 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_109 == NULL ))
        {
            tmp_mvar_value_109 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_109 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;

            goto frame_exception_exit_1;
        }

        tmp_source_name_107 = tmp_mvar_value_109;
        tmp_dict_value_107 = LOOKUP_ATTRIBUTE( tmp_source_name_107, const_str_plain_ControlRight );
        if ( tmp_dict_value_107 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 143;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_107, tmp_dict_value_107 );
        Py_DECREF( tmp_dict_value_107 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_108 = const_str_digest_acfa066df9d1ac14be92ac4f20bc315f;
        tmp_mvar_value_110 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_110 == NULL ))
        {
            tmp_mvar_value_110 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_110 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 144;

            goto frame_exception_exit_1;
        }

        tmp_source_name_108 = tmp_mvar_value_110;
        tmp_dict_value_108 = LOOKUP_ATTRIBUTE( tmp_source_name_108, const_str_plain_ControlLeft );
        if ( tmp_dict_value_108 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 144;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_108, tmp_dict_value_108 );
        Py_DECREF( tmp_dict_value_108 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_109 = const_str_digest_28534181036c43d70a84ccb606b5cd8f;
        tmp_mvar_value_111 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_111 == NULL ))
        {
            tmp_mvar_value_111 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_111 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }

        tmp_source_name_109 = tmp_mvar_value_111;
        tmp_dict_value_109 = LOOKUP_ATTRIBUTE( tmp_source_name_109, const_str_plain_ControlRight );
        if ( tmp_dict_value_109 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_109, tmp_dict_value_109 );
        Py_DECREF( tmp_dict_value_109 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_110 = const_str_digest_c56c5c9fd4919c9e8a403467a2a24736;
        tmp_mvar_value_112 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_112 == NULL ))
        {
            tmp_mvar_value_112 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_112 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }

        tmp_source_name_110 = tmp_mvar_value_112;
        tmp_dict_value_110 = LOOKUP_ATTRIBUTE( tmp_source_name_110, const_str_plain_ControlLeft );
        if ( tmp_dict_value_110 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_110, tmp_dict_value_110 );
        Py_DECREF( tmp_dict_value_110 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_111 = const_str_digest_13fd536cb3770b7dff6b2f1863eeb53c;
        tmp_mvar_value_113 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_113 == NULL ))
        {
            tmp_mvar_value_113 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_113 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;

            goto frame_exception_exit_1;
        }

        tmp_source_name_111 = tmp_mvar_value_113;
        tmp_dict_value_111 = LOOKUP_ATTRIBUTE( tmp_source_name_111, const_str_plain_ScrollUp );
        if ( tmp_dict_value_111 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 150;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_111, tmp_dict_value_111 );
        Py_DECREF( tmp_dict_value_111 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_112 = const_str_digest_999606f940dae79c7c12722f054004a5;
        tmp_mvar_value_114 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_114 == NULL ))
        {
            tmp_mvar_value_114 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_114 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 151;

            goto frame_exception_exit_1;
        }

        tmp_source_name_112 = tmp_mvar_value_114;
        tmp_dict_value_112 = LOOKUP_ATTRIBUTE( tmp_source_name_112, const_str_plain_ScrollDown );
        if ( tmp_dict_value_112 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 151;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_112, tmp_dict_value_112 );
        Py_DECREF( tmp_dict_value_112 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_113 = const_str_digest_c8f27a7f54392ae4a09ee9a266151602;
        tmp_mvar_value_115 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_115 == NULL ))
        {
            tmp_mvar_value_115 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_115 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 153;

            goto frame_exception_exit_1;
        }

        tmp_source_name_113 = tmp_mvar_value_115;
        tmp_dict_value_113 = LOOKUP_ATTRIBUTE( tmp_source_name_113, const_str_plain_BracketedPaste );
        if ( tmp_dict_value_113 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 153;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_113, tmp_dict_value_113 );
        Py_DECREF( tmp_dict_value_113 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_114 = const_str_digest_9510cccc23d088e9fb7184961464a33a;
        tmp_mvar_value_116 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_116 == NULL ))
        {
            tmp_mvar_value_116 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_116 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_source_name_114 = tmp_mvar_value_116;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_114, const_str_plain_Escape );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_114 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_114, 0, tmp_tuple_element_1 );
        tmp_mvar_value_117 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_117 == NULL ))
        {
            tmp_mvar_value_117 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_117 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_114 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_source_name_115 = tmp_mvar_value_117;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_115, const_str_plain_Left );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_114 );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_dict_value_114, 1, tmp_tuple_element_1 );
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_114, tmp_dict_value_114 );
        Py_DECREF( tmp_dict_value_114 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_115 = const_str_digest_07a71e66c22d8f31faf91779ad8c6612;
        tmp_mvar_value_118 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_118 == NULL ))
        {
            tmp_mvar_value_118 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_118 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }

        tmp_source_name_116 = tmp_mvar_value_118;
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_116, const_str_plain_Escape );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_115 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_115, 0, tmp_tuple_element_2 );
        tmp_mvar_value_119 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_119 == NULL ))
        {
            tmp_mvar_value_119 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_119 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_115 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }

        tmp_source_name_117 = tmp_mvar_value_119;
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_117, const_str_plain_Right );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_115 );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_dict_value_115, 1, tmp_tuple_element_2 );
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_115, tmp_dict_value_115 );
        Py_DECREF( tmp_dict_value_115 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_116 = const_str_digest_a7d3e33968f5a62c32c2139201127de4;
        tmp_mvar_value_120 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_120 == NULL ))
        {
            tmp_mvar_value_120 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_120 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_source_name_118 = tmp_mvar_value_120;
        tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_118, const_str_plain_Escape );
        if ( tmp_tuple_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_116 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_116, 0, tmp_tuple_element_3 );
        tmp_mvar_value_121 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_121 == NULL ))
        {
            tmp_mvar_value_121 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_121 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_116 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_source_name_119 = tmp_mvar_value_121;
        tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_119, const_str_plain_Up );
        if ( tmp_tuple_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_116 );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_dict_value_116, 1, tmp_tuple_element_3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_116, tmp_dict_value_116 );
        Py_DECREF( tmp_dict_value_116 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_117 = const_str_digest_0189d4926f5491d9b828b71ec2427458;
        tmp_mvar_value_122 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_122 == NULL ))
        {
            tmp_mvar_value_122 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_122 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_source_name_120 = tmp_mvar_value_122;
        tmp_tuple_element_4 = LOOKUP_ATTRIBUTE( tmp_source_name_120, const_str_plain_Escape );
        if ( tmp_tuple_element_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_117 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_117, 0, tmp_tuple_element_4 );
        tmp_mvar_value_123 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_123 == NULL ))
        {
            tmp_mvar_value_123 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_123 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_117 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_source_name_121 = tmp_mvar_value_123;
        tmp_tuple_element_4 = LOOKUP_ATTRIBUTE( tmp_source_name_121, const_str_plain_Down );
        if ( tmp_tuple_element_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_117 );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_dict_value_117, 1, tmp_tuple_element_4 );
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_117, tmp_dict_value_117 );
        Py_DECREF( tmp_dict_value_117 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_118 = const_str_digest_ba9fd5f52ba66ab48d226dc44f96fb84;
        tmp_mvar_value_124 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_124 == NULL ))
        {
            tmp_mvar_value_124 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_124 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }

        tmp_source_name_122 = tmp_mvar_value_124;
        tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_122, const_str_plain_Escape );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_118 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_118, 0, tmp_tuple_element_5 );
        tmp_mvar_value_125 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_125 == NULL ))
        {
            tmp_mvar_value_125 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_125 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_118 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }

        tmp_source_name_123 = tmp_mvar_value_125;
        tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_123, const_str_plain_Left );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_118 );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_dict_value_118, 1, tmp_tuple_element_5 );
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_118, tmp_dict_value_118 );
        Py_DECREF( tmp_dict_value_118 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_119 = const_str_digest_32243c0fdd5ee0c73bb689eb2bde2045;
        tmp_mvar_value_126 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_126 == NULL ))
        {
            tmp_mvar_value_126 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_126 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }

        tmp_source_name_124 = tmp_mvar_value_126;
        tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_124, const_str_plain_Escape );
        if ( tmp_tuple_element_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_119 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_119, 0, tmp_tuple_element_6 );
        tmp_mvar_value_127 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_127 == NULL ))
        {
            tmp_mvar_value_127 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_127 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_119 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }

        tmp_source_name_125 = tmp_mvar_value_127;
        tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_125, const_str_plain_Right );
        if ( tmp_tuple_element_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );
            Py_DECREF( tmp_dict_value_119 );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_dict_value_119, 1, tmp_tuple_element_6 );
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_119, tmp_dict_value_119 );
        Py_DECREF( tmp_dict_value_119 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_120 = const_str_digest_95a4e7fd4e59cff6239485de1ad0e5c7;
        tmp_mvar_value_128 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_128 == NULL ))
        {
            tmp_mvar_value_128 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_128 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;

            goto frame_exception_exit_1;
        }

        tmp_source_name_126 = tmp_mvar_value_128;
        tmp_dict_value_120 = LOOKUP_ATTRIBUTE( tmp_source_name_126, const_str_plain_Ignore );
        if ( tmp_dict_value_120 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 176;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_120, tmp_dict_value_120 );
        Py_DECREF( tmp_dict_value_120 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_121 = const_str_digest_5de5b2adce51faf092210ff0a12b20ac;
        tmp_mvar_value_129 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_129 == NULL ))
        {
            tmp_mvar_value_129 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_129 == NULL )
        {
            Py_DECREF( tmp_assign_source_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 177;

            goto frame_exception_exit_1;
        }

        tmp_source_name_127 = tmp_mvar_value_129;
        tmp_dict_value_121 = LOOKUP_ATTRIBUTE( tmp_source_name_127, const_str_plain_Ignore );
        if ( tmp_dict_value_121 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_7 );

            exception_lineno = 177;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_7, tmp_dict_key_121, tmp_dict_value_121 );
        Py_DECREF( tmp_dict_value_121 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_ANSI_SEQUENCES, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = MAKE_FUNCTION_prompt_toolkit$input$ansi_escape_sequences$$$function_1__get_reverse_ansi_sequences(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain__get_reverse_ansi_sequences, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_130;
        tmp_mvar_value_130 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain__get_reverse_ansi_sequences );

        if (unlikely( tmp_mvar_value_130 == NULL ))
        {
            tmp_mvar_value_130 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_reverse_ansi_sequences );
        }

        CHECK_OBJECT( tmp_mvar_value_130 );
        tmp_called_name_1 = tmp_mvar_value_130;
        frame_4d437a8d6dae6793ee53855afd1ebcd3->m_frame.f_lineno = 196;
        tmp_assign_source_9 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$ansi_escape_sequences, (Nuitka_StringObject *)const_str_plain_REVERSE_ANSI_SEQUENCES, tmp_assign_source_9 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4d437a8d6dae6793ee53855afd1ebcd3 );
#endif
    popFrameStack();

    assertFrameObject( frame_4d437a8d6dae6793ee53855afd1ebcd3 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4d437a8d6dae6793ee53855afd1ebcd3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4d437a8d6dae6793ee53855afd1ebcd3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4d437a8d6dae6793ee53855afd1ebcd3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4d437a8d6dae6793ee53855afd1ebcd3, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_prompt_toolkit$input$ansi_escape_sequences );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
