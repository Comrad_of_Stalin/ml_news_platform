/* Generated code for Python module 'prompt_toolkit.layout'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$layout" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$layout;
PyDictObject *moduledict_prompt_toolkit$layout;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_MultiColumnCompletionsMenu;
extern PyObject *const_str_plain_walk;
extern PyObject *const_str_plain___spec__;
static PyObject *const_list_6d19bd168aedc8c770efc9770862d79d_list;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_D;
extern PyObject *const_str_plain_HSplit;
extern PyObject *const_str_plain_Layout;
extern PyObject *const_str_plain_sum_layout_dimensions;
extern PyObject *const_str_plain_is_dimension;
extern PyObject *const_tuple_1f986959afc177ecff2266ea95788ab6_tuple;
extern PyObject *const_tuple_6c6f5ead7af16d2f11323080b4d91f21_tuple;
extern PyObject *const_str_plain_layout;
extern PyObject *const_str_plain_ScrollbarMargin;
extern PyObject *const_str_plain_VSplit;
extern PyObject *const_str_plain_VerticalAlign;
extern PyObject *const_str_plain_UIContent;
static PyObject *const_str_digest_3c46c31ef05c88c5c220412e1fa00be7;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_UIControl;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_environ;
extern PyObject *const_str_plain_dimension;
static PyObject *const_tuple_54c4646738ac1e0a69116d6340ca3041_tuple;
extern PyObject *const_str_plain_WindowAlign;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_plain_Float;
extern PyObject *const_str_plain___path__;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_PromptMargin;
extern PyObject *const_str_plain_to_dimension;
extern PyObject *const_str_plain_menus;
extern PyObject *const_str_plain_max_layout_dimensions;
static PyObject *const_tuple_b2087b7b3870f8aa842d8bf91e1824e3_tuple;
extern PyObject *const_str_digest_388882ed5238bd9499b19d10d66c5ccd;
extern PyObject *const_str_plain_HorizontalAlign;
static PyObject *const_str_digest_bd012c3516ca6a5e99140055a6390283;
extern PyObject *const_str_plain_NUITKA_PACKAGE_prompt_toolkit;
static PyObject *const_str_plain_NUITKA_PACKAGE_prompt_toolkit_layout;
extern PyObject *const_str_plain_Window;
extern PyObject *const_str_plain___all__;
static PyObject *const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple;
extern PyObject *const_str_plain_InvalidLayoutError;
extern PyObject *const_str_plain_ConditionalMargin;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_controls;
extern PyObject *const_str_plain_FloatContainer;
extern PyObject *const_str_plain_SearchBufferControl;
extern PyObject *const_str_plain_dirname;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_FormattedTextControl;
extern PyObject *const_str_plain_WindowRenderInfo;
extern PyObject *const_str_plain_CompletionsMenu;
extern PyObject *const_str_plain_DummyControl;
extern PyObject *const_str_plain_is_container;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_to_window;
extern PyObject *const_str_plain_margins;
extern PyObject *const_str_plain_ColorColumn;
extern PyObject *const_str_plain_BufferControl;
extern PyObject *const_int_pos_1;
static PyObject *const_tuple_str_plain_Layout_str_plain_InvalidLayoutError_str_plain_walk_tuple;
extern PyObject *const_str_plain_Container;
extern PyObject *const_str_plain_ScrollOffsets;
extern PyObject *const_str_plain_Dimension;
extern PyObject *const_str_plain_to_container;
extern PyObject *const_str_plain_Margin;
static PyObject *const_str_digest_73e80b7e39aa5daebc469322c565a252;
extern PyObject *const_str_plain_containers;
static PyObject *const_tuple_f302a26371e233e27e61f2516396faf9_tuple;
static PyObject *const_tuple_b711efd52ef8b2e4923368c5d13219f8_tuple;
extern PyObject *const_str_digest_5bfaf90dbd407b4fc29090c8f6415242;
extern PyObject *const_str_plain_NumberedMargin;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_DynamicContainer;
extern PyObject *const_str_plain_ConditionalContainer;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_list_6d19bd168aedc8c770efc9770862d79d_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 4738587 ], 568 );
    const_str_digest_3c46c31ef05c88c5c220412e1fa00be7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4739155 ], 30, 0 );
    const_tuple_54c4646738ac1e0a69116d6340ca3041_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_54c4646738ac1e0a69116d6340ca3041_tuple, 0, const_str_plain_BufferControl ); Py_INCREF( const_str_plain_BufferControl );
    PyTuple_SET_ITEM( const_tuple_54c4646738ac1e0a69116d6340ca3041_tuple, 1, const_str_plain_SearchBufferControl ); Py_INCREF( const_str_plain_SearchBufferControl );
    PyTuple_SET_ITEM( const_tuple_54c4646738ac1e0a69116d6340ca3041_tuple, 2, const_str_plain_DummyControl ); Py_INCREF( const_str_plain_DummyControl );
    PyTuple_SET_ITEM( const_tuple_54c4646738ac1e0a69116d6340ca3041_tuple, 3, const_str_plain_FormattedTextControl ); Py_INCREF( const_str_plain_FormattedTextControl );
    PyTuple_SET_ITEM( const_tuple_54c4646738ac1e0a69116d6340ca3041_tuple, 4, const_str_plain_UIControl ); Py_INCREF( const_str_plain_UIControl );
    PyTuple_SET_ITEM( const_tuple_54c4646738ac1e0a69116d6340ca3041_tuple, 5, const_str_plain_UIContent ); Py_INCREF( const_str_plain_UIContent );
    const_tuple_b2087b7b3870f8aa842d8bf91e1824e3_tuple = PyTuple_New( 2 );
    const_str_plain_NUITKA_PACKAGE_prompt_toolkit_layout = UNSTREAM_STRING_ASCII( &constant_bin[ 4739185 ], 36, 1 );
    PyTuple_SET_ITEM( const_tuple_b2087b7b3870f8aa842d8bf91e1824e3_tuple, 0, const_str_plain_NUITKA_PACKAGE_prompt_toolkit_layout ); Py_INCREF( const_str_plain_NUITKA_PACKAGE_prompt_toolkit_layout );
    PyTuple_SET_ITEM( const_tuple_b2087b7b3870f8aa842d8bf91e1824e3_tuple, 1, const_str_digest_5bfaf90dbd407b4fc29090c8f6415242 ); Py_INCREF( const_str_digest_5bfaf90dbd407b4fc29090c8f6415242 );
    const_str_digest_bd012c3516ca6a5e99140055a6390283 = UNSTREAM_STRING_ASCII( &constant_bin[ 4739221 ], 33, 0 );
    const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple = PyTuple_New( 17 );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 0, const_str_plain_Container ); Py_INCREF( const_str_plain_Container );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 1, const_str_plain_HSplit ); Py_INCREF( const_str_plain_HSplit );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 2, const_str_plain_VSplit ); Py_INCREF( const_str_plain_VSplit );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 3, const_str_plain_FloatContainer ); Py_INCREF( const_str_plain_FloatContainer );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 4, const_str_plain_Float ); Py_INCREF( const_str_plain_Float );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 5, const_str_plain_Window ); Py_INCREF( const_str_plain_Window );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 6, const_str_plain_WindowAlign ); Py_INCREF( const_str_plain_WindowAlign );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 7, const_str_plain_WindowRenderInfo ); Py_INCREF( const_str_plain_WindowRenderInfo );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 8, const_str_plain_ConditionalContainer ); Py_INCREF( const_str_plain_ConditionalContainer );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 9, const_str_plain_ScrollOffsets ); Py_INCREF( const_str_plain_ScrollOffsets );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 10, const_str_plain_ColorColumn ); Py_INCREF( const_str_plain_ColorColumn );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 11, const_str_plain_to_container ); Py_INCREF( const_str_plain_to_container );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 12, const_str_plain_to_window ); Py_INCREF( const_str_plain_to_window );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 13, const_str_plain_is_container ); Py_INCREF( const_str_plain_is_container );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 14, const_str_plain_HorizontalAlign ); Py_INCREF( const_str_plain_HorizontalAlign );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 15, const_str_plain_VerticalAlign ); Py_INCREF( const_str_plain_VerticalAlign );
    PyTuple_SET_ITEM( const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple, 16, const_str_plain_DynamicContainer ); Py_INCREF( const_str_plain_DynamicContainer );
    const_tuple_str_plain_Layout_str_plain_InvalidLayoutError_str_plain_walk_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Layout_str_plain_InvalidLayoutError_str_plain_walk_tuple, 0, const_str_plain_Layout ); Py_INCREF( const_str_plain_Layout );
    PyTuple_SET_ITEM( const_tuple_str_plain_Layout_str_plain_InvalidLayoutError_str_plain_walk_tuple, 1, const_str_plain_InvalidLayoutError ); Py_INCREF( const_str_plain_InvalidLayoutError );
    PyTuple_SET_ITEM( const_tuple_str_plain_Layout_str_plain_InvalidLayoutError_str_plain_walk_tuple, 2, const_str_plain_walk ); Py_INCREF( const_str_plain_walk );
    const_str_digest_73e80b7e39aa5daebc469322c565a252 = UNSTREAM_STRING_ASCII( &constant_bin[ 4739254 ], 1669, 0 );
    const_tuple_f302a26371e233e27e61f2516396faf9_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_f302a26371e233e27e61f2516396faf9_tuple, 0, const_str_plain_Dimension ); Py_INCREF( const_str_plain_Dimension );
    PyTuple_SET_ITEM( const_tuple_f302a26371e233e27e61f2516396faf9_tuple, 1, const_str_plain_D ); Py_INCREF( const_str_plain_D );
    PyTuple_SET_ITEM( const_tuple_f302a26371e233e27e61f2516396faf9_tuple, 2, const_str_plain_sum_layout_dimensions ); Py_INCREF( const_str_plain_sum_layout_dimensions );
    PyTuple_SET_ITEM( const_tuple_f302a26371e233e27e61f2516396faf9_tuple, 3, const_str_plain_max_layout_dimensions ); Py_INCREF( const_str_plain_max_layout_dimensions );
    PyTuple_SET_ITEM( const_tuple_f302a26371e233e27e61f2516396faf9_tuple, 4, const_str_plain_to_dimension ); Py_INCREF( const_str_plain_to_dimension );
    PyTuple_SET_ITEM( const_tuple_f302a26371e233e27e61f2516396faf9_tuple, 5, const_str_plain_is_dimension ); Py_INCREF( const_str_plain_is_dimension );
    const_tuple_b711efd52ef8b2e4923368c5d13219f8_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b711efd52ef8b2e4923368c5d13219f8_tuple, 0, const_str_plain_Margin ); Py_INCREF( const_str_plain_Margin );
    PyTuple_SET_ITEM( const_tuple_b711efd52ef8b2e4923368c5d13219f8_tuple, 1, const_str_plain_NumberedMargin ); Py_INCREF( const_str_plain_NumberedMargin );
    PyTuple_SET_ITEM( const_tuple_b711efd52ef8b2e4923368c5d13219f8_tuple, 2, const_str_plain_ScrollbarMargin ); Py_INCREF( const_str_plain_ScrollbarMargin );
    PyTuple_SET_ITEM( const_tuple_b711efd52ef8b2e4923368c5d13219f8_tuple, 3, const_str_plain_ConditionalMargin ); Py_INCREF( const_str_plain_ConditionalMargin );
    PyTuple_SET_ITEM( const_tuple_b711efd52ef8b2e4923368c5d13219f8_tuple, 4, const_str_plain_PromptMargin ); Py_INCREF( const_str_plain_PromptMargin );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$layout( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_9b0d4ada9fd304fc13f68ad9af7ab479;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_bd012c3516ca6a5e99140055a6390283 );
    codeobj_9b0d4ada9fd304fc13f68ad9af7ab479 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_3c46c31ef05c88c5c220412e1fa00be7, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.


// The module function definitions.



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$layout =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.layout",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$layout)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$layout)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$layout );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.layout: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.layout: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.layout: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$layout" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$layout = Py_InitModule4(
        "prompt_toolkit.layout",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$layout = PyModule_Create( &mdef_prompt_toolkit$layout );
#endif

    moduledict_prompt_toolkit$layout = MODULE_DICT( module_prompt_toolkit$layout );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$layout,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 1
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$layout,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$layout,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$layout,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$layout );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_388882ed5238bd9499b19d10d66c5ccd, module_prompt_toolkit$layout );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 1
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    PyObject *tmp_import_from_5__module = NULL;
    PyObject *tmp_import_from_6__module = NULL;
    struct Nuitka_FrameObject *frame_9b0d4ada9fd304fc13f68ad9af7ab479;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_73e80b7e39aa5daebc469322c565a252;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_9b0d4ada9fd304fc13f68ad9af7ab479 = MAKE_MODULE_FRAME( codeobj_9b0d4ada9fd304fc13f68ad9af7ab479, module_prompt_toolkit$layout );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_9b0d4ada9fd304fc13f68ad9af7ab479 );
    assert( Py_REFCNT( frame_9b0d4ada9fd304fc13f68ad9af7ab479 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_2;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_source_name_1 = PyObject_GetAttr( module, const_str_plain_path );
            }
            else
            {
                tmp_source_name_1 = NULL;
            }
        }

        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_dirname );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = module_filename_obj;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 1;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = PyList_New( 3 );
        PyList_SET_ITEM( tmp_assign_source_3, 0, tmp_list_element_1 );
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_source_name_2 = PyObject_GetAttr( module, const_str_plain_path );
            }
            else
            {
                tmp_source_name_2 = NULL;
            }
        }

        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_join );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_called_instance_1 = PyObject_GetAttr( module, const_str_plain_environ );
            }
            else
            {
                tmp_called_instance_1 = NULL;
            }
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 1;
        tmp_args_element_name_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_1f986959afc177ecff2266ea95788ab6_tuple, 0 ) );

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = const_str_plain_layout;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 1;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assign_source_3, 1, tmp_list_element_1 );
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_called_instance_2 = PyObject_GetAttr( module, const_str_plain_environ );
            }
            else
            {
                tmp_called_instance_2 = NULL;
            }
        }

        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 1;
        tmp_list_element_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_b2087b7b3870f8aa842d8bf91e1824e3_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assign_source_3, 2, tmp_list_element_1 );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___path__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 47;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_containers;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$layout;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_f29e5b75d7f890395b0218dc1231c836_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 48;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_7 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_Container,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Container );
        }

        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_Container, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_3 ) )
        {
           tmp_assign_source_8 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_3,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_HSplit,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_HSplit );
        }

        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_HSplit, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_4 ) )
        {
           tmp_assign_source_9 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_4,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_VSplit,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_VSplit );
        }

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_VSplit, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_5 ) )
        {
           tmp_assign_source_10 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_5,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_FloatContainer,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_FloatContainer );
        }

        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_FloatContainer, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_6 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_6 ) )
        {
           tmp_assign_source_11 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_6,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_Float,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_Float );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_Float, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_7 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_7 ) )
        {
           tmp_assign_source_12 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_7,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_Window,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_Window );
        }

        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_Window, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_8 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_13 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_WindowAlign,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_WindowAlign );
        }

        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_WindowAlign, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_9 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_9 ) )
        {
           tmp_assign_source_14 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_9,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_WindowRenderInfo,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_WindowRenderInfo );
        }

        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_WindowRenderInfo, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_10 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_10 ) )
        {
           tmp_assign_source_15 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_10,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_ConditionalContainer,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_ConditionalContainer );
        }

        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_ConditionalContainer, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_11 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_11 ) )
        {
           tmp_assign_source_16 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_11,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_ScrollOffsets,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_ScrollOffsets );
        }

        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_ScrollOffsets, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_12 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_12 ) )
        {
           tmp_assign_source_17 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_12,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_ColorColumn,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_ColorColumn );
        }

        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_ColorColumn, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_13 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_13 ) )
        {
           tmp_assign_source_18 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_13,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_to_container,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_to_container );
        }

        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_to_container, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_14 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_14 ) )
        {
           tmp_assign_source_19 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_14,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_to_window,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_to_window );
        }

        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_to_window, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_15;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_15 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_15 ) )
        {
           tmp_assign_source_20 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_15,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_is_container,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_is_container );
        }

        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_is_container, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_16;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_16 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_16 ) )
        {
           tmp_assign_source_21 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_16,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_HorizontalAlign,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_HorizontalAlign );
        }

        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_HorizontalAlign, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_17;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_17 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_17 ) )
        {
           tmp_assign_source_22 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_17,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_VerticalAlign,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_VerticalAlign );
        }

        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_VerticalAlign, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_18;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_18 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_18 ) )
        {
           tmp_assign_source_23 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_18,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_DynamicContainer,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_DynamicContainer );
        }

        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_DynamicContainer, tmp_assign_source_23 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_controls;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$layout;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_54c4646738ac1e0a69116d6340ca3041_tuple;
        tmp_level_name_2 = const_int_pos_1;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 49;
        tmp_assign_source_24 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_24;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_import_name_from_19;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_19 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_19 ) )
        {
           tmp_assign_source_25 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_19,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_BufferControl,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_BufferControl );
        }

        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_BufferControl, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_20;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_20 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_20 ) )
        {
           tmp_assign_source_26 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_20,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_SearchBufferControl,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_SearchBufferControl );
        }

        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_SearchBufferControl, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_import_name_from_21;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_21 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_21 ) )
        {
           tmp_assign_source_27 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_21,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_DummyControl,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_DummyControl );
        }

        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_DummyControl, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_import_name_from_22;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_22 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_22 ) )
        {
           tmp_assign_source_28 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_22,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_FormattedTextControl,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_FormattedTextControl );
        }

        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_FormattedTextControl, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_import_name_from_23;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_23 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_23 ) )
        {
           tmp_assign_source_29 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_23,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_UIControl,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_23, const_str_plain_UIControl );
        }

        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_UIControl, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_import_name_from_24;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_24 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_24 ) )
        {
           tmp_assign_source_30 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_24,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_UIContent,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_30 = IMPORT_NAME( tmp_import_name_from_24, const_str_plain_UIContent );
        }

        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_UIContent, tmp_assign_source_30 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_dimension;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$layout;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_f302a26371e233e27e61f2516396faf9_tuple;
        tmp_level_name_3 = const_int_pos_1;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 50;
        tmp_assign_source_31 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_31;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_import_name_from_25;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_25 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_25 ) )
        {
           tmp_assign_source_32 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_25,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_Dimension,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_32 = IMPORT_NAME( tmp_import_name_from_25, const_str_plain_Dimension );
        }

        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_Dimension, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_import_name_from_26;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_26 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_26 ) )
        {
           tmp_assign_source_33 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_26,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_D,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_33 = IMPORT_NAME( tmp_import_name_from_26, const_str_plain_D );
        }

        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_D, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_import_name_from_27;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_27 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_27 ) )
        {
           tmp_assign_source_34 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_27,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_sum_layout_dimensions,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_34 = IMPORT_NAME( tmp_import_name_from_27, const_str_plain_sum_layout_dimensions );
        }

        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_sum_layout_dimensions, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_import_name_from_28;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_28 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_28 ) )
        {
           tmp_assign_source_35 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_28,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_max_layout_dimensions,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_35 = IMPORT_NAME( tmp_import_name_from_28, const_str_plain_max_layout_dimensions );
        }

        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_max_layout_dimensions, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_import_name_from_29;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_29 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_29 ) )
        {
           tmp_assign_source_36 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_29,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_to_dimension,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_36 = IMPORT_NAME( tmp_import_name_from_29, const_str_plain_to_dimension );
        }

        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_to_dimension, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_import_name_from_30;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_30 = tmp_import_from_3__module;
        if ( PyModule_Check( tmp_import_name_from_30 ) )
        {
           tmp_assign_source_37 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_30,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_is_dimension,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_37 = IMPORT_NAME( tmp_import_name_from_30, const_str_plain_is_dimension );
        }

        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_is_dimension, tmp_assign_source_37 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_layout;
        tmp_globals_name_4 = (PyObject *)moduledict_prompt_toolkit$layout;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_Layout_str_plain_InvalidLayoutError_str_plain_walk_tuple;
        tmp_level_name_4 = const_int_pos_1;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 51;
        tmp_assign_source_38 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_4__module == NULL );
        tmp_import_from_4__module = tmp_assign_source_38;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_import_name_from_31;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_31 = tmp_import_from_4__module;
        if ( PyModule_Check( tmp_import_name_from_31 ) )
        {
           tmp_assign_source_39 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_31,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_Layout,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_39 = IMPORT_NAME( tmp_import_name_from_31, const_str_plain_Layout );
        }

        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_Layout, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_import_name_from_32;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_32 = tmp_import_from_4__module;
        if ( PyModule_Check( tmp_import_name_from_32 ) )
        {
           tmp_assign_source_40 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_32,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_InvalidLayoutError,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_40 = IMPORT_NAME( tmp_import_name_from_32, const_str_plain_InvalidLayoutError );
        }

        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_InvalidLayoutError, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_import_name_from_33;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_33 = tmp_import_from_4__module;
        if ( PyModule_Check( tmp_import_name_from_33 ) )
        {
           tmp_assign_source_41 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_33,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_walk,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_41 = IMPORT_NAME( tmp_import_name_from_33, const_str_plain_walk );
        }

        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_walk, tmp_assign_source_41 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_margins;
        tmp_globals_name_5 = (PyObject *)moduledict_prompt_toolkit$layout;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_b711efd52ef8b2e4923368c5d13219f8_tuple;
        tmp_level_name_5 = const_int_pos_1;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 52;
        tmp_assign_source_42 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_5__module == NULL );
        tmp_import_from_5__module = tmp_assign_source_42;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_import_name_from_34;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_34 = tmp_import_from_5__module;
        if ( PyModule_Check( tmp_import_name_from_34 ) )
        {
           tmp_assign_source_43 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_34,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_Margin,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_43 = IMPORT_NAME( tmp_import_name_from_34, const_str_plain_Margin );
        }

        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_Margin, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_import_name_from_35;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_35 = tmp_import_from_5__module;
        if ( PyModule_Check( tmp_import_name_from_35 ) )
        {
           tmp_assign_source_44 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_35,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_NumberedMargin,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_44 = IMPORT_NAME( tmp_import_name_from_35, const_str_plain_NumberedMargin );
        }

        if ( tmp_assign_source_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_NumberedMargin, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_import_name_from_36;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_36 = tmp_import_from_5__module;
        if ( PyModule_Check( tmp_import_name_from_36 ) )
        {
           tmp_assign_source_45 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_36,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_ScrollbarMargin,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_45 = IMPORT_NAME( tmp_import_name_from_36, const_str_plain_ScrollbarMargin );
        }

        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_ScrollbarMargin, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_import_name_from_37;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_37 = tmp_import_from_5__module;
        if ( PyModule_Check( tmp_import_name_from_37 ) )
        {
           tmp_assign_source_46 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_37,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_ConditionalMargin,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_46 = IMPORT_NAME( tmp_import_name_from_37, const_str_plain_ConditionalMargin );
        }

        if ( tmp_assign_source_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_ConditionalMargin, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_import_name_from_38;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_38 = tmp_import_from_5__module;
        if ( PyModule_Check( tmp_import_name_from_38 ) )
        {
           tmp_assign_source_47 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_38,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_PromptMargin,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_47 = IMPORT_NAME( tmp_import_name_from_38, const_str_plain_PromptMargin );
        }

        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_PromptMargin, tmp_assign_source_47 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_menus;
        tmp_globals_name_6 = (PyObject *)moduledict_prompt_toolkit$layout;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_6c6f5ead7af16d2f11323080b4d91f21_tuple;
        tmp_level_name_6 = const_int_pos_1;
        frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame.f_lineno = 53;
        tmp_assign_source_48 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_6__module == NULL );
        tmp_import_from_6__module = tmp_assign_source_48;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_import_name_from_39;
        CHECK_OBJECT( tmp_import_from_6__module );
        tmp_import_name_from_39 = tmp_import_from_6__module;
        if ( PyModule_Check( tmp_import_name_from_39 ) )
        {
           tmp_assign_source_49 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_39,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_CompletionsMenu,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_49 = IMPORT_NAME( tmp_import_name_from_39, const_str_plain_CompletionsMenu );
        }

        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_6;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_CompletionsMenu, tmp_assign_source_49 );
    }
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_import_name_from_40;
        CHECK_OBJECT( tmp_import_from_6__module );
        tmp_import_name_from_40 = tmp_import_from_6__module;
        if ( PyModule_Check( tmp_import_name_from_40 ) )
        {
           tmp_assign_source_50 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_40,
                (PyObject *)moduledict_prompt_toolkit$layout,
                const_str_plain_MultiColumnCompletionsMenu,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_50 = IMPORT_NAME( tmp_import_name_from_40, const_str_plain_MultiColumnCompletionsMenu );
        }

        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_6;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain_MultiColumnCompletionsMenu, tmp_assign_source_50 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
    Py_DECREF( tmp_import_from_6__module );
    tmp_import_from_6__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b0d4ada9fd304fc13f68ad9af7ab479 );
#endif
    popFrameStack();

    assertFrameObject( frame_9b0d4ada9fd304fc13f68ad9af7ab479 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b0d4ada9fd304fc13f68ad9af7ab479 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9b0d4ada9fd304fc13f68ad9af7ab479, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9b0d4ada9fd304fc13f68ad9af7ab479->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9b0d4ada9fd304fc13f68ad9af7ab479, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
    Py_DECREF( tmp_import_from_6__module );
    tmp_import_from_6__module = NULL;

    {
        PyObject *tmp_assign_source_51;
        tmp_assign_source_51 = LIST_COPY( const_list_6d19bd168aedc8c770efc9770862d79d_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$layout, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_51 );
    }

    return MOD_RETURN_VALUE( module_prompt_toolkit$layout );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
