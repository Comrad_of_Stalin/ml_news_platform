/* Generated code for Python module 'appnope._nope'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_appnope$_nope" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_appnope$_nope;
PyDictObject *moduledict_appnope$_nope;

/* The declarations of module constants used, if any. */
static PyObject *const_int_pos_1099511627776;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_digest_8716175da5d8b952a007edfc8eace7a7;
static PyObject *const_str_digest_8401a37508fc1918dbf758d6da78b2cc;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_bytes;
static PyObject *const_tuple_str_digest_2bb1dc26a4510c75fe01e8ca2ee6545e_tuple;
extern PyObject *const_str_plain_encode;
extern PyObject *const_str_plain_endActivity;
extern PyObject *const_tuple_str_plain_s_tuple;
static PyObject *const_str_plain_NSActivityIdleDisplaySleepDisabled;
extern PyObject *const_str_plain_LoadLibrary;
extern PyObject *const_str_plain_sel_registerName;
extern PyObject *const_tuple_str_plain_utf8_tuple;
extern PyObject *const_str_digest_581e9157f4cfd7a80dd5ba063afd246e;
static PyObject *const_str_digest_8f43fc54a25806031cc347a1de8bb50f;
static PyObject *const_str_plain_NSActivitySuddenTerminationDisabled;
extern PyObject *const_str_plain_c_void_p;
extern PyObject *const_tuple_str_plain_classname_tuple;
static PyObject *const_str_digest_fcbd9edf4d93a6fe9fa9667d52e00a27;
static PyObject *const_str_plain_processInfo;
static PyObject *const_tuple_str_plain_options_str_plain_reason_str_plain_activity_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_contextlib;
extern PyObject *const_str_plain_nope_scope;
extern PyObject *const_str_plain_reason;
static PyObject *const_str_digest_b85dea2d8f5be5b27829b3e8980650d3;
extern PyObject *const_str_plain_options;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_plain_nope;
extern PyObject *const_str_plain_napping_allowed;
extern PyObject *const_str_plain_n;
static PyObject *const_tuple_str_plain_NSString_tuple;
static PyObject *const_str_plain_NSActivityAutomaticTerminationDisabled;
static PyObject *const_str_plain_NSActivityLatencyCritical;
extern PyObject *const_str_plain_nap;
static PyObject *const_tuple_str_plain_processInfo_tuple;
extern PyObject *const_str_plain_argtypes;
static PyObject *const_str_plain_NSActivityUserInitiated;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_objc_msgSend;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain__utf8;
extern PyObject *const_str_plain_restype;
static PyObject *const_int_pos_1095216660480;
extern PyObject *const_tuple_str_plain_contextmanager_tuple;
extern PyObject *const_str_plain_util;
extern PyObject *const_tuple_str_plain_objc_tuple;
extern PyObject *const_str_plain_beginActivityWithOptions;
static PyObject *const_str_digest_76801b13d1dd9546d4ff3e143486b2fa;
extern PyObject *const_str_plain_contextmanager;
extern PyObject *const_tuple_str_empty_tuple;
extern PyObject *const_str_plain___all__;
static PyObject *const_str_digest_73dfae5394a947c478d57d4aca773a45;
static PyObject *const_tuple_str_plain_activity_str_plain_NSProcessInfo_str_plain_info_tuple;
extern PyObject *const_str_plain_classname;
extern PyObject *const_str_digest_6c008f4548bc145f72bbe35f04cc981e;
extern PyObject *const_str_plain_objc_getClass;
extern PyObject *const_str_plain_ctypes;
extern PyObject *const_int_0;
static PyObject *const_str_plain_NSActivityIdleSystemSleepDisabled;
static PyObject *const_str_plain_c_uint64;
extern PyObject *const_int_pos_1048576;
extern PyObject *const_str_plain_msg;
static PyObject *const_tuple_str_digest_4cbbe46f2be8a2215ca4a75fa16b9836_tuple;
static PyObject *const_str_digest_c0ccf90d649b0a702a06691c4b3d6147;
extern PyObject *const_str_plain_origin;
extern PyObject *const_int_pos_16777215;
extern PyObject *const_str_plain_C;
static PyObject *const_str_digest_4cbbe46f2be8a2215ca4a75fa16b9836;
extern PyObject *const_str_plain_objc;
extern PyObject *const_int_pos_32768;
extern PyObject *const_int_pos_16384;
static PyObject *const_str_digest_0555461c4bdcfbd26f9919949ed4354d;
extern PyObject *const_str_plain_cdll;
static PyObject *const_str_plain__theactivity;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain_NSString;
static PyObject *const_tuple_str_digest_fcbd9edf4d93a6fe9fa9667d52e00a27_tuple;
static PyObject *const_str_digest_8894e254d65e8f039433016b8960c715;
static PyObject *const_str_digest_97ffaaa9b2f6c1bd9b2b876b378b184b;
extern PyObject *const_str_plain_utf8;
static PyObject *const_str_digest_2ab1eea7107da4a33feb8927b5bddda1;
static PyObject *const_str_plain_NSProcessInfo;
static PyObject *const_str_digest_2bb1dc26a4510c75fe01e8ca2ee6545e;
extern PyObject *const_str_plain_void_p;
extern PyObject *const_str_plain_activity;
extern PyObject *const_int_pos_255;
extern PyObject *const_str_digest_f92034c4cbc30a0d45873404c1f32ad3;
extern PyObject *const_tuple_str_plain_name_tuple;
static PyObject *const_tuple_str_plain_NSProcessInfo_tuple;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_ull;
static PyObject *const_str_plain_NSActivityBackground;
static PyObject *const_str_digest_e664f0a69252ad9dfa061feb35a9ec1a;
static PyObject *const_tuple_ba5c5e7e671e8b044cf15f1420d31e4f_tuple;
extern PyObject *const_str_plain_info;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_find_library;
static PyObject *const_list_2bae79de3395bf6e29f8aa093735d1b7_list;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_int_pos_1099511627776 = PyLong_FromUnsignedLong( 1099511627776ul );
    const_str_digest_8401a37508fc1918dbf758d6da78b2cc = UNSTREAM_STRING_ASCII( &constant_bin[ 112467 ], 106, 0 );
    const_tuple_str_digest_2bb1dc26a4510c75fe01e8ca2ee6545e_tuple = PyTuple_New( 1 );
    const_str_digest_2bb1dc26a4510c75fe01e8ca2ee6545e = UNSTREAM_STRING_ASCII( &constant_bin[ 112573 ], 21, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_2bb1dc26a4510c75fe01e8ca2ee6545e_tuple, 0, const_str_digest_2bb1dc26a4510c75fe01e8ca2ee6545e ); Py_INCREF( const_str_digest_2bb1dc26a4510c75fe01e8ca2ee6545e );
    const_str_plain_NSActivityIdleDisplaySleepDisabled = UNSTREAM_STRING_ASCII( &constant_bin[ 112594 ], 34, 1 );
    const_str_digest_8f43fc54a25806031cc347a1de8bb50f = UNSTREAM_STRING_ASCII( &constant_bin[ 112628 ], 43, 0 );
    const_str_plain_NSActivitySuddenTerminationDisabled = UNSTREAM_STRING_ASCII( &constant_bin[ 112671 ], 35, 1 );
    const_str_digest_fcbd9edf4d93a6fe9fa9667d52e00a27 = UNSTREAM_STRING_ASCII( &constant_bin[ 112706 ], 12, 0 );
    const_str_plain_processInfo = UNSTREAM_STRING_ASCII( &constant_bin[ 112718 ], 11, 1 );
    const_tuple_str_plain_options_str_plain_reason_str_plain_activity_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_options_str_plain_reason_str_plain_activity_tuple, 0, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_str_plain_options_str_plain_reason_str_plain_activity_tuple, 1, const_str_plain_reason ); Py_INCREF( const_str_plain_reason );
    PyTuple_SET_ITEM( const_tuple_str_plain_options_str_plain_reason_str_plain_activity_tuple, 2, const_str_plain_activity ); Py_INCREF( const_str_plain_activity );
    const_str_digest_b85dea2d8f5be5b27829b3e8980650d3 = UNSTREAM_STRING_ASCII( &constant_bin[ 112729 ], 13, 0 );
    const_tuple_str_plain_NSString_tuple = PyTuple_New( 1 );
    const_str_plain_NSString = UNSTREAM_STRING_ASCII( &constant_bin[ 112742 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_NSString_tuple, 0, const_str_plain_NSString ); Py_INCREF( const_str_plain_NSString );
    const_str_plain_NSActivityAutomaticTerminationDisabled = UNSTREAM_STRING_ASCII( &constant_bin[ 112750 ], 38, 1 );
    const_str_plain_NSActivityLatencyCritical = UNSTREAM_STRING_ASCII( &constant_bin[ 112788 ], 25, 1 );
    const_tuple_str_plain_processInfo_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_processInfo_tuple, 0, const_str_plain_processInfo ); Py_INCREF( const_str_plain_processInfo );
    const_str_plain_NSActivityUserInitiated = UNSTREAM_STRING_ASCII( &constant_bin[ 112813 ], 23, 1 );
    const_int_pos_1095216660480 = PyLong_FromUnsignedLong( 1095216660480ul );
    const_str_digest_76801b13d1dd9546d4ff3e143486b2fa = UNSTREAM_STRING_ASCII( &constant_bin[ 112836 ], 36, 0 );
    const_str_digest_73dfae5394a947c478d57d4aca773a45 = UNSTREAM_STRING_ASCII( &constant_bin[ 112872 ], 32, 0 );
    const_tuple_str_plain_activity_str_plain_NSProcessInfo_str_plain_info_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_activity_str_plain_NSProcessInfo_str_plain_info_tuple, 0, const_str_plain_activity ); Py_INCREF( const_str_plain_activity );
    const_str_plain_NSProcessInfo = UNSTREAM_STRING_ASCII( &constant_bin[ 112904 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_activity_str_plain_NSProcessInfo_str_plain_info_tuple, 1, const_str_plain_NSProcessInfo ); Py_INCREF( const_str_plain_NSProcessInfo );
    PyTuple_SET_ITEM( const_tuple_str_plain_activity_str_plain_NSProcessInfo_str_plain_info_tuple, 2, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    const_str_plain_NSActivityIdleSystemSleepDisabled = UNSTREAM_STRING_ASCII( &constant_bin[ 112917 ], 33, 1 );
    const_str_plain_c_uint64 = UNSTREAM_STRING_ASCII( &constant_bin[ 112950 ], 8, 1 );
    const_tuple_str_digest_4cbbe46f2be8a2215ca4a75fa16b9836_tuple = PyTuple_New( 1 );
    const_str_digest_4cbbe46f2be8a2215ca4a75fa16b9836 = UNSTREAM_STRING_ASCII( &constant_bin[ 112958 ], 32, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4cbbe46f2be8a2215ca4a75fa16b9836_tuple, 0, const_str_digest_4cbbe46f2be8a2215ca4a75fa16b9836 ); Py_INCREF( const_str_digest_4cbbe46f2be8a2215ca4a75fa16b9836 );
    const_str_digest_c0ccf90d649b0a702a06691c4b3d6147 = UNSTREAM_STRING_ASCII( &constant_bin[ 112990 ], 22, 0 );
    const_str_digest_0555461c4bdcfbd26f9919949ed4354d = UNSTREAM_STRING_ASCII( &constant_bin[ 113012 ], 19, 0 );
    const_str_plain__theactivity = UNSTREAM_STRING_ASCII( &constant_bin[ 113031 ], 12, 1 );
    const_tuple_str_digest_fcbd9edf4d93a6fe9fa9667d52e00a27_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_fcbd9edf4d93a6fe9fa9667d52e00a27_tuple, 0, const_str_digest_fcbd9edf4d93a6fe9fa9667d52e00a27 ); Py_INCREF( const_str_digest_fcbd9edf4d93a6fe9fa9667d52e00a27 );
    const_str_digest_8894e254d65e8f039433016b8960c715 = UNSTREAM_STRING_ASCII( &constant_bin[ 113043 ], 46, 1 );
    const_str_digest_97ffaaa9b2f6c1bd9b2b876b378b184b = UNSTREAM_STRING_ASCII( &constant_bin[ 113089 ], 16, 0 );
    const_str_digest_2ab1eea7107da4a33feb8927b5bddda1 = UNSTREAM_STRING_ASCII( &constant_bin[ 113105 ], 73, 0 );
    const_tuple_str_plain_NSProcessInfo_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_NSProcessInfo_tuple, 0, const_str_plain_NSProcessInfo ); Py_INCREF( const_str_plain_NSProcessInfo );
    const_str_plain_ull = UNSTREAM_STRING_ASCII( &constant_bin[ 3654 ], 3, 1 );
    const_str_plain_NSActivityBackground = UNSTREAM_STRING_ASCII( &constant_bin[ 113178 ], 20, 1 );
    const_str_digest_e664f0a69252ad9dfa061feb35a9ec1a = UNSTREAM_STRING_ASCII( &constant_bin[ 113198 ], 160, 0 );
    const_tuple_ba5c5e7e671e8b044cf15f1420d31e4f_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_ba5c5e7e671e8b044cf15f1420d31e4f_tuple, 0, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_ba5c5e7e671e8b044cf15f1420d31e4f_tuple, 1, const_str_plain_reason ); Py_INCREF( const_str_plain_reason );
    PyTuple_SET_ITEM( const_tuple_ba5c5e7e671e8b044cf15f1420d31e4f_tuple, 2, const_str_plain_NSProcessInfo ); Py_INCREF( const_str_plain_NSProcessInfo );
    PyTuple_SET_ITEM( const_tuple_ba5c5e7e671e8b044cf15f1420d31e4f_tuple, 3, const_str_plain_NSString ); Py_INCREF( const_str_plain_NSString );
    PyTuple_SET_ITEM( const_tuple_ba5c5e7e671e8b044cf15f1420d31e4f_tuple, 4, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    PyTuple_SET_ITEM( const_tuple_ba5c5e7e671e8b044cf15f1420d31e4f_tuple, 5, const_str_plain_activity ); Py_INCREF( const_str_plain_activity );
    const_list_2bae79de3395bf6e29f8aa093735d1b7_list = PyList_New( 14 );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 0, const_str_plain_NSActivityIdleDisplaySleepDisabled ); Py_INCREF( const_str_plain_NSActivityIdleDisplaySleepDisabled );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 1, const_str_plain_NSActivityIdleSystemSleepDisabled ); Py_INCREF( const_str_plain_NSActivityIdleSystemSleepDisabled );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 2, const_str_plain_NSActivitySuddenTerminationDisabled ); Py_INCREF( const_str_plain_NSActivitySuddenTerminationDisabled );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 3, const_str_plain_NSActivityAutomaticTerminationDisabled ); Py_INCREF( const_str_plain_NSActivityAutomaticTerminationDisabled );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 4, const_str_plain_NSActivityUserInitiated ); Py_INCREF( const_str_plain_NSActivityUserInitiated );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 5, const_str_digest_8894e254d65e8f039433016b8960c715 ); Py_INCREF( const_str_digest_8894e254d65e8f039433016b8960c715 );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 6, const_str_plain_NSActivityBackground ); Py_INCREF( const_str_plain_NSActivityBackground );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 7, const_str_plain_NSActivityLatencyCritical ); Py_INCREF( const_str_plain_NSActivityLatencyCritical );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 8, const_str_plain_beginActivityWithOptions ); Py_INCREF( const_str_plain_beginActivityWithOptions );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 9, const_str_plain_endActivity ); Py_INCREF( const_str_plain_endActivity );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 10, const_str_plain_nope ); Py_INCREF( const_str_plain_nope );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 11, const_str_plain_nap ); Py_INCREF( const_str_plain_nap );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 12, const_str_plain_napping_allowed ); Py_INCREF( const_str_plain_napping_allowed );
    PyList_SET_ITEM( const_list_2bae79de3395bf6e29f8aa093735d1b7_list, 13, const_str_plain_nope_scope ); Py_INCREF( const_str_plain_nope_scope );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_appnope$_nope( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_7a1af2328286200d41af741491b777b7;
static PyCodeObject *codeobj_2ceb3a4b2e51b0cbdaeb2caf88819c48;
static PyCodeObject *codeobj_22d3c63337efd7bd12432647fe23f56c;
static PyCodeObject *codeobj_4695db8b57da51ef6ddef7da1575d767;
static PyCodeObject *codeobj_148803e32c7ad72014650357332a82f8;
static PyCodeObject *codeobj_25912bb3b36ae537599c9fe7d4e29815;
static PyCodeObject *codeobj_74ea5ec52692099c8abf5f7beff6e617;
static PyCodeObject *codeobj_caea3b89212a8e18c70c9c53a6e1e362;
static PyCodeObject *codeobj_9c1decd76cfc3e36438f564b9f8d6c2e;
static PyCodeObject *codeobj_5ccd5f67fa07880dac77345729b82d93;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_97ffaaa9b2f6c1bd9b2b876b378b184b );
    codeobj_7a1af2328286200d41af741491b777b7 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_c0ccf90d649b0a702a06691c4b3d6147, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_2ceb3a4b2e51b0cbdaeb2caf88819c48 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_C, 34, const_tuple_str_plain_classname_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_22d3c63337efd7bd12432647fe23f56c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__utf8, 24, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4695db8b57da51ef6ddef7da1575d767 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_beginActivityWithOptions, 49, const_tuple_ba5c5e7e671e8b044cf15f1420d31e4f_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_148803e32c7ad72014650357332a82f8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_endActivity, 69, const_tuple_str_plain_activity_str_plain_NSProcessInfo_str_plain_info_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_25912bb3b36ae537599c9fe7d4e29815 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_n, 30, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_74ea5ec52692099c8abf5f7beff6e617 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_nap, 85, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_caea3b89212a8e18c70c9c53a6e1e362 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_napping_allowed, 92, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9c1decd76cfc3e36438f564b9f8d6c2e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_nope, 77, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5ccd5f67fa07880dac77345729b82d93 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_nope_scope, 96, const_tuple_str_plain_options_str_plain_reason_str_plain_activity_tuple, 2, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope_maker( void );


static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_1__utf8(  );


static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_2_n(  );


static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_3_C(  );


static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_4_beginActivityWithOptions( PyObject *defaults );


static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_5_endActivity(  );


static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_6_nope(  );


static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_7_nap(  );


static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_8_napping_allowed(  );


static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_9_nope_scope( PyObject *defaults );


// The module function definitions.
static PyObject *impl_appnope$_nope$$$function_1__utf8( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_22d3c63337efd7bd12432647fe23f56c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_22d3c63337efd7bd12432647fe23f56c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_22d3c63337efd7bd12432647fe23f56c, codeobj_22d3c63337efd7bd12432647fe23f56c, module_appnope$_nope, sizeof(void *) );
    frame_22d3c63337efd7bd12432647fe23f56c = cache_frame_22d3c63337efd7bd12432647fe23f56c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_22d3c63337efd7bd12432647fe23f56c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_22d3c63337efd7bd12432647fe23f56c ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_s );
        tmp_isinstance_inst_1 = par_s;
        tmp_isinstance_cls_1 = (PyObject *)&PyBytes_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_s );
            tmp_called_instance_1 = par_s;
            frame_22d3c63337efd7bd12432647fe23f56c->m_frame.f_lineno = 27;
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_plain_utf8_tuple, 0 ) );

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 27;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_s;
                assert( old != NULL );
                par_s = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22d3c63337efd7bd12432647fe23f56c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22d3c63337efd7bd12432647fe23f56c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_22d3c63337efd7bd12432647fe23f56c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_22d3c63337efd7bd12432647fe23f56c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_22d3c63337efd7bd12432647fe23f56c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_22d3c63337efd7bd12432647fe23f56c,
        type_description_1,
        par_s
    );


    // Release cached frame.
    if ( frame_22d3c63337efd7bd12432647fe23f56c == cache_frame_22d3c63337efd7bd12432647fe23f56c )
    {
        Py_DECREF( frame_22d3c63337efd7bd12432647fe23f56c );
    }
    cache_frame_22d3c63337efd7bd12432647fe23f56c = NULL;

    assertFrameObject( frame_22d3c63337efd7bd12432647fe23f56c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_s );
    tmp_return_value = par_s;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_1__utf8 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_1__utf8 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_appnope$_nope$$$function_2_n( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_25912bb3b36ae537599c9fe7d4e29815;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_25912bb3b36ae537599c9fe7d4e29815 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_25912bb3b36ae537599c9fe7d4e29815, codeobj_25912bb3b36ae537599c9fe7d4e29815, module_appnope$_nope, sizeof(void *) );
    frame_25912bb3b36ae537599c9fe7d4e29815 = cache_frame_25912bb3b36ae537599c9fe7d4e29815;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_25912bb3b36ae537599c9fe7d4e29815 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_25912bb3b36ae537599c9fe7d4e29815 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_objc );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_objc );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "objc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sel_registerName );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__utf8 );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__utf8 );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_utf8" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_name );
        tmp_args_element_name_2 = par_name;
        frame_25912bb3b36ae537599c9fe7d4e29815->m_frame.f_lineno = 32;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_25912bb3b36ae537599c9fe7d4e29815->m_frame.f_lineno = 32;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_25912bb3b36ae537599c9fe7d4e29815 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_25912bb3b36ae537599c9fe7d4e29815 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_25912bb3b36ae537599c9fe7d4e29815 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_25912bb3b36ae537599c9fe7d4e29815, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_25912bb3b36ae537599c9fe7d4e29815->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_25912bb3b36ae537599c9fe7d4e29815, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_25912bb3b36ae537599c9fe7d4e29815,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_25912bb3b36ae537599c9fe7d4e29815 == cache_frame_25912bb3b36ae537599c9fe7d4e29815 )
    {
        Py_DECREF( frame_25912bb3b36ae537599c9fe7d4e29815 );
    }
    cache_frame_25912bb3b36ae537599c9fe7d4e29815 = NULL;

    assertFrameObject( frame_25912bb3b36ae537599c9fe7d4e29815 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_2_n );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_2_n );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_appnope$_nope$$$function_3_C( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_classname = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2ceb3a4b2e51b0cbdaeb2caf88819c48;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ceb3a4b2e51b0cbdaeb2caf88819c48, codeobj_2ceb3a4b2e51b0cbdaeb2caf88819c48, module_appnope$_nope, sizeof(void *) );
    frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 = cache_frame_2ceb3a4b2e51b0cbdaeb2caf88819c48;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_objc );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_objc );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "objc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_objc_getClass );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__utf8 );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__utf8 );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_utf8" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_classname );
        tmp_args_element_name_2 = par_classname;
        frame_2ceb3a4b2e51b0cbdaeb2caf88819c48->m_frame.f_lineno = 36;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_2ceb3a4b2e51b0cbdaeb2caf88819c48->m_frame.f_lineno = 36;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ceb3a4b2e51b0cbdaeb2caf88819c48, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ceb3a4b2e51b0cbdaeb2caf88819c48->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ceb3a4b2e51b0cbdaeb2caf88819c48, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ceb3a4b2e51b0cbdaeb2caf88819c48,
        type_description_1,
        par_classname
    );


    // Release cached frame.
    if ( frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 == cache_frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 )
    {
        Py_DECREF( frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 );
    }
    cache_frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 = NULL;

    assertFrameObject( frame_2ceb3a4b2e51b0cbdaeb2caf88819c48 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_3_C );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_classname );
    Py_DECREF( par_classname );
    par_classname = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_classname );
    Py_DECREF( par_classname );
    par_classname = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_3_C );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_appnope$_nope$$$function_4_beginActivityWithOptions( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_options = python_pars[ 0 ];
    PyObject *par_reason = python_pars[ 1 ];
    PyObject *var_NSProcessInfo = NULL;
    PyObject *var_NSString = NULL;
    PyObject *var_info = NULL;
    PyObject *var_activity = NULL;
    struct Nuitka_FrameObject *frame_4695db8b57da51ef6ddef7da1575d767;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_4695db8b57da51ef6ddef7da1575d767 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4695db8b57da51ef6ddef7da1575d767, codeobj_4695db8b57da51ef6ddef7da1575d767, module_appnope$_nope, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4695db8b57da51ef6ddef7da1575d767 = cache_frame_4695db8b57da51ef6ddef7da1575d767;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4695db8b57da51ef6ddef7da1575d767 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4695db8b57da51ef6ddef7da1575d767 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_C );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_C );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "C" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 57;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_str_plain_NSProcessInfo_tuple, 0 ) );

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_NSProcessInfo == NULL );
        var_NSProcessInfo = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_C );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_C );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "C" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 58;
        tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_plain_NSString_tuple, 0 ) );

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_NSString == NULL );
        var_NSString = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_msg );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_msg );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "msg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        CHECK_OBJECT( var_NSString );
        tmp_args_element_name_1 = var_NSString;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_n );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_n );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "n" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 60;
        tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_str_digest_2bb1dc26a4510c75fe01e8ca2ee6545e_tuple, 0 ) );

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__utf8 );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__utf8 );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_args_element_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_utf8" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_5;
        CHECK_OBJECT( par_reason );
        tmp_args_element_name_4 = par_reason;
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 60;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_reason;
            assert( old != NULL );
            par_reason = tmp_assign_source_3;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_msg );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_msg );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "msg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_6;
        CHECK_OBJECT( var_NSProcessInfo );
        tmp_args_element_name_5 = var_NSProcessInfo;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_n );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_n );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "n" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_7;
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 61;
        tmp_args_element_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_str_plain_processInfo_tuple, 0 ) );

        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 61;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_info == NULL );
        var_info = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_12;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_msg );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_msg );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "msg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_8;
        CHECK_OBJECT( var_info );
        tmp_args_element_name_7 = var_info;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_n );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_n );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "n" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_9;
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 63;
        tmp_args_element_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_digest_4cbbe46f2be8a2215ca4a75fa16b9836_tuple, 0 ) );

        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_ull );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ull );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_args_element_name_8 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ull" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_10;
        CHECK_OBJECT( par_options );
        tmp_args_element_name_10 = par_options;
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_args_element_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_8 );

            exception_lineno = 64;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_void_p );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_void_p );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_args_element_name_8 );
            Py_DECREF( tmp_args_element_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "void_p" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 65;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_11;
        CHECK_OBJECT( par_reason );
        tmp_args_element_name_12 = par_reason;
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 65;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
        }

        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_8 );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 65;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_4695db8b57da51ef6ddef7da1575d767->m_frame.f_lineno = 62;
        {
            PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_11 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_args_element_name_8 );
        Py_DECREF( tmp_args_element_name_9 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_activity == NULL );
        var_activity = tmp_assign_source_5;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4695db8b57da51ef6ddef7da1575d767 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4695db8b57da51ef6ddef7da1575d767 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4695db8b57da51ef6ddef7da1575d767, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4695db8b57da51ef6ddef7da1575d767->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4695db8b57da51ef6ddef7da1575d767, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4695db8b57da51ef6ddef7da1575d767,
        type_description_1,
        par_options,
        par_reason,
        var_NSProcessInfo,
        var_NSString,
        var_info,
        var_activity
    );


    // Release cached frame.
    if ( frame_4695db8b57da51ef6ddef7da1575d767 == cache_frame_4695db8b57da51ef6ddef7da1575d767 )
    {
        Py_DECREF( frame_4695db8b57da51ef6ddef7da1575d767 );
    }
    cache_frame_4695db8b57da51ef6ddef7da1575d767 = NULL;

    assertFrameObject( frame_4695db8b57da51ef6ddef7da1575d767 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_activity );
    tmp_return_value = var_activity;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_4_beginActivityWithOptions );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)par_reason );
    Py_DECREF( par_reason );
    par_reason = NULL;

    CHECK_OBJECT( (PyObject *)var_NSProcessInfo );
    Py_DECREF( var_NSProcessInfo );
    var_NSProcessInfo = NULL;

    CHECK_OBJECT( (PyObject *)var_NSString );
    Py_DECREF( var_NSString );
    var_NSString = NULL;

    CHECK_OBJECT( (PyObject *)var_info );
    Py_DECREF( var_info );
    var_info = NULL;

    CHECK_OBJECT( (PyObject *)var_activity );
    Py_DECREF( var_activity );
    var_activity = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)par_reason );
    Py_DECREF( par_reason );
    par_reason = NULL;

    Py_XDECREF( var_NSProcessInfo );
    var_NSProcessInfo = NULL;

    Py_XDECREF( var_NSString );
    var_NSString = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_4_beginActivityWithOptions );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_appnope$_nope$$$function_5_endActivity( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_activity = python_pars[ 0 ];
    PyObject *var_NSProcessInfo = NULL;
    PyObject *var_info = NULL;
    struct Nuitka_FrameObject *frame_148803e32c7ad72014650357332a82f8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_148803e32c7ad72014650357332a82f8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_148803e32c7ad72014650357332a82f8, codeobj_148803e32c7ad72014650357332a82f8, module_appnope$_nope, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_148803e32c7ad72014650357332a82f8 = cache_frame_148803e32c7ad72014650357332a82f8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_148803e32c7ad72014650357332a82f8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_148803e32c7ad72014650357332a82f8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_C );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_C );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "C" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_148803e32c7ad72014650357332a82f8->m_frame.f_lineno = 71;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_str_plain_NSProcessInfo_tuple, 0 ) );

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_NSProcessInfo == NULL );
        var_NSProcessInfo = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_msg );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_msg );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "msg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( var_NSProcessInfo );
        tmp_args_element_name_1 = var_NSProcessInfo;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_n );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_n );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "n" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        frame_148803e32c7ad72014650357332a82f8->m_frame.f_lineno = 72;
        tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_str_plain_processInfo_tuple, 0 ) );

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_148803e32c7ad72014650357332a82f8->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_info == NULL );
        var_info = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_msg );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_msg );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "msg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        CHECK_OBJECT( var_info );
        tmp_args_element_name_3 = var_info;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_n );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_n );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "n" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_5;
        frame_148803e32c7ad72014650357332a82f8->m_frame.f_lineno = 73;
        tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_str_digest_fcbd9edf4d93a6fe9fa9667d52e00a27_tuple, 0 ) );

        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_void_p );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_void_p );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_args_element_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "void_p" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_6;
        CHECK_OBJECT( par_activity );
        tmp_args_element_name_6 = par_activity;
        frame_148803e32c7ad72014650357332a82f8->m_frame.f_lineno = 73;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_4 );

            exception_lineno = 73;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_148803e32c7ad72014650357332a82f8->m_frame.f_lineno = 73;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_148803e32c7ad72014650357332a82f8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_148803e32c7ad72014650357332a82f8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_148803e32c7ad72014650357332a82f8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_148803e32c7ad72014650357332a82f8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_148803e32c7ad72014650357332a82f8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_148803e32c7ad72014650357332a82f8,
        type_description_1,
        par_activity,
        var_NSProcessInfo,
        var_info
    );


    // Release cached frame.
    if ( frame_148803e32c7ad72014650357332a82f8 == cache_frame_148803e32c7ad72014650357332a82f8 )
    {
        Py_DECREF( frame_148803e32c7ad72014650357332a82f8 );
    }
    cache_frame_148803e32c7ad72014650357332a82f8 = NULL;

    assertFrameObject( frame_148803e32c7ad72014650357332a82f8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_5_endActivity );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_activity );
    Py_DECREF( par_activity );
    par_activity = NULL;

    CHECK_OBJECT( (PyObject *)var_NSProcessInfo );
    Py_DECREF( var_NSProcessInfo );
    var_NSProcessInfo = NULL;

    CHECK_OBJECT( (PyObject *)var_info );
    Py_DECREF( var_info );
    var_info = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_activity );
    Py_DECREF( par_activity );
    par_activity = NULL;

    Py_XDECREF( var_NSProcessInfo );
    var_NSProcessInfo = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_5_endActivity );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_appnope$_nope$$$function_6_nope( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_9c1decd76cfc3e36438f564b9f8d6c2e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9c1decd76cfc3e36438f564b9f8d6c2e = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_9c1decd76cfc3e36438f564b9f8d6c2e, codeobj_9c1decd76cfc3e36438f564b9f8d6c2e, module_appnope$_nope, 0 );
    frame_9c1decd76cfc3e36438f564b9f8d6c2e = cache_frame_9c1decd76cfc3e36438f564b9f8d6c2e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9c1decd76cfc3e36438f564b9f8d6c2e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9c1decd76cfc3e36438f564b9f8d6c2e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_beginActivityWithOptions );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_beginActivityWithOptions );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "beginActivityWithOptions" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_digest_8894e254d65e8f039433016b8960c715 );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_digest_8894e254d65e8f039433016b8960c715 );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NSActivityUserInitiatedAllowingIdleSystemSleep" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        tmp_args_element_name_2 = const_str_digest_8716175da5d8b952a007edfc8eace7a7;
        frame_9c1decd76cfc3e36438f564b9f8d6c2e->m_frame.f_lineno = 80;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__theactivity, tmp_assign_source_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9c1decd76cfc3e36438f564b9f8d6c2e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9c1decd76cfc3e36438f564b9f8d6c2e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9c1decd76cfc3e36438f564b9f8d6c2e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9c1decd76cfc3e36438f564b9f8d6c2e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9c1decd76cfc3e36438f564b9f8d6c2e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9c1decd76cfc3e36438f564b9f8d6c2e,
        type_description_1
    );


    // Release cached frame.
    if ( frame_9c1decd76cfc3e36438f564b9f8d6c2e == cache_frame_9c1decd76cfc3e36438f564b9f8d6c2e )
    {
        Py_DECREF( frame_9c1decd76cfc3e36438f564b9f8d6c2e );
    }
    cache_frame_9c1decd76cfc3e36438f564b9f8d6c2e = NULL;

    assertFrameObject( frame_9c1decd76cfc3e36438f564b9f8d6c2e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_6_nope );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_appnope$_nope$$$function_7_nap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_74ea5ec52692099c8abf5f7beff6e617;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_74ea5ec52692099c8abf5f7beff6e617 = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_74ea5ec52692099c8abf5f7beff6e617, codeobj_74ea5ec52692099c8abf5f7beff6e617, module_appnope$_nope, 0 );
    frame_74ea5ec52692099c8abf5f7beff6e617 = cache_frame_74ea5ec52692099c8abf5f7beff6e617;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_74ea5ec52692099c8abf5f7beff6e617 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_74ea5ec52692099c8abf5f7beff6e617 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__theactivity );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__theactivity );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_theactivity" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 88;

            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = tmp_mvar_value_1;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_mvar_value_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_endActivity );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_endActivity );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "endActivity" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 89;

                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_2;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__theactivity );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__theactivity );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_theactivity" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 89;

                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = tmp_mvar_value_3;
            frame_74ea5ec52692099c8abf5f7beff6e617->m_frame.f_lineno = 89;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;

                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_assign_source_1;
            tmp_assign_source_1 = Py_None;
            UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__theactivity, tmp_assign_source_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_74ea5ec52692099c8abf5f7beff6e617 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_74ea5ec52692099c8abf5f7beff6e617 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_74ea5ec52692099c8abf5f7beff6e617, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_74ea5ec52692099c8abf5f7beff6e617->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_74ea5ec52692099c8abf5f7beff6e617, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_74ea5ec52692099c8abf5f7beff6e617,
        type_description_1
    );


    // Release cached frame.
    if ( frame_74ea5ec52692099c8abf5f7beff6e617 == cache_frame_74ea5ec52692099c8abf5f7beff6e617 )
    {
        Py_DECREF( frame_74ea5ec52692099c8abf5f7beff6e617 );
    }
    cache_frame_74ea5ec52692099c8abf5f7beff6e617 = NULL;

    assertFrameObject( frame_74ea5ec52692099c8abf5f7beff6e617 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_7_nap );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_appnope$_nope$$$function_8_napping_allowed( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_caea3b89212a8e18c70c9c53a6e1e362;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_caea3b89212a8e18c70c9c53a6e1e362 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_caea3b89212a8e18c70c9c53a6e1e362, codeobj_caea3b89212a8e18c70c9c53a6e1e362, module_appnope$_nope, 0 );
    frame_caea3b89212a8e18c70c9c53a6e1e362 = cache_frame_caea3b89212a8e18c70c9c53a6e1e362;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_caea3b89212a8e18c70c9c53a6e1e362 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_caea3b89212a8e18c70c9c53a6e1e362 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__theactivity );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__theactivity );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_theactivity" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = tmp_mvar_value_1;
        tmp_compexpr_right_1 = Py_None;
        tmp_return_value = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_caea3b89212a8e18c70c9c53a6e1e362 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_caea3b89212a8e18c70c9c53a6e1e362 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_caea3b89212a8e18c70c9c53a6e1e362 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_caea3b89212a8e18c70c9c53a6e1e362, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_caea3b89212a8e18c70c9c53a6e1e362->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_caea3b89212a8e18c70c9c53a6e1e362, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_caea3b89212a8e18c70c9c53a6e1e362,
        type_description_1
    );


    // Release cached frame.
    if ( frame_caea3b89212a8e18c70c9c53a6e1e362 == cache_frame_caea3b89212a8e18c70c9c53a6e1e362 )
    {
        Py_DECREF( frame_caea3b89212a8e18c70c9c53a6e1e362 );
    }
    cache_frame_caea3b89212a8e18c70c9c53a6e1e362 = NULL;

    assertFrameObject( frame_caea3b89212a8e18c70c9c53a6e1e362 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_8_napping_allowed );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_appnope$_nope$$$function_9_nope_scope( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_options = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_reason = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_options;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = par_reason;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_9_nope_scope );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)par_reason );
    Py_DECREF( par_reason );
    par_reason = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)par_reason );
    Py_DECREF( par_reason );
    par_reason = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_9_nope_scope );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope_locals {
    PyObject *var_activity;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope_locals *generator_heap = (struct appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_activity = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_5ccd5f67fa07880dac77345729b82d93, module_appnope$_nope, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_beginActivityWithOptions );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_beginActivityWithOptions );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "beginActivityWithOptions" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 105;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "options" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 105;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( generator->m_closure[0] );
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "reason" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 105;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( generator->m_closure[1] );
        generator->m_frame->m_frame.f_lineno = 105;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 105;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->var_activity == NULL );
        generator_heap->var_activity = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_expression_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_expression_name_1 = Py_None;
        Py_INCREF( tmp_expression_name_1 );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 107;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Preserve existing published exception.
    generator_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_type_1 );
    generator_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_value_1 );
    generator_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_tb_1 );

    if ( generator_heap->exception_keeper_tb_1 == NULL )
    {
        generator_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }
    else if ( generator_heap->exception_keeper_lineno_1 != 0 )
    {
        generator_heap->exception_keeper_tb_1 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_1, generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    PyException_SetTraceback( generator_heap->exception_keeper_value_1, (PyObject *)generator_heap->exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    // Tried code:
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_endActivity );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_endActivity );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "endActivity" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 109;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( generator_heap->var_activity );
        tmp_args_element_name_3 = generator_heap->var_activity;
        generator->m_frame->m_frame.f_lineno = 109;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 109;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
    if (unlikely( generator_heap->tmp_result == false ))
    {
        generator_heap->exception_lineno = 106;
    }

    if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
    generator_heap->type_description_1 = "cco";
    goto try_except_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_endActivity );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_endActivity );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "endActivity" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 109;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        CHECK_OBJECT( generator_heap->var_activity );
        tmp_args_element_name_4 = generator_heap->var_activity;
        generator->m_frame->m_frame.f_lineno = 109;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 109;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[0],
            generator->m_closure[1],
            generator_heap->var_activity
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_activity );
    generator_heap->var_activity = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->var_activity );
    Py_DECREF( generator_heap->var_activity );
    generator_heap->var_activity = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope_maker( void )
{
    return Nuitka_Generator_New(
        appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope_context,
        module_appnope$_nope,
        const_str_plain_nope_scope,
#if PYTHON_VERSION >= 350
        NULL,
#endif
        codeobj_5ccd5f67fa07880dac77345729b82d93,
        2,
        sizeof(struct appnope$_nope$$$function_9_nope_scope$$$genobj_1_nope_scope_locals)
    );
}



static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_1__utf8(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_appnope$_nope$$$function_1__utf8,
        const_str_plain__utf8,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_22d3c63337efd7bd12432647fe23f56c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_appnope$_nope,
        const_str_digest_6c008f4548bc145f72bbe35f04cc981e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_2_n(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_appnope$_nope$$$function_2_n,
        const_str_plain_n,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_25912bb3b36ae537599c9fe7d4e29815,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_appnope$_nope,
        const_str_digest_76801b13d1dd9546d4ff3e143486b2fa,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_3_C(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_appnope$_nope$$$function_3_C,
        const_str_plain_C,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2ceb3a4b2e51b0cbdaeb2caf88819c48,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_appnope$_nope,
        const_str_digest_f92034c4cbc30a0d45873404c1f32ad3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_4_beginActivityWithOptions( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_appnope$_nope$$$function_4_beginActivityWithOptions,
        const_str_plain_beginActivityWithOptions,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4695db8b57da51ef6ddef7da1575d767,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_appnope$_nope,
        const_str_digest_e664f0a69252ad9dfa061feb35a9ec1a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_5_endActivity(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_appnope$_nope$$$function_5_endActivity,
        const_str_plain_endActivity,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_148803e32c7ad72014650357332a82f8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_appnope$_nope,
        const_str_digest_73dfae5394a947c478d57d4aca773a45,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_6_nope(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_appnope$_nope$$$function_6_nope,
        const_str_plain_nope,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9c1decd76cfc3e36438f564b9f8d6c2e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_appnope$_nope,
        const_str_digest_2ab1eea7107da4a33feb8927b5bddda1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_7_nap(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_appnope$_nope$$$function_7_nap,
        const_str_plain_nap,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_74ea5ec52692099c8abf5f7beff6e617,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_appnope$_nope,
        const_str_digest_8f43fc54a25806031cc347a1de8bb50f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_8_napping_allowed(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_appnope$_nope$$$function_8_napping_allowed,
        const_str_plain_napping_allowed,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_caea3b89212a8e18c70c9c53a6e1e362,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_appnope$_nope,
        const_str_digest_0555461c4bdcfbd26f9919949ed4354d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_appnope$_nope$$$function_9_nope_scope( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_appnope$_nope$$$function_9_nope_scope,
        const_str_plain_nope_scope,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5ccd5f67fa07880dac77345729b82d93,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_appnope$_nope,
        const_str_digest_8401a37508fc1918dbf758d6da78b2cc,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_appnope$_nope =
{
    PyModuleDef_HEAD_INIT,
    "appnope._nope",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(appnope$_nope)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(appnope$_nope)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_appnope$_nope );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("appnope._nope: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("appnope._nope: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("appnope._nope: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initappnope$_nope" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_appnope$_nope = Py_InitModule4(
        "appnope._nope",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_appnope$_nope = PyModule_Create( &mdef_appnope$_nope );
#endif

    moduledict_appnope$_nope = MODULE_DICT( module_appnope$_nope );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_appnope$_nope,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_appnope$_nope,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_appnope$_nope,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_appnope$_nope,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_appnope$_nope );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_b85dea2d8f5be5b27829b3e8980650d3, module_appnope$_nope );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_7a1af2328286200d41af741491b777b7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_7a1af2328286200d41af741491b777b7 = MAKE_MODULE_FRAME( codeobj_7a1af2328286200d41af741491b777b7, module_appnope$_nope );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_7a1af2328286200d41af741491b777b7 );
    assert( Py_REFCNT( frame_7a1af2328286200d41af741491b777b7 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_contextlib;
        tmp_globals_name_1 = (PyObject *)moduledict_appnope$_nope;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_contextmanager_tuple;
        tmp_level_name_1 = const_int_0;
        frame_7a1af2328286200d41af741491b777b7->m_frame.f_lineno = 7;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_contextmanager );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_contextmanager, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_ctypes;
        tmp_globals_name_2 = (PyObject *)moduledict_appnope$_nope;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_7a1af2328286200d41af741491b777b7->m_frame.f_lineno = 9;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_ctypes, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_581e9157f4cfd7a80dd5ba063afd246e;
        tmp_globals_name_3 = (PyObject *)moduledict_appnope$_nope;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_7a1af2328286200d41af741491b777b7->m_frame.f_lineno = 10;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_ctypes, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_ctypes );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ctypes );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_2 = tmp_mvar_value_3;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cdll );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_LoadLibrary );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_ctypes );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ctypes );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ctypes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 12;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_4;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_util );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        frame_7a1af2328286200d41af741491b777b7->m_frame.f_lineno = 12;
        tmp_args_element_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_find_library, &PyTuple_GET_ITEM( const_tuple_str_plain_objc_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        frame_7a1af2328286200d41af741491b777b7->m_frame.f_lineno = 12;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_objc, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_ctypes );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ctypes );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ctypes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 14;

            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_5;
        tmp_assign_source_8 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_c_void_p );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_void_p, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_ctypes );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ctypes );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ctypes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 15;

            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_6;
        tmp_assign_source_9 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_c_uint64 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_ull, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_assattr_target_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_void_p );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_void_p );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "void_p" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 17;

            goto frame_exception_exit_1;
        }

        tmp_assattr_name_3 = tmp_mvar_value_7;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_objc );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_objc );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "objc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 17;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_8;
        tmp_assattr_target_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_objc_getClass );
        if ( tmp_assattr_target_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_restype, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_target_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_assattr_target_4;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_void_p );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_void_p );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "void_p" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 18;

            goto frame_exception_exit_1;
        }

        tmp_assattr_name_4 = tmp_mvar_value_9;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_objc );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_objc );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "objc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 18;

            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_10;
        tmp_assattr_target_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_sel_registerName );
        if ( tmp_assattr_target_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_restype, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_target_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_assattr_target_5;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_void_p );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_void_p );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "void_p" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 19;

            goto frame_exception_exit_1;
        }

        tmp_assattr_name_5 = tmp_mvar_value_11;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_objc );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_objc );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "objc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 19;

            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_12;
        tmp_assattr_target_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_objc_msgSend );
        if ( tmp_assattr_target_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_restype, tmp_assattr_name_5 );
        Py_DECREF( tmp_assattr_target_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_list_element_1;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_assattr_target_6;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_15;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_void_p );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_void_p );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "void_p" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 20;

            goto frame_exception_exit_1;
        }

        tmp_list_element_1 = tmp_mvar_value_13;
        tmp_assattr_name_6 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_assattr_name_6, 0, tmp_list_element_1 );
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_void_p );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_void_p );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_assattr_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "void_p" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 20;

            goto frame_exception_exit_1;
        }

        tmp_list_element_1 = tmp_mvar_value_14;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_assattr_name_6, 1, tmp_list_element_1 );
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_objc );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_objc );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_assattr_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "objc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 20;

            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_15;
        tmp_assattr_target_6 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_objc_msgSend );
        if ( tmp_assattr_target_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_6 );

            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_argtypes, tmp_assattr_name_6 );
        Py_DECREF( tmp_assattr_name_6 );
        Py_DECREF( tmp_assattr_target_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_16;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_objc );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_objc );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "objc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 22;

            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_16;
        tmp_assign_source_10 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_objc_msgSend );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_msg, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_appnope$_nope$$$function_1__utf8(  );



        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__utf8, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_appnope$_nope$$$function_2_n(  );



        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_n, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_appnope$_nope$$$function_3_C(  );



        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_C, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = const_int_pos_1099511627776;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivityIdleDisplaySleepDisabled, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = const_int_pos_1048576;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivityIdleSystemSleepDisabled, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = const_int_pos_16384;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivitySuddenTerminationDisabled, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = const_int_pos_32768;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivityAutomaticTerminationDisabled, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_mvar_value_17;
        tmp_left_name_1 = const_int_pos_16777215;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivityIdleSystemSleepDisabled );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NSActivityIdleSystemSleepDisabled );
        }

        CHECK_OBJECT( tmp_mvar_value_17 );
        tmp_right_name_1 = tmp_mvar_value_17;
        tmp_assign_source_18 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivityUserInitiated, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_left_name_2;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_right_name_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_mvar_value_19;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivityUserInitiated );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NSActivityUserInitiated );
        }

        CHECK_OBJECT( tmp_mvar_value_18 );
        tmp_left_name_2 = tmp_mvar_value_18;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivityIdleSystemSleepDisabled );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NSActivityIdleSystemSleepDisabled );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NSActivityIdleSystemSleepDisabled" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;

            goto frame_exception_exit_1;
        }

        tmp_operand_name_1 = tmp_mvar_value_19;
        tmp_right_name_2 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_1 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_19 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_digest_8894e254d65e8f039433016b8960c715, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = const_int_pos_255;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivityBackground, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = const_int_pos_1095216660480;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_NSActivityLatencyCritical, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_str_empty_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_22 = MAKE_FUNCTION_appnope$_nope$$$function_4_beginActivityWithOptions( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_beginActivityWithOptions, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = MAKE_FUNCTION_appnope$_nope$$$function_5_endActivity(  );



        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_endActivity, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = Py_None;
        UPDATE_STRING_DICT0( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain__theactivity, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = MAKE_FUNCTION_appnope$_nope$$$function_6_nope(  );



        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_nope, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = MAKE_FUNCTION_appnope$_nope$$$function_7_nap(  );



        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_nap, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = MAKE_FUNCTION_appnope$_nope$$$function_8_napping_allowed(  );



        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_napping_allowed, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_defaults_2;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_21;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_contextmanager );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_contextmanager );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "contextmanager" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_20;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_digest_8894e254d65e8f039433016b8960c715 );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_digest_8894e254d65e8f039433016b8960c715 );
        }

        CHECK_OBJECT( tmp_mvar_value_21 );
        tmp_tuple_element_1 = tmp_mvar_value_21;
        tmp_defaults_2 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_2, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_digest_8716175da5d8b952a007edfc8eace7a7;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_2, 1, tmp_tuple_element_1 );
        tmp_args_element_name_2 = MAKE_FUNCTION_appnope$_nope$$$function_9_nope_scope( tmp_defaults_2 );



        frame_7a1af2328286200d41af741491b777b7->m_frame.f_lineno = 96;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain_nope_scope, tmp_assign_source_28 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a1af2328286200d41af741491b777b7 );
#endif
    popFrameStack();

    assertFrameObject( frame_7a1af2328286200d41af741491b777b7 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a1af2328286200d41af741491b777b7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7a1af2328286200d41af741491b777b7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7a1af2328286200d41af741491b777b7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7a1af2328286200d41af741491b777b7, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = LIST_COPY( const_list_2bae79de3395bf6e29f8aa093735d1b7_list );
        UPDATE_STRING_DICT1( moduledict_appnope$_nope, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_29 );
    }

    return MOD_RETURN_VALUE( module_appnope$_nope );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
